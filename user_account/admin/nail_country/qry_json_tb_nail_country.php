<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_country_redirect']) && $_SESSION['ss_tb_nail_country_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_country_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_country_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_country_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_country_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_country_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_country->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_country_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_country_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_country_page'] = $v_page;
$_SESSION['ss_tb_nail_country_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_country = $cls_tb_nail_country->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_country as $arr){
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_country_name = isset($arr['country_name'])?$arr['country_name']:'';
	$v_country_status = isset($arr['country_status'])?$arr['country_status']:0;
	$v_country_order = isset($arr['country_order'])?$arr['country_order']:0;
	$v_country_total_state = isset($arr['country_total_state'])?$arr['country_total_state']:0;
	$v_country_total_users = isset($arr['country_total_users'])?$arr['country_total_users']:0;
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'country_id' => $v_country_id,
		'country_name' => $v_country_name,
		'country_status' => $v_country_status,
		'country_order' => $v_country_order,
		'country_total_state' => $v_country_total_state,
		'country_total_users' => $v_country_total_users
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_country'=>$arr_ret_data);
echo json_encode($arr_return);
?>