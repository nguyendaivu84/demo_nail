<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_country_id = 0;
$v_country_name = '';
$v_country_status = 0;
$v_country_order = 0;
$v_country_total_state = 0;
$v_country_total_users = 0;
$v_new_nail_country = true;
if(isset($_POST['btn_submit_tb_nail_country'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_country->set_mongo_id($v_mongo_id);
	$v_country_id = isset($_POST['txt_country_id'])?$_POST['txt_country_id']:$v_country_id;
	if(is_null($v_mongo_id)){
		$v_country_id = $cls_tb_nail_country->select_next('country_id');
	}
	$v_country_id = (int) $v_country_id;
	$cls_tb_nail_country->set_country_id($v_country_id);
	$v_country_name = isset($_POST['txt_country_name'])?$_POST['txt_country_name']:$v_country_name;
	$v_country_name = trim($v_country_name);
	if($v_country_name=='') $v_error_message .= '[Country Name] is empty!<br />';
	$cls_tb_nail_country->set_country_name($v_country_name);
	$v_country_status = isset($_POST['txt_country_status'])?$_POST['txt_country_status']:$v_country_status;
	$v_country_status = (int) $v_country_status;
	if($v_country_status<0) $v_error_message .= '[Country Status] is negative!<br />';
	$cls_tb_nail_country->set_country_status($v_country_status);
	$v_country_order = isset($_POST['txt_country_order'])?$_POST['txt_country_order']:$v_country_order;
	$v_country_order = (int) $v_country_order;
	if($v_country_order<0) $v_error_message .= '[Country Order] is negative!<br />';
	$cls_tb_nail_country->set_country_order($v_country_order);
	$v_country_total_state = isset($_POST['txt_country_total_state'])?$_POST['txt_country_total_state']:$v_country_total_state;
	$v_country_total_state = (int) $v_country_total_state;
	if($v_country_total_state<0) $v_error_message .= '[Country Total State] is negative!<br />';
	$cls_tb_nail_country->set_country_total_state($v_country_total_state);
	$v_country_total_users = isset($_POST['txt_country_total_users'])?$_POST['txt_country_total_users']:$v_country_total_users;
	$v_country_total_users = (int) $v_country_total_users;
	if($v_country_total_users<0) $v_error_message .= '[Country Total Users] is negative!<br />';
	$cls_tb_nail_country->set_country_total_users($v_country_total_users);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_country->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_country->update(array('_id' => $v_mongo_id));
			$v_new_nail_country = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_country_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_country) $v_nail_country_id = 0;
		}
	}
}else{
	$v_country_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_country_id,'int');
	if($v_country_id>0){
		$v_row = $cls_tb_nail_country->select_one(array('country_id' => $v_country_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_country->get_mongo_id();
			$v_country_id = $cls_tb_nail_country->get_country_id();
			$v_country_name = $cls_tb_nail_country->get_country_name();
			$v_country_status = $cls_tb_nail_country->get_country_status();
			$v_country_order = $cls_tb_nail_country->get_country_order();
			$v_country_total_state = $cls_tb_nail_country->get_country_total_state();
			$v_country_total_users = $cls_tb_nail_country->get_country_total_users();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>