<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_country").click(function(e){
		var css = '';

		var country_id = $("input#txt_country_id").val();
		country_id = parseInt(country_id, 10);
		css = isNaN(country_id)?'':'none';
		$("label#lbl_country_id").css("display",css);
		if(css == '') return false;
		var country_name = $("input#txt_country_name").val();
		country_name = $.trim(country_name);
		css = country_name==''?'':'none';
		$("label#lbl_country_name").css("display",css);
		if(css == '') return false;
		var country_status = $("input#txt_country_status").val();
		country_status = parseInt(country_status, 10);
		css = isNaN(country_status)?'':'none';
		$("label#lbl_country_status").css("display",css);
		if(css == '') return false;
		var country_order = $("input#txt_country_order").val();
		country_order = parseInt(country_order, 10);
		css = isNaN(country_order)?'':'none';
		$("label#lbl_country_order").css("display",css);
		if(css == '') return false;
		var country_total_state = $("input#txt_country_total_state").val();
		country_total_state = parseInt(country_total_state, 10);
		css = isNaN(country_total_state)?'':'none';
		$("label#lbl_country_total_state").css("display",css);
		if(css == '') return false;
		var country_total_users = $("input#txt_country_total_users").val();
		country_total_users = parseInt(country_total_users, 10);
		css = isNaN(country_total_users)?'':'none';
		$("label#lbl_country_total_users").css("display",css);
		if(css == '') return false;
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_nail_country').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Nail_country</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_nail_country" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_country_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_country_id" name="txt_country_id" value="<?php echo $v_country_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>Country Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_name" name="txt_country_name" value="<?php echo $v_country_name;?>" /> <label id="lbl_country_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Status</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_status" name="txt_country_status" value="<?php echo $v_country_status;?>" /> <label id="lbl_country_status" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Order</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_order" name="txt_country_order" value="<?php echo $v_country_order;?>" /> <label id="lbl_country_order" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Total State</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_total_state" name="txt_country_total_state" value="<?php echo $v_country_total_state;?>" /> <label id="lbl_country_total_state" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Total Users</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_total_users" name="txt_country_total_users" value="<?php echo $v_country_total_users;?>" /> <label id="lbl_country_total_users" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_nail_country" name="btn_submit_tb_nail_country" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
