<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_country_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_country_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_country_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_country = $cls_tb_nail_country->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_country = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_country .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_country .= '<th>Ord</th>';
$v_dsp_tb_nail_country .= '<th>Country Id</th>';
$v_dsp_tb_nail_country .= '<th>Country Name</th>';
$v_dsp_tb_nail_country .= '<th>Country Status</th>';
$v_dsp_tb_nail_country .= '<th>Country Order</th>';
$v_dsp_tb_nail_country .= '<th>Country Total State</th>';
$v_dsp_tb_nail_country .= '<th>Country Total Users</th>';
$v_dsp_tb_nail_country .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_country as $arr){
	$v_dsp_tb_nail_country .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_country .= '<td align="right">'.($v_count++).'</td>';
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_country_name = isset($arr['country_name'])?$arr['country_name']:'';
	$v_country_status = isset($arr['country_status'])?$arr['country_status']:0;
	$v_country_order = isset($arr['country_order'])?$arr['country_order']:0;
	$v_country_total_state = isset($arr['country_total_state'])?$arr['country_total_state']:0;
	$v_country_total_users = isset($arr['country_total_users'])?$arr['country_total_users']:0;
	$v_dsp_tb_nail_country .= '<td>'.$v_country_id.'</td>';
	$v_dsp_tb_nail_country .= '<td>'.$v_country_name.'</td>';
	$v_dsp_tb_nail_country .= '<td>'.$v_country_status.'</td>';
	$v_dsp_tb_nail_country .= '<td>'.$v_country_order.'</td>';
	$v_dsp_tb_nail_country .= '<td>'.$v_country_total_state.'</td>';
	$v_dsp_tb_nail_country .= '<td>'.$v_country_total_users.'</td>';
	$v_dsp_tb_nail_country .= '</tr>';
}
$v_dsp_tb_nail_country .= '</table>';
?>