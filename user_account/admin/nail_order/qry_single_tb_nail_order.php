<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_order_id = 0;
$v_date_create = date('Y-m-d H:i:s', time());
$v_user_create = '';
$v_user_id = 0;
$v_order_total = 0;
$v_order_status = 0;
$v_card_id = 0;
$v_address_book_id = 0;
$v_new_nail_order = true;
if(isset($_POST['btn_submit_tb_nail_order'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_order->set_mongo_id($v_mongo_id);
	$v_order_id = isset($_POST['txt_order_id'])?$_POST['txt_order_id']:$v_order_id;
	if(is_null($v_mongo_id)){
		$v_order_id = $cls_tb_nail_order->select_next('order_id');
	}
	$v_order_id = (int) $v_order_id;
	$cls_tb_nail_order->set_order_id($v_order_id);
	$v_date_create = isset($_POST['txt_date_create'])?$_POST['txt_date_create']:$v_date_create;
	if(!check_date($v_date_create)) $v_error_message .= '[Date Create] is invalid date/time!<br />';
	$cls_tb_nail_order->set_date_create($v_date_create);
	$v_user_create = isset($_POST['txt_user_create'])?$_POST['txt_user_create']:$v_user_create;
	$v_user_create = trim($v_user_create);
	if($v_user_create=='') $v_error_message .= '[User Create] is empty!<br />';
	$cls_tb_nail_order->set_user_create($v_user_create);
	$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:$v_user_id;
	$v_user_id = (int) $v_user_id;
	if($v_user_id<0) $v_error_message .= '[User Id] is negative!<br />';
	$cls_tb_nail_order->set_user_id($v_user_id);
	$v_order_total = isset($_POST['txt_order_total'])?$_POST['txt_order_total']:$v_order_total;
	$v_order_total = (float) $v_order_total;
	if($v_order_total<0) $v_error_message .= '[Order Total] is negative!<br />';
	$cls_tb_nail_order->set_order_total($v_order_total);
	$v_order_status = isset($_POST['txt_order_status'])?$_POST['txt_order_status']:$v_order_status;
	$v_order_status = (int) $v_order_status;
	if($v_order_status<0) $v_error_message .= '[Order Status] is negative!<br />';
	$cls_tb_nail_order->set_order_status($v_order_status);
	$v_card_id = isset($_POST['txt_card_id'])?$_POST['txt_card_id']:$v_card_id;
	$v_card_id = (int) $v_card_id;
	if($v_card_id<0) $v_error_message .= '[Card Id] is negative!<br />';
	$cls_tb_nail_order->set_card_id($v_card_id);
	$v_address_book_id = isset($_POST['txt_address_book_id'])?$_POST['txt_address_book_id']:$v_address_book_id;
	$v_address_book_id = (int) $v_address_book_id;
	if($v_address_book_id<0) $v_error_message .= '[Address Book Id] is negative!<br />';
	$cls_tb_nail_order->set_address_book_id($v_address_book_id);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_order->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_order->update(array('_id' => $v_mongo_id));
			$v_new_nail_order = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_order_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_order) $v_nail_order_id = 0;
		}
	}
}else{
	$v_order_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_order_id,'int');
	if($v_order_id>0){
		$v_row = $cls_tb_nail_order->select_one(array('order_id' => $v_order_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_order->get_mongo_id();
			$v_order_id = $cls_tb_nail_order->get_order_id();
			$v_date_create = date('Y-m-d H:i:s',$cls_tb_nail_order->get_date_create());
			$v_user_create = $cls_tb_nail_order->get_user_create();
			$v_user_id = $cls_tb_nail_order->get_user_id();
			$v_order_total = $cls_tb_nail_order->get_order_total();
			$v_order_status = $cls_tb_nail_order->get_order_status();
			$v_card_id = $cls_tb_nail_order->get_card_id();
			$v_address_book_id = $cls_tb_nail_order->get_address_book_id();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>