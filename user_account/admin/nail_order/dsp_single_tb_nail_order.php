<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_order").click(function(e){
		var css = '';

		var order_id = $("input#txt_order_id").val();
		order_id = parseInt(order_id, 10);
		css = isNaN(order_id)?'':'none';
		$("label#lbl_order_id").css("display",css);
		if(css == '') return false;
		var date_create = $("input#txt_date_create").val();
		css = check_date(date_create)?'':'none';
		$("label#lbl_date_create").css("display",css);
		if(css == '') return false;
		var user_create = $("input#txt_user_create").val();
		user_create = $.trim(user_create);
		css = user_create==''?'':'none';
		$("label#lbl_user_create").css("display",css);
		if(css == '') return false;
		var user_id = $("input#txt_user_id").val();
		user_id = parseInt(user_id, 10);
		css = isNaN(user_id)?'':'none';
		$("label#lbl_user_id").css("display",css);
		if(css == '') return false;
		var order_total = $("input#txt_order_total").val();
		order_total = parseFloat(order_total);
		css = isNaN(order_total)?'':'none';
		$("label#lbl_order_total").css("display",css);
		if(css == '') return false;
		var order_status = $("input#txt_order_status").val();
		order_status = parseInt(order_status, 10);
		css = isNaN(order_status)?'':'none';
		$("label#lbl_order_status").css("display",css);
		if(css == '') return false;
		var card_id = $("input#txt_card_id").val();
		card_id = parseInt(card_id, 10);
		css = isNaN(card_id)?'':'none';
		$("label#lbl_card_id").css("display",css);
		if(css == '') return false;
		var address_book_id = $("input#txt_address_book_id").val();
		address_book_id = parseInt(address_book_id, 10);
		css = isNaN(address_book_id)?'':'none';
		$("label#lbl_address_book_id").css("display",css);
		if(css == '') return false;
		return true;
	});
	$('input#txt_date_create').kendoDatePicker({format:"dd-MMM-yyyy"});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_nail_order').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Nail_order</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_nail_order" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_order_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_order_id" name="txt_order_id" value="<?php echo $v_order_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>Date Create</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_date_create" name="txt_date_create" value="<?php echo $v_date_create;?>" /> <label id="lbl_date_create" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>User Create</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_create" name="txt_user_create" value="<?php echo $v_user_create;?>" /> <label id="lbl_user_create" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>User Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" /> <label id="lbl_user_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Order Total</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_order_total" name="txt_order_total" value="<?php echo $v_order_total;?>" /> <label id="lbl_order_total" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Order Status</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_order_status" name="txt_order_status" value="<?php echo $v_order_status;?>" /> <label id="lbl_order_status" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Card Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_card_id" name="txt_card_id" value="<?php echo $v_card_id;?>" /> <label id="lbl_card_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Address Book Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_address_book_id" name="txt_address_book_id" value="<?php echo $v_address_book_id;?>" /> <label id="lbl_address_book_id" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_nail_order" name="btn_submit_tb_nail_order" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
