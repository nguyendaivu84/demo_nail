<?php if(!isset($v_sval)) die();?>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Orders</h3>
                    </div>
                    <div id="div_quick">
                    <div id="div_quick_search">
                    <form method="post" id="frm_quick_search">
                    <span class="k-textbox k-space-left" id="txt_quick_search">
                    <input type="text" name="txt_quick_search" placeholder="Search by ..." value="<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>" />
                    <a id="a_quick_search" style="cursor: pointer" class="k-icon k-i-search"></a>
                    <script type="text/javascript">
                        $(document).ready(function(e){
                            $('a#a_quick_search').click(function(e){
                                $('form#frm_quick_search').submit();
                            })
                        });
                    </script>
                    </span>
                    </form>
                    </div>
                    </div>
                    <div id="grid"></div>
				<script type="text/javascript">
					var window_search;
                    $(document).ready(function() {
                        var grid = $("#grid").kendoGrid({
                            dataSource: {
                                pageSize: 50,
                                page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
                                serverPaging: true,
                                serverSorting: true,
                                transport: {
                                    read: {
                                        url: "<?php echo URL.$v_admin_key;?>/json/",
                                        type: "POST",
                                        data: {txt_session_id:"<?php echo session_id();?>",txt_quick_search:'<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>', txt_search_company_id: '<?php echo $v_company_id;?>'}
                                    }
                                },
                                schema: {
                                    data: "tb_nail_order"
                                    ,total: function(data){
                                        return data.total_rows;
                                    }
                                },
                                type: "json"
                            },
                            pageSize: 50,
                            height: 430,
                            scrollable: true,
                            sortable: true,
                            //selectable: "single",
                            pageable: {
                                input: true,
                                refresh: true,
                                pageSizes: [10, 20, 30, 40, 50],
                                numeric: false
                            },
						columns: [
							{field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
							{field: "ref_no", title: "Ref no", type:"string", width:"30px", sortable: true },
							{field: "user_name", title: "Contact", type:"string", width:"50px", sortable: true},
							{field: "phone", title: "Phone", type:"string", width:"50px", sortable: true},
							{field: "email", title: "Email", type:"string", width:"50px", sortable: true},
							{field: "order_date", title: "Order Date", type:"string", width:"50px", sortable: true},
							{field: "order_amount", title: "Order amount", type:"string", width:"50px", sortable: true},
							{field: "status", title: "Status", type:"string", width:"50px", sortable: true},
							{ command:  [
								{ name: "View", text:'', click: view_row, imageClass: 'k-grid-View' }
								<?php if($v_edit_right || $v_is_admin){?>
								,{ name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
								<?php }?>
								<?php if($v_delete_right || $v_is_admin){?>
								,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
								<?php }?>
								],
								title: " ", width: "70px" }
						 ]
					 }).data("kendoGrid");
				});
              function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem._id+"/viewid";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to edit nail_order with ID: '+dataItem._id+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem._id+"/editid";
                    }
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete nail_order with ID: '+dataItem._id+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem._id+"/deleteid";
                    }
                }
            </script>
                </div>
            </div>
        </div>
  </div>