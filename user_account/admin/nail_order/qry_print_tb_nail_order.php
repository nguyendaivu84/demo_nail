<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_order_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_order_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_order_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_order = $cls_tb_nail_order->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_order = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_order .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_order .= '<th>Ord</th>';
$v_dsp_tb_nail_order .= '<th>Order Id</th>';
$v_dsp_tb_nail_order .= '<th>Date Create</th>';
$v_dsp_tb_nail_order .= '<th>User Create</th>';
$v_dsp_tb_nail_order .= '<th>User Id</th>';
$v_dsp_tb_nail_order .= '<th>Order Total</th>';
$v_dsp_tb_nail_order .= '<th>Order Status</th>';
$v_dsp_tb_nail_order .= '<th>Card Id</th>';
$v_dsp_tb_nail_order .= '<th>Address Book Id</th>';
$v_dsp_tb_nail_order .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_order as $arr){
	$v_dsp_tb_nail_order .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_order .= '<td align="right">'.($v_count++).'</td>';
	$v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
	$v_date_create = isset($arr['date_create'])?$arr['date_create']:(new MongoDate(time()));
	$v_user_create = isset($arr['user_create'])?$arr['user_create']:'';
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_order_total = isset($arr['order_total'])?$arr['order_total']:0;
	$v_order_status = isset($arr['order_status'])?$arr['order_status']:0;
	$v_card_id = isset($arr['card_id'])?$arr['card_id']:0;
	$v_address_book_id = isset($arr['address_book_id'])?$arr['address_book_id']:0;
	$v_dsp_tb_nail_order .= '<td>'.$v_order_id.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_date_create.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_user_create.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_order_total.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_order_status.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_card_id.'</td>';
	$v_dsp_tb_nail_order .= '<td>'.$v_address_book_id.'</td>';
	$v_dsp_tb_nail_order .= '</tr>';
}
$v_dsp_tb_nail_order .= '</table>';
?>