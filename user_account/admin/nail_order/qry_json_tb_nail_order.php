<?php if(!isset($v_sval)) die();
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
if($v_quick_search!='') $arr_where_clause ['$or'] = array(array("contact_name"=>new MongoRegex('/'.$v_quick_search.'/i')),array("name"=>new MongoRegex('/'.$v_quick_search.'/i')));
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_order_redirect']) && $_SESSION['ss_tb_nail_order_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_order_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_order_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_order_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_order_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_order_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_order->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_order_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_order_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_order_page'] = $v_page;
$_SESSION['ss_tb_nail_order_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_order = $cls_tb_nail_order->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_order as $arr){
    $v_code = isset($arr['code'])?$arr['code']:0;
	$v_user_name = isset($arr['contact_name'])?$arr['contact_name']:'';
	$v_phone = isset($arr['phone'])?$arr['phone']:'';
	$v_email = isset($arr['email'])?$arr['email']:'';
	$v_order_status = isset($arr['order_status'])?$arr['order_status']:0;
    $v_order_date = isset($arr['salesorder_date'])?date("d M,Y",$arr['salesorder_date']->sec):'';
    $v_order_amount = isset($arr['sum_amount'])?format_currency($arr['sum_amount']):"$ 0.00";
    $v_order_status = isset($arr['status'])?$arr['status']:"";
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'_id'=>(string)$arr['_id'],
		'ref_no' => $v_code,
		'user_name' => $v_user_name,
		'phone' => $v_phone,
		'email' => $v_email,
		'order_date' => (string)$v_order_date,
		'order_amount' => $v_order_amount,
		'status' => $v_order_status
	);
}
//die;
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_order'=>$arr_ret_data);
echo json_encode($arr_return);
?>