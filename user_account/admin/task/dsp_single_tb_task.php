<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_task").click(function(e){
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Task</h3>
                    </div>
                    <form id="frm_tb_task" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_thickness_id.'/edit';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_thickness_id" name="txt_thickness_id" value="<?php echo $v_thickness_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Activity</li>
                    </ul>
                    <div class="information div_details">
                        <?php if($v_mongo_id!='') {?>
                            <div>Click <a href="<?php echo URL; ?>jt/tasks/entry/<?php echo $v_mongo_id;?>">here</a> for more information</div>
                        <?php }?>
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td>Sale order Name</td>
                                <td style="width:1px;">&nbsp;</td>
                                <td align="left"><?php echo $v_sale_name;?></td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Name</td>
                                <td style="width:1px;">&nbsp;</td>
                                <td align="left"><?php echo $v_name;?> <label id="lbl_thickness_name" class="k-required">(*)</label></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Status</td>
                                <td>&nbsp;</td>
                                <td align="left"><?php echo $v_status;?></td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Time Start</td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <?php echo $v_time_start;?></td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Time End</td>
                                <td>&nbsp;</td>
                                <td align="left"><?php echo $v_time_end;?></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Rep name</td>
                                <td>&nbsp;</td>
                                <td align="left"><?php echo $v_our_rep;?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="other div_details">
                        <?php if($v_count) {?>
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <th>Content:</th>
                                    <th>Create by:</th>
                                    <th>Modified by:</th>
                                    <th>Type:</th>
                                </tr>
                                <?php foreach($arr_select_activity as $arr){ ?>
                                    <tr align="right" valign="top">
                                        <td><?php echo isset($arr['content']) ?$arr['content'] : '' ?></td>
                                        <td ><?php echo isset($arr['create_by']) ?$cls_tb_contact->select_scalar("full_name",array("_id"=>new MongoId($arr['create_by']))) : '' ?></td>
                                        <td ><?php echo isset($arr['date_modified']) ?$cls_tb_contact->select_scalar("full_name",array("_id"=>new MongoId($arr['date_modified']))) : '' ?></td>
                                        <td ><?php echo isset($arr['type']) ?$arr['type'] : '' ?>/td>
                                    </tr>
                                <?php } ?>
                            </table>
                        <?php }?>
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_task" name="btn_submit_tb_task" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
