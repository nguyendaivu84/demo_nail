<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_task_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_task_sort'])){
	$v_sort = $_SESSION['ss_tb_task_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_task = $cls_tb_task->select($arr_where_clause, $arr_sort);
$v_dsp_tb_task = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_task .= '<tr align="center" valign="middle">';
$v_dsp_tb_task .= '<th>Ord</th>';
$v_dsp_tb_task .= '<th>Thickness Id</th>';
$v_dsp_tb_task .= '<th>Thickness Name</th>';
$v_dsp_tb_task .= '<th>Thickness Size</th>';
$v_dsp_tb_task .= '<th>Thickness Type</th>';
$v_dsp_tb_task .= '</tr>';
$v_count = 1;
foreach($arr_tb_task as $arr){
	$v_dsp_tb_task .= '<tr align="left" valign="middle">';
	$v_dsp_tb_task .= '<td align="right">'.($v_count++).'</td>';
	$v_thickness_id = isset($arr['thickness_id'])?$arr['thickness_id']:0;
	$v_thickness_name = isset($arr['thickness_name'])?$arr['thickness_name']:'';
	$v_thickness_size = isset($arr['thickness_size'])?$arr['thickness_size']:'';
	$v_thickness_type = isset($arr['thickness_type'])?$arr['thickness_type']:'';
	$v_dsp_tb_task .= '<td>'.$v_thickness_id.'</td>';
	$v_dsp_tb_task .= '<td>'.$v_thickness_name.'</td>';
	$v_dsp_tb_task .= '<td>'.$v_thickness_size.'</td>';
	$v_dsp_tb_task .= '<td>'.$v_thickness_type.'</td>';
	$v_dsp_tb_task .= '</tr>';
}
$v_dsp_tb_task .= '</table>';
?>