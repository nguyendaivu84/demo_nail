<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$arr_where_clause['deleted'] = false;
$arr_where_clause['status'] = array('$in'=>array('New','Confirmed'));
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_task_redirect']) && $_SESSION['ss_tb_task_redirect']==1){
	if(isset($_SESSION['ss_tb_task_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_task_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_task_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_task_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_task_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_task->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_task_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_task_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_task_page'] = $v_page;
$_SESSION['ss_tb_task_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_task = $cls_tb_task->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_task as $arr){
	$v_thickness_id = (string)isset($arr['_id'])?$arr['_id']:0;
	$v_name = isset($arr['salesorder_name'])?$arr['salesorder_name']:'';
	$v_our_rep = isset($arr['our_rep'])?$arr['our_rep']:'';
	$v_status = isset($arr['status'])?$arr['status']:'';
	$v_work_start = isset($arr['work_start'])?date('d M,y',$arr['work_start']->sec):'';
	$v_work_end = isset($arr['work_end'])?date('M d,Y',$arr['work_end']->sec):'';
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'task_id' => (string)$v_thickness_id,
		'name' => $v_name,
		'our_rep' => $v_our_rep,
		'work_start' => $v_work_start,
		'status' => $v_status,
		'work_end' => $v_work_end
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_task'=>$arr_ret_data);
echo json_encode($arr_return);
?>