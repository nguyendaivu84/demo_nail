<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
if($v_quick_search!='') $arr_where_clause['name'] = new MongoRegex('/'.$v_quick_search.'/i');
//$arr_temp2 = unserialize($_SESSION['ss_tb_nail_category_where_clause']);
//if(isset($arr_temp2['name'])) $arr_where_clause['name'] = $arr_temp2['name'];
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_category_redirect']) && $_SESSION['ss_tb_nail_category_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_category_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_category_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_category_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_category_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_category_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_category->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_category_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_category_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_category_page'] = $v_page;
$_SESSION['ss_tb_nail_category_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_category = $cls_tb_nail_category->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_category as $arr){
	$v_category_id = (string)isset($arr['_id'])?$arr['_id']:'';
	$v_is_parent = isset($arr['is_parent'])?$arr['is_parent']:0;
    $v_is_parent = $v_is_parent == 1 ? "Yes" : "";
	$v_is_headline = isset($arr['is_headline'])?$arr['is_headline']:0;
    $v_is_headline = $v_is_headline == 1 ? "Yes" : "";
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_side = isset($arr['side'])?$arr['side']:0;
    $v_side = $v_side == 0 ? "Left" : "Right";
	$v_image_link = isset($arr['image_link'])?$arr['image_link']:'';
    $v_image_link = "resources/category/".$v_category_id."/".$v_image_link;
    if(file_exists($v_image_link)) $v_image_link = '<img src="'.URL.$v_image_link.'" style="max-width:150px" />';
    else $v_image_link = '';
	$v_status = isset($arr['status'])?$arr['status']:0;
    $v_status = $v_status == 1 ? "Active" : "Inactive";
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'category_id' => (string)$v_category_id,
		'is_parent' => $v_is_parent,
		'is_headline' => $v_is_headline,
		'name' => $v_name,
		'side' => $v_side,
		'image_link' => $v_image_link,
		'status' => $v_status
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_category'=>$arr_ret_data);
echo json_encode($arr_return);
?>