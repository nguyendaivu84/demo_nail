<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    var kit_grid;
    var index = 0;
    var kit_data = <?php echo json_encode($arr_language_content);?>;
    $(document).ready(function(){
        var editor_content = $('textarea#txt_content_content').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                path:"/resources/static_content/",
                dataType:'json',
                transport: {
                    read: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'list_images'}
                    },
                    destroy: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'delete_image_folder'}
                    },
                    create: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'create_image_folder'}
                    },
                    uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/upload-image",
                    thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/thumb-image"
                }
            }
        }).data("kendoEditor");
        var category_type = '<?php echo $v_category_type; ?>';
        category_type = parseInt(category_type,10);
        change_category_type(category_type);

        kit_grid = $('#kit_grid').kendoGrid({
            dataSource:{
                data: kit_data,
                batch: true,
                schema:{
                    model:{
                        fields:{
                            language: {editable:false},
                            language_value: {editable:true}
                        }
                    }
                }
                ,pageSize:20
            },
            height: 300,
            scrollable: true,
            navigatable: true,
            pageable: {
                input: true,
                numeric: false
            }
            ,editable: true
            ,columns: [
                {field: "language", title:"Language", width: "70px"},
                {field: "language_value", title:"Name", width: "70px",template: '<p style="text-align:center; margin:0">#=language_value#</p>' },
                { command: [{name:"Remove" ,click: remove_row, imageClass:"k-icon k-delete"}], title: "&nbsp;", width: "50px" }
            ]
        }).data("kendoGrid");
        function remove_row(e){
            e.preventDefault();
            kit_grid.removeRow($(e.currentTarget).closest("tr"));
        }
        function compare(pname,pvalue){
            var data = kit_grid.dataSource.data();
            var i= 0, found = false;
            while(i<data.length && !found){
                found = data[i].language == pname ;
                i++;
            }
            return found;
        }
        $("#txt_category_type").on("change",function(){
            var this_val = $(this).val();
            change_category_type(this_val);
        });
        $("input#btn_submit_tb_nail_category").click(function(e){
            var css = '';
            var name = $("input#txt_name").val();
            name = $.trim(name);
            css = name==''?'':'none';
            $("label#lbl_name").css("display",css);
            if(css == ''){
                alert("name empty");
                return false;
            }

            var kit_content = kit_grid.dataSource.data();
            var a_kit_content = [];
            for(var i=0; i<kit_content.length; i++){
                var one = {
                    language_type:kit_content[i].language
                    ,language_value:kit_content[i].language_value
                };
                a_kit_content.push(one);
            }
            $('input#txt_hidden_package_content').val(JSON.stringify(a_kit_content));
            return true;
        });
        $('input#txt_image_link').kendoUpload({
            multiple: false
        });
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        var tooltip = $("span.tooltips").kendoTooltip({
        filter:"a",
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        var validator = $("div.information").kendoValidator().data("kendoValidator");

        var multi_parent = $("#txt_group_parent").width(300).kendoMultiSelect({
            dataSource :<?php echo json_encode($arr_parent_list);  ?>,
            dataTextField: "name",
            dataValueField: "_id"
        }).data("kendoMultiSelect");
        var multi_headline = $("#txt_headline_item").width(300).kendoMultiSelect({
            dataSource :<?php echo json_encode($arr_headline_list);  ?>,
            dataTextField: "name",
            dataValueField: "_id"
        }).data("kendoMultiSelect");
        var multi_item_child = $("#txt_child_item").width(300).kendoMultiSelect({
            dataSource :<?php echo json_encode($arr_child_list);  ?>,
            dataTextField: "name",
            dataValueField: "_id"
        }).data("kendoMultiSelect");

        multi_parent.value(<?php echo json_encode($arr_parent_temp); ?>);
        multi_headline.value(<?php echo json_encode($arr_headline_temp); ?>);
        multi_item_child.value(<?php echo json_encode($arr_child_temp);?>);
        $("#txt_parent_id").width(300).kendoComboBox({
            dataTextField: "name",
            dataValueField: "_id"
        });
        var brand_data_temp = <?php echo json_encode($arr_brand) ; ?>;
        $("select#txt_brand_id").width(300).kendoComboBox({
            dataSource: brand_data_temp,
            dataTextField:'banner_name',
            dataValueField:'_id'
        });
        $("#txt_category_type").width(300).kendoComboBox({ });
        $("#txt_language").width(300).kendoComboBox({ });
        $("#txt_parent_list_option").width(300).kendoComboBox({ });
        $('input#txt_order_no').kendoNumericTextBox({
            format: "n0",
            step: 1
        });
        $("#btn_add_package").on("click",function(){
            var language_type = $("#txt_language").val();
            var language_value = $("#txt_language_value").val();
            if(language_value==''){
                alert("Value is empty!");
                return false;
            }
            if(!compare(language_type,language_value)){
                var data = kit_grid.dataSource.data();
                var tmp = [];
                var one = {
                    language: language_type,
                    language_value: language_value
                };
                tmp.push(one);
                for(var i= 0; i<data.length; i++){
                    tmp.push(data[i]);
                }
                kit_grid.dataSource.data(tmp);
                kit_grid.refresh();
            }
            else
            {
                alert(" The are maybe more than one duplicate material ! ");
            }
        });
    });
function change_category_type(category_type){
    if(category_type==0){
        $("#group_parent").hide();
        $("#headline").hide();
        $("#parent_list").hide();
        $("#child_item").hide();
        $("#headline_item").show();
    }else if(category_type == 1){
        $("#parent_list").show();
        $("#headline").hide();
        $("#group_parent").hide();
        $("#headline_item").hide();
        $("#child_item").show();
    }else if(category_type == 2){
        $("#headline").show();
        $("#headline_item").hide();
        $("#group_parent").hide();
        $("#child_item").hide();
        $("#parent_list").hide();
    }else if(category_type == 3){
        $("#headline").hide();
        $("#headline_item").hide();
        $("#group_parent").show();
        $("#child_item").hide();
        $("#parent_list").hide();
    }
}
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Category</h3>
                    </div>
                    <form id="frm_tb_nail_category" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_category_id.'/editid';?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_category_id" name="txt_category_id" value="<?php echo $v_category_id;?>" />
                    <input type="hidden" id="txt_old_parent" name="txt_old_parent" value="<?php echo $v_parent_id;?>" />
                    <input type="hidden" id="txt_category_one" name="txt_category_one" />
                    <input type="hidden" id="txt_category_all" name="txt_category_all" />
                    <input type="hidden" id="txt_category_all" name="txt_category_all" />
                        <input type="hidden" id="txt_hidden_package_content" name="txt_hidden_package_content" value="" />
                        <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <li>Other</li>
                        </ul>

                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td>Name</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_name" name="txt_name" value="<?php echo $v_name;?>" /> <label id="lbl_name" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Brand</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_brand_id" name="txt_brand_id">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Type</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_category_type" name="txt_category_type">
                                            <?php echo $v_dsp_category_type ; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                        <td>Order No</td>
                                        <td>&nbsp;</td>
                                        <td align="left"><input class="" style="width:300px;" type="text" id="txt_order_no" name="txt_order_no" value="<?php echo $v_order_no;?>" /> <label id="lbl_order_no" class="k-required">(*)</label></td>
                                </tr>
<!--                                <tr align="right" valign="top">-->
<!--                                    <td>Side</td>-->
<!--                                    <td>&nbsp;</td>-->
<!--                                    <td align="left">-->
<!--                                        <input type="radio" name="txt_side" id="txt_side" value="0" --><?php //echo $v_left ; ?><!-- >Left-->
<!--                                        <input type="radio" name="txt_side" id="txt_side" value="1" --><?php //echo $v_right ; ?><!--Right-->
<!--                                    </td>-->
<!--                                </tr>-->
                                <tr align="right" valign="top">
                                    <td>Image Link</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="hidden" name="txt_hidden_image"  value="<?php echo $v_hidden_image_link;?>" />
                                        <input type="file" id="txt_image_link" name="txt_image_link" accept="image/*" />
                                        <?php echo $v_image_link; ?>
                                    </td>
                                </tr>
                                <tr align="right" valign="top" id="parent_list">
                                    <td>Parent</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_parent_list_option" name="txt_parent_list_option">
                                            <option value=0>Select parent</option>
                                            <?php echo $arr_parent_list_option ; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top" id="child_item">
                                    <td>Child Item</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_child_item" name="txt_child_item[]" multiple="multiple">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top" id="headline_item">
                                    <td>Head Item</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_headline_item" name="txt_headline_item[]" multiple="multiple">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top" id="group_parent">
                                    <td>Parent List</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_group_parent" name="txt_group_parent[]" multiple="multiple">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top" id="headline">
                                    <td>Head line</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_parent_id" name="txt_parent_id" >
                                            <option value=0>select headline</option>
                                            <?php echo $arr_headline_list_option; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Options</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        Status:<input type="checkbox" id="txt_status" name="txt_status" <?php echo $v_status==1 ?"checked": ""; ?> /> Active ?
                                        <br />
                                        On Menu:<input type="checkbox" id="txt_on_menu" name="txt_on_menu" <?php echo $v_on_menu==1 ?"checked": ""; ?> /> Show on menu ?
                                        <br />
                                        On Brand:<input type="checkbox" id="txt_on_brand" name="txt_on_brand" <?php echo $v_on_brand==1 ?"checked": ""; ?> /> Show on brand ?
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Description</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 98%; padding:5px;"id="txt_content_content" name="txt_content_content"><?php echo $v_content_content;?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="other div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Name </td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="text" id="txt_language_value" name="txt_language_value" >
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Language </td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_language" name="txt_language">
                                            <?php echo $v_dsp_language ; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">&nbsp; </td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-button" id="btn_add_package" value="Add to Package" />
                                    </td>
                                </tr>
                            </table>
                            <div style="margin-top: 10px"></div>
                            <div id="kit_grid"></div>
                        </div>
                    </div>
                       <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                           <?php if($v_error_message!=''){?>
                                <div class="k-block k-widget k-error-colored div_errors">
                                    <?php echo $v_error_message;?>
                                </div>
                           <?php }?>
                            <div class="k-block k-widget div_buttons">
                                <input type="submit" id="btn_submit_tb_nail_category" name="btn_submit_tb_nail_category" value="Submit" class="k-button button_css" />
                            </div>
                       <?php }?>
                    </form>
                </div>
            </div>
        </div>
  </div>
