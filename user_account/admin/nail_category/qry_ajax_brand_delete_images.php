<?php if(!isset($v_sval)) die();?>
<?php
$v_index_image = isset($_POST['txt_index_image']) ? $_POST['txt_index_image'] : -1;
$v_static_id = isset($_POST['txt_static_id']) ? $_POST['txt_static_id'] : 0;
$v_action = isset($_POST['txt_action']) ? $_POST['txt_action'] : '';
$arr_response = array('error'=>1,'message'=>'','data'=>'');
if($v_index_image<=0 || $v_static_id <=0){
    $arr_response = array('error'=>1,'message'=>'Can not find the images ','data'=>'');
}
if($v_action!=''){
    $v_save_image = 'resource/static_content/';
    $arr_static_image = $cls_tb_nail_static_content->select_scalar("content_image",array("_id"=> new MongoId($v_static_id)));
    $arr_new_static_content = array();
    for($i=0;$i<count($arr_static_image);$i++){
        if($i!=$v_index_image) $arr_new_static_content [] = $arr_static_image[$i];
        else{
            @unlink($v_save_image.$arr_static_image[$i] );
        }
    }
    /*Delete images */
    $v_lst_image = '';
    if($v_action=='delete-images'){
        $v_result = $cls_tb_nail_static_content->update_field("content_image",$arr_new_static_content,array("_id"=> new MongoId($v_static_id)));
        if($v_result){
            for($i=0;$i<count($arr_new_static_content);$i++){
                $v_lst_image .= '<div style="float:left" class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                <img style="width:100px;height:100px"  src="'. RESOURCE_URL .'static_content/'. $arr_new_static_content[$i]. '">
                             </div> ' ;
            }
        }
    }
    $arr_response = array('error'=>0,'message'=>'','data'=>$v_lst_image);
}
else{
    $arr_response = array('error'=>1,'message'=>'Can not find act ','data'=>'');
}
die(json_encode($arr_response));
?>
