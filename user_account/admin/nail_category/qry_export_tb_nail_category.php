<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_category_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_category_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_category_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_category = $cls_tb_nail_category->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_nail_category_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Nail_category')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Nail_category');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Category Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Is Parent', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Is Headline', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Order No', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Side', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Link', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Parent Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Create Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Id Edit', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name Edit', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Status', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_nail_category as $arr){
	$v_excel_col = 1;
	$v_category_id = isset($arr['category_id'])?$arr['category_id']:0;
	$v_is_parent = isset($arr['is_parent'])?$arr['is_parent']:0;
	$v_is_headline = isset($arr['is_headline'])?$arr['is_headline']:0;
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_order_no = isset($arr['order_no'])?$arr['order_no']:0;
	$v_side = isset($arr['side'])?$arr['side']:0;
	$v_image_link = isset($arr['image_link'])?$arr['image_link']:'';
	$v_parent_id = isset($arr['parent_id'])?$arr['parent_id']:0;
	$v_user_create_id = isset($arr['user_create_id'])?$arr['user_create_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_id_edit = isset($arr['user_id_edit'])?$arr['user_id_edit']:0;
	$v_user_name_edit = isset($arr['user_name_edit'])?$arr['user_name_edit']:0;
	$v_status = isset($arr['status'])?$arr['status']:1;
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_category_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_is_parent, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_is_headline, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_order_no, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_side, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_link, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_parent_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_create_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_id_edit, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name_edit, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_status, 'right');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>