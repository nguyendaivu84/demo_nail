<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_category_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_category_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_category_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_category = $cls_tb_nail_category->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_category = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_category .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_category .= '<th>Ord</th>';
$v_dsp_tb_nail_category .= '<th>Category Id</th>';
$v_dsp_tb_nail_category .= '<th>Is Parent</th>';
$v_dsp_tb_nail_category .= '<th>Is Headline</th>';
$v_dsp_tb_nail_category .= '<th>Name</th>';
$v_dsp_tb_nail_category .= '<th>Order No</th>';
$v_dsp_tb_nail_category .= '<th>Side</th>';
$v_dsp_tb_nail_category .= '<th>Image Link</th>';
$v_dsp_tb_nail_category .= '<th>Parent Id</th>';
$v_dsp_tb_nail_category .= '<th>User Create Id</th>';
$v_dsp_tb_nail_category .= '<th>User Name</th>';
$v_dsp_tb_nail_category .= '<th>User Id Edit</th>';
$v_dsp_tb_nail_category .= '<th>User Name Edit</th>';
$v_dsp_tb_nail_category .= '<th>Status</th>';
$v_dsp_tb_nail_category .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_category as $arr){
	$v_dsp_tb_nail_category .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_category .= '<td align="right">'.($v_count++).'</td>';
	$v_category_id = isset($arr['category_id'])?$arr['category_id']:0;
	$v_is_parent = isset($arr['is_parent'])?$arr['is_parent']:0;
	$v_is_headline = isset($arr['is_headline'])?$arr['is_headline']:0;
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_order_no = isset($arr['order_no'])?$arr['order_no']:0;
	$v_side = isset($arr['side'])?$arr['side']:0;
	$v_image_link = isset($arr['image_link'])?$arr['image_link']:'';
	$v_parent_id = isset($arr['parent_id'])?$arr['parent_id']:0;
	$v_user_create_id = isset($arr['user_create_id'])?$arr['user_create_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_id_edit = isset($arr['user_id_edit'])?$arr['user_id_edit']:0;
	$v_user_name_edit = isset($arr['user_name_edit'])?$arr['user_name_edit']:0;
	$v_status = isset($arr['status'])?$arr['status']:1;
	$v_dsp_tb_nail_category .= '<td>'.$v_category_id.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_is_parent.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_is_headline.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_name.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_order_no.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_side.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_image_link.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_parent_id.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_user_create_id.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_user_name.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_user_id_edit.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_user_name_edit.'</td>';
	$v_dsp_tb_nail_category .= '<td>'.$v_status.'</td>';
	$v_dsp_tb_nail_category .= '</tr>';
}
$v_dsp_tb_nail_category .= '</table>';
?>