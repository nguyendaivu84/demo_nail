<?php if(!isset($v_sval)) die();
$v_error_message = '';
$v_content_content = '';
$v_mongo_id = NULL;
$v_category_id = 0;
$v_on_menu = 1;
$v_on_brand = 0;
$v_category_code = 0;
$v_is_parent = 0;
$v_is_headline = 0;
$v_name = '';
$v_order_no = 0;
$v_side = 0;
$v_hidden_image_link = '';
$v_image_link = '';
$v_brand_id = 0;
$arr_child_item = array();
$v_parent_id = 0;
$v_is_group = 0;
$v_user_create_id = 0;
$v_user_name = '';
$v_user_id_edit = 0;
$v_user_name_edit = 0;
$v_status = 1;
$v_category_type = 2;
$v_new_nail_category = true;
$v_left = "checked";
$v_right = "";
$v_dsp_language = "";
$arr_parent_list_option = '';
$arr_headline_list = array();
$arr_brand = array();
$arr_headline_temp = array();
$arr_parent_temp = array();
$arr_child_temp = array();
$arr_child_list = array();
$arr_language_content = array();
$arr_user = unserialize($_SESSION['ss_user']);
add_class('clsupload');
$cls_upload = new clsupload;
if(isset($_POST['btn_submit_tb_nail_category'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!=''){
        $v_mongo_id = new MongoID($v_mongo_id);
        $v_user_id_edit = (string)$arr_user['mongo_id'];
        $v_user_name_edit = (string)$arr_user['user_name'];
    }else{
        $v_mongo_id = NULL;
        $v_user_create_id = (string)$arr_user['mongo_id'];
        $v_user_name = (string)$arr_user['user_name'];
    }
	$cls_tb_nail_category->set_mongo_id($v_mongo_id);
    $v_category_code = $cls_tb_nail_category->select_next("category_code");
    $cls_tb_nail_category->set_code($v_category_code);
    $v_category_type = isset($_POST['txt_category_type'])?$_POST['txt_category_type']:$v_category_type;
    if($v_category_type == 0) {
        $v_is_parent = 1;
        $v_is_headline = 0;
        //die;
        $arr_child_item = isset($_POST['txt_headline_item'])?$_POST['txt_headline_item']:$arr_child_item;
        $arr_child_item_temp_2 = $cls_tb_nail_category->select_scalar("child_item" ,array("_id"=>new MongoId($v_mongo_id)));
        if(count($arr_child_item_temp_2)>0){
            for($i=0;$i<count($arr_child_item_temp_2);$i++){
                if(!in_array($arr_child_item_temp_2[$i],$arr_child_item)) {
                    $cls_tb_nail_category->update_field("parent_id",0 ,array("_id"=>new MongoId($arr_child_item_temp_2[$i])));
                }
            }
        }
    }else if($v_category_type == 1) {
        $v_is_parent = 0;
        $v_is_headline = 1;
        $arr_child_item = isset($_POST['txt_child_item'])?$_POST['txt_child_item']:$arr_child_item;
        $v_parent_id = isset($_POST['txt_parent_list_option'])?$_POST['txt_parent_list_option']:$v_parent_id;
        $arr_child_item_temp_2 = $cls_tb_nail_category->select_scalar("child_item" ,array("_id"=>new MongoId($v_mongo_id)));
        if(count($arr_child_item_temp_2)>0){
            for($i=0;$i<count($arr_child_item_temp_2);$i++){
                if(!in_array($arr_child_item_temp_2[$i],$arr_child_item)) {
                    $result = $cls_tb_nail_category->update_field("parent_id",0 ,array("_id"=>new MongoId($arr_child_item_temp_2[$i])));
                    if($v_mongo_id) $cls_tb_nail_category->update_field("child_item",$arr_child_item ,array("_id"=>new MongoId($v_mongo_id)));
                }
            }
        }
    }else if($v_category_type == 2) {
        $v_is_parent = 0;
        $v_is_headline = 0;
        $v_parent_id = isset($_POST['txt_parent_id'])?$_POST['txt_parent_id']:$v_parent_id;
        $v_string_id = (string)$v_mongo_id;
        if($v_string_id!=''){
            $v_old_parent_id = isset($_POST['txt_old_parent'])?$_POST['txt_old_parent']:0;
            $arr_old_data = $cls_tb_nail_category->select_scalar("child_item",array("_id"=>new MongoId($v_old_parent_id)));
            $arr_new_data = array();
            for($i=0;$i<count($arr_old_data);$i++){
                if($v_string_id!=$arr_old_data[$i]) $arr_new_data [] = $arr_old_data[$i];
            }
            $cls_tb_nail_category->update_field("child_item",$arr_new_data,array("_id"=>new MongoId($v_old_parent_id)));
        }
    }else{
        // group
        $v_is_parent = 0;
        $v_is_headline = 0;
        $v_parent_id = 0;
        $v_is_group = 1;
        $arr_child_item = isset($_POST['txt_group_parent'])?$_POST['txt_group_parent']:$arr_child_item;
    }
    $cls_tb_nail_category->set_is_group($v_is_group);
    $cls_tb_nail_category->set_is_parent($v_is_parent);
    $cls_tb_nail_category->set_is_headline($v_is_headline);
    $cls_tb_nail_category->set_parent_id($v_parent_id);
    $cls_tb_nail_category->set_child_item($arr_child_item);
	$v_name = isset($_POST['txt_name'])?$_POST['txt_name']:$v_name;
	$v_name = trim($v_name);
	if($v_name=='') $v_error_message .= '[Name] is empty!<br />';
	$cls_tb_nail_category->set_name($v_name);
    $v_brand_id = isset($_POST['txt_brand_id'])?$_POST['txt_brand_id']:$v_brand_id;
    $cls_tb_nail_category->set_brand_id($v_brand_id);
    $v_slugger = str_replace(" ","-",$v_name);
    $v_slugger = str_replace("&","",$v_slugger);
    $v_slugger = str_replace(";","",$v_slugger);
    $v_slugger = str_replace("^","",$v_slugger);
    $v_slugger = str_replace("%","",$v_slugger);
    $v_slugger = str_replace("$","",$v_slugger);
    $v_slugger .="-".$v_category_code;
    $cls_tb_nail_category->set_slugger($v_slugger);
	$v_order_no = isset($_POST['txt_order_no'])?$_POST['txt_order_no']:$v_order_no;
	$v_order_no = (int) $v_order_no;
	if($v_order_no<0) $v_error_message .= '[Order No] is negative!<br />';
	$cls_tb_nail_category->set_order_no($v_order_no);
	$v_side = isset($_POST['txt_side'])?$_POST['txt_side']:$v_side;
	$v_side = (int) $v_side;
	$cls_tb_nail_category->set_side($v_side);

    $v_package_content = isset($_POST['txt_hidden_package_content'])?$_POST['txt_hidden_package_content']:'';
    $v_package_content = stripcslashes($v_package_content);
    $arr_package_content = json_decode($v_package_content, true);
    if(!is_array($arr_package_content)) $arr_package_content = array();
    for($i=0;$i<count($arr_package_content);$i++){
        $arr_language_content[$arr_package_content[$i]['language_type']] = $arr_package_content[$i]['language_value'];
    }
    $cls_tb_nail_category->set_language_val($arr_language_content);
    // xu ly up anh
    $cls_upload->set_allow_overwrite(1);
    $v_upload_dir = PRODUCT_IMAGE_DIR.'category';
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
    if($v_allow_upload) $v_upload_dir.=DS.$v_mongo_id;
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
    $v_has_upload = false;
    if($v_allow_upload && isset($_FILES['txt_image_link']) && $v_mongo_id ){
        $cls_upload->set_destination_dir($v_upload_dir);
        $cls_upload->set_field_name('txt_image_link');
        $cls_upload->set_allow_array_extension(array('jpg', 'png'));
        $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
        $cls_upload->upload_process();
        $v_tmp_image_file = '';
        if($cls_upload->get_error_number()==0){
            $v_image_link = $cls_upload->get_filename();
//            $v_random_string = create_random_password(13);
//            $v_image_link .="_".date('d-M-y h:m:s').$v_random_string;
            $v_width = 0;
            list($width, $height) = @getimagesize($v_upload_dir.DS.$v_image_link);
            for($i=0; $i<count($arr_product_image_size); $i++){
                $v_width = $arr_product_image_size[$i];
                if($v_width < $width){
                    images_resize_by_width($v_width, $v_upload_dir.DS.$v_image_link,$v_upload_dir.DS.$v_width.'_'.$v_image_link );
                    if($v_width==PRODUCT_IMAGE_THUMB) $v_tmp_image_file = $v_image_link;
                }
            }
            if($v_tmp_image_file!='') $v_image_link = $v_tmp_image_file;
            $v_has_upload = true;
        }
    }
    if(!$v_has_upload){
        $v_image_link = isset($_POST['txt_hidden_image'])?$_POST['txt_hidden_image']:'';
    }
    $cls_tb_nail_category->set_image_link($v_image_link);
//
	$cls_tb_nail_category->set_user_create_id($v_user_create_id);
	$cls_tb_nail_category->set_user_name($v_user_name);
	$cls_tb_nail_category->set_user_id_edit($v_user_id_edit);
	$cls_tb_nail_category->set_user_name_edit($v_user_name_edit);
	$v_status = isset($_POST['txt_status'])?$_POST['txt_status']:$v_status;
    $v_status = $v_status=='on' ? 1 : 0;
    $cls_tb_nail_category->set_status($v_status);

    $v_on_menu = isset($_POST['txt_on_menu'])?$_POST['txt_on_menu']:$v_on_menu;
    $v_on_menu = $v_on_menu=='on' ? 1 : 0;
    $cls_tb_nail_category->set_is_on_menu($v_on_menu);

    $v_on_brand = isset($_POST['txt_on_brand'])?$_POST['txt_on_brand'] : '0';
    $v_on_brand = $v_on_brand=='on' ? 1 : 0;
    $cls_tb_nail_category->set_is_on_brand($v_on_brand);

    $v_content_content = isset($_POST['txt_content_content'])?$_POST['txt_content_content']:$v_content_content;
    $cls_tb_nail_category->set_description($v_content_content);

	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_category->insert();
            $cls_upload->set_allow_overwrite(1);
            $v_upload_dir = PRODUCT_IMAGE_DIR.'category';
            $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
            if($v_allow_upload) $v_upload_dir.=DS.$v_mongo_id;
            $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
            $v_has_upload = false;
            if($v_allow_upload && isset($_FILES['txt_image_link']) && $v_mongo_id ){
                $cls_upload->set_destination_dir($v_upload_dir);
                $cls_upload->set_field_name('txt_image_link');
                $cls_upload->set_allow_array_extension(array('jpg', 'png'));
                $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
                $cls_upload->upload_process();
                $v_tmp_image_file = '';
                if($cls_upload->get_error_number()==0){
                    $v_image_link = $cls_upload->get_filename();
                    list($width, $height) = @getimagesize($v_upload_dir.DS.$v_image_link);
                    for($i=0; $i<count($arr_product_image_size); $i++){
                        $v_width = $arr_product_image_size[$i];
                        if($v_width < $width){
                            images_resize_by_width($v_width, $v_upload_dir.DS.$v_image_link,$v_upload_dir.DS.$v_width.'_'.$v_image_link );
                            if($v_width==PRODUCT_IMAGE_THUMB) $v_tmp_image_file = $v_image_link;
                        }
                    }
                    if($v_tmp_image_file!='') $v_image_link = $v_tmp_image_file;
                    $v_has_upload = true;
                }
            }
            if(!$v_has_upload){
                $v_image_link = isset($_POST['txt_hidden_image'])?$_POST['txt_hidden_image']:'';
            }
            $cls_tb_nail_category->update_field("image_link",$v_image_link,array("_id"=> new MongoId($v_mongo_id)));
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_category->update(array('_id' => $v_mongo_id));
			$v_new_nail_category = false;
		}
        if($v_mongo_id){
            if($v_category_type==0){
                //parent . nothing to do
            }else if($v_category_type==1){
                //headline
                if(count($arr_child_item) >0){
                    // them parent id vao trong cac cat con
                    for($i=0;$i<count($arr_child_item);$i++){
                        $v_item_id = (string) $arr_child_item[$i];
                        $cls_tb_nail_category->update_field("parent_id",(string)$v_mongo_id,array("_id"=>new MongoId($v_item_id)));
                    }
                    $cls_tb_nail_category->update_field("child_item",$arr_child_item ,array("_id"=>new MongoId($v_mongo_id)));
                }
                if($v_parent_id !=0){
                    $v_id = (string)$v_mongo_id;
                    $arr_child_item_temp = $cls_tb_nail_category->select_scalar("child_item" ,array("_id"=>new MongoId($v_parent_id)));
                    // kiem tra xong trong parent da~ co no' chua
                    if(!in_array($v_id,$arr_child_item_temp)){
                        $arr_child_item_temp [] = $v_id;
                    }
                    else{
//                    $cls_tb_nail_category->update_field("parent_id",0,array("_id"=>new MongoId($v_id)));
                    }
                    $cls_tb_nail_category->update_field("child_item",$arr_child_item_temp ,array("_id"=>new MongoId($v_parent_id)));
                }
            }else if($v_category_type==2){
                //child nothing to do
            }
        }
		if($v_result){
			$_SESSION['ss_tb_nail_category_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_category) $v_nail_category_id = 0;
		}
	}
}else{
	$v_category_id= isset($_GET['id'])?$_GET['id']:'';
	if($v_category_id!=""){
		$v_row = $cls_tb_nail_category->select_one(array('_id' => new MongoId($v_category_id)));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_category->get_mongo_id();
			$v_is_parent = $cls_tb_nail_category->get_is_parent();
			$v_is_headline = $cls_tb_nail_category->get_is_headline();
			$v_name = $cls_tb_nail_category->get_name();
			$v_order_no = $cls_tb_nail_category->get_order_no();
			$v_side = $cls_tb_nail_category->get_side();
            if($v_side == 1){
                $v_right = "checked";
                $v_left = "";
            }
			$v_hidden_image_link = $cls_tb_nail_category->get_image_link();
            $v_image_link = "resources/products/".$v_category_id."/".$v_hidden_image_link;
            if(file_exists($v_image_link)) $v_image_link = '<img src="'.URL.$v_image_link.'" style="max-width:150px" />';
            else $v_image_link = '';
			$arr_child_item = $cls_tb_nail_category->get_child_item();
			$v_parent_id = $cls_tb_nail_category->get_parent_id();
            $v_is_group = $cls_tb_nail_category->get_is_group();
			$v_user_create_id = $cls_tb_nail_category->get_user_create_id();
			$v_user_name = $cls_tb_nail_category->get_user_name();
			$v_user_id_edit = $cls_tb_nail_category->get_user_id_edit();
			$v_user_name_edit = $cls_tb_nail_category->get_user_name_edit();
			$v_status = $cls_tb_nail_category->get_status();
            $v_brand_id = $cls_tb_nail_category->get_brand_id();
            $arr_language_content_temp = $cls_tb_nail_category->get_language_val();
            $v_content_content = $cls_tb_nail_category->get_description();
            $v_on_brand = $cls_tb_nail_category->get_is_on_brand();
            $v_on_menu = $cls_tb_nail_category->get_is_on_menu();
            foreach($arr_language_content_temp as $key =>$val){
                $arr_language_content [] = array("language"=>$key,"language_value"=>$val);
            }
            if($v_is_parent){
                $v_category_type = 0;
                $arr_headline_temp = $cls_tb_nail_category->get_child_item();
            }
            else if($v_is_headline){
                $v_category_type = 1;
                $arr_child_temp = $cls_tb_nail_category->get_child_item();
                $arr_select = $cls_tb_nail_category->select(array("parent_id"=>(string)$v_mongo_id));
                foreach($arr_select as $arr){
                    $v_id = (string)$arr['_id'];
//                    $arr_temp = array("_id"=>(string)$arr['_id'],"name"=>$arr['name']);
//                    $containsSearch = count(array_intersect($arr_temp, $arr_child_temp)) == count($arr_temp);
//                    if($containsSearch) $arr_child_temp [] = $arr_temp;
                    if(!in_array($v_id,$arr_child_temp)) $arr_child_temp [] = $v_id;
                }
            }else if($v_is_group){
                $v_category_type = 3;
                $arr_parent_temp = $cls_tb_nail_category->get_child_item();
            }
		}
	}
}
$arr_brand = get_array_data($cls_tb_banner,"_id","banner_name",$v_brand_id,array('0',"-------------"),array("banner_status"=>0),array("banner_name"=>1));
//category_side
$v_dsp_category_type = $cls_settings->draw_option_by_key("category_side",0,$v_category_type);
$arr_headline_list_option = $cls_tb_nail_category->draw_option("_id","name",(string)$v_parent_id,array("is_headline"=>1),array("name"=>1));
$arr_parent_list_option = $cls_tb_nail_category->draw_option("_id","name",(string)$v_parent_id,array("is_parent"=>1),array("name"=>1));
$arr_child_list = get_array_data($cls_tb_nail_category,"_id","name",$arr_child_item,array(),array('$or'=>array(array("is_parent"=>0,"is_headline"=>0),array("parent_id"=>(string)$v_mongo_id),array("parent_id"=>'0'),array("parent_id"=>'')),"status"=>1),array("name"=>1));
$arr_headline_list = get_array_data($cls_tb_nail_category,"_id","name",$arr_child_item,array(),array("is_headline"=>1,"parent_id"=>0,"status"=>1),array("name"=>1));
$arr_parent_list = get_array_data($cls_tb_nail_category,"_id","name",$arr_child_item,array(),array("is_parent"=>1,"status"=>1),array("name"=>1));
for($i=0;$i<count($arr_headline_temp);$i++){
    $v_id = $arr_headline_temp[$i] ;
    $v_name_temp = $cls_tb_nail_category->select_scalar("name",array("_id"=> new MongoId($v_id)));
    $arr_headline_list[] = array("_id"=>$v_id,"name"=>$v_name_temp);
}
for($i=0;$i<count($arr_child_temp);$i++){
    $v_id = $arr_child_temp[$i] ;
    $v_name_temp = $cls_tb_nail_category->select_scalar("name",array("_id"=> new MongoId($v_id)));
    $arr_child_list[] = array("_id"=>$v_id,"name"=>$v_name_temp);
}
for($i=0;$i<count($arr_parent_temp);$i++){
    $v_id = $arr_parent_temp[$i] ;
    $v_name_temp = $cls_tb_nail_category->select_scalar("name",array("_id"=> new MongoId($v_id)));
    $arr_parent_list[] = array("_id"=>$v_id,"name"=>$v_name_temp);
}
$v_dsp_language = $cls_settings->draw_option_by_key("language_setting",0,"");
?>