<?php if(!isset($v_sval)) die();?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
    <title><?php echo $v_main_site_title .' - '.  $v_sub_site_title;?> </title>
    <base href="<?php echo URL;?>" />
    <link href="<?php echo URL;?>lib/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URL;?>lib/css/special.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URL;?>lib/kendo/css/kendo.common.min.css" rel="stylesheet" />
    <link id="link_theme" href="<?php echo URL;?>lib/css/theme.<?php echo $v_default_theme;?>.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.resize.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/kendo/js/kendo.web.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo URL; ?>lib/js/common.js"></script>
    <?php echo js_fancybox();?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $("#menu").kendoMenu({
                dataUrlField: "LinksTo",
                <?php echo $v_dsp_horizontal_menu;?>
            });
            var treeview = $("#div_treeview").kendoTreeView({
                dataUrlField: "LinksTo",
                <?php echo $v_dsp_tree_menu;?>
            }).data("kendoTreeView");
            $('select#txt_company_id').width(250).kendoComboBox();
            $('select#txt_search_company_id').width(250).kendoComboBox();
            $('div#div_logo').bind('click', function(){
                document.location = '<?php echo URL."admin";?>';
            });
            $('div#div_right_pane').resize(function(){
                var splitter = $("#div_splitter_content").data("kendoSplitter");
                if(splitter) splitter.trigger("resize");
            });
        });
    </script>

</head>
<body>
<div id="div_wrapper">
    <div id="div_header" class="k-widget">
        <div id="div_logo"></div>
        <div id="div_info">
            <div id="div_account"><?php echo isset($v_user_full_name)&&$v_user_full_name!=""?$v_user_full_name:'';?> |<?php if(is_admin()){
                    add_class("cls_tb_support");
                    $cls_tb_support = new cls_tb_support($db);
                    $v_count = 0;
                    $arr_classe_2 = array("answer_by"=>array('$ne'=>"0"),"is_read"=>1,"date_answer"=>array('ne'=>array("")));
                    $arr_classe_1 = array("is_read"=>0);
                    $arr_where_count = array('$or'=>array($arr_classe_1,$arr_classe_2));
                    $v_count = $cls_tb_support->count($arr_where_count);
                    if($v_count>0){?>
                        <a style="color:red" href="<?php echo URL.'admin/system/support';?>"> Account
                            <span>(<?php echo $v_count;?>)</span>
                        </a>
                    <?php
                    }else echo " Account";} else echo " Account"; ?>  | <a href="<?php echo URL;?>admin/logout.html">Logout</a>
            </div>
            <div id="div_icon">
                <ul class="icons">
                    <li style="display: none">
                        <p><img id="icons_splitter" src="images/icons/arrow_right.png" title="Theme" /></p><p>Expand Pane</p>
                    </li>
                    <li>
                        <p><img id="icons_theme" src="images/icons/color_wheel.png" title="Theme" /></p><p>Theme</p>
                    </li>
                    <?php if($v_show_create_icon && @$v_act=='A'){?>
                        <li>
                            <a href="<?php echo URL.$v_admin_key.'/add';?>">
                                <p><img id="icons_add_new" src="images/icons/add.png" title="Add New" /></p><p>Add New</p>
                            </a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
        <div id="div_header_top">&nbsp;</div>
        <div id="div_header_menu">
            <div id="menu"></div>
        </div>
    </div>