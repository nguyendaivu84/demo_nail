<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_dsp_ship_history = '';
$v_mongo_id = NULL;
$v_location_id = 0;
$v_company_id = $_SESSION['company_id'];
$v_company_id = $_SESSION['ss_last_company_id'];;
$v_raw_id = '';
$v_anvy_id = '';
$v_po_number = '';
$v_order_type = 0;
$v_shipping_contact = '';
$v_total_order_amount = 0;
$v_total_discount = 0;
$v_billing_contact = '';
$v_net_order_amount = 0;
$v_gross_order_amount = 0;
$v_job_description = '';
$v_sale_rep = '';
$v_date_created = date('Y-M-d', time());
$v_date_required = '';
$v_date_modified = '';
$v_date_approved = '';
$v_term = 0;
$v_delivery_method = '';
$v_source = 0;
$v_status = 0;
$v_dispatch = 0;
$v_tax_1 = 0;
$v_tax_2 = 0;
$v_tax_3 = 0;
$v_order_ref = '';
$v_user_create = '';
$v_user_approved = '';
$v_user_modified = '';
$v_cost_kitting_temp = 0;
$dsp_order_template = new Template("order_detail.tpl",$v_dir_templates);
//$v_order_id = isset($_GET['id'])?$_GET['id']:0;
$v_dsp_tb_order_items = '';
$v_dsp_order_log = '';
$v_packaging_cost  = 0;
$v_total_amount_cost = 0;
$v_full_user_name = '';
$v_dispatch_content = '';
if($v_order_id!=0){
    $v_row = $cls_tb_order->select_one(array('order_id' => (int)$v_order_id));
    $arr_location_allocation = array();
    if($v_row == 1){
        $v_order_id = $cls_tb_order->get_order_id();
        $v_raw_id = $cls_tb_order->get_raw_id();
        $v_anvy_id = $cls_tb_order->get_anvy_id();
        $v_location_id = $cls_tb_order->get_location_id();
        $v_po_number = $cls_tb_order->get_po_number();
        $v_order_type = $cls_tb_order->get_order_type();
        $v_shipping_contact = $cls_tb_order->get_shipping_contact();
        $v_total_order_amount = $cls_tb_order->get_total_order_amount();
        $v_total_discount = $cls_tb_order->get_total_discount();
        $v_billing_contact = $cls_tb_order->get_billing_contact();
        $v_net_order_amount = $cls_tb_order->get_net_order_amount();
        $v_gross_order_amount = $cls_tb_order->get_gross_order_amount();
        $v_job_description = $cls_tb_order->get_description();
        $v_sale_rep = $cls_tb_order->get_sale_rep();
        $v_date_created = $cls_tb_order->get_date_created();
        if($v_date_created==NULL) $v_date_created = 'N/A';
        else $v_date_created = date('d-M-Y',$v_date_created);
        $v_date_modified = $cls_tb_order->get_date_modified();
        if($v_date_modified==NULL) $v_date_modified = 'N/A';
        else $v_date_modified = date('d-M-Y',$v_date_modified);
        $v_date_required = $cls_tb_order->get_date_required();
        if($v_date_required==NULL) $v_date_required = 'N/A';
        else $v_date_required = date('d-M-Y',$cls_tb_order->get_date_required());
        $v_date_approved = $cls_tb_order->get_date_approved();
        if($v_date_approved==NULL) $v_date_approved = 'N/A';
        else $v_date_approved = date('d-M-Y',$cls_tb_order->get_date_approved());
        $v_term = $cls_tb_order->get_term();
        $v_delivery_method = $cls_tb_order->get_delivery_method();
        $v_source = $cls_tb_order->get_source();
        $v_order_status = $cls_tb_order->get_status();
        settype($v_order_status,"int");
        $v_user_approved = $cls_tb_order->get_user_approved();
        $v_user_modified = $cls_tb_order->get_user_modified();
        $v_dispatch = $cls_tb_order->get_dispatch();
        $v_tax_1 = $cls_tb_order->get_tax_1();
        $v_tax_2 = $cls_tb_order->get_tax_2();
        $v_tax_3 = $cls_tb_order->get_tax_3();
        $v_user_allocate = $cls_tb_order->get_use_allocate();
        $v_cost_kitting = $cls_tb_order->get_cost_kitting();
        $v_user_create = $cls_tb_order->get_user_name();
        $v_company_id = $cls_tb_order->get_company_id();
        $v_order_location_id = $cls_tb_order->get_location_id();
        $v_order_ref = $cls_tb_order->get_order_ref();
        $v_company_code = "";
        $v_total = $cls_tb_company->select_one(array("company_id"=>$v_company_id));
        if($v_total>0) $v_company_code = $cls_tb_company->get_company_code();
    }else{
        redir(URL.'admin/error');
    }
    /* Distribution */
    $arr_distribution = array();
    $arr_where_clause = array('order_id' => (int)$v_order_id);
    $arr_tb_order_items = $cls_tb_order_items->select($arr_where_clause);
    foreach($arr_tb_order_items as $arr){
        $v_order_item_id = isset($arr['order_item_id']) ? $arr['order_item_id'] : 0 ;
        $v_product_id = isset($arr['product_id']) ?$arr['product_id'] :0 ;
        $v_product_code = isset($arr['product_code']) ?$arr['product_code'] :0 ;
        $v_product_description = isset($arr['product_description'])?$arr['product_description']:'';
        $v_product_quantity = isset($arr['quantity']) ? $arr['quantity'] : 0;
        $v_width = isset($arr['width']) ? $arr['width']:0 ;
        $v_length = isset($arr['length']) ? $arr['length']:0 ;
        $v_unit = isset($arr['unit']) ? $arr['unit'] : 0;
        $v_package_type = isset($arr['package_type'])?$arr['package_type']:0;
        $v_graphic_file = isset($arr['graphic_file']) ?$arr['graphic_file'] :'' ;
        $v_graphic_id =  isset($arr['graphic_id']) ?$arr['graphic_id'] :0;
        $v_current_price = isset($arr['current_price']) ?$arr['current_price'] : 0 ;
        $v_material_id = isset($arr['material_id']) ?$arr['material_id'] :0 ;
        $v_material_name = isset($arr['material_name']) ?$arr['material_name'] : '';
        $v_material_thickness_value = isset($arr['material_thickness_value']) ?$arr['material_thickness_value'] : 0;
        $v_material_thickness_unit = isset($arr['material_thickness_unit']) ?$arr['material_thickness_unit']:0 ;
        $v_material_color = isset($arr['material_color'])?$arr['material_color']:'' ;
        $v_item_status = isset($arr['status']) ?$arr['status']:0 ;
        $v_item_desc = isset($arr['description']) ?$arr['description']:'' ;
        $arr_allocation = isset($arr['allocation'])?$arr['allocation']:array();
        foreach($arr_allocation as $arr_loc){
            $v_location_id = $arr_loc['location_id'];
            $v_location_number= $cls_tb_location->select_scalar('location_number', array('location_id'=>(int)$v_location_id));
            $v_location_name = $arr_loc['location_name'];
            $v_location_address = $arr_loc['location_address'];
            $v_location_quantity = $arr_loc['location_quantity'];
            $v_location_price = $arr_loc['location_price'];
            $v_location_product_id = $arr_loc['product_id'];
            $v_location_product_name = $arr_loc['product_name'];
            $v_location_product_image = $arr_loc['product_image'];
            $v_location_tracking_number = isset($arr_loc['tracking_number']) && $arr_loc['tracking_number']!=''?$arr_loc['tracking_number']:'N/A';
            $v_location_tracking_company = isset($arr_loc['tracking_company']) && $arr_loc['tracking_company']!='' ?$arr_loc['tracking_company']:'N/A';
            $v_location_tracking_url = isset($arr_loc['tracking_url']) && $arr_loc['tracking_url']!='' ?$arr_loc['tracking_url']:'N/A';
            $v_date_shipping = isset($arr_loc['date_shipping']) && $arr_loc['date_shipping']!='' ?$arr_loc['date_shipping']:NULL;
            $v_product_infomation = $v_product_description.'<br>('. $v_width . '*'.$v_length .') '.$v_material_thickness_value .' '.$v_material_thickness_unit;
            $v_product_infomation .='<br>'.$v_material_name.' -'. $v_material_color;
            if(!isset($arr_distribution[$v_location_id]))
                $arr_distribution[$v_location_id][0]= array('location_name'=>$v_location_name
                    ,'location_number'=>$v_location_number
                    ,'location_address'=>$v_location_address
                    ,'tracking_name'=>$v_location_tracking_number
                    ,'tracking_number'=>$v_location_tracking_number
                    ,'tracking_url'=>$v_location_tracking_url
                    ,'tracking_company'=>$v_location_tracking_company
                    ,'date_shipping'=>$v_date_shipping
                );
            $arr_distribution[$v_location_id][1][] = array('product_infomation'=>$v_product_infomation,
                'quantity'=>$v_location_quantity
                ,'price'=>$v_location_price
                ,'graphic_file'=>$v_graphic_file
                ,'product_id'=>$v_product_id
                ,'order_item_id'=>$v_order_item_id
                ,'location_id'=>$v_location_id
            );
        }
    }
    $v_distribution = '';
    $v_total_distribution = 0;
    /* Explain */
    $arr_total_distribution = array();
    $v_location_count = 0;
    foreach($arr_distribution as $arr){
        $arr_location = $arr[0];
        $arr_product = $arr[1];
        $v_total_product = sizeof($arr_product);
        $v_location_count += 1 ;
        $dsp_product_dispatch_details = new Template("dsp_product_dispatch_details.tpl",$v_dir_templates);
        $dsp_product_dispatch_details->set("LOCATION_ORDER",$v_location_count);
        $dsp_product_dispatch_details->set("LOCATION_NAME",$arr_location['location_name']);
        $dsp_product_dispatch_details->set("LOCATION_NUMBER",$arr_location['location_number']);
        $dsp_product_dispatch_details->set("LOCATION_ADDRESS",$arr_location['location_address']);
        $dsp_product_dispatch_details->set("SHIPPING_COMPANY",$arr_location['tracking_company']);
        $dsp_product_dispatch_details->set("TRACKING_NUMBER",$arr_location['tracking_number']);
        $dsp_product_dispatch_details->set("TRACKING_URL",$arr_location['tracking_url']);
        if($arr_location['date_shipping']!=NULL || $arr_location['date_shipping']!='')
            $v_date = date('d-M-Y',strtotime($arr_location['date_shipping']));
        else $v_date = NO_PRICE;
        $dsp_product_dispatch_details->set("SHIPPING_DATE",$v_date);
        //$v_distribution .= '<div style="padding: 10px;">';
        /* Product lisssssst */
        $v_temp = '';
        $v_distribution .= '<table  width="100%" border="1" class="list_table"  cellpadding="3" cellspacing="0">                            ';
        $arr_dispath_temp = array();
        $arr_shipping_id = array();
        for($i=0;$i<$v_total_product;$i++){
            $dsp_order_dispatch_item = new Template("order_dispatch_item.tpl",$v_dir_templates);
            $v_infomation = $arr_product[$i]["product_infomation"];
            $v_orig_quantity = $arr_product[$i]["quantity"];
            $v_shipped_quantity = 0;
            $v_remain_quantity = 0;
            $v_price = $arr_product[$i]["price"];
            $v_graphic_file = URL . $arr_product[$i]["graphic_file"];
            $dsp_order_dispatch_item->set("PRODUCT_ID",$v_graphic_file);
            $dsp_order_dispatch_item->set("PRODUCT_NAME",$v_infomation);
            $dsp_order_dispatch_item->set("QUAN_ORIG",$v_orig_quantity);
            /* new */
            $v_row_count = $cls_tb_allocation->count(array("order_items_id"=>(int)$arr_product[$i]["order_item_id"]
            ,"location_id"=>(int)$arr_product[$i]["location_id"]
            ,"product_id"=>(int)$arr_product[$i]["product_id"]));
            if($v_row_count == 0) {
                $v_shipped_quantity = 0;
                $v_remain_quantity = $v_orig_quantity;
            }else {
                $arr_row_count = $cls_tb_allocation->select(array("order_items_id"=>(int)$arr_product[$i]["order_item_id"]
                ,"location_id"=>(int)$arr_product[$i]["location_id"]
                ,"product_id"=>(int)$arr_product[$i]["product_id"],"ship_complete"=>1));
                foreach($arr_row_count as $arr_temp){
                    /*
                     $v_shipping_id = (int)$arr_temp['shipping_id'];
                    $arr_shipping_infor = $cls_tb_shipping->select(array("shipping_id"=>$v_shipping_id));
                    foreach($arr_shipping_infor as $arr_shipping_temp){
                        $arr_shipping_details = $arr_shipping_temp['shipping_detail'];
                        foreach($arr_shipping_details as $arr_shipping_details_temp){
                            $v_dsp_ship_history .="has shipped =".$arr_shipping_temp['quantity']."on ".date('d-M-Y',$arr_shipping_temp['date_shipping'])
                            ."to ".$arr_shipping_temp['location_name']."<br />";
                        }
                    }
                     */
                    $v_shipped_his_quantity = isset($arr_temp['ship_quantity'])?$arr_temp['ship_quantity']:0;
                    $v_shipped_quantity += $arr_temp['quantity'] - $v_shipped_his_quantity;
                    $arr_shipping_id[] = $arr_temp['shipping_id'];
                }
                $v_remain_quantity = $v_orig_quantity - $v_shipped_quantity;
            }
            $dsp_order_dispatch_item->set("QUAN_SHIP",$v_shipped_quantity);
            $dsp_order_dispatch_item->set("QUAN_REMAIN",$v_remain_quantity);
            /* new */
            $arr_dispath_temp [] = $dsp_order_dispatch_item;
        }
        $v_shipping_history = '';
        if(count($arr_shipping_id)>0){
            $v_shipping_history = '<h3>Shipping History</h3>';
            $v_shipping_history .= '<table align="right" width="100%" style="margin-bottom: 20px;" border="1" class="list_table" cellpadding="3" cellspacing="0">';
            $v_shipping_history .= '<tr>';
                $v_shipping_history .= '<td>Tracking Company</td>';
                $v_shipping_history .= '<td>Tracking Number</td>';
                $v_shipping_history .= '<td>Tracking URL</td>';
                $v_shipping_history .= '<td>Tracking Address</td>';
                $v_shipping_history .= '<td>Date Shipping</td>';
                $v_shipping_history .= '<td>Quantity</td>';
                $v_shipping_history .= '<td>Shipping Details</td>';
            $v_shipping_history .= '</tr>';
            $arr_temp = $arr_shipping_id;
            $arr_shipping_id = array();
            for($i=0;$i<count($arr_temp);$i++){
                if(in_array($arr_temp[$i],$arr_shipping_id)==false){
                    $arr_shipping_id[] = $arr_temp[$i];
                }
            }
            for($i=0;$i<count($arr_shipping_id);$i++){
                $arr_shipping_history = $cls_tb_shipping->select(array("shipping_id"=>(int)$arr_shipping_id[$i]));
                foreach($arr_shipping_history as $arr){
                        $arr_shipping_details = $arr['shipping_detail'];
                        foreach($arr_shipping_details as $arr_2){
                            $v_shipping_history .= '<tr>';
                                $v_shipping_history .= '<td>'.$arr['shipper'].'</td>';
                                $v_shipping_history .= '<td>'.$arr['tracking_number'].'</td>';
                                $v_shipping_history .= '<td>'.$arr['tracking_url'].'</td>';
                                $v_shipping_history .= '<td>'.$cls_tb_location->get_shipping_address((int)$arr['location_id']).'</td>';
                                $v_shipping_history .= '<td>'.date('d-M-Y',$arr['date_shipping']->sec).'</td>';
                                $v_shipping_history .= '<td>'.$arr_2['quantity'].'</td>';
                                $v_shipping_history .= '<td>'.$arr_2['product_code'].' - '.$arr_2['width'].'*'.$arr_2['height'].' '.$arr_2['unit']
                                  .''.$arr_2['material_name'].' - '.$arr_2['material_thickness_value'].''.$arr_2['material_thickness_unit'].'</td>';
                            $v_shipping_history .= '</tr>';
                        }
                }
            }
            $v_shipping_history .='</table>';
        }
        $v_dsp_dispath_item = (isset($arr_dispath_temp)&&is_array($arr_dispath_temp))?Template::merge($arr_dispath_temp):'';
        $dsp_product_dispatch_details->set("TABLE_PRODUCT_DETAILS",$v_dsp_dispath_item);
        $dsp_product_dispatch_details->set("SHIPPING_HISTORY",$v_shipping_history);
        $v_distribution .= $dsp_product_dispatch_details->output();
        $arr_total_distribution[]['location_name'] = $arr_location["location_name"];
        $arr_total_distribution[]['location_number'] = $arr_location["location_number"];
        $arr_total_distribution[]['location_distribution'] = $v_temp;
        $v_total_distribution++;
    }
    if($v_user_allocate==1 ){
        $v_packaging_cost = $v_cost_kitting * $v_total_distribution;
        $v_total_amount_cost = $v_packaging_cost + $v_total_order_amount;
    }
    else{
        $v_total_amount_cost  = $v_total_order_amount;
    }

}
// starting template
$dsp_order_template->set("COMPANY_NAME",$cls_tb_company->select_scalar('company_name',array('company_id'=>(int)$v_company_id)));
$dsp_order_template->set("ORDER_ID",$v_order_id);
$dsp_order_template->set("ORDER_TYPE",$cls_settings->get_option_name_by_id('order_type',$v_order_type,0));
$dsp_order_template->set("PO_NUMBER",$v_po_number);
$dsp_order_template->set("JOB_DESCRIPTION",$v_job_description);
//$dsp_order_template->set("USER_CREATE",$cls_tb_contact->get_full_name_contact($v_contact_id));
$dsp_order_template->set("USER_LOCATION",$cls_tb_location->select_scalar('location_name',array('location_id'=>(int)$v_location_id)));
$dsp_order_template->set("DATE_CREATE",$v_date_created);
$dsp_order_template->set("DATE_REQUIRED",$v_date_required);
$v_date_modified_style = $v_date_modified!='N/A' ? '' :'display:none';
$dsp_order_template->set("MODIFY_STYLE",$v_date_modified_style);
$dsp_order_template->set("DATE_MODIFY",$v_date_modified);
$dsp_order_template->set("USER_MODIFY",$v_user_modified);
$dsp_order_template->set("ORDER_REF",$v_order_ref);
$dsp_order_template->set("ORDER_BY", $cls_tb_contact->get_infomation_contact($v_shipping_contact));
$dsp_order_template->set("PRINTING_COST", format_currency((float)$v_total_order_amount));
$dsp_order_template->set("NUMBER_DISTRIBUTION", number_format($v_total_distribution));
$v_cost_kitting_temp = $v_cost_kitting;
$v_cost_kitting = $v_user_allocate==1 ? format_currency($v_cost_kitting):NO_PRICE;
$dsp_order_template->set("KITTING_COST", $v_cost_kitting);
$dsp_order_template->set("TOTAL_AMOUNT", format_currency((float)$v_total_amount_cost));
$dsp_order_template->set("TOTAL_DISCOUNT", format_currency((float)$v_total_discount));
$dsp_order_template->set("BILLING_CONTACT", format_currency((float)$v_billing_contact));
$dsp_order_template->set("BILLING_CONTACT", $v_billing_contact);
$dsp_order_template->set("SALE_REP", $v_sale_rep);
$dsp_order_template->set("TERM", $v_term);
$v_delivery_method_name = '';
if($v_order_status !=50 && $v_order_status !=55 && $v_order_status !=60 && $v_order_status !=65 ){
    $v_delivery_method_name = "N/A";
}
else{
    $v_delivery_method_name =  $cls_settings->get_option_name_by_id('delivery_method',$v_delivery_method,0);
}
$dsp_order_template->set("DELIVER_METHOD", $v_delivery_method_name);
$dsp_order_template->set("SOURCE", $cls_settings->get_option_name_by_id('source_type',$v_source,0));
$dsp_order_template->set("ORDER_STATUS", $cls_settings->get_option_name_by_id('order_status',$v_order_status,0));
$v_dispatch_name = '';
if($v_order_status !=50 && $v_order_status !=55 && $v_order_status !=60 && $v_order_status !=65 )
    $v_dispatch_name = "<b>No</b>";
else $v_dispatch_name = "<b>".$cls_settings->get_option_name_by_id('dispatch',$v_dispatch,0)."</b>";
$dsp_order_template->set("DISPATCH", $v_dispatch_name);
// Start template item id
$v_count = 0;
$arr_order_item = array();
$arr_dispatch_item = array();
$arr_not_dispatch_item = array();
settype($v_company_id,"int");
$arr_location_id = array();
$arr_order_item_id = array();
foreach($arr_tb_order_items as $arr){
    $dsp_order_item_dispatch = new Template("order_item_dispatch.tpl",$v_dir_templates);
    $v_order_item_id = isset($arr['order_item_id'])?$arr['order_item_id']:0;
    $arr_order_item_id [] = $v_order_item_id;
    $v_order_id = isset($arr['order_id'])?$arr['order_id']:'';
    $v_product_id = isset($arr['product_id'])?$arr['product_id']:'';
    $v_product_code = isset($arr['product_code'])?$arr['product_code']:'';
    $v_quantity = isset($arr['quantity'])?$arr['quantity']:0;
    $v_description = isset($arr['description'])?$arr['description']:'';
    $v_customization_information = isset($arr['customization_information'])?$arr['customization_information']:'';
    $v_width = isset($arr['width'])?$arr['width']:0;
    $v_height = isset($arr['length'])?$arr['length']:0;
    $v_graphic_file = isset($arr['graphic_file'])?$arr['graphic_file']:'';
    $v_original_price = isset($arr['original_price'])?$arr['original_price']:0;
    $v_discount_price = isset($arr['discount_price'])?$arr['discount_price']:0;
    $v_current_price = isset($arr['current_price'])?$arr['current_price']:0;
    $v_unit_price = isset($arr['unit_price'])?$arr['unit_price']:0;
    $v_status = isset($arr['status'])?$arr['status']:0;
    $v_discount_type = isset($arr['discount_type'])?$arr['discount_type']:0;
    $v_discount_per_unit = isset($arr['discount_per_unit'])?$arr['discount_per_unit']:0;
    $v_net_price = isset($arr['net_price'])?$arr['net_price']:0;
    $v_extended_amount = isset($arr['extended_amount'])?$arr['extended_amount']:0;
    $v_unit = isset($arr['unit'])?$arr['unit']:'';
    $v_material_id = isset($arr['material_id'])?$arr['material_id']:0;
    $v_material_name = isset($arr['material_name'])?$arr['material_name']:'';
    $v_material_thickness_value = isset($arr['material_thickness_value'])?$arr['material_thickness_value']:'';
    $v_material_thickness_unit = isset($arr['material_thickness_unit'])?$arr['material_thickness_unit']:'';
    $v_material_color = isset($arr['material_color'])?$arr['material_color']:'';
    $v_total_price = isset($arr['total_price'])?$arr['total_price']:0;
    $arr_allocation = isset($arr['allocation'])?$arr['allocation']:array();
    $v_mongo_id = $arr['_id'];
    $v_count++;
    $dsp_order_item_dispatch->set("JOB_DESCRIPTION",$v_job_description);
    $dsp_order_item_dispatch->set("PO_NUMBER",$v_po_number);
    $dsp_order_item_dispatch->set("ORDER_ORDER_ITEM",$v_count);
    $dsp_order_item_dispatch->set("PRODUCT_CODE",$v_product_code);
    $v_image = '';
    if(trim($v_graphic_file)!='') $v_image = '<img src="'.URL. $v_graphic_file .'" />';
    $dsp_order_item_dispatch->set("PRODUCT_IMAGE",$v_image);
    $dsp_order_item_dispatch->set("PRODUCT_MATERIAL_WIDTH",$v_width. ' ' .$v_unit);
    $dsp_order_item_dispatch->set("PRODUCT_MATERIAL_HEIGHT",$v_height. ' ' .$v_unit);
    $dsp_order_item_dispatch->set("PRODUCT_MATERIAL_THICKNESS",$v_material_thickness_value. ' ' .$v_material_thickness_unit);
    $dsp_order_item_dispatch->set("PRODUCT_MATERIAL_COLOR",$v_material_color);
    //$dsp_order_item_dispatch->set("UNIT_PRICE",format_currency((float)$v_total_price/$v_quantity));
    $dsp_order_item_dispatch->set("UNIT_PRICE",format_currency((float)$v_current_price));
    $dsp_order_item_dispatch->set("QUANTITY",number_format($v_quantity));
    $v_total_allocation = sizeof($arr_allocation);
    $dsp_order_allocation = new Template("order_allocation.tpl",$v_dir_templates);
    /* ALLOCATION */
    $arr_allocation_item = array();
    $v_count_allocation = 0;
    for($j=0;$j<$v_total_allocation;$j++)
    {
        $dsp_order_allocate_item = new Template("order_allocate_item.tpl",$v_dir_templates);
        $v_location_id = isset($arr_allocation[$j]['location_id'])?$arr_allocation[$j]['location_id']:'';
        $arr_location_id[] = $v_location_id;
        $v_location_name = isset($arr_allocation[$j]['location_name'])?$arr_allocation[$j]['location_name']:'';
        $v_location_quantity = isset($arr_allocation[$j]['location_quantity'])?$arr_allocation[$j]['location_quantity']:0;
        $v_location_address = isset($arr_allocation[$j]['location_address'])?$arr_allocation[$j]['location_address']:'';
        $dsp_order_allocate_item->set('ALLOCATION_LOCATION_NUMBER',$cls_tb_location->select_scalar("location_number",array("location_id"=>(int)$v_location_id)));
        $dsp_order_allocate_item->set('ALLOCATION_LOCATION_NAME',$v_location_name);
        $dsp_order_allocate_item->set('ALLOCATION_LOCATION_QUANTITY',$v_location_quantity);
        $v_shipping_information = $cls_tb_location->get_shipping_address($v_location_id);
        $dsp_order_allocate_item->set('ALLOCATION_LOCATION_SHIPPING_ADDRESS',$v_shipping_information);
        settype($v_allocation_id, 'int');
        $arr_allocation_item [] = $dsp_order_allocate_item;
    }
    $dsp_order_item_dispatch->set("TOTAL_PRICE",format_currency($v_total_price));
    //$dsp_order_item_dispatch->set("TOTAL_PRICE",format_currency($v_quantity*$v_current_price));
    $v_content_allocation_list = (isset($arr_allocation_item)&&is_array($arr_allocation_item))?Template::merge($arr_allocation_item):'';
    $dsp_order_allocation->set("ALLOCATE_ITEM",$v_content_allocation_list);
    $dsp_order_item_dispatch->set("ALLOCATION",$dsp_order_allocation->output());
    $arr_order_item [] = $dsp_order_item_dispatch;
}
$v_content_all = (isset($arr_order_item)&&is_array($arr_order_item))?Template::merge($arr_order_item):'';
/*
// START DISPATCH
$arr_location_id = array_unique($arr_location_id);
$arr_dispatch_item = array();
$arr_dispatch_item_2 = array();
$arr_dispatch_item_content = array();
for($l=0;$l<count($arr_order_item_id);$l++){
    if(isset($arr_order_item_id[$l])){
        $v_order_item_id = $arr_order_item_id[$l];
        $arr_allocation_temp = $cls_tb_order_items->select_scalar("allocation",array("order_item_id"=>(int)$v_order_item_id));
        $v_location_count = 0;
        for($k = 0;$k <count($arr_allocation_temp);$k++){
            $v_location_count += 1 ;
            $dsp_product_dispatch_details = new Template("dsp_product_dispatch_details.tpl",$v_dir_templates);
            $dsp_product_dispatch_details->set("LOCATION_ORDER",$v_location_count);
            $dsp_product_dispatch_details->set("LOCATION_NAME",$arr_allocation_temp[$k]['location_name']);
            $dsp_product_dispatch_details->set("LOCATION_NUMBER",$cls_tb_location->select_scalar("location_number",array("location_id"=>$arr_allocation_temp[$k]['location_id'])));
            $dsp_product_dispatch_details->set("LOCATION_ADDRESS",$arr_allocation_temp[$k]['location_address']);
            $dsp_product_dispatch_details->set("SHIPPING_COMPANY",$arr_allocation_temp[$k]['tracking_company']);
            $dsp_product_dispatch_details->set("TRACKING_NUMBER",$arr_allocation_temp[$k]['tracking_number']);
            $dsp_product_dispatch_details->set("TRACKING_URL",$arr_allocation_temp[$k]['tracking_url']);
            if($arr_allocation_temp[$k]['date_shipping']!=NULL || $arr_allocation_temp[$k]['date_shipping']!='')
                $v_date = date('d-M-Y',strtotime($arr_allocation_temp[$k]['date_shipping']));
            else $v_date = NO_PRICE;
            $dsp_product_dispatch_details->set("SHIPPING_DATE",$v_date);

            $v_orig_quantity = $arr_allocation_temp[$k]['location_quantity'];
            $v_shipped_quantity = 0;
            $v_remain_quantity = 0;
            $v_compare_location_id = $arr_allocation_temp[$k]['location_id'];
               $dsp_order_dispatch_item = new Template("order_dispatch_item.tpl",$v_dir_templates);
            for($l2=0;$l2<count($arr_order_item_id);$l2++){
                $v_order_item_id_compare = $arr_order_item_id[$l2];
                $arr_allocation_compare_temp = $cls_tb_order_items->select_scalar("allocation",array("order_item_id"=>(int)$v_order_item_id_compare));
                for($yy=0;$yy<count($arr_allocation_compare_temp);$yy++ ){
                    //if($v_compare_location_id == $arr_allocation_compare_temp[$yy]['location_id'] && $v_order_item_id_compare!=$v_order_item_id)
                      //  echo "location $v_compare_location_id co product id ".$arr_allocation_compare_temp[$yy]['product_id']."<br />";
                    //else "<br />";
                }
            }
               $dsp_order_dispatch_item->set("PRODUCT_ID",$arr_allocation_temp[$k]['product_id']);
               $dsp_order_dispatch_item->set("PRODUCT_NAME",$arr_allocation_temp[$k]['product_name']);
               $dsp_order_dispatch_item->set("QUAN_ORIG",$v_orig_quantity);
               $v_row_count = $cls_tb_allocation->count(array("order_items_id"=>(int)$v_order_item_id
                   ,"location_id"=>(int)$arr_allocation_temp[$k]['location_id']
                   ,"product_id"=>(int)$arr_allocation_temp[$k]['product_id']));
               if($v_row_count == 0) {
                   $v_shipped_quantity = 0;
                   $v_remain_quantity = $v_orig_quantity;
               }else {
                   $v_row_count = $cls_tb_allocation->select_one(array("order_items_id"=>(int)$v_order_item_id
                       ,"location_id"=>(int)$arr_allocation_temp[$k]['location_id']
                       ,"product_id"=>$arr_allocation_temp[$k]['product_id'],"parent_allocation"=>0));
                   $v_shipped_quantity = $cls_tb_allocation->get_shipped_quantity();
                   $v_remain_quantity = $v_orig_quantity - $v_shipped_quantity;
               }
               $dsp_order_dispatch_item->set("QUAN_SHIP",$v_shipped_quantity);
               $dsp_order_dispatch_item->set("QUAN_REMAIN",$v_remain_quantity);
               $dsp_order_dispatch_item->set("QUAN_THIS",$v_shipped_quantity);
            $dsp_product_dispatch_details->set("TABLE_PRODUCT_DETAILS",$dsp_order_dispatch_item->output());
            $arr_dispatch_item_2 [] = $dsp_product_dispatch_details;
        }
    }
}
$v_dispatch_content = (isset($arr_dispatch_item_2)&&is_array($arr_dispatch_item_2))?Template::merge($arr_dispatch_item_2):'';
 */
?>
