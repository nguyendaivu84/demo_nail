<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_summarize_table = '';
$v_mongo_id = NULL;
$v_location_id = 0;
$v_company_id = $_SESSION['company_id'];
$v_company_id = $_SESSION['ss_last_company_id'];
$v_raw_id = '';
$v_anvy_id = '';
$v_po_number = '';
$v_order_type = 0;
$v_shipping_contact = '';
$v_total_order_amount = 0;
$v_total_discount = 0;
$v_billing_contact = '';
$v_net_order_amount = 0;
$v_gross_order_amount = 0;
$v_job_description = '';
$v_sale_rep = '';
$v_date_created = date('Y-M-d', time());
$v_date_required = '';
$v_date_modified = '';
$v_date_approved = '';
$v_term = 0;
$v_delivery_method = '';
$v_source = 0;
$v_status = 0;
$v_dispatch = 0;
$v_tax_1 = 0;
$v_tax_2 = 0;
$v_tax_3 = 0;
$v_order_ref = '';
$v_user_create = '';
$v_user_approved = '';
$v_user_modified = '';
$v_order_id = isset($_GET['id'])?$_GET['id']:0;
if($v_order_id!=0){
    $v_row = $cls_tb_order->select_one(array('order_id' => (int)$v_order_id));
    $arr_location_allocation = array();
    if($v_row == 1){
        $v_order_id = $cls_tb_order->get_order_id();
        $v_raw_id = $cls_tb_order->get_raw_id();
        $v_anvy_id = $cls_tb_order->get_anvy_id();
        $v_po_number = $cls_tb_order->get_po_number();
        $v_order_type = $cls_tb_order->get_order_type();
        $v_shipping_contact = $cls_tb_order->get_shipping_contact();
        $v_total_order_amount = $cls_tb_order->get_total_order_amount();
        $v_total_discount = $cls_tb_order->get_total_discount();
        $v_billing_contact = $cls_tb_order->get_billing_contact();
        $v_net_order_amount = $cls_tb_order->get_net_order_amount();
        $v_gross_order_amount = $cls_tb_order->get_gross_order_amount();
        $v_job_description = $cls_tb_order->get_description();
        $v_sale_rep = $cls_tb_order->get_sale_rep();
        $v_date_created = $cls_tb_order->get_date_created();
        if($v_date_created==NULL) $v_date_created = 'N/A';
        else $v_date_created = date('d-M-Y',$v_date_created);

        $v_date_modified = $cls_tb_order->get_date_modified();
        if($v_date_modified==NULL) $v_date_modified = 'N/A';
        else $v_date_modified = date('d-M-Y',$v_date_modified);

        $v_date_required = $cls_tb_order->get_date_required();
        if($v_date_required==NULL) $v_date_required = 'N/A';
        else $v_date_required = date('d-M-Y',$cls_tb_order->get_date_required());

        $v_date_approved = $cls_tb_order->get_date_approved();
        if($v_date_approved==NULL) $v_date_approved = 'N/A';
        else $v_date_approved = date('d-M-Y',$cls_tb_order->get_date_approved());
        $v_term = $cls_tb_order->get_term();
        $v_delivery_method = $cls_tb_order->get_delivery_method();
        $v_source = $cls_tb_order->get_source();
        $v_order_status = $cls_tb_order->get_status();
        $v_user_approved = $cls_tb_order->get_user_approved();
        $v_user_modified = $cls_tb_order->get_user_modified();
        $v_dispatch = $cls_tb_order->get_dispatch();
        $v_tax_1 = $cls_tb_order->get_tax_1();
        $v_tax_2 = $cls_tb_order->get_tax_2();
        $v_tax_3 = $cls_tb_order->get_tax_3();
        $v_user_allocate = $cls_tb_order->get_use_allocate();
        $v_cost_kitting = $cls_tb_order->get_cost_kitting();
        $v_user_create = $cls_tb_order->get_user_name();


        $v_company_id = $cls_tb_order->get_company_id();
        $v_order_location_id = $cls_tb_order->get_location_id();
        $v_order_ref = $cls_tb_order->get_order_ref();
        $v_company_code = "";
        $v_total = $cls_tb_company->select_one(array("company_id"=>$v_company_id));
        if($v_total>0) $v_company_code = $cls_tb_company->get_company_code();
    }else{
        redir(URL.'admin/error');
    }

    /* Distribution */
    $arr_distribution = array();
    $arr_where_clause = array('order_id' => (int)$v_order_id);
    $arr_tb_order_items = $cls_tb_order_items->select($arr_where_clause);
    $v_count = $cls_tb_order_items->count($arr_where_clause);
    $v_total_product = 0;
    $v_total_quantity = 0;
    $v_location_alocate = '';
    $arr_material_name = array();
    $arr_material_all = array();
    foreach($arr_tb_order_items as $arr){
        $v_total_product++;
        $v_order_item_id = isset($arr['order_item_id']) ? $arr['order_item_id'] : 0 ;
        $v_product_id = isset($arr['product_id']) ?$arr['product_id'] :0 ;
        $v_product_code = isset($arr['product_code']) ?$arr['product_code'] :0 ;
        $v_product_description = isset($arr['product_description'])?$arr['product_description']:'';
        $v_product_quantity = isset($arr['quantity']) ? $arr['quantity'] : 0;
        $v_width = isset($arr['width']) ? $arr['width']:0 ;
        $v_length = isset($arr['length']) ? $arr['length']:0 ;
        $v_unit = isset($arr['unit']) ? $arr['unit'] : 0;
        $v_package_type = isset($arr['package_type'])?$arr['package_type']:0;
        $v_graphic_file = isset($arr['graphic_file']) ?$arr['graphic_file'] :'' ;
        $v_graphic_id =  isset($arr['graphic_id']) ?$arr['graphic_id'] :0;
        $v_current_price = isset($arr['current_price']) ?$arr['current_price'] : 0 ;
        $v_material_id = isset($arr['material_id']) ?$arr['material_id'] :0 ;
        $v_material_name = isset($arr['material_name']) ?$arr['material_name'] : '';
        $v_material_thickness_value = isset($arr['material_thickness_value']) ?$arr['material_thickness_value'] : 0;
        $v_material_thickness_unit = isset($arr['material_thickness_unit']) ?$arr['material_thickness_unit']:0 ;
        $v_material_color = isset($arr['material_color'])?$arr['material_color']:'' ;
        $v_item_status = isset($arr['status']) ?$arr['status']:0 ;
        $v_item_desc = isset($arr['description']) ?$arr['description']:'' ;
        $arr_allocation = isset($arr['allocation'])?$arr['allocation']:array();
        settype($v_product_quantity,"int");
        $v_total_quantity +=$v_product_quantity;
        $v_summarize_table .='<tr>';
       // $v_summarize_table .='<td ><img src="'.URL.$v_graphic_file.'" /></td>';
        if(!in_array($v_material_name,$arr_material_name)){
            $arr_material_name [] = $v_material_name;
        }
        $arr_material_all  []['all'] = array("name"=>$v_material_name,"des"=>$v_product_description,"quantity"=>$v_product_quantity
                                        ,"color"=>$v_material_color
                                        ,"size"=>$v_width.'&times'.$v_length.' '.$v_unit
                                        ,"thickness"=>$v_material_thickness_value.$v_material_thickness_unit
                                        ,"images"=>'<img style="min-width:100px;max-width:150px; min-height:100px;max-height:150px" src="'.URL.$v_graphic_file.'" />'
                                );

        foreach($arr_allocation as $arr_loc){
            $v_location_id = $arr_loc['location_id'];
            $v_location_number= $cls_tb_location->select_scalar('location_number', array('location_id'=>(int)$v_location_id));
            $v_location_name = $arr_loc['location_name'];
            $v_location_address = $arr_loc['location_address'];
            $v_location_quantity = $arr_loc['location_quantity'];
            $v_location_price = $arr_loc['location_price'];
            $v_location_product_id = $arr_loc['product_id'];
            $v_location_product_name = $arr_loc['product_name'];
            $v_location_product_image = $arr_loc['product_image'];
            $v_location_tracking_number = $arr_loc['tracking_number'];
            $v_location_tracking_company = $arr_loc['tracking_company'];
            $v_location_tracking_url = $arr_loc['tracking_url'];
            $v_date_shipping = $arr_loc['date_shipping'];

            $v_product_infomation = $v_product_description.'<br>('. $v_width . '*'.$v_length .') '.$v_material_thickness_value .' '.$v_material_thickness_unit;
            $v_product_infomation .='<br>'.$v_material_name.' -'. $v_material_color;

            if(!isset($arr_distribution[$v_location_id]))
                $arr_distribution[$v_location_id][0]= array('location_id'=>$v_location_id,'location_name'=>$v_location_name,'location_number'=>$v_location_number);

            $arr_distribution[$v_location_id][1][] = array('product_infomation'=>$v_product_infomation,'quantity'=>$v_location_quantity,'price'=>$v_location_price,'graphic_file'=>$v_graphic_file);
        }
    }
    for($i=0;$i<count($arr_material_name);$i++){
        $v_total_product = 0;
        $v_total_quantity = 0;
        $v_name = $arr_material_name[$i];
        $v_summarize_table .='<h3>'.$v_name.'</h3>';
        $v_summarize_table .='
                <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="5">
                    <tr align="right" valign="top">
                        <th width="150px" align="center">Images</th>
                        <th width="250px" align="center">Product Name</th>
                        <th width="50px" align="center">Quantity</th>
                        <th align="center">Material</th>
                    </tr>';
            for($j=0;$j<count($arr_material_all);$j++){
                $v_temp_name = $arr_material_all[$j]['all']['name'];
                if($v_temp_name == $v_name){
                    $arr_all = $arr_material_all[$j]['all'];
                    $v_size_temp = $arr_all['size'];
                    $v_color_temp = $arr_all['color'];
                    $v_quantity_temp = $arr_all['quantity'];
                    $v_des_temp = $arr_all['des'];
                    $v_thick_temp = $arr_all['thickness'];
                    $v_images = $arr_all['images'];
                    $v_total_product++;
                    $v_total_quantity +=$v_quantity_temp;
                    $v_summarize_table .='<tr>';
                    $v_summarize_table .='<td>'.$v_images.'</td>';
                    $v_summarize_table .='<td>'.$v_des_temp.'</td>';
                    $v_summarize_table .='<td  style="text-align: center">'.$v_quantity_temp.'</td>';
                    $v_summarize_table .='<td>Material color: <b>'. $v_color_temp.'</b><br /> Material size:<b>'.$v_size_temp.'</b><br /> Material thickness:<b>'.$v_thick_temp.'<b/></td>';
                    $v_summarize_table .='</tr>';
                }else{ continue;}
            }
        $v_summarize_table .='<tr>';
        $v_summarize_table .='<td><b>Total Product:</b> '.$v_total_product.'</td>';
        $v_summarize_table .='<td>&nbsp</td>';
        $v_summarize_table .='<td><b>Total:</b> '.$v_total_quantity.'</td>';
        $v_summarize_table .='<td>&nbsp</td>';
        $v_summarize_table .='</tr>';
        $v_summarize_table .='</table>';
    }

    $v_distribution = '';
    $v_total_distribution = 0;
}
?>