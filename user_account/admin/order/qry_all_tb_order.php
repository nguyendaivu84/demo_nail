<?php if(!isset($v_sval)) die();?>
<?php
$v_quick_search = '';
$v_search_po_number = '';
$v_search_order_ref = '';
$v_page = 1;
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
if(isset($_SESSION['ss_tb_order_redirect']) && $_SESSION['ss_tb_order_redirect']==1){
    $v_page = isset($_SESSION['ss_tb_order_page'])?$_SESSION['ss_tb_order_page']:'1';
    settype($v_page,'int');
    if($v_page<1) $v_page = 1;
    if(isset($_SESSION['ss_tb_order_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_order_where_clause']);
    }
    $v_quick_search = isset($_SESSION['ss_tb_order_quick_search'])?$_SESSION['ss_tb_order_quick_search']:'';
}
$v_search_po_number = $v_quick_search;
$v_search_order_ref = $v_quick_search;
//Add code here if required
$arr_order_status = array();
$arr_exclude_status = array();
if(!is_admin()){
    //Not amin, exclude some status
    $arr_exclude_status = array('partly_shipped', 'fully_shipped', 'void');
}
$arr_option = $cls_settings->select_scalar('option', array('setting_name'=>'order_status','status'=>0));
for($i=0;$i<count($arr_option);$i++){
    $v_status_id = $arr_option[$i]['id'];
    $v_status_name = $arr_option[$i]['name'];
    $v_status_key = $arr_option[$i]['key'];
    if($v_status_id>20 && $arr_option[$i]['status']==0){
        if(!in_array($v_status_key, $arr_exclude_status))
            $arr_order_status[] = array('status_key'=>$v_status_key, 'status_name'=>$v_status_name);
    }
}
?>