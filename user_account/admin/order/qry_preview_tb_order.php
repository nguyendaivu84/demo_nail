<?php if(!isset($v_sval)) die();
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);
$v_error_message = '';
$v_summarize_table = '';
$v_mongo_id = NULL;
$v_location_id = 0;
$v_raw_id = '';
$v_anvy_id = '';
$v_po_number = '';
$v_order_type = 0;
$v_shipping_contact = '';
$v_total_order_amount = 0;
$v_total_discount = 0;
$v_billing_contact = '';
$v_net_order_amount = 0;
$v_gross_order_amount = 0;
$v_job_description = '';
$v_sale_rep = '';
$v_date_created = date('Y-M-d', time());
$v_date_required = '';
$v_date_modified = '';
$v_date_approved = '';
$v_term = 0;
$v_delivery_method = '';
$v_source = 0;
$v_status = 0;
$v_dispatch = 0;
$v_tax_1 = 0;
$v_tax_2 = 0;
$v_tax_3 = 0;
$v_order_ref = '';
$v_user_create = '';
$v_user_approved = '';
$v_user_modified = '';
$v_order_id = isset($_GET['id'])?$_GET['id']:'';
$v_dsp_tb_order_items = '';
$v_dsp_order_log = '';
$v_packaging_cost  = 0;
$v_cost_kitting = 0;
$v_total_amount_cost = 0;
$v_full_user_name = '';
$arr_list_entry = array();
if($v_order_id!=''){
    $v_row = $cls_tb_order->select_one(array('_id' => new MongoId($v_order_id)));
    $arr_location_allocation = array();
    if(is_array($v_row)){
        $arr_row = $v_row[$v_order_id];
        $v_order_id = isset($arr_row['code']) ? $arr_row['code'] : '';
        $v_raw_id = 0;
        $v_anvy_id = 0;
        $v_po_number = isset($arr_row['code']) ? $arr_row['code'] : '';
        $v_name = $arr_row['name'];
        $v_email = $arr_row['email'];
        $v_phone = $arr_row['phone'];
        $arr_shipping_address = $arr_row['shipping_address'][0];
        $v_address_last_name = isset($arr_shipping_address['last_name']) ? $arr_shipping_address['last_name'] : '';
        $v_address_first_name = isset($arr_shipping_address['first_name']) ? $arr_shipping_address['first_name'] : '';
        $v_address_address_city = isset($arr_shipping_address['address_city']) ? $arr_shipping_address['address_city'] : '';
        $v_address_address_province = isset($arr_shipping_address['address_province']) ? $arr_shipping_address['address_province'] : '';
        $v_address_postal_code = isset($arr_shipping_address['postal_code']) ? $arr_shipping_address['postal_code'] : '';
        $v_address_address_line_1 = isset($arr_shipping_address['address_line_1']) ? $arr_shipping_address['address_line_1'] : '';
        $v_address_phone_1 = isset($arr_shipping_address['phone_1']) ? $arr_shipping_address['phone_1'] : '';
        $v_address_phone_2 = isset($arr_shipping_address['phone_2']) ? $arr_shipping_address['phone_2'] : '';
        $v_address_phone_3 = isset($arr_shipping_address['phone_3']) ? $arr_shipping_address['phone_3'] : '';
        $v_address_phone_ext = isset($arr_shipping_address['phone_ext']) ? $arr_shipping_address['phone_ext'] : '';
        $v_address_shipping = $v_address_last_name." ".$v_address_first_name." <br/> ".$v_address_address_city." ".$v_address_address_province." ".$v_address_postal_code." ".$v_address_address_line_1;
        $v_address_shipping .= "<br />".$v_address_phone_1.".".$v_address_phone_2.".".$v_address_phone_3.$v_address_phone_ext;
        $v_order_type = '';
        $v_billing_address = '';
        $arr_billing_address = isset($arr_row['billing_address']) ? $arr_row['billing_address'] : array();
        if(isset($arr_billing_address['billing_address_same'])) $v_billing_address = $v_address_shipping;
        else {
            $v_billing_address = '';
            if(!empty($arr_billing_address)){
                $v_billing_address = $arr_billing_address['billing_country']." ".$arr_billing_address['billing_city']." <br/> ".$arr_billing_address['billing_state']." ".$arr_billing_address['billing_zip']." ".$arr_billing_address['billing_address_1'];
                $v_billing_address .= "<br />".$arr_billing_address['billing_phone_1'].".".$arr_billing_address['billing_phone_2'].".".$arr_billing_address['billing_phone_3'].$arr_billing_address['billing_phone_ext'];
            }
        }
        $v_shipping_contact = '';
        //$v_total_order_amount = $cls_tb_order->get_total_order_amount();
        $v_total_order_amount = isset($arr_row['sum_amount']) ? $arr_row['sum_amount'] : 0;
        $v_total_discount = 0;
        $v_billing_contact = '';// $cls_tb_order->get_billing_contact();
        $v_net_order_amount = 0;
        $v_gross_order_amount = 0;//
        $v_job_description = '';//$cls_tb_order->get_description();
        $v_sale_rep = 0;//$cls_tb_order->get_sale_rep();
        $v_date_created = @date("d M,Y",$arr_row['salesorder_date']->sec);
        $v_date_modified = '';

        $v_term = 0;
        $v_delivery_method = 0;
        $v_source = 0;
        $v_order_status = $arr_row['status'];
        $v_user_approved = '';
        $v_user_modified = '';
        $v_dispatch = '';
        $v_tax_1 = 0;
        $v_tax_2 = 0;
        $v_tax_3 = 0;
        //$v_user_allocate = $cls_tb_order->get_use_allocate();
        $v_cost_kitting = 0;
        $v_user_create = 0;
        $arr_list_entry = $arr_row['products'];
        $v_company_id = $arr_row['company_id'];
        $v_company = $arr_row['company_name'];
        $v_order_location_id = '';
        $v_order_ref = '';
        $v_company_code = "";

    /* Distribution */
    $arr_distribution = array();
    $arr_where_clause = array('order_id' => (int)$v_order_id);
    $arr_tb_order_items = $cls_tb_order_items->select($arr_where_clause);
    $v_total_product = 0;
    $v_total_quantity = 0;
    $v_distribution = '';
    $v_distribution .= '<div style="padding: 10px;">';
    $v_distribution .= '<table  width="100%" border="1" class="list_table"  cellpadding="3" cellspacing="0">
                        <tr>
                            <th width="30%">Images</th>
                            <th width="40%">Descriptions</th>
                            <th width="30%">#</th>
                        </tr>';
    for($i=0;$i<count($arr_list_entry) && is_array($arr_list_entry);$i++){
        $arr_temp = $arr_list_entry[$i];
        $v_infomation = "Name:".$arr_temp['sku'];
        $v_quantity = $arr_list_entry[$i]["quantity"];
        $v_price = $arr_list_entry[$i]["unit_price"];
        $v_select_one = $cls_tb_product->select_one(array("_id"=>new MongoId($arr_list_entry[$i]["products_id"])));
        if(!$v_select_one) continue;
        $v_distribution .= '<tr>';
        $v_products_upload = $cls_tb_product->get_products_upload();
        $v_graphic_file = URL . $v_products_upload;
//        $v_distribution .="<td>&npsp</td>";
//        $v_distribution .="<td>&npsp</td>";
//        $v_distribution .="<td>&npsp</td>";
        if(!checkRemoteFile($v_graphic_file)) $v_graphic_file = JT_URL . $v_products_upload;
            $v_distribution .='<td width="30%" valign="top" align="center"> <img  style="max-width:150px;min-width:75px;min-height:75px;max-height:125px;" src="'.$v_graphic_file.'"> &nbsp</td>';
            $v_distribution .='<td width="40%" valign="top">'.$v_infomation.'</td>';
            $v_distribution .='<td width="30%" valign="top">Quantity: '.$v_quantity.'<br />'.' Price: '.format_currency($v_price)."</td>";
        $v_distribution .= '</tr>';
    }
    $v_distribution .='</table>';
    $v_distribution .='</div>';
}
}
?>