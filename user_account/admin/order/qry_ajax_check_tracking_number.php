<?php
if(!isset($v_sval)) die();
?>
<?php
$v_tracking_number = isset($_POST['txt_tracking_number'])?$_POST['txt_tracking_number']:'0';
$v_tracking_url = isset($_POST['txt_tracking_url'])?$_POST['txt_tracking_url']:'0';
$v_tracking_id = isset($_POST['txt_tracking_id'])?$_POST['txt_tracking_id']:'0';
//settype($v_tracking_number, 'int');
settype($v_tracking_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK', 'show_message'=>0);
if($v_edit_right || $v_is_admin){
    $cls_tb_tracking = new cls_tb_tracking($db);
    $v_shipper = $cls_tb_tracking->select_scalar("tracking_name",array("tracking_id"=>$v_tracking_id));

    $cls_tb_shipping = new cls_tb_shipping($db);
    $v_count = $cls_tb_shipping->count(array("shipper"=>$v_shipper,"tracking_number"=>$v_tracking_number));
    if($v_count>=1){
        $arr_return['error'] = 2;
        $arr_return['message']= 'Tracking number and tracking url already exsit';
    }
}else{
    $arr_return['error'] = 1;
    $arr_return['message']= 'You have no permission!';
}
echo json_encode($arr_return);
?>