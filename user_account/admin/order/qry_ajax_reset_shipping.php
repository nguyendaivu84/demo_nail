<?php
if(!isset($v_sval)) die();
?>
<?php
$v_allocation_id = isset($_POST['txt_allocation_id'])?$_POST['txt_allocation_id']:'0';
settype($v_allocation_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK');
if($v_shipping_edit_right || $v_is_admin){
    if($v_allocation_id>0){
        $v_row = $cls_tb_allocation->select_one(array('allocation_id'=>$v_allocation_id));
        if($v_row==1){
            $v_parent_allocation_id = $cls_tb_allocation->get_parent_allocation();
            $v_plus_quantity = $cls_tb_allocation->get_quantity();

            $v_order_item_id = $cls_tb_allocation->get_order_items_id();
            $v_location_id = $cls_tb_allocation->get_location_id();
            $v_parent_allocation = $cls_tb_allocation->get_parent_allocation();
            $v_shipping_id = $cls_tb_allocation->get_shipping_id();
            $v_reset_shipping = true;
            if($v_shipping_id>0){
                $v_row = $cls_tb_shipping->select_one(array('shipping_id'=>$v_shipping_id));
                if($v_row==1){
                    $arr_shipping_detail = $cls_tb_shipping->get_shipping_detail();
                    $arr_temp = array();
                    for($i=0; $i<count($arr_shipping_detail); $i++){
                        if($arr_shipping_detail[$i]['allocation_id']!=$v_allocation_id) $arr_temp[] = $arr_shipping_detail[$i];
                    }
                    $v_reset_shipping = $cls_tb_shipping->update_field('shipping_detail', $arr_temp, array('allocation_id'=>$v_shipping_id));
                }else{
                    $v_reset_shipping = false;
                }
            }
            if($v_reset_shipping){
                $arr_fields = array('shipper', 'tracking_number', 'tracking_url', 'date_shipping', 'shipping_id', 'shipped_quantity');
                $arr_values = array('', '', '', NULL, 0, 0);
                $cls_tb_allocation->update_fields($arr_fields, $arr_values, array('allocation_id'=>$v_allocation_id));

                $v_order_id = isset($_POST['txt_order_id']) ? $_POST['txt_order_id'] : 0;
                $v_dispatch_item = isset($_POST['dispatch_length']) ? $_POST['dispatch_length'] : 0;
                settype($v_dispatch_item,"int");
                $v_dispatch_ship_item = isset($_POST['txt_dispatch_ship_length']) ? $_POST['txt_dispatch_ship_length'] : 0;
                settype($v_dispatch_ship_item,"int");
                $v_result = $v_dispatch_item - $v_dispatch_ship_item-1;
                if( $v_result <= 0){
                    $v_status = 30;
                }
                else $v_status = 50;
                //$v_status = (int)$v_dispatch_item - (int)$v_dispatch_ship_item - 1 ==0 ? 30 : 50;
                /**/
                $cls_tb_order->update_field("status", (int)$v_status, array('order_id'=>(int)$v_order_id));
                $cls_tb_shipping->delete(array("shipping_id"=>(int)$v_shipping_id));
                //$v_child_quantity = $cls_tb_allocation->select_scalar("quantity",array("parent_allocation"=>$v_allocation_id,"ship_complete"=>0));

               if($v_parent_allocation==0){
                   $arr_where = array("parent_allocation"=>$v_allocation_id,"ship_complete"=>0);
                   $v_child_quantity = $cls_tb_allocation->select_scalar("quantity",$arr_where);
                   $v_delete_allocation_id = $cls_tb_allocation->select_scalar("allocation_id",$arr_where);
                   //$v_delete_allocation_id = $cls_tb_allocation->select_scalar("allocation_id",$arr_where);
                   $cls_tb_allocation->update_field("ship_complete",0,array("allocation_id"=>$v_allocation_id));
                   settype($v_child_quantity,"int");
                   settype($v_allocation_id,"int");
                   settype($v_delete_allocation_id,"int");
                   $v_total_quantity = $v_plus_quantity + $v_child_quantity;
                   $cls_tb_allocation->update_field("quantity",$v_total_quantity,array("allocation_id"=>$v_allocation_id));
                   $cls_tb_allocation = new cls_tb_allocation($db);
                   $cls_tb_allocation->delete(array("allocation_id"=>$v_delete_allocation_id));
               }else{
                   $arr_where = array("order_id"=>(int)$v_order_id,"parent_allocation"=>$v_parent_allocation,"ship_complete"=>0);
                   //die(var_dump($arr_where));
                   $v_child_quantity = $cls_tb_allocation->select_scalar("quantity",$arr_where);
                   //$v_delete_allocation_id = $cls_tb_allocation->select_scalar("allocation_id",array("parent_allocation"=>$v_allocation_id,"ship_complete"=>0));
                   $v_delete_allocation_id = $cls_tb_allocation->select_scalar("allocation_id",$arr_where);
                   //  die("$v_delete_allocation_id");
                   $cls_tb_allocation->update_field("ship_complete",0,array("allocation_id"=>$v_allocation_id));
                   settype($v_child_quantity,"int");
                   settype($v_allocation_id,"int");
                   settype($v_delete_allocation_id,"int");
                   $v_total_quantity = $v_plus_quantity + $v_child_quantity;
                   $cls_tb_allocation->update_field("quantity",$v_total_quantity,array("allocation_id"=>$v_allocation_id));
                   $cls_tb_allocation = new cls_tb_allocation($db);
                   $cls_tb_allocation->delete(array("allocation_id"=>$v_delete_allocation_id));
               }
            }else{
                $arr_return['error'] = 4;
                $arr_return['message'] = 'Can not reset shipping!';
            }
        }else{
            $arr_return['error'] = 3;
            $arr_return['message'] = 'Not found!';
        }
    }else{
        $arr_return['error'] = 2;
        $arr_return['message'] = 'Lost data!';
    }
}else{
    $arr_return['error'] = 1;
    $arr_return['message'] = 'You have no permission!';
}

echo json_encode($arr_return);
?>