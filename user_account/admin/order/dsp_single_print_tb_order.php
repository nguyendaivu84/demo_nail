<?php if(!isset($v_sval)) die();?>
    <div id="header">
        <a href="<?php echo URL;?>"><img src="<?php echo URL.'images/logo.png';?>" alt="Anvy Inc" style="border:none" /></a>
    </div>
<?php echo $dsp_order_template->output(); ?>
<?php
/*
 $v_count = 0;
foreach($arr_tb_order_items as $arr){
    $v_order_item_id = isset($arr['order_item_id'])?$arr['order_item_id']:0;
    $v_order_id = isset($arr['order_id'])?$arr['order_id']:'';
    $v_product_id = isset($arr['product_id'])?$arr['product_id']:'';
    $v_product_code = isset($arr['product_code'])?$arr['product_code']:'';
    $v_quantity = isset($arr['quantity'])?$arr['quantity']:0;
    $v_description = isset($arr['description'])?$arr['description']:'';
    $v_customization_information = isset($arr['customization_information'])?$arr['customization_information']:'';
    $v_width = isset($arr['width'])?$arr['width']:0;
    $v_height = isset($arr['length'])?$arr['length']:0;
    $v_graphic_file = isset($arr['graphic_file'])?$arr['graphic_file']:'';
    $v_original_price = isset($arr['original_price'])?$arr['original_price']:0;
    $v_discount_price = isset($arr['discount_price'])?$arr['discount_price']:0;
    $v_current_price = isset($arr['current_price'])?$arr['current_price']:0;
    $v_unit_price = isset($arr['unit_price'])?$arr['unit_price']:0;
    $v_status = isset($arr['status'])?$arr['status']:0;
    $v_discount_type = isset($arr['discount_type'])?$arr['discount_type']:0;
    $v_discount_per_unit = isset($arr['discount_per_unit'])?$arr['discount_per_unit']:0;
    $v_net_price = isset($arr['net_price'])?$arr['net_price']:0;
    $v_extended_amount = isset($arr['extended_amount'])?$arr['extended_amount']:0;
    $v_unit = isset($arr['unit'])?$arr['unit']:'';
    $v_material_id = isset($arr['material_id'])?$arr['material_id']:0;
    $v_material_name = isset($arr['material_name'])?$arr['material_name']:'';
    $v_material_thickness_value = isset($arr['material_thickness_value'])?$arr['material_thickness_value']:'';
    $v_material_thickness_unit = isset($arr['material_thickness_unit'])?$arr['material_thickness_unit']:'';
    $v_material_color = isset($arr['material_color'])?$arr['material_color']:'';
    $v_total_price = isset($arr['total_price'])?$arr['total_price']:0;
    $arr_allocation = isset($arr['allocation'])?$arr['allocation']:array();

    $v_mongo_id = $arr['_id'];
    $v_count++;
    ?>
    <p style="page-break-before: always">
    <h3>Details Order :  <?php echo $v_job_description .' # PO Number : ' . $v_po_number; ?> </h3>
    <h3>Order items ID #: <?php echo $v_count;  ?> </h3>
    <p class="break"/>
    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
        <tr  valign="top">
            <td width="200px">Product Code </td>
            <td align="left" colspan="3" ><?php echo $v_product_code ;?></td>
            <td rowspan="5" align="center">
                <?php
                $v_image = '';
                if(trim($v_graphic_file)!='') $v_image = '<img src="'.URL. $v_graphic_file .'" />';
                echo $v_image;
                ?>
            </td>
        </tr>
        <tr>
            <td >Width </td>
            <td ><b><?php echo $v_width. ' ' .$v_unit;?></b> </td>
            <td >Height </td>
            <td ><b><?php echo $v_height. ' ' .$v_unit;?></b> </td>

        </tr>
        <tr>
            <td> Thickness </td>
            <td colspan="3" ><b><?php echo $v_material_thickness_value. ' ' .$v_material_thickness_unit;?></b> </td>
        </tr>
        <tr>
            <td>Color </td>
            <td ><?php echo $v_material_color;?> </td>
            <?php
            $v_count_color = $cls_tb_color->select_one(array('color_name'=>$v_material_color));
            $v_bgcolor = "";
            if($v_count_color > 0)
                $v_bgcolor = $cls_tb_color->get_color_code_hex();
            ?>
            <td colspan="2"><span style="background-color:<?php echo $v_bgcolor; ?>">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span></td>
        </tr>
        <tr>
            <td>Unit Price: </td>
            <td ><b><?php echo format_currency((float)$v_total_price/$v_quantity);?> </b></td>
            <td>Quantity : <b><?php echo number_format($v_quantity) ;?> </b> </td>
            <td>Total Price: <b><?php echo format_currency($v_total_price+$v_packaging_cost) ;?> </b></td>
        </tr>
    </table>

    <?php
    $v_total_allocation = sizeof($arr_allocation);

    $v_count = 0;
    for($j=0;$j<$v_total_allocation;$j++)
    {
        $v_location_id = isset($arr_allocation[$j]['location_id'])?$arr_allocation[$j]['location_id']:'';
        $v_location_name = isset($arr_allocation[$j]['location_name'])?$arr_allocation[$j]['location_name']:'';
        $v_location_address = isset($arr_allocation[$j]['location_address'])?$arr_allocation[$j]['location_address']:'';
        $v_location_quantity = isset($arr_allocation[$j]['location_quantity'])?$arr_allocation[$j]['location_quantity']:0;
        $v_location_price = isset($arr_allocation[$j]['location_price'])?$arr_allocation[$j]['location_price']:0;
        $v_tracking_url = isset($arr_allocation[$j]['tracking_url'])?$arr_allocation[$j]['tracking_url']:'';
        $v_tracking_number = isset($arr_allocation[$j]['tracking_number'])?$arr_allocation[$j]['tracking_number']:'';
        $v_tracking_company = isset($arr_allocation[$j]['tracking_company'])?$arr_allocation[$j]['tracking_company']:'';
        $v_date_shipping = isset($arr_allocation[$j]['date_shipping'])? fdate(date('d-M-Y',$arr_allocation[$j]['date_shipping']->sec)) :'';
        $v_allocation_id = isset($arr_allocation[$j]['allocation_id'])?$arr_allocation[$j]['allocation_id']:0;
        settype($v_allocation_id, 'int');
        $v_count++;

        ?>
        <h3>Dispath ID #: <?php echo $v_count;?> </h3>
        <p class="break"/>
        <table align="right" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
            <tr align="right" valign="top">
                <td width="200px" >Location Name:</td>
                <td align="left" colspan="2"><b> <?php echo $v_location_name; ?> </b></td>
            </tr>
            <tr align="right" valign="top">
                <td>Location Number: </td>
                <td align="left" colspan="2">
                    <?php echo $cls_tb_location->select_scalar("location_number",array("location_id"=>(int)$v_location_id)); ?>
                </td>
            </tr>
            <tr align="right" valign="top">
                <td text-align="right">Location Address: </td>
                <td align="left" colspan="2">
                    <?php echo $v_location_address; ?>
                </td>
            </tr>
            <?php
            if($v_allocation_id<=0){
            ?>
            <tr align="right" valign="top">
                <td>Quantity: </td>
                <td align="left" colspan="2">
                    <b> <?php echo $v_location_quantity; ?> </b>
                </td>
            </tr>
            <tr align="right" valign="top">
                <td>Shipping Company...</td>
                <td align="left" colspan="2"><?php echo $v_tracking_company; ?> </td>
            </tr>
            <tr align="right" valign="top">
                <td>Tracking Number...</td>
                <td align="left"><b></b><?php echo $v_tracking_number; ?> </b></td>
                <td align="left">Tracking URL: <?php echo $v_tracking_url; ?> </td>
            </tr>
            <tr align="right" valign="top">
                <td>Shipping Date</td>
                <td align="left" colspan="2"><?php echo $v_date_shipping; ?> </td>
            </tr>
            <?php
            }else{
                $arr_where_clause = array('$or'=>array(array('allocation_id'=>$v_allocation_id),array('parent_allocation'=>$v_allocation_id)));
                $arr_tmp_allocation = $cls_tb_allocation->select($arr_where_clause, array('parent_allocation'=>1));
                foreach($arr_tmp_allocation as $arr_tmp){
                    $v_ship_quantity = $arr_tmp['quantity'];
                    $v_ship_date = $arr_tmp['date_shipping'];
                    if(is_object($v_ship_date)) $v_ship_date = date('d-M-Y', $v_ship_date->sec);
            ?>
                    <tr align="right" valign="top">
                        <td>Quantity: </td>
                        <td align="left" colspan="2">
                            <b> <?php echo $v_ship_quantity; ?> </b>
                        </td>
                    </tr>
                    <tr align="right" valign="top">
                        <td >Tracking Company...</td>
                        <td align="left" colspan="2"><?php echo $arr_tmp['shipper']; ?> </td>
                    </tr>
                    <tr align="right" valign="top">
                        <td>Tracking Number...</td>
                        <td align="left"><b></b><?php echo $arr_tmp['tracking_number']; ?> </b></td>
                        <td>Tracking URL: <?php echo $arr_tmp['tracking_url']; ?> </td>
                    </tr>
                    <tr align="right" valign="top">
                        <td>Date Shipping...</td>
                        <td align="left" colspan="2"><?php echo $v_ship_date; ?> </td>
                    </tr>
            <?php
                }
            }
            ?>
        </table>
    <?php } ?>
<?php }
 */
echo $v_content_all;
?>
    <h3>DISPATCH DETAILS</h3>
<?php //echo $v_dispatch_content;?>

    <!--h4>distribution</h4-->
<?php echo $v_distribution; ?>
<?php //var_dump($arr_total_distribution); ?>