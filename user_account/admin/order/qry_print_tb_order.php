<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_tb_order_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_order_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}

if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_order_sort'])){
	$v_sort = $_SESSION['ss_tb_order_sort'];
	$arr_sort = unserialize($v_sort);
}

add_class("cls_tb_company");
$cls_tb_company = new cls_tb_company($db);
$arr_kitting_cost = array();
$arr_select = $cls_tb_company->select();
foreach($arr_select as $arr_company){
    $arr_kitting_cost[$arr_company['company_id']] = $arr_company['default_kitting_cost'];
}


if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_order = $cls_tb_order->select($arr_where_clause, $arr_sort);
$v_dsp_tb_order = '<table class="list_table"  cellpadding="3" cellspacing="1" border="1" align="center">';

$v_dsp_tb_order .= '<tr align="center" valign="middle">';
$v_dsp_tb_order .= '<th>Ord</th>';
$v_dsp_tb_order .= '<th>Order Id</th>';
$v_dsp_tb_order .= '<th>Company Id</th>';
$v_dsp_tb_order .= '<th>Location Id</th>';
$v_dsp_tb_order .= '<th>Po Number</th>';
$v_dsp_tb_order .= '<th>Shipping Contact</th>';
$v_dsp_tb_order .= '<th>Total Order Amount</th>';
$v_dsp_tb_order .= '<th>Description</th>';
$v_dsp_tb_order .= '<th>Order Ref</th>';
$v_dsp_tb_order .= '<th>Sale Rep</th>';
$v_dsp_tb_order .= '<th>Date Created</th>';
$v_dsp_tb_order .= '<th>Date Required</th>';
$v_dsp_tb_order .= '<th>Date Approved</th>';

$v_dsp_tb_order .= '<th>Status</th>';
$v_dsp_tb_order .= '<th>Require Approved</th>';
$v_dsp_tb_order .= '<th>User Approved</th>';
$v_dsp_tb_order .= '<th>User Modified</th>';
$v_dsp_tb_order .= '<th>User Name</th>';

$v_dsp_tb_order .= '</tr>';
$v_count = 1;
$arr_temp_company = array();
$arr_temp_location = array();
$arr_total_location = array();
$arr_tracking = array();
$arr_status_order = array();
$arr_shipping_contact_id = array();
$arr_shipping_contact_name = array();
foreach($arr_tb_order as $arr){
	$v_dsp_tb_order .= '<tr align="left" valign="middle">';
	$v_dsp_tb_order .= '<td align="right">'.($v_count++).'</td>';
	$v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
	$v_raw_id = isset($arr['raw_id'])?$arr['raw_id']:'';
	$v_anvy_id = isset($arr['anvy_id'])?$arr['anvy_id']:'';
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_po_number = isset($arr['po_number'])?$arr['po_number']:'';
	$v_order_type = isset($arr['order_type'])?$arr['order_type']:0;
	$v_shipping_contact = isset($arr['shipping_contact'])?$arr['shipping_contact']:'';

    settype($v_shipping_contact,"int");
    if(!in_array($v_shipping_contact,$arr_shipping_contact_id)){
        $arr_shipping_contact_id[] = $v_shipping_contact;
        $arr_shipping_contact_name[$v_shipping_contact] = $cls_tb_contact->get_full_name_contact($v_shipping_contact);
    }
	$v_total_order_amount = isset($arr['total_order_amount'])?$arr['total_order_amount']:0;
    add_class("cls_tb_order_items");
    $cls_tb_order_item = new cls_tb_order_items($db);
    $arr_select_allocation = $cls_tb_order_item->select(array("order_id"=>(int)$arr['order_id']));
    $arr_total_location = array();
    foreach($arr_select_allocation as $arr_order_item){
        $arr_allo_temp = $arr_order_item['allocation'];
       // var_dump($arr_allo_temp);
        //die(var_dump($arr_allo_temp));
        for($i_location = 0; $i_location < count($arr_allo_temp) ; $i_location ++){
            if(!in_array($arr_allo_temp[$i_location]['location_id'],$arr_total_location)) $arr_total_location [] = $arr_allo_temp[$i_location]['location_id'];
        }
    }
    $v_total_order_amount +=$arr_kitting_cost[$v_company_id] * count($arr_total_location);

	$v_total_discount = isset($arr['total_discount'])?$arr['total_discount']:0;
	$v_billing_contact = isset($arr['billing_contact'])?$arr['billing_contact']:'';
	$v_net_order_amount = isset($arr['net_order_amount'])?$arr['net_order_amount']:0;
	$v_gross_order_amount = isset($arr['gross_order_amount'])?$arr['gross_order_amount']:0;
	$v_job_description = isset($arr['job_description'])?$arr['job_description']:'';
	$v_description = isset($arr['description'])?$arr['description']:'';
    $v_description = preg_replace("/&#?[a-z0-9]{2,8};/i","",$v_description);
	$v_notes = isset($arr['notes'])?$arr['notes']:'';
	$v_order_ref = isset($arr['order_ref'])?$arr['order_ref']:'';
	$v_sale_rep = isset($arr['sale_rep'])?$arr['sale_rep']:'';
	$v_date_created = isset($arr['date_created'])? $arr['date_created']:'';
    if($v_date_created!='') $v_date_created = date('d-M-Y', $arr['date_created']->sec);
    else $v_date_created = 'N/A';

	$v_date_required = isset($arr['date_required'])?$arr['date_required']:'';
    if($v_date_required!='') $v_date_required = date('d-M-Y', $arr['date_required']->sec );
    else $v_date_required = 'N/A';

    $v_date_approved = isset($arr['date_approved'])?$arr['date_approved']:'';
    if($v_date_approved!='') $v_date_approved = date('d-M-Y', strtotime($arr['date_approved']));
    else $v_date_approved = 'N/A';

	$v_term = isset($arr['term'])?$arr['term']:0;
	$v_delivery_method = isset($arr['delivery_method'])?$arr['delivery_method']:0;
	$v_source = isset($arr['source'])?$arr['source']:0;
	$v_status = isset($arr['status'])?$arr['status']:0;
	$v_require_approved = isset($arr['require_approved'])?$arr['require_approved']:'0';
    settype($v_require_approved,"int");
    $v_require_approved = $v_require_approved==0?'No':'Yes';
	$v_user_approved = isset($arr['user_approved'])?$arr['user_approved']:'';
	$v_user_modified = isset($arr['user_modified'])?$arr['user_modified']:'';
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';

    if(!isset($arr_temp_company[$v_company_id]))
        $arr_temp_company[$v_company_id] = $cls_tb_company->select_scalar('company_name',array('company_id'=>(int)$v_company_id ));
    if(!isset($arr_temp_location[$v_location_id]))
        $arr_temp_location[$v_location_id] = $cls_tb_location->select_scalar('location_name',array('location_id'=>(int)$v_location_id ));
    if(!isset($arr_tracking[$v_shipping_contact]))
        $arr_tracking[$v_shipping_contact] = $cls_tb_tracking->select_scalar('tracking_name',array('tracking_id'=>(int)$v_shipping_contact ));
    if(!isset($arr_status_order[$v_status]))
        $arr_status_order[$v_status] = $cls_settings->get_option_name_by_id('order_status',$v_status);
    else{
        if($v_status<0)
            $arr_status_order[$v_status] = "Deleted";
    }

	$v_dsp_tb_order .= '<td>'.$v_order_id.'</td>';
    $v_dsp_tb_order .= '<td>'.$arr_temp_company[$v_company_id].'</td>';
	$v_dsp_tb_order .= '<td>'.$arr_temp_location[$v_location_id].'</td>';

	$v_dsp_tb_order .= '<td>'.$v_po_number.'</td>';

	$v_dsp_tb_order .= '<td>'.$arr_shipping_contact_name[$v_shipping_contact].'</td>';
	$v_dsp_tb_order .= '<td>'.format_currency($v_total_order_amount) .'</td>';
	$v_dsp_tb_order .= '<td>'.$v_description.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_order_ref.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_sale_rep.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_date_created.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_date_required.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_date_approved.'</td>';


	$v_dsp_tb_order .= '<td>'.$arr_status_order[$v_status].'</td>';
	$v_dsp_tb_order .= '<td>'.$v_require_approved.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_user_approved.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_user_modified.'</td>';
	$v_dsp_tb_order .= '<td>'.$v_user_name.'</td>';

	$v_dsp_tb_order .= '</tr>';
}
$v_dsp_tb_order .= '</table>';
?>