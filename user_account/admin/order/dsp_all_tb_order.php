<?php if(!isset($v_sval)) die();?>
<style>
    .k-tooltip{
        top: -15px !important;
    }
    .k-animation-container{
        margin-bottom: 10px !important;
        margin-top: 0 !important;
    }
</style>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Order</h3>
                    </div>
                    <div id="div_quick">
                    <div id="div_quick_search">
                    <form method="post" id="frm_quick_search">

                    <span class="k-textbox k-space-left" id="txt_quick_search">
                    <input type="text" name="txt_quick_search" placeholder="Search by PO Number Or Order Ref" value="<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>" />
                    <a id="a_quick_search" style="cursor: pointer" class="k-icon k-i-search"></a>
                    <script type="text/javascript">
                        $(document).ready(function(e){
                            $('a#a_quick_search').click(function(e){
                                $('form#frm_quick_search').submit();
                            })
                        });
                    </script>
                    </span>
                    </form>
                    </div>
                    <div id="div_select">
                    </div>
                    </div>


                    <div id="grid"></div>
				<script type="text/javascript">
					var window_search;
                    $(document).ready(function() {
                        var grid = $("#grid").kendoGrid({
                            dataSource: {
                                pageSize: 50,
                                page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
                                serverPaging: true,
                                serverSorting: true,
                                serverFiltering: true,
                                transport: {
                                    read: {
                                        url: "<?php echo URL.$v_admin_key;?>/json/",
                                        type: "POST",
                                        data: {txt_session_id:"<?php echo session_id();?>",txt_json_type:'json_order', txt_quick_search:'<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>', txt_search_po_number:'<?php echo isset($v_search_po_number)?htmlspecialchars($v_search_po_number):'';?>', txt_search_order_ref:'<?php echo isset($v_search_order_ref)?htmlspecialchars($v_search_order_ref):'';?>'}
                                    }
                                },
                                schema: {
                                    data: "tb_order"
                                    ,total: function(data){
                                        return data.total_rows;
                                    }
                                },
                                type: "json"
                            },
                            pageSize: 50,
                            height: 430,
                            scrollable: true,
                            sortable: true,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Status is",
                                        neq: "Status is not"
                                    }
                                }
                            },
                            //selectable: "single",
                            pageable: {
                                input: true,
                                refresh: true,
                                pageSizes: [10, 20, 30, 40, 50],
                                numeric: false
                            },
                        dataBound: on_data_bound,
						columns: [
                            {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false,filterable:false},
                            {field: "ref_no", title: "Ref no", type:"string", width:"30px", sortable: true,filterable:false },
                            {field: "user_name", title: "Contact", type:"string", width:"50px", sortable: true,filterable:false},
                            {field: "phone", title: "Phone", type:"string", width:"50px", sortable: true,filterable:false},
                            {field: "email", title: "Email", type:"string", width:"50px", sortable: true,filterable:false},
                            {field: "order_date", title: "Order Date", type:"string", width:"50px", sortable: true,filterable:false},
                            {field: "order_amount", title: "Order amount", type:"string", width:"50px", sortable: true,filterable:false},
                            <?php if($v_edit_right || $v_is_admin){?>
                            //{field: "status_name", title: "Order Status", type:"string", width:"60px",filterable: {ui: filter_order_status}, sortable: false,template:'<span id="sp_save" style="display:none"><span id="sp_sel_status"><select data-status="#= status#" id="sel_status" data-order="#= order_id#"></select></span><span id="sp_cancel">&nbsp;<img id="img_save_#= order_id#" data-order="#= order_id#" src="<?php echo URL;?>images/icons/accept.png" style="cursor: pointer" onclick="save_status(this)" />&nbsp;<img id="img_cancel_#= order_id#" data-order="#= order_id#" src="<?php echo URL;?>images/icons/cancel.png" style="cursor: pointer" onclick="cancel_status(this)" /></span></span><span id="sp_status_title">#= status_name #</span><span id="sp_edit"#= <?php echo $v_is_admin?'status_name!="Submitted"':'status_name!="Submitted';?>?" style=\'display:none\'":"" #>&nbsp;&nbsp;<img id="img_edit_#= order_id#" data-order="#= order_id#" src="<?php echo URL;?>images/icons/tab_edit.png" style="cursor: pointer;" onclick="edit_status(this)" /></span>'},
                            {field: "status_name", title: "Order Status", type:"string", width:"60px",filterable: {ui: filter_order_status}, sortable: false,template:'<span id="sp_save" style="display:none"><span id="sp_sel_status"><select data-status="#= status#" id="sel_status" data-order="#= order_id#"></select></span><span id="sp_cancel">&nbsp;<img id="img_save_#= order_id#" data-order="#= order_id#" src="<?php echo URL;?>images/icons/accept.png" style="cursor: pointer" onclick="save_status(this)" />&nbsp;<img id="img_cancel_#= order_id#" data-order="#= order_id#" src="<?php echo URL;?>images/icons/cancel.png" style="cursor: pointer" onclick="cancel_status(this)" /></span></span><span id="sp_status_title">#= status_name #</span><span id="sp_edit"#= <?php echo $v_is_admin?'status_name!="New"':'status_name!="New';?>?" style=\'display:none\'":"" #>&nbsp;&nbsp;<img id="img_edit_#= order_id#" data-order="#= order_id#" src="<?php echo URL;?>images/icons/tab_edit.png" style="cursor: pointer;" onclick="edit_status(this)" /></span>'},
                            <?php }else{?>
                            {field: "status_name", title: "Order Status", type:"string", width:"60px",filterable: {ui: filter_order_status}, sortable: false, encoded: false},
                            <?php }?>
							{ command:  [
								{ name: "View", text:'', alt:'View current order', click: view_row, imageClass: 'k-grid-View' }
								],
								title: " ", width: "50px" }
						 ]
					 }).data("kendoGrid");

                        var tooltip = grid.table.kendoTooltip({
                            filter: "td:last-child a",
                            position: "top",
                            content: function (e) {
                                var target = e.target; // element for which the tooltip is shown
                                if($(target).hasClass('k-grid-View'))
                                    return 'View current Order';
                            }
                        }).data("kendoTooltip");

                    var status_data = <?php echo json_encode($arr_order_status);?>;
                        function filter_order_status(element){
                            element.kendoDropDownList({
                                dataSource:status_data,
                                dataTextField:'status_name',
                                dataValueField:'status_key',
                                optionLabel: "--Select Status--"
                            });
                        }
                    function on_data_bound(e){
                        var grid = e.sender;
                        var combo = '';
                        var data = grid.dataSource.view();
                        for(var i=0; i<data.length; i++){
                            if(data[i].anvy_id=='none' || data[i].anvy_id==''){
                                var $a = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] a.k-grid-Edit-order-price");
                                $a.css('display', 'none');
                            }
                            var $sel = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] select#sel_status");
                            var $sp = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] span#sp_status");
                            <?php if($v_is_admin){?>
//                                if(data[i].status_name=='Submitted'){
                                if(data[i].status_name=='New'){
                                    combo = $sel.width(120).kendoDropDownList({
                                        dataSource:status_data,
                                        dataTextField:'status_name',
                                        dataValueField:'status_key'
                                    }).data("kendoDropDownList");
//                                    alert(data[i].status_name);
                                    combo.value(data[i].status_name);
                                }
                            <?php }else if($v_edit_right){?>
//                                if(data[i].status_name=='Submitted'){
                                if(data[i].status_name=='New'){
                                     combo = $sel.width(120).kendoDropDownList({
                                        dataSource:status_data,
                                        dataTextField:'status_name',
                                        dataValueField:'status_key'
                                    }).data("kendoDropDownList");
                                    combo.value(data[i].status_name);
                                }
                            <?php }?>
                        }
                    }
				});
                var loading = '<?php echo URL;?>images/icons/loading.gif';
                function edit_status(obj){
                    var id = obj.id;
                    var $this = $('img#'+id);
                    $this.parent().parent().find('span#sp_save').css('display','');
                    $this.parent().parent().find('span#sp_status_title').css('display','none');
                    $this.parent().parent().find('span#sp_edit').css('display','none');
                }
                function save_status(obj){
                    var id = obj.id;
                    var $this = $('img#'+id);
                    var $sel = $this.parent().parent().find('select#sel_status');
                    var old_status = $sel.attr("data-status");
                    var order_id = $sel.attr("data-order");
                    var combo = $sel.data("kendoDropDownList");
                    var new_status = combo.value();
                    var src = $this.attr("src");
                    $.ajax({
                        url     : '<?php echo URL.$v_admin_key;?>/ajax',
                        type    :   'POST',
                        data    :   {txt_session_id:'<?php echo session_id();?>', txt_ajax_type:'save_order_status', txt_order_id:order_id, txt_status: new_status},
                        beforeSend: function(){
                            $this.attr("src", loading);
                            $this.prop("disabled", true);
                        },
                        success: function(data, status){
                            var ret = $.parseJSON(data);
                            alert(ret.error);
                            $this.attr("src", src);
                            $this.prop("disabled", false);
                            if(ret.error==0){
                                $sel.attr("data-status", new_status);
                                $this.parent().parent().parent().find('span#sp_save').css('display','none');
                                $this.parent().parent().parent().find('span#sp_status_title').css('display','');

                                if(new_status=="Partly shipped" || new_status=="In production"){
                                    <?php if(($v_shipping_view_right && $v_shipping_edit_right) || $v_is_admin){?>
                                    $this.parent().parent().parent().find('span#sp_status_title').html('<a class="a-link" href="<?php echo URL.$v_admin_key.'/';?>'+order_id+'/shippingid">'+combo.text()+'</a>');
                                    <?php }else{?>
                                    $this.parent().parent().parent().find('span#sp_status_title').html(combo.text());
                                    <?php }?>
                                    $this.parent().parent().parent().find('span#sp_edit').css('display','none');
                                }else{
                                    $this.parent().parent().parent().find('span#sp_status_title').html(combo.text());
                                }
                                $this.parent().parent().parent().find('span#sp_edit').css('display',new_status==300?'none':'');
                                if(ret.show_message==1) alert(ret.message);
                            }else{
                                alert(ret.message);
                            }
                        }
                    });
                }
                function cancel_status(obj){
                    var id = obj.id;
                    var $this = $('img#'+id);
                    var $sel = $this.parent().parent().find('select#sel_status');
                    var status = $sel.attr("data-status");
                    status = parseInt(status, 10);
                    var combo = $sel.data("kendoDropDownList");
                    combo.value(status);
                    $this.parent().parent().parent().find('span#sp_save').css('display','none');
                    $this.parent().parent().parent().find('span#sp_status_title').css('display','');
                    $this.parent().parent().parent().find('span#sp_edit').css('display','');
                }
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
<!--                      window.open("--><?php //echo URL.$v_admin_key.'/';?><!--"+dataItem.order_id+"/viewid","_self");-->
                    document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.order_id+"/viewid";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    //if(confirm('Do you want to add price kitting for order : '+dataItem.po_number+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.order_id+"/editid";
                    //}
                }
            </script>
                </div>
            </div>
        </div>
  </div>