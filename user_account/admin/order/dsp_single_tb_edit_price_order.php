<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $('.product_current_price').kendoNumericTextBox({
            format:"c2",
            min:0,
            step:0.01
        });
        $('.product_current_price').each(function(){
            $(this).on("keyup",function(){
                var price = $(this).val();
                var id = $(this).attr("data-order-item-id");
                var id_quantity = "#quantity_"+id;
                var id_total = "#total_"+id;
                var quantity = $(id_quantity).text();
                var total = $(id_total).text("$ "+quantity * price );
                if(price > 0){
                    var order_id = $("#txt_order_id").val();
                    var order_item = $("#txt_order_items_id").val();
                    if(order_item!="")
                        order_item +=id+"|";
                    else order_item = id +"|";
                    var hide_id = "#price_" + id;
                    if(price>0){
                        $("#txt_order_items_id").val(order_item);
                        $(hide_id).val(price);
                        var total_money = $("#txt_hidden_money").val();
                        total_money = parseFloat(total_money);
                        total_money += quantity * price
                        $("#total_money_all").text(total_money);
                        $("#txt_hidden_money").val(total_money)
                    }else{
                        $(hide_id).val(0);
                    }
                }
                return false;
            });
        });
        $("#btn_submit_tb_product").click(function(){
            if(!confirm("You only edit price one times. After you save prices, you can't change it again. Are you sure?")){
                return false;
            }
        });
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Order<?php echo ': #'.$v_po_number;?></h3>
                </div>
                <div id="data_single_tab">
                    <ul>
                        <li class="k-state">Information</li>
                        <li class="k-state-active">Prices</li>
                    </ul>
                    <div class="information div_details">
                        <form id="frm_tb_company" action="<?php echo URL.$v_admin_key;?>/<?php echo $v_order_id.'/edit-prices';?>" method="POST">
                            <input type="hidden" id="txt_order_id" name="txt_order_id" value="<?php echo $v_order_id;?>" />
                            <input type="hidden" id="txt_hidden_money" name="txt_hidden_money" value="<?php echo $v_total_order_amount;?>" />
                            <input type="hidden" id="txt_order_items_id" name="txt_order_items_id" value="<?php echo $v_order_id_item_string; ?>" />
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="5" cellspacing="5">
                                <tr  valign="top">
                                    <td width="200px">Company</td>
                                    <td align="left" colspan="6">
                                        <b> <?php echo $cls_tb_company->select_scalar('company_name',array('company_id'=>(int)$v_company_id)); ?></b>
                                    </td>
                                </tr>
                                <tr  valign="top">
                                    <td>Order Ref</td>
                                    <td align="left" colspan="6">
                                        <?php echo $v_order_ref; ?>
                                    </td>
                                </tr>
                                <tr  valign="top" >
                                    <td>Po Number</td>
                                    <td align="left" colspan="6">
                                        <?php echo $v_po_number;?>
                                    </td>
                                </tr>
                                <tr  valign="top">
                                    <td>Total Order Amount</td>
                                    <td align="left">
                                        $ <span id="total_money_all" class="note"><b><?php echo $v_total_order_amount ;?></b> </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Number of distribution location :</td>
                                    <td>
                                        <?php echo number_format($v_use_allocate);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cost for Kitting / Distribution</td>
                                    <td>
                                        <?php echo format_currency($v_cost_kitting);?>
                                    </td>

                                </tr>
                            </table>


                    </div>
                    <div class="prices div_details">
                            <input type="hidden" id="txt_order_id" name="txt_order_id" value="<?php echo $v_order_id;?>" />
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="5" cellspacing="5">
                                <tr>
                                    <th>Description</th>
                                    <!--th>Size</th>
                                    <th>Material name</th>
                                    <th>Material color</th>
                                    <th>Thickness </th-->
                                    <th>Quantity</th>
                                    <th>Unit prices</th>
                                    <th>Total prices</th>
                                </tr>
                                <?php echo $v_table_product; ?>
                            </table>

                    </div>
                </div>
            </div>
            <div class="k-block k-widget div_buttons">
                <input type="submit" class="k-button button_css" value="Update " name="btn_submit_tb_product" id="btn_submit_tb_product">
            </div>
            </form>
        </div>
    </div>
</div>
