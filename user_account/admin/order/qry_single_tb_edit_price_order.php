<?php if(!isset($v_sval)) die();?>
<?php
$v_order_id = isset($_GET['id'])?$_GET['id']:0;
settype($v_order_id,"int");
$v_error_message = '';
$v_location_id = 0;
$v_po_number = '';
$v_total_order_amount = 0;
$v_order_ref = '';
$v_cost_kitting = 0;
$v_use_allocate = 0;
$v_dsp_tb_order_items = '';
$v_dsp_order_log = '';
add_class("cls_tb_order_items");
$cls_tb_order_items = new cls_tb_order_items($db);

add_class("cls_tb_order");
$cls_tb_order = new cls_tb_order($db);

$v_count_order = $cls_tb_order->select_one(array("order_id"=>$v_order_id));
$v_table_product = '';
$v_result_2 = false;
$v_order_id_item_string = '';
$v_result = false;

if(isset($_POST['btn_submit_tb_product']) && $_POST['btn_submit_tb_product']){
    $v_temp = $_POST['txt_order_items_id'];
    $v_order_id_temp = $_POST['txt_order_id'];
    settype($v_order_id_temp,"int");
    $v_price = '';
    $arr_temp = explode("|",$v_temp);
    $v_order_total_amount = (float)$cls_tb_order->get_total_order_amount();
    $v_total_temp = 0;
    $arr_temp_temp = array();
    for($i=0;$i<count($arr_temp);$i++){
        $v_order_item_temp = $arr_temp[$i];
        settype($v_order_item_temp,"int");
        if($v_order_item_temp > 0) $arr_temp_temp[] = $v_order_item_temp;
    }
    $arr_temp_temp_duplicate = array();
    for($i=0;$i<count($arr_temp);$i++){
        $v_order_item_temp = $arr_temp[$i];
        settype($v_order_item_temp,"int");
        $v_count = 0;
        for($j=0;$j<count($arr_temp);$j++){
            $v_order_item_temp_j = $arr_temp[$j];settype($v_order_item_temp_j,"int");
            if($v_order_item_temp_j == $v_order_item_temp) $v_count++;
        }
        if($v_count > 1 && !in_array($v_order_item_temp,$arr_temp_temp_duplicate)) $arr_temp_temp_duplicate[] = $v_order_item_temp;
    }
    $arr_temp = $arr_temp_temp_duplicate;
    for($i=0;$i<count($arr_temp);$i++){

        $v_order_item_temp = $arr_temp[$i];
        $v_order_item_price = isset($_POST["price_".$v_order_item_temp])?$_POST["price_".$v_order_item_temp] :0;

        $v_order_item_price = (float) $v_order_item_price;
        if($v_order_item_price > 0 && $v_order_item_price!='' ){
            $v_order_item_quantity = $cls_tb_order_items->select_scalar("quantity",array("order_id"=>$v_order_id_temp,"order_item_id"=>$v_order_item_temp));

            $arr_allocation_temp = $cls_tb_order_items->select_scalar("allocation",array("order_id"=>$v_order_id_temp,"order_item_id"=>$v_order_item_temp));

            for($j=0;$j<count($arr_allocation_temp);$j++){
                $arr_allocation_temp[$j]['location_price'] = $v_order_item_price;
            }
            settype($v_order_item_quantity,"int");

            $v_total = $v_order_item_price * $v_order_item_quantity;
            $v_total_temp +=$v_total;
            $arr_where = array("order_item_id"=>$v_order_item_temp,"order_id"=>$v_order_id_temp);

            $v_order_total_amount +=$v_total;
           // die("$v_order_total_amount");
            $v_result = $cls_tb_order_items->update_fields(array("status","current_price","total_price","allocation"),array(0,$v_order_item_price,$v_total,$arr_allocation_temp),$arr_where);
            $v_result = $cls_tb_order->update_field("total_order_amount",$v_order_total_amount,array("order_id"=>$v_order_id_temp));
            //die("$v_order_total_amount");
        }

    }
    if($v_result){
       // $v_order_total_amount +=$v_total_temp;
        $v_raw_id = $cls_tb_order->get_anvy_id();
        $v_count = $cls_tb_order_items->count(array("order_id"=>$v_order_id_temp,"status"=>100));
        if($v_count == 0)
            $v_result_2 = $cls_tb_order->update_fields(array("status","anvy_id","raw_id"),array(10,"",$v_raw_id),array("order_id"=>$v_order_id_temp));
            //$v_result_2 = $cls_tb_order->update_fields(array("status","anvy_id","raw_id"),array(10,"",$v_raw_id,$v_order_total_amount),array("order_id"=>$v_order_id_temp));
        else redir(URL.'admin/order');
    }
    if($v_result_2) redir(URL.'admin/order/order');
    else redir(URL.'admin/order/order/'.$v_order_id.'/edit-prices');
}
else if($v_order_id!=0 && $v_count_order==1){
    $v_anvy_id = $cls_tb_order->select_scalar("anvy_id",array("order_id"=>$v_order_id));
    if($v_anvy_id=='') redir(URL.'admin/error');// co ai do co vao order hop le thi bao loi

    $v_location_id = $cls_tb_order->get_location_id();
    $v_po_number = $cls_tb_order->get_po_number();
    $v_company_id = $cls_tb_order->get_company_id();

    settype($v_company_id,"int");
    $v_order_ref = $cls_tb_order->get_order_ref();
    $v_total_order_amount = $cls_tb_order->get_total_order_amount();
    $v_cost_kitting = $cls_tb_order->get_cost_kitting();
    $arr_order_item = $cls_tb_order_items->select(array("order_id"=>$v_order_id,"status"=>100,"location_id"=>$v_location_id));
    $arr_temp_location = array();
    foreach($arr_order_item as $arr_temp){
        //$arr_temp
        $arr_allocation = $arr_temp['allocation'];
        foreach($arr_allocation as $arr){
            $v_temp_location_id = $arr['location_id'];
            settype($v_temp_location_id,"int");
            if(!in_array($v_temp_location_id,$arr_temp_location)){
                $arr_temp_location[] = $v_temp_location_id;
                $v_use_allocate++;
            }
        }
        $v_order_item_id = $arr_temp['order_item_id'];
        $v_product_description = $arr_temp['product_description'];
        $v_product_size = $arr_temp['width']." &times ".$arr_temp['length']." ".$arr_temp['unit'];
        $v_material_name = $arr_temp['material_name'];
        $v_material_color = $arr_temp['material_color'];
        $v_material_thickness = $arr_temp['material_thickness_value']." ".$arr_temp['material_thickness_unit'];
        $v_product_quantity = $arr_temp['quantity'];
        $v_current_price = $arr_temp['current_price'];
        $v_total_price = $v_product_quantity * $v_current_price;
        $v_order_id_item_string .=$v_order_item_id.'|';
        $v_table_product .='<tr>';
            $v_table_product .='<td>'.$v_product_description.'</td>';
            //$v_table_product .='<td>'.$v_product_size.'</td>';
            //$v_table_product .='<td>'.$v_material_name.'</td>';
            //$v_table_product .='<td>'.$v_material_color.'</td>';
            //$v_table_product .='<td>'.$v_material_thickness.'</td>';
            $v_table_product .='<td><span id="quantity_'.$v_order_item_id.'">'.$v_product_quantity.'</span></td>';
            $v_table_product .='<td><input type="hidden" id="price_'.$v_order_item_id.'" name="price_'.$v_order_item_id.'" value="'.$v_current_price.'"  /><input type="number" class="product_current_price" id="product_current_price" name="product_current_price" data-order-item-id="'.$v_order_item_id.'" value="'.$v_current_price.'" /></td>';
            $v_table_product .='<td><span id="total_'.$v_order_item_id.'">'.format_currency($v_total_price).'</span></td>';
        $v_table_product .='</tr>';
    }
    $v_total_order_amount += $v_cost_kitting*$v_use_allocate;
}
else{
    redir(URL.'admin/error');
}
?>