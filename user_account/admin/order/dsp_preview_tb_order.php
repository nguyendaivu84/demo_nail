<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $("#panelbar").kendoPanelBar({
            expandMode: "single"
        });
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>PO Number<?php echo ': #'.$v_po_number;?></h3>
                    <h3>Order Ref<?php echo $v_order_ref;?></h3>
                </div>
                <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>List Entry</li>
                    </ul>
                    <div class="information div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="5">
                            <tr align="right" valign="top">
                                <td width="200px">Company</td>
                                <td align="left" colspan="6">
                                    <b> <?php echo $v_company; ?></b>
                                </td>
                            </tr>
                            <tr align="right" valign="top" >
                                <td>Order By</td>
                                <td align="left">
                                    <?php echo $v_name;?>
                                </td>
                            </tr>
                            <tr align="right" valign="top" >
                                <td>Customer email</td>
                                <td align="left">
                                    <?php echo $v_email;?>
                                </td>
                            </tr>
                            <tr align="right" valign="top" >
                                <td>Customer phone</td>
                                <td align="left">
                                    <?php echo $v_phone;?>
                                </td>
                            </tr>
                            <tr align="right" valign="top" >
                                <td>Shipping Address</td>
                                <td align="left">
                                    <?php echo $v_address_shipping;?>
                                </td>
                            </tr>
                            <tr align="right" valign="top" >
                                <td>Billing Address</td>
                                <td align="left">
                                    <?php echo $v_billing_address;?>
                                </td>
                            </tr>

                            <tr align="right" valign="top" >
                                <td>Date Created</td>
                                <td align="left">
                                    <?php echo fdate($v_date_created);?>
                                </td>
                            </tr>
                            <tr align="right" valign="top" >
                                <td>Total Order Amount</td>
                                <td align="left" colspan="6">
                                    <span class="note"><b>  </span><?php echo format_currency((float)$v_total_amount_cost) ;?></b> </span>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Status</td>
                                <td align="left">
                                    <b>  <?php  echo $v_order_status;?></b>
                                </td>
                            </tr>
                            <?php if($v_order_status == 30 && $v_user_approved!='' ){ ?>
                                <tr align="right" valign="top" >
                                        <td>User Approved</td>
                                        <td align="left">
                                            <b>  <?php  echo $v_user_approved;?></b>
                                        </td>
                                        <td>Date Approved</td>
                                        <td align="left">
                                            <b>  <?php  echo fdate($v_date_approved);;?></b>
                                        </td>
                                </tr>
                            <?php }?>
                            <tr align="right" valign="top">
                                <td>Dispatch</td>
                                <td align="left" colspan="6">
                                    <?php
                                        if($v_order_status !=50 && $v_order_status !=55 && $v_order_status !=60 && $v_order_status !=65 ){
                                    ?>
                                            <b> <?php echo  "No";?></b>
                                    <?php
                                        }
                                        else{
                                    ?>
                                         <b> <?php echo  $cls_settings->get_option_name_by_id('dispatch',$v_dispatch,0);?></b>
                                    <?php
                                        }
                                    ?>

                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="items div_details">
                        <ul id="panelbar">
                            <?php echo $v_distribution;?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
