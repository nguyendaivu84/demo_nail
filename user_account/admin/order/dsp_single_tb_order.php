<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
    var tab_strip = $("#data_single_tab").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        }
    }).data("kendoTabStrip");
    $('#txt_cost_kitting').kendoNumericTextBox({
        format:"n0",
        min:0,
        step:1
    });
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Order<?php echo ': #'.$v_po_number;?></h3>
                    </div>
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                    </ul>
                    <div class="information div_details">
                        <form id="frm_tb_company" action="<?php echo URL.$v_admin_key;?>/<?php echo $v_order_id.'/edit';?>" method="POST">
                        <input type="hidden" id="txt_order_id" name="txt_order_id" value="<?php echo $v_order_id;?>" />
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="5" cellspacing="5">
                            <tr  valign="top">
                                <td width="200px">Company</td>
                                <td align="left" colspan="6">
                                    <b> <?php echo $cls_tb_company->select_scalar('company_name',array('company_id'=>(int)$v_company_id)); ?></b>
                                </td>
                            </tr>
                            <tr  valign="top" >
                                <td>Location</td>
                                <td align="left" colspan="6">
                                    <b> <?php echo $cls_tb_location->select_scalar('location_name',array('location_id'=>(int)$v_order_location_id)); ?></b>
                                </td>
                            </tr>
                            <tr  valign="top">
                                <td>Order Ref</td>
                                <td align="left" colspan="6">
                                    <?php echo $v_order_ref; ?>
                                </td>
                            </tr>
                            <tr  valign="top" >
                                <td>Po Number</td>
                                <td align="left" colspan="6">
                                    <?php echo $v_po_number;?>
                                </td>
                            </tr>
                            <tr  valign="top" >
                                <td>Shipping Contact</td>
                                <td align="left" colspan="6">
                                    <?php echo  $cls_tb_contact->get_infomation_contact($v_shipping_contact)  ;?>
                                </td>
                            </tr>
                            <tr  valign="top">
                                <td>Total Order Amount</td>
                                <td align="left">
                                    <span class="note"><b><?php echo format_currency((float)$v_total_order_amount) ;?></b> </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Use Allocate :</td>
                                <td>
                                    <input type="checkbox" name="txt_use_allocate" id="txt_use_allocate" <?php  echo ($v_use_allocate==1?'checked="checked"':'' ); ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Cost for Kitting / Distribution</td>
                                <td>
                                    <input type="text" name="txt_cost_kitting" id="txt_cost_kitting" value="<?php echo $v_cost_kitting; ?>">
                                </td>

                            </tr>
                        </table>
                    </div>
                   </div>
                </div>
                <div class="k-block k-widget div_buttons">
                    <input type="submit" class="k-button button_css" value="Update " name="btn_submit_tb_product" id="btn_submit_tb_product">
                </div>
                </form>
            </div>
        </div>
  </div>
