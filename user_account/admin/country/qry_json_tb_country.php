<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_country_name = isset($_POST['txt_search_country_name'])?trim($_POST['txt_search_country_name']):'';
if($v_search_country_name!='') $arr_where_clause['country_name'] = new MongoRegex('/'.$v_search_country_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_tag_redirect']) && $_SESSION['ss_tb_tag_redirect']==1){
    if(isset($_SESSION['ss_tb_tag_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_tag_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_tag_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_tag_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_tag_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_country->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_tag_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_tag_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_tag_page'] = $v_page;
$_SESSION['ss_tb_tag_quick_search'] = $v_quick_search;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("country_name"=>1);
$arr_tb_country = $cls_tb_country->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_country as $arr){
    $v_country_id = (string)isset($arr['_id'])?$arr['_id']:'';
    $v_country_name = isset($arr['name'])?$arr['name']:'';
    $v_country_key = isset($arr['value'])?$arr['value']:'';
    $v_country_order = isset($arr['country_order'])?$arr['country_order']:0;
    $v_country_status = isset($arr['deleted'])?$arr['deleted']:false;
    $v_country_status = $v_country_status==false?"Active":"Inactive";
    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'country_id' => (string)$v_country_id,
        'country_name' => $v_country_name,
        'country_key' => $v_country_key,
        'country_order' => $v_country_order,
        'country_status' => $v_country_status
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_country'=>$arr_ret_data);
echo json_encode($arr_return);
?>