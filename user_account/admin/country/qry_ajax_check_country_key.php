<?php
if(!isset($v_sval)) die();?>
<?php
$v_country_key = isset($_POST['txt_country_key'])?$_POST['txt_country_key']:'';
$v_country_id = isset($_POST['txt_country_id'])?$_POST['txt_country_id']:'0';
settype($v_country_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK');
if($v_country_key!=''){

    if($cls_tb_country->count(array('country_key'=>strtoupper($v_country_key), 'country_id'=>array('$ne'=>$v_country_id)))>0){
        $arr_return = array('error'=>1, 'message'=>'Duplicate Country Key!');
    }
}else{
    $arr_return = array('error'=>2, 'message'=>'Lost data!');
}
echo json_encode($arr_return);
?>