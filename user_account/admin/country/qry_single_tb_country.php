<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_country_id = 0;
$v_country_name = '';
$v_country_key = '';
$v_country_status = '';
$v_country_order = 0;
$v_new_country = true;
if(isset($_POST['btn_submit_tb_country'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_country->set_mongo_id($v_mongo_id);
    $v_country_name = isset($_POST['txt_country_name'])?$_POST['txt_country_name']:$v_country_name;
    $v_country_name = trim($v_country_name);
    if($v_country_name=='') $v_error_message .= '[Country Name] is empty!<br />';
    $cls_tb_country->set_country_name($v_country_name);
    $v_country_key = isset($_POST['txt_country_key'])?$_POST['txt_country_key']:$v_country_key;
    $v_country_key = trim($v_country_key);
    if($v_country_key=='') $v_error_message .= '[Country Key] is empty!<br />';
    $cls_tb_country->set_country_key($v_country_key);
    $v_country_status = isset($_POST['txt_country_status'])?0:1;
    $v_country_status = (int) $v_country_status;
    $cls_tb_country->set_country_status($v_country_status);
    $v_country_order = isset($_POST['txt_country_order'])?$_POST['txt_country_order']:$v_country_order;
    $v_country_order = (int) $v_country_order;
    $cls_tb_country->set_country_order($v_country_order);
    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_country->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_country->update(array('_id' => $v_mongo_id));
            $v_new_country = false;
        }
        if($v_result){
            $_SESSION['ss_tb_tag_redirect'] = 1;
            redir(URL.$v_admin_key);
        }else{
            if($v_new_country) $v_country_id = 0;
        }
    }
}else{
    $v_country_id= isset($_GET['id'])?$_GET['id']:'';
    if($v_country_id!=''){
        $v_row = $cls_tb_country->select_one(array('_id' => new MongoId($v_country_id)));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_country->get_mongo_id();
            $v_country_id = $cls_tb_country->get_country_id();
            $v_country_name = $cls_tb_country->get_country_name();
            $v_country_key = $cls_tb_country->get_country_key();
            $v_country_status = $cls_tb_country->get_country_status();
            $v_country_order = $cls_tb_country->get_country_order();
        }
    }
}
?>