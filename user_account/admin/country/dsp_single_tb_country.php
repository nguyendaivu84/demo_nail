<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        var validator = $("div.information").kendoValidator().data("kendoValidator");
        $("input#btn_submit_tb_country").click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                return false;
            }
        });
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $('input#txt_country_key').focusout(function(e){
            var val = $.trim($(this).val());
            if(val==''){
                $(this).val('');
                $('input#txt_hidden_country_key').val('N');
                return false;
            }
            var country_id = $('input#txt_country_id').val();
            $.ajax({
                url:    '<?php echo URL.$v_admin_key;?>/ajax',
                type:   'POST',
                data:   {txt_session_id: '<?php echo session_id();?>', txt_country_key: val, txt_ajax_type: 'check_country_key', txt_country_id: country_id},
                success: function(data, status){
                    $('span#sp_country_key').html('');
                    var ret = $.parseJSON(data);
                    if(ret.error==0){
                        $('input#txt_hidden_country_key').val('Y');
                    }else{
                        $('input#txt_hidden_country_key').val('');
                    }
                    validator.validate();
                }
            });
        });
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Country <?php echo (isset($v_country_id) && $v_country_id)?': '.$v_country_name:'';?></h3>
                </div>
                <div id="div_quick">
                    <div id="div_quick_search">
                        &nbsp;
                    </div>
                </div>
                <form id="frm_tb_country" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_country_id.'/editid';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_country_id" name="txt_country_id" value="<?php echo $v_country_id;?>" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">information</li>
                        </ul>
                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Country Name</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left"><input class="k-textbox" type="text" id="txt_country_name" name="txt_country_name" value="<?php echo $v_country_name;?>" required validationMessage="Please input Country Name" /> <label id="lbl_tag_name" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Country Key</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_country_key" name="txt_country_key" value="<?php echo $v_country_key;?>" required validationMessage="Please input Country Key" />
                                        <input type="hidden" id="txt_hidden_country_key" name="txt_hidden_country_key" value="<?php echo $v_country_key!=''?'Y':'N';?>" required data-required-msg="Duplicate Country Key!" />
                                        <span id="sp_country_key"></span>
                                        <label id="lbl_country_key" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Country Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><label><input type="checkbox" id="txt_country_status" name="txt_country_status"<?php echo $v_country_status==0?' checked="checked"':'';?> /> Active?</label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_country" name="btn_submit_tb_country" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
</div>