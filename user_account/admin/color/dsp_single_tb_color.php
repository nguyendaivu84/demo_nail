<?php if(!isset($v_sval)) die();?>
<script type="text/javascript" src="<? echo URL; ?>lib/colorpicker/js/colorpicker.js"></script>
<link href="<? echo URL; ?>lib/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
    $(document).ready(function(){
        $("form#frm_tb_color").submit(function(){
            if($("#txt_color_name").val()=="")
            {
                alert("Input color name!....");
                $("#txt_color_name").focus();
                $("#txt_color_name").addClass('invalid');
                return false;
            }
            return true;
        });
        $('#txt_color_code_hex').kendoColorPicker({value:$(this).val(), buttons: true})
        $("input#btn_submit_tb_province").click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                return false;
            }
            return true;
        });
        $('input#txt_color_code_hex').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val('<?php echo $v_color_code_hex;?>');
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }
        }).bind('keyup', function(){
                $(this).ColorPickerSetColor(this.value);
            });
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        var tooltip = $("#tooltip").kendoTooltip({
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        if(tooltip) tooltip.show();
        var validator = $("div.information").kendoValidator().data("kendoValidator");
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Color</h3>
                </div>
                <div id="div_quick">
                    <div id="div_quick_search">
                        &nbsp;
                    </div>
                    <div id="div_select"><!--
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    //echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        --></div>
                </div>

                <form id="frm_tb_color" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_color_id.'/edit';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_color_id" name="txt_color_id" value="<?php echo $v_color_id;?>" />
                    <input type="hidden" id="txt_color_order" name="txt_color_order" value="<?php echo $v_color_order;?>" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                        </ul>

                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Color Name</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_color_name" name="txt_color_name" value="<?php echo $v_color_name;?>" required data-required-msg="Please input Color Name" /> <label id="lbl_province_name" class="k-required">(*)</label></td>
                                </tr>
                                <!--tr align="right" valign="top">
                                    <td style="width: 200px">Color Code</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_color_code" name="txt_color_code" value="<?php echo $v_color_code;?>" required data-required-msg="Please input Color Code" /> <label id="lbl_province_name" class="k-required">(*)</label></td>
                                </tr-->
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Color </td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" size="50" type="text" id="txt_color_code_hex" name="txt_color_code_hex" value="<?php echo $v_color_code_hex;?>" required data-required-msg="Please input Color Code Hex" /> <label id="lbl_province_name" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Color Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <label><input type="checkbox" id="txt_color_status" name="txt_color_status"<?php echo $v_color_status==0?' checked="checked"':'';?> /> Active?</label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_color" name="btn_submit_tb_color" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
