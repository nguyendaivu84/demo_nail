<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_static_content_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_static_content_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_static_content_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_static_content = $cls_tb_nail_static_content->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_static_content = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_static_content .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_static_content .= '<th>Ord</th>';
$v_dsp_tb_nail_static_content .= '<th>Content Id</th>';
$v_dsp_tb_nail_static_content .= '<th>Content Key</th>';
$v_dsp_tb_nail_static_content .= '<th>Content Status</th>';
$v_dsp_tb_nail_static_content .= '<th>Content Content</th>';
$v_dsp_tb_nail_static_content .= '<th>Content Image</th>';
$v_dsp_tb_nail_static_content .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_static_content as $arr){
	$v_dsp_tb_nail_static_content .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_static_content .= '<td align="right">'.($v_count++).'</td>';
	$v_content_id = isset($arr['content_id'])?$arr['content_id']:0;
	$v_content_key = isset($arr['content_key'])?$arr['content_key']:'';
	$v_content_status = isset($arr['content_status'])?$arr['content_status']:0;
	$v_content_content = isset($arr['content_content'])?$arr['content_content']:'';
	$v_content_image = isset($arr['content_image'])?$arr['content_image']:'';
	$v_dsp_tb_nail_static_content .= '<td>'.$v_content_id.'</td>';
	$v_dsp_tb_nail_static_content .= '<td>'.$v_content_key.'</td>';
	$v_dsp_tb_nail_static_content .= '<td>'.$v_content_status.'</td>';
	$v_dsp_tb_nail_static_content .= '<td>'.$v_content_content.'</td>';
	$v_dsp_tb_nail_static_content .= '<td>'.$v_content_image.'</td>';
	$v_dsp_tb_nail_static_content .= '</tr>';
}
$v_dsp_tb_nail_static_content .= '</table>';
?>