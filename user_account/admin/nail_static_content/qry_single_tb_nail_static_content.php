<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = '';
$v_content_id = 'none';
$v_content_key = '';
$v_content_status = 1;
$v_content_content = '';
$v_content_image = '';
$v_content_des = '';
$v_new_nail_static_content = true;
$arr_list_image = array();
$v_lst_image = '';
$v_saved_dir = 'resources/static_content/';
if(isset($_POST['btn_submit_tb_nail_static_content'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_static_content->set_mongo_id($v_mongo_id);
	$v_content_id = isset($_POST['txt_content_id'])?$_POST['txt_content_id']:$v_content_id;
	if(is_null($v_mongo_id)){
		$v_content_id = $cls_tb_nail_static_content->select_next('content_id');
	}
	$v_content_id = (int) $v_content_id;
	$cls_tb_nail_static_content->set_content_id($v_content_id);
    $v_content_des = isset($_POST['txt_content_des'])?$_POST['txt_content_des']:$v_content_des;
    $cls_tb_nail_static_content->set_content_description($v_content_des);
	$v_content_key = isset($_POST['txt_content_key'])?$_POST['txt_content_key']:$v_content_key;
	$v_content_key = trim($v_content_key);
	if($v_content_key=='') $v_error_message .= '[Content Key] is empty!<br />';
	$cls_tb_nail_static_content->set_content_key($v_content_key);
	$v_content_status = isset($_POST['txt_content_status'])?$_POST['txt_content_status']:0;
    $v_content_status = $v_content_status == 'on' ? 1 : 0 ;
	$cls_tb_nail_static_content->set_content_status($v_content_status);
	$v_content_content = isset($_POST['txt_content_content'])?$_POST['txt_content_content']:$v_content_content;
	$v_content_content = trim($v_content_content);
//	if($v_content_content=='') $v_error_message .= '[Content Content] is empty!<br />';
	$cls_tb_nail_static_content->set_content_content($v_content_content);
    //==================== xu ly upload multi images
    if(!file_exists($v_upload_dir = PRODUCT_IMAGE_DIR)){
        mkdir($v_upload_dir = PRODUCT_IMAGE_DIR);
    }
    $v_upload_dir .= 'static_content';
    if(isset($_FILES['txt_content_image'])){
        $v_image_file = '';
        foreach($_FILES['txt_content_image']['tmp_name'] as $v_key => $v_tmp_name ){
            $v_file_name = $_FILES['txt_content_image']['name'][$v_key];
            $v_file_size =$_FILES['txt_content_image']['size'][$v_key];
            $v_file_tmp =$_FILES['txt_content_image']['tmp_name'][$v_key];
            $v_file_type=$_FILES['txt_content_image']['type'][$v_key];
            /* Upload images */
            $v_width = 0;
            if($v_file_size >0){
                $arr_image = explode('.', $v_file_name);
                $v_file_type = $arr_image[sizeof($arr_image)-1];
                $v_file_name = $arr_image[0];
                $v_file_name = remove_invalid_char($v_file_name);
                $v_new_file_name =$v_file_name .".".$v_file_type;

                if(!move_uploaded_file($v_file_tmp,$v_upload_dir.DS. $v_new_file_name)){
                    $v_error_message .="Can't upload images ". $v_new_file_name.'<br>';
                }
                else{
                    list($width, $height) = @getimagesize($v_upload_dir.DS.$v_new_file_name);
                    // tat chuc nang resize cua upload image
//                    for($i=0; $i<count($arr_product_image_size); $i++){
//                        $v_width = $arr_product_image_size[$i];
//                        if($v_width < $width){
//                            images_resize_by_width($v_width, $v_upload_dir.DS.$v_new_file_name,$v_upload_dir.DS.$v_width.'_'.$v_new_file_name );
//                            if($v_width==PRODUCT_IMAGE_THUMB) $v_tmp_image_file = PRODUCT_IMAGE_THUMB.'_'.$v_new_file_name;
//                        }
//                    }
                    $arr_list_image [] = $v_new_file_name;
                }
            }
        }
    }else{
        $v_temp = isset($_POST['txt_hidden_content_image'])?$_POST['txt_hidden_content_image']:'';
        echo $v_temp;
        var_dump($v_temp);
        if($v_temp!=''){
            $arr_list_image = json_decode($v_temp,true);
        }
    }
    $v_content_image = $arr_list_image;
    //====================
	$cls_tb_nail_static_content->set_content_image($v_content_image);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_static_content->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_static_content->update(array('_id' => $v_mongo_id));
			$v_new_nail_static_content = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_static_content_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_static_content) $v_nail_static_content_id = 0;
		}
	}
}else{
	$v_content_id= isset($_GET['id'])?$_GET['id']:'none';
	if($v_content_id!='none'){
		$v_row = $cls_tb_nail_static_content->select_one(array('_id' => new MongoId($v_content_id)));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_static_content->get_mongo_id();
			$v_content_id = $cls_tb_nail_static_content->get_content_id();
            $v_content_des = $cls_tb_nail_static_content->get_content_description();
			$v_content_key = $cls_tb_nail_static_content->get_content_key();
			$v_content_status = $cls_tb_nail_static_content->get_content_status();
			$v_content_content = $cls_tb_nail_static_content->get_content_content();
			$v_content_image = $cls_tb_nail_static_content->get_content_image();
            if(is_array($v_content_image)){
                for($i=0;$i<count($v_content_image);$i++){
                    $v_image_name_temp = $v_content_image[$i];
                    if(is_admin() || $v_edit_right ){
                        if(file_exists($v_saved_dir .DS . PRODUCT_IMAGE_THUMB.'_'. $v_image_name_temp))
                            $v_lst_image .= '<div style="float:left;margin-right:20px;" class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img style="width:100px;height:100px" src="'. URL . $v_saved_dir. $v_image_name_temp. '">
                                     </div> ' ;
                        else
                            $v_lst_image .= '<div style="float:left" class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img style="width:100px;height:100px" src="'. URL . $v_saved_dir. $v_image_name_temp. '">
                                    </div> ' ;
                    }else{
                        if(file_exists($v_saved_dir .DS . PRODUCT_IMAGE_THUMB.'_'. $v_image_name_temp))
                            $v_lst_image .= '<div style="float:left" class="img_more_products" >
                                        <img style="width:100px;height:100px" src="'. URL . $v_saved_dir.'_'. $v_image_name_temp. '">
                                     </div> ' ;
                        else
                            $v_lst_image .= '<div style="float:left" class="img_more_products" >
                                        <img style="width:100px;height:100px"  src="'. URL . $v_saved_dir.'_'. $v_image_name_temp. '">
                                    </div> ' ;
                    }
                }
            }
		}
	}
}
?>