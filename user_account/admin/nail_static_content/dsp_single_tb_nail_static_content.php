<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
    $("img[rel=images_product]").on("click",function(){
        if(confirm("Do you want to delete this image ? ")){
            var index_image = $(this).attr('product_images_id');
            var static_id = String("<?php echo (string)$v_mongo_id;?>");
            $.ajax({
                url	    :'<?php echo URL.$v_admin_key ?>/ajax',
                type	:'POST',
                data	:{txt_ajax_type:'static_images',txt_action:'delete-images', txt_index_image:index_image,txt_static_id:static_id},
                success: function(data, type){
                    var ret = $.parseJSON(data);
                    $('span#td_more_images').html('Loading..');
                    if(ret.error==0){
                        $('span#td_more_images').html(ret.data);
                    }
                    else
                        alert(ret.message);
                }
            });
        }
    });
    $("input#txt_content_key").on("change",function(){
        var key_value = $(this).val();
        var static_id = String("<?php echo (string)$v_mongo_id;?>");
        $.ajax({
            url	    :'<?php echo URL.$v_admin_key ?>/ajax',
            type	:'POST',
            data	:{txt_ajax_type:'check_key', txt_value:key_value,txt_static_id:static_id},
            success: function(data, type){
                var ret = $.parseJSON(data);
                if(ret.message!=''){
                    alert(ret.message);
                    $("#txt_check_key").val(1);
                }else if(ret.error == '0') $("#txt_check_key").val(0);
            }
        });
    });
    var editor_content = $('textarea#txt_content_content').kendoEditor({
        tools: [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "fontName",
            "fontSize",
            "foreColor",
            "backColor",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "insertUnorderedList",
            "insertOrderedList",
            "indent",
            "outdent",
            "formatBlock",
            "createLink",
            "unlink",
            "insertImage",
            "subscript",
            "superscript",
            "viewHtml"
        ],
        encoded: false,
        imageBrowser: {
            path:"/resources/static_content/",
            dataType:'json',
            transport: {
                read: {
                    url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                    type: "POST",
                    dataType:'json',
                    data: {txt_ajax_type: 'list_images'}
                },
                destroy: {
                    url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                    type: "POST",
                    dataType:'json',
                    data: {txt_ajax_type: 'delete_image_folder'}
                },
                create: {
                    url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                    type: "POST",
                    dataType:'json',
                    data: {txt_ajax_type: 'create_image_folder'}
                },
                uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_content_id;?>/upload-image",
                thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_content_id;?>/thumb-image"
            }
        }
    }).data("kendoEditor");
	$("input#btn_submit_tb_nail_static_content").click(function(e){
		var css = '';
		var content_key = $("input#txt_content_key").val();
		content_key = $.trim(content_key);
		css = content_key==''?'':'none';
		$("label#lbl_content_key").css("display",css);
		if(css == '') return false;
        var check_key = $("#txt_check_key").val();
        check_key = parseInt(check_key,10);
        if(check_key) {
            alert("Check content key and try again!");
            return false;
        }
		return true;
	});
    $('input#txt_content_image').kendoUpload({
        multiple: true
    });
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Static content</h3>
                    </div>
                    <form id="frm_tb_nail_static_content" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_content_id.'/editid';?>" method="POST">
                        <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                        <input type="hidden" id="txt_content_id" name="txt_content_id" value="<?php echo $v_content_id;?>" />
                        <input type="hidden" id="txt_hidden_content_image" name="txt_hidden_content_image" value='<?php echo json_encode($v_content_image);?>' />
                        <input type="hidden" id="txt_check_key" name="txt_check_key" value="0" />
                        <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <li>Other</li>
                        </ul>

                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">Content Key</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_content_key" name="txt_content_key" value="<?php echo $v_content_key;?>" /> <label id="lbl_content_key" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width:200px">Content Description</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_content_des" name="txt_content_des" value="<?php echo $v_content_des;?>" /> </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Content Content</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 90%; height: 200px; padding:5px;"id="txt_content_content" name="txt_content_content"><?php echo $v_content_content;?></textarea>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width:200px">Upload images</td>
                                    <td style="width:1px">&nbsp;</td>
                                    <td align="left">
                                        <input type="file" id="txt_content_image" name="txt_content_image[]" accept="image/*" />
                                        <span id="td_more_images"><?php echo $v_lst_image;?><span>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Content Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="checkbox" id="txt_content_status" name="txt_content_status" <?php echo $v_content_status==1?' checked="checked"':'';?> /> Active?</label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="other div_details"></div>
                    </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                       <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_nail_static_content" name="btn_submit_tb_nail_static_content" value="Submit" class="k-button button_css" />
                        </div>
                        <?php }?>
                    </form>
                </div>
            </div>
        </div>
  </div>
