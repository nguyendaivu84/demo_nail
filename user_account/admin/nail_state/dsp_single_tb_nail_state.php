<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_state").click(function(e){
		var css = '';

		var state_id = $("input#txt_state_id").val();
		state_id = parseInt(state_id, 10);
		css = isNaN(state_id)?'':'none';
		$("label#lbl_state_id").css("display",css);
		if(css == '') return false;
		var state_name = $("input#txt_state_name").val();
		state_name = $.trim(state_name);
		css = state_name==''?'':'none';
		$("label#lbl_state_name").css("display",css);
		if(css == '') return false;
		var state_order = $("input#txt_state_order").val();
		state_order = parseInt(state_order, 10);
		css = isNaN(state_order)?'':'none';
		$("label#lbl_state_order").css("display",css);
		if(css == '') return false;
		var country_id = $("input#txt_country_id").val();
		country_id = parseInt(country_id, 10);
		css = isNaN(country_id)?'':'none';
		$("label#lbl_country_id").css("display",css);
		if(css == '') return false;
		var country_name = $("input#txt_country_name").val();
		country_name = $.trim(country_name);
		css = country_name==''?'':'none';
		$("label#lbl_country_name").css("display",css);
		if(css == '') return false;
		var state_status = $("input#txt_state_status").val();
		state_status = parseInt(state_status, 10);
		css = isNaN(state_status)?'':'none';
		$("label#lbl_state_status").css("display",css);
		if(css == '') return false;
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_nail_state').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Nail_state</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_nail_state" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_state_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_state_id" name="txt_state_id" value="<?php echo $v_state_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>State Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_state_name" name="txt_state_name" value="<?php echo $v_state_name;?>" /> <label id="lbl_state_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>State Order</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_state_order" name="txt_state_order" value="<?php echo $v_state_order;?>" /> <label id="lbl_state_order" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_id" name="txt_country_id" value="<?php echo $v_country_id;?>" /> <label id="lbl_country_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_name" name="txt_country_name" value="<?php echo $v_country_name;?>" /> <label id="lbl_country_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>State Status</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_state_status" name="txt_state_status" value="<?php echo $v_state_status;?>" /> <label id="lbl_state_status" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_nail_state" name="btn_submit_tb_nail_state" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
