<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_state_redirect']) && $_SESSION['ss_tb_nail_state_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_state_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_state_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_state_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_state_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_state_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_state->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_state_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_state_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_state_page'] = $v_page;
$_SESSION['ss_tb_nail_state_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_state = $cls_tb_nail_state->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_state as $arr){
	$v_state_id = isset($arr['state_id'])?$arr['state_id']:0;
	$v_state_name = isset($arr['state_name'])?$arr['state_name']:'';
	$v_state_order = isset($arr['state_order'])?$arr['state_order']:0;
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_country_name = isset($arr['country_name'])?$arr['country_name']:'';
	$v_state_status = isset($arr['state_status'])?$arr['state_status']:0;
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'state_id' => $v_state_id,
		'state_name' => $v_state_name,
		'state_order' => $v_state_order,
		'country_id' => $v_country_id,
		'country_name' => $v_country_name,
		'state_status' => $v_state_status
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_state'=>$arr_ret_data);
echo json_encode($arr_return);
?>