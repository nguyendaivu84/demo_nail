<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_state_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_state_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_state_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_state = $cls_tb_nail_state->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_state = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_state .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_state .= '<th>Ord</th>';
$v_dsp_tb_nail_state .= '<th>State Id</th>';
$v_dsp_tb_nail_state .= '<th>State Name</th>';
$v_dsp_tb_nail_state .= '<th>State Order</th>';
$v_dsp_tb_nail_state .= '<th>Country Id</th>';
$v_dsp_tb_nail_state .= '<th>Country Name</th>';
$v_dsp_tb_nail_state .= '<th>State Status</th>';
$v_dsp_tb_nail_state .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_state as $arr){
	$v_dsp_tb_nail_state .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_state .= '<td align="right">'.($v_count++).'</td>';
	$v_state_id = isset($arr['state_id'])?$arr['state_id']:0;
	$v_state_name = isset($arr['state_name'])?$arr['state_name']:'';
	$v_state_order = isset($arr['state_order'])?$arr['state_order']:0;
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_country_name = isset($arr['country_name'])?$arr['country_name']:'';
	$v_state_status = isset($arr['state_status'])?$arr['state_status']:0;
	$v_dsp_tb_nail_state .= '<td>'.$v_state_id.'</td>';
	$v_dsp_tb_nail_state .= '<td>'.$v_state_name.'</td>';
	$v_dsp_tb_nail_state .= '<td>'.$v_state_order.'</td>';
	$v_dsp_tb_nail_state .= '<td>'.$v_country_id.'</td>';
	$v_dsp_tb_nail_state .= '<td>'.$v_country_name.'</td>';
	$v_dsp_tb_nail_state .= '<td>'.$v_state_status.'</td>';
	$v_dsp_tb_nail_state .= '</tr>';
}
$v_dsp_tb_nail_state .= '</table>';
?>