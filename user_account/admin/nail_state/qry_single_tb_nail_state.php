<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_state_id = 0;
$v_state_name = '';
$v_state_order = 0;
$v_country_id = 0;
$v_country_name = '';
$v_state_status = 0;
$v_new_nail_state = true;
if(isset($_POST['btn_submit_tb_nail_state'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_state->set_mongo_id($v_mongo_id);
	$v_state_id = isset($_POST['txt_state_id'])?$_POST['txt_state_id']:$v_state_id;
	if(is_null($v_mongo_id)){
		$v_state_id = $cls_tb_nail_state->select_next('state_id');
	}
	$v_state_id = (int) $v_state_id;
	$cls_tb_nail_state->set_state_id($v_state_id);
	$v_state_name = isset($_POST['txt_state_name'])?$_POST['txt_state_name']:$v_state_name;
	$v_state_name = trim($v_state_name);
	if($v_state_name=='') $v_error_message .= '[State Name] is empty!<br />';
	$cls_tb_nail_state->set_state_name($v_state_name);
	$v_state_order = isset($_POST['txt_state_order'])?$_POST['txt_state_order']:$v_state_order;
	$v_state_order = (int) $v_state_order;
	if($v_state_order<0) $v_error_message .= '[State Order] is negative!<br />';
	$cls_tb_nail_state->set_state_order($v_state_order);
	$v_country_id = isset($_POST['txt_country_id'])?$_POST['txt_country_id']:$v_country_id;
	$v_country_id = (int) $v_country_id;
	if($v_country_id<0) $v_error_message .= '[Country Id] is negative!<br />';
	$cls_tb_nail_state->set_country_id($v_country_id);
	$v_country_name = isset($_POST['txt_country_name'])?$_POST['txt_country_name']:$v_country_name;
	$v_country_name = trim($v_country_name);
	if($v_country_name=='') $v_error_message .= '[Country Name] is empty!<br />';
	$cls_tb_nail_state->set_country_name($v_country_name);
	$v_state_status = isset($_POST['txt_state_status'])?$_POST['txt_state_status']:$v_state_status;
	$v_state_status = (int) $v_state_status;
	if($v_state_status<0) $v_error_message .= '[State Status] is negative!<br />';
	$cls_tb_nail_state->set_state_status($v_state_status);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_state->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_state->update(array('_id' => $v_mongo_id));
			$v_new_nail_state = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_state_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_state) $v_nail_state_id = 0;
		}
	}
}else{
	$v_state_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_state_id,'int');
	if($v_state_id>0){
		$v_row = $cls_tb_nail_state->select_one(array('state_id' => $v_state_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_state->get_mongo_id();
			$v_state_id = $cls_tb_nail_state->get_state_id();
			$v_state_name = $cls_tb_nail_state->get_state_name();
			$v_state_order = $cls_tb_nail_state->get_state_order();
			$v_country_id = $cls_tb_nail_state->get_country_id();
			$v_country_name = $cls_tb_nail_state->get_country_name();
			$v_state_status = $cls_tb_nail_state->get_state_status();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>