<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_province_id = 0;
$v_province_name = '';
$v_province_key = '';
$v_created_id = '';
$v_province_status = 0;
$v_province_order = 0;
$v_country_id = 15;
$v_new_province = true;
$v_user_mongo_id = $arr_user['mongo_id'];
if(isset($_POST['btn_submit_tb_province'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!=''){
        $v_mongo_id = new MongoID($v_mongo_id);
        $v_created_id = isset($_POST['txt_create_id'])?$_POST['txt_create_id']:'';
        $v_created_id = new MongoId($v_created_id);
        $cls_tb_province->set_modified_by($v_user_mongo_id);
        $cls_tb_province->set_create_by($v_created_id);
    } else{
        $v_mongo_id = NULL;
        $cls_tb_province->set_modified_by('');
        $cls_tb_province->set_create_by($v_user_mongo_id);
    }
	$cls_tb_province->set_mongo_id($v_mongo_id);
	$v_province_name = isset($_POST['txt_province_name'])?$_POST['txt_province_name']:$v_province_name;
	$v_province_name = trim($v_province_name);
	if($v_province_name=='') $v_error_message .= '[Province Name] is empty!<br />';
	$cls_tb_province->set_province_name($v_province_name);
	$v_province_key = isset($_POST['txt_province_key'])?$_POST['txt_province_key']:$v_province_key;
	$v_province_key = trim($v_province_key);
	if($v_province_key=='') $v_error_message .= '[Province Key] is empty!<br />';
	$cls_tb_province->set_province_key($v_province_key);
	$v_province_status = isset($_POST['txt_province_status'])?false:true;
	$cls_tb_province->set_province_status($v_province_status);
	$v_country_id = isset($_POST['txt_country_id'])?$_POST['txt_country_id']:$v_country_id;
	$v_country_id = (int) $v_country_id;
	if($v_country_id<0) $v_error_message .= '[Country Id] is negative!<br />';
	$cls_tb_province->set_country_id($v_country_id);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_province->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_province->update(array('_id' => $v_mongo_id));
			$v_new_province = false;
		}
		if($v_result){
			$_SESSION['ss_tb_province_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_province) $v_province_id = 0;
		}
	}
}else{
	$v_province_id= isset($_GET['id'])?$_GET['id']:'';
	if($v_province_id!=''){
		$v_row = $cls_tb_province->select_one(array('_id' => new MongoId($v_province_id)));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_province->get_mongo_id();
			$v_province_id = $cls_tb_province->get_province_id();
			$v_province_name = $cls_tb_province->get_province_name();
			$v_province_key = $cls_tb_province->get_province_key();
			$v_province_status = $cls_tb_province->get_province_status();
			$v_province_order = $cls_tb_province->get_province_order();
			$v_country_id = $cls_tb_province->get_country_id();
            $v_created_id = (string)$cls_tb_province->get_create_by();
		}
	}
}
$v_dsp_country = $cls_tb_country->draw_option("value","name",$v_country_id,array("deleted"=>false),array("name"=>1));
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
?>