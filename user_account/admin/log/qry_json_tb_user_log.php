<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:0;
settype($v_quick_search,"int");
if($v_quick_search >0){
    $v_ts = time() - $v_quick_search*3600*24;
    $arr_where_clause['log_datetime'] = array('$gte' => new MongoDate(strtotime(date("Y-m-d 00:00:00", $v_ts ))),
            '$lte' => new MongoDate(strtotime(date("Y-m-d 23:59:59", time()))));
}
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_user_log_redirect']) && $_SESSION['ss_tb_user_log_redirect']==1){
	if(isset($_SESSION['ss_tb_user_log_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_user_log_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_user_log_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_user_log_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_user_log_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_user_log->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_user_log_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_user_log_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_user_log_page'] = $v_page;
$_SESSION['ss_tb_user_log_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_user_log = $cls_tb_user_log->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_user_log as $arr){
	$v_log_id = isset($arr['log_id'])?$arr['log_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_password = isset($arr['user_password'])?$arr['user_password']:'';
	$v_log_ipaddress = isset($arr['log_ipaddress'])?$arr['log_ipaddress']:'';
	$v_log_url = isset($arr['log_url'])?$arr['log_url']:'';
	$v_log_datetime = isset($arr['log_datetime'])?$arr['log_datetime']:(new MongoDate(time()));
    $v_log_datetime = date("d-M-Y",$v_log_datetime->sec);
    $v_company_name = isset($arr['company_name'])?$arr['company_name']:'';
    $v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
    $arr_log_get = isset($arr['log_get'])?$arr['log_get']:'';
    $arr_log_post = isset($arr['log_post'])?$arr['log_post']:'';
    $arr_log_device = isset($arr['log_device'])?$arr['log_device']:array();
    $v_device = "Flat form:".$arr_log_device['user_flat_form']
        ." - Browser:".$arr_log_device['user_browser']." - ".$arr_log_device['user_version']
    ;
	$arr_ret_data[] = array(
		'row_order'=>++$v_row
		,'log_id' => $v_log_id
        ,'user_name' => $v_user_name
        ,'company_name' => $v_company_name
		,'log_ipaddress' => $v_log_ipaddress
		,'log_url' => $v_log_url
        ,'log_get' => $arr_log_get
        ,'log_post' => $arr_log_post
        ,'log_datetime' => $v_log_datetime
        ,'log_device' => $v_device
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_user_log'=>$arr_ret_data);
echo json_encode($arr_return);
?>