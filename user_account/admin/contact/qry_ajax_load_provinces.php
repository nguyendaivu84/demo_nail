<?php if(!isset($v_sval)) die();?>
<?php
add_class("cls_tb_province");
$cls_tb_province = new cls_tb_province($db);
$v_country_id = isset($_POST['txt_country_id'])?$_POST['txt_country_id']:0;
settype($v_country_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK', 'provinces'=>array());
$arr_all_province = array();
$arr_all_province[] = array('province_id'=>0, 'province_name'=>'--------');
if($v_country_id>0){
    $arr_provnce = $cls_tb_province->select(array('country_id'=>$v_country_id,"province_status"=>0));
    foreach($arr_provnce as $arr){
        $v_provnce_id = isset($arr['province_id'])?$arr['province_id']:0;
        $v_provnce_name = isset($arr['province_name'])?$arr['province_name']:'';
        $arr_all_province[] = array('province_id'=>$v_provnce_id,'province_name'=>$v_provnce_name);
    }
}
$arr_return['provinces'] = $arr_all_province;
echo json_encode($arr_return);
?>