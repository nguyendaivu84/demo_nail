<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_credit_card_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_credit_card_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_credit_card_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_credit_card = $cls_tb_nail_credit_card->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_nail_credit_card_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Nail_credit_card')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Nail_credit_card');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Cart Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Cart Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Cart Number', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Expiration Month', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Expiration Year', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'First Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Last Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Address Line 1', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Address Line 2', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'City Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Zip Code', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'State Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Country Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Phone', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Default Cart', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_nail_credit_card as $arr){
	$v_excel_col = 1;
	$v_cart_id = isset($arr['cart_id'])?$arr['cart_id']:0;
	$v_cart_type = isset($arr['cart_type'])?$arr['cart_type']:0;
	$v_cart_number = isset($arr['cart_number'])?$arr['cart_number']:'';
	$v_expiration_month = isset($arr['expiration_month'])?$arr['expiration_month']:0;
	$v_expiration_year = isset($arr['expiration_year'])?$arr['expiration_year']:0;
	$v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
	$v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
	$v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
	$v_city_name = isset($arr['city_name'])?$arr['city_name']:'';
	$v_zip_code = isset($arr['zip_code'])?$arr['zip_code']:'';
	$v_state_id = isset($arr['state_id'])?$arr['state_id']:0;
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_phone = isset($arr['phone'])?$arr['phone']:(new MongoDate(time()));
	$v_default_cart = isset($arr['default_cart'])?$arr['default_cart']:0;
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_cart_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_cart_type, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_cart_number, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_expiration_month, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_expiration_year, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_first_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_last_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_address_line_1, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_address_line_2, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_city_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_zip_code, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_state_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_country_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_phone, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_default_cart, 'right');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>