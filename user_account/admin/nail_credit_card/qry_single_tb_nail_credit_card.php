<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_cart_id = 0;
$v_cart_type = 0;
$v_cart_number = '';
$v_expiration_month = 0;
$v_expiration_year = 0;
$v_first_name = '';
$v_last_name = '';
$v_user_id = 0;
$v_address_line_1 = '';
$v_address_line_2 = '';
$v_city_name = '';
$v_zip_code = '';
$v_state_id = 0;
$v_country_id = 0;
$v_phone = date('Y-m-d H:i:s', time());
$v_default_cart = 0;
$v_new_nail_credit_card = true;
if(isset($_POST['btn_submit_tb_nail_credit_card'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_credit_card->set_mongo_id($v_mongo_id);
	$v_cart_id = isset($_POST['txt_cart_id'])?$_POST['txt_cart_id']:$v_cart_id;
	if(is_null($v_mongo_id)){
		$v_cart_id = $cls_tb_nail_credit_card->select_next('cart_id');
	}
	$v_cart_id = (int) $v_cart_id;
	$cls_tb_nail_credit_card->set_cart_id($v_cart_id);
	$v_cart_type = isset($_POST['txt_cart_type'])?$_POST['txt_cart_type']:$v_cart_type;
	$v_cart_type = (int) $v_cart_type;
	if($v_cart_type<0) $v_error_message .= '[Cart Type] is negative!<br />';
	$cls_tb_nail_credit_card->set_cart_type($v_cart_type);
	$v_cart_number = isset($_POST['txt_cart_number'])?$_POST['txt_cart_number']:$v_cart_number;
	$v_cart_number = trim($v_cart_number);
	if($v_cart_number=='') $v_error_message .= '[Cart Number] is empty!<br />';
	$cls_tb_nail_credit_card->set_cart_number($v_cart_number);
	$v_expiration_month = isset($_POST['txt_expiration_month'])?$_POST['txt_expiration_month']:$v_expiration_month;
	$v_expiration_month = (int) $v_expiration_month;
	if($v_expiration_month<0) $v_error_message .= '[Expiration Month] is negative!<br />';
	$cls_tb_nail_credit_card->set_expiration_month($v_expiration_month);
	$v_expiration_year = isset($_POST['txt_expiration_year'])?$_POST['txt_expiration_year']:$v_expiration_year;
	$v_expiration_year = (int) $v_expiration_year;
	if($v_expiration_year<0) $v_error_message .= '[Expiration Year] is negative!<br />';
	$cls_tb_nail_credit_card->set_expiration_year($v_expiration_year);
	$v_first_name = isset($_POST['txt_first_name'])?$_POST['txt_first_name']:$v_first_name;
	$v_first_name = trim($v_first_name);
	if($v_first_name=='') $v_error_message .= '[First Name] is empty!<br />';
	$cls_tb_nail_credit_card->set_first_name($v_first_name);
	$v_last_name = isset($_POST['txt_last_name'])?$_POST['txt_last_name']:$v_last_name;
	$v_last_name = trim($v_last_name);
	if($v_last_name=='') $v_error_message .= '[Last Name] is empty!<br />';
	$cls_tb_nail_credit_card->set_last_name($v_last_name);
	$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:$v_user_id;
	$v_user_id = (int) $v_user_id;
	if($v_user_id<0) $v_error_message .= '[User Id] is negative!<br />';
	$cls_tb_nail_credit_card->set_user_id($v_user_id);
	$v_address_line_1 = isset($_POST['txt_address_line_1'])?$_POST['txt_address_line_1']:$v_address_line_1;
	$v_address_line_1 = trim($v_address_line_1);
	if($v_address_line_1=='') $v_error_message .= '[Address Line 1] is empty!<br />';
	$cls_tb_nail_credit_card->set_address_line_1($v_address_line_1);
	$v_address_line_2 = isset($_POST['txt_address_line_2'])?$_POST['txt_address_line_2']:$v_address_line_2;
	$v_address_line_2 = trim($v_address_line_2);
	if($v_address_line_2=='') $v_error_message .= '[Address Line 2] is empty!<br />';
	$cls_tb_nail_credit_card->set_address_line_2($v_address_line_2);
	$v_city_name = isset($_POST['txt_city_name'])?$_POST['txt_city_name']:$v_city_name;
	$v_city_name = trim($v_city_name);
	if($v_city_name=='') $v_error_message .= '[City Name] is empty!<br />';
	$cls_tb_nail_credit_card->set_city_name($v_city_name);
	$v_zip_code = isset($_POST['txt_zip_code'])?$_POST['txt_zip_code']:$v_zip_code;
	$v_zip_code = trim($v_zip_code);
	if($v_zip_code=='') $v_error_message .= '[Zip Code] is empty!<br />';
	$cls_tb_nail_credit_card->set_zip_code($v_zip_code);
	$v_state_id = isset($_POST['txt_state_id'])?$_POST['txt_state_id']:$v_state_id;
	$v_state_id = (int) $v_state_id;
	if($v_state_id<0) $v_error_message .= '[State Id] is negative!<br />';
	$cls_tb_nail_credit_card->set_state_id($v_state_id);
	$v_country_id = isset($_POST['txt_country_id'])?$_POST['txt_country_id']:$v_country_id;
	$v_country_id = (int) $v_country_id;
	if($v_country_id<0) $v_error_message .= '[Country Id] is negative!<br />';
	$cls_tb_nail_credit_card->set_country_id($v_country_id);
	$v_phone = isset($_POST['txt_phone'])?$_POST['txt_phone']:$v_phone;
	if(!check_date($v_phone)) $v_error_message .= '[Phone] is invalid date/time!<br />';
	$cls_tb_nail_credit_card->set_phone($v_phone);
	$v_default_cart = isset($_POST['txt_default_cart'])?$_POST['txt_default_cart']:$v_default_cart;
	$v_default_cart = (int) $v_default_cart;
	if($v_default_cart<0) $v_error_message .= '[Default Cart] is negative!<br />';
	$cls_tb_nail_credit_card->set_default_cart($v_default_cart);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_credit_card->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_credit_card->update(array('_id' => $v_mongo_id));
			$v_new_nail_credit_card = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_credit_card_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_credit_card) $v_nail_credit_card_id = 0;
		}
	}
}else{
	$v_cart_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_cart_id,'int');
	if($v_cart_id>0){
		$v_row = $cls_tb_nail_credit_card->select_one(array('cart_id' => $v_cart_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_credit_card->get_mongo_id();
			$v_cart_id = $cls_tb_nail_credit_card->get_cart_id();
			$v_cart_type = $cls_tb_nail_credit_card->get_cart_type();
			$v_cart_number = $cls_tb_nail_credit_card->get_cart_number();
			$v_expiration_month = $cls_tb_nail_credit_card->get_expiration_month();
			$v_expiration_year = $cls_tb_nail_credit_card->get_expiration_year();
			$v_first_name = $cls_tb_nail_credit_card->get_first_name();
			$v_last_name = $cls_tb_nail_credit_card->get_last_name();
			$v_user_id = $cls_tb_nail_credit_card->get_user_id();
			$v_address_line_1 = $cls_tb_nail_credit_card->get_address_line_1();
			$v_address_line_2 = $cls_tb_nail_credit_card->get_address_line_2();
			$v_city_name = $cls_tb_nail_credit_card->get_city_name();
			$v_zip_code = $cls_tb_nail_credit_card->get_zip_code();
			$v_state_id = $cls_tb_nail_credit_card->get_state_id();
			$v_country_id = $cls_tb_nail_credit_card->get_country_id();
			$v_phone = date('Y-m-d H:i:s',$cls_tb_nail_credit_card->get_phone());
			$v_default_cart = $cls_tb_nail_credit_card->get_default_cart();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>