<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_credit_card_redirect']) && $_SESSION['ss_tb_nail_credit_card_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_credit_card_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_credit_card_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_credit_card_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_credit_card_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_credit_card_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_credit_card->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_credit_card_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_credit_card_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_credit_card_page'] = $v_page;
$_SESSION['ss_tb_nail_credit_card_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_credit_card = $cls_tb_nail_credit_card->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_credit_card as $arr){
	$v_cart_id = isset($arr['cart_id'])?$arr['cart_id']:0;
	$v_cart_type = isset($arr['cart_type'])?$arr['cart_type']:0;
	$v_cart_number = isset($arr['cart_number'])?$arr['cart_number']:'';
	$v_expiration_month = isset($arr['expiration_month'])?$arr['expiration_month']:0;
	$v_expiration_year = isset($arr['expiration_year'])?$arr['expiration_year']:0;
	$v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
	$v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
	$v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
	$v_city_name = isset($arr['city_name'])?$arr['city_name']:'';
	$v_zip_code = isset($arr['zip_code'])?$arr['zip_code']:'';
	$v_state_id = isset($arr['state_id'])?$arr['state_id']:0;
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_phone = isset($arr['phone'])?$arr['phone']:(new MongoDate(time()));
	$v_default_cart = isset($arr['default_cart'])?$arr['default_cart']:0;
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'cart_id' => $v_cart_id,
		'cart_type' => $v_cart_type,
		'cart_number' => $v_cart_number,
		'expiration_month' => $v_expiration_month,
		'expiration_year' => $v_expiration_year,
		'first_name' => $v_first_name,
		'last_name' => $v_last_name,
		'user_id' => $v_user_id,
		'address_line_1' => $v_address_line_1,
		'address_line_2' => $v_address_line_2,
		'city_name' => $v_city_name,
		'zip_code' => $v_zip_code,
		'state_id' => $v_state_id,
		'country_id' => $v_country_id,
		'phone' => $v_phone,
		'default_cart' => $v_default_cart
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_credit_card'=>$arr_ret_data);
echo json_encode($arr_return);
?>