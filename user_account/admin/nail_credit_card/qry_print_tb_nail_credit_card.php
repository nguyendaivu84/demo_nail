<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_credit_card_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_credit_card_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_credit_card_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_credit_card = $cls_tb_nail_credit_card->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_credit_card = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_credit_card .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_credit_card .= '<th>Ord</th>';
$v_dsp_tb_nail_credit_card .= '<th>Cart Id</th>';
$v_dsp_tb_nail_credit_card .= '<th>Cart Type</th>';
$v_dsp_tb_nail_credit_card .= '<th>Cart Number</th>';
$v_dsp_tb_nail_credit_card .= '<th>Expiration Month</th>';
$v_dsp_tb_nail_credit_card .= '<th>Expiration Year</th>';
$v_dsp_tb_nail_credit_card .= '<th>First Name</th>';
$v_dsp_tb_nail_credit_card .= '<th>Last Name</th>';
$v_dsp_tb_nail_credit_card .= '<th>User Id</th>';
$v_dsp_tb_nail_credit_card .= '<th>Address Line 1</th>';
$v_dsp_tb_nail_credit_card .= '<th>Address Line 2</th>';
$v_dsp_tb_nail_credit_card .= '<th>City Name</th>';
$v_dsp_tb_nail_credit_card .= '<th>Zip Code</th>';
$v_dsp_tb_nail_credit_card .= '<th>State Id</th>';
$v_dsp_tb_nail_credit_card .= '<th>Country Id</th>';
$v_dsp_tb_nail_credit_card .= '<th>Phone</th>';
$v_dsp_tb_nail_credit_card .= '<th>Default Cart</th>';
$v_dsp_tb_nail_credit_card .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_credit_card as $arr){
	$v_dsp_tb_nail_credit_card .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_credit_card .= '<td align="right">'.($v_count++).'</td>';
	$v_cart_id = isset($arr['cart_id'])?$arr['cart_id']:0;
	$v_cart_type = isset($arr['cart_type'])?$arr['cart_type']:0;
	$v_cart_number = isset($arr['cart_number'])?$arr['cart_number']:'';
	$v_expiration_month = isset($arr['expiration_month'])?$arr['expiration_month']:0;
	$v_expiration_year = isset($arr['expiration_year'])?$arr['expiration_year']:0;
	$v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
	$v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
	$v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
	$v_city_name = isset($arr['city_name'])?$arr['city_name']:'';
	$v_zip_code = isset($arr['zip_code'])?$arr['zip_code']:'';
	$v_state_id = isset($arr['state_id'])?$arr['state_id']:0;
	$v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
	$v_phone = isset($arr['phone'])?$arr['phone']:(new MongoDate(time()));
	$v_default_cart = isset($arr['default_cart'])?$arr['default_cart']:0;
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_cart_id.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_cart_type.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_cart_number.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_expiration_month.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_expiration_year.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_first_name.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_last_name.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_address_line_1.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_address_line_2.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_city_name.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_zip_code.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_state_id.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_country_id.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_phone.'</td>';
	$v_dsp_tb_nail_credit_card .= '<td>'.$v_default_cart.'</td>';
	$v_dsp_tb_nail_credit_card .= '</tr>';
}
$v_dsp_tb_nail_credit_card .= '</table>';
?>