<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_credit_card").click(function(e){
		var css = '';

		var cart_id = $("input#txt_cart_id").val();
		cart_id = parseInt(cart_id, 10);
		css = isNaN(cart_id)?'':'none';
		$("label#lbl_cart_id").css("display",css);
		if(css == '') return false;
		var cart_type = $("input#txt_cart_type").val();
		cart_type = parseInt(cart_type, 10);
		css = isNaN(cart_type)?'':'none';
		$("label#lbl_cart_type").css("display",css);
		if(css == '') return false;
		var cart_number = $("input#txt_cart_number").val();
		cart_number = $.trim(cart_number);
		css = cart_number==''?'':'none';
		$("label#lbl_cart_number").css("display",css);
		if(css == '') return false;
		var expiration_month = $("input#txt_expiration_month").val();
		expiration_month = parseInt(expiration_month, 10);
		css = isNaN(expiration_month)?'':'none';
		$("label#lbl_expiration_month").css("display",css);
		if(css == '') return false;
		var expiration_year = $("input#txt_expiration_year").val();
		expiration_year = parseInt(expiration_year, 10);
		css = isNaN(expiration_year)?'':'none';
		$("label#lbl_expiration_year").css("display",css);
		if(css == '') return false;
		var first_name = $("input#txt_first_name").val();
		first_name = $.trim(first_name);
		css = first_name==''?'':'none';
		$("label#lbl_first_name").css("display",css);
		if(css == '') return false;
		var last_name = $("input#txt_last_name").val();
		last_name = $.trim(last_name);
		css = last_name==''?'':'none';
		$("label#lbl_last_name").css("display",css);
		if(css == '') return false;
		var user_id = $("input#txt_user_id").val();
		user_id = parseInt(user_id, 10);
		css = isNaN(user_id)?'':'none';
		$("label#lbl_user_id").css("display",css);
		if(css == '') return false;
		var address_line_1 = $("input#txt_address_line_1").val();
		address_line_1 = $.trim(address_line_1);
		css = address_line_1==''?'':'none';
		$("label#lbl_address_line_1").css("display",css);
		if(css == '') return false;
		var address_line_2 = $("input#txt_address_line_2").val();
		address_line_2 = $.trim(address_line_2);
		css = address_line_2==''?'':'none';
		$("label#lbl_address_line_2").css("display",css);
		if(css == '') return false;
		var city_name = $("input#txt_city_name").val();
		city_name = $.trim(city_name);
		css = city_name==''?'':'none';
		$("label#lbl_city_name").css("display",css);
		if(css == '') return false;
		var zip_code = $("input#txt_zip_code").val();
		zip_code = $.trim(zip_code);
		css = zip_code==''?'':'none';
		$("label#lbl_zip_code").css("display",css);
		if(css == '') return false;
		var state_id = $("input#txt_state_id").val();
		state_id = parseInt(state_id, 10);
		css = isNaN(state_id)?'':'none';
		$("label#lbl_state_id").css("display",css);
		if(css == '') return false;
		var country_id = $("input#txt_country_id").val();
		country_id = parseInt(country_id, 10);
		css = isNaN(country_id)?'':'none';
		$("label#lbl_country_id").css("display",css);
		if(css == '') return false;
		var phone = $("input#txt_phone").val();
		css = check_date(phone)?'':'none';
		$("label#lbl_phone").css("display",css);
		if(css == '') return false;
		var default_cart = $("input#txt_default_cart").val();
		default_cart = parseInt(default_cart, 10);
		css = isNaN(default_cart)?'':'none';
		$("label#lbl_default_cart").css("display",css);
		if(css == '') return false;
		return true;
	});
	$('input#txt_phone').kendoDatePicker({format:"dd-MMM-yyyy"});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_nail_credit_card').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Nail_credit_card</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_nail_credit_card" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_cart_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_cart_id" name="txt_cart_id" value="<?php echo $v_cart_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>Cart Type</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_cart_type" name="txt_cart_type" value="<?php echo $v_cart_type;?>" /> <label id="lbl_cart_type" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Cart Number</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_cart_number" name="txt_cart_number" value="<?php echo $v_cart_number;?>" /> <label id="lbl_cart_number" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Expiration Month</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_expiration_month" name="txt_expiration_month" value="<?php echo $v_expiration_month;?>" /> <label id="lbl_expiration_month" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Expiration Year</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_expiration_year" name="txt_expiration_year" value="<?php echo $v_expiration_year;?>" /> <label id="lbl_expiration_year" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>First Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_first_name" name="txt_first_name" value="<?php echo $v_first_name;?>" /> <label id="lbl_first_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Last Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_last_name" name="txt_last_name" value="<?php echo $v_last_name;?>" /> <label id="lbl_last_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>User Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" /> <label id="lbl_user_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Address Line 1</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_address_line_1" name="txt_address_line_1" value="<?php echo $v_address_line_1;?>" /> <label id="lbl_address_line_1" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Address Line 2</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_address_line_2" name="txt_address_line_2" value="<?php echo $v_address_line_2;?>" /> <label id="lbl_address_line_2" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>City Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_city_name" name="txt_city_name" value="<?php echo $v_city_name;?>" /> <label id="lbl_city_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Zip Code</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_zip_code" name="txt_zip_code" value="<?php echo $v_zip_code;?>" /> <label id="lbl_zip_code" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>State Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_state_id" name="txt_state_id" value="<?php echo $v_state_id;?>" /> <label id="lbl_state_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Country Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_country_id" name="txt_country_id" value="<?php echo $v_country_id;?>" /> <label id="lbl_country_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Phone</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_phone" name="txt_phone" value="<?php echo $v_phone;?>" /> <label id="lbl_phone" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Default Cart</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_default_cart" name="txt_default_cart" value="<?php echo $v_default_cart;?>" /> <label id="lbl_default_cart" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_nail_credit_card" name="btn_submit_tb_nail_credit_card" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
