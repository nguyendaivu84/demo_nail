<?php if(!isset($v_sval)) die();
$v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:'';
$v_product_sku = isset($_POST['txt_product_sku'])?$_POST['txt_product_sku']:'';
$v_product_sku = trim($v_product_sku);
if($v_product_id<0) $v_product_id = 0;
$arr_return = array('error'=>0, 'message'=>'', 'data'=>'');

if($v_product_sku!=''){
    $arr_where = array();
    $arr_where['sku'] = $v_product_sku;
    if($v_product_id!='') $arr_where['_id'] = array('$ne'=>new MongoId($v_product_id));
    if($cls_tb_product->count($arr_where)>0){
        $arr_return['error'] = 1;
        $arr_return['message'] = 'Duplicate product sku!';
    }
    $arr_return['data'] = json_encode($arr_where);
}else{
    $arr_return['error'] = 100;
    $arr_where['message'] = 'Empty data!';
}
die(json_encode($arr_return));
?>