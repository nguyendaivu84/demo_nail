<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
var material_grid;
var product_list;
var duplicate_material = false;
// poup search
var popupResult = "Cancel";
var popupResultValue = "0";
var popupButtonType = "View";
var popupWindow;
var popupCallback;
function popupClose(BtnResult,value,buttontType){
    popupResult = BtnResult;
    popupResultValue = value;
    popupButtonType = buttontType;
    popupWindow.close();
};
function popuploseCallBack(e){
    popupWindow.unbind("close", popuploseCallBack);
    if (popupCallback !== null){
        popupCallback(popupResult,popupResultValue,popupButtonType);
    }
}
function popupOpen(Title,Message,Type,Buttons,theFunction,value,buttontype){
    popupResult = "Cancel";
    if (theFunction !== undefined){
        popupCallback = theFunction;
    } else {
        popupCallback = null;
    }
    popupWindow.bind("close", popuploseCallBack);
    popupWindow.title(Title);
    popupWindow.center();
    popupWindow.open();
}
function remove_image(_id){
    var list_product = $("#txt_hidden_product_id").val();
    var new_list = '';
    var list_product_arr = list_product.split(",");
    for(var i =0 ; i<list_product_arr.length;i++){
        if(list_product_arr[i] !=_id) {
            if(new_list=='') new_list = list_product_arr[i];
            else new_list +=","+ list_product_arr[i];
        }
    }
    $("#"+_id).remove();
    $("#txt_hidden_product_id").val(new_list);
}
function check_item(_id){
    var list_product = $("#txt_hidden_product_id").val();
    var list_product_arr = list_product.split(",");
    for(var i =0 ; i<list_product_arr.length;i++){
        if(list_product_arr[i] ==_id) {
            return false;
        }
    }
    return true;
}
function showData(){}
//
$(document).ready(function(){
    var product_list_data = <?php echo json_encode($arr_product_list); ?>;
    product_list = $('#product_list').kendoGrid({
        dataSource:{
            data: product_list_data,
            batch: true,
            schema:{
                model:{
                    fields:{
                        code: {editable:false}
                        ,sku: {editable:false}
                        ,name: {editable:false}
                        ,category: {editable:false}
                        ,price: {editable:false}
                    }
                }
            }
            ,pageSize:20
        },
        height: 420,
        scrollable: true,
        navigatable: true,
        pageable: {
            input: true,
            numeric: false
        }
        ,editable: true
        ,columns: [
            {field: "code", title:"Code", width: "30px"},
            {field: "sku", title:"Sku", width: "70px"},
            {field: "name", title:"Name", width: "70px"},
            {field: "category", title:"Category", width: "70px"},
            {field: "image_url", title: "Image", type:"string", width:"100px",filterable: false, sortable: false, template:'<p style="text-align:center; margin:0"><img src="#= image_url#" style="max-width:100px" /></p>'},
            {field: "price", title:"Price", width: "70px"}
            ,{ command: [{name:"Add" ,click: add_row, imageClass:"k-icon k-add"}], title: "&nbsp;", width: "50px" }
        ]
    }).data("kendoGrid");
    function add_row(e){
        e.preventDefault();
        var dataItem2 = product_list.dataItem($(e.currentTarget).closest("tr"));
        var _id = dataItem2.id;
        var _name = dataItem2.name;
        if(check_item(_id)) add_product_to_list(_name,_id);
        else{
            if(confirm("This product already exsited. Are you sure you want to add again ?")){
                add_product_to_list(_name,_id);
            }
        }
//        product_list.removeRow($(e.currentTarget).closest("tr"));
    }
    function add_product_to_list(name,_id){
        var list_id = $("#txt_hidden_product_id").val();
        var list_name = $("#txt_hidden_product_name").val();
        if(list_id=='' ){
            list_id = _id;
        }
        else {
            list_id +=","+_id;
        }
        if(isNaN(list_name) || list_name=='') list_name = name;
        else list_name +=","+name;
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type: 'POST',
            data:   {txt_list_id: list_id , txt_ajax_type: 'get_product_information'},
            beforeSend: function(){
            },
            success: function(data, status){
                if(data.error==0){
                    $("#txt_hidden_product_id").val(list_id);
                    $("#dsp_list_product").html(data.content);
                }
            }
            ,error:function(data){
                alert(data.responseText);
            }
        });
    }
    popupWindow = $("#popupWindow").kendoWindow({
        actions: ["Close"],
        draggable: true,
        modal: true,
        width: "900px",
        height: "500px",
        resizable: true,
        visible: false,
        title: "Confirm action"
    }).data("kendoWindow");
    var category_list = <?php echo json_encode($arr_category) ; ?>;
    $('#category_list').width(150).kendoComboBox({
        dataSource: category_list,
        dataTextField: "name",
        dataValueField: "_id"
    });
    function search_product_list(name,cat){
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type: 'POST',
            data:   {txt_keyword: name, txt_cat: cat, txt_ajax_type: 'search_product'},
            beforeSend: function(){
            },
            success: function(data, status){
                product_list.dataSource.data(data.product_list);
                product_list.refresh();
            }
            ,error:function(data){
                alert(data.responseText);
            }
        });
    }
    $("#category_list").on("change",function(){
        var keyword = $("#txt_name_product").val();
        var cat = $(this).val();
        search_product_list(keyword,cat);
    });
    $("#txt_name_product").on("focusout",function(){
        var keyword = $(this).val();
        var cat = $("#category_list").val();
        search_product_list(keyword,cat);
    });
    $("#txt_name_product").on("keypress",function(event){
        if(event.which == 13)  {
            var keyword = $(this).val();
            var cat = $("#category_list").val();
            search_product_list(keyword,cat);
        }
    });
    $('textarea#txt_product_name').kendoEditor({
        tools: [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "fontName",
            "fontSize",
            "foreColor",
            "backColor",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "insertUnorderedList",
            "insertOrderedList",
            "indent",
            "outdent",
            "formatBlock",
            "unlink",
            "subscript",
            "superscript",
            "viewHtml"
        ],
        encoded: false
    }).data("kendoEditor");

    var editor_over_option = $('textarea#txt_product_overview,textarea#txt_product_ingredient').kendoEditor({
        tools: [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "fontName",
            "fontSize",
            "foreColor",
            "backColor",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "insertUnorderedList",
            "insertOrderedList",
            "indent",
            "outdent",
            "formatBlock",
            "createLink",
            "unlink",
            "insertImage",
            "subscript",
            "superscript",
            "viewHtml"
        ],
        encoded: false,
        imageBrowser: {
            path:"/upload",
            dataType:'json',
            transport: {
                read: {
                    url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                    type: "POST",
                    dataType:'json',
                    data: {txt_ajax_type: 'list_images'}
                },
                destroy: {
                    url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                    type: "POST",
                    dataType:'json',
                    data: {txt_ajax_type: 'delete_image_folder'}
                },
                create: {
                    url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                    type: "POST",
                    dataType:'json',
                    data: {txt_ajax_type: 'create_image_folder'}
                },
                uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_product_id;?>/upload-image",
                thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_product_id;?>/thumb-image"
            }
        }
    }).data("kendoEditor");
    $("input#btn_submit_tb_product,input#btn_submit_tb_product2").click(function(e){
        if(!validator.validate()){
            e.preventDefault();
            if(tab_strip.select().index()!=0) tab_strip.select(0);
            return false;
        }
        return true;
    });
    $("#btn_search").on("click",function(){
        var index = $("#slt_about_brand").val();
        popupOpen("Add detail information","You have unsaved changes. Do you want to save before continuing?","hung",["Cancel","No","Yes"],showData,index,"Edit");
    });
    var data_banner = <?php echo json_encode($arr_all_banner);?>;
    $("#product_banner").width(300).kendoMultiSelect({
        dataSource: data_banner,
        dataTextField: "banner_name",
        dataValueField: "_id",
        maxSelectedItems:1
    }).data("kendoMultiSelect");
    var product_banner = $("#product_banner").data("kendoMultiSelect");
    product_banner.value(<?php echo json_encode($arr_product_banner);?>);
    $("img[rel=images_product]").click(function(){
        if(confirm("Do you want to delete this image ? ")){
            var product_images_id = $(this).attr('product_images_id');
            var product_id = '<?php echo $v_product_id; ?>';
            $.ajax({
                url	    :'<?php echo URL.$v_admin_key ?>/ajax',
                type	:'POST',
                data	:{txt_ajax_type:'product_images',txt_action:'delete-images', txt_product_id:product_id, txt_product_images_id:product_images_id},
                success: function(data, type){
                    var ret = $.parseJSON(data);
                    $('td#td_more_images').html('Loading..');
                    if(ret.error==0)
                        $('td#td_more_images').html(ret.data);
                    else
                        alert(ret.message);
                }
            });
        }
    });
    $("img[rel=images_product_slider]").click(function(){
        if(confirm("Do you want to delete this image ? ")){
            var product_images_id = $(this).attr('product_images_id');
            var product_id = '<?php echo $v_mongo_id; ?>';
            $.ajax({
                url	    :'<?php echo URL.$v_admin_key ?>/ajax',
                type	:'POST',
                data	:{txt_ajax_type:'product_images_slider',txt_action:'delete-images', txt_product_id:product_id, txt_product_images_id:product_images_id},
                success: function(data, type){
                    var ret = $.parseJSON(data);
                    if(ret.error==0){
                        $("#txt_hidden_slider").val(ret.data);
                        $('#div_image_slider_'+product_images_id).remove();
                    }
                    else
                        alert(ret.message);
                }
            });
        }
    });
    $("img[rel=images_product_grab]").click(function(){
        if(confirm("Do you want to delete this image ? ")){
            var product_images_id = $(this).attr('product_images_id');
            var product_id = '<?php echo $v_mongo_id; ?>';
            $.ajax({
                url	    :'<?php echo URL.$v_admin_key ?>/ajax',
                type	:'POST',
                data	:{txt_ajax_type:'product_grab',txt_action:'delete-images', txt_product_id:product_id, txt_product_images_id:product_images_id},
                success: function(data, type){
                    var ret = $.parseJSON(data);
                    if(ret.error==0){
                        $("#txt_hidden_product_grab").val(ret.data);
                        $('#div_image_grab_'+product_images_id).remove();
                    }
                    else
                        alert(ret.message);
                }
            });
        }
    });
    var validator = $('div.information').kendoValidator().data("kendoValidator");
    var tags = <?php echo json_encode($arr_all_tag);?>;
    $("#txt_tag").width(300).kendoMultiSelect({
        dataSource: tags,
        dataTextField: "name",
        dataValueField: "_id",
        maxSelectedItems:1
    });
    $("select#txt_product_status").width(200).kendoComboBox();
    $("select#txt_sold_by").width(200).kendoComboBox();
    var product_tag = $("#txt_tag").data("kendoMultiSelect");
    product_tag.value(<?php echo json_encode($arr_product_tag);?>);
    $("#txt_default_price,#txt_discount_price").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });

    $('input#txt_image_file').kendoUpload({
        multiple: false
        ,select: on_select
    });

    $('input#txt_image_slider').kendoUpload({
        multiple: true
    });
    $('input#txt_image_hd').kendoUpload({
        multiple: false
    });


    <?php if($v_product_id!=''){?>
    function on_upload(e){
        e.data = {txt_ajax_type: "upload_thumbnail", txt_product_id: '<?php echo $v_product_id;?>'};
    }
    <?php }?>
    function on_select(e){
        //alert(e.files[0].name);
    }
    $('input#txt_is_threshold').click(function(e) {
        var chk = $(this).prop('checked');
        if(chk){
            var old = $('input#txt_hidden_threshold').val();
            old = parseInt(old, 10);
            if(isNaN(old) || old<0) old = 0;
            thresholds.value(old);
            thresholds.enable(true);
        }else{
            thresholds.value([]);
            thresholds.enable(false);
        }
    });
    var tab_strip = $("#data_single_tab").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        }
    }).data("kendoTabStrip");
    var tooltip = $("span.tooltips").kendoTooltip({
        filter: 'a',
        width: 120,
        position: "top"
    }).data("kendoTooltip");


    $('input#txt_product_sku').focusout(function(){
        var v_old_product_sku = '<?php echo $v_product_sku; ?>';
        var v_new_product_sku = $.trim($(this).val());
        if((v_old_product_sku + v_new_product_sku)=='') return false;
        if(v_old_product_sku==v_new_product_sku) return false;
        if(v_new_product_sku==''){
            $(this).val('');
            $('input#txt_hidden_product_sku').val('N');
            validator.validate();
            return false;
        }
        var product_id = $('input#txt_mongo_id').val().toString();
        $.ajax({
            url	    :'<?php echo URL.$v_admin_key ?>/ajax',
            type	:'POST',
            data	:{txt_ajax_type:'product_sku',txt_product_sku:$.trim(v_new_product_sku), txt_product_id:product_id},
            beforeSend: function(){
                $('span#sp_product_sku').html('Checking!....');
            },
            success: function(data, type){
                var ret = $.parseJSON(data);
                $('span#sp_product_sku').html('');
                var val = ret.error==0?v_new_product_sku:'';
                $('input#txt_hidden_product_sku').val(val);
                validator.validate();
            }
        });
    });
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Product<?php if($v_product_id!='') echo ': '.$v_product_sku.' ('.$v_short_description.')';?></h3>
                    </div>
                    <form id="frm_tb_product" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_product_id.'/edit';?>" method="POST" enctype="multipart/form-data">
                        <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                        <input type="hidden" id="txt_product_id" name="txt_product_id" value="<?php echo $v_product_id;?>" />
                        <input type="hidden" id="txt_product_code_1111" name="txt_product_code_1111" value="<?php echo $v_code;?>" />
                        <input type="hidden" id="txt_hidden_product_id" name="txt_hidden_product_id" value="<?php echo $v_product_grab_list;?>" />
                        <input type="hidden" id="txt_hidden_product_grab_content" name="txt_hidden_product_grab_content" value="" />
                        <input type="hidden" id="txt_hidden_saved_dir" name="txt_hidden_saved_dir" value="<?php echo $v_image_saved_dir; ?>" />
                        <input type="hidden" id="txt_hidden_origin" name="txt_hidden_origin" value="<?php echo $v_image_origin; ?>" />
                        <div id="data_single_tab">
                            <ul>
                            <li title="menu" class="k-state-active">Information</li>
                            <li title="menu">Product description</li>
                            <li title="menu">Product Indredents</li>
                            <li title="menu">Image</li>
                            <li title="menu">Beauty Grab</li>
<!--                            <li title="menu">Material</li>-->
                            </ul>
                            <div class="information div_details">
                                <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">Product Sku</td>
                                    <td style="width:1px">&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_sku" name="txt_product_sku" value="<?php echo $v_product_sku;?>" required data-required-msg="Please input Product Sku" />
                                        <input type="hidden" style="width:0px !important; border:none" name="txt_hidden_product_sku" id="txt_hidden_product_sku" value="<?php echo $v_product_sku==''?'Y':'N';?>" required validationMessage="This Product Sku is existed!" />
                                        <span id="sp_product_sku"></span>
                                        <span class="tooltips"><a title="Product Sku is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
                                        <label id="lbl_product_sku" class="k-required">(*)Leave 'Auto' for auto generate</label>
                                    </td>
                                </tr>
                                    <tr align="right" valign="top">
                                        <td style="width:200px">Product Name</td>
                                        <td style="width:1px">&nbsp;</td>
                                        <td align="left">
                                            <!--input class="text_css k-textbox" size="50" type="text" id="txt_product_name" name="txt_product_name" value="<?php echo $v_product_name;?>" required data-required-msg="Please input Product Name" /-->
                                            <textarea cols="20" style="height:30px;" id="txt_product_name" name="txt_product_name" required data-required-msg="Please input Product Name">
                                                <?php echo $v_product_name;?>
                                            </textarea>
                                        </td>
                                    </tr>
                                <tr align="right" valign="top">
                                        <td>Description</td>
                                        <td>&nbsp;</td>
                                        <td align="left">
                                            <textarea class="text_css k-textbox" cols="40" style="height:80px" id="txt_product_detail" name="txt_product_detail">
                                                <?php echo $v_description;?></textarea>
                                            <label id="lbl_long_description" style="color:red;display:none;">(*)</label>
                                        </td>
                                    </tr>
                                <tr align="right" valign="top">
                                    <td style="width:200px">Category</td>
                                    <td style="width:1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_tag" name="txt_tag[]" multiple="multiple"> </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Product Banner</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="product_banner" name="product_banner[]" multiple="multiple">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Sold By</td>
                                    <td>&nbsp;</td>
                                    <td align="left" colspan="2">
                                        <select id="txt_sold_by" name="txt_sold_by">
                                            <?php echo $v_dsp_sold_by;?>
                                        </select>
                                        <label id="lbl_sold_by" style="color:red;display:none;">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Options</td>
                                    <td>&nbsp;</td>
                                    <td align="left" colspan="2">
                                       <input type="checkbox" id="txt_free_sample" name="txt_free_sample" <?php echo $v_free_sample==1 ? "checked" : ""; ?> /> Free sample ?
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Sell Price</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input type="text" id="txt_default_price" name="txt_default_price" value="<?php echo $v_default_price ;?>" /> <label id="lbl_default_price" style="color:red;display:none;">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Options</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="checkbox" id="exclusive" name="exclusive" <?php $v_exclusive==1?"checked":""; ?> /> Exclusive ?
                                        <input type="checkbox" id="online_only" name="online_only" <?php $v_online_only==1?"checked":""; ?> /> Online only ?
                                        <input type="checkbox" id="limited_edition" name="limited_edition" <?php $v_limited_edition==1?"checked":""; ?> /> Limited edition ?
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Discount Price</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input type="text" id="txt_discount_price" name="txt_discount_price" value="<?php echo $v_discount_price ;?>" /> <label id="lbl_default_price" style="color:red;display:none;">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left" colspan="2">
                                        <select id="txt_product_status" name="txt_product_status">
                                            <?php
                                            echo $v_dsp_product_status_draw;
                                            ?>
                                        </select>
                                        <label id="lbl_product_status" style="color:red;display:none;">(*)</label>
                                    </td>
                                </tr>
                            </table>
                    </div>
                            <div class="description div_details">
                                <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                    <tr align="right" valign="top">
                                        <td style="vertical-align: middle;padding:5px">
                                            <textarea id="txt_product_overview" name="txt_product_overview" style="width:98%; height:500px; padding:5px">
                                                <?php echo $v_product_specs;?>
                                            </textarea>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="indredients div_details">
                                <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                    <tr align="right" valign="top">
                                        <td style="vertical-align: middle;padding:5px">
                                            <textarea id="txt_product_ingredient" name="txt_product_ingredient" style="width:98%; height:500px; padding:5px">
                                                <?php echo $v_product_gallery;?>
                                            </textarea>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="image div_details">
                                <div id="window"></div>
                                <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                    <tr align="right" valign="top">
                                        <td style="width:200px">Thumbnail for Product</td>
                                        <td style="width:1px">&nbsp;</td>
                                        <td align="left">
                                            <input type="hidden" name="txt_hidden_image_thumb"  value="<?php echo $v_image_thumb;?>" />
                                            <input type="file" id="txt_image_file" name="txt_image_file" accept="image/*" />
                                            <?php
                                                echo $v_image_url!=''? $v_image_url :'';
                                            ?>
                                        </td>
                                    </tr>

                                    <tr align="right" valign="top">
                                        <td>Images slider</td>
                                        <td>&nbsp;</td>
                                        <td align="left">
                                            <input type="file" id="txt_image_slider" name="txt_image_slider[]" accept="image/*" />
                                            <input type="hidden" name="txt_hidden_slider" id="txt_hidden_slider"  value="<?php echo $v_image_slider;?>" />
                                            <?php
                                                echo $v_lst_image!=''? $v_lst_image :'';
                                            ?>
                                        </td>
                                    </tr>
                                    <tr align="right" valign="top">
                                        <td>Image Hd</td>
                                        <td>&nbsp;</td>
                                        <td align="left">
                                            <input type="hidden" name="txt_hidden_image_hd"  value="<?php echo $v_product_image_hd;?>" />
                                            <input type="file" id="txt_image_hd" name="txt_image_hd" accept="image/*" />
                                            <label id="lbl_file_hd" style="color:red;display:none;">(*)</label>
                                            <?php
                                                echo $v_product_image_hd_link!=''? $v_product_image_hd_link :'';
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="beauty_grab div_details">
                                <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                    <tr align="right" valign="top">
                                        <td style="width:200px">Product List</td>
                                        <td style="width:1px">&nbsp;</td>
                                        <td align="left">
                                            <input class="k-button" id="btn_search" value="Search" />
                                            <input type="hidden" name="txt_hidden_product_grab" id="txt_hidden_product_grab"  value="<?php echo $v_product_grab_list;?>" />
                                            <div id="dsp_list_product">
                                                <?php echo $v_product_grab_url; ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <!--div class="material div_details">
                                <div id="size_option">
                                    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                        <tr align="right" valign="top">
                                            <td style="width:200px">Size Option</td>
                                            <td align="left">
                                                <label><input type="checkbox" id="txt_size_option" name="txt_size_option" value="1"<?php echo $v_size_option==1?' checked="checked"':'';?> /> Allow customize</label>

                                            </td>
                                        </tr>
                                        <tr align="right" valign="top">
                                            <td style="width:200px">Maintain Aspect Ration</td>
                                            <td align="left">
                                                <label></label><input type="checkbox" id="is_use_function" name="is_use_function" value="1"<?php echo $v_use_function==1?' checked="checked"':'';?> /> Use maintain Aspect Ration for this product</label>

                                            </td>
                                        </tr>
                                        <tr align="right" valign="top">
                                            <td>Print's Type</td>
                                            <td align="left" colspan="2"><label>
                                                    <input type="radio" value="0" id="txt_print_type" name="txt_print_type"<?php echo $v_print_type==0?' checked="checked"':'';?> /> <?php echo $cls_settings->get_option_name_by_id('print_type',0);?></label> /
                                                <label><input type="radio" value="1" id="txt_print_type" name="txt_print_type"<?php echo $v_print_type==1?' checked="checked"':'';?> /> <?php echo $cls_settings->get_option_name_by_id('print_type',1);?></label> /
                                                <label><input type="radio" value="2" id="txt_print_type" name="txt_print_type"<?php echo $v_print_type==2?' checked="checked"':'';?> /> <?php echo $cls_settings->get_option_name_by_id('print_type',2);?></label>
                                            </td>
                                        </tr>
                                        <tr align="right" valign="top">
                                            <td>
                                                Material<br /><br /><br />

                                            </td>
                                            <td align="left" colspan="2">
                                                <div class="grid_table">
                                                    <div class="fielddis">
                                                        <label>Width:</label>
                                                        <input type="text" id="txt_size_width" name="txt_size_width" value="0" />
                                                    </div>
                                                    <div class="fielddis">
                                                        <label>Length:</label>
                                                        <input type="text" id="txt_size_length" name="txt_size_length" value="0" />
                                                    </div>
                                                    <div class="fielddis">
                                                        <label>Size Unit</label>
                                                        <select id="txt_size_unit" name="txt_size_unit">
                                                            <?php echo $v_dsp_size_unit_draw;?>
                                                        </select>
                                                    </div>
                                                    <div class="fielddis dismaterial">
                                                        <label for="txt_material_id">Material: </label>
                                                        <select id="txt_material_id" name="txt_material_id">
                                                            <option value="0" selected="selected">-------</option>
                                                            <?php echo $v_dsp_material_draw;?>
                                                        </select>
                                                    </div>
                                                    <div class="fielddis">
                                                        <label for="txt_thick">Thickness:</label>
                                                        <select id="txt_thick" name="txt_thick">
                                                            <option value="0" selected="selected">----</option>
                                                        </select>
                                                    </div>
                                                    <div class="fielddis">
                                                        <label>Thickness Unit:</label>
                                                        <select id="txt_thick_unit" name="txt_thick_unit">
                                                            <option value="" selected="selected">----</option>
                                                        </select>
                                                    </div>
                                                    <div class="fielddis">
                                                        <label for="txt_color">Color: </label>
                                                        <span id="sp_color">
                                                            <select id="txt_color" name="txt_color">
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div class="fielddis">
                                                        <label>&nbsp;</label>
                                                         <input type="checkbox" id="txt_two_sided" name="txt_two_sided" disabled="disabled" />
                                                         Allow two-sided print?
                                                    </div>
                                                    <div class="fielddis">
                                                        <label for="txt_price">Price (<?php echo $v_sign_money;?>):</label>
                                                        <input type="text" value="" id="txt_price" name="txt_price" />
                                                    </div>
                                                    <div class="fielddis">
                                                        <label for="txt_allow_size_option">Allow size option </label>
                                                        <input type="checkbox" value="1" id="txt_allow_size_option" name="txt_allow_size_option" />
                                                    </div>
                                                    <div style="clear:both" class="fielddis use_size_option">
                                                        <label for="txt_custome_size_price" >Custome price </label>
                                                        <input type="text"  id="txt_custome_size_price" name="txt_custome_size_price" value="0" style="width:100px"  />
                                                        <span style="color: red">(* This price use for custome size/square feet)</span>
                                                    </div>
                                                     <div class="fielddis" style="clear:both;">
                                                        <label for="txt_allow_size_option"> Add </label>
                                                        <img id="add_material" src="images/icons/add-icon.png" style="cursor:pointer" title="Add Material" width="25" />
                                                    </div>
                                                </div>
                                                <div id="product_material">
                                                    <?php echo $v_dsp_material_list;?>
                                                </div>
                                                <input type="hidden" id="txt_product_material" name="txt_product_material" />

                                            </td>
                                        </tr>
                                        <tr align="right" valign="top" class="use_size_option">
                                            <td>Size limit</td>
                                            <td align="left" colspan="2">
                                                <div class="fielddis">
                                                    <label for="txt_thick">Min width:</label>
                                                    <input type="text" id="txt_min_width" name="txt_min_width" value="0" />
                                                </div>
                                                <div class="fielddis">
                                                    <label for="txt_thick">Max width:</label>
                                                    <input type="text" id="txt_max_width" name="txt_max_width" value="0" />
                                                </div>
                                                <div class="fielddis" style="clear:both">
                                                    <label for="txt_thick">Min height:</label>
                                                    <input type="text" id="txt_min_height" name="txt_min_height" value="0" />
                                                </div>
                                                <div class="fielddis">
                                                    <label for="txt_thick">Max height:</label>
                                                    <input type="text" id="txt_max_height" name="txt_max_height" value="0" />
                                                </div>
                                                <div class="fielddis" style="clear: both">
                                                    <label for="txt_thick">Minimun material price:</label>
                                                    <input disabled="disabled" type="text" id="txt_minimum_material_price" name="txt_minimum_material_price" value="0" />
                                                    <span style="color: red">(*) Not implemented</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="material_memory">
                                    <ul>
                                        <li>Click icon <img src="images/icons/add.png"" /> to bring information to the list after completing all the necessary data.</li>
                                        <li>Information material will be stored temporarily in the list below.</li>
                                        <li>Can be fixed quickly following columns: Width, Length, Size's Unit, Size Option, 2-Sided Print and Price (by clicking on them)</li>
                                    </ul>
                                    <input type="submit" id="btn_submit_tb_product2" name="btn_submit_tb_product" value="Save All" class="k-button button_css" style=" margin:0px 0px 10px 0;display:table;" />
                                </div>
                                <div id="material_grid"></div>
                        </div-->
                        </div>
                       <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                                <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                            <div class="k-block k-widget div_buttons">
                               <input type="submit" id="btn_submit_tb_product" name="btn_submit_tb_product" value="Submit" class="k-button button_css" />
                           </div>
                    <?php }?>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div id="popupWindow">
    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
        <tr align="right" valign="top">
            <td style="width: 200px">Filter list below: Name/Code/SKU </td>
            <td align="left">
                <input style="width:300px" class="k-textbox" type="text" id="txt_name_product" name="txt_name_product" />
                Category <select id="category_list" name="category_list">
                </select>
                <input class="k-button" id="btn_add_prododuct" value="Add" />
            </td>
        </tr>
    </table>
    <div style="margin-top:20px" id="product_list">
    </div>
</div>