<?php
if(!isset($v_sval)) die();

add_class('cls_tb_design_template');
$cls_template = new cls_tb_design_template($db, LOG_DIR);

$arr_templates = $cls_template->select(array('template_status'=>0));

$arr_template = array();
$arr_folding = array();
$arr_direction = array();
$arr_die_cut = array();

foreach($arr_templates as $arr){
    $v_template_id = $arr['template_id'];
    $v_template_name = $arr['template_name'];
    $v_template_width = $arr['template_width'];
    $v_template_height = $arr['template_height'];
    $v_saved_dir = $arr['saved_dir'];
    $v_sample_image = $arr['sample_image'];
    $v_folding_type = $arr['folding_type'];
    $v_folding_direction = $arr['folding_direction'];
    $v_die_cut_type = $arr['die_cut_type'];

    if(!isset($arr_folding[$v_folding_type])) $arr_folding[$v_folding_type] = $cls_settings->get_option_name_by_id('folding_type', $v_folding_type,'');
    if(!isset($arr_direction[$v_folding_direction])) $arr_direction[$v_folding_direction] = $cls_settings->get_option_name_by_id('folding_direction', $v_folding_direction,'');
    if(!isset($arr_die_cut[$v_die_cut_type])) $arr_die_cut[$v_die_cut_type] = $cls_settings->get_option_name_by_id('die_cut_type', $v_die_cut_type,'');

    $arr_template[] = array(
        'template_id'=>$v_template_id
        ,'template_name'=>$v_template_name
        ,'template_image'=>$v_saved_dir.$v_sample_image
        ,'description'=>$v_template_width.'" &times; '.$v_template_height.'" '.$arr_folding[$v_folding_type].' '.$arr_direction[$v_folding_direction]
    );
}
header('Content-Type: application/json');
echo json_encode($arr_template);