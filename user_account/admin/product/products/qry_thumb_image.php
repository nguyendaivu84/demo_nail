<?php
if(!isset($v_sval)) die();
$v_dir = ROOT_DIR.DS;

$v_uri = $_SERVER['REQUEST_URI'];

$v_search = '?path=';
$v_pos = strpos($v_uri, $v_search);


if($v_pos!==false){
    $v_image_file = substr($v_uri, $v_pos + strlen($v_search));
    if(file_exists($v_dir.$v_image_file) && is_file($v_dir.$v_image_file)){
        $v_file = $v_dir.$v_image_file;
        list($width, $height, $type) = @getimagesize($v_file);
        $v_thumb_width = 150;
        $v_percent = $v_thumb_width / $width;
        $v_thumb_height = floor($height * $v_percent);
        switch($type){
            case 1;//Gif
                $im_source = @imagecreatefromgif($v_file);
                break;
            case 2;//Jpg
                $im_source = @imagecreatefromjpeg($v_file);
                break;
            case 3;//Png
                $im_source = @imagecreatefrompng($v_file);
                break;
            default ;
                $im_source = @imagecreatefromjpeg($v_file);
                break;
        }
        $tmp_image = imagecreatetruecolor( $v_thumb_width, $v_thumb_height );
        imagecopyresized($tmp_image, $im_source, 0, 0, 0, 0, $v_thumb_width, $v_thumb_height, $width, $height );

        /* Output the image with headers */
        header("Content-Type: image/jpeg");
        imagejpeg($tmp_image);
        imagedestroy($tmp_image);
        imagedestroy($im_source);
    }echo null;
}echo null;
?>