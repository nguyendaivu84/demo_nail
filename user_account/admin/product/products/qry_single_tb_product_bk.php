<?php if(!isset($v_sval)) die();
add_class("cls_tb_banner");
$cls_tb_banner = new cls_tb_banner($db);
add_class('clsupload');
$cls_upload = new clsupload;
add_class('cls_tb_product');
$cls_tb_product = new cls_tb_product($db, LOG_DIR);
$cls_temp = new cls_tb_product($db, LOG_DIR);
$arr_all_banner = array();
$arr_product_list = array();
$arr_product_banner = array();
$v_product_overview = '';
$v_lst_image = '';
$v_product_gallery = '';
$v_product_ingredient = '';
$v_description = '';
$v_product_grab_list = '';
$v_product_image_hd = '';
$v_product_image_hd_link = '';
$v_slugger_list = '';
$v_free_sample = 0;
$v_product_category_id = '';
$v_product_specs = '';
$v_error_message = '';
$v_mongo_id = NULL;
$v_product_id = '';
$v_product_sku = '';
$v_image_slider = '';
$v_short_description = '';
$v_image_choose = '';
$v_image_url = '';
$v_product_grab = '';
$v_product_grab_url = '';
$v_image_thumb = '';
$v_image_option = '';
$v_image_origin= '';
$v_image_saved_dir= '';
$arr_product_tag  = array();
$arr_list_image_slider  = array();
$v_num_images = 1;
$arr_product_grab_content = array();
$v_image_file = '';
$v_image_desc='';
$v_saved_dir='';
$v_sold_by = 0;
$v_code = 0;
$v_default_price = 0;
$v_product_status = 0;
$v_page = isset($_REQUEST['txt_page']) ? $_REQUEST['txt_page'] : 1;
$v_date_created = date('Y-m-d H:i:s');
$v_edit = false;
$v_total_product_images  = 0;
$v_new_product = true;
if(isset($_POST['btn_submit_tb_product'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    $v_edit = $v_mongo_id!='' ? true:$v_mongo_id;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_product->set_mongo_id($v_mongo_id);
    $v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:$v_product_id;
    $v_tmp_product_id = $v_product_id;
    $v_product_id = $cls_tb_product->select_next("product_id");
    $v_code = $cls_tb_product->select_next("code");
    $cls_tb_product->set_code($v_code);
    if(is_null($v_mongo_id)){
        $v_product_id = $cls_tb_product->select_next('product_id');
        $v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
        $v_user_type = isset($arr_user['user_type'])?$arr_user['user_type']:0;
        settype($v_user_type, 'int');
        $v_date_created = time();
    }
    $cls_tb_product->set_product_id($v_product_id);
    $v_product_sku = isset($_POST['txt_product_sku'])?$_POST['txt_product_sku']:$v_product_sku;
    $v_product_sku = trim($v_product_sku);
    if($v_product_sku=='')
        $v_error_message .= 'Product Sku is empty!<br />';
    else{
        if($cls_tb_product->count(array('sku'=>$v_product_sku, '_id'=>array('$ne'=>new MongoId($v_mongo_id))))>0) $v_error_message.='+ Duplicate Product Sku<br />';
    }
    $v_slugger = str_replace(" ","-",$v_product_sku);
    $v_slugger = str_replace("-","-",$v_product_sku);
    $v_slugger = remove_invalid_char($v_slugger);
    $v_slugger .="-".$v_code;
    $cls_tb_product->set_slugger($v_slugger);
    $cls_tb_product->set_sku($v_product_sku);
    $v_short_description = isset($_POST['txt_product_detail'])?$_POST['txt_product_detail']:$v_short_description;
    $v_short_description = trim($v_short_description);
    $cls_tb_product->set_description($v_short_description);
    //
    $v_product_description = isset($_POST['txt_product_overview'])?$_POST['txt_product_overview']:$v_product_overview;
    $v_product_ingredient= isset($_POST['txt_product_ingredient'])?$_POST['txt_product_ingredient']:$v_product_ingredient;
    $v_product_description = trim($v_product_description);
    $v_product_ingredient = trim($v_product_ingredient);
    $v_product_description = htmlspecialchars($v_product_description);
    $v_product_ingredient = htmlspecialchars($v_product_ingredient);
    $cls_tb_product->set_product_decription($v_product_description);
    $cls_tb_product->set_product_indgridient($v_product_ingredient);
    $v_product_category_id = isset($_POST['txt_tag'])?$_POST['txt_tag']:$v_product_category_id;
    $v_product_category_id = isset($v_product_category_id[0]) ? $v_product_category_id[0] : '';
    $cls_tb_product->set_category_id($v_product_category_id);
    $cls_tb_product->set_category($cls_tb_tag->select_scalar("name",array("_id"=>new MongoId($v_product_category_id))));
    $arr_product_banner = isset($_POST['product_banner'])?$_POST['product_banner']:$arr_product_banner;
    $arr_product_banner = isset($arr_product_banner[0]) ? $arr_product_banner[0] : '';
    $cls_tb_product->set_brand_id($arr_product_banner);
    $v_free_sample = isset($_POST['txt_free_sample'])?1:0;
    $cls_tb_product->set_is_free_sample($v_free_sample);
    $cls_upload->set_allow_overwrite(1);
    $v_upload_dir = PRODUCT_IMAGE_DIR_2;
    $v_upload_dir .= date('Y')."_".date('m');
//    $v_upload_dir = "upload_".date('Y');
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
    if(!$v_allow_upload) @mkdir($v_upload_dir);
    $v_has_upload = false;
    //upload thumb image
    if($v_allow_upload && isset($_FILES['txt_image_file']) ){
        $cls_upload->set_destination_dir($v_upload_dir);
        $cls_upload->set_field_name('txt_image_file');
        $cls_upload->set_allow_array_extension(array('jpg', 'png'));
        $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
        $cls_upload->upload_process();
        $v_tmp_image_file = '';
        if($cls_upload->get_error_number()==0){
            $v_image_file = $cls_upload->get_filename();
            $v_image_origin = $v_image_file;
            $v_image_desc = 'Image for current product';
            $v_width = PRODUCT_IMAGE_THUMB_FRONT;
            list($width, $height) = @getimagesize($v_upload_dir.DS.$v_image_file);
            images_resize_by_height_width($v_width, $v_upload_dir.DS.$v_image_file,$v_upload_dir.DS.$v_width.'_'.$v_image_file );

            if($v_tmp_image_file!='') $v_image_file = $v_tmp_image_file;
            $v_has_upload = true;
            $v_image_file ="upload/".date('Y')."_".date("m")."/".$v_image_file;
            $v_saved_dir = str_replace(ROOT_DIR.DS, '', $v_upload_dir);
            $v_saved_dir = str_replace(DS, '/', $v_saved_dir).'/';
            $cls_tb_product->set_saved_dir($v_saved_dir);
            $v_image_saved_dir = $v_saved_dir;
        }
    }
    if($v_image_origin==''){
        $v_image_saved_dir = isset($_POST['txt_hidden_saved_dir'])?$_POST['txt_hidden_saved_dir']:$v_image_saved_dir;
        $v_image_origin = isset($_POST['txt_hidden_origin'])?$_POST['txt_hidden_origin']:$v_image_origin;
    }
    $cls_tb_product->set_saved_dir($v_image_saved_dir);
    $cls_tb_product->set_url_image_link_origin($v_image_origin);
    if($v_image_file=='') $v_image_file = isset($_POST['txt_hidden_image_thumb'])?$_POST['txt_hidden_image_thumb']:$v_image_file; ;
    $cls_tb_product->set_products_upload($v_image_file);
    $v_has_upload = false;
    // upload hd image
    if($v_allow_upload && isset($_FILES['txt_image_hd']) ){
        $cls_upload->set_destination_dir($v_upload_dir);
        $cls_upload->set_field_name('txt_image_hd');
        $cls_upload->set_allow_array_extension(array('jpg', 'png'));
        $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
        $cls_upload->upload_process();
        $v_tmp_image_file = '';
        if($cls_upload->get_error_number()==0){
            $v_image_file = $cls_upload->get_filename();
            $v_image_desc = 'Image for current product';
            $v_width = 0;
            list($width, $height) = @getimagesize($v_upload_dir.DS.$v_image_file);
            if($v_tmp_image_file!='') $v_image_file = $v_tmp_image_file;
            $v_has_upload = true;
//            $v_product_image_hd ="upload/upload_".date('Y')."/".$v_image_file;
            $v_product_image_hd ="upload/".date('Y')."_".date("m")."/".$v_image_file;
        }
    }
    if($v_product_image_hd=='') $v_product_image_hd = isset($_POST['txt_hidden_image_hd'])?$_POST['txt_hidden_image_hd']:$v_product_image_hd; ;
    $cls_tb_product->set_product_big_image($v_product_image_hd);
    //echo $v_product_image_hd;

    // upload image slider
    if(isset($_FILES['txt_image_slider'])){
        $v_image_file = '';
        foreach($_FILES['txt_image_slider']['tmp_name'] as $v_key => $v_tmp_name ){
            $v_file_name = $_FILES['txt_image_slider']['name'][$v_key];
            $v_file_size =$_FILES['txt_image_slider']['size'][$v_key];
            $v_file_tmp =$_FILES['txt_image_slider']['tmp_name'][$v_key];
            $v_file_type=$_FILES['txt_image_slider']['type'][$v_key];
            /* Upload images */
            $v_width = 0;
            if($v_file_size >0){
                $arr_image = explode('.', $v_file_name);
                $v_file_type = $arr_image[sizeof($arr_image)-1];
                $v_file_name = $arr_image[0];
                $v_file_name = remove_invalid_char($v_file_name);
                $v_new_file_name =$v_file_name .".".$v_file_type;

                if(!move_uploaded_file($v_file_tmp,$v_upload_dir.DS. $v_new_file_name)){
                    $v_error_message .="Can't upload images ". $v_new_file_name.'<br>';
                }
                else{
                    list($width, $height) = @getimagesize($v_upload_dir.DS.$v_new_file_name);
//                    $arr_list_image_slider [] = "upload/upload_".date("Y")."/".$v_new_file_name;
                    $arr_list_image_slider [] = "upload/".date('Y')."_".date("m")."/".$v_image_file;
                }
            }
        }
    }
    if(empty($arr_list_image_slider)){
        $v_image_slider = isset($_POST['txt_hidden_slider'])?$_POST['txt_hidden_slider']:'';
        $arr_image_slider = explode(",",$v_image_slider);
        for($i=0;$i<count($arr_image_slider);$i++){
            $arr_list_image_slider [] = $arr_image_slider[$i];
        }
    }
    $cls_tb_product->set_product_slide_image($arr_list_image_slider);
    $v_sold_by = isset($_POST['txt_sold_by'])?$_POST['txt_sold_by']:$v_sold_by;
    if($v_sold_by==1) $v_sold_by='Unit'; else $v_sold_by = 'Square feet' ;
    $cls_tb_product->set_sell_by($v_sold_by);
    $v_default_price = isset($_POST['txt_default_price'])?$_POST['txt_default_price']:$v_default_price;
    $v_default_price = (float) $v_default_price;
    if($v_default_price<0) $v_error_message .= 'Default Price is negative!<br />';
    $cls_tb_product->set_sell_price($v_default_price);
    $v_product_status = isset($_POST['txt_product_status'])?$_POST['txt_product_status']:$v_product_status;
    $v_product_status = $v_product_status==3 ? true : false;
    $cls_tb_product->set_deleted($v_product_status);
    $cls_tb_product->set_status($v_product_status);
    $v_package_quantity = 1;
    $v_allow_single = 1;
    $v_product_grab = isset($_POST['txt_hidden_product_id'])?$_POST['txt_hidden_product_id']:'';
    if($v_product_grab=='') $v_product_grab = isset($_POST['txt_hidden_product_grab'])?$_POST['txt_hidden_product_grab']:'';
    $arr_product_grab = explode(",",$v_product_grab);
    for($i=0;$i<count($arr_product_grab);$i++){
        $v_select_one = $cls_temp->select_one(array("_id"=>new MongoId($arr_product_grab[$i])));
        if($v_select_one){
            $arr_product_grab_content[] = array(
                '_id'=>(string)$cls_temp->get_mongo_id()
                ,'sku'=>$cls_temp->get_sku()
                ,'name'=>$cls_temp->get_name()
                ,'product_upload'=>$cls_temp->get_products_upload()
                ,'sell_price'=>$cls_temp->get_sell_price()
                ,'slugger'=>$cls_temp->get_slugger()
                ,'image_hd'=>$cls_temp->get_product_big_image()
            );
        }
    }
    $cls_tb_product->set_product_grab($arr_product_grab_content);
    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_product->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_product->update(array('_id' => $v_mongo_id));
            $v_new_product = false;
        }
        if($v_result) {
            $_SESSION['ss_tb_product_redirect'] = 1;
            redir(URL.$v_admin_key);
        }else{
            if($v_new_product) $v_product_id = 0;
        }
    }
}else{
    $cls_tb_company = new cls_tb_company($db,LOG_DIR);
    $v_product_id = isset($_GET['id'])?$_GET['id']:'';
    if($v_product_id!=''){
        $v_row = $cls_tb_product->select_one(array('_id' => new MongoId($v_product_id)));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_product->get_mongo_id();
            $v_product_id = $cls_tb_product->get_product_id();
            $v_product_sku = $cls_tb_product->get_sku();
            $v_description = $cls_tb_product->get_description();
            $v_free_sample = $cls_tb_product->get_is_free_sample();
            $v_product_upload = $cls_tb_product->get_products_upload();
            $v_sold_by = $cls_tb_product->get_sell_by();
            $v_default_price = $cls_tb_product->get_sell_price();
            $arr_product_tag [] = $cls_tb_product->get_category_id();
            $v_product_status = $cls_tb_product->get_deleted();
            $v_product_status = $v_product_status?3:1;
            $v_product_category_id = $cls_tb_product->get_category_id();
            $v_code = $cls_tb_product->get_code();
            $v_short_description = $cls_tb_product->get_description();
            $v_product_brand_id = $cls_tb_product->get_brand_id();
            $arr_product_banner [] = $v_product_brand_id;
            $v_product_overview = $cls_tb_product->get_description();
            $v_product_specs = $cls_tb_product->get_product_decription();
            $v_product_gallery = $cls_tb_product->get_product_indgridient();
            $v_product_option = '';
            $v_image_saved_dir = $cls_tb_product->get_saved_dir();
            $v_image_origin = $cls_tb_product->get_url_image_link_origin();
            $v_image_thumb = $cls_tb_product->get_products_upload();
            if(checkRemoteFile(URL.$v_image_thumb)) $v_image_url = URL.$v_image_thumb;
            else $v_image_url = JT_URL.$v_image_thumb;
            $v_image_url = '<img style="max-height:200px" src="'.$v_image_url.'" title="Product Image thumb" />';
            $v_product_image_hd = $cls_tb_product->get_product_big_image();
            if(checkRemoteFile(URL.$v_product_image_hd)) $v_product_image_hd_link = URL.$v_product_image_hd;
            else $v_product_image_hd_link = JT_URL.$v_product_image_hd;
            $v_product_image_hd_link = '<img style="max-height:200px" src="'.$v_product_image_hd_link.'" title="Image hd" />';

            $arr_product_grab = $cls_tb_product->get_product_grab();
            for($i=0;$i<count($arr_product_grab);$i++){
                $v_id = $arr_product_grab[$i]['_id'];
                $v_product_grab_list .= $v_product_grab_list=='' ? $v_id."," :($i== count($arr_product_grab)-1 ? $v_id : $v_id.",");
                $v_image_temp = $arr_product_grab[$i]['product_upload'];
                if(is_admin() || $v_edit_right){
                    if(checkRemoteFile(URL.$v_image_temp)){
                        $v_product_grab_url .= '<div id="div_image_grab_'.$i.'" style="float:left; padding-right:10px;" class="img_more_products" ><p> <img rel="images_product_grab" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img style="max-width:150px;"  src="'. URL.$v_image_temp . '">
                                     </div> ' ;
                    }else{
                        $v_product_grab_url .= '<div id="div_image_grab_'.$i.'" style="float:left ; padding-right:10px;" class="img_more_products" ><p> <img rel="images_product_grab" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img style="max-width:150px;"  src="'. JT_URL. $v_image_temp. '">
                                    </div> ' ;
                    }
                }else{
                    if(checkRemoteFile(URL.$v_image_temp)){
                        $v_product_grab_url .= '<div id="div_image_grab_'.$i.'" style="float:left ; padding-right:10px;" class="img_more_products" >
                                        <img style="max-width:150px;"  src="'. URL.$v_image_temp . '">
                                     </div> ' ;
                    }else{
                        $v_product_grab_url .= '<div id="div_image_grab_'.$i.'" style="float:left ; padding-right:10px;" class="img_more_products" >
                                        <img style="max-width:150px;"  src="'. JT_URL. $v_image_temp. '">
                                    </div> ' ;
                    }
                }
            }
            $arr_product_images = $cls_tb_product->get_product_slide_image();
            for($i=0;$i<count($arr_product_images);$i++){
                $v_image_temp = $arr_product_images[$i];
//                $v_image_slider .= $v_image_slider=='' ? $v_image_temp."," :($i== count($arr_product_images)-1 ? $v_image_slider : $v_image_slider);
                $v_image_slider .= $v_image_slider=='' ? $v_image_temp."," :($i== count($arr_product_images)-1 ? $v_image_temp : $v_image_temp.",");
                if(is_admin() || $v_edit_right){
                    if(checkRemoteFile(URL.$v_image_temp)){
                        $v_lst_image .= '<div id="div_image_slider_'.$i.'" style="float:left; padding-right:10px;" class="img_more_products" ><p> <img rel="images_product_slider" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img style="max-width:150px;"  src="'. URL.$v_image_temp . '">
                                     </div> ' ;
                    }else{
                        $v_lst_image .= '<div id="div_image_slider_'.$i.'" style="float:left ; padding-right:10px;" class="img_more_products" ><p> <img rel="images_product_slider" product_images_id="'.$i.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img style="max-width:150px;"  src="'. JT_URL. $v_image_temp. '">
                                    </div> ' ;
                    }
                }else{
                    if(checkRemoteFile(URL.$v_image_temp)){
                        $v_lst_image .= '<div id="div_image_slider_'.$i.'" style="float:left ; padding-right:10px;" class="img_more_products" >
                                        <img style="max-width:150px;"  src="'. URL.$v_image_temp . '">
                                     </div> ' ;
                    }else{
                        $v_lst_image .= '<div id="div_image_slider_'.$i.'" style="float:left ; padding-right:10px;" class="img_more_products" >
                                        <img style="max-width:150px;"  src="'. JT_URL. $v_image_temp. '">
                                    </div> ' ;
                    }
                }
            }

        }
    }
}
$v_dsp_material_draw = $cls_tb_material->draw_option('material_id', 'material_name', 0, array('status'=>0, '$where'=>'this.material_option!=null && this.material_option.length>0'),array('material_name'=>1));
//Combobox for locations
$v_tmp = 0;
$arr_all_tag = array();
$arr_tag = $cls_tb_tag->select(array("status"=>1));
foreach($arr_tag as $arr){
    $v_tag_id = $arr['_id'];
    $v_tag_name = isset($arr['name'])?$arr['name']:'';
    $arr_all_tag[] = array('_id'=>(string)$v_tag_id, 'name'=>$v_tag_name);
}
$v_dsp_product_status_draw = $cls_settings->draw_option_by_id('product_status', 1, $v_product_status);
$arr_option = $cls_settings->select_scalar('option', array('setting_name'=>'size_unit'));
$v_dsp_size_unit_draw = $cls_settings->draw_option_by_key("size_unit",0,'0');
$v_dsp_sold_by = $cls_settings->draw_option_by_id('sold_by',0,$v_sold_by);
$arr_banner_all = $cls_tb_banner->select(array("banner_status"=>0));
foreach($arr_banner_all as $arr){
    $arr_all_banner [] = array("banner_name"=>$arr['banner_name'],"_id"=>(string)$arr['_id']);
}
$arr_product = $cls_tb_product->select();
foreach($arr_product as $arr){
    if($v_mongo_id == (string) $arr["_id"]) continue;
    $v_image_temp = URL.$arr['products_upload'];
    if(!checkRemoteFile($v_image_temp)) $v_image_temp = JT_URL.$arr['products_upload'];
    $arr_product_list [] = array(
        'name'=>$arr['name']
        ,'id'=>(string)$arr['_id']
        ,'sku'=>$arr['sku']
        ,'slugger'=>$arr['slugger']
        ,'code'=>$arr['code']
        ,'price'=>$arr['sell_price']
        ,'image_url'=>$v_image_temp
        ,'category'=>$arr['category']
    );
}
?>