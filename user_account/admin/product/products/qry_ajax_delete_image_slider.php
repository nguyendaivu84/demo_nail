<?php if(!isset($v_sval)) die();
$v_product_images_id = isset($_REQUEST['txt_product_images_id']) ? $_REQUEST['txt_product_images_id'] : 0;
$v_product_id = isset($_REQUEST['txt_product_id']) ? $_REQUEST['txt_product_id'] : '';
$v_action = isset($_REQUEST['txt_action']) ? $_REQUEST['txt_action'] : '';
$arr_response = array('error'=>1,'message'=>'','data'=>'');
if($v_product_id=='' || $v_product_images_id<0 ){
    $arr_response = array('error'=>1,'message'=>'Can not find the images ','data'=>'');
}
if($v_action!=''){
    if($v_action=='delete-images'){
        $arr_response['error'] = 0;
        $cls_tb_product->select_one(array('_id'=>new MongoId($v_product_id)));
        $arr_image_slider = $cls_tb_product->get_product_slide_image();
        $arr_new_image_slider = array();
        $v_data = '';
        for($i=0;$i<count($arr_image_slider);$i++){
            if($i!=$v_product_images_id){
                $v_data .= $v_data=='' ? $arr_image_slider[$i]."," :($i==count($arr_image_slider)-1 ? $arr_image_slider[$i] : $arr_image_slider[$i].",");
                $arr_new_image_slider [] = $arr_image_slider[$i];
            }
            else @unlink($arr_product_image_size[$i]);
        }
        $arr_response['data'] = $v_data;
        $cls_tb_product->update_field("product_slide_image",$arr_new_image_slider,array('_id'=>new MongoId($v_product_id)));
    }
}
else{
    $arr_response = array('error'=>1,'message'=>'Can not find act ','data'=>'');
}
die(json_encode($arr_response));
?>
