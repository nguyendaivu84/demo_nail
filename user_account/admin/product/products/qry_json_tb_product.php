<?php if(!isset($v_sval)) die();
$arr_where_clause = array();
$v_quick_search = isset($_REQUEST['txt_quick_search'])?$_REQUEST['txt_quick_search']:'';
if($v_quick_search!=''){
//    $arr_where_clause['$where'] = array('$or'=> array( 'sku'=>new MongoRegex('/'.$v_quick_search.'/i') ,'name'=>new MongoRegex('/'.$v_quick_search.'/i') ) );
    $arr_where_clause['$or'] = array(array("name"=>new MongoRegex('/'.$v_quick_search.'/i')),array("sku"=>new MongoRegex('/'.$v_quick_search.'/i')));
//    $arr_where_clause['name'] = new MongoRegex('/'.$v_quick_search.'/i') ;
}
if(isset($_REQUEST['filter'])){
    $arr_filter = $_REQUEST['filter'];
    if(isset($arr_filter['filters'])){
        $arr_filters = $arr_filter['filters'];
        $arr_where_clause['category_id'] = $arr_filters[0]['value'];
    }
}
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        if($arr_temp[$i]['field'] =='deleted') $arr_sort['deleted'] = $arr_temp[$i]['dir']=='asc'?1:-1;
        else $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();

//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_product_redirect']) && $_SESSION['ss_tb_product_redirect']==1){
    if(isset($_SESSION['ss_tb_product_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_product_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_product_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_product_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_product_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_product->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_product_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_product_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_product_page'] = $v_page;
$_SESSION['ss_tb_product_quick_search'] = $v_quick_search;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("name"=>1);
$arr_tb_product = $cls_tb_product->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
add_class("cls_tb_nail_category");
$cls_tb_nail_category = new cls_tb_nail_category($db);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_category_id = array();
foreach($arr_tb_product as $arr){
	$v_product_id = (string)isset($arr['_id'])?$arr['_id']:'';
	$v_product_sku = isset($arr['sku'])?$arr['sku']:'';
	$v_short_description = isset($arr['description'])?$arr['description']:'';
	$v_image_file = isset($arr['products_upload'])?$arr['products_upload']:'';
    $v_image_file_jt = URL.$v_image_file;
    if(!checkRemoteFile($v_image_file_jt)) {
        $v_image_file_jt = JT_URL.$v_image_file;
    }
	$v_product_status = isset($arr['deleted'])?$arr['deleted']:false;
    $v_category = isset($arr['category_id'])?$arr['category_id']:'';
    $v_product_category = '';
    if(!isset($arr_category_id[$v_category]) && $v_category!=''){
        if($v_category!=''){
            $cls_tb_nail_category->select_one(array("_id"=> new MongoId($v_category)));
            $arr_category_id [$v_category] = $cls_tb_nail_category->get_name();
            $v_product_category = $cls_tb_nail_category->get_name();
        }
    }else if($v_category!='' && isset($arr_category_id[$v_category])) $v_product_category = $arr_category_id[$v_category];
    $v_status = $v_product_status==false ?"Active":"Inactive";
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'product_id' => (string)$v_product_id,
		'name' => $v_product_sku,
		'description' => $v_short_description,
		'image_url' =>  $v_image_file_jt,
		'deleted' => $v_status,
        'category' => $v_product_category
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_product'=>$arr_ret_data);
echo json_encode($arr_return);
?>