<?php if(!isset($v_sval)) die();
add_class('cls_tb_nail_category');
$cls_tb_tag = new cls_tb_nail_category($db, LOG_DIR);
$arr_product_tag = array();
$v_quick_search = '';
$v_page = 1;
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
if(isset($_SESSION['ss_tb_product_redirect']) && $_SESSION['ss_tb_product_redirect']==1){
    $v_page = isset($_SESSION['ss_tb_product_page'])?$_SESSION['ss_tb_product_page']:'1';
    settype($v_page,'int');
    if($v_page<1) $v_page = 1;
    if(isset($_SESSION['ss_tb_product_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_product_where_clause']);
    }
    $v_quick_search = isset($_SESSION['ss_tb_product_quick_search'])?$_SESSION['ss_tb_product_quick_search']:'';
}
$v_search_product_sku = $v_quick_search;
$v_search_short_description = $v_quick_search;
$arr_tag = $cls_tb_tag->select(array('status'=>1,"is_parent"=>0,"is_headline"=>0),array("name"=>1));
$arr_all_tag = array();
foreach($arr_tag as $arr){
    $arr_all_tag[] = array('_id'=>(string)$arr['_id'], 'name'=>$arr['name']);
}
?>