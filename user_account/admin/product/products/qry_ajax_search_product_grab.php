<?php if(!isset($v_sval)) die;
$v_keyword = isset($_POST['txt_keyword']) ? $_POST['txt_keyword'] : '';
$v_cat = isset($_POST['txt_cat']) ? $_POST['txt_cat'] : '';
$arr_return = array('product_list'=>array(),'error'=>0);
$arr_product = array();
if(isset($_POST['txt_keyword'])  && isset($_POST['txt_cat'])){
    $arr_where = array();
    if($v_cat!='' && $v_cat!=0){
        $arr_where['category_id'] = $v_cat;
    }
    if($v_keyword!='' ){
        $arr_where['$or'] = array(array("name"=>new MongoRegex('/'.$v_keyword.'/i')),array("code"=>new MongoRegex('/'.$v_keyword.'/i')),array("sku"=>new MongoRegex('/'.$v_keyword.'/i')));
    }
    $arr_list_product = $cls_tb_product->select($arr_where,array("name"=>1));
    foreach($arr_list_product as $arr){
        $arr_product [] = array(
            'name'=>$arr['name']
            ,'id'=>(string)$arr['_id']
            ,'sku'=>$arr['sku']
            ,'code'=>$arr['code']
            ,'price'=>$arr['sell_price']
            ,'image_url'=>JT_URL.$arr['products_upload']
            ,'category'=>$arr['category']
        );
    }
    $arr_return['error'] = 1;
    $arr_return['product_list'] = $arr_product;
}
header("Content-type: application/json");
echo json_encode($arr_return);
?>