<?php
if(!isset($v_sval)) die();
?>
<?php
add_class("cls_tb_banner");
$cls_tb_banner = new cls_tb_banner($db);

$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$arr_return = array('error'=>0, 'message'=>'', 'location'=>array(), 'tag'=>array(), 'group'=>array());
$arr_all_location = array();
$arr_all_location[] = array('location_id'=>0, 'location_name'=>'--------');
$arr_all_tag = array();
$arr_all_group = array();
$arr_all_group[] = array('product_group_id'=>0, 'product_group_name'=>'--------');
$_SESSION['ss_last_company'] = $v_company_id;
if($v_company_id>0){
    $arr_location = $cls_tb_location->select(array('company_id'=>$v_company_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])), array('location_name'=>1));
    foreach($arr_location as $arr){
        $arr_all_location[] = array('location_id'=>$arr['location_id'], 'location_name'=>$arr['location_name']);
    }
    $arr_tag = $cls_tb_tag->select(array('company_id'=>$v_company_id));
    foreach($arr_tag as $arr){
        $v_tag_id = isset($arr['tag_id'])?$arr['tag_id']:0;
        $v_tag_name = isset($arr['tag_name'])?$arr['tag_name']:'';
        $v_tag_parent = isset($arr['tag_parent'])?$arr['tag_parent']:0;
        if($v_tag_id>0 && $v_tag_name!=''){
            $arr_all_tag[] = array('tag_id'=>$v_tag_id, 'tag_name'=>$v_tag_name, 'tag_parent'=>$v_tag_parent);
        }
    }
    $arr_group = $cls_tb_product_group->select(array('company_id'=>$v_company_id));
    foreach($arr_group as $arr){
        $arr_all_group[] = array('product_group_id'=>$arr['product_group_id'], 'product_group_name'=>$arr['product_group_name']);
    }

}else{
    $arr_return['error'] = 1;
    $arr_return['message'] = 'Invalid Company ID';
}
//$v_temp = 0;
//$arr_all_banner = get_array_data($cls_tb_banner,'banner_id', 'banner_name',$v_temp,array(),array("company_id"=>$v_company_id,"banner_status"=>0));

$arr_banner_location_all = $cls_tb_location->select_distinct("location_banner",$db,array("company_id"=>$v_company_id));
for($i=0;$i<count($arr_banner_location_all['values']);$i++){
    if($arr_banner_location_all['values'][$i]!='')
        $arr_all_banner [] = array("banner_name"=>$arr_banner_location_all['values'][$i]);
}

$arr_return['banner'] = $arr_all_banner;

$arr_return['location'] = $arr_all_location;
$arr_return['tag'] = $arr_all_tag;
$arr_return['group'] = $arr_all_group;
echo(json_encode($arr_return));
?>
