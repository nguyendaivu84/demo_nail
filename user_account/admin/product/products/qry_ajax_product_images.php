<?php if(!isset($v_sval)) die();?>
<?php
$v_product_images_id = isset($_REQUEST['txt_product_images_id']) ? $_REQUEST['txt_product_images_id'] : 0;
$v_product_id = isset($_REQUEST['txt_product_id']) ? $_REQUEST['txt_product_id'] : 0;
$v_action = isset($_REQUEST['txt_action']) ? $_REQUEST['txt_action'] : '';
$v_company_code = isset($_REQUEST['txt_company_code']) ? $_REQUEST['txt_company_code'] : '';

$arr_response = array('error'=>1,'message'=>'','data'=>'');

if($v_product_id==0 || $v_product_images_id==0 ){
    $arr_response = array('error'=>1,'message'=>'Can not find the images ','data'=>'');
}
if($v_action!=''){
    add_class('cls_tb_product_images');
    $cls_tb_product_images = new cls_tb_product_images($db);
    $v_file_name = $cls_tb_product_images->select_scalar('product_image',array('product_id'=>(int)$v_product_id, 'product_images_id'=>(int)$v_product_images_id));
    if($v_file_name!=''){
        $v_save_image = 'resource/' . $v_company_code .'/products/'. $v_product_id.'/';
        for($i=0; $i<count($arr_product_image_size); $i++){
            @unlink($v_save_image.$arr_product_image_size[$i].'_'.$v_file_name );
        }
    }

    /*Delete images */
    if($v_action=='delete-images'){
        $cls_tb_product_images->delete(array('product_id'=>(int)$v_product_id, 'product_images_id'=>(int)$v_product_images_id));
    }
    /*Response images */

    $arr_product_images = $cls_tb_product_images->select(array('product_id'=>(int)$v_product_id ));
    $v_lst_image = '';
    foreach ($arr_product_images as $arr) {
        $v_product_images_id = isset($arr['product_images_id']) ? $arr['product_images_id'] : 0;
        $v_image = isset($arr['product_image']) ? $arr['product_image'] :'';
        $v_saved_dir = isset($arr['saved_dir']) ? $arr['saved_dir'] :'';
        if($v_saved_dir!='' && strrpos($v_saved_dir,'/')!==strlen($v_saved_dir)-1) $v_saved_dir.='/';
        if(file_exists($v_saved_dir .DS . PRODUCT_IMAGE_THUMB.'_'. $v_image))
            $v_lst_image .= '<div class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$v_product_images_id.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                <img  src="'. RESOURCE_URL . '/' .$v_company_code . '/' .'products'.'/'. $v_product_id .'/'.PRODUCT_IMAGE_THUMB.'_'. $v_image. '">
                             </div> ' ;
        else
            $v_lst_image .= '<div class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$v_product_images_id.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                <img  src="'. RESOURCE_URL . '/' .$v_company_code . '/' .'products'.'/'. $v_product_id .'/'. $v_image. '">';

    }
    $arr_response = array('error'=>0,'message'=>'','data'=>$v_lst_image);
}
else{
    $arr_response = array('error'=>1,'message'=>'Can not find act ','data'=>'');
}
die(json_encode($arr_response));
?>
