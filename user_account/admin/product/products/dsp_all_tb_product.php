    <?php if(!isset($v_sval)) die();?>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Product</h3>
                    </div>
                    <div id="div_quick">
                    <div id="div_quick_search">
                    <form method="post" id="frm_quick_search">
                    <span class="k-textbox k-space-left" id="txt_quick_search">
                    <input type="text" name="txt_quick_search" placeholder="Search by Product Sku or Description" value="<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>" />
                    <a id="a_quick_search" style="cursor: pointer" class="k-icon k-i-search"></a>
                        <script type="text/javascript">
                            $(document).ready(function(e){
                                $('a#a_quick_search').click(function(e){
                                    $('form#frm_quick_search').submit();
                                })
                            });
                        </script>
                    </span>
                    </form>
                    </div>
                    <div id="div_select">
                    </div>
                    </div>
                    <div id="grid"></div>
                    <script type="text/javascript">
					var window_search;
                    $(document).ready(function() {
                        var grid = $("#grid").kendoGrid({
                            dataSource: {
                                pageSize: 50,
                                page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
                                serverPaging: true,
                                serverSorting: true,
                                serverFiltering: true,
                                transport: {
                                    read: {
                                        url: "<?php echo URL.$v_admin_key;?>/json/",
                                        type: "POST",
                                        data: {txt_session_id:"<?php echo session_id();?>",txt_quick_search:'<?php echo $v_quick_search;?>', txt_search_product_tag: <?php echo json_encode($arr_product_tag);?>}
                                    }
                                },
                                schema: {
                                    data: "tb_product"
                                    ,total: function(data){
                                        return data.total_rows;
                                    }
                                },
                                type: "json"
                            },filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Tag is",
                                        neq: "Tag is not"
                                    }
                                }
                            },
                            pageSize: 50,
                            height: 430,
                            scrollable: true,
                            sortable: true,
                            //selectable: "single",
                            pageable: {
                                input: true,
                                refresh: true,
                                pageSizes: [10, 20, 30, 40, 50],
                                numeric: false
                            },
						columns: [
							{field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false,filterable: false, template: '<span style="float:right">#= row_order #</span>'},
							{field: "name", title: "Product Sku", type:"string", width:"80px",filterable: false, sortable: true },
							{field: "description", title: "Short Description", type:"string",filterable: false, width:"120px", sortable: true },
                            {field: "image_url", title: "Image", type:"string", width:"100px",filterable: false, sortable: false, template:'<p style="text-align:center; margin:0"><img src="#= image_url#" style="max-width:150px" /></p>'},
							{field: "deleted", title: "Product Status", type:"string",filterable: false, width:"50px", sortable: true},
                            {field: "category", title: "Category", type:"string", width:"100px", sortable: true,filterable: {ui: filtet_product_tag}},
                            { command:  [
								{ name: "View", text:'', title:'View info', click: view_row, imageClass: 'k-grid-View' }
								<?php if($v_edit_right || $v_is_admin){?>
								,{ name: "Edit", text:'', title:'Edit item', click: edit_row, imageClass: 'k-grid-Edit' }
								<?php }?>
								<?php if($v_delete_right || $v_is_admin){?>
								,{ name: "Delete", text:'', title:'Delete item', click: delete_row, imageClass: 'k-grid-Delete' }
								<?php }?>
								],
								title: " ", width: "70px" }
						 ]
					 }).data("kendoGrid");
				});
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.product_id+"/viewid";
                }
                var tag_search = <?php echo json_encode($arr_all_tag); ?>;
                function filtet_product_tag(element){
                    element.kendoDropDownList({
                        dataSource:tag_search,
                        dataTextField:'name',
                        dataValueField:'_id',
                        optionLabel: "--Select tag--"
                    });
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    //if(confirm('Do you want to edit product '+dataItem.product_sku+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.product_id+"/editid";
                    //}
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete product : '+dataItem.product_sku+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.product_id+"/deleteid";
                    }
                }
            </script>
                </div>
            </div>
        </div>
  </div>
<style type="text/css">
    .k-grid td {
        background: -moz-linear-gradient(top,  rgba(0,0,0,0.05) 0%, rgba(0,0,0,0.15) 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.05)), color-stop(100%,rgba(0,0,0,0.15)));
        background: -webkit-linear-gradient(top,  rgba(0,0,0,0.05) 0%,rgba(0,0,0,0.15) 100%);
        background: -o-linear-gradient(top,  rgba(0,0,0,0.05) 0%,rgba(0,0,0,0.15) 100%);
        background: -ms-linear-gradient(top,  rgba(0,0,0,0.05) 0%,rgba(0,0,0,0.15) 100%);
        background: linear-gradient(to bottom,  rgba(0,0,0,0.05) 0%,rgba(0,0,0,0.15) 100%);
        padding: 20px;
    }
</style>