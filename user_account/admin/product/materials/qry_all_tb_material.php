<?php if(!isset($v_sval)) die();?>
<?php
$v_quick_search = '';
$v_company_id = 0;
$v_page = 1;
if(isset($_POST['btn_advanced_search'])){
	$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
}else if(isset($_POST['btn_advanced_reset'])){
}else{
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
	$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
	if(isset($_SESSION['ss_tb_material_redirect']) && $_SESSION['ss_tb_material_redirect']==1){
		$v_page = isset($_SESSION['ss_tb_material_page'])?$_SESSION['ss_tb_material_page']:'1';
		settype($v_page,'int');
		if($v_page<1) $v_page = 1;
		if(isset($_SESSION['ss_tb_material_where_clause'])){
			$arr_where_clause = unserialize($_SESSION['ss_tb_material_where_clause']);
			if(isset($arr_where_clause['company_id'])) $v_company_id = $arr_where_clause['company_id'];
		}
		$v_quick_search = isset($_SESSION['ss_tb_material_quick_search'])?$_SESSION['ss_tb_material_quick_search']:'';
	}
}
settype($v_company_id, 'int');
$v_dsp_company_option ='';// $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
//Add code here if required
$v_content = '';
$v_title = '';
add_class("cls_tb_help");
$cls_tb_help = new cls_tb_help($db);
add_class("cls_tb_module");
$cls_tb_module = new cls_tb_module($db);
$v_module_id = $cls_tb_module->select_scalar("module_id",array("module_menu"=>"manage_material"));
$v_row = $cls_tb_help->select_one(array("help_status"=>0,"help_module"=>(int)$v_module_id,"help_type"=>"admin_page"));
if($v_row == 1){
    $v_content = $cls_tb_help->get_help_content();
    $v_title = $cls_tb_help->get_help_title();
}
else{
    $v_content = "Help on this topic is not yet avaible";
    $v_title = "Help on company";
}
?>