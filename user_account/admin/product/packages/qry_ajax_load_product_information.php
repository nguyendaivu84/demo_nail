<?php if(!isset($v_sval)) die();
$v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:'';
$arr_material = array('id'=>-1,'mat_id'=>0, 'description'=>'---------', 'status'=>0);
$cls_tb_product->select_one(array('_id'=>new MongoId($v_product_id)));
$v_image_link = URL.$cls_tb_product->get_products_upload();
if(!checkRemoteFile($v_image_link)) $v_image_link = JT_URL.$cls_tb_product->get_products_upload();
$arr_package_content_row = array(
    'product_id'=>$v_product_id
    ,'product_name'=>$cls_tb_product->get_name()
    ,'product_image'=>$v_image_link
    ,'status'=>$cls_tb_product->get_deleted()
);
$v_package_content_row = json_encode($arr_package_content_row);
$arr_return = array('success'=>1, 'message'=>'OK','package_content_one_row'=>$v_package_content_row);
header('Content-Type: application/json');
echo(json_encode($arr_return));
?>
