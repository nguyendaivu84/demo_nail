<?php if(!isset($v_sval)) die();?>
<style>
</style>
<script type="text/javascript">
    var kit_grid;
    var index = 0;
    var kit_data = <?php echo json_encode($arr_package_content_category);?>;
    $(document).ready(function(){
        kit_grid = $('#kit_grid').kendoGrid({
            dataSource:{
                data: kit_data,
                batch: true,
                schema:{
                    model:{
                        fields:{
                            package_id: {editable:false},
                            package_name: {editable:false},
                            quantity: {type: "number", validation: { required: true, min: 1, step:1, format:'n0'} },
                            status: {type: "boolean",editable:false},
                            material_desc: {type: "string",editable:false}
                            ,package_type: {type: "int",editable:false}
                        }
                    }
                }
                ,pageSize:20
            },
            height: 300,
            scrollable: true,
            navigatable: true,
            pageable: {
                input: true,
                numeric: false
            }
            ,editable: true
            ,columns: [
                {field: "package_id", title:"Order", width: "30px"},
                {field: "package_image", title:"Image", width: "100px",template: '<p style="text-align:center; margin:0"><img src="#= package_image#" style="max-width:150px" /><br />#=package_name#</p>'},
                {field: "status", title:"Status", width: "50px",template: '<p style="text-align:center; margin:0">#=!status?"Active":"Inactive"#</p>' },
                { command: [{name:"Remove" ,click: remove_row, imageClass:"k-icon k-delete"}], title: "&nbsp;", width: "50px" }

            ]
        }).data("kendoGrid");
        var tooltip = $("#tooltip").kendoTooltip({
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        if(tooltip) tooltip.show();

        var validator = $("div.information").kendoValidator().data("kendoValidator");
        $('input#txt_package_order').kendoNumericTextBox({
            format: "n0",
            step: 1
        });
        var product_data = $('#txt_product').width(500).kendoDropDownList({
            dataTextField: "product_sku",
            dataValueField: "product_id",
            template: '<img style="width: 150px" src=\"${data.product_image}\" alt=\"${data.product_sku}\" />' +
                '<h3 style="margin:2px">${ data.product_sku }</h3>' ,
            dataSource: {
                transport: {
                    read: {
                        dataType: "json",
                        url: "<?php echo URL.$v_admin_key;?>/ajax",
                        type: "POST",
                        data    :   {txt_package_id:'<?php echo $v_mongo_id;?>', txt_ajax_type: 'load_product'}
                        ,beforeSend: function(){
                        }
                    }
                }
            }
        }).data("kendoDropDownList");
        function remove_row(e){
            e.preventDefault();
            kit_grid.removeRow($(e.currentTarget).closest("tr"));
        }
        function compare(pname){
            var data = kit_grid.dataSource.data();
            var i= 0, found = false;
            while(i<data.length && !found){
                found = data[i].package_id == pname;
                i++;
            }
            return found;
        }
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $("#btn_submit_tb_tag").on("click",function(){
            var kit_content = kit_grid.dataSource.data();
            if(kit_content.length <=0){
                alert("Please choose product to continue!");
                return false;
            }
            var a_kit_content = [];
            for(var i=0; i<kit_content.length; i++){
                var one = {
                    package_id:kit_content[i].package_id
                };
                a_kit_content.push(one);
            }
            $('input#txt_hidden_package_content').val(JSON.stringify(a_kit_content));
        });
        $('#txt_product').change(function(e){
            var product_id = $(this).val();
            $.ajax({
                url     : '<?php echo URL.$v_admin_key;?>/ajax',
                dataType: 'json',
                type    : 'POST',
                data    : {txt_session_id: '<?php echo session_id();?>', txt_product_id: product_id, txt_ajax_type: 'load_product_information'},
                success : function(data){
                    $("#txt_category_one").val(data.package_content_one_row);
                },error:function(data){
                }
            });
        });
        var tags = <?php echo json_encode($arr_all_tag);?>;
        $("#txt_tag").width(300).kendoMultiSelect({
            dataSource: tags,
            dataTextField: "name",
            dataValueField: "_id",
            maxSelectedItems:1
        });
        var product_tag = $("#txt_tag").data("kendoMultiSelect");
        product_tag.value(<?php echo json_encode($arr_product_tag);?>);
        $("#btn_add_package").on("click",function(){
            var data_one_row = $("#txt_category_one").val();
            data_one_row = JSON.parse(data_one_row);
            if(!compare(data_one_row.product_id)){
                var data = kit_grid.dataSource.data();
                var url_image = data_one_row.product_image;
                var tmp = [];
                var one = {
                    package_id: data_one_row.product_id,
                    package_name: data_one_row.product_name,
                    package_image: url_image
                    ,package_type: data_one_row.package_type
                };
                tmp.push(one);
                for(var i= 0; i<data.length; i++){
                    tmp.push(data[i]);
                }
                kit_grid.dataSource.data(tmp);
                kit_grid.refresh();
            }
            else
            {
                alert(" Product already existed ! ");
            }
        });
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Package <?php echo (isset($v_mongo_id) && $v_mongo_id!='')?': '.$v_kit_name:'';?></h3>
                </div>

                <form id="frm_tb_tag" action="<?php echo URL.$v_admin_key;?><?php echo $v_mongo_id==''?'/add':("/".$v_mongo_id.'/editid');?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_category_one" name="txt_category_one" />
                    <input type="hidden" id="txt_category_all" name="txt_category_all" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <li>Content</li>
                        </ul>

                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Package Sku</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" style="width:300px" type="text" id="txt_package_sku" name="txt_package_sku" value="<?php echo $v_kit_name;?>" required validationMessage="Please input Package Sku" /> <label id="lbl_tag_name" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Category</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_tag" name="txt_tag[]" multiple="multiple"> </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Description</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" style="width:300px" type="text" id="txt_package_short_des" name="txt_package_short_des" value="<?php echo $v_kit_des;?>" />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Package's Order</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="numeric" id="txt_package_order" name="txt_package_order" value="<?php echo $v_kit_order_no;?>" />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <label>
                                            <input type="checkbox" id="txt_package_status" name="txt_package_status"<?php echo isset($v_kit_status)&&$v_kit_status==1?' checked="checked"':'';?> /> Active?
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="content">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Product</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_product" name="txt_product">

                                        </select>
                                        <div style="margin-top: 10px;margin-bottom: 10px;">
                                            <input class="k-button" id="btn_add_package" value="Add to Package" />
                                        </div>
                                    </td>
                                </tr>

                            </table>
                            <div style="margin-top: 10px"></div>
                            <div id="kit_grid"></div>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if(isset($v_error_message) && $v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="hidden" id="txt_hidden_package_content" name="txt_hidden_package_content" value="" />
                            <input type="submit" id="btn_submit_tb_tag" name="btn_submit_tb_tag" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
