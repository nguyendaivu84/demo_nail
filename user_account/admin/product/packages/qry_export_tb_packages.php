<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
    $v_where_clause = $_SESSION['ss_tb_tag_where_clause'];
    $arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_tag_sort'])){
    $v_sort = $_SESSION['ss_tb_tag_sort'];
    $arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_where_clause['package_type'] = array('$gt'=>1);
$arr_tb_product = $cls_tb_product->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_product_package_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
    ->setLastModifiedBy("Anvy")
    ->setTitle('Tag')
    ->setSubject("Office 2003 XLS Test Document")
    ->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
    ->setKeywords("office 2003 openxml php")
    ->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Tag');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Package Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Description', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Package Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Package Content', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Package Price', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name', 'center', true, true, 20, '', true);
//create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Date Created', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_product as $arr){
    $v_excel_col = 1;
    $v_package_id = isset($arr['product_id'])?$arr['product_id']:0;
    $v_package_name = isset($arr['product_sku'])?$arr['product_sku']:'';
    $v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
    $v_package_des = isset($arr['short_description'])?$arr['short_description']:$arr['long_description'];
    $v_package_type_id = isset($arr['package_type'])?$arr['package_type']:0; // settings lay ra package type
    $v_package_type = $cls_settings->get_option_name_by_id('package_type', $v_package_type_id);
    $v_package_content='';
    $arr_package_content = isset($arr['package_content'])?$arr['package_content']:'';// package content
    if(isset($arr_package_content) && $arr_package_content!=''){
        $v_package_content = '';
        for($i=0; $i<count($arr_package_content); $i++){
            $v_name = $arr_package_content[$i]['package_name'];
            $v_type = $arr_package_content[$i]['package_type'];

            if(!isset($arr_settings[$v_type]))
                $arr_settings[$v_type] = $cls_settings->get_option_name_by_id('package_type', $v_type);
            $v_type = $arr_settings[$v_type];
            $v_quantity = $arr_package_content[$i]['quantity'];
            $v_price = $arr_package_content[$i]['price'];
            $v_package_content .= 'Name: '.$v_name.' - Type: '.$v_type.' - Quantity: '.$v_quantity.' - Price: '.$v_price;

            if($i<count($arr_package_content)-1) $v_package_content.='\n';
        }
    }
    $v_package_price = isset($arr['default_price'])?$arr['default_price']:0;
    $v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
    $v_date_created = isset($arr['date_created'])?$arr['date_created']:(new MongoDate(time()));
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
    //create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_package_id, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_package_name, 'left');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_package_des, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_package_type, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_package_content, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_package_price, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_company_id, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name, 'left');
    //create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_date_created, 'right');
    $v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>