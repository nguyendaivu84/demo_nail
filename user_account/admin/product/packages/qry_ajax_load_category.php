<?php
if(!isset($v_sval)) die();
?>
<?php
add_class("cls_tb_category");
$cls_tb_category = new cls_tb_category($db);
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:'0';
settype($v_location_id, 'int');

$arr_category = $cls_tb_category->get_tree_category(0, $v_company_id, $v_location_id,'-', true);

header('Content-Type: application/json');
echo(json_encode($arr_category));
?>
