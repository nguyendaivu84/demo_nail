<?php if(!isset($v_sval)) die();
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_tag_name = isset($_POST['txt_search_tag_name'])?trim($_POST['txt_search_tag_name']):'';
if($v_search_tag_name!='') $arr_where_clause['group_name'] = new MongoRegex('/'.$v_search_tag_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_tag_redirect']) && $_SESSION['ss_tb_tag_redirect']==1){
    if(isset($_SESSION['ss_tb_tag_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_tag_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_tag_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_tag_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_tag_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_product_group->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_tag_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_tag_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_tag_page'] = $v_page;
$_SESSION['ss_tb_tag_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_tag = $cls_tb_product_group->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_location = array();
foreach($arr_tb_tag as $arr){
    $v_package_id = (string)isset($arr['_id'])?$arr['_id']:'';
    $v_package_key = isset($arr['group_name'])?$arr['group_name']:'';
    $v_description = isset($arr['group_description'])?$arr['group_description']:'';
    $v_status = isset($arr['status'])?$arr['status']:0;
    $v_status = $v_status==1 ? "Active" :"Inactive";
    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'package_id' => (string)$v_package_id,
        'package_name' => $v_package_key,
        'status' => $v_status,
        'description' => $v_description
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_product'=>$arr_ret_data);
echo json_encode($arr_return);
?>