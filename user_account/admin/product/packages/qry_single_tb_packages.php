<?php if(!isset($v_sval)) die();
$v_kit_name = '';
$v_mongo_id = '';
$arr_child_content = array();
$v_kit_des = '';
$v_error_message = '';
$v_kit_status = 1;
$v_kit_order_no = 0;
$arr_product_tag  = array();
$arr_package_content_category = array();
if(isset($_POST['btn_submit_tb_tag'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:'';
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_product_group->set_mongo_id($v_mongo_id);
    $v_kit_name = isset($_POST['txt_package_sku'])?$_POST['txt_package_sku']:$v_kit_name;
    $v_kit_name = trim($v_kit_name);
    if($v_kit_name=='') $v_error_message .= 'Group name is empty!<br />';
    $cls_tb_product_group->set_group_name($v_kit_name);
    $v_kit_des = isset($_POST['txt_package_short_des'])?$_POST['txt_package_short_des']:$v_kit_des;
    $v_kit_des = trim($v_kit_des);
    $cls_tb_product_group->set_group_description($v_kit_des);
    $v_kit_status = isset($_POST['txt_package_status'])?1:0;
//    $v_kit_status = $v_kit_status=='on'?1:0;
    $cls_tb_product_group->set_status($v_kit_status);
    $v_package_content = isset($_POST['txt_hidden_package_content'])?$_POST['txt_hidden_package_content']:'';
    $v_package_content = stripcslashes($v_package_content);
    $arr_package_content = json_decode($v_package_content, true);
    if(!is_array($arr_package_content)) $arr_package_content = array();
    for($i=0;$i<count($arr_package_content);$i++){
        $arr_child_content[] = $arr_package_content[$i]['package_id'];
    }
    $cls_tb_product_group->set_child_item($arr_child_content);
    $v_kit_order_no = isset($_POST['txt_package_order'])?$_POST['txt_package_order']:0;
    settype($v_kit_order_no,"int");
    $cls_tb_product_group->set_order_no($v_kit_order_no);;
    $v_product_group_id = $cls_tb_product_group->select_next('product_group_id');
    $cls_tb_product_group->set_product_group_id($v_product_group_id);
    $arr_product_tag = isset($_POST['txt_tag'])?$_POST['txt_tag']:$arr_product_tag;
    $arr_product_tag = isset($arr_product_tag[0]) ? $arr_product_tag[0] : "";
    $cls_tb_product_group->set_category_id($arr_product_tag);
    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_product_group->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_product_group->update(array('_id' => $v_mongo_id));
        }
        if($v_result) redir(URL.$v_admin_key);
    }
}else{
    $v_package_id= isset($_GET['id'])?$_GET['id']:'';
    if($v_package_id!=''){
        $v_row = $cls_tb_product_group->select_one(array('_id' => new MongoId($v_package_id)));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_product_group->get_mongo_id();
            $arr_child_content = $cls_tb_product_group->get_child_item();
            $v_kit_des = $cls_tb_product_group->get_group_description();
            $v_kit_status = $cls_tb_product_group->get_status();
            $v_kit_order_no = $cls_tb_product_group->get_order_no();
            $v_kit_name = $cls_tb_product_group->get_group_name();
            $arr_product_tag [] = $cls_tb_product_group->get_category_id();
        }
    }
}
$arr_all_tag = array();
$arr_tag = $cls_tb_tag->select(array("status"=>1));
foreach($arr_tag as $arr){
    $v_tag_id = $arr['_id'];
    $v_tag_name = isset($arr['name'])?$arr['name']:'';
    $arr_all_tag[] = array('_id'=>(string)$v_tag_id, 'name'=>$v_tag_name);
}
for($i=0;$i<count($arr_child_content);$i++){
    $v_id_temp = $arr_child_content[$i];
    $v_result = $cls_tb_product->select_one(array("_id"=> new MongoId($v_id_temp)));
    $v_status = $cls_tb_product->get_deleted() == 1 ? "Active" : "Inactive";
    if($v_result){
        $v_image_temp = URL.$cls_tb_product->get_products_upload();
        if(!checkRemoteFile($v_image_temp)) $v_image_temp = JT_URL.$cls_tb_product->get_products_upload();
        $arr_package_content_category[] = array(
            'package_id'=>(string)$cls_tb_product->get_mongo_id(),
            'package_name'=>$cls_tb_product->get_name(),
            'package_image'=>$v_image_temp,
            'status'=>$v_status
        );
    }
}
?>