<?php if(!isset($v_sval)) die();
$arr_product = array();
$arr_where = array('deleted'=>false);

$arr_product[] = array(
    'product_id'=>''
    ,'product_sku'=>'--------'
    ,'short_description'=>''
    ,'product_image'=>''
);

$arr_package = array();
$arr_all_product = $cls_tb_product->select(array(), array('product_sku'=>1));

foreach($arr_all_product as $arr){
    $v_product_id = (string)isset($arr['_id'])?$arr['_id']:'';
    $v_product_sku = isset($arr['sku'])?$arr['sku']:'';
    $v_description = isset($arr['description'])?$arr['description']:'';
    $v_image = isset($arr['products_upload'])?$arr['products_upload']:'';
    $v_image_temp = URL.$v_image;
    if(!checkRemoteFile($v_image_temp)) $v_image = JT_URL.$v_image;
    else $v_image = $v_image_temp;
    $arr_product[] = array(
        'product_id'=>(string)$v_product_id
        ,'product_sku'=>$v_product_sku
        ,'short_description'=>$v_description
        ,'product_image'=>$v_image
    );
}
header('Content-Type: application/json');
echo(json_encode($arr_product));
?>