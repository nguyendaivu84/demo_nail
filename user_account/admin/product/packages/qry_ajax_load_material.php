<?php
if(!isset($v_sval)) die();
?>
<?php
$v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:'0';
settype($v_product_id, 'int');

$arr_material = array('id'=>-1,'mat_id'=>0, 'description'=>'---------', 'status'=>0);
$arr_material = $cls_tb_product->get_material_info($v_product_id, array(), false, -1, $arr_material);

$v_default_quantity = $cls_tb_product->select_scalar('default_quantity', array('product_id'=>$v_product_id));
if(is_null($v_default_quantity)) $v_default_quantity = 0;
settype($v_default_quantity, 'int');
if($v_default_quantity<1) $v_default_quantity = 1;
/*---------------------- get all information of product for kit content -------------------------*/
$cls_tb_product->select_one(array('product_id'=>$v_product_id));
$v_saved_dir = $cls_tb_product->get_saved_dir();
if(substr($v_saved_dir,0,-1)!='/') $v_saved_dir .='/';
$arr_package_content_row = array(
    'product_id'=>$v_product_id
    ,'parent_id'=>0 // update after submit
    ,'product_name'=>$cls_tb_product->get_product_sku()
    ,'package_type'=>$cls_tb_product->get_package_type()
    //,'material_index'=>0 // update after change
    ,'product_quantity'=>$v_default_quantity// quantity items of kit
    //,'product_template'=>$cls_tb_product->get_template()
    ,'product_image'=>$cls_tb_product->get_image_file()
    ,'product_image_dir'=>$v_saved_dir
    ,'product_status'=>$cls_tb_product->get_product_status()
    ,'product_status_pair'=>array('status_id'=>$cls_tb_product->get_product_status(), 'status_name'=>$cls_settings->get_option_name_by_id('product_status', (int) $cls_tb_product->get_product_status(),'rrrrr'))
    //,'price'=>$cls_tb_product->get_default_price() // sold by unit
);
$v_package_content_row = json_encode($arr_package_content_row);
/**--------------------------------- end --------------------------------------------------------*/
$arr_return = array('success'=>1, 'message'=>'OK', 'material'=>$arr_material, 'quantity'=>$v_default_quantity,'package_content_one_row'=>$v_package_content_row);
header('Content-Type: application/json');
echo(json_encode($arr_return));
?>
