<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_order_items_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_order_items_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_order_items_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_order_items = $cls_tb_nail_order_items->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_order_items = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_order_items .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_order_items .= '<th>Ord</th>';
$v_dsp_tb_nail_order_items .= '<th>Order Item Id</th>';
$v_dsp_tb_nail_order_items .= '<th>Order Id</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Id</th>';
$v_dsp_tb_nail_order_items .= '<th>Card Id</th>';
$v_dsp_tb_nail_order_items .= '<th>Address Book Id</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Price</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Sku</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Quantity</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Material Id</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Material Name</th>';
$v_dsp_tb_nail_order_items .= '<th>Total Price</th>';
$v_dsp_tb_nail_order_items .= '<th>Product Image</th>';
$v_dsp_tb_nail_order_items .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_order_items as $arr){
	$v_dsp_tb_nail_order_items .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_order_items .= '<td align="right">'.($v_count++).'</td>';
	$v_order_item_id = isset($arr['order_item_id'])?$arr['order_item_id']:0;
	$v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_card_id = isset($arr['card_id'])?$arr['card_id']:0;
	$v_address_book_id = isset($arr['address_book_id'])?$arr['address_book_id']:0;
	$v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
	$v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
	$v_product_quantity = isset($arr['product_quantity'])?$arr['product_quantity']:1;
	$v_product_material_id = isset($arr['product_material_id'])?$arr['product_material_id']:0;
	$v_product_material_name = isset($arr['product_material_name'])?$arr['product_material_name']:'';
	$v_total_price = isset($arr['total_price'])?$arr['total_price']:;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:;
	$v_dsp_tb_nail_order_items .= '<td>'.$v_order_item_id.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_order_id.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_id.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_card_id.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_address_book_id.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_price.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_sku.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_quantity.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_material_id.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_material_name.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_total_price.'</td>';
	$v_dsp_tb_nail_order_items .= '<td>'.$v_product_image.'</td>';
	$v_dsp_tb_nail_order_items .= '</tr>';
}
$v_dsp_tb_nail_order_items .= '</table>';
?>