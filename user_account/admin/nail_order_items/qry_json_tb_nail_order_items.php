<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_order_items_redirect']) && $_SESSION['ss_tb_nail_order_items_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_order_items_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_order_items_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_order_items_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_order_items_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_order_items_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_order_items->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_order_items_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_order_items_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_order_items_page'] = $v_page;
$_SESSION['ss_tb_nail_order_items_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_order_items = $cls_tb_nail_order_items->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_order_items as $arr){
	$v_order_item_id = isset($arr['order_item_id'])?$arr['order_item_id']:0;
	$v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_card_id = isset($arr['card_id'])?$arr['card_id']:0;
	$v_address_book_id = isset($arr['address_book_id'])?$arr['address_book_id']:0;
	$v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
	$v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
	$v_product_quantity = isset($arr['product_quantity'])?$arr['product_quantity']:1;
	$v_product_material_id = isset($arr['product_material_id'])?$arr['product_material_id']:0;
	$v_product_material_name = isset($arr['product_material_name'])?$arr['product_material_name']:'';
	$v_total_price = isset($arr['total_price'])?$arr['total_price']:;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:;
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'order_item_id' => $v_order_item_id,
		'order_id' => $v_order_id,
		'product_id' => $v_product_id,
		'card_id' => $v_card_id,
		'address_book_id' => $v_address_book_id,
		'product_price' => $v_product_price,
		'product_sku' => $v_product_sku,
		'product_quantity' => $v_product_quantity,
		'product_material_id' => $v_product_material_id,
		'product_material_name' => $v_product_material_name,
		'total_price' => $v_total_price,
		'product_image' => $v_product_image
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_order_items'=>$arr_ret_data);
echo json_encode($arr_return);
?>