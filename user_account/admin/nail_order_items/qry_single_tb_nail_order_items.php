<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_order_item_id = 0;
$v_order_id = 0;
$v_product_id = 0;
$v_card_id = 0;
$v_address_book_id = 0;
$v_product_price = 0;
$arr_product_info = Array;
$v_product_sku = '';
$v_product_quantity = 1;
$arr_product_material = Array;
$v_product_material_id = 0;
$v_product_material_name = '';
$v_total_price = ;
$v_product_image = ;
$v_new_nail_order_items = true;
if(isset($_POST['btn_submit_tb_nail_order_items'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_order_items->set_mongo_id($v_mongo_id);
	$v_order_item_id = isset($_POST['txt_order_item_id'])?$_POST['txt_order_item_id']:$v_order_item_id;
	if(is_null($v_mongo_id)){
		$v_order_item_id = $cls_tb_nail_order_items->select_next('order_item_id');
	}
	$v_order_item_id = (int) $v_order_item_id;
	$cls_tb_nail_order_items->set_order_item_id($v_order_item_id);
	$v_order_id = isset($_POST['txt_order_id'])?$_POST['txt_order_id']:$v_order_id;
	$v_order_id = (int) $v_order_id;
	if($v_order_id<0) $v_error_message .= '[Order Id] is negative!<br />';
	$cls_tb_nail_order_items->set_order_id($v_order_id);
	$v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:$v_product_id;
	$v_product_id = (int) $v_product_id;
	if($v_product_id<0) $v_error_message .= '[Product Id] is negative!<br />';
	$cls_tb_nail_order_items->set_product_id($v_product_id);
	$v_card_id = isset($_POST['txt_card_id'])?$_POST['txt_card_id']:$v_card_id;
	$v_card_id = (int) $v_card_id;
	if($v_card_id<0) $v_error_message .= '[Card Id] is negative!<br />';
	$cls_tb_nail_order_items->set_card_id($v_card_id);
	$v_address_book_id = isset($_POST['txt_address_book_id'])?$_POST['txt_address_book_id']:$v_address_book_id;
	$v_address_book_id = (int) $v_address_book_id;
	if($v_address_book_id<0) $v_error_message .= '[Address Book Id] is negative!<br />';
	$cls_tb_nail_order_items->set_address_book_id($v_address_book_id);
	$v_product_price = isset($_POST['txt_product_price'])?$_POST['txt_product_price']:$v_product_price;
	$v_product_price = (float) $v_product_price;
	if($v_product_price<0) $v_error_message .= '[Product Price] is negative!<br />';
	$cls_tb_nail_order_items->set_product_price($v_product_price);
	$arr_product_info = isset($_POST['txt_product_info'])?$_POST['txt_product_info']:$arr_product_info;
	$cls_tb_nail_order_items->set_product_info($arr_product_info);
	$v_product_sku = isset($_POST['txt_product_sku'])?$_POST['txt_product_sku']:$v_product_sku;
	$v_product_sku = trim($v_product_sku);
	if($v_product_sku=='') $v_error_message .= '[Product Sku] is empty!<br />';
	$cls_tb_nail_order_items->set_product_sku($v_product_sku);
	$v_product_quantity = isset($_POST['txt_product_quantity'])?$_POST['txt_product_quantity']:$v_product_quantity;
	$v_product_quantity = (int) $v_product_quantity;
	if($v_product_quantity<0) $v_error_message .= '[Product Quantity] is negative!<br />';
	$cls_tb_nail_order_items->set_product_quantity($v_product_quantity);
	$arr_product_material = isset($_POST['txt_product_material'])?$_POST['txt_product_material']:$arr_product_material;
	$cls_tb_nail_order_items->set_product_material($arr_product_material);
	$v_product_material_id = isset($_POST['txt_product_material_id'])?$_POST['txt_product_material_id']:$v_product_material_id;
	$v_product_material_id = (int) $v_product_material_id;
	if($v_product_material_id<0) $v_error_message .= '[Product Material Id] is negative!<br />';
	$cls_tb_nail_order_items->set_product_material_id($v_product_material_id);
	$v_product_material_name = isset($_POST['txt_product_material_name'])?$_POST['txt_product_material_name']:$v_product_material_name;
	$v_product_material_name = trim($v_product_material_name);
	if($v_product_material_name=='') $v_error_message .= '[Product Material Name] is empty!<br />';
	$cls_tb_nail_order_items->set_product_material_name($v_product_material_name);
	$v_total_price = isset($_POST['txt_total_price'])?$_POST['txt_total_price']:$v_total_price;
	$v_total_price = (float) $v_total_price;
	if($v_total_price<0) $v_error_message .= '[Total Price] is negative!<br />';
	$cls_tb_nail_order_items->set_total_price($v_total_price);
	$v_product_image = isset($_POST['txt_product_image'])?$_POST['txt_product_image']:$v_product_image;
	$v_product_image = (int) $v_product_image;
	if($v_product_image<0) $v_error_message .= '[Product Image] is negative!<br />';
	$cls_tb_nail_order_items->set_product_image($v_product_image);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_order_items->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_order_items->update(array('_id' => $v_mongo_id));
			$v_new_nail_order_items = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_order_items_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_order_items) $v_nail_order_items_id = 0;
		}
	}
}else{
	$v_order_item_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_order_item_id,'int');
	if($v_order_item_id>0){
		$v_row = $cls_tb_nail_order_items->select_one(array('order_item_id' => $v_order_item_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_order_items->get_mongo_id();
			$v_order_item_id = $cls_tb_nail_order_items->get_order_item_id();
			$v_order_id = $cls_tb_nail_order_items->get_order_id();
			$v_product_id = $cls_tb_nail_order_items->get_product_id();
			$v_card_id = $cls_tb_nail_order_items->get_card_id();
			$v_address_book_id = $cls_tb_nail_order_items->get_address_book_id();
			$v_product_price = $cls_tb_nail_order_items->get_product_price();
			$arr_product_info = $cls_tb_nail_order_items->get_product_info();
			$v_product_sku = $cls_tb_nail_order_items->get_product_sku();
			$v_product_quantity = $cls_tb_nail_order_items->get_product_quantity();
			$arr_product_material = $cls_tb_nail_order_items->get_product_material();
			$v_product_material_id = $cls_tb_nail_order_items->get_product_material_id();
			$v_product_material_name = $cls_tb_nail_order_items->get_product_material_name();
			$v_total_price = $cls_tb_nail_order_items->get_total_price();
			$v_product_image = $cls_tb_nail_order_items->get_product_image();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>