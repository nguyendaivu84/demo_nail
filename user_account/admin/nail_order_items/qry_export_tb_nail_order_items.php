<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_order_items_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_order_items_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_order_items_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_order_items = $cls_tb_nail_order_items->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_nail_order_items_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Nail_order_items')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Nail_order_items');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Order Item Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Order Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Card Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Address Book Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Price', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Sku', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Quantity', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Material Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Material Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Total Price', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Product Image', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_nail_order_items as $arr){
	$v_excel_col = 1;
	$v_order_item_id = isset($arr['order_item_id'])?$arr['order_item_id']:0;
	$v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_card_id = isset($arr['card_id'])?$arr['card_id']:0;
	$v_address_book_id = isset($arr['address_book_id'])?$arr['address_book_id']:0;
	$v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
	$v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
	$v_product_quantity = isset($arr['product_quantity'])?$arr['product_quantity']:1;
	$v_product_material_id = isset($arr['product_material_id'])?$arr['product_material_id']:0;
	$v_product_material_name = isset($arr['product_material_name'])?$arr['product_material_name']:'';
	$v_total_price = isset($arr['total_price'])?$arr['total_price']:;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:;
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_order_item_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_order_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_card_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_address_book_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_price, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_sku, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_quantity, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_material_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_material_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_total_price, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_product_image, 'right');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>