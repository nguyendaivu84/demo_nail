<?php
if(!isset($v_sval)) die();
$v_dir = ROOT_DIR.DS.'resources/static_content/';

$handle = opendir($v_dir);

$arr_list_image = array();

if($handle){
    while(($file = readdir($handle))!==false){
        if($file!='.' && $file!='..'){
            if(is_file($v_dir.DS.$file)){
                $arr_list_image[] = array(
                    'name'=>$file
                    ,'type'=>'f'
                    ,'size'=>filesize($v_dir.DS.$file)
                );
            }else if(is_dir($v_dir.DS.$file)){
                $arr_list_image[] = array(
                    'name'=>$file
                    ,'type'=>'d'
                    ,'size'=>0
                );
            }
        }
    }
}
header('Content-Type:application/json');
echo json_encode($arr_list_image);