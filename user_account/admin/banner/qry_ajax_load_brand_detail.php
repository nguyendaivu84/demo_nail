<?php if(!isset($v_sval)) die;
    $arr_ret_data = array();
    $arr_return = array('total_rows'=>0, 'tb_banner'=>$arr_ret_data);
    $v_brand_id = isset($_POST['txt_brand_id']) ? $_POST['txt_brand_id'] : '';
    $v_brand_index = isset($_POST['txt_index']) ? $_POST['txt_index'] : '';
    if($v_brand_id!='' && $v_brand_index!=''){
        settype($v_brand_index,"int");
        $arr_child_item = $cls_tb_banner->select_scalar("child_item",array('_id'=> new MongoId($v_brand_id)));
        for($i=0;$i<count($arr_child_item);$i++){
            if($i!=$v_brand_index && $v_brand_index!=-1) continue;
            $arr_detail = isset($arr_child_item[$i]['detail']) ? $arr_child_item[$i]['detail'] : array();
            for($j=0;$j<count($arr_detail);$j++){
                $v_text = isset($arr_detail[$j]['text']) ? $arr_detail[$j]['text'] : '';
                $arr_ret_data [] = array(
                    'order'=>$j
                    ,'text'=>$v_text
                );
            }
            $arr_return = array('total_rows'=>count($arr_ret_data), 'tb_banner'=>$arr_ret_data);
        }
    }
    header("Content-type: application/json");
    echo json_encode($arr_return);
?>