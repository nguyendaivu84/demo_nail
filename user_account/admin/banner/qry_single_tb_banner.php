<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = 'none';
$v_banner_id = 0;
$v_image_thumb = '';
$v_banner_name = '';
$v_content_content = '';
$v_brand_thumb = '';
$v_brand_thumb_link = '';
$v_brand_logo = '';
$v_brand_logo_link = '';
$v_banner_status = 0;
$v_language_type = 0;
$v_banner_order = 0;
$arr_brand_content_category = array();
$arr_product_list_image = array();
$arr_product_list = array();
$arr_list_about_brand = array();
$arr_list_about_all_brand = array();
add_class('clsupload');
$cls_upload = new clsupload;
$arr_qry_category = $cls_tb_nail_category->select(array("status"=>1,"is_parent"=>1));
$arr_qry_all_category = $cls_tb_nail_category->select(array("status"=>1,"is_parent"=>1));
if(isset($_POST['btn_submit_tb_banner'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='' && $v_mongo_id!='none') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_banner->set_mongo_id($v_mongo_id);
    $v_banner_id = isset($_POST['txt_banner_id'])?$_POST['txt_banner_id']:$v_banner_id;
    if(is_null($v_mongo_id)){
        $v_banner_id = $cls_tb_banner->select_next('banner_id');
    }
    $v_banner_id = (int) $v_banner_id;
    $cls_tb_banner->set_banner_id($v_banner_id);
    $v_banner_name = isset($_POST['txt_banner_name'])?$_POST['txt_banner_name']:$v_banner_name;
    $v_banner_name = trim($v_banner_name);
    if($v_banner_name=='') $v_error_message .= '[Brand Name] is empty!<br />';
    $cls_tb_banner->set_banner_name($v_banner_name);
    $v_slugger = str_replace(" ","-",$v_banner_name);
    $v_slugger .="-".$v_banner_id;
    $cls_tb_banner->set_banner_slugger($v_slugger);
    $v_banner_status = isset($_POST['txt_banner_status'])?0:1;
    $v_banner_status = (int) $v_banner_status;
    $cls_tb_banner->set_banner_status($v_banner_status);
    $v_banner_order = isset($_POST['txt_banner_order'])?$_POST['txt_banner_order']:$v_banner_order;
    $v_banner_order = (int) $v_banner_order;
    $cls_tb_banner->set_banner_order($v_banner_order);
    $v_content_content = isset($_POST['txt_content_content'])?$_POST['txt_content_content']:$v_content_content;
    $cls_tb_banner->set_banner_des($v_content_content);
    $v_temp = isset($_POST['txt_hidden_brand_content'])?$_POST['txt_hidden_brand_content']:'';
    $arr_temp = json_decode($v_temp,true);
    for($i=0;$i<count($arr_temp);$i++){
        $arr_temp_detail = $arr_temp[$i]['detail'];
        if(!empty($arr_temp_detail)){
            for($ij=0;$ij<count($arr_temp_detail);$ij++){
                $v_product_list = $arr_temp_detail[$ij]['list'];
                $arr_product_list = explode(",",$v_product_list);
                $arr_temp_detail[$ij]['list'] = $arr_product_list;
            }
            $v_check_one = $cls_tb_banner->select_one(array("_id"=> new MongoId($v_mongo_id)));
            if($v_check_one){
                $arr_temp_late = $cls_tb_banner->get_child_item();
                if(isset($arr_temp_late[$i])){
                    $arr_template_detail = $arr_temp_late[$i]['detail'];
                    for($i_temp = 0 ; $i_temp<count($arr_template_detail);$i_temp++){
                        $arr_temp_detail [] = $arr_template_detail[$i_temp];
                    }
                }
            }
        }else{
            $v_check_one = $cls_tb_banner->select_one(array("_id"=> new MongoId($v_mongo_id)));
            if($v_check_one){
                $arr_temp_late = $cls_tb_banner->get_child_item();
                if(isset($arr_temp_late[$i])){
                    $arr_temp_detail = $arr_temp_late[$i]['detail'];
                }
            }
        }
        $arr_brand_content_category [] = array(
            'text'=>$arr_temp[$i]['text']
            ,'name_language'=>$arr_temp[$i]['language_type']
            ,'category_name'=>$arr_temp[$i]['category_name']
            ,'category_id'=>$arr_temp[$i]['category_id']
            ,'text_content'=>$arr_temp[$i]['text_content']
            ,'image_url'=>$arr_temp[$i]['image_url']
            ,'detail'=>$arr_temp_detail
        );
    }
    $cls_tb_banner->set_child_item($arr_brand_content_category);

    // xu ly up anh
    $cls_upload->set_allow_overwrite(1);
    $v_upload_dir = PRODUCT_IMAGE_DIR;
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
    if($v_allow_upload) $v_upload_dir.='brand/';
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);

    $v_has_upload = false;
    $v_image_file_logo = '';
    if($v_allow_upload && isset($_FILES['txt_brand_logo']) ){
        $cls_upload->set_destination_dir($v_upload_dir);
        $cls_upload->set_field_name('txt_brand_logo');
        $cls_upload->set_allow_array_extension(array('jpg', 'png'));
        $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
        $cls_upload->upload_process();
        $v_tmp_image_file = '';
        if($cls_upload->get_error_number()==0){
            $v_image_file_logo = $cls_upload->get_filename();
            $v_has_upload = true;
        }
    }
    if(!$v_has_upload){
        $v_image_file_logo = isset($_POST['txt_hidden_brand_logo'])?$_POST['txt_hidden_brand_logo']:'';
    }
    $cls_tb_banner->set_banner_logo($v_image_file_logo);
    $v_has_upload = false;
    $v_image_file_thumb = '';
    if($v_allow_upload && isset($_FILES['txt_brand_thumb']) ){
        $cls_upload->set_destination_dir($v_upload_dir);
        $cls_upload->set_field_name('txt_brand_thumb');
        $cls_upload->set_allow_array_extension(array('jpg', 'png'));
        $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
        $cls_upload->upload_process();
        $v_tmp_image_file = '';
        if($cls_upload->get_error_number()==0){
            $v_image_file_thumb = $cls_upload->get_filename();
            $v_has_upload = true;
        }
    }
    if(!$v_has_upload){
        $v_image_file_thumb = isset($_POST['txt_hidden_brand_thumb'])?$_POST['txt_hidden_brand_thumb']:'';
    }
    $cls_tb_banner->set_banner_thumb($v_image_file_thumb);
    if($v_error_message==''){
        if(is_null($v_mongo_id) || $v_mongo_id=='none'){
            $v_mongo_id = $cls_tb_banner->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_banner->update(array('_id' => $v_mongo_id));

            $v_new_banner = false;
        }

        if($v_result){
            $_SESSION['ss_tb_banner_redirect'] = 1;
            redir(URL.$v_admin_key);
        }else{
            if($v_new_banner) $v_banner_id = 0;
        }
    }

}else{
    $v_banner_id= isset($_GET['id'])?$_GET['id']:'';
    if($v_banner_id!=''){
        $v_row = $cls_tb_banner->select_one(array('_id' => new MongoId($v_banner_id)));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_banner->get_mongo_id();
            $v_banner_id = $cls_tb_banner->get_banner_id();
            $v_banner_name = $cls_tb_banner->get_banner_name();
            $v_banner_status = $cls_tb_banner->get_banner_status();
            $v_banner_order = $cls_tb_banner->get_banner_order();
            $v_content_content = $cls_tb_banner->get_banner_des();
            $arr_temp = $cls_tb_banner->get_child_item();
            $v_brand_logo = $cls_tb_banner->get_banner_logo();
            $v_brand_logo_link = '<img src="'.URL.'resources/brand/'.$v_brand_logo.'" title="'.$v_brand_logo.'" />';
            $v_brand_thumb = $cls_tb_banner->get_banner_thumb();
            $v_brand_thumb_link = '<img src="'.URL.'resources/brand/'.$v_brand_thumb.'" title="'.$v_brand_thumb.'" />';
            if(count($arr_temp)){
                for($i=0;$i<count($arr_temp);$i++){
                    $arr_brand_content_category[] = array(
                        "order"=>$i
                        ,"text"=>$arr_temp[$i]['text']
                        ,"language_type"=>$arr_temp[$i]['name_language']
                        ,"category_name"=>$arr_temp[$i]['category_name']
                        ,"category_id"=>$arr_temp[$i]['category_id']
                        ,"image_url"=>$arr_temp[$i]['image_url']
                    );
                    $arr_list_about_brand[] = array(
                        '_val'=>$i
                        ,"_name"=>$arr_temp[$i]['text']
                    );
                    $arr_detail = isset($arr_temp[$i]['detail']) ? $arr_temp[$i]['detail'] : array();
                    for($j=0;$j<count($arr_detail);$j++){
                        $v_text = isset($arr_detail[$j]['text']) ? $arr_detail[$j]['text'] : '';
                        $v_list = '';
                        $arr_list = isset($arr_detail[$j]['list']) ? $arr_detail[$j]['list'] : array();
                        for($jj=0;$jj<count($arr_list);$jj++){
                            if($v_list=='') $v_list = $arr_list[$jj];
                            else $v_list .=",".$arr_list[$jj];
                        }
                        $arr_list_about_all_brand [] = array(
                            'order'=>$j
                            ,'text'=>$v_text
                            ,"product_list"=>$v_list
                            ,"about_index" => $i
                        );
                    }
                }
            }
        }
    }
}
$arr_qry_product_select = $cls_tb_product->select();
foreach($arr_qry_product_select as $arr){
    $arr_product_list_image [] = $arr['name'];
}
$v_temp = 0 ;
$arr_qry_all_category = get_array_data($cls_tb_nail_category,"_id","name",$v_temp,array(0,'--------'),array("status"=>1,"is_parent"=>0,"is_headline"=>0),array("name"=>1));
$arr_product = $cls_tb_product->select();
foreach($arr_product as $arr){
    $arr_product_list [] = array(
        'name'=>$arr['name']
        ,'id'=>(string)$arr['_id']
        ,'sku'=>$arr['sku']
        ,'code'=>$arr['code']
        ,'price'=>$arr['sell_price']
        ,'image_url'=>JT_URL.$arr['products_upload']
        ,'category'=>$arr['category']
    );
}
?>