<?php if(!isset($v_sval)) die();
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_banner_name = isset($_POST['txt_search_banner_name'])?trim($_POST['txt_search_banner_name']):'';
if($v_search_banner_name!='') $arr_where_clause['banner_name'] = new MongoRegex('/'.$v_search_banner_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_banner_redirect']) && $_SESSION['ss_tb_banner_redirect']==1){
    if(isset($_SESSION['ss_tb_banner_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_banner_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_banner_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_banner_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_banner_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_banner->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_banner_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_banner_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_banner_page'] = $v_page;
$_SESSION['ss_tb_banner_quick_search'] = $v_quick_search;

//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("banner_name"=>1);
$arr_tb_banner = $cls_tb_banner->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_location = array();
foreach($arr_tb_banner as $arr){
//    $v_banner_id = isset($arr['banner_id'])?$arr['banner_id']:0;
    $v_banner_id = isset($arr['_id'])?$arr['_id']:'';
    $v_banner_name = isset($arr['banner_name'])?$arr['banner_name']:'';
    $v_banner_status = isset($arr['banner_status'])?$arr['banner_status']:0;
    $v_banner_order = isset($arr['banner_order'])?$arr['banner_order']:0;
    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'banner_id' => (string)$v_banner_id,
        'banner_name' => $v_banner_name,
        'banner_status' => $v_banner_status,
        'banner_order' => $v_banner_order
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_banner'=>$arr_ret_data);
echo json_encode($arr_return);
?>