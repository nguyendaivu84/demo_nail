<?php if(!isset($v_sval)) die();
add_class('cls_upload');
add_class('cls_validation');
$v_upload_dir = "resources/static_content/";
$arr_return = array('message'=>'OK', 'upload'=>0, 'image_link'=>'none');

$up = cls_upload::factory($v_upload_dir);
$up->set_max_file_size(DESIGN_MAX_SIZE_UPLOAD);

if(!empty($_FILES['txt_content_image'])){
    $up->file($_FILES['txt_content_image']);
    //$up->callbacks($validate, array('check_file_extension', 'check_max_file_size', 'check_file_name_length'));
    $arr_result = $up->upload();

    $v_root_dir = ROOT_DIR;
    $arr_ext = array('', 'gif', 'jpg', 'png');

    if($arr_result['status']){
        $v_current_full_path = $up->get_result_path();
        $v_image_file = $up->get_current_name();
        list($width, $height, $type) = getimagesize($v_current_full_path);
        if($type>3) $type = 0;
        if($type>0){
            $v_extension = '.'.$arr_ext[$type];
            $v_extension_len = strlen($v_extension);
            $v_full_path_len = strlen($v_current_full_path)-$v_extension_len;

            if(strrpos($v_current_full_path, $v_extension)!==$v_full_path_len){
                $v_new_full_path = substr($v_current_full_path, 0, $v_full_path_len).$v_extension;
                $v_new_image_file = substr($v_image_file, 0, strlen($v_image_file)-$v_extension_len).$v_extension;
                $v_count = 0;
                $v_is_exist = file_exists($v_new_full_path);
                while($v_is_exist){
                    $v_count++;
                    $v_new_full_path = substr($v_current_full_path, 0, $v_full_path_len).'('.$v_count.')'.$v_extension;
                    $v_new_image_file = substr($v_image_file, 0, strlen($v_image_file)-$v_extension_len).'('.$v_count.')'.$v_extension;
                    $v_is_exist = file_exists($v_new_full_path);
                }
                rename($v_current_full_path, $v_new_full_path);
                $v_current_full_path = $v_new_full_path;
                $v_image_file = $v_new_image_file;
                $arr_return['image_link'] = $v_upload_dir.$v_image_file;
            }else {
                $arr_return['image_link'] = $v_upload_dir.$up->get_current_name();
            }
        }else {
            $arr_return['image_link'] = $v_upload_dir.$up->get_current_name();
        }
    }
    $arr_return['upload'] = $arr_result['status']?1:0;
}
$arr_return['message'] = $up->get_errors();
ob_end_clean();
ob_start();
header("Content-type: application/json");
echo json_encode($arr_return);