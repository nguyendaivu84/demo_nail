<?php if(!isset($v_sval)) die();?>
<style>
    .table_parent{
        padding-right:10px;
        width:32px;
        height:32px;
        padding-bottom:10px;
        background-repeat:no-repeat;
    }
    .td_class{
        padding-right:20px;
        font-weight:bold;
        padding-bottom:10px;
    }
    #popupWindow{margin: 0 auto;}
    .class_input{
        margin-right:5px;
        border: 1px solid #666;
        background-color:#0d0d0d;
        color:#d59eff;
        font-weight:bold;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        font-size: 13px;
        padding: 4px 10px;
        outline: 0;
        -webkit-appearance: none;
        cursor:pointer;
        float:right;
    }
    .class_input:hover {
        background-color:#1d1d1d;
    }
    #btn_add_new_contact{

        z-index: 1;
        position: absolute;
        clear: both;
        margin-top: -15px;
    }
    #pop_up input:last-child {
        margin-right: 5px;
    }
    #pop_up input:first-child {
        margin-left: -118px;
    }
    #pop_up input {
        margin-right: 125px;
    }
    .k-grid-content{
        height: 368px !important;
    }
</style>
<script type="text/javascript">
    var product_list;
    var grid_detail;
    var kit_grid;
    var index = 0;
    var popupResult = "Cancel";
    var popupResultValue = "0";
    var popupButtonType = "View";
    var popupWindow;
    var popupCallback;
    function popupClose(BtnResult,value,buttontType){
        popupResult = BtnResult;
        popupResultValue = value;
        popupButtonType = buttontType;
        popupWindow.close();
    };
    function reload_data_detail(brand_id,about_index){
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type: 'POST',
            data:   {txt_brand_id: brand_id, txt_index: about_index, txt_ajax_type: 'check_brand'},
            beforeSend: function(){
            },
            success: function(data, status){
                grid_detail.dataSource.data(data.tb_banner);
                grid_detail.refresh();
            }
            ,error:function(data){
                alert(data.responseText);
            }
        });
    }
    function search_product_list(name,cat){
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type: 'POST',
            data:   {txt_keyword: name, txt_cat: cat, txt_ajax_type: 'search_product'},
            beforeSend: function(){
            },
            success: function(data, status){
                product_list.dataSource.data(data.product_list);
                product_list.refresh();
            }
            ,error:function(data){
                alert(data.responseText);
            }
        });
    }
    function add_product_to_list(name,_id){
        var list_id = $("#txt_hidden_product_id").val();
        var list_name = $("#txt_hidden_product_name").val();
        if(list_id=='' ){
            list_id = _id;
        }
        else {
            list_id +=","+_id;
        }
        if(isNaN(list_name) || list_name=='') list_name = name;
        else list_name +=","+name;
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type: 'POST',
            data:   {txt_list_id: list_id , txt_ajax_type: 'get_product_information'},
            beforeSend: function(){
            },
            success: function(data, status){
                if(data.error==0){
                    $("#txt_hidden_product_id").val(list_id);
                    $("#dsp_list_product").html(data.content);
                }
            }
            ,error:function(data){
                alert(data.responseText);
            }
        });
    }
    function popuploseCallBack(e){
        popupWindow.unbind("close", popuploseCallBack);
        if (popupCallback !== null){
            popupCallback(popupResult,popupResultValue,popupButtonType);
        }
    }

    function popupOpen(Title,Message,Type,Buttons,theFunction,value,buttontype){
        popupResult = "Cancel";
        if (theFunction !== undefined){
            popupCallback = theFunction;
        } else {
            popupCallback = null;
        }
        popupWindow.bind("close", popuploseCallBack);
        popupWindow.title(Title);
        popupWindow.center();
//        popupWindow.content(popupData);
        popupWindow.open();
    }
    function showData(){}
    var kit_data = <?php echo json_encode($arr_brand_content_category);?>;
    var grid_detail_data = <?php echo json_encode($arr_list_about_all_brand); ?>;
    var product_list_data = <?php echo json_encode($arr_product_list); ?>;
    function remove_image(_id){
        var list_product = $("#txt_hidden_product_id").val();
        var new_list = '';
        var list_product_arr = list_product.split(",");
        for(var i =0 ; i<list_product_arr.length;i++){
            if(list_product_arr[i] !=_id) {
                if(new_list=='') new_list = list_product_arr[i];
                else new_list +=","+ list_product_arr[i];
            }
        }
        $("#"+_id).remove();
        $("#txt_hidden_product_id").val(new_list);
    }
    $(document).ready(function(){
        popupWindow = $("#popupWindow").kendoWindow({
            actions: ["Close"],
            draggable: true,
            modal: true,
            width: "900px",
            height: "500px",
            resizable: true,
            visible: false,
            title: "Confirm action"
        }).data("kendoWindow");
        product_list = $('#product_list').kendoGrid({
            dataSource:{
                data: product_list_data,
                batch: true,
                schema:{
                    model:{
                        fields:{
                            code: {editable:false}
                            ,sku: {editable:false}
                            ,name: {editable:false}
                            ,category: {editable:false}
                            ,price: {editable:false}
                        }
                    }
                }
                ,pageSize:20
            },
            height: 420,
            scrollable: true,
            navigatable: true,
            pageable: {
                input: true,
                numeric: false
            }
            ,editable: true
            ,columns: [
                {field: "code", title:"Code", width: "30px"},
                {field: "sku", title:"Sku", width: "70px"},
                {field: "name", title:"Name", width: "70px"},
                {field: "category", title:"Category", width: "70px"},
                {field: "image_url", title: "Image", type:"string", width:"100px",filterable: false, sortable: false, template:'<p style="text-align:center; margin:0"><img src="#= image_url#" style="max-width:100px" /></p>'},
                {field: "price", title:"Price", width: "70px"}
                ,{ command: [{name:"Add" ,click: add_row, imageClass:"k-icon k-add"}], title: "&nbsp;", width: "50px" }
            ]
        }).data("kendoGrid");
        grid_detail = $('#grid_detail').kendoGrid({
            dataSource:{
                data: grid_detail_data,
                batch: true,
                schema:{
                    model:{
                        fields:{
                            order: {editable:false},
                            text: {editable:true}
                        }
                    }
                }
                ,pageSize:20
            },
            height: 300,
            scrollable: true,
            navigatable: true,
            pageable: {
                input: true,
                numeric: false
            }
            ,editable: true
            ,columns: [
                {field: "order", title:"Order", width: "30px"},
                {field: "text", title:"Text", width: "100px"}
                ,{ command: [{name:"Remove" ,click: remove_row, imageClass:"k-icon k-delete"}], title: "&nbsp;", width: "50px" }
            ]
        }).data("kendoGrid");
        $("#txt_name_product").on("focusout",function(){
            var keyword = $(this).val();
            var cat = $("#category_list").val();
            search_product_list(keyword,cat);
        });
        $("#category_list").on("change",function(){
            var keyword = $("#txt_name_product").val();
            var cat = $(this).val();
            search_product_list(keyword,cat);
        });
        kit_grid = $('#kit_grid').kendoGrid({
            dataSource:{
                data: kit_data,
                batch: true,
                schema:{
                    model:{
                        fields:{
                            order: {editable:false},
                            text: {editable:true},
                            language_type: {editable:false},
                            category_name: {editable:false},
                            image_url: {type: "string",editable:false}
                        }
                    }
                }
                ,pageSize:20
            },
            height: 300,
            scrollable: true,
            navigatable: true,
            pageable: {
                input: true,
                numeric: false
            }
            ,editable: true
            ,columns: [
                {field: "order", title:"Order", width: "30px"},
                {field: "text", title:"Text", width: "100px"},
                {field: "language_type", title:"Language", width: "30px"},
                {field: "category_name", title:"Category", width: "100px"},
                {field: "image_url", title:"Image", width: "100px",template: '<p style="text-align:center; margin:0"><img src="#= image_url#" style="max-width:150px" />'}
                ,{ command: [{name:"Remove" ,click: remove_row, imageClass:"k-icon k-delete"}], title: "&nbsp;", width: "50px" }

            ]
        }).data("kendoGrid");
        function remove_row(e){
            e.preventDefault();
            kit_grid.removeRow($(e.currentTarget).closest("tr"));
        }
        function add_row(e){
            e.preventDefault();
//            product_list.removeRow($(e.currentTarget).closest("tr"));
            var dataItem2 = product_list.dataItem($(e.currentTarget).closest("tr"));
            var _id = dataItem2.id;
            var _name = dataItem2.name;
            product_list.removeRow($(e.currentTarget).closest("tr"));
            add_product_to_list(_name,_id);
        }
        $("#slt_about_brand").on("change",function(){
            var index = $(this).val();
            reload_data_detail('<?php echo $v_mongo_id; ?>',index);
        });
        $("#btn_search").on("click",function(){
            var index = $("#slt_about_brand").val();
            popupOpen("Add detail information","You have unsaved changes. Do you want to save before continuing?","hung",["Cancel","No","Yes"],showData,index,"Edit");
        });

        var editor_text_content_detail = $('textarea#txt_content_content_detail').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                path:"/resources/static_content/",
                dataType:'json',
                transport: {
                    read: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'list_images'}
                    },
                    destroy: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'delete_image_folder'}
                    },
                    create: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'create_image_folder'}
                    },
                    uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/upload-image",
                    thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/thumb-image"
                }
            }
        }).data("kendoEditor");
        $('input#txt_brand_logo').kendoUpload({
            multiple: false
        });
        $('input#txt_brand_thumb').kendoUpload({
            multiple: false
        });
        var editor_text_content = $('textarea#txt_text_content').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                path:"/resources/static_content/",
                dataType:'json',
                transport: {
                    read: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'list_images'}
                    },
                    destroy: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'delete_image_folder'}
                    },
                    create: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'create_image_folder'}
                    },
                    uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/upload-image",
                    thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/thumb-image"
                }
            }
        }).data("kendoEditor");
        var editor_content = $('textarea#txt_content_content').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                path:"/resources/static_content/",
                dataType:'json',
                transport: {
                    read: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'list_images'}
                    },
                    destroy: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'delete_image_folder'}
                    },
                    create: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'create_image_folder'}
                    },
                    uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/upload-image",
                    thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_mongo_id;?>/thumb-image"
                }
            }
        }).data("kendoEditor");
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        var tooltip = $("#tooltip").kendoTooltip({
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        if(tooltip) tooltip.show();

        var validator = $("div.information").kendoValidator().data("kendoValidator");
        $('select#txt_language_type').width(150).kendoDropDownList();

        var data_about_brand = '<?php echo json_encode($arr_list_about_brand) ; ?>';
        $('#slt_about_brand').width(150).kendoComboBox({
        });
        var category_list = <?php echo json_encode($arr_qry_all_category) ; ?>;
        $('#category_list').width(150).kendoComboBox({
            dataSource: category_list,
            dataTextField: "name",
            dataValueField: "_id"
        });

        $('select#txt_category').width(150).kendoDropDownList();
        $('input#txt_banner_order').kendoNumericTextBox({
            format: "n0",
            step: 1
        });

        var data_auto_complete = <?php echo json_encode($arr_product_list_image) ; ?>;
        var product_banner = $("#product_banner").kendoAutoComplete({
            dataSource: data_auto_complete,
            filter: "startswith",
            placeholder: "Type brand...",
            separator: ","
        }).data("kendoAutoComplete");

        $("#btn_add").on("click",function(){
            var data = grid_detail.dataSource.data();
            var length = grid_detail.dataSource.data().length;
            var list_id = $("#txt_hidden_product_id").val();
//            $("#txt_hidden_product_id").val('');
//            $("#dsp_list_product").html('');
            var _index_ = $("#slt_about_brand").val();
            var _text = $("#txt_content_content_detail").val();
            if(_index_=='-1'){
                alert('Choose About Brand Text ');
                return false;
            }else if(_text==''){
                alert('Empty detail information! ');
                return false;
            }
            var tmp = [];
            var one = {
                order: length+1
                ,text: _text
                ,product_list: list_id
                ,about_index: _index_
            };
            tmp.push(one);
            for(var i= 0; i<data.length; i++){
                tmp.push(data[i]);
            }
            grid_detail.dataSource.data(tmp);
            grid_detail.refresh();
        });
        $("#btn_add_package").on("click",function(){
            var data = kit_grid.dataSource.data();
            var length = kit_grid.dataSource.data().length;
            var url_image = $('#txt_hidden_image_url').val(); //$("#txt_content_image").next().val();
            url_image = "<?php echo URL;?>"+url_image;
            var text = $("#txt_text").val();
            if(text==''){
                alert('Text name is empty');
                return false;
            }
            var language_type = $("#txt_language_type").val();
            var category_id = $("#txt_category option:selected").val();
            if(category_id==0){
                alert('category is empty');
                return false;
            }
            var category_name = $("#txt_category option:selected").text();
            var text_content = $('textarea[name=txt_text_content]').val();
            var tmp = [];
            var one = {
                order: length+1
                ,text: text
                ,language_type: language_type
                ,category_name: category_name
                ,category_id: category_id
                ,text_content: text_content
                ,image_url:url_image
            };
            tmp.push(one);
            for(var i= 0; i<data.length; i++){
                tmp.push(data[i]);
            }
            kit_grid.dataSource.data(tmp);
            kit_grid.refresh();
        });
        $('input#txt_content_image').kendoUpload({
            async: {
                saveUrl:'<?php echo URL.$v_admin_key;?>/9/upload/',
                autoUpload:true,
                multiple:false
            }
            ,success: on_success
            ,select: on_select
            ,error :on_error
        });
        function on_success(e){
            if(e.response.upload==1){
               $("#txt_hidden_image_url").val(e.response.image_link);
            }
        }
        function on_error(e){
            alert(e.response);
        }
        function on_select(e){
            $("ul.k-upload-files").html("");
            if(e.files.length>1){
                e.preventDefault();
                alert('Please choose only max-two files!');
                return false;
            }
            return true;
        }
        $("#txt_name_product").on("keypress",function(event){
            if(event.which == 13)  {
                var keyword = $(this).val();
                var cat = $("#category_list").val();
                search_product_list(keyword,cat);
            }
        });
        $("#btn_submit_tb_banner").on("click",function(){
            var kit_content = kit_grid.dataSource.data();
            var a_kit_content = [];
            for(var i=0; i<kit_content.length; i++){
                var detail = [];
                var product_list = '';
                var grid_detail_content = grid_detail.dataSource.data();
                for(var j=0; j<grid_detail_content.length; j++){
                    if(grid_detail_content[j].about_index == i ){
                        var one = {
                            'text':grid_detail_content[j].text
                            ,'list': grid_detail_content[j].product_list
                        }
                        detail.push(one);
                    }
                }
                var one = {
                    order: i
                    ,text: kit_content[i].text
                    ,language_type: kit_content[i].language_type
                    ,category_name: kit_content[i].category_name
                    ,category_id: kit_content[i].category_id
                    ,text_content: kit_content[i].text_content
                    ,image_url: kit_content[i].image_url
                    ,detail: detail
                };
                a_kit_content.push(one);
            }
            $('input#txt_hidden_brand_content').val(JSON.stringify(a_kit_content));
        });
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div style="min-height: 800px;" id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Brand<?php echo (isset($v_banner_id) && $v_banner_id>0)?': '.$v_banner_name:'';?></h3>
                </div>
                <form id="frm_tb_banner" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id) || $v_mongo_id=='none'?'add':$v_mongo_id.'/editid';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_banner_id" name="txt_banner_id" value="<?php echo $v_banner_id;?>" />
                    <input type="hidden" id="txt_hidden_brand_content" name="txt_hidden_brand_content" value="" />
                    <input type="hidden" id="txt_hidden_image_url" name="txt_hidden_image_url" value="" />
                    <input type="hidden" id="txt_hidden_product_id" name="txt_hidden_product_id" value="" />
                    <input type="hidden" id="txt_hidden_product_name" name="txt_hidden_product_name" value="" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <li class="">About Brand</li>
                            <li class="">Detail</li>
                        </ul>
                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Banner Name</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input style="width:300px" class="k-textbox" type="text" id="txt_banner_name" name="txt_banner_name" value="<?php echo $v_banner_name;?>" required validationMessage="Please input Banner Name" />
                                        <label id="lbl_banner_name" class="k-required">(*)</label></td>
                                </tr>

                                <tr align="right" valign="top">
                                    <td>Banner Order</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input style="width:300px" type="number" id="txt_banner_order" name="txt_banner_order" value="<?php echo $v_banner_order;?>" /></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Banner Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <label><input type="checkbox" id="txt_banner_status" name="txt_banner_status"<?php echo $v_banner_status==0?' checked="checked"':'';?> /> Active?</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Banner Logo</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="file" id="txt_brand_logo" name="txt_brand_logo" accept="image/*" />
                                        <input type="hidden" name="txt_hidden_brand_logo" value = "<?php echo $v_brand_logo ; ?>" />
                                        <?php echo $v_brand_logo_link;?>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Banner Thumb</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="file" id="txt_brand_thumb" name="txt_brand_thumb" accept="image/*" />
                                        <input type="hidden" name="txt_hidden_brand_thumb" value = "<?php echo $v_brand_thumb ; ?>" />
                                        <?php echo $v_brand_thumb_link;?>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Content Content</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 98%; padding:5px;"id="txt_content_content" name="txt_content_content"><?php echo $v_content_content;?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="about_brand div_details" style="height: 900px">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Text</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input style="width:300px" class="k-textbox" type="text" id="txt_text" name="txt_text" />
                                    </td>
                                </tr>

                                <tr align="right" valign="top">
                                    <td>Language</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_language_type" name="txt_language_type">
                                            <?php echo $cls_settings->draw_option_by_key('language_setting',0,$v_language_type);?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Image Thumb</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="file" id="txt_content_image" name="txt_content_image" accept="image/*" />
                                        <span id="td_more_images"><?php echo $v_image_thumb;?><span>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Category</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_category" name="txt_category">
                                            <option value=0>Select category</option>
                                            <?php foreach($arr_qry_category as $arr){ ?>
                                                <option value="<?php echo (string)$arr['_id'];?>"> <?php echo $arr['name'];?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Content Content</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 90%; padding:5px;"id="txt_text_content" name="txt_text_content"></textarea>
                                    </td>
                                </tr>
                                <tr align="left" valign="top">
                                    <td>&nbsp</td>
                                    <td>&nbsp</td>
                                    <td><input class="k-button" id="btn_add_package" value="Add" /></td>
                                </tr>
                            </table>
                            <div style="margin-top:20px;" id="kit_grid"></div>
                        </div>
                        <div style="min-height: 990px" id="about_details div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">About brand</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="slt_about_brand" name="slt_about_brand">
                                            <option value=-1>---Select---</option>
                                            <?php for($i=0;$i<count($arr_list_about_brand);$i++){ ?>
                                                <option value="<?php echo $arr_list_about_brand[$i]['_val'];?>"><?php echo $arr_list_about_brand[$i]['_name'] ;?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Detail information</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 95%; padding:5px;"id="txt_content_content_detail" name="txt_content_content_detail"></textarea>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Product list</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input style="width:300px;display: none" class="k-textbox" type="text" id="product_banner" name="product_banner" />
                                        <input class="k-button" id="btn_search" value="Search" />
                                        <div id="dsp_list_product"></div>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="k-button" id="btn_add" value="Add" />
                                    </td>
                                </tr>
                            </table>
                            <div style="margin-top:10px;" id="grid_detail">

                            </div>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_banner" name="btn_submit_tb_banner" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
<div id="popupWindow">
    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
        <tr align="right" valign="top">
            <td style="width: 200px">Filter list below: Name/Code/SKU </td>
            <td align="left">
                <input style="width:300px" class="k-textbox" type="text" id="txt_name_product" name="txt_name_product" />
                Category <select id="category_list" name="category_list">
                </select>
                <input class="k-button" id="btn_add_prododuct" value="Add" />
            </td>
        </tr>
    </table>
    <div style="margin-top:20px" id="product_list"></div>
</div>