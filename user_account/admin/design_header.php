<?php if(!isset($v_sval)) die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
    <title><?php echo $v_main_site_title .' - '.  $v_sub_site_title;?> </title>
    <base href="<?php echo URL;?>" />

    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.cookie.js"></script>

</head>

<body>
<div id="div_wrapper">
