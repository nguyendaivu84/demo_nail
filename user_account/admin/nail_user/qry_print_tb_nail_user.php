<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_user_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_user_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_user_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_user = $cls_tb_nail_user->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_user = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_user .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_user .= '<th>Ord</th>';
$v_dsp_tb_nail_user .= '<th>User Id</th>';
$v_dsp_tb_nail_user .= '<th>Password</th>';
$v_dsp_tb_nail_user .= '<th>First Name</th>';
$v_dsp_tb_nail_user .= '<th>Middle Name</th>';
$v_dsp_tb_nail_user .= '<th>Last Name</th>';
$v_dsp_tb_nail_user .= '<th>User Lastlog</th>';
$v_dsp_tb_nail_user .= '<th>User Email</th>';
$v_dsp_tb_nail_user .= '<th>User Type</th>';
$v_dsp_tb_nail_user .= '<th>User Status</th>';
$v_dsp_tb_nail_user .= '<th>Is Subcription</th>';
$v_dsp_tb_nail_user .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_user as $arr){
	$v_dsp_tb_nail_user .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_user .= '<td align="right">'.($v_count++).'</td>';
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_password = isset($arr['password'])?$arr['password']:'';
	$v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
	$v_middle_name = isset($arr['middle_name'])?$arr['middle_name']:'';
	$v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
	$v_user_lastlog = isset($arr['user_lastlog'])?$arr['user_lastlog']:(new MongoDate(time()));
	$v_user_email = isset($arr['user_email'])?$arr['user_email']:'';
	$v_user_type = isset($arr['user_type'])?$arr['user_type']:0;
	$v_user_status = isset($arr['user_status'])?$arr['user_status']:0;
	$v_is_subcription = isset($arr['is_subcription'])?$arr['is_subcription']:'';
	$v_dsp_tb_nail_user .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_password.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_first_name.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_middle_name.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_last_name.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_user_lastlog.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_user_email.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_user_type.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_user_status.'</td>';
	$v_dsp_tb_nail_user .= '<td>'.$v_is_subcription.'</td>';
	$v_dsp_tb_nail_user .= '</tr>';
}
$v_dsp_tb_nail_user .= '</table>';
?>