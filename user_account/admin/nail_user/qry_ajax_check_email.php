<?php if(!isset($v_sval)) die();
$v_user_email = isset($_POST['txt_user_email'])?$_POST['txt_user_email']:'';
$v_user_email = trim($v_user_email);
$arr_return = array('error'=>0, 'message'=>'OK');
if($v_user_email!=''){
    $arr_where = array('$where'=>"this.user_email.toLowerCase()==='".strtolower($v_user_email)."'");
    if($cls_tb_nail_user->count($arr_where)>0){
        $arr_return['error'] = 1;
        $arr_return['message'] = 'Existing email';
    }else{
        $arr_return['message'] = 'OK';
    }
}else{
    $arr_return['error'] = 2;
    $arr_return['message'] = 'Empty value!';
}
echo json_encode($arr_return);
?>