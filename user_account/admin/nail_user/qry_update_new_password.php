<?php if(!isset($v_sval)) die();
$v_new_password = isset($_POST['txt_new_pass'])?$_POST['txt_new_pass']:'';
$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:'';
$arr_return = array('error'=>0, 'message'=>'Update password success');
if($v_new_password!='' && $v_user_id!=''){
    $arr_where = array('_id'=>new MongoId($v_user_id) );
    $cls_tb_user->update_field("user_password",md5($v_new_password),$arr_where);
}else{
    $arr_return['error'] = 2;
    $arr_return['message'] = 'Empty value!';
}
header("Content-type: application/json");
echo json_encode($arr_return);
?>