<?php if(!isset($v_sval)) die();
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_user_redirect']) && $_SESSION['ss_tb_nail_user_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_user_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_user_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_user_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_user_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_user_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_user->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_user_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_user_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_user_page'] = $v_page;
$_SESSION['ss_tb_nail_user_quick_search'] = $v_quick_search;
//End pagination
//write(json_encode($arr_where_clause),"hung.txt","a+");
$arr_tb_nail_user = $cls_tb_nail_user->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_user_type = array();
$arr_setting_user = $cls_tb_setting->get_option_by_name("user_type");
for($i=0;$i<count($arr_setting_user);$i++){
    $arr_temp = $arr_setting_user[$i];
    $arr_user_type[(int)$arr_temp['id']] = $arr_temp['name'];
}
foreach($arr_tb_nail_user as $arr){
	$v_user_id = (string)isset($arr['_id'])?$arr['_id']:'';
	$v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
	$v_middle_name = isset($arr['middle_name'])?$arr['middle_name']:'';
	$v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
	$v_user_lastlog = isset($arr['user_lastlog'])?$arr['user_lastlog']:(new MongoDate(time()));
    $v_user_lastlog = date('d-M-y h:m:s',$v_user_lastlog->sec);
	$v_user_email = isset($arr['email'])?$arr['email']:'';
	$v_user_type = isset($arr['user_type'])?$arr['user_type']:0;
    $v_user_type_name = $arr_user_type[$v_user_type];
	$v_user_status = isset($arr['user_status'])?$arr['user_status']:0;
    $v_user_status = $v_user_status==0?"Active":"Inactive";
	$v_is_subcription = isset($arr['is_subcription'])?$arr['is_subcription']:'';
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'user_id' => (string)$v_user_id,
		'first_name' => $v_first_name,
		'middle_name' => $v_middle_name,
		'last_name' => $v_last_name,
		'user_lastlog' => $v_user_lastlog,
		'user_email' => $v_user_email,
		'user_type' => $v_user_type,
		'user_type_name' => $v_user_type_name,
		'user_status' => $v_user_status
		,'user_name' => $v_user_name
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_user'=>$arr_ret_data);
echo json_encode($arr_return);
?>