<?php
    if(!isset($v_sval)) die;
    $v_user_id = isset($_POST['txt_contact_id']) ? $_POST['txt_contact_id'] : 0;
    $v_index = isset($_POST['txt_address_index']) ? $_POST['txt_address_index'] : -1;
    $arr_return = array('error'=>1,'message'=>'Invalid data');
    settype($v_user_id,"int");
    settype($v_index,"int");
    if($v_user_id>0 && $v_index>=0){
        $arr_addresses = $cls_tb_nail_user->select_scalar("addresses",array("contact_id"=>$v_user_id));
        if(count($arr_addresses)>0){
            $arr_new_address = array();
            for($i=0;$i<count($arr_addresses);$i++){
                if($i==$v_index) continue;
                $arr_new_address[] = $arr_addresses[$i];
            }
            $v_result = $cls_tb_nail_user->update_field("addresses",$arr_new_address,array("contact_id"=>$v_user_id));
            if($v_result){
                $arr_return['error'] = 0;
                $arr_return['message'] = "Ok";
            }
        }
    }
?>