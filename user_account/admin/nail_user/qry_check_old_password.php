<?php if(!isset($v_sval)) die();
$v_old_password = isset($_POST['txt_old_pass'])?$_POST['txt_old_pass']:'';
$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:'';
$arr_return = array('error'=>0, 'message'=>'OK');
if($v_old_password!='' && $v_user_id!=''){
    $arr_where = array('_id'=>new MongoId($v_user_id) , "user_password"=>md5($v_old_password));
    if($cls_tb_user->count($arr_where)<=0){
        $arr_return['error'] = 1;
        $arr_return['message'] = 'Wrong password';
    }
}else{
    $arr_return['error'] = 2;
    $arr_return['message'] = 'Empty value!';
}
header("Content-type: application/json");
echo json_encode($arr_return);
?>