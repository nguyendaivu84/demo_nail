<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_user").click(function(e){
		var css = '';
		var first_name = $("input#txt_first_name").val();
		first_name = $.trim(first_name);
		css = first_name==''?'':'none';
		$("label#lbl_first_name").css("display",css);
		if(css == '') {
            alert("first name");
            return false;
        }
		var last_name = $("input#txt_last_name").val();
		last_name = $.trim(last_name);
		css = last_name==''?'':'none';
		$("label#lbl_last_name").css("display",css);
		if(css == '') {
            alert("last name");
            return false;
        }
		var user_email = $("input#txt_user_email").val();
		user_email = $.trim(user_email);
		css = user_email==''?'':'none';
		$("label#lbl_user_email").css("display",css);
		if(css == '') {
            alert("email");
            return false;
        }
        var error_ = $("#txt_error").val();
        error_ = parseInt(error_,10);
        if(error_ >0) {
            alert("Exsting email");
            return false;
        }
        error_ = $("#txt_error_user_name").val();
        error_ = parseInt(error_,10);
        if(error_ >0){
            alert("Exsting username ");
            return false;
        }
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
    $('select#txt_user_type').width(300).kendoComboBox();
    $('input#txt_user_name').focusout(function(){
        var username = $.trim($(this).val());
        $this = $(this);
        if(username==''){
            $(this).val('');
            $('input#txt_hidden_user_name').val('N');
            validator.validate();
            return false;
        }
        $.ajax({
            url :'<?php echo URL.$v_admin_key;?>/ajax',
            type:'POST',
            data: {txt_session_id: '<?php echo session_id();?>', txt_user_name: username, txt_ajax_type: 'check_exist_user'},
            beforeSend:function(){
                $this.prop('disabled', true);
                $('span#sp_user_name').addClass('k-info-colored');
                $('span#sp_user_name').html('Checking...');
            },
            success: function(data, status){
                $('span#sp_user_name').html('');
                var ret = $.parseJSON(data);
                if(ret.error==0){
                    $("#txt_error_user_name").val(0);
                    $('input#txt_hidden_user_name').val('Y');
                }else{
                    $("#txt_error_user_name").val(1);
                    $('input#txt_hidden_user_name').val('');
                }
                validator.validate();
                $this.prop('disabled', false);
            }
        });

    });
    $('input#txt_user_email').focusout(function(){
        var email = $.trim($(this).val());
        $this = $(this);
        if(email==''){
            alert("Email is empty!");
            return false;
        }
        $.ajax({
            url :'<?php echo URL.$v_admin_key;?>/ajax',
            type:'POST',
            data: {txt_user_email: email, txt_ajax_type: 'check_email'},
            beforeSend:function(){
                $this.prop('disabled', true);
            },
            success: function(data, status){
                var ret = $.parseJSON(data);
                if(ret.error >0){
                    $("#txt_error").val(1);
                    alert(ret.message);
                }else $("#txt_error").val(0);
                $this.prop('disabled', false);
            }
        });

    });
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
    $("#txt_old_pass").on("focusout",function(){
        var old_pass = $(this).val();
        check_old_password(old_pass,'<?php echo $v_mongo_id ; ?>',"check_old_password",'<?php echo URL.$v_admin_key;?>/ajax',"lbl_old_pass","check_old_pass");
    });
    $("#btn_change_password").on("click",function(){
        var check_old = $("#check_old_pass").val();
        var new_pass = $("#txt_new_password").val();
        if(check_old==1){
            update_new_password(new_pass,'<?php echo $v_mongo_id ; ?>',"update_new_password",'<?php echo URL.$v_admin_key;?>/ajax',"","");
        }else{
            alert("Old password is invalid!");
        }

    });

    //======================================== grid address book
    var user_id = '<?php echo $v_user_id ?>';
    var grid = $("#grid_user_adressbook").kendoGrid({
        dataSource: {
            pageSize: 12,
            page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: {
                    url: "<?php echo URL.$v_admin_key;?>/json/",
                    type: "POST",
                    data: {txt_user_id:user_id,txt_json_type:"load_all_address_book",txt_quick_search:"<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>"}
                }
            },
            schema: {
                data: "tb_addressbook"
                ,total: function(data){
                    return data.total_rows;
                }
            },
            type: "json"
        },
        pageSize: 12,
        height: 380,
        scrollable: true,
        sortable: true,
        pageable: {
            input: true,
            refresh: true,
            pageSizes: [10, 20, 30, 40, 50],
            numeric: false
        },
        columns: [
            {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
            {field: "full_name", title: "Full name", type:"string", width:"80px", sortable: true},
            {field: "full_address", title: "Full address", type:"string", width:"60px", sortable: true },
            {field: "full_information", title: "Full information", type:"string", width:"150px", sortable: false},
            {field: "default_address", title: "Default address book", type:"string", width:"80px", sortable: true },
            { command:  [
                <?php if($v_delete_right || $v_is_admin) { ?>
                { name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                <?php }?>
            ],
                title: " ", width: "30px" }
        ]
    }).data("kendoGrid");
    function delete_row(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var address_index = dataItem.address_index;
        if(confirm("Are you sure you want to delete this address ?")){
            delete_address(address_index,'<?php echo $v_user_id; ?>');
            $(e.currentTarget).closest("tr").remove();
        }
    }
    //======================================== end

});
function delete_address(index,user_id){
    var company_id = <?php echo $v_company_id; ?>;
    $.ajax({
        url     : '<?php echo URL.$v_admin_key;?>/ajax',
        type    : 'POST',
        data    : {txt_address_index:index,txt_contact_id:user_id, txt_ajax_type:'delete_addresses'},
        beforeSend: function(){
        }
        ,success: function(data, status){
        }
        ,error:function(data){
            //alert(data.responseText);
        }
    });
}
</script>
<style>

</style>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>User</h3>
                    </div>
                    <form id="frm_tb_nail_user" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_user_id.'/editid';?>" method="POST">
                        <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                        <input type="hidden" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" />
                        <input type="hidden" id="txt_error" name="txt_error" value="0" />
                        <input type="hidden" id="txt_error_user_name" name="txt_error_user_name" value="0" />
                        <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <?php if(!empty($v_user_id)) { ?>
                                <li>Chang password</li>
                            <?php } ?>
                            <li>Users's address book</li>
                        </ul>
                        <div class="information div_details" >
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">User Name</td>
                                    <td style="width:1px;">&nbsp;</td>
                                    <?php if($v_user_id <=0 ){ ?>
                                        <td align="left">
                                            <input class="text_css k-textbox" size="50" type="text" id="txt_user_name" name="txt_user_name" value="<?php echo $v_user_name;?>" required validationMessage="Please input username" /> <label id="lbl_first_name" class="k-required">(*)</label>
                                            <input type="hidden" id="txt_hidden_user_name" name="txt_hidden_user_name" required validationMessage="Duplicate username" value="<?php echo $v_user_name==''?'Y':'N';?>" />
                                        </td>
                                    <?php }else{ ?>
                                        <td align="left">
                                            <span><?php echo $v_user_name ; ?></span>
                                            <input type="hidden" id="txt_hidden_user_name" name="txt_hidden_user_name" value="<?php echo $v_user_name ;?>" />
                                        </td>
                                    <?php }?>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width:200px">First Name</td>
                                    <td style="width:1px;">&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_first_name" name="txt_first_name" value="<?php echo $v_first_name;?>" /> <label id="lbl_first_name" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Middle Name</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_middle_name" name="txt_middle_name" value="<?php echo $v_middle_name;?>" /> <label id="lbl_middle_name" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Last Name</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_last_name" name="txt_last_name" value="<?php echo $v_last_name;?>" /> <label id="lbl_last_name" class="k-required">(*)</label></td>
                                </tr>
                                <?php if($v_user_id<=0){ ?>
                                    <tr align="right" valign="top">
                                        <td style="width:200px">Password</td>
                                        <td style="width:1px;">&nbsp;</td>
                                        <td align="left"><input class="text_css k-textbox" size="50" type="password" id="txt_password" name="txt_password" value="<?php echo $v_password;?>" required validationMessage="Please input password" /> <label id="lbl_password" class="k-required">(*)</label></td>
                                    </tr>
                                <?php }?>
                                <tr align="right" valign="top">
                                    <td>Email</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="email" id="txt_user_email" name="txt_user_email" value="<?php echo $v_user_email;?>" required validationMessage="Please input email" data-email-msg="Invalid email" />
                                        <label id="lbl_user_email" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>User Type</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_user_type" name="txt_user_type">
                                            <?php echo $v_dsp_user_type_option ; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>User Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="checkbox" id="txt_user_status" name="txt_user_status" <?php echo $v_user_status==0 ? "checked":""; ?> /> Active ?
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <?php if(!empty($v_user_id)) { ?>
                            <div class="change_pass div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">Old password</td>
                                    <td style="width:1px;">&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="password" id="txt_old_pass" name="txt_old_pass" value="" />
                                        <input type="hidden" id="check_old_pass" value="0">
                                        <label id="lbl_old_pass" for="txt_old_pass" class="k-required"></label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>New password</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="password" id="txt_new_password" name="txt_new_password" value="" />
                                        <label id="txt_new_password" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="button" id="btn_change_password" name="btn_change_password" value="Change pass" class="k-button button_css" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <?php }?>
                        <div class="other div_details" style="height:500px;"  >
                            <div id="grid_user_adressbook" style="padding-bottom: 30px;" ></div>
                        </div>
                </div>
               <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
               <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                        <?php echo $v_error_message;?>
                    </div>
               <?php }?>
               <div class="k-block k-widget div_buttons">
                <input type="submit" id="btn_submit_tb_nail_user" name="btn_submit_tb_nail_user" value="Submit" class="k-button button_css" />
                </div>
            <?php }?>
            </form>
            </div>
        </div>
    </div>
</div>
