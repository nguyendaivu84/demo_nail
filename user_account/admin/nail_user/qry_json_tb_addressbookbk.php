<?php if(!isset($v_sval)) die;
$arr_where_clause = array();
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:0;
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_user_id,"int");
if($v_user_id>0) $arr_where_clause['user_id'] = $v_user_id;
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        if($arr_temp[$i]['field'] == "full_name") $arr_sort = array("full_name"=>$arr_temp[$i]['dir']=='asc'?1:-1);
        else $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
add_class("cls_tb_nail_addressbook");
$cls_tb_nail_addressbook = new cls_tb_nail_addressbook($db);
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_user_redirect']) && $_SESSION['ss_tb_nail_user_redirect']==1){
    if(isset($_SESSION['ss_tb_nail_user_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_nail_user_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_nail_user_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_nail_user_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_nail_user_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_addressbook->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_user_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_user_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_location_page'] = $v_page;
$_SESSION['ss_tb_location_quick_search'] = $v_quick_search;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("full_name"=>1);
$arr_tb_location = $cls_tb_nail_addressbook->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_company = array();
add_class("cls_tb_country");
$cls_tb_country = new cls_tb_country($db);
$arr_select_country = $cls_tb_country->select();
$arr_country = array();
foreach($arr_select_country as $arr){
    $arr_country[$arr['country_id']] = $arr['country_name'];
}

$arr_state = array();
add_class("cls_tb_province");
$cls_tb_province = new cls_tb_province($db);
$arr_select_province = $cls_tb_province->select(array());
foreach($arr_select_province as $arr){
    $arr_state[$arr['province_id']] = $arr['province_name'];
}
foreach($arr_tb_location as $arr){
    $v_address_id = isset($arr['address_id'])?$arr['address_id']:0;
    $v_full_name = isset($arr['full_name'])?$arr['full_name']:'';
    $v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
    $v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
    $v_address_city = isset($arr['city_name'])?$arr['city_name']:'';
    $v_address_postal = isset($arr['address_postal'])?$arr['address_postal']:'';
    $v_address_province = isset($arr['state_id'])?$arr['state_id']:0;
    $v_address_province = $arr_state[$v_address_province];
    $v_address_country = isset($arr['country_id'])?$arr['country_id']:0;
    $v_address_country = $arr_country[$v_address_country];
    $v_phone = isset($arr['phone'])?$arr['phone']:'';
    $v_status = isset($arr['default_address'])?$arr['default_address']:0;
    $v_status = $v_status==1?"Yes":"No";
    $v_full_address = $v_address_country." - ".$v_address_city." - ".$v_address_province;
    $v_full_information = $v_address_line_1." - ".$v_address_line_2." - ".$v_address_postal." - ".$v_phone;
    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'full_name'=>$v_full_name,
        'full_address'=>$v_full_address,
        'full_information'=>$v_full_information,
        'default_address'=>$v_status
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_addressbook'=>$arr_ret_data);
echo json_encode($arr_return);
?>