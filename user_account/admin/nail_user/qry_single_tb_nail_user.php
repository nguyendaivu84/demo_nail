<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_user_id = 0;
$v_password = '';
$v_first_name = '';
$v_middle_name = '';
$v_last_name = '';
$v_user_lastlog = date('Y-m-d H:i:s', time());
$v_user_email = '';
$v_user_type = 0;
$arr_user_type_list = array();
$v_user_status = 0;
$v_user_name = '';
$v_old_password = '';
$v_is_subcription = '';
$v_new_nail_user = true;
if(isset($_POST['btn_submit_tb_nail_user'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_user->set_mongo_id($v_mongo_id);
	$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:$v_user_id;
	if(is_null($v_mongo_id)){
		$v_user_id = $cls_tb_nail_user->select_next('contact_id');
	}
	$v_user_id = (int) $v_user_id;
	$cls_tb_nail_user->set_user_id($v_user_id);
    if(is_null($v_mongo_id)){
        $v_password = isset($_POST['txt_password'])?$_POST['txt_password']:$v_password;
        $v_password = trim($v_password);
        if($v_password=='') $v_error_message .= '[Password] is empty!<br />';
        $cls_tb_nail_user->set_password(md5($v_password));

        $v_user_name = isset($_POST['txt_user_name'])?$_POST['txt_user_name']:$v_user_name;
        $v_user_name = trim($v_user_name);
        if($v_user_name=='') $v_error_message .= '[Username] is empty!<br />';
        $cls_tb_nail_user->set_user_name($v_user_name);
    }
	$v_first_name = isset($_POST['txt_first_name'])?$_POST['txt_first_name']:$v_first_name;
	$v_first_name = trim($v_first_name);
	if($v_first_name=='') $v_error_message .= '[First Name] is empty!<br />';
	$cls_tb_nail_user->set_first_name($v_first_name);
	$v_middle_name = isset($_POST['txt_middle_name'])?$_POST['txt_middle_name']:$v_middle_name;
	$v_middle_name = trim($v_middle_name);
	if($v_middle_name=='') $v_error_message .= '[Middle Name] is empty!<br />';
	$cls_tb_nail_user->set_middle_name($v_middle_name);
	$v_last_name = isset($_POST['txt_last_name'])?$_POST['txt_last_name']:$v_last_name;
	$v_last_name = trim($v_last_name);
	if($v_last_name=='') $v_error_message .= '[Last Name] is empty!<br />';
	$cls_tb_nail_user->set_last_name($v_last_name);
	$v_user_lastlog = date('d-M-y h:m:s');

	$cls_tb_nail_user->set_user_lastlog($v_user_lastlog);
	$v_user_email = isset($_POST['txt_user_email'])?$_POST['txt_user_email']:$v_user_email;
	$v_user_email = trim($v_user_email);
	if($v_user_email=='') $v_error_message .= '[User Email] is empty!<br />';
	$cls_tb_nail_user->set_user_email($v_user_email);
	$v_user_type = isset($_POST['txt_user_type'])?$_POST['txt_user_type']:$v_user_type;
	$cls_tb_nail_user->set_user_type((int)$v_user_type);
	$v_user_status = isset($_POST['txt_user_status'])?$_POST['txt_user_status']:$v_user_status;
    if($v_user_status=='on') $v_user_status = 0;
    else $v_user_status = 1;
	$v_user_status = (int) $v_user_status;
	if($v_user_status<0) $v_error_message .= '[User Status] is negative!<br />';
	$cls_tb_nail_user->set_user_status($v_user_status);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_user->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_user->update(array('_id' => $v_mongo_id));
			$v_new_nail_user = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_user_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_user) $v_nail_user_id = 0;
		}
	}
}else{
	$v_user_id= isset($_GET['id'])?$_GET['id']:'';
	if($v_user_id!=''){
		$v_row = $cls_tb_nail_user->select_one(array('_id' => new MongoId($v_user_id)));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_user->get_mongo_id();
			$v_user_id = (string)$cls_tb_nail_user->get_mongo_id();
			$v_password = $cls_tb_nail_user->get_password();
			$v_first_name = $cls_tb_nail_user->get_first_name();
			$v_middle_name = $cls_tb_nail_user->get_middle_name();
			$v_last_name = $cls_tb_nail_user->get_last_name();
			$v_user_lastlog = date('Y-m-d H:i:s',$cls_tb_nail_user->get_user_lastlog());
			$v_user_email = $cls_tb_nail_user->get_user_email();
			$v_user_type = $cls_tb_nail_user->get_user_type();
			$v_user_status = $cls_tb_nail_user->get_user_status();
            $v_user_name = $cls_tb_nail_user->get_user_name();
            $v_old_password = $cls_tb_nail_user->get_password();
			$v_is_subcription = $cls_tb_nail_user->get_is_subcription();
		}
	}
}
$arr_your_user = unserialize($_SESSION['ss_user']);
$v_your_user_type = (int)$arr_your_user['user_type'];
$arr_exclude = array();
$arr_setting_option = $cls_settings->select(array('setting_name'=>'user_type'));
for($i=0;$i<count($arr_setting_option);$i++){
    if($v_your_user_type>$i)
        $arr_exclude[] = $i;
}
$v_dsp_user_type_option = $cls_settings->draw_option_by_id('user_type', 0, $v_user_type,$arr_exclude);
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>