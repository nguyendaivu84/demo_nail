<?php if(!isset($v_sval)) die();?>
<?php
$v_root_key = isset($_GET['key'])?$_GET['key']:'';
$cls_tb_order  = new cls_tb_order($db);


$arr_where = array('status'=>array('$gte' => 1));

$v_user_company_id = isset($arr_user['company_id'])?$arr_user['company_id']:'0';
settype($v_user_company_id, 'int');
if($v_user_company_id>0 && $v_user_company_id!=10000) $arr_where['company_id']=$v_user_company_id;


$arr_order = array('date_created'=>-1);
$arr_order = $cls_tb_order->select_limit(0,5,$arr_where,$arr_order);
$v_list_order = '<table cellpadding="2" cellspacing="2" border="1" width="100%"  class="list_table">';
foreach ($arr_order as $arr) {
    $v_order_id  = isset($arr['order_id'])?$arr['order_id']:0;
    $v_order_ref = isset($arr['order_ref'])?$arr['order_ref']:'';
    $v_po_number = isset($arr['po_number'])?$arr['po_number']:'';
    $v_status = isset($arr['status'])?$arr['status']:0;
    $v_total_order_amount = isset($arr['total_order_amount'])?$arr['total_order_amount']:0;
    $v_cost_kitting = isset($arr['cost_kitting'])?$arr['cost_kitting']:0;
    $v_date_created = isset($arr['date_created'])?date('d-M-y',$arr['date_created']->sec):date('d-M-y', time());
    $v_total_order_amount += $v_cost_kitting;
    $v_list_order .= '<tr>
                        <td>PO Number :'. $v_po_number . "  -- Order Ref: ". $v_order_ref.'
                            <br /><span class="note"> Date Created: '.$v_date_created.'</span>
                        </td>
                        <td><b>'. format_currency($v_total_order_amount).'</b></td>
                        <td >'.$cls_settings->get_option_name_by_id('order_status',$v_status,0).'</td>
                    </tr>';
}
$v_list_order .= '</table>';


/* Shipping */
$v_count = 1;
$v_count_location = 0;
$v_temp_location_id = 0;

$cls_tb_allocation = new cls_tb_allocation($db);
$arr_where_clause = array();

if($v_user_company_id!=0 && $v_user_company_id!=10000) $arr_where_clause['company_id'] = $v_user_company_id;

$arr_sort = array('date_shipping'=>-1,'location_name'=>1);
$arr_tb_allocation = $cls_tb_allocation->select_limit(0,5,$arr_where_clause,$arr_sort);
$v_list_shipping = '<table cellpadding="2" cellspacing="2" border="1" width="100%"  class="list_table">';
foreach($arr_tb_allocation as $arr){
    $v_allocation_id = isset($arr['allocation_id'])?$arr['allocation_id']:0;
    $v_order_items_id = isset($arr['order_items_id'])?$arr['order_items_id']:0;
    $v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
    $v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
    $v_location_name = isset($arr['location_name'])?$arr['location_name']:'';
    $v_location_address = isset($arr['location_address'])?$arr['location_address']:'';
    $v_quantity = isset($arr['quantity'])?$arr['quantity']:1;
    $v_shipper = isset($arr['shipper'])?$arr['shipper']:'';
    $v_tracking_number = isset($arr['tracking_number'])?$arr['tracking_number']:'';
    $v_shipping_url = isset($arr['tracking_url'])?$arr['tracking_url']:'';
    $v_ship_status = isset($arr['ship_status'])?$arr['ship_status']:'';
    $v_date_shipping = isset($arr['date_shipping'])?date('d-M-Y',$arr['date_shipping']->sec):'';
    if($v_temp_location_id!=$v_location_id){
        $v_count_location++;
        $v_temp_location_id = $v_location_id;
    }

    $v_bgcolor = "#FFFFFF";
    if($v_count_location%2==0)
        $v_bgcolor = "#98FB98";


    $v_list_shipping .= '<tr align="left" valign="middle" >';
    $v_list_shipping .= '<td align="right" bgcolor="'.$v_bgcolor.'">'.($v_count++).'</td>';
    $v_list_shipping .= '<td bgcolor="'.$v_bgcolor.'">'.$v_location_name.'</td>';
    $v_list_shipping .= '<td bgcolor="'.$v_bgcolor.'">'.$v_location_address.'</td>';
    $v_list_shipping .= '<td bgcolor="'.$v_bgcolor.'">'.$v_tracking_number.'</td>';
    $v_list_shipping .= '<td bgcolor="'.$v_bgcolor.'">'.fdate($v_date_shipping).'</td>';
    $v_list_shipping .= '</tr>';
}
$v_list_shipping .='</table>';


/*Lis Charts */

//$v_date = date('Y-m-d');
//$v_ts = strtotime($v_date);
//$v_ts = time();
$v_ts = time() - 7*3600*24;
//$v_dow = date('w', $v_ts);
//$v_offset = $v_dow - 1;
//if ($v_offset < 0) $v_offset = 6;
//$v_ts = $v_ts - $v_offset*86400;
$arr_date_charts = array();
$arr_total_order = array();
$arr_all_order_chart = array();
$arr_company_id = array();
$v_relation_ship_id = $cls_settings->get_option_id_by_key("relationship","customer");
settype($v_relation_ship_id,"int");
$v_company_select = $cls_tb_company->select(array("relationship"=>$v_relation_ship_id));
foreach($v_company_select as $arr){
    $v_temp = (int)$arr['company_id'];
    if($v_temp !=0 && $v_temp!=10000)
        $arr_company_id[] = $v_temp;
}
if($v_user_company_id!=0 && $v_user_company_id!=10000){
    for ($i = 0; $i < 7; $i++) {
        $arr_date_charts[$i] = date("d-M-Y", $v_ts + ($i+1) * 86400);
        $v_check_date = new MongoDate(strtotime(date("Y-m-d", $v_ts + $i * 86400)));
        $arr_total_order[$i] = $cls_tb_order->sum(array('company_id'=>$v_user_company_id,'status'=>array('$gte'=>2),
            'date_created'=>array('$gte' => new MongoDate(strtotime(date("Y-m-d 00:00:00", $v_ts + $i * 86400))),
                '$lte' => new MongoDate(strtotime(date("Y-m-d 23:59:59", $v_ts + $i * 86400))))));
    }
}else{
    for($j=0;$j<count($arr_company_id);$j++){
        $v_temp = (int)$arr_company_id[$j];
        $arr_total_order = array();
        for ($i = 0; $i < 7; $i++) {
            $arr_date_charts[$i] = date("d-M-Y", $v_ts + ($i+1) * 86400);
            $v_check_date = new MongoDate(strtotime(date("Y-m-d", $v_ts + $i * 86400)));
            $v_sum = $cls_tb_order->sum(array('company_id'=>$v_temp,'status'=>array('$gte'=>2),
                'date_created'=>array('$gte' => new MongoDate(strtotime(date("Y-m-d 00:00:00", $v_ts + $i * 86400))),
                    '$lte' => new MongoDate(strtotime(date("Y-m-d 23:59:59", $v_ts + $i * 86400))))));
            //if($v_sum >0)
                $arr_total_order[$i] = $v_sum;
        }
        //if(count($arr_total_order)>0)
            $arr_all_order_chart [] = array('chart'=>$arr_total_order,'company_name'=>$cls_tb_company->select_scalar("company_name",array("company_id"=>$v_temp)));
    }
}
//die(var_dump($arr_date_charts));
/* Statistics Order */
$cls_settings = new cls_settings($db);
$arr_where = array('setting_name'=>'order_status');
$arr_options = $cls_settings->select($arr_where);

$v_statistics_order = '<table cellpadding="2" cellspacing="2" border="1" width="100%"  class="list_table">
                <tr>
                    <th colspan="5"> Statistics Order!.... </th>
                </tr>';
foreach ($arr_options as $arr) {
    $v_static = isset($arr['id']) ? $arr['id'] : 0;
    $v_static_name  = isset($arr['name']) ? $arr['name'] : '';
    $v_count_order = $cls_tb_order->count(array('status'=>(int)$v_static));

    $v_statistics_order .= '<tr>
                                <td>'.$v_static_name.'</td>
                                <td align="right"><b>'.$v_count_order.'</b></td>
                            </tr>';
}
$v_statistics_order .='</table>';
//echo $v_year.$v_month.$v_day;

//$v_temp = $v_temp->sec;
//strtotime($v_temp);
$v_ts = time() - 7*3600*24;
$v_queyry_date = new MongoDate(strtotime(date("Y-m-d 00:00:00", $v_ts )));
$arr_where = array('status'=>array('$gte'=>1),'date_created'=>array('$gte' => $v_queyry_date));
if($v_user_company_id>0 && $v_user_company_id!=10000) $arr_where['company_id']=$v_user_company_id;
$arr_order = array('date_create'=>-1);
//die(var_dump($arr_where));
$arr_20_order = $cls_tb_order->select_limit(0,20,$arr_where);
$v_list_20_order = '<table cellpadding="2" cellspacing="2" border="1" width="100%"  class="list_table">';
$v_list_20_order .= "<tr>
                        <th>&nbsp</th>
                        <th>Company</th>
                        <th>Po Number</th>
                        <th>Order Ref</th>
                        <th>Total Amount</th>
                        <th>Date Create</th>
                        <th>Order status</th>
                        <th>Action</th>
                    </tr>";
$v_row_id = 1;
add_class("cls_tb_company");
$v_company_name = '';
$cls_tb_company = new cls_tb_company($db);
foreach ($arr_20_order as $arr) {
    $v_order_id  = isset($arr['order_id'])?$arr['order_id']:0;
    $v_company_name  = $cls_tb_company->select_scalar("company_name",array("company_id"=>(int)$arr['company_id']));
    $v_order_ref = isset($arr['order_ref'])?$arr['order_ref']:'';
    $v_po_number = isset($arr['po_number'])?$arr['po_number']:'';
    $v_status = isset($arr['status'])?$arr['status']:0;
    $v_total_order_amount = isset($arr['total_order_amount'])?$arr['total_order_amount']:0;
    $v_cost_kitting = isset($arr['cost_kitting'])?$arr['cost_kitting']:0;
    $v_date_created = isset($arr['date_created'])?date('d-M-y',$arr['date_created']->sec):date('d-M-y', time());
    $v_total_order_amount += $v_cost_kitting;
    $v_list_20_order .= '<tr>
                        <td>'.$v_row_id.'</td>
                        <td>'.$v_company_name.'</td>
                        <td>'.$v_po_number.'</td>
                        <td>'.$v_order_ref.'</td>
                        <td>'.$v_total_order_amount.'</td>
                        <td>'.$v_date_created.'</td>
                        <td>'.$cls_settings->get_option_name_by_id('order_status',$v_status,0).'</td>
                        <td>
                            <a title="edit kitting price cost" href="'.URL."admin/order/order/".$v_order_id."/edit".'"><img src='. URL."images/icons/tab_edit.png".' /></a>
                            <a title="view order" href="'.URL."admin/order/order/".$v_order_id."/view".'"><img src='. URL."images/icons/view.png".' /></a>
                            <a title="print sumarize" href="'.URL."admin/order/order/".$v_order_id."/printsummarize".'"><img src='. URL."images/icons/order_product_plan.png".' /></a>
                        </td>
                    </tr>';
    $v_row_id ++;
}
$v_list_20_order .= '</table>';


?>