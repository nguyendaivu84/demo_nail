<?php if(!isset($v_sval)) die();?>
<script>
    $(function () {
        var chart;
        $(document).ready(function() {
            $("#list_20_order").hide();
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container',
                    type: 'line'
                },
                title: {
                    text: 'Total Orders in this week'
                },
                subtitle: {
                    text: 'From <?php echo $arr_date_charts[0];?> To  <?php echo $arr_date_charts[6];?>'
                },
                xAxis: {
                    categories:
                        [
                            <?php

                                for ($i = 0; $i < 7; $i++) {
                                   if($i==6)
                                        echo "'". $arr_date_charts[$i]."'";
                                   else
                                       echo "'".$arr_date_charts[$i] ."',";

                               }

                            ?>
                        ]
                },
                yAxis: {
                    title: {
                        text: 'Number Orders'
                    }
                }
                ,
                tooltip: {
                    enabled: false,
                    formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                            this.x +': '+ this.y +'°C';
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                <?php if($v_user_company_id!=0 && $v_user_company_id!=10000){ ?>
                series: [{
                    name: '<?php echo $v_company_name; ?>'
                    ,
                    data: [
                        <?php
                         for ($i = 0; $i < 7; $i++) {
                                if($i==6)
                                    echo $arr_total_order[$i];
                                else
                                    echo $arr_total_order[$i] .",";
                            }
                            /*
                             for ($i = 0; $i < count($arr_all_order_chart); $i++) {
                                $arr_chart = $arr_all_order_chart[$i];
                                for ($j = 0; $j < 7; $j++) {
                                    if($j==6)
                                        echo $arr_chart[$j];
                                    else
                                        echo $arr_chart[$j] .",";
                                }
                            }
                             */
                        ?>
                    ]
                }]
                <?php }else{?>
                series: [
                    <?php  for($j=0;$j<count($arr_all_order_chart);$j++){ ?>
                    {
                        name: "<?php echo $arr_all_order_chart[$j]['company_name'];?>",
                        data: [
                            <?php
                             $arr_temp = $arr_all_order_chart[$j]['chart'];
                             for ($i = 0; $i < 7; $i++) {
                                    if($i==6)
                                        echo $arr_temp[$i];
                                    else
                                        echo ($arr_temp[$i]) .",";
                                }
                            ?>
                        ]
                    }
                    <?php if($j < count($arr_all_order_chart)-1) {?> , <?php } ?>
                    <?php } ?>
                ]
                <?php } ?>
            });
            $('button#btn_change_').on("click",function(){
                var text = $(this).val();
                if(text ==1){
                    $(this).val("2");
                    $(this).text("View Graph");
                    $('#container').hide();
                    $('#list_20_order').show();
                }else{
                    $(this).val("1");
                    $(this).text("View List");
                    $('#container').show();
                    $('#list_20_order').hide();
                }
                //chart.series[0].show();
            });
        });
    });
</script>
<div class="pane-content">
    <div class="k-block k-widget" style="margin-bottom: 20px; padding-left: 20px">
        <h3>SYSTEM</h3>
    </div>
    <div id="leftNav" class="k-block k-info-colored" style="float: left; width: 48%; text-align: left">
        <div class="k-header k-shadow" style="text-align: center">
            <a href="#">5 recent orders</a>
        </div>
        <?php echo $v_list_order; ?>
        <div><a href="<?php echo URL;?>admin/order/order" style="color:blue;">See more ...</a></div>
    </div>
    <div id="rightNav" class="k-block k-info-colored" style="margin-left: 52% ; text-align: left">
        <div class="k-header k-shadow" style="text-align: center"><a href="#">5 recent shippings</a></div>
        <?php echo $v_list_shipping; ?>
        <div style="text-align: right"><a style="color:blue;" href="<?php echo URL;?>admin/shipping/shipping">See more ...</a></div>
    </div>
</div>
<?php  echo js_hight_chart(); ?>
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto; clear:both"></div>
<div id="list_20_order" class="k-block k-info-colored" style="min-width: 400px;min-height: 10px; max-height: 530px; margin: 0 auto;margin-left:10px;margin-right:10px; clear:both">
    <?php echo $v_list_20_order; ?>
</div>
<div class="k-block k-widget div_buttons">
    <button id="btn_change_" class="k-button button_css" type="button" name="btn_change_" value="1 ">View List</button>
</div>

<style>
    a:hover{
        text-decoration: underline;
    }
</style>