<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_global_redirect']) && $_SESSION['ss_tb_global_redirect']==1){
	if(isset($_SESSION['ss_tb_global_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_global_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_global_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_global_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_global_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_global->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_global_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_global_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_global_page'] = $v_page;
$_SESSION['ss_tb_global_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_global = $cls_tb_global->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_global as $arr){
	$v_global_id = isset($arr['global_id'])?$arr['global_id']:0;
	$v_global_key = isset($arr['global_key'])?$arr['global_key']:'';
	$v_global_name = isset($arr['global_name'])?$arr['global_name']:'';
	$v_global_description = isset($arr['global_description'])?$arr['global_description']:'';
	$v_global_value = isset($arr['global_value'])?$arr['global_value']:'';
	$v_setting_name = isset($arr['setting_name'])?$arr['setting_name']:'';
	$v_setting_key = isset($arr['setting_key'])?$arr['setting_key']:'';
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'global_id' => $v_global_id,
		'global_key' => $v_global_key,
		'global_name' => $v_global_name,
		'global_description' => $v_global_description,
		'global_value' => $cls_settings->get_option_name_by_id('status',$v_global_value),
		'setting_name' => $v_setting_name,
		'setting_key' => $v_setting_key
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_global'=>$arr_ret_data);
echo json_encode($arr_return);
?>