<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_global").click(function(e){
		var css = '';

		var global_id = $("input#txt_global_id").val();
		global_id = parseInt(global_id, 10);
		css = isNaN(global_id)?'':'none';
		$("label#lbl_global_id").css("display",css);
		if(css == '') return false;
		var global_key = $("input#txt_global_key").val();
		global_key = $.trim(global_key);
		css = global_key==''?'':'none';
		$("label#lbl_global_key").css("display",css);
		if(css == '') return false;
		var global_name = $("input#txt_global_name").val();
		global_name = $.trim(global_name);
		css = global_name==''?'':'none';
		$("label#lbl_global_name").css("display",css);
		if(css == '') return false;
		var global_description = $("input#txt_global_description").val();
		global_description = $.trim(global_description);
		css = global_description==''?'':'none';
		$("label#lbl_global_description").css("display",css);
		if(css == '') return false;

		var setting_name = $("input#txt_setting_name").val();
		setting_name = $.trim(setting_name);
		css = setting_name==''?'':'none';
		$("label#lbl_setting_name").css("display",css);
		if(css == '') return false;
		var setting_key = $("input#txt_setting_key").val();
		setting_key = $.trim(setting_key);
		css = setting_key==''?'':'none';
		$("label#lbl_setting_key").css("display",css);
		if(css == '') return false;
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");

    var combo_global_value = $('select#txt_global_value').data('kendoComboBox');
        $('select#txt_global_value').width(150).kendoComboBox();

	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_global').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Global</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                    </div>

<form id="frm_tb_global" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_global_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_global_id" name="txt_global_id" value="<?php echo $v_global_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>Global Key</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_global_key" name="txt_global_key" value="<?php echo $v_global_key;?>" /> <label id="lbl_global_key" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Global Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_global_name" name="txt_global_name" value="<?php echo $v_global_name;?>" /> <label id="lbl_global_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Global Description</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_global_description" name="txt_global_description" value="<?php echo $v_global_description;?>" /> <label id="lbl_global_description" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Global Value</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_global_value" name="txt_global_value">
                <?php
                echo $cls_settings->draw_option_by_id('status',0,$v_global_value);
                ?>
            </select>
            <label id="lbl_global_value" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Setting Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_setting_name" name="txt_setting_name" value="<?php echo $v_setting_name;?>" /> <label id="lbl_setting_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Setting Key</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_setting_key" name="txt_setting_key" value="<?php echo $v_setting_key;?>" /> <label id="lbl_setting_key" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_global" name="btn_submit_tb_global" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
