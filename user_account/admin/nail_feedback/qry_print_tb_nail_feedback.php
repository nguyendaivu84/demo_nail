<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_feedback_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_feedback_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_feedback_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_feedback = $cls_tb_nail_feedback->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_feedback = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_feedback .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_feedback .= '<th>Ord</th>';
$v_dsp_tb_nail_feedback .= '<th>Support Id</th>';
$v_dsp_tb_nail_feedback .= '<th>Name</th>';
$v_dsp_tb_nail_feedback .= '<th>Phone</th>';
$v_dsp_tb_nail_feedback .= '<th>Message</th>';
$v_dsp_tb_nail_feedback .= '<th>Department</th>';
$v_dsp_tb_nail_feedback .= '<th>Content</th>';
$v_dsp_tb_nail_feedback .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_feedback as $arr){
	$v_dsp_tb_nail_feedback .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_feedback .= '<td align="right">'.($v_count++).'</td>';
	$v_support_id = isset($arr['support_id'])?$arr['support_id']:0;
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_phone = isset($arr['phone'])?$arr['phone']:'';
	$v_message = isset($arr['message'])?$arr['message']:'';
	$v_department = isset($arr['department'])?$arr['department']:0;
	$v_content = isset($arr['content'])?$arr['content']:'';
	$v_dsp_tb_nail_feedback .= '<td>'.$v_support_id.'</td>';
	$v_dsp_tb_nail_feedback .= '<td>'.$v_name.'</td>';
	$v_dsp_tb_nail_feedback .= '<td>'.$v_phone.'</td>';
	$v_dsp_tb_nail_feedback .= '<td>'.$v_message.'</td>';
	$v_dsp_tb_nail_feedback .= '<td>'.$v_department.'</td>';
	$v_dsp_tb_nail_feedback .= '<td>'.$v_content.'</td>';
	$v_dsp_tb_nail_feedback .= '</tr>';
}
$v_dsp_tb_nail_feedback .= '</table>';
?>