<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_feedback").click(function(e){
		var css = '';

		var support_id = $("input#txt_support_id").val();
		support_id = parseInt(support_id, 10);
		css = isNaN(support_id)?'':'none';
		$("label#lbl_support_id").css("display",css);
		if(css == '') return false;
		var name = $("input#txt_name").val();
		name = $.trim(name);
		css = name==''?'':'none';
		$("label#lbl_name").css("display",css);
		if(css == '') return false;
		var phone = $("input#txt_phone").val();
		phone = $.trim(phone);
		css = phone==''?'':'none';
		$("label#lbl_phone").css("display",css);
		if(css == '') return false;
		var message = $("input#txt_message").val();
		message = $.trim(message);
		css = message==''?'':'none';
		$("label#lbl_message").css("display",css);
		if(css == '') return false;
		var department = $("input#txt_department").val();
		department = parseInt(department, 10);
		css = isNaN(department)?'':'none';
		$("label#lbl_department").css("display",css);
		if(css == '') return false;
		var content = $("input#txt_content").val();
		content = $.trim(content);
		css = content==''?'':'none';
		$("label#lbl_content").css("display",css);
		if(css == '') return false;
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_nail_feedback').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Nail_feedback</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_nail_feedback" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_support_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_support_id" name="txt_support_id" value="<?php echo $v_support_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_name" name="txt_name" value="<?php echo $v_name;?>" /> <label id="lbl_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Phone</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_phone" name="txt_phone" value="<?php echo $v_phone;?>" /> <label id="lbl_phone" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Message</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_message" name="txt_message" value="<?php echo $v_message;?>" /> <label id="lbl_message" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Department</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_department" name="txt_department" value="<?php echo $v_department;?>" /> <label id="lbl_department" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Content</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_content" name="txt_content" value="<?php echo $v_content;?>" /> <label id="lbl_content" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_nail_feedback" name="btn_submit_tb_nail_feedback" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
