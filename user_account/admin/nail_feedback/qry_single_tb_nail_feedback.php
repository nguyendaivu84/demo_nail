<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_support_id = 0;
$v_name = '';
$v_phone = '';
$v_message = '';
$v_department = 0;
$v_content = '';
$v_new_nail_feedback = true;
if(isset($_POST['btn_submit_tb_nail_feedback'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_feedback->set_mongo_id($v_mongo_id);
	$v_support_id = isset($_POST['txt_support_id'])?$_POST['txt_support_id']:$v_support_id;
	if(is_null($v_mongo_id)){
		$v_support_id = $cls_tb_nail_feedback->select_next('support_id');
	}
	$v_support_id = (int) $v_support_id;
	$cls_tb_nail_feedback->set_support_id($v_support_id);
	$v_name = isset($_POST['txt_name'])?$_POST['txt_name']:$v_name;
	$v_name = trim($v_name);
	if($v_name=='') $v_error_message .= '[Name] is empty!<br />';
	$cls_tb_nail_feedback->set_name($v_name);
	$v_phone = isset($_POST['txt_phone'])?$_POST['txt_phone']:$v_phone;
	$v_phone = trim($v_phone);
	if($v_phone=='') $v_error_message .= '[Phone] is empty!<br />';
	$cls_tb_nail_feedback->set_phone($v_phone);
	$v_message = isset($_POST['txt_message'])?$_POST['txt_message']:$v_message;
	$v_message = trim($v_message);
	if($v_message=='') $v_error_message .= '[Message] is empty!<br />';
	$cls_tb_nail_feedback->set_message($v_message);
	$v_department = isset($_POST['txt_department'])?$_POST['txt_department']:$v_department;
	$v_department = (int) $v_department;
	if($v_department<0) $v_error_message .= '[Department] is negative!<br />';
	$cls_tb_nail_feedback->set_department($v_department);
	$v_content = isset($_POST['txt_content'])?$_POST['txt_content']:$v_content;
	$v_content = trim($v_content);
	if($v_content=='') $v_error_message .= '[Content] is empty!<br />';
	$cls_tb_nail_feedback->set_content($v_content);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_feedback->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_feedback->update(array('_id' => $v_mongo_id));
			$v_new_nail_feedback = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_feedback_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_feedback) $v_nail_feedback_id = 0;
		}
	}
}else{
	$v_support_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_support_id,'int');
	if($v_support_id>0){
		$v_row = $cls_tb_nail_feedback->select_one(array('support_id' => $v_support_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_feedback->get_mongo_id();
			$v_support_id = $cls_tb_nail_feedback->get_support_id();
			$v_name = $cls_tb_nail_feedback->get_name();
			$v_phone = $cls_tb_nail_feedback->get_phone();
			$v_message = $cls_tb_nail_feedback->get_message();
			$v_department = $cls_tb_nail_feedback->get_department();
			$v_content = $cls_tb_nail_feedback->get_content();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>