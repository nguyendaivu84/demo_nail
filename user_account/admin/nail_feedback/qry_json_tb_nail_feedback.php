<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_feedback_redirect']) && $_SESSION['ss_tb_nail_feedback_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_feedback_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_feedback_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_feedback_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_feedback_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_feedback_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_feedback->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_feedback_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_feedback_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_feedback_page'] = $v_page;
$_SESSION['ss_tb_nail_feedback_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_feedback = $cls_tb_nail_feedback->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_feedback as $arr){
	$v_support_id = isset($arr['support_id'])?$arr['support_id']:0;
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_phone = isset($arr['phone'])?$arr['phone']:'';
	$v_message = isset($arr['message'])?$arr['message']:'';
	$v_department = isset($arr['department'])?$arr['department']:0;
	$v_content = isset($arr['content'])?$arr['content']:'';
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'support_id' => $v_support_id,
		'name' => $v_name,
		'phone' => $v_phone,
		'message' => $v_message,
		'department' => $v_department,
		'content' => $v_content
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_feedback'=>$arr_ret_data);
echo json_encode($arr_return);
?>