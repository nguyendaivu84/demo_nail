<h4 style="text-align: center">Location #[@LOCATION_ORDER]</h4>
<table align="right" width="100%" style="margin-bottom: 20px;" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr align="left" valign="top">
        <td width="200px" >Location Name</td>
        <td align="left" colspan="2"><b> [@LOCATION_NAME]</b></td>
    </tr>
    <tr align="left" valign="top">
        <td>Location Number </td>
        <td align="left" colspan="2">
            [@LOCATION_NUMBER]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td text-align="right">Location Address: </td>
        <td align="left" colspan="2">
            [@LOCATION_ADDRESS]
        </td>
    </tr>
    <!--tr align="left" valign="top">
        <td>Shipping Company</td>
        <td align="left">[@SHIPPING_COMPANY] </td>
    </tr>
    <tr align="left" valign="top">
        <td>Tracking Number</td>
        <td>
            <b>[@TRACKING_NUMBER] </b>
        </td>
    </tr>
    <tr>
        <td>Tracking URL:</td>
        <td>[@TRACKING_URL]</td>
    </tr>
    <tr align="left" valign="top">
        <td>Shipping Date</td>
        <td align="left" colspan="2">[@SHIPPING_DATE] </td>
    </tr-->
</table>
<table align="right" width="100%" style="margin-bottom: 20px;" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr align="left" valign="top">
        <td width="45px;" >Product Image</td>
        <td width="200px" >Product Name</td>
        <!--td width="170px" >Product Description</td-->
        <td width="50px" >Orig Quantity</td>
        <td width="50px" >Already Shipped Quantity</td>
        <td width="50px" >Remaining Quantity</td>
        <!--td width="50px" >This Dispatch</td-->
    </tr>
    [@TABLE_PRODUCT_DETAILS]
</table>
<style>
    table.list_table tr {
        line-height: 22px;
    }
</style>
[@SHIPPING_HISTORY]