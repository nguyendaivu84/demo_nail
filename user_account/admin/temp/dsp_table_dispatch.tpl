<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr>
        <td>Location Number</td>
        <td>[@LOCATION_NUMBER]</td>
    </tr>
    <tr>
        <td>Location Name</td>
        <td>[@LOCATION_NAME]</td>
    </tr>
    <tr>
        <td>Tracking Company</td>
        <td>[@TRACKING_COMPANY]</td>
    </tr>
    <tr>
        <td>Tracking Number</td>
        <td>[@TRACKING_NUMBER]</td>
    </tr>
    <tr>
        <td>Tracking Url</td>
        <td>[@TRACKING_URL]</td>
    </tr>
    <tr>
        <td>Shipping Date</td>
        <td>[@SHIPPING_DATE]</td>
    </tr>
    <tr>
        <td>Dispatch Status</td>
        <td>[@DISPATCH_STATUS]</td>
    </tr>
</table>
<br />

[@TABLE_CONTENT]

<br />