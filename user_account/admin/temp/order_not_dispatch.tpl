<h4 STYLE="TEXT-ALIGN: CENTER">Order Item #[@NOT_DISPATCH_ORDER]</h4>
<table align="right" width="100%" style="margin-bottom: 10px;" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr align="left" valign="top">
        <td>Order status </td>
        <td align="left" colspan="2">
            [@DISPATCH_STATUS]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td width="200px" >Location Name</td>
        <td align="left" colspan="2"><b> [@LOCATION_NAME]</b></td>
    </tr>
    <tr align="left" valign="top">
        <td>Location Number </td>
        <td align="left" colspan="2">
            [@LOCATION_NUMBER]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td text-align="right">Location Address: </td>
        <td align="left" colspan="2">
            [@LOCATION_ADDRESS]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td>Quantity </td>
        <td align="left" colspan="2">
            [@LOCATION_QUANTITY]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td>Shipping Company</td>
        <td align="left" colspan="2">[@SHIPPING_COMPANY] </td>
    </tr>
    <tr align="left" valign="top">
        <td>Tracking Number</td>
        <td align="left"><b></b>[@TRACKING_NUMBER] </b></td>
        <td align="left">Tracking URL: [@TRACKING_URL] </td>
    </tr>
    <tr align="left" valign="top">
        <td>Shipping Date</td>
        <td align="left" colspan="2">[@SHIPPING_DATE] </td>
    </tr>
</table>