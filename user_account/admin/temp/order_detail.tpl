<h3>ORDER DETAILS: <br /> #PO Number :[@PO_NUMBER] <br /> #Order Ref : [@ORDER_REF] </h3>
<p class="break"/>
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr align="left" valign="top">
        <td width="200px">Company</td>
        <td align="left" colspan="6">
            <b> [@COMPANY_NAME]</b>
        </td>
    </tr>
    <tr align="right" valign="top">
        <td align="left">Location</td>
        <td align="left" colspan="6">
            <b> [@USER_LOCATION] </b>
        </td>
    </tr>
    <tr align="right" valign="top">
        <td align="left">Date Create</td>
        <td align="left">
            [@DATE_CREATE]
        </td>
        <td align="left">Date Required</td>
        <td align="left" colspan="6">
            [@DATE_REQUIRED]
        </td>
    </tr>
    <tr align="right" valign="top" style="[@MODIFY_STYLE]">
        <td align="left">Date Modified</td>
        <td align="left" colspan="6">
            [@DATE_MODIFY]
        </td>
    </tr>
    <tr align="right" valign="top" style="[@MODIFY_STYLE]">
        <td align="left">User Modified</td>
        <td align="left" colspan="6">
            [@USER_MODIFY]
        </td>
    </tr>
    <!--tr align="right" valign="top" >
        <td align="left">Po Number</td>
        <td align="left" colspan="6">
            [@PO_NUMBER]
        </td>
    </tr-->


    <tr align="left" valign="top" >
        <td align="left">Order Type</td>
        <td align="left" colspan="6">
            [@ORDER_TYPE]
        </td>
    </tr>
    <tr align="left" valign="top" >
        <td>Order By</td>
        <td align="left" colspan="6">
            [@ORDER_BY]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td style="width: 200px">Printing Cost</td>
        <td style="width: 200px" align="left">
            <b> [@PRINTING_COST] </b>
        </td>
        <td style="width: 200px">Number of Distributions</td>
        <td align="left" style="width: 100px">
            [@NUMBER_DISTRIBUTION]
        </td>
        <td style="width: 300px">Kitting/packaging Cost Per Distribution</td>
        <td align="left">
            <b>[@KITTING_COST]</b>
        </td>
    </tr>
    <tr align="left" valign="top">
        <td>Total Order Amount</td>
        <td align="left" colspan="6">
            <span class="note"><b>[@TOTAL_AMOUNT]</b> </span>
        </td>
    </tr>

    <tr align="left" valign="top" >
        <td>Order Note</td>
        <td align="left" colspan="6" >
            [@JOB_DESCRIPTION]
        </td>
    </tr>
    <tr align="left" valign="top">
        <td>Sale Rep</td>
        <td align="left" colspan="6">
            [@SALE_REP]
        </td>
    </tr>
    <!--tr align="left" valign="top">
        <td>Terms</td>
        <td align="left">
            [@TERM]
        </td>
    </tr-->
    <tr align="left" valign="top">
        <td>Delivery Method</td>
        <td align="left">
            [@DELIVER_METHOD]
        </td>
        <td>Source</td>
        <td align="left">
            [@SOURCE]
        </td>
        <td>Order's Status</td>
        <td align="left">
            <b>[@ORDER_STATUS]</b>
        </td>
    </tr>

    <tr align="left" valign="top">
        <td>Dispatch</td>
        <td align="left" colspan="6">
            [@DISPATCH]
        </td>
    </tr>
</table>
<h3>PRODUCT DISTRIBUTIONS</h3>