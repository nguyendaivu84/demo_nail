<br />
<h3 style="text-align: center; border-bottom: 1px solid red">Order items #:[@ORDER_ORDER_ITEM]</h3>
<p class="break"/>
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr  valign="top">
        <td width="200px">Product Code </td>
        <td align="left" colspan="3" >[@PRODUCT_CODE]</td>
        <td rowspan="5" align="center">
           [@PRODUCT_IMAGE]
        </td>
    </tr>
    <tr>
        <td >Width </td>
        <td ><b>[@PRODUCT_MATERIAL_WIDTH]</b> </td>
        <td >Height </td>
        <td ><b>[@PRODUCT_MATERIAL_HEIGHT]</b> </td>

    </tr>
    <tr>
        <td> Thickness </td>
        <td colspan="3" ><b>[@PRODUCT_MATERIAL_THICKNESS]</b> </td>
    </tr>
    <tr>
        <td>Color </td>
        <td >[@PRODUCT_MATERIAL_COLOR] </td>
    </tr>
    <tr>
        <td>Unit Price: </td>
        <td ><b>[@UNIT_PRICE] </b></td>
        <td>Quantity : <b>[@QUANTITY] </b> </td>
        <td>Total Price: <b>[@TOTAL_PRICE] </b></td>
    </tr>
</table>
<br />
<h4>ALLOCATION</h4>
[@ALLOCATION]
<!--span style="[@DISPATCH_STYLE]">
    <h3>DISPATCH ORDER</h3>
    [@DISPATCH]
</span>
<span style="[@NOT_DISPATCH_STYLE]">
    <h3>NOT DISPATCH</h3>
    [@NOTDISPATCH]
</span-->