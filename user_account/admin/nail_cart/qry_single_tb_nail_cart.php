<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_cart_id = 0;
$v_product_id = 0;
$v_product_quantity = 0;
$v_product_image = '';
$v_product_name = '';
$v_product_sku = '';
$v_product_price = 0;
$v_new_nail_cart = true;
if(isset($_POST['btn_submit_tb_nail_cart'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_nail_cart->set_mongo_id($v_mongo_id);
	$v_cart_id = isset($_POST['txt_cart_id'])?$_POST['txt_cart_id']:$v_cart_id;
	if(is_null($v_mongo_id)){
		$v_cart_id = $cls_tb_nail_cart->select_next('cart_id');
	}
	$v_cart_id = (int) $v_cart_id;
	$cls_tb_nail_cart->set_cart_id($v_cart_id);
	$v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:$v_product_id;
	$v_product_id = (int) $v_product_id;
	if($v_product_id<0) $v_error_message .= '[Product Id] is negative!<br />';
	$cls_tb_nail_cart->set_product_id($v_product_id);
	$v_product_quantity = isset($_POST['txt_product_quantity'])?$_POST['txt_product_quantity']:$v_product_quantity;
	$v_product_quantity = (int) $v_product_quantity;
	if($v_product_quantity<0) $v_error_message .= '[Product Quantity] is negative!<br />';
	$cls_tb_nail_cart->set_product_quantity($v_product_quantity);
	$v_product_image = isset($_POST['txt_product_image'])?$_POST['txt_product_image']:$v_product_image;
	$v_product_image = trim($v_product_image);
	if($v_product_image=='') $v_error_message .= '[Product Image] is empty!<br />';
	$cls_tb_nail_cart->set_product_image($v_product_image);
	$v_product_name = isset($_POST['txt_product_name'])?$_POST['txt_product_name']:$v_product_name;
	$v_product_name = trim($v_product_name);
	if($v_product_name=='') $v_error_message .= '[Product Name] is empty!<br />';
	$cls_tb_nail_cart->set_product_name($v_product_name);
	$v_product_sku = isset($_POST['txt_product_sku'])?$_POST['txt_product_sku']:$v_product_sku;
	$v_product_sku = trim($v_product_sku);
	if($v_product_sku=='') $v_error_message .= '[Product Sku] is empty!<br />';
	$cls_tb_nail_cart->set_product_sku($v_product_sku);
	$v_product_price = isset($_POST['txt_product_price'])?$_POST['txt_product_price']:$v_product_price;
	$v_product_price = (float) $v_product_price;
	if($v_product_price<0) $v_error_message .= '[Product Price] is negative!<br />';
	$cls_tb_nail_cart->set_product_price($v_product_price);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_nail_cart->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_nail_cart->update(array('_id' => $v_mongo_id));
			$v_new_nail_cart = false;
		}
		if($v_result){
			$_SESSION['ss_tb_nail_cart_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_nail_cart) $v_nail_cart_id = 0;
		}
	}
}else{
	$v_cart_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_cart_id,'int');
	if($v_cart_id>0){
		$v_row = $cls_tb_nail_cart->select_one(array('cart_id' => $v_cart_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_nail_cart->get_mongo_id();
			$v_cart_id = $cls_tb_nail_cart->get_cart_id();
			$v_product_id = $cls_tb_nail_cart->get_product_id();
			$v_product_quantity = $cls_tb_nail_cart->get_product_quantity();
			$v_product_image = $cls_tb_nail_cart->get_product_image();
			$v_product_name = $cls_tb_nail_cart->get_product_name();
			$v_product_sku = $cls_tb_nail_cart->get_product_sku();
			$v_product_price = $cls_tb_nail_cart->get_product_price();
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
?>