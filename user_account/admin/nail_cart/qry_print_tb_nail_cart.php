<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_cart_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_cart_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_cart_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_cart = $cls_tb_nail_cart->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_cart = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_cart .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_cart .= '<th>Ord</th>';
$v_dsp_tb_nail_cart .= '<th>Cart Id</th>';
$v_dsp_tb_nail_cart .= '<th>Product Id</th>';
$v_dsp_tb_nail_cart .= '<th>Product Quantity</th>';
$v_dsp_tb_nail_cart .= '<th>Product Image</th>';
$v_dsp_tb_nail_cart .= '<th>Product Name</th>';
$v_dsp_tb_nail_cart .= '<th>Product Sku</th>';
$v_dsp_tb_nail_cart .= '<th>Product Price</th>';
$v_dsp_tb_nail_cart .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_cart as $arr){
	$v_dsp_tb_nail_cart .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_cart .= '<td align="right">'.($v_count++).'</td>';
	$v_cart_id = isset($arr['cart_id'])?$arr['cart_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_product_quantity = isset($arr['product_quantity'])?$arr['product_quantity']:0;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:'';
	$v_product_name = isset($arr['product_name'])?$arr['product_name']:'';
	$v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
	$v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
	$v_dsp_tb_nail_cart .= '<td>'.$v_cart_id.'</td>';
	$v_dsp_tb_nail_cart .= '<td>'.$v_product_id.'</td>';
	$v_dsp_tb_nail_cart .= '<td>'.$v_product_quantity.'</td>';
	$v_dsp_tb_nail_cart .= '<td>'.$v_product_image.'</td>';
	$v_dsp_tb_nail_cart .= '<td>'.$v_product_name.'</td>';
	$v_dsp_tb_nail_cart .= '<td>'.$v_product_sku.'</td>';
	$v_dsp_tb_nail_cart .= '<td>'.$v_product_price.'</td>';
	$v_dsp_tb_nail_cart .= '</tr>';
}
$v_dsp_tb_nail_cart .= '</table>';
?>