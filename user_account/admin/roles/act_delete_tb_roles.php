<?php if(!isset($v_sval)) die();?>
<?php
$v_rule_id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
settype($v_rule_id,"int");
$cls_tb_rule->delete(array('rule_id' =>$v_rule_id));
add_class("cls_tb_user");
$cls_tb_user = new cls_tb_user($db);
$v_company_id = $_SESSION['ss_last_company_id'];
$arr_list = $cls_tb_user->select(array("company_id"=>(int)$v_company_id));
foreach($arr_list as $arr){
    $arr_role = $arr['user_role'];
    if(in_array($v_rule_id,$arr_role)){
        $v_user_id = (int)$arr['user_id'];
        $arr_role_temp = array();
        for($i=0; $i<count($arr_role);$i++){
            $v_role_id_temp = $arr_role[$i];
            settype($v_role_id_temp,"int");
            if($v_role_id_temp == $v_rule_id)
                $arr_role_temp[] = $arr_role[$i];
        }
        $cls_tb_user->update_field("user_role",$arr_role_temp,array("user_id"=>$v_user_id));
    }
}
redir($_SERVER['HTTP_REFERER']);
?>