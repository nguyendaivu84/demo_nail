<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_tb_role_where_clause'])){
    $v_where_clause = $_SESSION['ss_tb_role_where_clause'];
    $arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_role_sort'])){
    $v_sort = $_SESSION['ss_tb_role_sort'];
    $arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_role = $cls_tb_rule->select($arr_where_clause, $arr_sort);
$v_dsp_tb_role = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="1" align="center">';
$v_temp_rule = '-1';
$v_dsp_tb_role .= '<tr align="center" valign="middle">';
$v_dsp_tb_role .= '<th>Ord</th>';
$v_dsp_tb_role .= '<th>Rule Id</th>';
$v_dsp_tb_role .= '<th>Rule Title</th>';
$v_dsp_tb_role .= '<th>Rule Type</th>';
$v_dsp_tb_role .= '<th>Rule Key</th>';
$v_dsp_tb_role .= '<th>Rule Menu</th>';
$v_dsp_tb_role .= '<th>Group Type</th>';
$v_dsp_tb_role .= '</tr>';
$v_count = 1;
foreach($arr_tb_role as $arr){
    $v_dsp_tb_role .= '<tr align="left" valign="middle">';
    $v_dsp_tb_role .= '<td align="right">'.($v_count++).'</td>';
    $v_rule_id = isset($arr['rule_id'])?$arr['rule_id']:0;
    $v_rule_title = isset($arr['rule_title'])?$arr['rule_title']:'';
    $v_rule_type = isset($arr['rule_type'])?$arr['rule_type']:0;
    $v_rule_key = isset($arr['rule_key'])?$arr['rule_key']:'';
    $v_rule_menu = isset($arr['rule_menu'])?$arr['rule_menu']:'';
    $v_dsp_rule_comp ='User';
    $v_rule_comp = isset($arr['rule_comp'])?$arr['rule_comp']:0;
    if($v_rule_comp==1) $v_dsp_rule_comp ='Company';
    if($v_temp_rule!=$v_rule_type)
    {
        if(!isset($arr_setting[$v_rule_type]))
            $arr_setting[$v_rule_type] = $cls_tb_setting->get_option_name_by_id('rule_type',$v_rule_type);

        $v_temp_rule = $v_rule_type;
        $v_dsp_tb_rule = $arr_setting[$v_rule_type];
    }
    $v_dsp_tb_role .= '<td>'.$v_rule_id.'</td>';
    $v_dsp_tb_role .= '<td>'.$v_rule_title.'</td>';
    $v_dsp_tb_role .= '<td>'.$v_rule_type.'</td>';
    $v_dsp_tb_role .= '<td>'.$v_rule_key.'</td>';
    $v_dsp_tb_role .= '<td>'.$v_rule_menu.'</td>';
    $v_dsp_tb_role .= '<td>'.$v_dsp_rule_comp.'</td>';
    $v_dsp_tb_role .= '</tr>';
}
$v_dsp_tb_role .= '</table>';
?>