<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        var tooltip = $("span.tooltips").kendoTooltip({
            filter: 'a',
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $('input#txt_rule_key').focusout(function(e){
            var key = $.trim($(this).val());
            if(key==''){
                $(this).val('');
                $('input#txt_hidden_role_key').val('N');
                validator.validate();
                return false;
            }
            var company_id = $('input#txt_company_id').val();
            var rule_id = $('input#txt_rule_id').val();
            $.ajax({
                url : '<?php echo URL.$v_admin_key;?>/ajax',
                type:   'POST',
                data:   {txt_session_id: '<?php echo session_id();?>', txt_ajax_type: 'check_role_key', txt_company_id: company_id, txt_role_id: rule_id, txt_role_key: key},
                beforeSend: function(){
                    $('span#sp_role_key').html('Checking...');
                },
                success: function(data, status){
                    $('span#sp_role_key').html('');
                    var ret = $.parseJSON(data);
                    $('input#txt_hidden_role_key').val(ret.error==0?'Y':'');
                    validator.validate();
                }
            });
        });
        $("select#txt_rule_type_id").width(300).kendoComboBox({
        });
    });
    function Permission(menu, key, description){
        this.menu = menu;
        this.key = key;
        this.description = description;
        this.status = 0;
    }
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Function <?php if($v_rule_id>0) echo ': '.$v_rule_title;?></h3>
                </div>

                <form id="frm_tb_role" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_rule_id.'/edit';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_rule_id" name="txt_rule_id" value="<?php echo $v_rule_id;?>" />
                    <input type="hidden" id="txt_role_content" name="txt_role_content" value="" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">information</li>
                        </ul>
                        <div class="information div_details">
                            <table id="tbl_required" align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top" style="display: none">
                                    <td style="width:200px">Location</td>
                                    <td style="width:1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_location_id" name="txt_location_id">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width:200px">Rule Title</td>
                                    <td style="width:1px">&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_rule_title" name="txt_rule_title" value="<?php echo $v_rule_title;?>"  required validationMessage="Please input Rule Title" /> <label id="lbl_role_title" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Rule Type</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_rule_type_id" name="txt_rule_type_id">
                                            <option value="-1">------</option>
                                            <?php echo  $cls_tb_setting->draw_option_by_id('rule_type',0,$v_rule_type);  ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Rule Descriptions</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="text" id="txt_rule_des" name="txt_rule_des" value="<?php echo $v_rule_des;?>"  >
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Rule Key</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="text" id="txt_rule_key" name="txt_rule_key" value="<?php echo $v_rule_key;?>" required validationMessage="Please input Rule Key" />
                                        <input type="hidden" id="txt_hidden_rule_key" name="txt_hidden_rule_key" value="<?php echo $v_rule_key!=''?'Y':'N';?>" required validationMessage="Role Key is unique" />
                                        <span id="sp_rule_key"></span>
                                        <span class="tooltips"><a title="Rule Key is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
                                        <label id="lbl_role_key" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Rule Menu</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="text" id="txt_rule_menu" name="txt_rule_menu" value="<?php echo $v_rule_menu;?>"  >
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Rule Admin</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><label><input type="checkbox" id="txt_status_admin" name="txt_status_admin"<?php echo $v_rule_admin==1?'checked="checked"':' ';?>" /> Active</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Rule Company</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><label><input type="checkbox" id="txt_status_company" name="txt_status_company"<?php echo $v_rule_function==1?'checked="checked"':' ';?>" /> Active</label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_role" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
