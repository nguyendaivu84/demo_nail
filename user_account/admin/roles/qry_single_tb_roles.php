<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_rule_id = 0;
$v_rule_title = '';
$v_rule_type = 0;
$v_rule_key = '';
$v_rule_admin = '';
$v_rule_function = '';
$v_rule_des = '';
$v_rule_menu = '';
$arr_rule_content = array();
$arr_rule_child = array();
if(isset($_POST['btn_submit_tb_role'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_rule->set_mongo_id($v_mongo_id);
    $v_rule_id = isset($_POST['txt_rule_id'])?$_POST['txt_rule_id']:$v_rule_id;
    if(is_null($v_mongo_id)){
        $v_rule_id = $cls_tb_rule->select_next('rule_id');
    }
    $v_rule_id = (int) $v_rule_id;
    $cls_tb_rule->set_rule_id($v_rule_id);
    $v_rule_title = isset($_POST['txt_rule_title'])?$_POST['txt_rule_title']:$v_rule_title;
    $v_rule_title = trim($v_rule_title);
    if($v_rule_title=='') $v_error_message .= '[Rule Title] is empty!<br />';
    $cls_tb_rule->set_rule_title($v_rule_title);
    $v_rule_des = isset($_POST['txt_rule_des'])?$_POST['txt_rule_des']:$v_rule_des;
    $v_rule_des = trim($v_rule_des);
    $cls_tb_rule->set_rule_description($v_rule_des);

    $v_rule_menu = isset($_POST['txt_rule_menu'])?$_POST['txt_rule_menu']:$v_rule_menu;
    $v_rule_menu = trim($v_rule_menu);
    $cls_tb_rule->set_rule_menu($v_rule_menu);
    // rule type lam sau
    $v_rule_type = isset($_POST['txt_rule_type_id'])?$_POST['txt_rule_type_id']:$v_rule_type;
    $v_rule_type = (int) $v_rule_type;
    if($v_rule_type<0){
        die("$v_rule_type");
        $v_error_message .= '[Rule Type] is negative!<br />';
    }

    $cls_tb_rule->set_rule_type($v_rule_type);
    //
    $v_rule_key = isset($_POST['txt_rule_key'])?$_POST['txt_rule_key']:$v_rule_key;
    $v_rule_key = trim($v_rule_key);
    if($v_rule_key=='')
        $v_error_message .= '[Role Key] is empty!<br />';
    else{
        if($cls_tb_rule->count(array('rule_key'=>$v_rule_key, 'rule_id'=>array('$ne'=>$v_rule_id)))>0) $v_error_message.= '+ [Rule Key] is unique.<br />';
    }
    $cls_tb_rule->set_rule_key($v_rule_key);
    $v_rule_admin = isset($_POST['txt_status_admin'])?1:0;
    $v_rule_admin = (int) $v_rule_admin;
    $v_rule_function = isset($_POST['txt_status_company'])?1:0;
    $v_rule_function = (int) $v_rule_function;
    $cls_tb_rule->set_rule_admin($v_rule_admin);
    $cls_tb_rule->set_rule_comp($v_rule_function);
    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_rule->insert();
            $v_result = is_object($v_mongo_id);
        }else $v_result = $cls_tb_rule->update(array('_id' => $v_mongo_id));
        if($v_result){
            $_SESSION['ss_tb_role_redirect'] = 1;
            redir(URL.$v_admin_key);
        }
    }
}else{
    $v_rule_id= isset($_GET['id'])?$_GET['id']:'0';
    settype($v_rule_id,'int');
    if($v_role_id>0){
        $v_row = $cls_tb_rule->select_one(array('rule_id' => $v_rule_id));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_rule->get_mongo_id();
            $v_rule_id = $cls_tb_rule->get_rule_id();
            $v_rule_title = $cls_tb_rule->get_rule_title();
            $v_rule_type = $cls_tb_rule->get_rule_type();
            $v_rule_key = $cls_tb_rule->get_rule_key();
            $v_rule_admin = $cls_tb_rule->get_rule_admin();
            $v_rule_admin = $v_rule_admin!=''?$v_rule_admin:'';
            $v_rule_function = $cls_tb_rule->get_rule_comp();
            $v_rule_function = $v_rule_function!=''?$v_rule_function:'';
            $v_rule_des = $cls_tb_rule->get_rule_description();
        }
    }
}
?>