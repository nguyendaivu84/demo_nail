<?php if(!isset($v_sval)) die();?>
<?php
$row_order = 0;
$arr_where_clause = array();
$v_role_title = isset($_POST['txt_search_role_title'])?$_POST['txt_search_role_title']:'';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
if($v_role_title!='') $arr_where_clause['rule_title'] = new MongoRegex('/'.$v_role_title.'/i');

//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;

if(isset($_SESSION['ss_tb_role_redirect']) && $_SESSION['ss_tb_role_redirect']==1){
    if(isset($_SESSION['ss_tb_role_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_role_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_role_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_role_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_role_redirect']);
}

settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_role->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_role_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_role_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_role_page'] = $v_page;
$_SESSION['ss_tb_role_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_rule = $cls_tb_rule->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);//select_limit($v_offset,$v_num_row,$arr_where_clause,array('rule_type'=>1,'rule_comp'=>1));
$arr_ret_data = array();
$v_row = $v_skip;
$v_temp_rule = '-1';
foreach($arr_tb_rule as $arr){
    $v_mongo_id = $cls_tb_rule->get_mongo_id();
    $v_rule_id = isset($arr['rule_id'])?$arr['rule_id']:0;
    $v_rule_title = isset($arr['rule_title'])?$arr['rule_title']:'';
    $v_rule_type = isset($arr['rule_type'])?$arr['rule_type']:'';
    $v_rule_key = isset($arr['rule_key'])?$arr['rule_key']:0;
    $v_rule_menu = isset($arr['rule_menu'])?$arr['rule_menu']:0;
    $v_dsp_rule_comp ='User';
    $v_rule_comp = isset($arr['rule_comp'])?$arr['rule_comp']:0;
    if($v_rule_comp==1) $v_dsp_rule_comp ='Company';
    if($v_temp_rule!=$v_rule_type)
    {
        if(!isset($arr_setting[$v_rule_type]))
            $arr_setting[$v_rule_type] = $cls_tb_setting->get_option_name_by_id('rule_type',$v_rule_type);

        $v_temp_rule = $v_rule_type;
        $v_dsp_tb_rule = $arr_setting[$v_rule_type];
    }
    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'role_id' => $v_rule_id,
        'rule_title' => $v_rule_title,
        'rule_type' => $v_rule_type,
        'rule_key' => $v_rule_key,
        'rule_menu' => $v_rule_menu,
        'group_type' => $v_dsp_tb_rule
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_role'=>$arr_ret_data);
echo json_encode($arr_return);
?>