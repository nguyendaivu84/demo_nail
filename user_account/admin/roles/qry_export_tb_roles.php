<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
    $v_where_clause = $_SESSION['ss_tb_role_where_clause'];
    $arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_role_sort'])){
    $v_sort = $_SESSION['ss_tb_role_sort'];
    $arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_role = $cls_tb_rule->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_function_for_company_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
    ->setLastModifiedBy("Anvy")
    ->setTitle('Function for company')
    ->setSubject("Office 2003 XLS Test Document")
    ->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
    ->setKeywords("office 2003 openxml php")
    ->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Role');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Rule Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Rule Title', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Rule Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Rule Key', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Rule Menu', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Group Type', 'center', true, true, 20, '', true);
$v_excel_row++;
$v_temp_rule = '-1';
foreach($arr_tb_role as $arr){
    $v_excel_col = 1;
    $v_rule_id = isset($arr['rule_id'])?$arr['rule_id']:0;
    $v_rule_title = isset($arr['rule_title'])?$arr['rule_title']:'';
    $v_rule_type = isset($arr['rule_type'])?$arr['rule_type']:0;
    $v_rule_key = isset($arr['rule_key'])?$arr['rule_key']:'';
    $v_rule_menu = isset($arr['rule_menu'])?$arr['rule_menu']:'';
    $v_dsp_rule_comp ='User';
    $v_rule_comp = isset($arr['rule_comp'])?$arr['rule_comp']:0;
    if($v_rule_comp==1) $v_dsp_rule_comp ='Company';
    if($v_temp_rule!=$v_rule_type)
    {
        if(!isset($arr_setting[$v_rule_type]))
            $arr_setting[$v_rule_type] = $cls_tb_setting->get_option_name_by_id('rule_type',$v_rule_type);

        $v_temp_rule = $v_rule_type;
        $v_dsp_tb_rule = $arr_setting[$v_rule_type];
    }
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_rule_id, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_rule_title, 'left');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_rule_type, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_rule_key, 'left');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_rule_menu, 'right');
    create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_dsp_tb_rule, 'right');
    $v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>