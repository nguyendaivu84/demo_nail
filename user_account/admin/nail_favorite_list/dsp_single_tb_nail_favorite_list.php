<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_nail_favorite_list").click(function(e){
		var css = '';

		var favorite_id = $("input#txt_favorite_id").val();
		favorite_id = parseInt(favorite_id, 10);
		css = isNaN(favorite_id)?'':'none';
		$("label#lbl_favorite_id").css("display",css);
		if(css == '') return false;
		var user_id = $("input#txt_user_id").val();
		user_id = parseInt(user_id, 10);
		css = isNaN(user_id)?'':'none';
		$("label#lbl_user_id").css("display",css);
		if(css == '') return false;
		var product_id = $("input#txt_product_id").val();
		product_id = parseInt(product_id, 10);
		css = isNaN(product_id)?'':'none';
		$("label#lbl_product_id").css("display",css);
		if(css == '') return false;
		var product_image = $("input#txt_product_image").val();
		product_image = $.trim(product_image);
		css = product_image==''?'':'none';
		$("label#lbl_product_image").css("display",css);
		if(css == '') return false;
		var product_name = $("input#txt_product_name").val();
		product_name = $.trim(product_name);
		css = product_name==''?'':'none';
		$("label#lbl_product_name").css("display",css);
		if(css == '') return false;
		var product_sku = $("input#txt_product_sku").val();
		product_sku = $.trim(product_sku);
		css = product_sku==''?'':'none';
		$("label#lbl_product_sku").css("display",css);
		if(css == '') return false;
		var product_price = $("input#txt_product_price").val();
		product_price = parseFloat(product_price);
		css = isNaN(product_price)?'':'none';
		$("label#lbl_product_price").css("display",css);
		if(css == '') return false;
		return true;
	});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_nail_favorite_list').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Nail_favorite_list</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_nail_favorite_list" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_favorite_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_favorite_id" name="txt_favorite_id" value="<?php echo $v_favorite_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>User Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" /> <label id="lbl_user_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Product Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_id" name="txt_product_id" value="<?php echo $v_product_id;?>" /> <label id="lbl_product_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Product Image</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_image" name="txt_product_image" value="<?php echo $v_product_image;?>" /> <label id="lbl_product_image" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Product Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_name" name="txt_product_name" value="<?php echo $v_product_name;?>" /> <label id="lbl_product_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Product Sku</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_sku" name="txt_product_sku" value="<?php echo $v_product_sku;?>" /> <label id="lbl_product_sku" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Product Price</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_price" name="txt_product_price" value="<?php echo $v_product_price;?>" /> <label id="lbl_product_price" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_nail_favorite_list" name="btn_submit_tb_nail_favorite_list" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
