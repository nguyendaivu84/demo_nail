<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_nail_favorite_list_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_nail_favorite_list_sort'])){
	$v_sort = $_SESSION['ss_tb_nail_favorite_list_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_nail_favorite_list = $cls_tb_nail_favorite_list->select($arr_where_clause, $arr_sort);
$v_dsp_tb_nail_favorite_list = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_nail_favorite_list .= '<tr align="center" valign="middle">';
$v_dsp_tb_nail_favorite_list .= '<th>Ord</th>';
$v_dsp_tb_nail_favorite_list .= '<th>Favorite Id</th>';
$v_dsp_tb_nail_favorite_list .= '<th>User Id</th>';
$v_dsp_tb_nail_favorite_list .= '<th>Product Id</th>';
$v_dsp_tb_nail_favorite_list .= '<th>Product Image</th>';
$v_dsp_tb_nail_favorite_list .= '<th>Product Name</th>';
$v_dsp_tb_nail_favorite_list .= '<th>Product Sku</th>';
$v_dsp_tb_nail_favorite_list .= '<th>Product Price</th>';
$v_dsp_tb_nail_favorite_list .= '</tr>';
$v_count = 1;
foreach($arr_tb_nail_favorite_list as $arr){
	$v_dsp_tb_nail_favorite_list .= '<tr align="left" valign="middle">';
	$v_dsp_tb_nail_favorite_list .= '<td align="right">'.($v_count++).'</td>';
	$v_favorite_id = isset($arr['favorite_id'])?$arr['favorite_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:'';
	$v_product_name = isset($arr['product_name'])?$arr['product_name']:'';
	$v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
	$v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_favorite_id.'</td>';
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_product_id.'</td>';
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_product_image.'</td>';
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_product_name.'</td>';
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_product_sku.'</td>';
	$v_dsp_tb_nail_favorite_list .= '<td>'.$v_product_price.'</td>';
	$v_dsp_tb_nail_favorite_list .= '</tr>';
}
$v_dsp_tb_nail_favorite_list .= '</table>';
?>