<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_nail_favorite_list_redirect']) && $_SESSION['ss_tb_nail_favorite_list_redirect']==1){
	if(isset($_SESSION['ss_tb_nail_favorite_list_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_nail_favorite_list_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_nail_favorite_list_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_nail_favorite_list_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_nail_favorite_list_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_nail_favorite_list->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_nail_favorite_list_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_nail_favorite_list_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_nail_favorite_list_page'] = $v_page;
$_SESSION['ss_tb_nail_favorite_list_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_nail_favorite_list = $cls_tb_nail_favorite_list->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_nail_favorite_list as $arr){
	$v_favorite_id = isset($arr['favorite_id'])?$arr['favorite_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:'';
	$v_product_name = isset($arr['product_name'])?$arr['product_name']:'';
	$v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
	$v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'favorite_id' => $v_favorite_id,
		'user_id' => $v_user_id,
		'product_id' => $v_product_id,
		'product_image' => $v_product_image,
		'product_name' => $v_product_name,
		'product_sku' => $v_product_sku,
		'product_price' => $v_product_price
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_nail_favorite_list'=>$arr_ret_data);
echo json_encode($arr_return);
?>