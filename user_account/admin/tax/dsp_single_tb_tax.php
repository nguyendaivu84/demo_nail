<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$('input#txt_date_modified').kendoDatePicker({format:"dd-MMM-yyyy"});
    $("#txt_fed_tax,#txt_pro_tax").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
    var provinceData = <?php echo json_encode($arr_groupProvince) ;?>;
	var validator = $("div.information").kendoValidator().data("kendoValidator");
    $("#txt_province").on("change",function(){
        $("#txt_province_name").val($("#txt_province option:checked").text());
    });
    var cbo_province = $("#txt_province").width(300).kendoDropDownList({
        dataSource : provinceData
        ,dataTextField : 'name'
        ,dataValueField : 'key'
    }).data('kendoDropDownList');
    cbo_province.value('<?php echo $v_province_key;?>');
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Tax</h3>
                    </div>
                    <form id="frm_tb_tax" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':((string)$v_mongo_id).'/editid';?>" method="POST">
                        <input type="hidden" id="txt_province_name" name="txt_province_name" value="" />
                        <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                        <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <li>Other</li>
                        </ul>
                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td>Name</td>
                                    <td style="width:1px;">&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_name" name="txt_name" value="<?php echo $v_name;?>" /> <label id="lbl_name" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                        <td>Description</td>
                                        <td>&nbsp;</td>
                                        <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_description" name="txt_description" value="<?php echo $v_description;?>" /> <label id="lbl_description" class="k-required">(*)</label></td>
                                    </tr>
                                <tr align="right" valign="top">
                                        <td>Fed Tax</td>
                                        <td>&nbsp;</td>
                                        <td align="left"><input size="50" type="text" id="txt_fed_tax" name="txt_fed_tax" value="<?php echo $v_fed_tax;?>" /> <label id="lbl_fed_tax" class="k-required">(*)</label></td>
                                    </tr>
                                <tr align="right" valign="top">
                                        <td>Hst Tax</td>
                                        <td>&nbsp;</td>
                                        <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_hst_tax" name="txt_hst_tax" value="<?php echo $v_hst_tax;?>" /> <label id="lbl_hst_tax" class="k-required">(*)</label></td>
                                    </tr>
                                <tr align="right" valign="top">
                                        <td>Pro Tax</td>
                                        <td>&nbsp;</td>
                                        <td align="left"><input size="50" type="text" id="txt_pro_tax" name="txt_pro_tax" value="<?php echo $v_pro_tax;?>" /> <label id="lbl_pro_tax" class="k-required">(*)</label></td>
                                    </tr>
                                <tr align="right" valign="top">
                                        <td>Province</td>
                                        <td>&nbsp;</td>
                                        <td align="left">
                                            <select id="txt_province" name="txt_province"></select>
                                        </td>
                                    </tr>
                                <tr align="right" valign="top">
                                    <td>Deleted</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input type="checkbox" id="txt_deleted" name="txt_deleted" <?php echo !$v_deleted ? "checked":"";?> />
                                </tr>
                            </table>
                        </div>
                        <div class="other div_details"></div>
                       </div>
                       <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                           <?php if($v_error_message!=''){?>
                                <div class="k-block k-widget k-error-colored div_errors">
                                    <?php echo $v_error_message;?>
                                </div>
                            <?php }?>
                            <div class="k-block k-widget div_buttons">
                                <input type="submit" id="btn_submit_tb_tax" name="btn_submit_tb_tax" value="Submit" class="k-button button_css" />
                            </div>
                        <?php }?>
                    </form>
                </div>
            </div>
        </div>
  </div>
