<?php if(!isset($v_sval)) die();
$arr_groupProvince = array();
$v_error_message = '';
$v_mongo_id = NULL;
$v_created_by = '';
$v_date_modified = date('Y-m-d H:i:s', time());
$v_deleted = 0;
$v_description = '';
$v_fed_tax = 0;
$v_hst_tax = '';
$v_modified_by = '';
$v_name = '';
$v_pro_tax = 0;
$v_province = '';
$v_province_key = '';
$v_new_tax = true;
if(isset($_POST['btn_submit_tb_tax'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_tax->set_mongo_id($v_mongo_id);
    $v_created_by = $arr_user['mongo_id'];
	$cls_tb_tax->set_created_by($v_created_by);
	$v_date_modified = isset($_POST['txt_mongo_id'])?$v_date_modified:'';
	$cls_tb_tax->set_date_modified($v_date_modified);
	$v_deleted = isset($_POST['txt_deleted'])?false:true;
	$cls_tb_tax->set_deleted($v_deleted);
	$v_description = isset($_POST['txt_description'])?$_POST['txt_description']:$v_description;
	$cls_tb_tax->set_description($v_description);
	$v_fed_tax = isset($_POST['txt_fed_tax'])?$_POST['txt_fed_tax']:$v_fed_tax;
	$cls_tb_tax->set_fed_tax($v_fed_tax);
	$v_hst_tax = isset($_POST['txt_hst_tax'])?$_POST['txt_hst_tax']:$v_hst_tax;
	$v_hst_tax = trim($v_hst_tax);
	$cls_tb_tax->set_hst_tax($v_hst_tax);
    $v_modified_by = isset($_POST['txt_mongo_id'])?$arr_user['mongo_id']:'';
	$cls_tb_tax->set_modified_by($v_modified_by);
	$v_name = isset($_POST['txt_name'])?$_POST['txt_name']:$v_name;
	$v_name = trim($v_name);
	$cls_tb_tax->set_name($v_name);
	$v_pro_tax = isset($_POST['txt_pro_tax'])?$_POST['txt_pro_tax']:$v_pro_tax;
	$cls_tb_tax->set_pro_tax($v_pro_tax);

	$v_province = isset($_POST['txt_province_name'])?$_POST['txt_province_name']:$v_province;
	$v_province = trim($v_province);
	$cls_tb_tax->set_province($v_province);
	$v_province_key = isset($_POST['txt_province'])?$_POST['txt_province']:$v_province_key;
	$v_province_key = trim($v_province_key);
	if($v_province_key=='') $v_error_message .= '[Province] is empty!<br />';
	$cls_tb_tax->set_province_key($v_province_key);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_tax->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_tax->update(array('_id' => $v_mongo_id));
			$v_new_tax = false;
		}
		if($v_result){
			$_SESSION['ss_tb_tax_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_tax) $v_tax_id = 0;
		}
	}
}else{
	$v__id= isset($_GET['id'])?$_GET['id']:'0';
	if($v__id!=''){
		$v_row = $cls_tb_tax->select_one(array('_id' => new MongoId($v__id)));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_tax->get_mongo_id();
			$v_created_by = $cls_tb_tax->get_created_by();
			$v_deleted = $cls_tb_tax->get_deleted();
			$v_description = $cls_tb_tax->get_description();
			$v_fed_tax = $cls_tb_tax->get_fed_tax();
			$v_hst_tax = $cls_tb_tax->get_hst_tax();
			$v_modified_by = $cls_tb_tax->get_modified_by();
			$v_name = $cls_tb_tax->get_name();
			$v_pro_tax = $cls_tb_tax->get_pro_tax();
			$v_province = $cls_tb_tax->get_province();
			$v_province_key = $cls_tb_tax->get_province_key();
		}
	}
}
$arr_groupProvince = get_array_data($cls_tb_province,"key","name",$v_province_key,array(),array('deleted'=>false),array('name'=>1));
?>