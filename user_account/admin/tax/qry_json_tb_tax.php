<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
if($v_quick_search!=''){
    $arr_where_clause['or'] = array('description'=>new MongoRegex('/'.$v_search_location_name.'/i'),'province'=>new MongoRegex('/'.$v_search_location_name.'/i'),'province_key'=>new MongoRegex('/'.$v_search_location_name.'/i'));
}
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_tax_redirect']) && $_SESSION['ss_tb_tax_redirect']==1){
	if(isset($_SESSION['ss_tb_tax_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_tax_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_tax_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_tax_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_tax_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_tax->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_tax_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_tax_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_tax_page'] = $v_page;
$_SESSION['ss_tb_tax_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_tax = $cls_tb_tax->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_user_create = array();
foreach($arr_tb_tax as $arr){
    $id = (string)$arr['_id'];
	$v_created_by = isset($arr['created_by'])?$arr['created_by']:'';
	$v_date_modified = isset($arr['date_modified'])?$arr['date_modified']:(new MongoDate(time()));
	$v_deleted = isset($arr['deleted'])?$arr['deleted']:0;
    $v_deleted = !$v_deleted ?"Active" :"Inactive";
	$v_description = isset($arr['description'])?$arr['description']:'';
	$v_fed_tax = isset($arr['fed_tax'])?$arr['fed_tax']:0;
	$v_hst_tax = isset($arr['hst_tax'])?$arr['hst_tax']:'';
	$v_modified_by = isset($arr['modified_by'])?$arr['modified_by']:'';
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_pro_tax = isset($arr['pro_tax'])?$arr['pro_tax']:0;
	$v_province = isset($arr['province'])?$arr['province']:'';
    $UsernameByMongoID = '';
    if(isset($arr_user_create[(string)$v_created_by])){
        $UsernameByMongoID = $arr_user_create[(string)$v_created_by];
    }else{
        $UsernameByMongoID = $cls_tb_user->select_scalar("user_name",array("_id"=>$v_created_by));
        $arr_user_create [(string)$v_created_by] = $UsernameByMongoID;
    }
    if($UsernameByMongoID=='') $UsernameByMongoID = 'admin';
	$v_province_key = isset($arr['province_key'])?$arr['province_key']:'';
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'created_by' => $UsernameByMongoID,
		'deleted' => $v_deleted,
		'description' => $v_description,
		'fed_tax' => format_currency($v_fed_tax),
		'pro_tax' => format_currency($v_pro_tax),
		'province' => $v_province,
		'province_key' => $v_province_key
		,'tax_id' => $id
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_tax'=>$arr_ret_data);
echo json_encode($arr_return);
?>