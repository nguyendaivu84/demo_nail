<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_tax_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_tax_sort'])){
	$v_sort = $_SESSION['ss_tb_tax_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_tax = $cls_tb_tax->select($arr_where_clause, $arr_sort);
$v_dsp_tb_tax = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_tax .= '<tr align="center" valign="middle">';
$v_dsp_tb_tax .= '<th>Ord</th>';
$v_dsp_tb_tax .= '<th>Created By</th>';
$v_dsp_tb_tax .= '<th>Date Modified</th>';
$v_dsp_tb_tax .= '<th>Deleted</th>';
$v_dsp_tb_tax .= '<th>Description</th>';
$v_dsp_tb_tax .= '<th>Fed Tax</th>';
$v_dsp_tb_tax .= '<th>Hst Tax</th>';
$v_dsp_tb_tax .= '<th>Modified By</th>';
$v_dsp_tb_tax .= '<th>Name</th>';
$v_dsp_tb_tax .= '<th>Pro Tax</th>';
$v_dsp_tb_tax .= '<th>Province</th>';
$v_dsp_tb_tax .= '<th>Province Key</th>';
$v_dsp_tb_tax .= '</tr>';
$v_count = 1;
foreach($arr_tb_tax as $arr){
	$v_dsp_tb_tax .= '<tr align="left" valign="middle">';
	$v_dsp_tb_tax .= '<td align="right">'.($v_count++).'</td>';
	$v_created_by = isset($arr['created_by'])?$arr['created_by']:'';
	$v_date_modified = isset($arr['date_modified'])?$arr['date_modified']:(new MongoDate(time()));
	$v_deleted = isset($arr['deleted'])?$arr['deleted']:0;
	$v_description = isset($arr['description'])?$arr['description']:'';
	$v_fed_tax = isset($arr['fed_tax'])?$arr['fed_tax']:0;
	$v_hst_tax = isset($arr['hst_tax'])?$arr['hst_tax']:'';
	$v_modified_by = isset($arr['modified_by'])?$arr['modified_by']:'';
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_pro_tax = isset($arr['pro_tax'])?$arr['pro_tax']:0;
	$v_province = isset($arr['province'])?$arr['province']:'';
	$v_province_key = isset($arr['province_key'])?$arr['province_key']:'';
	$v_dsp_tb_tax .= '<td>'.$v_created_by.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_date_modified.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_deleted.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_description.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_fed_tax.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_hst_tax.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_modified_by.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_name.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_pro_tax.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_province.'</td>';
	$v_dsp_tb_tax .= '<td>'.$v_province_key.'</td>';
	$v_dsp_tb_tax .= '</tr>';
}
$v_dsp_tb_tax .= '</table>';
?>