<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_tax_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_tax_sort'])){
	$v_sort = $_SESSION['ss_tb_tax_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_tax = $cls_tb_tax->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_tax_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Tax')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Tax');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Created By', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Date Modified', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Deleted', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Description', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Fed Tax', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Hst Tax', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Modified By', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Pro Tax', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Province', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Province Key', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_tax as $arr){
	$v_excel_col = 1;
	$v_created_by = isset($arr['created_by'])?$arr['created_by']:'';
	$v_date_modified = isset($arr['date_modified'])?$arr['date_modified']:(new MongoDate(time()));
	$v_deleted = isset($arr['deleted'])?$arr['deleted']:0;
	$v_description = isset($arr['description'])?$arr['description']:'';
	$v_fed_tax = isset($arr['fed_tax'])?$arr['fed_tax']:0;
	$v_hst_tax = isset($arr['hst_tax'])?$arr['hst_tax']:'';
	$v_modified_by = isset($arr['modified_by'])?$arr['modified_by']:'';
	$v_name = isset($arr['name'])?$arr['name']:'';
	$v_pro_tax = isset($arr['pro_tax'])?$arr['pro_tax']:0;
	$v_province = isset($arr['province'])?$arr['province']:'';
	$v_province_key = isset($arr['province_key'])?$arr['province_key']:'';
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_created_by, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_date_modified, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_deleted, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_description, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_fed_tax, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_hst_tax, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_modified_by, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_pro_tax, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_province, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_province_key, 'left');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>