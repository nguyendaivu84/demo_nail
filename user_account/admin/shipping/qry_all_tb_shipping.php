<?php if(!isset($v_sval)) die();?>
<?php
$v_quick_search = '';
$v_search_location_name = '';
$v_company_id = $_SESSION['company_id'];
$_SESSION['company_search_id'] = $_SESSION['company_id'];
$v_company_id = $_SESSION['ss_last_company_id'];
$v_page = 1;
if(isset($_POST['btn_advanced_search'])){
    $v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:$v_company_id;
}else if(isset($_POST['btn_advanced_reset'])){
}else{
    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
    $_SESSION['company_search_id'] = $v_company_id;
    $v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
    if(isset($_SESSION['ss_tb_allocations_redirect']) && $_SESSION['ss_tb_allocations_redirect']==1){
        $v_page = isset($_SESSION['ss_tb_allocations_page'])?$_SESSION['ss_tb_allocations_page']:'1';
        settype($v_page,'int');
        if($v_page<1) $v_page = 1;
        if(isset($_SESSION['ss_tb_allocations_where_clause'])){
            $arr_where_clause = unserialize($_SESSION['ss_tb_allocations_where_clause']);
            if(isset($arr_where_clause['company_id'])) $v_company_id = $arr_where_clause['company_id'];
        }
        $v_quick_search = isset($_SESSION['ss_tb_allocations_quick_search'])?$_SESSION['ss_tb_allocations_quick_search']:'';
    }
    $v_search_location_name = $v_quick_search;
}
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id,$arr_global_company,$arr_sort);
//Add code here if required
?>