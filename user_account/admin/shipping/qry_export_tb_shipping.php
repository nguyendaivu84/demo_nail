<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_shipping_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_shipping_sort'])){
	$v_sort = $_SESSION['ss_tb_shipping_sort'];
	$arr_sort = unserialize($v_sort);
}
if(isset($_SESSION['company_id'])){
    if(isset($_SESSION['company_search_id']) && $_SESSION['company_id'] == 10000 ){
        $v_company_id = $_SESSION['company_search_id'];
    }else $v_company_id = $_SESSION['company_id'];
    if($v_company_id!=0){
        settype($v_company_id,"int");
        $arr_where_clause['company_id'] = $v_company_id;
    }
    if(isset($_SESSION['ss_tb_shipping_quick_search']) && $_SESSION['ss_tb_shipping_quick_search']!=''){
        $arr_where_clause['location_name'] = new MongoRegex('/'.$_SESSION['ss_tb_shipping_quick_search'].'/i');
    }
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_shipping = $cls_tb_shipping->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;

$arr_exits = array();
$arr_exits_address = array();
$v_flag = false;

$v_excel_file = 'export_shipping_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15.0;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Shipping')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 1;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Shipping');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Shipping Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Shipper', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Tracking Number', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Tracking Url', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Date Shipping', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ship Status', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ship Create By', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ship Create Date', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location From', 'center', true, true, 20, '', true);
//create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'To Location', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Address', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Shipping Detail', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_shipping as $arr){
	$v_excel_col = 1;
	$v_shipping_id = isset($arr['shipping_id'])?$arr['shipping_id']:0;
	$v_shipper = isset($arr['shipper'])?$arr['shipper']:'';
	$v_tracking_number = isset($arr['tracking_number'])?$arr['tracking_number']:'';
	$v_tracking_url = isset($arr['tracking_url'])?$arr['tracking_url']:'';
	$v_date_shipping = isset($arr['date_shipping'])?$arr['date_shipping']:(new MongoDate(time()));
    $v_date_shipping = date('d-M-Y',$v_date_shipping->sec);
	$v_ship_status = isset($arr['ship_status'])?$arr['ship_status']:0;
    $v_ship_status = $v_ship_status==0?"Dispatch":($v_ship_status==1?"On Delivery":"Delivered");
	$v_ship_create_by = isset($arr['create_by'])?$arr['create_by']:'';
	$v_ship_create_date = isset($arr['ship_create_date'])?$arr['ship_create_date']:(new MongoDate(time()));
    $v_ship_create_date = date('d-M-Y',$v_ship_create_date->sec);
	$v_location_from = isset($arr['location_from'])?$arr['location_from']:0;
    settype($v_location_from,"int");
    $v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
    settype($v_location_id,"int");
    $v_location_name = $cls_tb_location->select_scalar("location_name",array("location_id"=>$v_location_id));//isset($arr['location_name'])?$arr['location_name']:'';
    if(!in_array($v_location_id,$arr_exits)){
        $arr_exits[] = $v_location_id;
        $arr_exits_address[$v_location_id] = $cls_tb_location->get_shipping_address($v_location_id);
        $v_location_address = $arr_exits_address[$v_location_id];
    }else
        $v_location_address = $arr_exits_address[$v_location_id];

	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$arr_shipping_detail = isset($arr['shipping_detail'])?$arr['shipping_detail']:'';
    $v_shipping_detail = '';
    if(count($arr_shipping_detail)>0){
        foreach($arr_shipping_detail as $arr){
            $v_shipping_detail .='\\n Product code:'.$arr['product_code'] ;
            $v_shipping_detail .='- width:'.$arr['width'];
            $v_shipping_detail .='- height:'.$arr['height'];
            $v_shipping_detail .='- unit:'.$arr['unit'];
            $v_shipping_detail .='- quantity:'.$arr['quantity'];
            $v_shipping_detail .='- amount:'.$arr['amount'];
            $v_shipping_detail .='- material_name:'.$arr['material_name'];
            $v_shipping_detail .='- material_color:'.$arr['material_color'];
            $v_shipping_detail .='- material_thickness_value:'.$arr['material_thickness_value'];
            $v_shipping_detail .='- material_thickness_unit:'.$arr['material_thickness_unit'];
        }
    }else {
        $v_shipping_detail = 'N/A';
    }
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_row++ , 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_shipping_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_shipper, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_tracking_number, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_tracking_url, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row,(string)$v_date_shipping , 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_ship_status, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_ship_create_by, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, (string)$v_ship_create_date, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $cls_tb_location->select_scalar("location_name",array("location_id"=>$v_location_from)), 'right');
	//create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_address, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $cls_tb_company->select_scalar("company_name",array("company_id"=>$v_company_id)), 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_shipping_detail , 'left');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(true);
$sheet->getPageSetup()->setFitToHeight(true);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>