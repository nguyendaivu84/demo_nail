<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_tb_shipping_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_shipping_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_shipping_sort'])){
	$v_sort = $_SESSION['ss_tb_shipping_sort'];
	$arr_sort = unserialize($v_sort);
}
if(isset($_SESSION['company_id'])){
    if(isset($_SESSION['company_search_id']) && $_SESSION['company_id'] == 10000 ){
        $v_company_id = $_SESSION['company_search_id'];
    }else $v_company_id = $_SESSION['company_id'];
    if($v_company_id!=0){
        settype($v_company_id,"int");
        $arr_where_clause['company_id'] = $v_company_id;
    }
    if(isset($_SESSION['ss_tb_shipping_quick_search']) && $_SESSION['ss_tb_shipping_quick_search']!=''){
        $arr_where_clause['location_name'] = new MongoRegex('/'.$_SESSION['ss_tb_shipping_quick_search'].'/i');
    }
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_shipping = $cls_tb_shipping->select($arr_where_clause, $arr_sort);
$v_dsp_tb_shipping = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="1" align="center">';

$v_dsp_tb_shipping .= '<tr align="center" valign="middle">';
$v_dsp_tb_shipping .= '<th>Ord</th>';
$v_dsp_tb_shipping .= '<th>Shipping Id</th>';
$v_dsp_tb_shipping .= '<th>Shipper</th>';
$v_dsp_tb_shipping .= '<th>Tracking Number</th>';
$v_dsp_tb_shipping .= '<th>Tracking Url</th>';
$v_dsp_tb_shipping .= '<th>Date Shipping</th>';
$v_dsp_tb_shipping .= '<th>Ship Status</th>';
$v_dsp_tb_shipping .= '<th>Ship Create By</th>';
$v_dsp_tb_shipping .= '<th>Ship Create Date</th>';
$v_dsp_tb_shipping .= '<th>From Location</th>';
//$v_dsp_tb_shipping .= '<th>Location Id</th>';
$v_dsp_tb_shipping .= '<th>To Location</th>';
$v_dsp_tb_shipping .= '<th>Location Address</th>';
$v_dsp_tb_shipping .= '<th>Company</th>';
$v_dsp_tb_shipping .= '<th>Shipping Detail</th>';
$v_dsp_tb_shipping .= '</tr>';
$v_count = 1;
$arr_exits = array();
$arr_exits_address = array();
$v_flag = false;
foreach($arr_tb_shipping as $arr){
	$v_dsp_tb_shipping .= '<tr align="left" valign="middle">';
	$v_dsp_tb_shipping .= '<td align="center">'.($v_count++).'</td>';
	$v_shipping_id = isset($arr['shipping_id'])?$arr['shipping_id']:0;
	$v_shipper = isset($arr['shipper'])?$arr['shipper']:'';
	$v_tracking_number = isset($arr['tracking_number'])?$arr['tracking_number']:'';
	$v_tracking_url = isset($arr['tracking_url'])?$arr['tracking_url']:'';
	$v_date_shipping = isset($arr['date_shipping'])?date('d-M-Y',$arr['date_shipping']->sec):'';
	$v_ship_status = isset($arr['ship_status'])?$arr['ship_status']:0;
    $v_ship_status = $v_ship_status==0?"Dispatch":($v_ship_status==1?"On Delivery":"Delivered");
	$v_ship_create_by = isset($arr['create_by'])?$arr['create_by']:'';
	$v_ship_create_date = isset($arr['create_time'])?date('d-M-Y',$arr['create_time']->sec):'';
	$v_location_from = isset($arr['location_from'])?$arr['location_from']:0;

    $v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
    settype($v_location_id,"int");
    $v_location_name = $cls_tb_location->select_scalar("location_name",array("location_id"=>$v_location_id));//isset($arr['location_name'])?$arr['location_name']:'';
    if(!in_array($v_location_id,$arr_exits)){
        $arr_exits[] = $v_location_id;
        $arr_exits_address[$v_location_id] = $cls_tb_location->get_shipping_address($v_location_id);
        $v_location_address = $arr_exits_address[$v_location_id];
    }else
        $v_location_address = $arr_exits_address[$v_location_id];
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_shipping_detail = isset($arr['shipping_detail'])?$arr['shipping_detail']:'';
    $v_shipping_ = '';
    foreach($v_shipping_detail as $arr){
        $v_shipping_ .= '<b>Product name:</b> '.$arr['product_code'];
        $v_shipping_ .= ' - width: '.$arr['width'];
        $v_shipping_ .= ' - height: '.$arr['height'];
        $v_shipping_ .= ' - amount: '.format_currency($arr['amount']);
        $v_shipping_ .= ' - material_name: '.$arr['material_name'];
        $v_shipping_ .= ' - material_color: '.$arr['material_color'];
        $v_shipping_ .= ' - material_thickness_value: '.$arr['material_thickness_value'];
        $v_shipping_ .= ' - material_thickness_unit: '.$arr['material_thickness_unit']. '<br /> <br />';
    }
	$v_dsp_tb_shipping .= '<td align="center">'.$v_shipping_id.'</td>';
	$v_dsp_tb_shipping .= '<td align="center">'.$v_shipper.'</td>';
	$v_dsp_tb_shipping .= '<td >'.$v_tracking_number.'</td>';
	$v_dsp_tb_shipping .= '<td>'.$v_tracking_url.'</td>';
	$v_dsp_tb_shipping .= '<td align="center">'.$v_date_shipping.'</td>';
	$v_dsp_tb_shipping .= '<td align="center">'.$v_ship_status.'</td>';
	$v_dsp_tb_shipping .= '<td>'.$v_ship_create_by.'</td>';
	$v_dsp_tb_shipping .= '<td align="center">'.$v_ship_create_date.'</td>';
	$v_dsp_tb_shipping .= '<td>'.$cls_tb_location->select_scalar("location_name",array("location_id"=>$v_location_from)).'</td>';
	//$v_dsp_tb_shipping .= '<td align="center">'.$v_location_id.'</td>';
	$v_dsp_tb_shipping .= '<td>'.$v_location_name.'</td>';
	$v_dsp_tb_shipping .= '<td>'.$v_location_address.'</td>';
	$v_dsp_tb_shipping .= '<td>'.$cls_tb_company->select_scalar("company_name",array("company_id"=>$v_company_id)).'</td>';
	$v_dsp_tb_shipping .= '<td style="">'.$v_shipping_.'</td>';
	$v_dsp_tb_shipping .= '</tr>';
}
$v_dsp_tb_shipping .= '</table>';
?>