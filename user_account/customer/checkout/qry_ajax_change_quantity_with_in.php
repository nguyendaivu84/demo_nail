<?php    if(!isset($v_sval)) die;
    if(isset($_POST['txt_product_id']) && isset($_POST['txt_quantity']) && isset($_POST['txt_in']) ){
        $v_product_id = $_POST['txt_product_id'] ;
        $v_select_one = $cls_tb_product->select_one(array("_id"=>new MongoId($v_product_id)));
        if(!$v_select_one) return ("Invalid product");
        $v_product_quantity = (int)$_POST['txt_quantity'] ;
        if($v_product_quantity<=0) return ("Invalid quantity");
        $v_product_index = (int)$_POST['txt_in'] ;
        if($v_product_quantity<0) return ("Invalid product in");
        if(!isset($_SESSION['ss_basket_content'])) return ("Nothing to update!");
        $arr_ss_basket_content = unserialize($_SESSION['ss_basket_content']) ;
        $v_total_price = 0;
        for($i=0;$i<count($arr_ss_basket_content);$i++){
            if(!isset($arr_ss_basket_content[$i]['tax'])) $arr_ss_basket_content[$i]['tax'] = 5;
            if($i!=$v_product_index){
                $v_total_price += $arr_ss_basket_content[$i]['product_quantity'] * $arr_ss_basket_content[$i]['product_price'];
                continue;
            }
            $arr_ss_basket_content[$i]['product_quantity'] = $v_product_quantity;
            $v_total_price += $arr_ss_basket_content[$i]['product_quantity'] * $arr_ss_basket_content[$i]['product_price'];
        }
        $_SESSION['ss_basket_content'] = serialize($arr_ss_basket_content);
        die(format_currency($v_total_price)) ;
    }else{
        die("Invalid data");
    }
?>