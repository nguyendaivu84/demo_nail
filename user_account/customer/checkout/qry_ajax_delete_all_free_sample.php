<?php if(!isset($v_sval)) die;
$arr_ss_basket = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
$arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
delete_all_free_sample($arr_ss_basket,$arr_ss_basket_content,$arr_delete_id);
$_SESSION['ss_basket_item'] = $arr_ss_basket;
$_SESSION['ss_basket_content'] = serialize($arr_ss_basket_content);
if(count($arr_ss_basket)<=0){
    unset($_SESSION['ss_basket_content']);
    unset($_SESSION['ss_basket_item']);
}
header("Content-type: application/json");
echo json_encode(array('new_url'=>URL));
?>