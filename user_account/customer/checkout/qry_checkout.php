<?php if (!isset($v_sval)) die();
$tpl_content = new Template('dsp_checkout.tpl',$v_dir_templates);
// -------------- Sản phẩm trong checkout ----
$v_order_total = 0;
$v_item_info = '';
$v_link_to = 'checkout_accordion';
if(!isset($_SESSION['ss_customer'])){
    $v_link_to = 'checkout_accordion';
}
$v_is_login = 0;
if(isset($_SESSION['ss_customer'])) $v_is_login = 1;
$arr_dsp_item = array();
$arr_brand_name = array();
$arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
$arr_ss_basket_item_id = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
$v_count = count($arr_ss_basket_item_id);
$arr_beauty_grab = array();
for($i=0;$i<count($arr_ss_basket_content);$i++){
//    var_dump($arr_ss_basket_content);die;
    $v_style = '';
	$dsp_item_item = new Template("dsp_checkout_item.tpl",$v_dir_templates);
    $v_product_type = $arr_ss_basket_content[$i]['product_type'];
    $discount = isset($arr_ss_basket_content[$i]['discount'])?$arr_ss_basket_content[$i]['discount']:0;
	if($v_product_type==1){// TH sp miễn phí kèm theo
		$v_price = '<span class="price">FREE</span>';
		$v_item_move_love_remove = '<ul><li class="single remove"><a onclick=delete_item_basket("'.$arr_ss_basket_content[$i]['product_id'].'","'.URL.$arr_ss_basket_content[$i]['product_slugger'].'");  href="javascript:void(0)" id="ci237500005342" class="basketRemove_Href">remove</a></li></ul>';
		$v_item_info = '';
        $v_style = "disabled";
	}else{
        $v_order_total += $arr_ss_basket_content[$i]['product_price']*$arr_ss_basket_content[$i]['product_quantity'];
		$v_price = '<span style="font-size:1.4em;margin-right: 120px;" class="price info_price_">'.format_currency($arr_ss_basket_content[$i]['product_price']).'</span>';
		$v_item_move_love_remove = '<ul><li class="single remove">
		    <a onclick=delete_item_basket("'.$arr_ss_basket_content[$i]['product_id'].'","'.URL.$arr_ss_basket_content[$i]['product_slugger'].'");
		     href="javascript:void(0)" id="ci237500005342" class="basketRemove_Href">remove</a></li><li class="list">
		     </li></ul>';

        $v_item_move_love_remove = '<a onclick=delete_item_basket("'.$arr_ss_basket_content[$i]['product_id'].'","'.URL.$arr_ss_basket_content[$i]['product_slugger'].'");
		     href="javascript:void(0)" id="ci237500005342" class="basketRemove_Href"><img style="margin-right: 10px;margin-top: -3px;" src="/images/btn_trash.gif" /></a>';
		$v_item_info = '<span class="value OneLinkNoTx">Prada Prada Amber Pour Homme Eau de Toilette - 0.05 oz </span>';
	}
    // $arr_beauty_grab_temp = $cls_tb_product->select_scalar("product_grab",array("_id"=>new MongoId($arr_ss_basket_content[$i]['product_id'])));
    // if(is_array($arr_beauty_grab_temp)){
    //     for($j=0;$j<count($arr_beauty_grab_temp);$j++){
    //         $v_id = (string)$arr_beauty_grab_temp[$j]['_id'];
    //         if(!isset($arr_beauty_grab[(string)$v_id])){
    //             $arr_beauty_grab[(string)$v_id] = $arr_beauty_grab_temp[$j];
    //         }
    //     }
    // }
    $v_product_quantity = $arr_ss_basket_content[$i]['product_quantity'];
    $v_product_slugger = $arr_ss_basket_content[$i]['product_slugger'];
    $v_product_image = URL.$arr_ss_basket_content[$i]['product_image'];
    if(!checkRemoteFile($v_product_image)) $v_product_image = JT_URL.$arr_ss_basket_content[$i]['product_image'];
    $v_product_name = $arr_ss_basket_content[$i]['product_name'];
//    $v_product_brand_name = $cls_tb_brand->select_scalar("banner_name",array("_id"=>new MongoId($cls_tb_product->select_scalar("brand_id",array("_id"=> new MongoId($arr_ss_basket_content[$i]['product_id']))))));
    $v_product_brand_name = $arr_ss_basket_content[$i]['brand_name'];
    $v_quantity_option = '';
    for($j=1;$j<=10;$j++){
        $v_selected = '';
        if($j==$v_product_quantity) $v_selected = 'selected="selected"';
        $v_quantity_option .='<option value="'.$j.'" '.$v_selected.' >'.$j.'</option>';
    }
    $dsp_item_item->set('STYLE',$v_style);
    $dsp_item_item->set('PRODUCT_IN',$i);
    $dsp_item_item->set('URL',URL);
//	$dsp_item_item->set('QUANTITY_OPTION',$v_quantity_option);
	$dsp_item_item->set('QUANTITY_OPTION',$v_product_quantity);
	$dsp_item_item->set('PRODUCT_CODE',$v_product_name);
	$dsp_item_item->set('PRODUCT_NAME',$v_product_name);
	$dsp_item_item->set('BRAND_NAME',$v_product_brand_name);
	$dsp_item_item->set('PRODUCT_SLUGGER',$v_product_slugger);
	$dsp_item_item->set('PRODUCT_IMAGE',$v_product_image);
	$dsp_item_item->set('ITEM_PRICE',$v_price);
	$dsp_item_item->set('PRODUCT_ID',$arr_ss_basket_content[$i]['product_id']);
	$dsp_item_item->set('ITEM_MOVE_LOVE_REMOVE',$v_item_move_love_remove);
	$dsp_item_item->set('ITEM_INFO',$v_item_info);
	$arr_dsp_item[] = $dsp_item_item;
}
$v_list_item = is_array($arr_dsp_item) ? Template::merge($arr_dsp_item) : '';
$tpl_content->set('ITEM',$v_list_item);

// -------------- Danh sách sample bên tay phải ----
$arr_dsp_sample = array();
$arr_sample_list = $cls_tb_product->select(array("is_free_sample"=>1));
foreach($arr_sample_list as $arr){
    $v_id = (string)$arr['_id'];
    $v_slugger = (string)$arr['slugger'];
	$dsp_sample_item = new Template("dsp_checkout_sample.tpl",$v_dir_templates);
	// nếu sản phẩm đã chọn rồi thì hiển thị nút remove
	if(in_array($v_id,$arr_ss_basket_item_id))
		$v_button = '<button id="'.$v_id.'" type="button" onclick=delete_item_basket("'.$v_id.'","'.URL.$v_slugger.'");  class="btn btn-default btn-sm btn-remove add_remove_all" data-much="'.$v_slugger.'" data-product-type="'.$v_id.'" value="1601442">remove</button>';
	else
		// nếu chưa thì hiển thị dấu + để người dùng chọn vào danh sách checkout
		$v_button = '<button id="'.$v_id.'" onclick=add_to_basket("'.$v_id.'",1,"'.URL.$v_slugger.'",1); type="button" class="btn btn-default btn-sm btn-add add_remove_all" data-much="'.$v_slugger.'" data-product-type="'.$v_id.'" value="1601442">+</button>';
    $v_image_url = URL.$arr['products_upload'];
    if(!checkRemoteFile($v_image_url)) $v_image_url = JT_URL.$arr['products_upload'];
	$dsp_sample_item->set('PRODUCT_IMAGE',$v_image_url);
	$dsp_sample_item->set('PRODUCT_SKU',$arr['sku']);
	$dsp_sample_item->set('BUTTON',$v_button);
	$arr_dsp_sample[] = $dsp_sample_item;
}
$v_list_samples = is_array($arr_dsp_sample) ? Template::merge($arr_dsp_sample) : '';
$tpl_content->set('LIST_SAMPLES',$v_list_samples);

// -------------- Danh sách beauty grab ----
$dsp_beauty_grab = new Template("dsp_checkout_beauty_grab.tpl",$v_dir_templates);
$arr_beauty_grab_item = array();
//foreach($arr_beauty_grab as $product_id => $content){
//	$dsp_beauty_grab_item = new Template("dsp_checkout_beauty_grab_item.tpl",$v_dir_templates);
//	$v_beauty_grab_item_class = '';
//	if($i%2==1)
//		$v_beauty_grab_item_class = 'odd'; // thêm class odd
//    $v_image_url = URL.$content['product_upload'];
//    if(!checkRemoteFile($v_image_url)) $v_image_url = JT_URL.$content['product_upload'];
//	$dsp_beauty_grab_item->set('BEAUTY_GRAB_ITEM_CLASS',$v_beauty_grab_item_class);
//	$dsp_beauty_grab_item->set('PRODUCT_ID',$content['_id']);
//	$dsp_beauty_grab_item->set('ADD_URL',URL.$content['slugger']);
//	$dsp_beauty_grab_item->set('PRODUCT_NAME',$content['name']);
//	$dsp_beauty_grab_item->set('PRODUCT_PRICE',format_currency($content['sell_price']));
//	$dsp_beauty_grab_item->set('PRODUCT_PRICE_DISCOUNT',(isset($content['discount']) ? format_currency($content['discount']) : 0));
//	$dsp_beauty_grab_item->set('BRAND_NAME',$cls_tb_brand->select_scalar("banner_name",array("_id"=> new MongoId($cls_tb_product->select_scalar("brand_id",array("_id"=>new MongoId($product_id)))))));
//	$dsp_beauty_grab_item->set('PRODUCT_IMAGE',$v_image_url);
//	$dsp_beauty_grab_item->set('PRODUCT_SLUGGER',$content['slugger']);
//	$arr_beauty_grab_item[] = $dsp_beauty_grab_item;
//}
$v_list_beauty_grab = is_array($arr_beauty_grab_item) ? Template::merge($arr_beauty_grab_item) : '';
$dsp_beauty_grab->set('LIST_BEAUTY_ITEM',$v_list_beauty_grab);
$tpl_content->set('TOTAL_ORDER',format_currency($v_order_total));

$v_not_sign_in_message = 'Please <a class="js-sign-in" id="checkout-sign-in" href="javascript:void(0)">sign in</a> if you are trying to retrieve a shopping cart created in the past.';
if(isset($_SESSION['ss_customer'])) $v_not_sign_in_message = '';
$tpl_content->set('LIST_BEAUTY_GRAB',$dsp_beauty_grab->output());
$tpl_content->set('NOT_SIGN_IN_MESSAGE',$v_not_sign_in_message);
$tpl_content->set('LINK_TO',$v_link_to);
$tpl_content->set('URL_CHECKOUT',URL);
$tpl_content->set('URL_CHECKOUT',URL);
$tpl_content->set('COUNT',$v_count);
$tpl_content->set('IS_LOGIN',$v_is_login);

echo $tpl_content->output();
