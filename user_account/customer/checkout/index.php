<?php if(!isset($v_sval)) die();
add_class("cls_tb_banner");
$cls_tb_brand = new cls_tb_banner($db);
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);
if(isset($_POST['ajax'])){
    if($_POST['ajax'] == 'delete_all_free_sample')
        include 'qry_ajax_delete_all_free_sample.php';
    else  if($_POST['ajax'] == 'change_provinces')
        include 'qry_change_province_update_tax.php';
    else  if($_POST['ajax'] == 'change_quantity_with_in')
        include 'qry_ajax_change_quantity_with_in.php';
}else{
    include $v_head.'header.php';
    include $v_head.'checkout/qry_checkout.php';
    include $v_head.'checkout_login/footer.php';}