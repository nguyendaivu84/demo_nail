<?php if(!isset($v_sval)) die;
    $arr_return = array('error'=>1,'message'=>'Invalid data','NewTaxValue'=>0,'totalPrice'=>0);
    if(isset($_POST['provinces']) && isset($_SESSION['ss_basket_content'])){
        add_class("cls_tb_tax");
        add_class("cls_tb_province");
        $provinceKey = isset($_POST['provinces']) ? $_POST['provinces'] : '';
        $provinceKeyCheck = checkProvincesKeyValid($provinceKey,$db);
        if($provinceKeyCheck){
            $arr_session = unserialize($_SESSION['ss_basket_content']);
            $arr_new_session = update_session_tax($provinceKey,$arr_session,$db);
            $_SESSION['ss_basket_content'] = serialize($arr_new_session['data']);
            $arr_return = array('error'=>0,'message'=>'','NewTaxValue'=>(float)number_format($arr_new_session['NewTaxValue'],2),'totalPrice'=>(float)number_format($arr_new_session['totalPrice'],2));
        }
    }
    header("Content-type: application/json");
    echo json_encode($arr_return);
?>