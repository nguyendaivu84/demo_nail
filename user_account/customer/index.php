<?php if(!isset($v_sval)) die();
error_reporting(E_ALL);
function pr($data){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}
include_once "classes/xtemplate.class.php";
$v_customer_id = isset($_REQUEST['id'])? $_REQUEST['id'] : 0;
$v_acc = isset($_REQUEST['acc'])? $_REQUEST['acc'] : '';
//die;
require 'classes/cls_tb_contact.php';
add_class("cls_settings");
$cls_settings = new cls_settings($db, LOG_DIR);
add_class('cls_tb_module');
$cls_module = new cls_tb_module($db, LOG_DIR);
add_class("cls_tb_message");
$cls_tb_message = new cls_tb_message($db,LOG_DIR);
require 'classes/cls_tb_global.php';
$cls_tb_global = new cls_tb_global($db,LOG_DIR);
$v_contact_id = (int) get_unserialize_user('contact_id');
$v_company_id = (int) get_unserialize_user('company_id');
$v_customer_template_id = '';
$v_templates_name = 'default';
$v_url = URL;
$v_website_testing = false;
$v_break = (int) get_unserialize_user('break');
$v_company_code = '';

$v_templates = URL."user_account/customer/templates/".$v_templates_name;
$v_dir_templates = "user_account/customer/templates/default";

switch($v_acc)
{
	case 'CAT':
		$v_navigator_name = "Catalogue";
		include "user_account/customer/catalogue/index.php";
		break;
	case 'STATIC_CONTENT':
		$v_navigator_name = "static_content";
		include "user_account/customer/static_content/index.php";
		break;
	case 'BRAND':
		$v_navigator_name = "BRAND";
		include "user_account/customer/brand/index.php";
		break;
	case 'CHECKOUT':
		$v_navigator_name = "CHECKOUT";
		include "user_account/customer/checkout/index.php";
		break;
	case 'CHECKOUT_LOGIN':
		$v_navigator_name = "CHECKOUT_LOGIN";
		include "user_account/customer/checkout_login/index.php";
		break;
	case 'CHECKOUT_ACCORDION':
		$v_navigator_name = "CHECKOUT_ACCORDION";
		include "user_account/customer/checkout_accordion/index.php";
		break;
	case 'CHECKOUT_GIFT':
		$v_navigator_name = "CHECKOUT_GIFT";
		include "user_account/customer/checkout_gift/index.php";
		break;
	case 'CHECKOUT_CONFIRM':
		$v_navigator_name = "CHECKOUT_CONFIRM";
		include "user_account/customer/checkout_confirm/index.php";
		break;
	case 'REGISTER':
		$v_navigator_name = "REGISTER";
		include "user_account/customer/register/index.php";
		break;
    case 'RESETPASSWORD':
		$v_navigator_name = "RESETPASSWORD";
		include "user_account/customer/resetpassword/index.php";
		break;
	case 'SIGN_IN':
		$v_navigator_name = "SIGN_IN";
		include "user_account/customer/sign_in/index.php";
		break;
    case 'POPUP':
		$v_navigator_name = "POPUP";
		include "user_account/customer/popup/index.php";
		break;
    case 'SEARCH':
		$v_navigator_name = "SEARCH";
		include "user_account/customer/search/index.php";
		break;
	case 'STORE':
		$v_navigator_name = "STORE";
		include "user_account/customer/store/index.php";
		break;
    case 'PROFILE':
		$v_navigator_name = "PROFILE";
		include "user_account/customer/myaccount/index.php";
		break;
	case 'SAMPLE':
		$v_navigator_name = "SAMPLE";
		include "user_account/customer/sample/index.php";
		break;
	case 'LOVE':
		$v_navigator_name = "LOVE";
		include "user_account/customer/love/index.php";
		break;
    case 'PRINTLOVE':
        $v_navigator_name = "PRINTLOVE";
        include "user_account/customer/printlove/index.php";
        break;
    case 'SHARELOVE':
        $v_navigator_name = "LOVE";
        include "user_account/customer/sharelove/index.php";
        break;
	case 'BEAUTYINSIDER':
		$v_navigator_name = "BEAUTYINSIDER";
		include "user_account/customer/beautyinsider/index.php";
		break;
	case 'MYINFORMATION':
		$v_navigator_name = "MYINFORMATION";
		include "user_account/customer/myinformation/index.php";
		break;
	default:
		$v_navigator_name = "Home"; // BaoNam
		include "user_account/customer/home/index.php"; // BaoNam
		break;
}