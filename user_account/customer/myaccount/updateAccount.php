<?php if(!isset($v_sval)) die;
    if(!isset($_SESSION['ss_customer'])) die;
    $arr_return = array('error'=>1,'message'=>'Invalid data');
    $arr_user = unserialize($_SESSION['ss_customer']);
    if(isset($_POST['txt_key'])){
        $error_found = false;
        $arr_field = array();
        $arr_field_value = array();
        $v_key = $_POST['txt_key'];
        if($v_key=='name'){
            $v_first_name = $_POST['txt_first_name'];
            $v_last_name = $_POST['txt_last_name'];
            if($v_first_name=='' && $v_last_name==''){
                $arr_return['message'] = 'First and Last name cannot empty';
                $error_found = true;
            }
            else if($v_first_name==''){
                $error_found = true;
                $arr_return['message'] = 'First name cannot empty';
            }else if($v_last_name==''){
                $error_found = true;
                $arr_return['message'] = 'Last name cannot empty';
            }else{
                $error_found = false;
                $arr_user ['first_name'] = $v_first_name;
                $arr_user ['last_name'] = $v_last_name;
                $arr_field = array('first_name','last_name');
                $arr_field_value = array($v_first_name,$v_last_name);
            }
        }else if($v_key=='email'){
            $v_email = $_POST['txt_user_name'];
            $v_confirm_email = $_POST['txt_confirm_user_name'];
            if(!is_valid_email($v_email) || !is_valid_email($v_confirm_email)){
                $error_found = true;
                $arr_return['message'] = 'Email or confirm email is invalid';
            }else{
                $v_check = $cls_tb_contact->count(array('email'=>$v_email,array('$ne'=>array('_id'=>$arr_user['_id']))));
                if(!$v_check){
                    $error_found = false;
                    $arr_field  = array('email','user_email');
                    $arr_field_value  = array($v_email,$v_email);
                    $arr_user['email'] = $v_email;
                }else{
                    $error_found = true;
                    $arr_return['message'] = 'An account already exists for this email. Please sign in or choose another e-mail address.';
                }
            }
        }else if($v_key=='passwordUpdate'){
            $v_pass = $_POST['txt_password'];
            $v_confirm_pass = $_POST['txt_password_confirmation'];
            if($v_pass=='' || $v_confirm_pass==''){
                $error_found = true;
                $arr_return['message'] = 'Password or confirm password is empty';
            }
            if(strlen($v_pass)<6 || strlen($v_confirm_pass)<6){
                $error_found = true;
                $arr_return['message'] = 'Password must have at least 6 letter';
            }else{
                if($v_pass!=$v_confirm_pass){
                    $error_found = true;
                    $arr_return['message'] = 'Password confirm not match';
                }else{
                    $error_found = false;
                    $arr_field  = array('user_password');
                    $arr_field_value  = array(md5($v_pass));
                }
            }
        }
        if(!$error_found){
            $v_check = $cls_tb_contact->update_fields($arr_field,$arr_field_value,array('_id'=>$arr_user['_id']));
            if($v_check){
                $arr_return['error'] = 0;
                $_SESSION['ss_customer'] = serialize($arr_user);
                if($v_key=='name'){
                    $arr_return['message'] = 'Update name successful';
                }else if($v_key=='email'){
                    $arr_return['message'] = 'Update email successful';
                }else if($v_key=='passwordUpdate'){
                    $arr_return['message'] = 'Update password successful';
                }
            }
        }
    }
    header("Content-type: application/json");
    echo json_encode($arr_return);
