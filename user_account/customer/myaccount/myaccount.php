<?php if(!isset($v_sval)) die();
if(!isset($_SESSION['ss_customer'])) redir('/');
$v_key = isset($_GET['txt_key']) ? $_GET['txt_key'] : '';
$v_dsp_view = '';
$v_dsp_view_name = '';
$arr_user = unserialize($_SESSION['ss_customer']);
if($v_key=='my-information'){
    $v_first_name = $arr_user['first_name'];
    $v_last_name = $arr_user['last_name'];
    $v_full_name = $arr_user['full_name'];
    $v_email = $arr_user['email'];
    if($v_full_name=='') $v_full_name = $arr_user['first_name']." ".$arr_user['middle_name']." ".$arr_user['last_name'];
    $v_dsp_view_name = 'My information';
    $dsp_account_information = new Template("dsp_update_my_account_information.tpl",$v_dir_templates);
    $dsp_account_information->set('FULL_NAME',$v_full_name);
    $dsp_account_information->set('EMAIL',$v_email);
    $dsp_account_information->set('FIRST_NAME',$v_first_name);
    $dsp_account_information->set('LAST_NAME',$v_last_name);
    $v_dsp_view = $dsp_account_information->output();
}else if (strpos($v_key,'TOKEN') !== false){
    $v_key = str_replace("TOKEN=","",$v_key);
    $check = $cls_tb_contact->count(array('_id'=> new MongoId($v_key)));
    if(!$check){
        $v_dsp_view = '<h2>Token broken!. Please try again later</h2>';
    }else{
        $check = $cls_tb_contact->update_field("inactive",1,array("_id"=>new MongoId($v_key)));
        if($check){
            $v_dsp_view = '<h2>Account has been active successful</h2>';
        }
    }
}
$dsp_static_content = new Template("dsp_profile.tpl",$v_dir_templates);
$dsp_static_content->set('CONTENT',$v_dsp_view);
$dsp_static_content->set('DESCRIPTION',$v_dsp_view_name);
$dsp_static_content->set('CURRENT_PAGE',$v_key);
$dsp_static_content->set('URL',URL);
echo $dsp_static_content->output();
?>