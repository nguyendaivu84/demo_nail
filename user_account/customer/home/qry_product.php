<?php if (!isset($v_sval)) die();
add_class("cls_tb_nail_category");
add_class("cls_tb_banner");
add_class("cls_tb_message");
$cls_tb_banner = new cls_tb_banner($db);
$cls_tb_message = new cls_tb_message($db);
$v_product_slugger = isset($_GET['_url']) ? $_GET['_url'] : '';
$v_select_one = $cls_tb_product->select_one(array("slugger"=>$v_product_slugger,"deleted"=>false));
if(!$v_select_one) redir("/");
$arr_ss_language = array();
if(isset($_SESSION['ss_language_value'])) $arr_ss_language = unserialize($_SESSION['ss_language_value']);
$v_limited_edition = isset($arr_ss_language[$v_language]['LIMITED_EDITION']) ? $arr_ss_language[$v_language]['LIMITED_EDITION'] : '';
$v_view_lager = isset($arr_ss_language[$v_language]['VIEW_LAGER']) ? $arr_ss_language[$v_language]['VIEW_LAGER'] : '';
$v_add_to_love = isset($arr_ss_language[$v_language]['ADD_TO_LOVE']) ? $arr_ss_language[$v_language]['ADD_TO_LOVE'] : '';
$v_is_loved = 0;
$v_class_love = '';
$v_add_delete = 'add_love';
$arr_love = isset($_SESSION['ss_love']) ? unserialize($_SESSION['ss_love']) : array();
$arr_love_item = isset($_SESSION['ss_love_item']) ? unserialize($_SESSION['ss_love_item']) : array();
if(in_array((string)$cls_tb_product->get_mongo_id(),$arr_love)){
    $v_add_to_love = 'loved';
    $v_class_love = 'loved';
    $v_is_loved = 1;
    $v_add_delete = 'delete_love';
}
$v_on_click_love = "add_item_to_love_list('".$v_product_slugger."','".$v_add_delete."');";
$v_add_to_basket = isset($arr_ss_language[$v_language]['ADD_TO_BASKET']) ? $arr_ss_language[$v_language]['ADD_TO_BASKET'] : '';
$v_find_in_store = isset($arr_ss_language[$v_language]['FIND_IN_STORE']) ? $arr_ss_language[$v_language]['FIND_IN_STORE'] : '';
$v_description = isset($arr_ss_language[$v_language]['DESCRIPTION']) ? $arr_ss_language[$v_language]['DESCRIPTION'] : $cls_tb_product->get_product_indgridient();
$v_ingredients = isset($arr_ss_language[$v_language]['INGREDIENTS']) ? $arr_ss_language[$v_language]['INGREDIENTS'] : $cls_tb_product->get_product_indgridient();
$v_shipping_info = isset($arr_ss_language[$v_language]['SHIPPING_INFO']) ? $arr_ss_language[$v_language]['SHIPPING_INFO'] : '';
$v_also_like = isset($arr_ss_language[$v_language]['ALSO_LIKE']) ? $arr_ss_language[$v_language]['ALSO_LIKE'] : '';
$v_rating_review = isset($arr_ss_language[$v_language]['RATING_REVIEWS']) ? $arr_ss_language[$v_language]['RATING_REVIEWS'] : '';
$v_tab_rating_view = isset($arr_ss_language[$v_language]['TAB_RATING_REVIEW']) ? $arr_ss_language[$v_language]['TAB_RATING_REVIEW'] : '';
$v_tab_q_a = isset($arr_ss_language[$v_language]['TAB_Q_A']) ? $arr_ss_language[$v_language]['TAB_Q_A'] : '';
$v_write_review = isset($arr_ss_language[$v_language]['WRITE_REVIEW']) ? $arr_ss_language[$v_language]['WRITE_REVIEW'] : '';
$v_write_question = isset($arr_ss_language[$v_language]['ASK_QUESTION']) ? $arr_ss_language[$v_language]['ASK_QUESTION'] : '';
$v_find_in_store_placeholder = isset($arr_ss_language[$v_language]['FIND_IN_STORE_PLACEHOLDER']) ? $arr_ss_language[$v_language]['FIND_IN_STORE_PLACEHOLDER'] : '';
$v_brand_id = $cls_tb_product->get_brand_id();
//$arr_brand = $cls_tb_banner->select_limit_fields(0,1,array("banner_name","banner_logo","banner_slugger"),array("_id"=>new MongoId($v_brand_id)));
//$arr_brand = iterator_to_array($arr_brand);
$arr_brand = array();
$v_brand_name = isset($arr_brand[$v_brand_id]['banner_name']) ? $arr_brand[$v_brand_id]['banner_name'] : '';
$v_brand_image = isset($arr_brand[$v_brand_id]['banner_logo'])?$arr_brand[$v_brand_id]['banner_logo']:'';
$v_brand_slugger = isset($arr_brand[$v_brand_id]['banner_slugger'])?$arr_brand[$v_brand_id]['banner_slugger']:'';
//BRAND_IMAGE
$v_product_sku = $cls_tb_product->get_sku();
$v_product_name = $cls_tb_product->get_name();
$v_product_code = $cls_tb_product->get_code();
$v_mongo_id = $cls_tb_product->get_mongo_id();
$v_product_price = $cls_tb_product->get_sell_price();
$v_product_discount_price = $cls_tb_product->get_product_sell_discount();
$v_product_des = $cls_tb_product->get_product_decription();
$v_product_ing = $cls_tb_product->get_product_indgridient();
$v_product_image = '';
if(checkRemoteFile(URL.$cls_tb_product->get_products_upload()))
    $v_product_image = str_replace('//', '/', URL.$cls_tb_product->get_products_upload());
else if(checkRemoteFile(JT_URL.$cls_tb_product->get_products_upload())){
    $v_product_image = JT_URL.$cls_tb_product->get_products_upload();
}
$v_product_image = URL.'images/product_sample.jpg';
$v_category = $cls_tb_product->get_category_id();
$cls_tb_nail_category = new cls_tb_nail_category($db);
$cls_tb_nail_category->select_one(array("_id"=> new MongoId($v_category)));
$v_child_name = $cls_tb_nail_category->get_name();
$v_child_slugger = $cls_tb_nail_category->get_slugger();
$v_child_id = $cls_tb_nail_category->get_mongo_id();
$v_headline_id = $cls_tb_nail_category->get_parent_id();
$cls_tb_nail_category->select_one(array("_id"=> new MongoId($v_headline_id)));
$v_headline_name = $cls_tb_nail_category->get_name();
$v_headline_slugger = $cls_tb_nail_category->get_slugger();
$v_parent_id = $cls_tb_nail_category->get_parent_id();
$v_parent_name = $cls_tb_nail_category->get_name();
$v_parent_slugger = $cls_tb_nail_category->get_slugger();
$tpl_content = new Template('dsp_product.tpl',$v_dir_templates);
$v_url_default = '/user_account/customer/templates/default';
$tpl_content->set('URL_DEFAULT',$v_url_default);
$tpl_sidebar = new Template('dsp_product_sidebar.tpl',$v_dir_templates);
$arr_product_slider = $cls_tb_product->get_product_slide_image();
$v_slider = '';
$v_popup_slider = '';
$i=0;
for(;$i<count($arr_product_slider);$i++){
    if($arr_product_slider[$i]!=''){
        $v_url_temp = URL.$arr_product_slider[$i];
        if(!checkRemoteFile($v_url_temp)){
            $v_url_temp = JT_URL.$arr_product_slider[$i];
        }
        if(checkRemoteFile($v_url_temp)) {
            if($i==0){
                $v_product_image = $v_url_temp;
            }
            $v_slider .= '<a id="a_item_'.$i.'" href="javascript:void(0)" class="product-thumb"> <img id="slide-item-'.$i.'" onclick="change_image('.$i.');" onmouseout="restore_image('.$i.');" onmouseover="change_image_template('.$i.');" data-link="'.$v_url_temp.'" src="'.$v_url_temp.'" alt="thumb-4" width="36" height="36"></a>';
            $v_popup_slider .= '<a id="popup_item_'.$i.'" href="javascript:void(0)" class="product-thumb"> <img id="popup-item-'.$i.'" onclick="change_image('.$i.');" onmouseout="restore_image('.$i.');" onmouseover="change_image_template('.$i.');" data-link="'.$v_url_temp.'" src="'.$v_url_temp.'" alt="thumb-4" width="36" height="36"></a>';
        }
    }
}
$i_next = $i+1;
//$v_slider = '<a id="a_item_'.$i_next.'" href="javascript:void(0)" class="product-thumb current"> <img id="slide-item-'.$i_next.'" onclick="change_image('.$i_next.');" onmouseout="restore_image('.$i_next.');" onmouseover="change_image_template('.$i_next.');" data-link="'.$v_product_image.'" src="'.$v_product_image.'" alt="thumb-4" width="36" height="36"></a>';
//$v_popup_slider .= '<a id="popup_item_'.$i_next.'" href="javascript:void(0)" class="product-thumb current"> <img id="popup-item-'.$i_next.'" onclick="change_image('.$i_next.');" onmouseout="restore_image('.$i_next.');" onmouseover="change_image_template('.$i_next.');" data-link="'.$v_product_image.'" src="'.$v_product_image.'" alt="thumb-4" width="36" height="36"></a>';
if($v_popup_slider!=''){
    $v_view_lager = '<div class="view-larger-trigger"> <a href="javascript:void(0)" id="view_larger" rel="#modal-view-larger" style=" font-family: fontsDosis; font-weight: 600; font-style: normal; font-size:15px"> '.$v_view_lager;
    $v_view_lager .='<span class="icon icon-view-large" style="display:none"></span></a></div>';
}else{
    $v_view_lager = '';
}
$v_product_type = $cls_tb_product->get_limited_edition() == 1 ? "Limited edition" :'';
$v_product_type .= $cls_tb_product->get_exclusive() == 1 ? "Exclusive" :'';
$v_product_type .= $cls_tb_product->get_online_only() == 1 ? "Online only" :'';
$tpl_sidebar->set('LIMITED_EDITION_SIDEBAR',$v_product_type);
$tpl_sidebar->set('MINI_IMAGE',$v_slider);
$tpl_content->set('PRODUCT_SIDEBAR',$tpl_sidebar->output());
    $v_k = 1;

    $tpl_ymal = new Template('dsp_product_ymal.tpl',$v_dir_templates);




// PRODUCT LIEN QUAN
$arr_other_product = $cls_tb_product->select(array("deleted"=>false,"category_id"=> (string) $v_child_id));

$tpl_slide = '';
$arr_tpl_slide = array();
$i=0;


foreach($arr_other_product as $arr){
    if($v_product_code ==$arr['code']) continue;
    $v_url_temp = URL.$arr['products_upload'];
    if(!checkRemoteFile($v_url_temp)){
        $v_url_temp = JT_URL.$arr['products_upload'];
    }
    $tpl_slide = new Template('dsp_product_slide.tpl',$v_dir_templates);
	$tpl_slide->set('DATA_INDEX',$i++);
	$tpl_slide->set('URL_DEFAULT',JT_URL);
	$tpl_slide->set('SLUGGER',$arr['slugger']);
	$tpl_slide->set('PRODUCT_PRICE2',format_currency($arr['sell_price']));
	$tpl_slide->set('PRODUCT_NAME2',$arr['sku']);
	$tpl_slide->set('PRODUCT_IMAGE',$v_url_temp);
    $arr_tpl_slide[] = $tpl_slide;
}
if($i==0)
{
    $tpl_ymal->set('ALSO_LIKE','');    
}
else
{
       $tpl_ymal->set('ALSO_LIKE',$v_also_like);
}
    $dsp_tpl_slide = Template::merge($arr_tpl_slide);
    $tpl_ymal->set('PRODUCT_SLIDE',$dsp_tpl_slide);
    $tpl_content->set('PRODUCT_YMAL',$tpl_ymal->output());

$tpl_rating_review = new Template('dsp_product_rating_review.tpl',$v_dir_templates);
$v_dsp_rating_item = '';
$arr_dsp_rating_item = array();
for($i=0;$i<3;$i++){
    $tpl_rating_item = new Template('dsp_rating_item.tpl',$v_dir_templates);
    $arr_dsp_rating_item [] = $tpl_rating_item;
}
$v_dsp_sort_ration_option = '';
$arr_dsp_sort_ration_option = '';
for($i=0;$i<10;$i++){
    $tpl_rating_sort_option = new Template('dsp_sort_rating_option.tpl',$v_dir_templates);
    $tpl_rating_sort_option->set('OPTION',$i+1);
    $arr_dsp_sort_ration_option [] = $tpl_rating_sort_option;
}
$v_dsp_sort_ration_option = Template::merge($arr_dsp_sort_ration_option);
$tpl_rating_item_pagetion = new Template('dsp_rating_pagitaion.tpl',$v_dir_templates);
$tpl_rating_form = new Template('dsp_rating_form.tpl',$v_dir_templates);
$v_dsp_rating_item = Template::merge($arr_dsp_rating_item);
$tpl_rating_review->set('SORT_RATING_ITEM',$v_dsp_sort_ration_option);
$tpl_rating_review->set('RATING_REVIEWS',$v_rating_review);
$tpl_rating_review->set('SORT_RATING',$v_rating_review);
$tpl_rating_review->set('RATING_FORM',$tpl_rating_form->output());
$tpl_rating_review->set('RATING_ITEMS',$v_dsp_rating_item);
$tpl_rating_review->set('RATING_ITEMS_PAGETION',$tpl_rating_item_pagetion->output());
$tpl_rating_review->set('TAB_RATING_REVIEW',$v_tab_rating_view);
$tpl_rating_review->set('TAB_Q_A',$v_tab_q_a);
$tpl_rating_review->set('WRITE_REVIEW',$v_write_review);
$tpl_rating_review->set('ASK_QUESTION',$v_write_question);
$tpl_content->set('PRODUCT_RATING_REVIEW',$tpl_rating_review->output());
$tpl_content->set('PRODUCT_SKU',$v_product_sku);
$tpl_content->set('PRODUCT_NAME',ucwords($v_product_name));
$tpl_content->set('ON_CLICK_EVENT',$v_on_click_love);
$tpl_content->set('PRODUCT_SLUGGER',$v_product_slugger);
$tpl_content->set('CLASS_LOVE',$v_class_love);
$tpl_content->set('IS_ON_LOVE',$v_is_loved);
$tpl_content->set('PRODUCT_CODE',$v_product_code);
$tpl_content->set('PRODUCT_PRICE',format_currency($v_product_price));
$tpl_content->set('PRODUCT_PRICE_DISCOUNT',format_currency($v_product_discount_price));
$tpl_content->set('PRODUCT_IMAGE',$v_product_image);
$tpl_content->set('PRODUCT_NAME_IMAGE',$v_product_image);
$tpl_content->set('CATEGORY_PARENT_NAME',$v_parent_name);
$tpl_content->set('CATEGORY_HEADLINE_NAME',$v_headline_name);
$tpl_content->set('CATEGORY_CHILD_NAME',$v_child_name);
$tpl_content->set('PRODUCT_DES',html_entity_decode($v_product_des));
$tpl_content->set('PRODUCT_ING',html_entity_decode($v_product_ing));
$tpl_content->set('LIMITED_EDITION',$v_product_type);
$tpl_content->set('VIEW_LAGER',$v_view_lager);
$tpl_content->set('ADD_TO_BASKET',$v_add_to_basket);
$tpl_content->set('ADD_TO_LOVE',$v_add_to_love);
$tpl_content->set('FIND_IN_STORE',$v_find_in_store);
$tpl_content->set('FIND_IN_STORE_PLACEHOLDER',$v_find_in_store_placeholder);
$tpl_content->set('DESCRIPTION',$v_description);
$tpl_content->set('INGREDIENTS',$v_ingredients);
$tpl_content->set('SHIPPING_INFO',$v_shipping_info);
$tpl_content->set('BRAND_NAME',$v_brand_name);
$tpl_content->set('BRAND_SLUGGER',$v_brand_slugger);
$tpl_content->set('URL_BRAND',URL."resources/brand/");
$tpl_content->set('BRAND_IMAGE',$v_brand_image);
$tpl_content->set('PARENT_SLUGGER',$v_parent_slugger);
$tpl_content->set('HEADLINE_SLUGGER',$v_headline_slugger);
$tpl_content->set('CHILD_SLUGGER',$v_child_slugger);
$tpl_content->set('PRODUCT_ID',$v_mongo_id);
$tpl_content->set('PRODUCT_URL',URL.$v_product_slugger);
$tpl_content->set('URL',URL);
$tpl_content->set('POPUP_ITEM',$v_popup_slider);
echo $tpl_content->output();
//create_cache($pathCache);
