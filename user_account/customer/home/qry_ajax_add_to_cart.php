<?php if(!isset($v_sval)) die;
$arr_return = array('error'=>1, 'message'=>'Invalid data','count'=>0,'checkout_item'=>'','image_link'=>'','price'=>'','brand_name'=>'','p_name'=>'','code'=>'');
$v_product_id = isset($_POST['txt_product_id']) ? $_POST['txt_product_id'] : '';
$v_product_id = trim($v_product_id);
$v_product_id = strip_tags($v_product_id);
$v_product_id = addslashes($v_product_id);
$v_product_quantity = isset($_POST['txt_product_quantity']) ? $_POST['txt_product_quantity'] : 0;
$v_type = isset($_POST['txt_type']) ? $_POST['txt_type'] : 0;
settype($v_type,"int");
settype($v_product_quantity,"int");
add_class("cls_tb_banner");
$cls_tb_banner = new cls_tb_banner($db);
if($v_product_id!='' && $v_product_quantity >0 ){
	$v_select_one = $cls_tb_product->select_one(array("_id"=> new MongoId($v_product_id)));
	if($v_select_one){
		$arr_return = array('error'=>0, 'message'=>'','count'=>0);
		$v_user_id = '';
		if(isset($_SESSION['ss_customer'])){
			$arr_user = unserialize($_SESSION['ss_customer']);
			$v_user_id = (string)$arr_user['_id'];
		}
		$arr_ss_basket = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
		$arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
		$v_check = session_basket($arr_ss_basket,$arr_ss_basket_content,$v_product_id,$v_product_quantity,$v_user_id,$db);
		if($v_check){
			$_SESSION['ss_basket_item'] = $arr_ss_basket;
			$_SESSION['ss_basket_content'] = serialize($arr_ss_basket_content);
			$arr_return['count'] = count($arr_ss_basket);
			$v_image_link = URL.$cls_tb_product->get_products_upload();
			if(!checkRemoteFile($v_image_link)) $v_image_link = JT_URL.$cls_tb_product->get_products_upload();
			$arr_return['image_link'] = $v_image_link;
			$arr_return['price'] = $cls_tb_product->get_sell_price();
			$arr_return['p_name'] = $cls_tb_product->get_sku();
			$arr_return['brand_name'] = 'no brand';//$cls_tb_banner->select_scalar("banner_name",array("_id"=>new MongoId($cls_tb_product->get_brand_id())));
			$arr_return['code'] = $cls_tb_product->get_code();
			// div checkout for checkout
			if($v_type){
				$dsp_item_item = new Template("dsp_checkout_item.tpl",$v_dir_templates);
				$v_product_type = $cls_tb_product->get_is_free_sample();
				if($cls_tb_product->get_is_free_sample()==1){
					$v_price = '<span class="price">FREE</span>';
					$v_item_move_love_remove = '<ul><li class="single remove"><a onclick=delete_item_basket("'.(string)$cls_tb_product->get_mongo_id().'","'.URL.$cls_tb_product->get_slugger().'"); href="javascript:void(0);" id="ci237500005342" class="basketRemove_Href">remove</a></li></ul>';
					$v_item_info = '';
				}else{
					// never go here, but just in case
					$v_price = '<span class="currency">C</span><span class="price">'.format_currency($cls_tb_product->get_sell_price()).'</span>';
					$v_item_move_love_remove = '<ul><li class="single remove"><a onclick=delete_item_basket("'.(string)$cls_tb_product->get_mongo_id().'","'.URL.$cls_tb_product->get_slugger().'"); href="javascript:void(0);"  id="ci237500005342" class="basketRemove_Href">remove</a></li><li class="list"><a href="#" id="1539543,ci237500005342,">move to loves</a></li></ul>';
					$v_item_info = '<span class="value OneLinkNoTx">Prada Prada Amber Pour Homme Eau de Toilette - 0.05 oz </span>';
				}
				$v_product_slugger = $cls_tb_product->get_slugger();
				$v_product_image = JT_URL.$cls_tb_product->get_products_upload();
				$v_product_name = $cls_tb_product->get_name();
				$v_product_brand_id = $cls_tb_product->get_brand_id();
				$v_product_brand_name = 'no brand';//$cls_tb_banner->select_scalar("banner_name",array("_id"=>new MongoId($v_product_brand_id)));
				$v_quantity_option = '';
				for($j=1;$j<=10;$j++){
					$v_selected = '';
					if($j==$v_product_quantity) $v_selected = 'selected="selected"';
					$v_quantity_option .='<option value="'.$j.'" '.$v_selected.' >'.$j.'</option>';
				}
				$dsp_item_item->set('QUANTITY_OPTION',$v_quantity_option);
				$dsp_item_item->set('STYLE',"disabled");
				$dsp_item_item->set('PRODUCT_CODE',$v_product_name);
				$dsp_item_item->set('PRODUCT_NAME',$v_product_name);
				$dsp_item_item->set('PRODUCT_ID',$v_product_id);
				$dsp_item_item->set('BRAND_NAME',$v_product_brand_name);
				$dsp_item_item->set('PRODUCT_SLUGGER',$v_product_slugger);
				$dsp_item_item->set('PRODUCT_IMAGE',$v_product_image);
				$dsp_item_item->set('ITEM_PRICE',$v_price);
				$dsp_item_item->set('ITEM_MOVE_LOVE_REMOVE',$v_item_move_love_remove);
				$dsp_item_item->set('ITEM_INFO',$v_item_info);
				$arr_return['checkout_item'] = $dsp_item_item->output();
			}
		}else{
			$arr_return = array('error'=>1, 'message'=>'Product already exsited','count'=>0);
		}
	}
}
header("Content-type: application/json");
echo json_encode($arr_return);