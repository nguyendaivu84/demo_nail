<?php if (!isset($v_sval)) die();
add_class("cls_tb_nail_category");
$cls_tb_nail_category = new cls_tb_nail_category($db);
add_class("cls_tb_banner");
$cls_tb_banner = new cls_tb_banner($db);
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);
add_class("cls_settings");
$cls_settings = new cls_settings($db);
$v_cat_slugger = isset($_GET['_url']) ? $_GET['_url'] : '';
//$pathCache = open_cache(PRODUCT_IMAGE_DIR.'cache-php/','home-'.$v_cat_slugger);
$v_cat_slugger = str_replace("/","",$v_cat_slugger);
$v_select_one = $cls_tb_nail_category->select_one(array("slugger"=>$v_cat_slugger,"status"=>1));
$v_category_image = $cls_tb_nail_category->get_image_link();
$v_current_id = $cls_tb_nail_category->get_mongo_id();
$tpl_content = new Template('dsp_cg_search.tpl', $v_dir_templates);
$tpl_cg_search_content = new Template('dsp_cg_search_content.tpl', $v_dir_templates);
$arr_filter_item = array();

$v_url_page_size = $v_url_page_sort = URL.$v_cat_slugger."/";
$v_url_page_size = isset($_GET['page']) ? URL.$v_cat_slugger."/?page=".$_GET['page'] : URL.$v_cat_slugger."/";
//$v_page_link = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
//$v_page_link = isset($_GET['num_row']) ?URL.$v_cat_slugger."/&num_row=".$_GET['num_row'] : $v_url_page_sort;
//$v_page_link = isset($_GET['sort']) ?$v_page_link."&sort=".$_GET['sort'] : $v_page_link;
//$v_page_link_cat = isset($_GET['page']) ? $v_page_link."&page=".$_GET['page'] : $v_page_link;
//$v_url_page_sort = isset($_GET['num_row']) ?$v_url_page_sort."&num_row=".$_GET['num_row'] : $v_url_page_sort;
//$v_url_page_size = isset($_GET['sort']) ?$v_url_page_size."&sort=".$_GET['sort'] : $v_url_page_size;

$v_page_link = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
$v_page_link = isset($_GET['num_row']) ?URL.$v_cat_slugger."/&num_row=".$_GET['num_row'] : $v_url_page_sort;
$v_page_link = isset($_GET['sort']) ?$v_page_link."&sort=".$_GET['sort'] : $v_page_link;
$v_page_link_cat = isset($_GET['page']) ? $v_page_link."&page=".$_GET['page'] : $v_page_link;
$v_url_page_sort = isset($_GET['num_row']) ?$v_url_page_sort."&num_row=".$_GET['num_row'] : $v_url_page_sort;
$v_url_page_sort = isset($_GET['page']) ? $v_url_page_sort."?page=".$_GET['page'] : $v_url_page_sort;
$v_url_page_size = isset($_GET['sort']) ?$v_url_page_size."&sort=".$_GET['sort'] : $v_url_page_size;

$arr_sort = array();//array('name'=>1);
$v_sort_type = '';
if(isset($_GET['sort'])){
    $v_sort_type = $_GET['sort'];
    if($v_sort_type=='new') $arr_sort['_id'] = -1;
    else if($v_sort_type=='price_height_low') $arr_sort['sell_price'] = -1;
    else if($v_sort_type=='price_low_height') $arr_sort['sell_price'] = 1;
}
$v_item_drop = 4;
$v_item_start = 1;
$v_page = isset($_GET['page'])?$_GET['page']:1;
settype($v_page,"int");
$v_page = ($v_page<=0)?1:$v_page;
$v_num_row = isset($_GET['num_row'])?$_GET['num_row']:20;
$v_number_of_record = $cls_settings->draw_option_name_by_key("number_of_record",0,$v_num_row);
settype($v_num_row,"int");
$v_num_row = ($v_num_row<=0)?100:$v_num_row;
$v_product_sort_option = $cls_settings->draw_option_name_by_key("product_sort_option",0,$v_sort_type);

$arr_where_clause["deleted"] = false;
$arr_where_clause["category_id"] = (string)$v_current_id;
$v_total_row = $cls_tb_product->count($arr_where_clause);
$v_total_page = ceil($v_total_row /$v_num_row);
if($v_total_page <= 0) $v_total_page = 1;
if($v_total_page<$v_page) $v_page = $v_total_page;
$v_offset = ($v_page - 1)*$v_num_row;
$v_pagination = '';
$page_link = isset($_SERVER['REDIRECT_URL'])?$_SERVER['REDIRECT_URL']:'';
if($v_total_page > 1)
	for($i=1;$i<=$v_total_page;$i++){
		if($v_page == $i) $v_pagination .= '<li class="selected OneLinkNoTx">'.$i.'</li>';
		else $v_pagination .= '<li class="OneLinkNoTx"> <a class="click_a" href="'.$page_link.'?page='.$i.'">'.$i.'</a> </li>';
	}
$v_sort_footer = new Template('dsp_product_item_sort.tpl', $v_dir_templates);
$v_sort_footer->set('URL_PAGESIZE',$v_url_page_size);
$v_sort_footer->set('URL_SORT',$v_url_page_sort);
$v_sort_footer->set('PRODUCT_SORT_OPTION',$v_product_sort_option);
$v_sort_footer->set('INDEX',0);

$arr_select = $cls_tb_product->select_limit($v_offset,$v_num_row,$arr_where_clause,$arr_sort);
$v_product_pagination = pagination2($v_total_page, $v_page, $v_page_link.'?page=');
$v_sort_footer->set('PAGING',$v_product_pagination);
$arr_product_content = array();
$v_dsp_product_list = '';
$arr_brand_list = array();
foreach($arr_select as $arr){
    if(!in_array($arr['brand_id'],$arr_brand_list) && $arr['brand_id']!='' && !is_array($arr['brand_id']) ) $arr_brand_list [] = $arr['brand_id'];
    $v_image_url = URL.$arr['products_upload'];
    if(!checkRemoteFile($v_image_url)){
        $v_image_url = JT_URL.$arr['products_upload'];
    }else{
        $v_image_url_temp =  isset($arr['image_url_origin']) ? $arr['saved_dir'].PRODUCT_IMAGE_THUMB_FRONT."_".$arr['image_url_origin'] : '' ;
        if(file_exists($v_image_url_temp)){
            $v_image_url = $v_image_url_temp;
        }
    }
	if($v_dsp_product_list=='' || $v_item_start == 4)
		$v_dsp_product_list .= '<div class="product-items container">';
	$v_product_row_content_item = new Template('dsp_product_item.tpl', $v_dir_templates);
	$v_product_row_content_item->set("PRODUCT_SLUGGER",$arr['slugger']);
	$v_product_row_content_item->set("PRODUCT_IMAGE",$v_image_url);//[count($arr['products_upload']) - 1]['path']);
	$v_product_row_content_item->set("PRODUCT_PRICE",format_currency($arr['sell_price']));
	$v_product_row_content_item->set("PRODUCT_PRICE_DISCOUNT",format_currency($arr['discount']));
	$v_product_row_content_item->set("PRODUCT_NAME",$arr['name']);
	$v_product_row_content_item->set("PRODUCT_DES",$arr['description']);
	$v_product_new_html = '';
	if (!rand(0,2)){
		$v_product_new_html = '<span class="flag-new">NEW</span>';
	}
    $v_product_new_html = '';
	$v_product_row_content_item->set("PRODUCT_NEW",$v_product_new_html);
	$v_product_note_1 = isset($arr['exclusive']) && $arr['exclusive'] == 1 ? 'exclusive' : '';
	$v_product_note_2 = isset($arr['limited_edition']) && $arr['limited_edition'] == 1 ? 'limited edition' : (isset($arr['online_only']) && $arr['online_only'] == 1 ? 'online only' : '');
	$v_product_note_html = '';
	if ($v_product_note_1 || $v_product_note_2){
		$v_product_note_html = '<span class="flags-market">';
		if ($v_product_note_1)
			$v_product_note_html .= '<span class="flag flag-ex">'.$v_product_note_1.'</span>';
		if ($v_product_note_2 && $v_product_note_2 != $v_product_note_1)
			$v_product_note_html .= ' <span class="flag flag-ex">'.$v_product_note_2.'</span>';
		$v_product_note_html .= '</span>';
	}
	$v_product_row_content_item->set("PRODUCT_NOTE",$v_product_note_html);
	// more color
	$v_product_more_color = rand(0, 4);
	$v_product_more_color_html = '';
	if ($v_product_more_color)
		$v_product_more_color_html = '<span class="more-colors">[ '.$v_product_more_color.' more colors ]</span>';
    $v_product_more_color_html = '';
	$v_product_row_content_item->set("PRODUCT_MORE_COLOR",$v_product_more_color_html);

	$v_dsp_product_list .=$v_product_row_content_item->output();
	if($v_item_start == 4 || $v_dsp_product_list =='') {
		$v_dsp_product_list .= '</div>';
		$v_item_start = 1;
	}
	else $v_item_start ++;
	if(!in_array($arr['brand_id'],$arr_brand_list) && $arr['brand_id'] !='') $arr_brand_list [] = $arr['brand_id'];
}
$v_category_image = $v_category_image=='' ?'user_account/customer/templates/default/img/091213_skincare_banner_face_cleanser.jpg':URL."resources/category/".(string)$cls_tb_nail_category->get_mongo_id()."/".$v_category_image;
$v_category_image = '<a href="javascript:void(0)" class="imageComponent  OneLinkHide" data-lazyload="true" target="" title="Face Wash &amp; Cleansers">
        <img src="'.$v_category_image.'" width="777" height="224" alt="Face Wash &amp; Cleansers">
    </a>';
if(!$v_select_one) redir("/");
$v_category_ul = '';
$arr_cat_language = $cls_tb_nail_category->get_language_val();
$v_name_cat = isset($arr_cat_language[$v_language]) ? $arr_cat_language[$v_language]  :$cls_tb_nail_category->get_name();
$v_category_ul = '<li class="current"><a style="color:red" href="'.$cls_tb_nail_category->get_slugger(). '/">'.ucwords($v_name_cat).'</a></li>';

$arr_category_sidebar = array();
$dsp_category_sidebar = '';
$arr_category_right_item = array();
$arr_category_right_dsp = array();
$arr_right_temp = array();
$dsp_category_right_item = '';
$dsp_filter = '';
if($cls_tb_nail_category->get_parent_id()==0){ // parent
	$arr_child_item = $cls_tb_nail_category->get_child_item();
    $arr_search_for_child = $cls_tb_nail_category->select(array("parent_id"=>(string)$cls_tb_nail_category->get_mongo_id()));
    foreach($arr_search_for_child as $arr_search){
        if(!in_array((string)$arr_search['_id'],$arr_child_item))
        $arr_child_item [] = (string)$arr_search['_id'];
    }
	for($i=0;$i<count($arr_child_item);$i++){
		$cls_category_temp = new cls_tb_nail_category($db);
		$arr_select_field = $cls_tb_nail_category->select_limit_fields(0,1,array("name","name_language","slugger","child_item"),array("_id"=> new MongoId($arr_child_item[$i])) );
		$arr_select_field = iterator_to_array($arr_select_field);
		if(isset($arr_select_field[$arr_child_item[$i]])){
			$tpl_search_sidebar_category = new Template('dsp_search_sidebar_category.tpl', $v_dir_templates);
			$arr_name_temp = isset($arr_select_field[$arr_child_item[$i]]['name_language']) ? $arr_select_field[$arr_child_item[$i]]['name_language'] : array();
			$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] : $arr_select_field[$arr_child_item[$i]]['name'];
			if(!count($arr_select_field[$arr_child_item[$i]]['child_item'])){
				$v_headline_name = '<a class="nav-title" data-name="'.$v_name_temp.'" href="'.$arr_select_field[$arr_child_item[$i]]['slugger'].'/">
										'.ucwords($v_name_temp).'
										<span class="arrow arrow-right"></span>
								</a>';
			}else{
				$v_headline_name = '<div class="nav-title non-click" data-name="'.$v_name_temp.'/">'.ucwords($v_name_temp).'</div>';
			}
			$tpl_search_sidebar_category->set('HEADLINE_NAME',ucwords($v_headline_name));
			$arr_child_item_temp = array();
            $arr_child_item_temp_headline = array();
			$dsp_child_item = '';
            $arr_search_for_child = $cls_tb_nail_category->select(array("parent_id"=>(string)$arr_child_item[$i],"status"=>1));
            foreach($arr_search_for_child as $arr_search){
                if(!in_array((string)$arr_search['_id'],$arr_child_item_temp_headline))
                    $arr_child_item_temp_headline [] = (string)$arr_search['_id'];
            }
			for($j=0;$j<count($arr_child_item_temp_headline);$j++){
				$v_id_tem = $arr_child_item_temp_headline[$j];
                $check = $cls_tb_nail_category->select_one(array("_id"=> new MongoId($v_id_tem),"status"=>1));
                if(!$check) continue;
				$arr_select_field_child = $cls_tb_nail_category->select_limit_fields(0,1,array("name","name_language","slugger","child_item"),array("_id"=> new MongoId($v_id_tem)));
				$arr_select_field_child = iterator_to_array($arr_select_field_child);
				$tpl_search_sidebar_category_item = new Template('dsp_search_sidebar_category_item.tpl', $v_dir_templates);

				$arr_name_temp = isset($arr_select_field_child[$v_id_tem]['name_language']) ? $arr_select_field_child[$v_id_tem]['name_language'] : array();
				$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] : (isset($arr_select_field_child[$v_id_tem]['name'])?$arr_select_field_child[$v_id_tem]['name'] :'');

				if(!count($arr_select_field_child[$v_id_tem]['child_item'])){
					$v_child_name_link = '<a href="/'.$arr_select_field_child[$v_id_tem]['slugger'].'/" data-name="'.$v_name_temp.'">'.ucwords($v_name_temp).'</a>';
				}else{
					$v_child_name_link = '<div class="">'.ucwords($v_name_temp).' <span> ('.$v_total_row.')</span></div>';
				}
				$tpl_search_sidebar_category_item->set('CHILD_NAME',$v_child_name_link);
				$arr_child_item_temp [] = $tpl_search_sidebar_category_item;
			}
			$dsp_child_item = count($arr_child_item_temp) > 0 ? Template::merge($arr_child_item_temp) : '';
			$tpl_search_sidebar_category->set('CATEGORY_CHILD',$dsp_child_item);
			// FILTER THEO NH?NG ?I?U KI?N KH?C NHAU
			$tpl_search_sidebar_category->set('CATEGORY_FILTER',$dsp_child_item);
			$arr_category_sidebar[] = $tpl_search_sidebar_category;
		}
	}
}else {
	$v_cat_parent_id = $cls_tb_nail_category->get_parent_id();
	if($cls_tb_nail_category->get_is_headline()){
		$cls_cat_parent = new cls_tb_nail_category($db);
		$v_cat_parent_id = $cls_tb_nail_category->get_parent_id();
		$v_select_one = $cls_cat_parent->select_one(array("_id"=> new MongoId($v_cat_parent_id)));
		$arr_child_item_headline = $cls_cat_parent->get_child_item();

        $arr_search_for_child = $cls_tb_nail_category->select(array("status"=>1,"parent_id"=>(string)$cls_cat_parent->get_mongo_id()));
        foreach($arr_search_for_child as $arr_search){
            if(!in_array((string)$arr_search['_id'],$arr_child_item_headline))
                $arr_child_item_headline [] = (string)$arr_search['_id'];
        }

		$arr_name_temp = $cls_cat_parent->get_language_val();
		$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] : $cls_cat_parent->get_name();
		if($v_select_one){
			$arr_cat_language = $cls_cat_parent->get_language_val();
			$v_name_cat = isset($arr_cat_language[$v_language]) ? $arr_cat_language[$v_language]  :$cls_cat_parent->get_name();
			$v_category_ul = '<li><a href="'.$cls_cat_parent->get_slugger().'/">'.ucwords($v_name_cat).'</a></li>'.$v_category_ul;
		}
		for($i=0;$i<count($arr_child_item_headline);$i++){
			$cls_category_temp = new cls_tb_nail_category($db);
			$arr_select_field = $cls_tb_nail_category->select_limit_fields(0,1,array("name","name_language","slugger","child_item"),array("_id"=> new MongoId($arr_child_item_headline[$i]) ,"side"=>0) );
			$arr_select_field = iterator_to_array($arr_select_field);

            $arr_child_item_headline_temp = array();
            $arr_search_for_child = $cls_tb_nail_category->select(array("status"=>1,"parent_id"=>$arr_child_item_headline[$i]));
            foreach($arr_search_for_child as $arr_search){
                if(!in_array((string)$arr_search['_id'],$arr_child_item_headline_temp))
                    $arr_child_item_headline_temp [] = (string)$arr_search['_id'];
            }

			if(isset($arr_select_field[$arr_child_item_headline[$i]])){
				$tpl_search_sidebar_category = new Template('dsp_search_sidebar_category.tpl', $v_dir_templates);
				$arr_name_temp = isset($arr_select_field[$arr_child_item_headline[$i]]['name_language'])?$arr_select_field[$arr_child_item_headline[$i]]['name_language'] : array();
				$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] : $arr_select_field[$arr_child_item_headline[$i]]['name'];

				if(!count($arr_select_field[$arr_child_item_headline[$i]]['child_item'])){
					$v_headline_name = '<a class="nav-title" data-name="'.$v_name_temp.'" href="'.$arr_select_field[$arr_child_item_headline[$i]]['slugger'].'/">
										'.ucwords($v_name_temp).'
										<span class="arrow arrow-right"></span>
								</a>';
				}else{
					$v_headline_name = '<div class="nav-title non-click" data-name="'.$v_name_temp.'/">'.ucwords($v_name_temp).'</div>';
				}
				if($v_current_id == $arr_child_item_headline[$i] ) {
					$v_headline_name = ' <li class="selected-cat second-level-cat"><div class=" nav-title">'.ucwords($v_name_temp).' <span> ('.$cls_tb_product->count(array("category_id"=>(string)$v_current_id)).')</span> </div> </li>';
				}
				$tpl_search_sidebar_category->set('HEADLINE_NAME',$v_headline_name);
				$arr_child_item_temp = array();
				$dsp_child_item = '';
//				for($j=0;$j<count($arr_select_field[$arr_child_item_headline[$i]]['child_item']);$j++){
				for($j=0;$j<count($arr_child_item_headline_temp);$j++){
					$v_id_tem = $arr_child_item_headline_temp[$j];
                    $check = $cls_tb_nail_category->select_one(array("_id"=> new MongoId($v_id_tem),"status"=>1));
                    if(!$check) continue;
					$arr_select_field_child = $cls_tb_nail_category->select_limit_fields(0,1,array("name","name_language","slugger","child_item"),array("_id"=> new MongoId($v_id_tem)));
					$arr_select_field_child = iterator_to_array($arr_select_field_child);
					$arr_name_temp = isset($arr_select_field_child[$v_id_tem]['name_language']) ? $arr_select_field_child[$v_id_tem]['name_language'] : array();
					$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] :  $arr_select_field_child[$v_id_tem]['name'];
					$tpl_search_sidebar_category_item = new Template('dsp_search_sidebar_category_item.tpl', $v_dir_templates);
					if(!count($arr_select_field_child[$v_id_tem]['child_item'])){
						$v_child_name_link = '<a href="/'.$arr_select_field_child[$v_id_tem]['slugger'].'/" data-name="'.$v_name_temp.'/">'.ucwords($v_name_temp).'</a>';
					}else{
						$v_child_name_link = '<div class="">'.ucwords($v_name_temp).' <span> ('.$v_total_row.')</span></div>';
					}
					$tpl_search_sidebar_category_item->set('CHILD_NAME',$v_child_name_link);
					$arr_child_item_temp [] = $tpl_search_sidebar_category_item;
				}
				$dsp_child_item = count($arr_child_item_temp) > 0 ? Template::merge($arr_child_item_temp) : '';
				$tpl_search_sidebar_category->set('CATEGORY_CHILD',$dsp_child_item);
				$arr_category_sidebar[] = $tpl_search_sidebar_category;
			}
		}
		// headline
	}else{
        $check = $cls_tb_nail_category->select_one(array("_id"=>new MongoId($v_cat_parent_id),"status"=>1));
		$arr_headline_category = $cls_tb_nail_category->select_limit_fields(0,1,array("_id","parent_id","name","name_language","slugger"),array("_id"=>new MongoId($v_cat_parent_id)));
		$arr_headline_category = iterator_to_array($arr_headline_category);
		$arr_name_temp = isset($arr_headline_category[$v_cat_parent_id]['name_language']) ? $arr_headline_category[$v_cat_parent_id]['name_language'] : array();
		$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] : (isset($arr_headline_category[$v_cat_parent_id]['name'])?$arr_headline_category[$v_cat_parent_id]['name'] :'');
		$v_cat_parent_id_temp = isset($arr_headline_category[$v_cat_parent_id]["parent_id"])?$arr_headline_category[$v_cat_parent_id]["parent_id"]:0;
		$v_count = $cls_tb_product->count(array("category_id"=>$v_cat_parent_id));
		if($v_count) $v_category_ul = '<li ><a href="'.$arr_headline_category[$v_cat_parent_id]['slugger'].'/">'.ucwords($v_name_temp).'</a></li>'.$v_category_ul;
		else{
//            $v_category_ul = '<li class="non-clickable">'.ucwords($v_name_temp).'</li>'.$v_category_ul;
            $v_category_ul = '<li ><a href="'.(isset($arr_headline_category[$v_cat_parent_id]['slugger'])?$arr_headline_category[$v_cat_parent_id]['slugger']:'').'/">'.ucwords($v_name_temp).'</a></li>'.$v_category_ul;
        }
		if($v_cat_parent_id_temp!=''){
            $arr_parent_category = $cls_tb_nail_category->select_limit_fields(0,1,array("_id","parent_id","name","name_language","slugger"),array("_id"=>new MongoId($v_cat_parent_id_temp)));
            $arr_parent_category = iterator_to_array($arr_parent_category);
        }
        else $arr_parent_category = array();
		$v_count = $cls_tb_product->count(array("category_id"=>$v_cat_parent_id_temp));
		$arr_name_temp = isset($arr_parent_category[$v_cat_parent_id_temp]['name_language'])?$arr_parent_category[$v_cat_parent_id_temp]['name_language']:0;
		$v_name_temp = isset($arr_name_temp[$v_language]) ? $arr_name_temp[$v_language] : (isset($arr_parent_category[$v_cat_parent_id_temp]['name'])?$arr_parent_category[$v_cat_parent_id_temp]['name']:'');
		$v_category_ul = '<li><a href="'.(isset($arr_parent_category[$v_cat_parent_id_temp]['slugger'])?$arr_parent_category[$v_cat_parent_id_temp]['slugger']:'').'/">'.ucwords($v_name_temp).'</a></li>'.$v_category_ul;
		$cls_cat_parent = new cls_tb_nail_category($db);
		$v_select_one = $cls_cat_parent->select_one(array("_id"=> new MongoId($v_cat_parent_id)));
		$arr_child_item_headline = $cls_cat_parent->get_child_item();
        $arr_child_item_headline = array();
        $arr_search_for_child = $cls_tb_nail_category->select(array("status"=>1,"parent_id"=>(string)$cls_cat_parent->get_mongo_id()));
        foreach($arr_search_for_child as $arr_search){
            if(!in_array((string)$arr_search['_id'],$arr_child_item_headline))
                $arr_child_item_headline [] = (string)$arr_search['_id'];
        }
		$arr_name_language = $cls_cat_parent->get_language_val();
		$v_name_headline = isset($arr_name_language[$v_language]) ? $arr_name_language[$v_language] : $cls_cat_parent->get_name();
		$v_name_headline = '<div class="nav-title non-click" data-name="'.$v_name_headline.'/">'.ucwords($v_name_headline).'</div>';
		$tpl_search_sidebar_category = new Template("dsp_search_sidebar_category.tpl",$v_dir_templates);
		$tpl_search_sidebar_category->set('HEADLINE_NAME',$v_name_headline);
		$arr_child_item = array();
		for($i=0;$i<count($arr_child_item_headline);$i++){
			$tpl_search_sidebar_category_item = new Template("dsp_search_sidebar_category_item.tpl",$v_dir_templates);
			$v_selected = '';
			$v_count = $cls_tb_product->count(array("category_id"=>$arr_child_item_headline[$i]));
			$cls_temp = new cls_tb_nail_category($db);
			$cls_temp->select_one(array("_id"=>new MongoId($arr_child_item_headline[$i])));
			$arr_name_language = $cls_temp->get_language_val();
			$v_name_temp = isset($arr_name_language[$v_language]) ? $arr_name_language[$v_language] : $cls_temp->get_name();
			$v_child_name_link = '<a href="/'.$cls_temp->get_slugger().'/" data-name="'.$v_name_temp.'">'.ucwords($v_name_temp).'</a>';
			if($arr_child_item_headline[$i]!=$v_current_id)
				$v_child_name_link = '<a href="/'.$cls_temp->get_slugger().'/" data-name="'.$v_name_temp.'">'.ucwords($v_name_temp).'</a>';
			else{
				$v_child_name_link = '<div class="">'.ucwords($v_name_temp).' <span> ('.$v_total_row.')</span></div>';
			}
			if($arr_child_item_headline[$i]==$v_current_id) $v_selected = 'selected-cat';
			$tpl_search_sidebar_category_item->set('CHILD_NAME',$v_child_name_link);
			$tpl_search_sidebar_category_item->set('CLASS',$v_selected);
			$arr_child_item [] = $tpl_search_sidebar_category_item;
		}
		$dsp_child_item = Template::merge($arr_child_item);
		$tpl_search_sidebar_category->set('CATEGORY_CHILD',$dsp_child_item);
        $arr_category_sidebar[] = $tpl_search_sidebar_category;
	}
}
$dsp_category_right_item = !empty($arr_category_right_dsp) ? Template::merge($arr_category_right_dsp) :'';
$v_dsp_right_item = !empty($arr_right_temp) ? Template::merge($arr_right_temp) : '';
$tpl_search_sidebar_right_content = new Template("dsp_search_category_right_content.tpl",$v_dir_templates);
$tpl_search_sidebar_right_content->set('CATEGORY_SEARCH_RIGHT_ITEM',$arr_right_temp);
$dsp_category_sidebar = !empty($arr_category_sidebar)>0 ? Template::merge($arr_category_sidebar) : '';
$v_style = '';
//if(!count($arr_category_sidebar)) $v_style = 'display:none';
$v_total_product = 100;
$v_dsp_product_items = '';
if($v_total_row){
    $v_sort_footer->set('NUMBER_ROW_OPTION',$v_number_of_record);
    $tpl_cg_search_content->set('PRODUCT_ITEM_HEADER',$v_sort_footer->output());
    $v_sort_footer->set('INDEX',1);
    $tpl_cg_search_content->set('PRODUCT_ITEM_FOOTER',$v_sort_footer->output());
}else{
    $v_sort_footer->set('NUMBER_ROW_OPTION',$v_number_of_record);
    $tpl_cg_search_content->set('PRODUCT_ITEM_HEADER','');
    $v_sort_footer->set('INDEX',1);
    $tpl_cg_search_content->set('PRODUCT_ITEM_FOOTER','');
}
$tpl_content->set('v_category_ul',$tpl_cg_search_content->output());
$tpl_content->set('SEARCH_CONTENT',$tpl_cg_search_content->output());
$tpl_content->set('CATEGORY_IMAGE',$v_category_image);
$tpl_cg_search_sidebar = new Template('dsp_cg_search_sidebar.tpl', $v_dir_templates);
$tpl_cg_search_sidebar->set('DSP_CG_SEARCH_SIDEBAR_CATEGORY',$dsp_category_sidebar);
$tpl_cg_search_sidebar->set('CATEGORY_RIGHT_ITEM',$dsp_category_right_item);
if(count($arr_brand_list)){
	for($i=0;$i<count($arr_brand_list);$i++){
		$cls_tb_banner->select_one(array("_id"=> new MongoId($arr_brand_list[$i])));
		$tpl_cg_filter_item = new Template('dsp_cg_filter_brand_item.tpl', $v_dir_templates);
		$tpl_cg_filter_item->set('SLUGGER', $cls_tb_banner->get_banner_name());
		$tpl_cg_filter_item->set('ITEM_NAME', $cls_tb_banner->get_banner_slugger());
		$arr_filter_item [] = $tpl_cg_filter_item;
	}
}
if($v_total_row) $v_total_row .=" items";
else{
    $v_total_row = '';

    $arr_select = $cls_tb_nail_category->select(array("parent_id"=>(string)$v_current_id));
    $v_dsp_product_listcat = '';
    $arr_brand_list = array();
    $v_item_start = 1;
    foreach($arr_select as $arr){
        $v_image_url = IMAGE_URL.'icon.jpg';
        if($v_dsp_product_listcat=='' || $v_item_start == 4)
            $v_dsp_product_listcat .= '<div class="product-items container">';
        $v_product_row_content_item = new Template('dsp_cat_item.tpl', $v_dir_templates);
        $v_product_row_content_item->set("PRODUCT_SLUGGER",$arr['slugger']."/");
        $v_product_row_content_item->set("PRODUCT_IMAGE",$v_image_url);//[count($arr['products_upload']) - 1]['path']);
        $v_product_row_content_item->set("PRODUCT_PRICE",'');
        $v_product_row_content_item->set("PRODUCT_PRICE_DISCOUNT",'');
        $v_product_row_content_item->set("PRODUCT_NAME",$arr['name']);
        $v_product_row_content_item->set("PRODUCT_DES",$arr['description']);
        $v_product_row_content_item->set("PRODUCT_NEW",'');
        $v_product_row_content_item->set("PRODUCT_NOTE",'');
        $v_product_row_content_item->set("PRODUCT_MORE_COLOR",'');
        $v_dsp_product_listcat .=$v_product_row_content_item->output();
        if($v_item_start == 4 || $v_dsp_product_listcat =='') {
            $v_dsp_product_listcat .= '</div>';
            $v_item_start = 1;
        }
        else $v_item_start ++;
    }
    $v_total_row .=$v_dsp_product_listcat;
}
$dsp_filter = Template::merge($arr_filter_item);
//else $v_style = 'display:none';
$tpl_cg_search_sidebar->set('DSP_CG_SEARCH_SIDEBAR_BRAND_ITEM','');
$tpl_cg_search_sidebar->set('STYLE',$v_style);
$tpl_content->set('SEARCH_SIDEBAR',$tpl_cg_search_sidebar->output());
$tpl_content->set('PRODUCT_ITEMS',$v_dsp_product_list);
$tpl_content->set('TOTAL_ITEM',$v_total_row);
$tpl_content->set('LANGUAGE_SORT_BY',$arr_language_switch[$v_language]['SEARCH_SORT']);
$tpl_content->set('LANGUAGE_VIEW',$arr_language_switch[$v_language]['SEARCH_VIEW']);
$tpl_content->set('CATEGORY_UL',$v_category_ul);
echo $tpl_content->output();
//create_cache($pathCache);