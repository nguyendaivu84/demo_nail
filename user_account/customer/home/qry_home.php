<?php if (!isset($v_sval)) die();
$v_static_dir = RESOURCE_URL."static_content/";
add_class("cls_tb_product_group");
$cls_tb_product_group = new cls_tb_product_group($db);
add_class("cls_tb_banner");
$cls_brand = new cls_tb_banner($db);
add_class("cls_tb_nail_category");
$cls_tb_nail_category = new cls_tb_nail_category($db);
add_class("cls_tb_nail_static_content");
$cls_tb_nail_static_content = new cls_tb_nail_static_content($db);
$arr_statict_image = $cls_tb_nail_static_content->select_scalar("content_image",array("content_key"=>"home_left_adv_".$v_language));
$v_home_left_adv = '';
$v_home_footer_adv = '';
for($i=0;$i<count($arr_statict_image);$i++){
    $v_home_left_adv .= '<img src="'.$v_static_dir.$arr_statict_image[$i].'" width="210" height="235"  />';
}
$v_select_one = $cls_tb_nail_static_content->select_one(array("content_key"=>"footer_adv_".$v_language));

if($v_select_one){
    $dsp_footer_adv = new Template("dsp_footer_adv.tpl",$v_dir_templates);
    $arr_adv_item = array();
    $v_des = $cls_tb_nail_static_content->get_content_description();
    $arr_des = explode("|",$v_des);
    $dsp_footer_adv->set('CONTENT',$cls_tb_nail_static_content->get_content_content());
    $arr_statict_image = $cls_tb_nail_static_content->get_content_image();
    for($i=0;$i<count($arr_statict_image);$i++){
        $v_link_name = isset($arr_des[$i]) ? $arr_des[$i] : '';
        $dsp_footer_item_adv = new Template("dsp_footer_adv_item.tpl",$v_dir_templates);
        $v_space = '<div class="section">
                        <div class="html-component">
                            <div style="width:48px;height:308px;background-color:#ffffff;">&nbsp;</div>
                        </div>
                    </div>';
        if($i==count($arr_statict_image) -1) $v_space = '';
        $dsp_footer_item_adv->set('ITEM_IMAGE',$v_static_dir.$arr_statict_image[$i]);
        $dsp_footer_item_adv->set('NAME',$v_link_name);
        $dsp_footer_item_adv->set('SPACE',$v_space);
        $arr_adv_item [] = $dsp_footer_item_adv;
    }
    $v_adv_item = is_array($arr_adv_item) && count($arr_adv_item)>0 ? Template::merge($arr_adv_item) : '';
    $dsp_footer_adv->set('ADV_ITEM',$v_adv_item);
    $v_home_footer_adv = $dsp_footer_adv->output();
}
$arr_statict_slider_image = $cls_tb_nail_static_content->select_scalar("content_image",array("content_key"=>"image_slider_".$v_language));
add_class("cls_tb_nail_category");
$tpl_content = new Template('dsp_home.tpl',$v_dir_templates);
$tpl_slide = '';
for ($i=0; $i < count($arr_statict_slider_image); $i++) {
    $tpl_slide .= '<li style="width:990px" data-slide-index="'.($i+1).'">';
    $tpl_slide .= '<a href="/br-new-scents" class="imageComponent" title="Elizabeth and James Nirvana The first">';
    $tpl_slide .= '<img src="'.$v_static_dir.$arr_statict_slider_image[$i].'" width="990" height="520" alt="Elizabeth and James Nirvana The first fragrance" />';
    $tpl_slide .= '</a>';
    $tpl_slide .= '</li>';
}
$tpl_content->set('SILDER_ITEM',$tpl_slide);
$arr_qry_all_product_group = $cls_tb_product_group->select(array("status"=>1),array("order_no"=>1));
$arr_home_product_group_slider = array();
$arr_existed_category = array();
foreach($arr_qry_all_product_group as $arr_temp){
    $dsp_product_group_slider = new Template("dsp_product_group_slider.tpl",$v_dir_templates);
    $dsp_product_group_slider->set('GROUP_NAME',$arr_temp['group_name']);
    $arr_product_group_child_item = $arr_temp['child_item'];
    $v_cat_id = isset($arr_temp['category_id']) ? $arr_temp['category_id'] : '';
  $v_cat_slugger = '';
    $tpl_slide = '';
    $v_k=1;
    for($i=0;$i<count($arr_product_group_child_item);$i++){
        $v_result = $cls_tb_product->select_one(array('_id'=> new MongoId($arr_product_group_child_item[$i])));
        $v_image_temp = $cls_tb_product->get_products_upload();
        if(!file_exists($v_image_temp)) $v_image_temp = JT_URL. $cls_tb_product->get_products_upload();
        if(!$v_result) continue;
        $v_brand_name = '';
        $tpl_slide .= '<li data-slide-index="'.$v_k++.'">';
        $tpl_slide .= '<div class="product-item">';
        $tpl_slide .= '<div class="product-image">';
        $tpl_slide .= '<a href="/'.$cls_tb_product->get_slugger().'">';
        $tpl_slide .= "<img src='".$v_image_temp."' width='135' height='135' />";
        $tpl_slide .= '</a>';
        $tpl_slide .= '</div>';
        $tpl_slide .= '<div class="new-love">';
        $tpl_slide .= '<span class="flag-new">NEW</span>';
        $tpl_slide .= '</div>';
        $tpl_slide .= '<a href="/'.$cls_tb_product->get_slugger().'">';
        $tpl_slide .= '<span class="product-info">';
        $tpl_slide .= '<span class="name OneLinkNoTx" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;"><span class="brand">'.$v_brand_name.'</span>'.$cls_tb_product->get_name().'</span>';
        $tpl_slide .= '<span class="product-price">';
        $tpl_slide .= '<span class="list-price"><span class="currency"></span><span class="price">'.format_currency($cls_tb_product->get_sell_price()).'</span></span>';
        $tpl_slide .= '<span class="value-price"> (<span class="currency"></span><span class="price">'.format_currency($cls_tb_product->get_product_sell_discount()).'</span><span style=" font-family: fontsDosis; font-weight: 400; font-style: normal;"> value</span>)</span>';
        $tpl_slide .= '</span>';
        $tpl_slide .= '</a>';
        $tpl_slide .= '</div>';
        $tpl_slide .= '</li>';
    }
    $dsp_product_group_slider->set('SLIDER_ITEM_CONTENT',$tpl_slide);
    $dsp_product_group_slider->set('CAT_SLUGGER',$v_cat_slugger);
    $arr_home_product_group_slider[] = $dsp_product_group_slider;
}
$cls_tb_nail_category = new cls_tb_nail_category($db);
$arr_quick_link = $cls_tb_nail_category->select(array("is_group"=>1,"status"=>1));
$v_dsp_quick_link = '';
$arr_dsp_quick = array();
foreach($arr_quick_link as $arr){
    $dsp_quick_link = new Template("dsp_quick_link.tpl",$v_dir_templates);
    $arr_child_item = $arr['child_item'];
    $arr_child_item_search = $cls_tb_nail_category->select(array("status"=>1,"parent_id"=>(string)$arr['_id']));
    $dsp_quick_link->set('CATEGORY_NAME',$arr['name']);
    $v_dsp_child_item = '';
    $arr_child_item_temp = array();
    foreach($arr_child_item_search as $arr_tt_temp){
        $dsp_quick_link_item = new Template("dsp_quick_link_item.tpl",$v_dir_templates);
        $dsp_quick_link_item->set('NAME',$arr_tt_temp['name']);
        $dsp_quick_link_item->set('SLUGGER',$arr_tt_temp['slugger']);
        $arr_child_item_temp [] = $dsp_quick_link_item;
    }
    $v_dsp_child_item = is_array($arr_child_item_temp) ? Template::merge($arr_child_item_temp) : '';
    $dsp_quick_link->set('QUICK_LINK_ITEM',$v_dsp_child_item);
    $arr_dsp_quick[] = $dsp_quick_link;
}
$v_dsp_quick_link = is_array($arr_dsp_quick) ? Template::merge($arr_dsp_quick) : '';
$tpl_content->set('QUICK_LINK',$v_dsp_quick_link);
$tpl_content->set('HOME_LEFT_ADV',$v_home_left_adv);
$tpl_content->set('FOOTER_ADV',$v_home_footer_adv);
$v_dsp_all_product_group_slider = count($arr_home_product_group_slider)>0 ? Template::merge($arr_home_product_group_slider) : '';
$tpl_content->set('PRODUCT_GROUP_SLIDER',$v_dsp_all_product_group_slider);
echo $tpl_content->output();

