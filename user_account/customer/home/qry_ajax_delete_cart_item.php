<?php if(!isset($v_sval)) die;
    $arr_return = array('error'=>1, 'message'=>'Invalid data','count'=>0,'product_type'=>0,'total_order'=>0);
    $v_product_id = isset($_POST['txt_product_id']) ? $_POST['txt_product_id'] : '';
    $v_product_id = trim($v_product_id);
    $v_product_id = strip_tags($v_product_id);
    $v_product_id = addslashes($v_product_id);
    if($v_product_id!=''){
        $v_select_one = $cls_tb_product->select_one(array("_id"=> new MongoId($v_product_id)));
        if($v_select_one){
            $v_total_order = 0;
            $v_product_price = 0;
            $arr_return = array('error'=>0, 'message'=>'','count'=>0,'product_type'=>0);
            $v_user_id = '';
            if(isset($_SESSION['ss_customer'])){
                $arr_user = unserialize($_SESSION['ss_customer']);
                $v_user_id = (string)$arr_user['_id'];
            }
            $arr_ss_basket = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
            $arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
            delete_basket($db,$v_user_id,$arr_ss_basket,$arr_ss_basket_content,$v_product_id,$v_total_order,$v_product_price);
            $arr_return['product_type'] = $cls_tb_product->get_is_free_sample();
            $arr_return['count'] = count($arr_ss_basket);
            $_SESSION['ss_basket_item'] = $arr_ss_basket;
            $_SESSION['ss_basket_content'] = serialize($arr_ss_basket_content);
            if($cls_tb_product->get_is_free_sample()==1) $arr_return['total_order'] = format_currency($v_total_order);
            else{
//                $v_money = $v_total_order - $v_product_price;
                $arr_return['total_order'] = $v_total_order >0 ? format_currency($v_total_order) : format_currency(0);
            }
            if(count($arr_ss_basket)<=0){
                unset($_SESSION['ss_basket_content']);
                unset($_SESSION['ss_basket_item']);
            }
        }
    }
    header("Content-type: application/json");
    echo json_encode($arr_return);
?>