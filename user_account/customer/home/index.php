<?php if(!isset($v_sval)) die();
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);
add_class("cls_tb_nail_cart");
$cls_tb_cart = new cls_tb_nail_cart($db);
if(isset($_POST['ajax'])){
	if($_POST['ajax'] == 'cg_search_get_product')
		include $v_head.'home/qry_cg_search_ajax_product.php';
    else if($_POST['ajax'] == 'cg_search_get_product')
        include $v_head.'home/qry_cg_search_ajax_product.php';
    else if($_POST['ajax'] == 'add_to_cart')
        include 'qry_ajax_add_to_cart.php';
    else if($_POST['ajax'] == 'delete_cart_item')
        include 'qry_ajax_delete_cart_item.php';
    else if($_POST['ajax'] == 'change_language')
        include 'qry_ajax_changelanguage.php';
}else{
//    $temp = isset($_GET['_url']) ? $_GET['_url'] : 'home';
//    if($temp!='home') $temp = 'home-'.$temp;
//    $pathCache = open_cache(PRODUCT_IMAGE_DIR.'cache-php/',$temp);
	include $v_head.'header.php';
	if(isset($_GET['_url'])){
		if( substr($_GET['_url'], -1, 1) != '/' ){
            if(1==1)
                include $v_head.'home/qry_product.php';
            else
                include $v_head.'home/qry_post.php';
        }
		else
			include $v_head.'home/qry_cg_search.php';
	}else
		include $v_head.'home/qry_home.php';
	include $v_head.'footer.php';
//    create_cache($pathCache);
}
