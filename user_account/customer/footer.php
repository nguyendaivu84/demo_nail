<?php if(!isset($v_sval)) die();
//$pathCache = open_cache(PRODUCT_IMAGE_DIR.'cache-php/','home-footer');
// =============================== XU LY LAY CAC TRANG STATIC
add_class("cls_tb_nail_static_content");
add_class("cls_settings");
$cls_settings = new cls_settings($db);
$arr_option = $cls_settings->draw_option_by_key('language_setting',0,$_SESSION['ss_language']);
$cls_tb_nail_static_content = new cls_tb_nail_static_content($db);
$v_footer = $cls_tb_nail_static_content->select_scalar("content_content",array("content_key"=>"footer_".$v_language));
// =============================== END
$arr_ss_language = array();
if(isset($_SESSION['ss_language_value'])) $arr_ss_language = unserialize($_SESSION['ss_language_value']);
$v_footer_sign_up = isset($arr_ss_language[$v_language]['SIGN_UP_TEXT']) ? $arr_ss_language[$v_language]['SIGN_UP_TEXT'] : '';
$v_footer_sign_up_placeholder = isset($arr_ss_language[$v_language]['SIGN_UP_TEXT_INPUT_PLACEHOLDER']) ? $arr_ss_language[$v_language]['SIGN_UP_TEXT_INPUT_PLACEHOLDER'] : '';
$v_footer_store_event = isset($arr_ss_language[$v_language]['STORE_LOCATION_AND_EVENT']) ? $arr_ss_language[$v_language]['STORE_LOCATION_AND_EVENT'] : '';
$v_footer_store_event_holder = isset($arr_ss_language[$v_language]['STORE_LOCATION_AND_EVENT_INPUT_PLACEHOLDER']) ? $arr_ss_language[$v_language]['STORE_LOCATION_AND_EVENT_INPUT_PLACEHOLDER'] : '';
$tpl_footer = new Template('footer.tpl',$v_dir_templates);
$tpl_footer->set('URL_TEMPLATE',$v_templates);
$tpl_footer->set('URL',URL);
$tpl_footer->set('STATIC_FOOTER',$v_footer);
$tpl_footer->set('LG_FOOTER_SIGN_UP',$v_footer_sign_up);
$tpl_footer->set('LG_FOOTER_SIGN_UP_TEXT',$v_footer_sign_up_placeholder);
$tpl_footer->set('LG_FOOTER_STORE_EVENT',$v_footer_store_event);
$tpl_footer->set('LG_FOOTER_STORE_EVENT_TEXT',$v_footer_store_event_holder);
$tpl_footer->set('OPTION_LANGUAGE',$arr_option);
$tpl_footer->set('URL',URL);
echo $tpl_footer->output();
//create_cache($pathCache);