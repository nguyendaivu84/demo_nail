<?php if(!isset($v_sval)) die;
if(isset($_POST['txt_email'])){
    add_class('PHPMailer','class.phpmailer.php');
    $cls_mail = new PHPMailer (true);
    $v_main_site_title = $cls_settings->get_option_name_by_key('website_attribute', 'website_main_title','AnvyDigital WorkTraq Website');
    $v_support_email = $cls_settings->get_option_name_by_key('email','support_email', 'info@anvydigital.com');
    $v_mail_dir_templates = 'mail/';
    if(is_valid_email($_POST['txt_email'])){
        $v_mail_subject = 'Reset password';
        $v_random = create_random_password(7);
        $v_email = $_POST['txt_email'];
        $arr_user_order = array('name'=>'', 'contact'=>0, 'full'=>'', 'email'=>$v_email);
        $tpl_mail = new Template('tpl_send_new_passoword.tpl', $v_mail_dir_templates);
        $tpl_mail->set('USER_NAME', $v_email);
        $tpl_mail->set('NEW_PASSWORD', $v_random);
        $v_mail_body = $tpl_mail->output();
        send_email_el($cls_mail,$v_main_site_title, $v_support_email, $arr_user_order,$v_mail_subject, $v_mail_body);
    }
}