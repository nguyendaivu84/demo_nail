<?php if(!isset($v_sval)) die();
    $dsp_print_love = new Template("dsp_print_love.tpl",$v_dir_templates);
    $dsp_print_love->set('URL_CUSTOMER',$v_dir_templates);
    $v_dsp_tr_table = '';
    $arr_love_item = isset($_SESSION['ss_love_item']) ? unserialize($_SESSION['ss_love_item']) : array();
    for($i=0;$i<count($arr_love_item);$i++){
        $v_check_product = $cls_tb_product->select_one(array("_id"=>new MongoId($arr_love_item[$i]['product_id'])));
        if($v_check_product){
            $v_product_image = URL.$cls_tb_product->get_products_upload();
            if(!checkRemoteFile($v_product_image)) $v_product_image = JT_URL.$cls_tb_product->get_products_upload();
            $v_dsp_tr_table .='<tr>';
            $v_dsp_tr_table .='<td class="item-image"><div class="product-image"><img src="'.$v_product_image.'" width="62" height="62"></div></td>';
            $v_dsp_tr_table .='<td class="item-description">';
            $v_dsp_tr_table .='<p class="item-name OneLinkNoTx">';
            $v_dsp_tr_table .='<span class="brand">'.$cls_tb_banner->select_scalar("banner_name",array("_id"=> new MongoId($cls_tb_product->get_brand_id()))).'</span>';
            $v_dsp_tr_table .='<span class="product-name">'.$cls_tb_product->get_sku().'</span>';
            $v_dsp_tr_table .='<div class="info-row">';
            $v_dsp_tr_table .='<span class="sku">';
            $v_dsp_tr_table .= '<span class="label">Item #</span>';
            $v_dsp_tr_table .= '<span class="value OneLinkNoTx">'.$cls_tb_product->get_code().'</span>';
            $v_dsp_tr_table .= '</span>';
            $v_dsp_tr_table .= '</div>';
            $v_dsp_tr_table .= '<div class="info-row variation">';
            $v_dsp_tr_table .= '<span class="color">';
            $v_dsp_tr_table .= '<span class="label">Color</span>';
            $v_dsp_tr_table .= '<span class="value OneLinkNoTx">100 Finish Line</span>';
            $v_dsp_tr_table .= '</span>';
            $v_dsp_tr_table .= '</div></div></td>';
            $v_dsp_tr_table .= ' <td class="price">';
            $v_dsp_tr_table .= ' <p>C$'.format_currency($cls_tb_product->get_sell_price()).'</p></td></tr>';
        }
    }
    $dsp_print_love->set('URL',URL);
    $dsp_print_love->set('TR_TABLE',$v_dsp_tr_table);
    echo $dsp_print_love->output();
?>