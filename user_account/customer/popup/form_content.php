<?php if(!isset($v_sval)) die;
    $v_product_name = isset($_GET['txt_data']) ? $_GET['txt_data'] : '';
    $v_select_one = $cls_tb_product->select_one(array("slugger"=>$v_product_name));
    if($v_select_one){
        $v_image = $cls_tb_product->get_products_upload();
        $v_product_slide = '';
        $v_is_loved = 0;
        $v_text_love = 'add to loves';
        $v_class_love = '';
        $arr_love = isset($_SESSION['ss_love']) ? unserialize($_SESSION['ss_love']) : array();
        if(in_array((string)$cls_tb_product->get_mongo_id(),$arr_love)){
            $v_text_love = 'loved';
            $v_class_love = 'loved';
            $v_is_loved = 1;
        }
        $arr_product_slide = $cls_tb_product->get_product_slide_image();
        $i=0;
        for(;$i<count($arr_product_slide);$i++){
            if($arr_product_slide[$i]=='') continue;
            $v_image_temp = URL.$arr_product_slide[$i];
            if(!checkRemoteFile($v_image_temp)) $v_image_temp = JT_URL.$arr_product_slide[$i];
            if(!checkRemoteFile($v_image_temp)) continue;
            $v_product_slide .='<a id="a_item_'.$i.'" id="sku-563320" class="product-thumb" title="" href="#" data-original-title="">';
            $v_product_slide .='';
            $v_product_slide .='<img  onmouseover="change_image_templateql('.$i.');" onmouseout="restore_imageql('.$i.');" onclick="change_image_quick_look('.$i.');" data-link="'.$v_image_temp.'" id="slide-item-'.$i.'" width="62" height="62"  src="'.$v_image_temp.'">';
            $v_product_slide .='</a>';
        }
        if(!checkRemoteFile(URL.$v_image)) $v_image = JT_URL.$cls_tb_product->get_products_upload();
//        $i_next = $i+1;
//        $v_product_slide .='<a id="a_item_'.$i_next.'" class="product-thumb" title="" href="#" data-original-title="">';
//        $v_product_slide .='';
//        $v_product_slide .='<img  onmouseover="change_image_templateql('.$i_next.');" onmouseout="restore_imageql('.$i_next.');" onclick="change_image_quick_look('.$i_next.');" data-link="'.$v_image.'" id="slide-item-'.$i_next.'" width="62" height="62"  src="'.$v_image.'">';
//        $v_product_slide .='</a>';
        $tpl_popup_quick_look = new Template("dsp_popup_quick_look.tpl",$v_dir_templates);
        $tpl_popup_quick_look->set('URL_CUSTOMER',URL.$v_dir_templates);
        $tpl_popup_quick_look->set('PRODUCT_SLUGGER',$v_product_name);
        $tpl_popup_quick_look->set('PRODUCT_IMAGE',$v_image);
        $tpl_popup_quick_look->set('PRODUCT_ID',(string)$cls_tb_product->get_mongo_id());
        $tpl_popup_quick_look->set('PRODUCT_BANNER','');
        $tpl_popup_quick_look->set('PRODUCT_NAME',$cls_tb_product->get_sku());
        $tpl_popup_quick_look->set('PRODUCT_PRICE',format_currency($cls_tb_product->get_sell_price()));
        $tpl_popup_quick_look->set('PRODUCT_PRICE_DISCOUNT',format_currency($cls_tb_product->get_product_sell_discount()));
        $tpl_popup_quick_look->set('PRODUCT_DETAIL',$cls_tb_product->get_description());
        $tpl_popup_quick_look->set('PRODUCT_CODE',$cls_tb_product->get_code());
        $tpl_popup_quick_look->set('PRODUCT_SLIDE_IMAGE',$v_product_slide);
        $tpl_popup_quick_look->set('URL',URL);
        $tpl_popup_quick_look->set('TEXT_LOVE',$v_text_love);
        $tpl_popup_quick_look->set('CLASS_LOVE',$v_class_love);
        $tpl_popup_quick_look->set('IS_ON_LOVE',$v_is_loved);
        echo $tpl_popup_quick_look->output();
    }
?>