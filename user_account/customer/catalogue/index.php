<?php
if (!isset($v_sval)) die();
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);

if(isset($_SESSION['ss_save_temp'])){
    unset($_SESSION['ss_save_temp']);
}

add_class("cls_tb_tag");
$cls_tb_tag = new cls_tb_tag($db);

$v_tag_id_get = isset($_GET['txt_tag_id']) ? $_GET['txt_tag_id'] : 0;
$v_package_id_get = isset($_GET['txt_package_id']) ? $_GET['txt_package_id'] : 0;
settype($v_tag_id_get,"int");
settype($v_package_id_get,"int");
$v_ajax_type_ = isset($_POST['txt_ajax']) ? $_POST['txt_ajax'] : '';
switch($v_ajax_type_){
    case 'AJT_PACKAGE':
       include $v_head.'catalogue/qry_ajax_product_page.php';
        break;
    case 'AJT_TAG_PAGE':
        include $v_head.'catalogue/qry_ajax_load_tag.php';
        break;
    default:
        include $v_head.'header.php';
        include $v_head.'catalogue/qry_catalogue.php';
        //include $v_head.'catalogue/test.php';
        include $v_head.'footer.php';
}
function draw_menu($tpl_tag_item,$v_class,$arr){
    $tpl_tag_item->set("TAG_ID",$arr['tag_id']);
    $tpl_tag_item->set("URL",URL);
    $tpl_tag_item->set("TAG_NAME",$arr['tag_name']);
    $tpl_tag_item->set("FIRST_ITEM",$v_class);
    return $tpl_tag_item;
}
function draw_package($tpl_package_item,$v_class,$arr_package_item,&$tpl_pack_details,$v_dir_templates,$v_tag_id){
    if($v_class!=''){
        // draw tab of package
        $tpl_pack_tab_info = new Template("dsp_package_detail_tab.tpl",$v_dir_templates);
        $arr_all_information = draw_package_info_tab($tpl_pack_tab_info,$arr_package_item,$v_dir_templates);

        //
        //$tpl_pack_info_item->set('PACKAKE_TAB',$v_dsp_package_tab_information);
        $tpl_pack_details->set('PACKAKE_TAB',$arr_all_information['tab']);
        $tpl_pack_details->set('PACKAKE_CONTROL',$arr_all_information['content']);
    }
    $tpl_package_item->set('PACKAGE_IMAGE',URL.$arr_package_item['saved_dir'].$arr_package_item['image_file']);
    $tpl_package_item->set('PACKAGE_NAME',$arr_package_item['product_sku']);
    $tpl_package_item->set('FIST_PACKAGE',$v_class);
    $tpl_package_item->set('PACKAGE_ID',$arr_package_item['product_id']);
    $tpl_package_item->set('TAG_ID',$v_tag_id);
    $tpl_package_item->set('URL',URL);
    return $tpl_package_item;
}
function draw_package_info_tab($tpl_pack_tab_info,$arr_package_item,$v_dir_templates){
    $v_class = 'active';
    $arr_return = array();
    $arr_tab = array();
    $arr_content = array();
    $v_temp = $arr_package_item['product_overview'];
    $v_temp = trim($v_temp);
    if($v_temp!=''){
        $arr_tab[] = draw_one_tab($tpl_pack_tab_info,$v_class,"Overview","overview",$v_dir_templates);
        $arr_content[] = draw_one_content($v_class,"overview",$v_dir_templates,$arr_package_item['product_overview']);
        $v_class = '';
    }
    $v_temp = $arr_package_item['product_specs'];
    $v_temp = trim($v_temp);
    if($v_temp!=''){
        $arr_tab[] = draw_one_tab($tpl_pack_tab_info,$v_class,"Specs","specs",$v_dir_templates);
        $arr_content[] = draw_one_content($v_class,"specs",$v_dir_templates,$arr_package_item['product_specs']);
        $v_class = '';
    }
    $v_temp = $arr_package_item['product_gallery'];
    $v_temp = trim($v_temp);
    if($v_temp!=''){
        $arr_tab[] = draw_one_tab($tpl_pack_tab_info,$v_class,"Gallery","gallery",$v_dir_templates);
        $arr_content[] = draw_one_content($v_class,"gallery",$v_dir_templates,$arr_package_item['product_gallery']);
        $v_class = '';
    }
    $v_temp = $arr_package_item['product_options'];
    $v_temp = trim($v_temp);
    if($v_temp!=''){
        $arr_tab[] = draw_one_tab($tpl_pack_tab_info,$v_class,"Options","options",$v_dir_templates);
        $arr_content[] = draw_one_content($v_class,"options",$v_dir_templates,$arr_package_item['product_options']);
        $v_class = '';
    }
    $arr_return['tab'] = Template::merge($arr_tab);
    $arr_return['content'] = Template::merge($arr_content);
    return $arr_return;
}
function draw_one_tab($tpl_pack_tab_info,$p_class,$p_display_name,$p_tab_name,$v_dir_templates){
    $tpl_pack_tab_info = new Template("dsp_package_detail_tab.tpl",$v_dir_templates);
    $tpl_pack_tab_info->set('FIRST_TAB_CONTROL',$p_class);
    $tpl_pack_tab_info->set('TAB_NAME',$p_tab_name);
    $tpl_pack_tab_info->set('TAB_NAME_DISPLAY',$p_display_name);
    return $tpl_pack_tab_info;
}
function draw_one_content($p_class,$p_tab_name,$v_dir_templates,$arr_content){
    $tpl_pack_content_info = new Template("dsp_package_detail_tab_content.tpl",$v_dir_templates);
    $tpl_pack_content_info->set('TAB_NAME',$p_tab_name);
    $tpl_pack_content_info->set('FIRST_CONTENT',$p_class);
    $tpl_pack_content_info->set('PACKAGE_CONTENT',$arr_content);
    return $tpl_pack_content_info;
}
function draw_all_menu($arr_tag_where,$arr_short_tag,$v_dir_templates,$db,&$tpl_pack_details,$v_dir_templates,$v_first_item=true,&$v_package_id,$v_tag_id_get,&$v_count){
    $cls_tb_product = new cls_tb_product($db);
    $cls_tb_tag = new cls_tb_tag($db);
    //$v_first_item = true;
    $v_first_pack = true;
    if($v_package_id >0) $v_first_pack = false;
    $arr_return = array();
    $arr_dsp_package_item = array();
    $arr_tag_item = array();
    $arr_tag = $cls_tb_tag->select($arr_tag_where,$arr_short_tag);
    $v_stop_count = false;
    foreach($arr_tag as $arr){
        $tpl_tag_item = new Template('menu_item.tpl',$v_dir_templates);
        $v_class = '';
        if($v_tag_id_get == (int)$arr['tag_id'] ){
            $v_class = "current";
            $arr_where_clause['$where'] = "this.tag.indexOf('".(int)$arr['tag_id']."')>=0";
            $arr_where_clause['package_type'] = array('$gt'=>0);
            $arr_select = $cls_tb_product->select(
                array(
                    'package_type'=>array('$gt'=>0)
                    //'$or'=>array('package_type'=>array('$gt'=>0),'package_type'=>array('$lt'=>0))
                    ,'tag'=>array('$in'=>array($arr['tag_id']))
                ),
                array("product_id"=>-1)
            );
            foreach($arr_select as $arr_package_item){
                $tpl_package_item = new Template('dsp_package_item.tpl',$v_dir_templates);
                $v_class_pack = '';
               if($v_package_id == (int)$arr_package_item['product_id'] ){
                    $v_package_id = $arr_package_item['product_id'];
                    $v_first_pack = false;
                    $v_class_pack = 'active';
                }else if($v_first_pack){
                   $v_package_id = $arr_package_item['product_id'];
                   $v_first_pack = false;
                   $v_class_pack = 'active';
               }
                if($v_package_id != (int)$arr_package_item['product_id'] && !$v_stop_count ) $v_count++;
                else if($v_package_id == (int)$arr_package_item['product_id']) $v_stop_count = true;
                $arr_dsp_package_item[] = draw_package($tpl_package_item,$v_class_pack,$arr_package_item,$tpl_pack_details,$v_dir_templates,(int)$arr['tag_id']);
            }
        }
        else if($v_first_item){
            $v_class = "current";
            $v_first_item = false;

            $arr_where_clause['$where'] = "this.tag.indexOf('".(int)$arr['tag_id']."')>=0";
            $arr_where_clause['package_type'] = array('$gt'=>0);
            $arr_select = $cls_tb_product->select(array('package_type'=>array('$gt'=>0),'tag'=>array('$in'=>array($arr['tag_id']))),array("product_id"=>-1));
            foreach($arr_select as $arr_package_item){
                $tpl_package_item = new Template('dsp_package_item.tpl',$v_dir_templates);
                $v_class_pack = '';
                if($v_first_pack){
                    if($v_package_id <=0)
                    $v_package_id = $arr_package_item['product_id'];
                    $v_first_pack = false;
                    $v_class_pack = 'active';
                }
                $arr_dsp_package_item[] = draw_package($tpl_package_item,$v_class_pack,$arr_package_item,$tpl_pack_details,$v_dir_templates,(int)$arr['tag_id']);
            }
        }
        $arr_tag_item[] = draw_menu($tpl_tag_item,$v_class,$arr);


    }
    $arr_return['tag_menu'] =  Template::merge($arr_tag_item);
    $arr_return['tag_menu_item'] = Template::merge($arr_dsp_package_item);
    $arr_return['count_tag_menu_item'] = count($arr_dsp_package_item);
    return $arr_return;
}
?>