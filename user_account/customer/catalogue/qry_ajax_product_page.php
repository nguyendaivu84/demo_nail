<?php if (!isset($v_sval)) die(); ?>
<?php
    $v_product_id = isset($_POST['txt_package_id']) ? $_POST['txt_package_id'] : 0;
    settype($v_product_id,"int");
    $arr_return = array('error'=>0,'message'=>'','tab'=>array(),'content'=>array(),'image_slider'=>'');
    add_class("cls_tb_product");
    $cls_tb_product = new cls_tb_product($db);
    $arr_select = $cls_tb_product->select(array("product_id"=>$v_product_id));
    $v_temp = '';
    $tpl_controls = new Template('dsp_controls.tpl',$v_dir_templates);
    $tpl_controls->set('URL',URL);
    $tpl_controls->set('PACKAGE_ID',$v_product_id);
    foreach($arr_select as $arr){
        $tpl_pack_tab_info = new Template("dsp_package_detail_tab.tpl",$v_dir_templates);
        $v_temp = draw_package_info_tab($tpl_pack_tab_info,$arr,$v_dir_templates);
    }

    //slider
    add_class("cls_tb_product_images");
    $cls_tb_product_image = new cls_tb_product_images($db);
    $tpl_package_content = new Template('dsp_packet_information.tpl',$v_dir_templates);
    $tpl_slide = new Template('dsp_slide.tpl',$v_dir_templates);
    $tpl_slide->set('URL_TEMP',$v_dir_templates);
    $tpl_slide->set('URL',URL);
    $v_slide = $tpl_slide->output();
    //===
    $arr_return['content'] = $v_temp['content'];
    $arr_return['tab'] = $v_temp['tab'];
    $arr_return['control'] = $tpl_controls->output();
    $arr_return['image_slider'] = $v_slide;

    header ('Content-Type: text/html; charset=utf-8');
    header("Content-Type:application/json");
    echo json_encode($arr_return);
?>