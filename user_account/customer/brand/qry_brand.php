<?php if (!isset($v_sval)) die();
add_class("cls_tb_banner");
$cls_tb_banner = new cls_tb_banner($db);
add_class("cls_tb_nail_category");
$cls_tb_nail_category = new cls_tb_nail_category($db);
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);

$v_keyword = isset($_GET['txt_key']) ? $_GET['txt_key'] : '';
$v_keyword = trim($v_keyword);

$v_result = $cls_tb_banner->select_one(array("banner_slugger"=>$v_keyword));

if(!$v_result) redir("/");

// get data
$v_brand_name = $cls_tb_banner->get_banner_name();
$v_brand_id = (string)$cls_tb_banner->get_mongo_id();
$arr_qry_category_of_brand = $cls_tb_nail_category->select(array("brand_id"=>(string)$v_brand_id));
$v_dsp_brand_category = '';
$arr_qry_brand_category = array();
$v_total_product_all_cat = 0 ;
$v_dsp_brand_detail_question = '';
$arr_qry_brand_detail_question = array();
$v_dsp_brand_slider = '';
$arr_qry_brand_slider = array();
$v_first = true;
$v_detail_name = '';
$arr_child_item = $cls_tb_banner->get_child_item();
for($i=0;$i<count($arr_child_item);$i++){
    if($arr_child_item[$i]['name_language'] == $v_language){
        $tpl_brand_about_detail = new Template('dsp_brand_about_detail.tpl', $v_dir_templates);
        $tpl_brand_about_detail->set('ABOUT_QUESTION',$arr_child_item[$i]['text']);
        $arr_qry_brand_detail_question [] = $tpl_brand_about_detail;
        if($v_first){
            $v_detail_name = $arr_child_item[$i]['text'];
            $v_first = false;
            $arr_detail = $arr_child_item[$i]['detail'];
            $arr_product_list = $arr_detail[0]['list'];
            for($j=0;$j<count($arr_product_list);$j++){
                $tpl_slider_item = new Template('dsp_brand_slide_item.tpl', $v_dir_templates);
                $cls_tb_product->select_one(array("_id"=>new MongoId($arr_product_list[$i])));
                $tpl_slider_item->set('PRODUCT_SLUGGER',$cls_tb_product->get_slugger());
                $v_product_image = URL.$cls_tb_product->get_products_upload();
                if(!checkRemoteFile($v_product_image)) $v_product_image = JT_URL.$cls_tb_product->get_products_upload();
                $tpl_slider_item->set('PRODUCT_IMAGE',$v_product_image);
                $tpl_slider_item->set('BRAND_NAME',$v_brand_name);
                $tpl_slider_item->set('PRODUCT_NAME',$cls_tb_product->get_name());
                $tpl_slider_item->set('PRODUCT_SELL_PRICE',$cls_tb_product->get_sell_price());
                $tpl_slider_item->set('PRODUCT_COST_PRICE',$cls_tb_product->get_cost_price());
                $arr_qry_brand_slider [] = $tpl_slider_item;
            }
        }
    }
}
$v_dsp_brand_slider = count($arr_qry_brand_slider)>0 ? Template::merge($arr_qry_brand_slider):'';
$v_dsp_brand_detail_question = count($arr_qry_brand_detail_question) > 0 ? Template::merge($arr_qry_brand_detail_question) : '';
foreach($arr_qry_category_of_brand as $arr_brand_category){
    $v_category_id_temp = (string) $arr_brand_category["_id"];
    $v_total_product = $cls_tb_product->count(array("category_id"=>(string)$v_category_id_temp));
    $v_total_product_all_cat +=$v_total_product;
    $tpl_brand_category = new Template('dsp_brand_category.tpl', $v_dir_templates);
    $tpl_brand_category->set('CATEGORY_NAME',$arr_brand_category['name']);
    $tpl_brand_category->set('TOTAL_PRODUCT',$v_total_product);
    $arr_qry_brand_category [] = $tpl_brand_category;
}
$v_dsp_brand_category = count($arr_qry_brand_category)>0 ? Template::merge($arr_qry_brand_category) : '';
$v_brand_logo = URL."resources/brand/".$cls_tb_banner->get_banner_logo();
$v_brand_thumb = URL."resources/brand/".$cls_tb_banner->get_banner_thumb();
$v_brand_text = $cls_tb_banner->get_banner_des();
$v_detail_des = $cls_tb_banner->get_banner_des();
$v_detail_image = '/images/082112_bareminerals_ready_found.jpg';
// end

$tpl_content = new Template('dsp_brand.tpl', $v_dir_templates);
$tpl_content->set('BRAND_NAME',$v_brand_name);
$tpl_content->set('BRAND_CATEGORY',$v_dsp_brand_category);
$tpl_content->set('TOTAL_PRODUCT_ALL_CAT',$v_total_product_all_cat);
$tpl_content->set('BRAND_LOGO',$v_brand_logo);
$tpl_content->set('BRAND_THUMB',$v_brand_thumb);
$tpl_content->set('BRAND_TEXT',$v_brand_text);
$tpl_content->set('ABOUT_BRAND',$v_dsp_brand_detail_question);
$tpl_content->set('CONTENT_IMAGE',$v_detail_image);
$tpl_content->set('CONTENT_TEXT',$v_detail_des);
$tpl_content->set('DETAIL_NAME',$v_detail_name);
$tpl_content->set('SLIDE_ITEM',$v_dsp_brand_slider);
echo $tpl_content->output();