<?php if (!isset($v_sval)) die();
if(isset($_SESSION['ss_basket_item'])) redir("/checkout_accordion");
$tpl_content = new Template('dsp_checkout_login.tpl',$v_dir_templates);
$dsp_list_basket_item = '';
$v_total_price = 0;
$v_total_tax = 0;
$v_total_fll_price = 0;
$arr_list_basket_item = array();
$arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
$arr_ss_basket_item_id = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
$v_error_message = '';
if(isset($_POST['sign-in'])){
    $v_email = isset($_POST['login']) ? $_POST['login'] : '';
    if(!is_valid_email($v_email)){
        $v_error_message = 'Wrong type email!';
    }else{
        $v_email = trim($v_email);
        $v_email = strip_tags($v_email);
        $v_password = isset($_POST['password']) ? $_POST['password'] : '';
        $v_password = trim($v_password);
        $v_password = strip_tags($v_password);
        $v_select_one = $cls_user->select_one(array("user_email"=>$v_email));
        if($v_select_one){
            $v_select_one = $cls_user->select_one(array("user_email"=>$v_email,"user_password"=>md5($v_password)));
            if($v_select_one){
                $arr_customer['_id'] = $cls_user->get_mongo_id();
                $arr_customer['contact_id'] = $cls_user->get_contact_id();
                $arr_customer['first_name'] = $cls_user->get_first_name();
                $arr_customer['middle_name'] = $cls_user->get_middle_name();
                $arr_customer['last_name'] = $cls_user->get_last_name();
                $arr_customer['full_name'] = $cls_user->get_full_name();
                $arr_customer['addresses'] = $cls_user->get_addresses();
                $arr_customer['address_default'] = $cls_user->get_addresses_default_key();
                $arr_customer['company'] = $cls_user->get_company();
                $arr_customer['deleted'] = $cls_user->get_deleted();
                $arr_customer['direct_dia'] = $cls_user->get_direct_dial();
                $arr_customer['email'] = $cls_user->get_email();
                $arr_customer['extension_no'] = $cls_user->get_extension_no();
                $arr_customer['fax'] = $cls_user->get_fax();
                $arr_customer['is_customer'] = $cls_user->get_is_customer();
                $arr_customer['is_employee'] = $cls_user->get_is_employee();
                $arr_customer['mobile'] = $cls_user->get_mobile();
                $arr_customer['no'] = $cls_user->get_no();
                $arr_customer['position'] = $cls_user->get_position();
                $arr_customer['title'] = $cls_user->get_title();
                $arr_customer['user_password'] = $cls_user->get_user_password();
                $arr_customer['user_name'] = $cls_user->get_user_name();
                $arr_customer['user_status'] = $cls_user->get_user_status();
                $arr_customer['user_type'] = $cls_user->get_user_type();
                $_SESSION['ss_customer'] = serialize($arr_customer);
                $arr_ss_basket = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
                $arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
                $arr_ss_basket_content = get_data_table_cart($arr_ss_basket,$arr_ss_basket_content,$db,$cls_user->get_mongo_id(),$v_count);
                $_SESSION['ss_basket_item'] = $arr_ss_basket;
                $_SESSION['ss_basket_content'] = serialize($arr_ss_basket_content);
                redir("/checkout_accordion");
            }else{
                $v_error_message = 'We have encountered one or more errors during the sign-in process. Please re-enter your e-mail address and password.';
            }
        }else{
            $v_error_message = 'We have encountered one or more errors during the sign-in process. Please re-enter your e-mail address and password.';
        }
    }
}
for($i=0;$i<count($arr_ss_basket_content);$i++){
    $v_check_type = $arr_ss_basket_content[$i]['product_type'];
    $v_type = "STANDARD";
    $v_product_price = $arr_ss_basket_content[$i]['product_price'];
    if($v_check_type==1){
        $v_type = "SAMPLE";
        $v_product_price = "FREE";
    }
    $tpl_checkout_item = new Template("dsp_checkout_login_basket_item.tpl",$v_dir_templates);
    $tpl_checkout_item->set('PRODUCT_ID',$arr_ss_basket_content[$i]['product_id']);
    $tpl_checkout_item->set('PRODUCT_TYPE',$v_type);
    $tpl_checkout_item->set('PRODUCT_IMAGE',JT_URL.$arr_ss_basket_content[$i]['product_image']);
    $tpl_checkout_item->set('PRODUCT_PRICE',$v_product_price);
    $tpl_checkout_item->set('BRAND_NAME',$arr_ss_basket_content[$i]['brand_name']);
    $tpl_checkout_item->set('PRODUCT_NAME',$arr_ss_basket_content[$i]['product_name']);
    $tpl_checkout_item->set('PRODUCT_CODE',$arr_ss_basket_content[$i]['product_code']);
    $tpl_checkout_item->set('PRODUCT_QUANTITY',$arr_ss_basket_content[$i]['product_quantity']);
    $arr_list_basket_item [] = $tpl_checkout_item;
}
$dsp_list_basket_item = Template::merge($arr_list_basket_item);
$tpl_content->set('LIST_CONTENT',$dsp_list_basket_item);
$tpl_content->set('TOTAL_PRICE',$v_total_price);
$tpl_content->set('TAX',$v_total_tax);
$tpl_content->set('ERROR_MESSAGE',$v_error_message);
$tpl_content->set('ALL_PRICE',$v_total_fll_price);
echo $tpl_content->output();