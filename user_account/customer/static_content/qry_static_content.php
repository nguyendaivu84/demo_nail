<?php if(!isset($v_sval)) die();
    $v_static_dir = RESOURCE_URL."static_content/";
    add_class("cls_tb_nail_static_content");
    $v_key = isset($_GET['txt_key']) ? $_GET['txt_key'] : '';
    $cls_tb_nail_static_content = new cls_tb_nail_static_content($db);
    $cls_tb_nail_static_content->select_one(array("content_key"=>$v_key."_".$v_language));
    $dsp_static_content = new Template("dsp_static_content.tpl",$v_dir_templates);
    $dsp_static_content->set('DESCRIPTION',$cls_tb_nail_static_content->get_content_description());
    $dsp_static_content->set('STATIC_CONTENT',$cls_tb_nail_static_content->get_content_content());
    $dsp_static_content->set('URL_CUSTOMER',$v_dir_templates);
    echo $dsp_static_content->output();
?>