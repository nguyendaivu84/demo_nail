<link rel="stylesheet" href="/user_account/customer/templates/default/css/search.css">
<ul class="breadcrumb">
    <li class="OneLinkNoTx">
        <h1>[@BRAND_NAME]</h1>
    </li>
</ul>
<div class="content brand" data-social="true" data-brand="bareMinerals">
<div class="sidebar">
    <div class="sidenav search-navigation">
        <div class="nav-title OneLinkNoTx">[@BRAND_NAME]</div>
        <ul class="nav nav-stacked ">
            <li class="see-all">
                <a data-brandid="5737" data-ref="900000" href="#">
                    See All
                    <span>([@TOTAL_PRODUCT_ALL_CAT])</span>
                </a>
            </li>
            [@BRAND_CATEGORY]
        </ul>
    </div>
    <div class="clear-all-filters top hidden">
        <h3 class="refinement-title">FILTER BY:</h3>
        <input class="btn btn-default" type="submit" value="clear all filters">
    </div>
    <div id="refinements" class="refinements-list"></div>
    <div class="clear-all-filters hidden">
        <input class="btn btn-default" type="submit" value="clear all filters">
    </div>
    <div class="social-links social-components">
        <div class="share-btn icon icon-facebook-large social-enabled " data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=" data-socialtype="fb">
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.astoria.com%2Fbareminerals&description=bareMinerals%20%23astoria&text=bareMinerals%20%23astoria&media=http%3A%2F%2Fwww.astoria.comundefined"></a>
        </div>
        <div id="pinterest-root" class="pin share-btn icon icon-pinterest-large">
            <a data-pin-do="buttonPin" href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.astoria.com%2Fbareminerals%3Fom_mmc%3Doth-pinterest-dotcompinbuttons-2012&media=http%3A%2F%2Fwww.astoria.com%2Fcontentimages%2Fbrands%2Fbareescentuals%2FBareMinerals_Hero_012214_Image.jpg&description=bareMinerals%20%23astoria&ref=http%3A%2F%2Fwww.astoria.com%2Fbareminerals&layout=none" data-pin-config="beside">
                <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png">
            </a>
        </div>
    </div>
</div>

<div id="main" class="maincontent">
<div class="search-banner container">
    <div class="search-header"></div>
</div>

<div class="brand-box with-buttons flush container">
    <div class="box-1">
        <div class="inner">
            <div class="brand-logo">
                <a href="/bareminerals">
                    <img alt="bareMinerals" src="[@BRAND_LOGO]">
                </a>
            </div>
            <div class="brand-content">
                <div class="desc desc-less">
                    [@BRAND_TEXT]
                </div>

            </div>
        </div>
    </div>
    <div class="box-2">
        <img alt="[@BRAND_NAME]" src="[@BRAND_THUMB]">
    </div>
    <div class="brand-right-nav">
        <h3>
            <img width="180" height="22" alt="About the Brand" src="/images/about_the_brand.png">
        </h3>
        <ul class="nav nav-stacked">
            [@ABOUT_BRAND]
        </ul>
    </div>
</div>
<div class="authored-content">
<div class="imageComponent " data-lazyload="true">
    <img width="777" height="28" alt="[@DETAIL_NAME]" src="/images/bareescentuals/082112_bareminerals_ready_found.jpg">
</div>
<a class="imageComponent OneLinkHide" title="" target="" data-lazyload="true" href="/buxom?icid2=Shop_Buxom_Link">
    <img width="777" height="24" src="/contentimages/brands/bareescentuals/bare_minerals_shop_buxom_link_052913.jpg">
</a>
<hr class="spacer5">
<div class="imageComponent OneLinkHide" data-lazyload="true">
    <img width="777" height="279" usemap="#2990409_imageMap" alt="Bare Escentuals" src="[@CONTENT_IMAGE]">
</div>
<div class="html-component">
    <div class="indent">[@CONTENT_TEXT]</div>
</div>
<div class=" carousel-component display-4" data-lazyload="true" data-name="bareminerals_editorspicks_carousel" style="margin-bottom:30px;">
<h2 class="carousel-title">[@DETAIL_NAME]</h2>
<div class="carousel" data-circle="false" data-show="4" data-speed="550">
<ul class="carousel-inner container" style="width: 1448px; left: 0px;">
[@SLIDE_ITEM]


<!-- end of li -->
<li>
    <div class="product-item spacer"></div>
</li>
</ul>
<div class="carousel-control carousel-prev disabled">
    <span class="icon icon-arrow-left"></span>
</div>
<div class="carousel-control carousel-next">
    <span class="icon icon-arrow-right"></span>
</div>
</div>
</div>
</div>
<div id="fs-loader" class="hide" style="height: 745px; top: 0px; display: none;"></div>
</div>
</div>