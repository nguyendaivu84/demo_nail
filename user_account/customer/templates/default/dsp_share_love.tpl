<link type="text/css" rel="stylesheet" href="[@URL_TEMPLATE]/css/account.css" />
<meta name="description" content="Love it. Heart it. Share it. Check out all the beauty I'm loving at Tony nail" />
<meta property="og:type" content="article"/>
<meta property="og:description" content="Love it. Heart it. Share it. Check out all the beauty I'm loving at Tony nail! #Tony nail"/>
<meta property="og:title" content="My Beauty Bag at Tony nail"/>
<meta property="og:image" content="http://www.astoria.com/images/mbb-social-badge.png"/>
<meta property='og:url' content="[@URL_SHARE_LOVE]"/>
<meta property="fb:admins" content="1314428622,676965799"/>
<meta property="fb:app_id" content="124755580884988" />
<meta property="og:site_name" content="Tony nail"/>
<div id="myLoves" class="content shared-loves">
    <div class="sidebar">
        <div class="sidenav search-navigation">
            <section class="fade-in" style="opacity: 1;">
                <strong>
                    <span data-ownername="true">[@CUSTOMER_NAME]’s</span>
                    LOVES LIST
                </strong>
                <p>
                    <span class="bi" data-ownerstatus="true"></span>
                </p>
            </section>
            <section>
                <a class="imageComponent " title="See More Brand Founders' Love Lists" target="" data-lazyload="true" href="[@URL]love">
                    <img width="161" height="258" alt="See More Brand Founders' Love Lists" src="http://www.astoria.com/contentimages/mybeautybag/LoveList_Founders_122613_image.gif">
                </a>
            </section>
        </div>
    </div>
    <div id="main" class="maincontent loves-content list-container list-loves view-list" >
        <section>
            <div id="modal-component-3070016" class="modal ">
                <div class="modal-body">
                    <div class="html-component">
                        <style>
                            .message-public
                            {display:none}
                        </style>
                    </div>
                    <div class="imageComponent " style="padding-left:33px;" data-lazyload="true">
                        <img width="458" height="431" usemap="#3070018_imageMap" src="http://www.astoria.com/contentimages/mybeautybag/beautybag_popup.jpg">
                        <map name="3070018_imageMap">
                            <area alt="Questions? Read FAQs" href="/shopping-astoria-com-my-beauty-bag?mediaId=18600059&icid2=MBB - modal popup - image" coords="0,440,561,502" shape="rect">
                        </map>
                    </div>
                    <div class="html-component">
                        <div class="indent">
                            Questions?
                            <a href="/shopping-astoria-com-my-beauty-bag?mediaId=18600059">Read FAQs ></a>
                        </div>
                    </div>
                </div>
                <span class="icon icon-close"></span>
            </div>
        </section>
        <h1 class="fade-in text-center text-upper alt-bold" style="opacity: 1;">
            <span data-ownername="true">[@CUSTOMER_NAME]’s</span>
            LOVES LIST
        </h1>
        <div class="product-list-head container">
            <div class="pagination">
                <div class="page-numbers" data-seph-controller="astoria.ui.controller.PaginationController"></div>
            </div>
            <div id="paging-form-top" class="pull-right">
                <div class="sort-by">
                    <select class="form-control" name="sortBy">
                        <option value="recently">most recently added</option>
                        <option value="brandNameASC">brand name A - Z</option>
                        <option value="brandNameDESC">brand name Z - A</option>
                        <option value="priceLowToHigh">price low to high</option>
                        <option value="priceHighToLow">price high to low</option>
                    </select>
                </div>
            </div>
        </div>
        [@LIST_FAVORITE_LIST]
        <div class="product-list-foot container">
            <div class="pagination">
                <div class="page-numbers" data-seph-controller="astoria.ui.controller.PaginationController"></div>
            </div>
            <div id="paging-form-top" class="pull-right">
                <div class="sort-by">
                    <select class="form-control" name="sortBy">
                        <option value="recently">most recently added</option>
                        <option value="brandNameASC">brand name A - Z</option>
                        <option value="brandNameDESC">brand name Z - A</option>
                        <option value="priceLowToHigh">price low to high</option>
                        <option value="priceHighToLow">price high to low</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>