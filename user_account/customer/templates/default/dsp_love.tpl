<script>
    $(document).ready(function(){
        $("#list_list").on("click",function(){
            $(this).addClass("selected");
            $("#list_grid").removeClass("selected");
            $("#list-loves").removeClass("view-grid");
            $("#list-loves").addClass("view-list");
        });
        var check_login = '[@IS_LOGIN]';
        $("#click_share").on("click",function(){
            if(check_login!='-1'){
                $("#modal-share").bPopup({
                    opacity:0
                    ,fadeSpeed: 'slow',
                    followSpeed: 1500
                    ,escClose :true
                });
            }else{
                $("#sign_in").trigger("click");
            }
//            return false;
        });
        $("#share_1").on("click",function(){
            if(check_login!='-1'){
                $("#modal-share").bPopup({
                    opacity:0
                    ,fadeSpeed: 'slow',
                    followSpeed: 1500
                    ,escClose :true
                });
            }else{
                $("#sign_in").trigger("click");
            }
//            return false;
        });
        $("#list_grid").on("click",function(){
            $(this).addClass("selected");
            $("#list_list").removeClass("selected");
            $("#list-loves").removeClass("view-list");
            $("#list-loves").addClass("view-grid");
        });
    });
</script>
<link rel="stylesheet" href="[@URL_TEMP]/css/account.css">
<style>
    .popupmodel {
        background-attachment: scroll;
        background-clip: border-box;
        background-color: #FFFFFF;
        background-image: none;
        background-origin: padding-box;
        background-position: 0 0;
        background-repeat: repeat;
        background-size: auto auto;
        box-shadow: 3px 3px 9px rgba(0, 0, 0, 0.3);
        padding-bottom: 24px;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 24px;
        width: 520px;
    }
</style>
<ul class="breadcrumb">
	<li>
		<a href="/profile/myBeautyBag/">My Beauty Bag</a>
	</li>
	<li>Loves</li>
</ul>
<div class="content" id="myLoves">
	<div class="sidebar">
		<div class="sidenav">
			<h2 class="nav-title">
				<a href="/profile/beautyInsider/?mediaId=13700022">Beauty Insider</a>
			</h2>
			<ul class="nav nav-stacked">
				<li>
					<a class="" href="/profile/beautyInsider/rewards.jsp?mediaId=16800018">Rewards Boutique</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/personalized/personalized.jsp?mediaId=16800020">Personalized Recommendations</a>
				</li>
				<li>
					<a class="" href="/profile/beautyInsider/?tab=bi&amp;mediaId=17500017">About Beauty Insider</a>
				</li>
				<li>
					<a class="" href="/profile/beautyInsider/?tab=vib&amp;mediaId=11700041">About VIB</a>
				</li>
				<li>
					<a class="" href="/profile/beautyInsider/?tab=rouge&amp;mediaId=19200029">About VIB Rouge</a>
				</li>
			</ul>
			<h2 class="nav-title">
				<a href="/profile/myBeautyBag/?mediaId=18300038">My Beauty Bag</a>
			</h2>
			<ul class="nav nav-stacked">
				<li>
					<a class="" href="/profile/purchaseHistory/purchaseHistory.jsp?mediaId=16800024">Purchases</a>
				</li>
				<li>Loves</li>
				<li>
					<a class="" href="/contentStore/mediaContentTemplate.jsp?mediaId=20600034">Share the Loves</a>
				</li>
			</ul>
			<h2 class="nav-title">
				<a href="/profile/MyAccount/myAccountLoading.jsp?mediaId=16500028">My Account</a>
			</h2>
			<ul class="nav nav-stacked">
				<li>
					<a class="" href="/profile/MyAccount/MyInformation/myInformation.jsp?mediaId=16800026">My Information</a>
				</li>
				<li>
					<a class="" href="/profile/orders/orderHistory.jsp?mediaId=16800022">Orders</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/PaymentMethods/paymentMethods.jsp?mediaId=16800030">Payments &amp; Credits</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/Subscriptions/subscriptions.jsp?mediaId=16800028">Subscription</a>
				</li>
			</ul>
		</div>
		<div class="link-group ">
			<ul class="nav nav-stacked nav-link-group">
				<li>
					<a href="/contentStore/mediaContentTemplate.jsp?mediaId=9300044&amp;icid2=My Accont Help Link" class="linkComponent ">Help</a>
				</li>
			</ul>
		</div>

	</div>
	<div id="main" class="maincontent loves-content" >
		<div class="account-banner loves-banner">
			<div class="row-component container ">
				<div class="section">
					<div class="imageComponent " data-lazyload="true" rel="#modal-component-3070016" data-modal="true">
						<img src="/user_account/customer/templates/default/img/MBB_Loves01_Banner_122613_Image.jpg" width="270" height="86" alt="MY BEAUTY BAG LOVES" style="display: block;"></div>
				</div>
				<div class="section">
					<a href="/lovelist?icid2= MBB_Loves_top_banner_122613_right_Image" class="imageComponent " data-lazyload="true" target="" title="Keep track of all your favorite beauty and must-try products. See what our brand founders are loving &gt;">
						<img src="/user_account/customer/templates/default/img/MBB_Loves02_Banner_122613_Image.jpg" width="507" height="86" alt="Keep track of all your favorite beauty and must-try products. See what our brand founders are loving &gt;" style="display: block;"></a>
				</div>
			</div>

			<div id="modal-component-3070016" class="modal ">
				<div class="modal-body">
					<div class="html-component">
					</div>
					<div class="imageComponent " data-lazyload="true" style="padding-left:33px;">
						<img src="/user_account/customer/templates/default/img/blank.gif" width="458" height="431" usemap="#3070018_imageMap">
						<map name="3070018_imageMap">
							<area shape="rect" coords="0,440,561,502" href="/shopping-astoria-com-my-beauty-bag?mediaId=18600059&amp;icid2=MBB - modal popup - image" alt="Questions? Read FAQs"></map>
					</div>
					<div class="html-component">
						<div class="indent">
							Questions?
							<a href="/shopping-astoria-com-my-beauty-bag?mediaId=18600059">Read FAQs &gt;</a>
						</div>
					</div>
				</div>
				<span class="icon icon-close" onclick="close_popup_lv1('modal-share"');"></span>
			</div>

		</div>
		<div class="product-list-head container">
			<div class="pagination paging-top container">
				<div class="view-toggles container pull-right">
					<a id="list_list" href="javascript:void(0)" data-toggle="view-list" class="">
						<span class="bar"></span>
						<span class="bar"></span>
						<span class="bar"></span>
						<span class="bar last"></span>
					</a>
					<a id="list_grid" href="javascript:void(0)" class="selected" data-toggle="view-grid">
						<span class="square-row">
							<span class="square"></span>
							<span class="square"></span>
							<span class="square"></span>
						</span>
						<span class="square-row">
							<span class="square"></span>
							<span class="square"></span>
							<span class="square"></span>
						</span>
						<span class="square-row">
							<span class="square"></span>
							<span class="square"></span>
							<span class="square"></span>
						</span>
					</a>
				</div>
				<div class="page-numbers" data-seph-controller="astoria.ui.controller.PaginationController"></div>
				<div id="paging-form-top" class="pull-right">
					<div class="sort-by">
						<label class="inline">sort by</label>
						<select name="sortBy" class="form-control">
							<option value="recently">most recently added</option>
							<option value="brandNameASC">brand name A - Z</option>
							<option value="brandNameDESC">brand name Z - A</option>
							<option value="priceLowToHigh">price low to high</option>
							<option value="priceHighToLow">price high to low</option>
						</select>
					</div>
				</div>
			</div>
			<a class="btn btn-default pop-window pull-right" href="javascript:void(0)" onclick="window.open('[@URL]printlove','Tony nail','height=500,width=800,status=1,scrollbars=1,resizable=1')" >print</a>
			<a class="btn btn-default pull-right" id="share_1" rel="#modal-share" data-modal="true" href="javascript:void(0)">share</a>
		</div>

		<div id="list-loves" class="list-container list-loves view-grid">
			<div id="list_love_item" style="[@EXISTED]" class="product-list container">
				[@LOVED_ITEM_LIST]
			</div>
            <div id="list_love_no_item" style="[@NO_EXISTED]" class="product-list container">
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-1.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-2.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-3.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-4.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-5.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-6.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-7.png">
                </div>
                <div class="list-item list-item-placeholder">
                    <img width="135" height="135" src="[@URL_TEMP]/img/grid-placeholder-8.png">
                </div>
                <div class="overlay overlay-no-love">
                    <div class="message-user">
                        <h3 class="alt-bold">MY LOVES</h3>
                        <p>YOU HAVEN’T LOVED ANYTHING YET!</p>
                        <p>
                            Collect all your favorite beauty and must-try
                            <br>
                            products by clicking on the
                            <span class="icon icon-love"></span>
                            while you shop.
                        </p>
                    </div>
                    <div class="message-public">
                    </div>
                </div>
            </div>
		</div>

		<div class="product-list-foot container">
			<div class="pagination paging-top container">
				<div class="view-toggles container pull-right">
					<a href="pagination.jsp#" data-toggle="view-list" class="">
						<span class="bar"></span>
						<span class="bar"></span>
						<span class="bar"></span>
						<span class="bar last"></span>
					</a>
					<a href="pagination.jsp#" class="selected" data-toggle="view-grid">
						<span class="square-row">
							<span class="square"></span>
							<span class="square"></span>
							<span class="square"></span>
						</span>
						<span class="square-row">
							<span class="square"></span>
							<span class="square"></span>
							<span class="square"></span>
						</span>
						<span class="square-row">
							<span class="square"></span>
							<span class="square"></span>
							<span class="square"></span>
						</span>
					</a>
				</div>
				<div class="page-numbers" data-seph-controller="astoria.ui.controller.PaginationController"></div>
				<div id="paging-form-top" class="pull-right">
					<div class="sort-by">
						<label class="inline">sort by</label>
						<select name="sortBy" class="form-control">
							<option value="recently">most recently added</option>
							<option value="brandNameASC">brand name A - Z</option>
							<option value="brandNameDESC">brand name Z - A</option>
							<option value="priceLowToHigh">price low to high</option>
							<option value="priceHighToLow">price high to low</option>
						</select>
					</div>
				</div>
			</div>
			<a class="btn btn-default pop-window pull-right" href="javascript:void(0);" onclick="window.open('[@URL]printlove','Tony nail','height=500,width=800,status=1,scrollbars=1,resizable=1')" >print</a>
			<a class="btn btn-default pull-right" id="click_share" data-modal="true" href="javascript:void(0)">share</a>
		</div>

		<div class="check popupmodel" id="modal-share" style="display: none" >
			<button type="button" class="icon icon-close" data-dismiss="modal" onclick="close_popup_lv1('modal-share"');"></button>
			<h2>Share</h2>
			<h3>Your Loves</h3>
			<hr>
			<p>Copy this link and then email it to Friends:</p>
			<form name="copyLovesUrl" id="copyLovesUrl">
				<input type="text" name="loves_url" value="[@URL]lovelist/[@USER_ID]" class="form-control input-lg">
				<a class="btn btn-primary btn-lg" id="copyLovesUrlButton" data-clipboard-text="" href="javascript:void(0)">Copy</a>
			</form>
			<p> <strong>OR</strong>
				Share Your Loves on
			</p>
			<div class="social-components">
				<div class="share-btn icon icon-facebook-large social-enabled" data-socialtype="fb" data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=http://nail.anvy.local/lovelist/52f44fc09463c4c00f000073">
					<a href="#" target="_blank"></a>
				</div>
				<div class="share-btn icon icon-pinterest-large social-enabled" data-socialtype="pinterest" data-socialurlprefix="http://pinterest.com/pin/create/button/?url=">
					<a href="#" target="_blank"></a>
				</div>
				<div class="share-btn icon icon-twitter-large social-enabled" data-socialtype="twitter" data-socialurlprefix="https://twitter.com/share?url=" data-socialmessage=" -  | https://www.astoria.com">
					<a href="#" target="_blank"></a>
				</div>
				<div class="share-btn icon icon-gplus-large social-enabled gplus" data-socialtype="gplus">
					<a href="#" class="g-plusone" data-href="" target="_blank" data-callback="trackGPlus"></a>
				</div>
			</div>
			<p>
				<a href="/shopping-astoria-com-my-beauty-bag?mediaId=18600059">
					Questions? Read FAQs
					<span class="arrow arrow-right arrow-mini"></span>
				</a>
			</p>
		</div>
	</div>

</div>

<div class="mbb-nav">
	<a href="/profile/myBeautyBag/">
		<img src="/user_account/customer/templates/default/img/link-mbb.png" width="151" height="20" alt="My Beauty Bag"></a>
	<a href="/profile/purchaseHistory/purchaseHistory.jsp">
		<img src="/user_account/customer/templates/default/img/link-purchases.png" width="115" height="20" alt="Purchases"></a>
</div>

<style type="text/css">a.PIN_1394160765685_pin_it_button_20 {  background-repeat: none!important; background-size: 40px 60px!important; height: 20px!important; margin: 0!important; padding: 0!important; vertical-align: baseline!important; text-decoration: none!important; width: 40px!important; background-position: 0 -20px }
a.PIN_1394160765685_pin_it_button_20:hover { background-position: 0 0px }
a.PIN_1394160765685_pin_it_button_20:active, a.PIN_1394160765685_pin_it_button_20.PIN_1394160765685_hazClick { background-position: 0 -40px }
a.PIN_1394160765685_pin_it_button_inline_20 { position: relative!important; display: inline-block!important; }
a.PIN_1394160765685_pin_it_button_floating_20 { position: absolute!important; }
a.PIN_1394160765685_pin_it_button_en_20_red { background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_rect_red_20_1.png)!important; }
a.PIN_1394160765685_pin_it_button_en_20_white { background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_rect_white_20_1.png)!important; }
a.PIN_1394160765685_pin_it_button_en_20_gray { background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_rect_gray_20_1.png)!important; }
a.PIN_1394160765685_pin_it_button_ja_20_red { background-image: url(/user_account/customer/templates/default/img/pinit_bg_ja_rect_red_20_1.png)!important; }
a.PIN_1394160765685_pin_it_button_ja_20_white { background-image: url(/user_account/customer/templates/default/img/pinit_bg_ja_rect_white_20_1.png)!important; }
a.PIN_1394160765685_pin_it_button_ja_20_gray { background-image: url(/user_account/customer/templates/default/img/pinit_bg_ja_rect_gray_20_1.png)!important; }
a.PIN_1394160765685_pin_it_above_20 span.PIN_1394160765685_pin_it_button_count { background: transparent url(/user_account/customer/templates/default/img/count_north_white_rect_20_1.png) 0 0 no-repeat!important; background-size: 40px 29px!important; position: absolute!important; bottom: 21px!important; left: 0px!important; height: 29px!important; width: 40px!important; font: 12px Arial, Helvetica, sans-serif!important; line-height: 24px!important; text-indent: 0!important;}
a.PIN_1394160765685_pin_it_button_20 span.PIN_1394160765685_pin_it_button_count { position: absolute!important; color: #777!important; text-align: center!important; text-indent: 0!important; }
a.PIN_1394160765685_pin_it_beside_20 span.PIN_1394160765685_pin_it_button_count, a.PIN_1394160765685_pin_it_beside_20 span.PIN_1394160765685_pin_it_button_count i { background-color: transparent!important; background-repeat: no-repeat!important; background-image: url(/user_account/customer/templates/default/img/count_east_white_rect_20_1.png)!important; }
a.PIN_1394160765685_pin_it_beside_20 span.PIN_1394160765685_pin_it_button_count { padding: 0 3px 0 10px!important; background-size: 45px 20px!important; background-position: 0 0!important; position: absolute!important; top: 0!important; left: 41px!important; height: 20px!important; font: 10px Arial, Helvetica, sans-serif!important; line-height: 20px!important; }
a.PIN_1394160765685_pin_it_beside_20 span.PIN_1394160765685_pin_it_button_count i { background-position: 100% 0!important; position: absolute!important; top: 0!important; right: -2px!important; height: 20px!important; width: 2px!important; }
a.PIN_1394160765685_pin_it_button_20.PIN_1394160765685_pin_it_above { margin-top: 20px!important; }
a.PIN_1394160765685_pin_it_button_28 { background-repeat: none!important; background-size: 56px 84px!important; height: 28px!important; margin: 0!important; padding: 0!important; vertical-align: baseline!important; text-decoration: none!important; width: 56px!important; background-position: 0 -28px }
a.PIN_1394160765685_pin_it_button_28:hover { background-position: 0 0px }
a.PIN_1394160765685_pin_it_button_28:active, a.PIN_1394160765685_pin_it_button_28.PIN_1394160765685_hazClick { background-position: 0 -56px }
a.PIN_1394160765685_pin_it_button_inline_28 { position: relative!important; display: inline-block!important; }
a.PIN_1394160765685_pin_it_button_floating_28 { position: absolute!important; }
a.PIN_1394160765685_pin_it_button_en_28_red { background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_rect_red_28_1.png)!important; }
a.PIN_1394160765685_pin_it_button_en_28_white { background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_rect_white_28_1.png)!important; }
a.PIN_1394160765685_pin_it_button_en_28_gray { background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_rect_gray_28_1.png)!important; }
a.PIN_1394160765685_pin_it_button_ja_28_red { background-image: url(/user_account/customer/templates/default/img/pinit_bg_ja_rect_red_28_1.png)!important; }
a.PIN_1394160765685_pin_it_button_ja_28_white { background-image: url(/user_account/customer/templates/default/img/pinit_bg_ja_rect_white_28_1.png)!important; }
a.PIN_1394160765685_pin_it_button_ja_28_gray { background-image: url(/user_account/customer/templates/default/img/pinit_bg_ja_rect_gray_28_1.png)!important; }
a.PIN_1394160765685_pin_it_button_en_16_red_round, a.PIN_1394160765685_pin_it_button_en_32_red_round { background-repeat: none!important; margin: 0!important; padding: 0!important; vertical-align: baseline!important; text-decoration: none!important; }
a.PIN_1394160765685_pin_it_button_en_16_red_round { height: 16px!important; width: 16px!important; background-size: 16px 16px!important; background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_round_red_16_1.png)!important;}
a.PIN_1394160765685_pin_it_button_en_32_red_round { height: 32px!important; width: 32px!important; background-size: 32px 32px!important; background-image: url(/user_account/customer/templates/default/img/pinit_bg_en_round_red_32_1.png)!important;}
a.PIN_1394160765685_pin_it_button_inline_en_16_red_round, a.PIN_1394160765685_pin_it_button_inline_en_32_red_round { position: relative!important; display: inline-block!important; }
a.PIN_1394160765685_pin_it_button_floating_en_16_red_round, a.PIN_1394160765685_pin_it_button_floating_en_32_red_round  { position: absolute!important; }
a.PIN_1394160765685_pin_it_button_28 span.PIN_1394160765685_pin_it_button_count { position: absolute!important; color: #777!important; text-align: center!important; text-indent: 0!important; }
a.PIN_1394160765685_pin_it_above_28 span.PIN_1394160765685_pin_it_button_count { background: transparent url(/user_account/customer/templates/default/img/count_north_white_rect_28_1.png) 0 0 no-repeat!important; background-size: 56px 37px!important; position: absolute!important; bottom: 29px!important; left: 0px!important; height: 37px!important; width: 56px!important; font: 15px Arial, Helvetica, sans-serif!important; line-height: 28px!important; text-indent: 0!important;}
a.PIN_1394160765685_pin_it_beside_28 span.PIN_1394160765685_pin_it_button_count, a.PIN_1394160765685_pin_it_beside_28 span.PIN_1394160765685_pin_it_button_count i { background-color: transparent!important; background-repeat: no-repeat!important; background-image: url(/user_account/customer/templates/default/img/count_east_white_rect_28_1.png)!important; }
a.PIN_1394160765685_pin_it_beside_28 span.PIN_1394160765685_pin_it_button_count { padding: 0 3px 0 10px!important; background-size: 63px 28px!important; background-position: 0 0!important; position: absolute!important; top: 0!important; left: 57px!important; height: 28px!important; font: 12px Arial, Helvetica, sans-serif!important; line-height: 28px!important; }
a.PIN_1394160765685_pin_it_beside_28 span.PIN_1394160765685_pin_it_button_count i { background-position: 100% 0!important; position: absolute!important; top: 0!important; right: -2px!important; height: 28px!important; width: 2px!important; }
a.PIN_1394160765685_pin_it_button_28.PIN_1394160765685_pin_it_above { margin-top: 28px!important; }
a.PIN_1394160765685_follow_me_button, a.PIN_1394160765685_follow_me_button i { background-size: 200px 60px!important; background: transparent url(/user_account/customer/templates/default/img/bfs1.png) 0 0 no-repeat }
a.PIN_1394160765685_follow_me_button { color: #444!important; display: inline-block!important; font: bold normal normal 11px/20px "Helvetica Neue",helvetica,arial,san-serif!important; height: 20px!important; margin: 0!important; padding: 0!important; position: relative!important; text-decoration: none!important; text-indent: 19px!important; vertical-align: baseline!important;}
a.PIN_1394160765685_follow_me_button:hover { background-position: 0 -20px}
a.PIN_1394160765685_follow_me_button:active  { background-position: 0 -40px}
a.PIN_1394160765685_follow_me_button b { position: absolute!important; top: 3px!important; left: 3px!important; height: 14px!important; width: 14px!important; background-size: 14px 14px!important; background-image: url(/user_account/customer/templates/default/img/log1.png)!important; }
a.PIN_1394160765685_follow_me_button i { position: absolute!important; top: 0!important; right: -4px!important; height: 20px!important; width: 4px!important; background-position: 100% 0px!important; }
a.PIN_1394160765685_follow_me_button:hover i { background-position: 100% -20px!important;  }
a.PIN_1394160765685_follow_me_button:active i { background-position: 100% -40px!important; }
a.PIN_1394160765685_follow_me_button_tall, a.PIN_1394160765685_follow_me_button_tall i { background-size: 400px 84px!important; background: transparent url(/user_account/customer/templates/default/img/bft1.png) 0 0 no-repeat }
a.PIN_1394160765685_follow_me_button_tall { color: #444!important; display: inline-block!important; font: bold normal normal 13px/28px "Helvetica Neue",helvetica,arial,san-serif!important; height: 28px!important; margin: 0!important; padding: 0!important; position: relative!important; text-decoration: none!important; text-indent: 33px!important; vertical-align: baseline!important;}
a.PIN_1394160765685_follow_me_button_tall:hover { background-position: 0 -28px}
a.PIN_1394160765685_follow_me_button_tall:active  { background-position: 0 -56px}
a.PIN_1394160765685_follow_me_button_tall b { position: absolute!important; top: 5px!important; left: 10px!important; height: 18px!important; width: 18px!important; background-size: 18px 18px!important; background-image: url(/user_account/customer/templates/default/img/smt1.png)!important; }
a.PIN_1394160765685_follow_me_button_tall i { position: absolute!important; top: 0!important; right: -10px!important; height: 28px!important; width: 10px!important; background-position: 100% 0px!important; }
a.PIN_1394160765685_follow_me_button_tall:hover i { background-position: 100% -28px!important;  }
a.PIN_1394160765685_follow_me_button_tall:active i { background-position: 100% -56px!important; }
span.PIN_1394160765685_embed_pin { display: inline-block!important; text-align: center!important; width: 237px!important; overflow: hidden!important; vertical-align: top!important; }
span.PIN_1394160765685_embed_pin.PIN_1394160765685_fancy { background: #fff!important; box-shadow: 0 0 3px #aaa!important; border-radius: 3px!important; }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link { display: block!important;  margin: 0 auto!important; padding: 0!important; position: relative!important;  line-height: 0}
span.PIN_1394160765685_embed_pin img { border: 0!important; margin: 0!important; padding: 0!important;}
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link i.PIN_1394160765685_repin { left: 10px!important; top: 10px!important; position: absolute!important; height: 33px!important; width: 64px!important; background-size: 64px 99px!important; background: transparent url(/user_account/customer/templates/default/img/repin1.png) }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link i.PIN_1394160765685_repin_ja { left: 10px!important; top: 10px!important; position: absolute!important; height: 33px!important; width: 64px!important; background-size: 64px 99px!important; background: transparent url(/user_account/customer/templates/default/img/ja_repin1.png) }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link i.PIN_1394160765685_repin:hover { background-position: 0 -33px!important; }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link i.PIN_1394160765685_repin.PIN_1394160765685_hazClick { background-position: 0 -66px!important; }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link i.PIN_1394160765685_getThis { display: none }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link:hover i.PIN_1394160765685_getThis, span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link:hover i.PIN_1394160765685_getThis i { background: transparent url(/user_account/customer/templates/default/img/bfs1.png) }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link:hover i.PIN_1394160765685_getThis { color: #555!important; display: inline-block!important; font: normal normal normal 11px/20px "Helvetica Neue",helvetica,arial,san-serif!important; height: 20px!important; margin: 0!important; padding: 0 1px 0 5px!important; position: absolute!important; bottom: 10px!important; right: 10px!important; text-decoration: none!important;  }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link:hover i.PIN_1394160765685_getThis:hover { background-position: 0 -20px }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link:hover i.PIN_1394160765685_getThis i { position: absolute!important; top: 0!important; right: -4px!important; height: 20px!important; width: 5px!important; background-position: 100% 0px }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_link:hover i.PIN_1394160765685_getThis:hover i { background-position: 100% -20px }
span.PIN_1394160765685_embed_pin span.PIN_1394160765685_embed_pin_desc { color: #333!important; white-space: normal!important; border-bottom: 1px solid #eee!important; display: block!important; font-family: "Helvetica Neue", arial, sans-serif!important; font-size: 12px!important; line-height: 17px!important; padding: 10px!important; text-align: left!important; }
span.PIN_1394160765685_embed_pin span.PIN_1394160765685_embed_pin_attrib, span.PIN_1394160765685_embed_pin span.PIN_1394160765685_embed_pin_text_container { color: #a7a7a7!important; font-family: "Helvetica", sans-serif!important; font-size: 10px!important; line-height: 18px!important; font-weight: bold!important; display: block!important;}
span.PIN_1394160765685_embed_pin span.PIN_1394160765685_embed_pin_attrib img.PIN_1394160765685_embed_pin_attrib_icon { height: 16px!important; width: 16px!important; vertical-align: middle!important; margin-right: 5px!important; float: left!important;}
span.PIN_1394160765685_embed_pin span.PIN_1394160765685_embed_pin_attrib a { color: #a7a7a7!important; text-decoration: none!important;}
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text, span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text span.PIN_1394160765685_embed_pin_text_container { position: relative!important; text-decoration: none!important; display: block!important; font-weight: bold!important; color: #b7b7b7!important; font-family: "Helvetica Neue", arial, sans-serif!important; font-size: 11px!important; line-height: 14px!important; height: 39px!important; text-align: left!important; }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text { padding: 5px 0 0 7px!important; }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text:hover { background: #eee!important;}
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text img.PIN_1394160765685_embed_pin_text_avatar { border-radius: 2px!important; overflow: hidden!important; height: 30px!important; width: 30px!important; vertical-align: middle!important; margin-right: 5px!important; float: left!important;}
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text span.PIN_1394160765685_embed_pin_text_container em.PIN_1394160765685_embed_pin_text_container_em { font-family: inherit!important; display: block!important; color: #717171!important; font-style: normal!important; width: 180px!important; text-overflow: ellipsis!important; overflow: hidden!important; white-space: nowrap!important; }
span.PIN_1394160765685_embed_pin a.PIN_1394160765685_embed_pin_text b.PIN_1394160765685_embed_pin_link_shield { position: absolute!important; top: 0!important; left: 0!important; height: 100%!important; width: 100%!important; }
span.PIN_1394160765685_embed_grid { display: inline-block!important; margin: 0!important; padding:10px 0!important; position: relative!important; text-align: center}
span.PIN_1394160765685_embed_grid.PIN_1394160765685_fancy { background: #fff!important; box-shadow: 0 0 3px #aaa!important; border-radius: 3px!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd { display: block!important; margin: 0 10px!important; padding: 0!important; height: 45px!important; position: relative!important; background: #fff}
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a.PIN_1394160765685_avatar { position: absolute!important; top: 0!important; left: 0!important; height: 36px!important; width: 36px!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a.PIN_1394160765685_avatar::before { position: absolute!important; content:""!important; z-index: 2!important; top: 0!important; left: 0!important; right: 0!important; bottom: 0!important; box-shadow: inset 0 0 2px #888!important;  border-radius: 3px!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a.PIN_1394160765685_avatar img { position: relative!important; height: 36px!important; width: 36px!important; margin: 0!important; padding: 0!important; border-radius: 3px!important; border: none!important;}
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a { text-decoration: none!important; background: transparent!important; cursor: pointer!important; white-space: nowrap!important; position: absolute!important; left: 44px!important; text-align: left!important; overflow: hidden!important; text-overflow: ellipsis!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a:hover { text-decoration: none!important; background: #fff!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a:active { text-decoration: none!important; background: #fff!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a.PIN_1394160765685_embed_grid_first { top: 2px!important; font-family: helvetica, sans-serif!important; font-weight: bold!important; color:#333!important; font-size: 14px!important; line-height: 16px!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a.PIN_1394160765685_embed_grid_second { bottom: 11px!important; font-family: helvetica, sans-serif!important; color:#8e8e8e!important; font-size: 12px!important; line-height: 14px!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_hd a.PIN_1394160765685_embed_grid_mid { top: 12px!important; font-family: helvetica, sans-serif!important; font-weight: bold!important; color:#333!important; font-size: 14px!important; line-height: 16px!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_bd { display:block!important; margin: 0 10px!important; border-radius: 2px!important; position: relative!important; overflow: hidden }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_scrolling_okay { overflow: auto!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_bd span.PIN_1394160765685_embed_grid_ct { display:block!important; position: relative!important; overflow: hidden!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_bd a.PIN_1394160765685_embed_grid_th { cursor: pointer!important; display: inline-block!important; position: absolute!important; overflow: hidden!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_bd a.PIN_1394160765685_embed_grid_th::before { position: absolute!important; content:""!important; z-index: 2!important; top: 0!important; left: 0!important; right: 0!important; bottom: 0!important; box-shadow: inset 0 0 2px #888!important; }
span.PIN_1394160765685_embed_grid span.PIN_1394160765685_embed_grid_bd a.PIN_1394160765685_embed_grid_th img.PIN_1394160765685_embed_grid_img { border: none!important; position: absolute!important; top: 50%!important; left: 0!important; }
a.PIN_1394160765685_embed_grid_ft { text-shadow: 0 1px #fff!important; display: block!important; text-align: center!important; border: 1px solid #ccc!important; margin: 10px 10px 0!important; height: 31px!important; line-height: 30px!important;border-radius: 2px!important; text-decoration: none!important; font-family: Helvetica!important; font-weight: bold!important; font-size: 13px!important; color: #746d6a!important; background: #f4f4f4 url(/user_account/customer/templates/default/img/board_button_link.png) 0 0 repeat-x}
a.PIN_1394160765685_embed_grid_ft:hover { text-decoration: none!important; background: #fefefe url(/user_account/customer/templates/default/img/board_button_hover.png) 0 0 repeat-x}
a.PIN_1394160765685_embed_grid_ft:active { text-decoration: none!important; background: #e4e4e4 url(/user_account/customer/templates/default/img/board_button_active.png) 0 0 repeat-x}
a.PIN_1394160765685_embed_grid_ft span.PIN_1394160765685_embed_grid_ft_logo { vertical-align: top!important; display: inline-block!important; margin-left: 2px!important; height: 30px!important; width: 66px!important; background: transparent url(/user_account/customer/templates/default/img/board_button_logo.png) 50% 48% no-repeat!important; }
.PIN_1394160765685_hidden { display:none!important; }</style>