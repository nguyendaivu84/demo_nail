<div class="footer wrapper">
	<!-- <div class="footer-banner">
		<div class='imageComponent ' data-lazyload='true' >
			<img src="/user_account/customer/templates/default/img/socialfooter_hp_can_092512.jpg" width="980" height="87" usemap="#2820468_imageMap" />
		</div>
	</div> -->
	<div class="footer-actions container">
		<div class="email-signup">
			<label for="email-signup-field" class="inline">
				<span class="icon icon-mail"></span>
        <!-- <span class="label-txt text-upper">[@LG_FOOTER_SIGN_UP]</span> -->
				<span class="label-txt text-upper" style=" font-family: fontsDosis;">Sign up for Astoria Nails Supply e-mails</span>
			</label>
			<input style=" font-family: fontsDosis;" type="email" id="email-signup-field" maxlength="60" class="form-control email-signup-field" placeholder="Email">
			<button class="btn btn-primary btn-icon" type="submit" name="go-email">
				<span class="arrow arrow-right"></span>
			</button>
            <!-- <select onchange="change_language($(this).val());" id="language" name="language">
                [@OPTION_LANGUAGE]
            </select> -->
			<div id="email-error" class="modal modal-small">
				<div class="modal-header">
					<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">OOPS...</h4>
				</div>
				<div class="modal-body">
					<p class="error-message large">Please re-enter your e-mail address using the proper format.</p>
				</div>
			</div>

			<div id="email-thanks" class="modal modal-small">
				<div class="modal-header">
					<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">THANK YOU!</h4>
				</div>
				<div class="modal-body">
					<p class="success-message large">Thanks for signing up for Astoria Nails Supply.</p>
				</div>
			</div>
		</div>

</div>
<div class="copyright" style=" font-family: fontsDosis;">
          <p>
            Copyright &copy; 2014 <span class="bold">Astoria Nails &amp; Beauty Supply</span> CA, Inc. All rights reserved. &nbsp;
            <a href="/store-content/term-of-use">Terms of Use</a>
            &nbsp;|&nbsp;
            <a href="/store-content/privacy-policy">Privacy Policy</a>
          </p>
          <p>
            Contact us: #101, 3016 10 ave NE Calgary, AB T2A 6A3 Canada&nbsp; - &nbsp;Email: <a href="mailto:loannly@astorianails.com">loannly@astorianails.com</a>
          </p>
          <p>
            <a class="icon icon-facebook social-enabled" data-socialtype="fb" data-socialurlprefix="" href="#" target="_blank"></a>
            <a href="" class="icon icon-twitter"></a>
            <a href="" class="icon icon-pinterest"></a>
            </p>
</div>
</div>
<div id="modal-canada-welcome" class="modal modal-canada-welcome">
<div class="modal-body text-center">
<div class="divider"></div>
<span class="icon icon-close"></span>
</div>
<div id="quick-look" class="modal quick-look">
<div class="container">
<div class="modal-body container hide"></div>
</div>
<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
</div>
<div class="modal" id="modal-content-component">
<div class="modal-body"></div>
<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
</div>
<div id="email-stock" class="modal email-stock">
<div class="modal-header">
<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body"></div>
</div>
<script src="/user_account/customer/templates/default/js/jquery.easing.js"></script>
<script src="/user_account/customer/templates/default/js/jquery-ui.custom.min.js"></script>
<script src="/user_account/customer/templates/default/js/jcarousellite.js"></script>
<script src="/user_account/customer/templates/default/js/jquery.placeholder.js"></script>
<script src="/user_account/customer/templates/default/js/transition.js"></script>
<script src="/user_account/customer/templates/default/js/tooltip.js"></script>
<script src="/user_account/customer/templates/default/js/popover.js"></script>
<script src="/user_account/customer/templates/default/js/tab.js"></script>
<script src="/user_account/customer/templates/default/js/handlebars.js"></script>
<script src="/user_account/customer/templates/default/js/jail.js"></script>

<script>
window.astoriaBundle = {
  "basket.sku.quantity.invalid": "Please enter a valid quantity before adding item to your basket.",
  "basket.hasNoItems": "{0}Your basket is currently empty.{1}",
  "basket.hasNoItems.notSignedIn": "{0}Your basket is currently empty.{1} Please {2} if you are trying to retrieve a basket created in the past.",
  "basket.popup.warning.bi.remove": "The Beauty Insider rewards will be removed. Click 'continue' to confirm.",
  "basket.freeShipping.flash": "You now qualify for {0}free 2 Day shipping!{1}",
  "basket.freeShipping.flash.hazmat": "You now qualify for {0}free ground shipping!{1}",
  "basket.freeShipping.treshold": "You now qualify for {0}free standard shipping!{1}",
  "basket.freeShipping.rouge": "{0} members enjoy {1}free 3-day shipping{2} on every order.",
  "basket.freeShipping.rouge.ca": "{0} members enjoy {1}free standard shipping{2} on every order.",
  "basket.freeShipping.rouge.hazmat": "{0} members enjoy {1}Free Ground Shipping{2}",
  "checkout.bireward.summary.noneSelected": "None selected",
  "egc.max.message.count": "Maximum count reached.",
  "egc.email.check_valid": "Please review email provided and make sure it's a valid email address.",
  "validation.zip.invalid": "Invalid zip or postal code.",
  "validation.postal.invalid": "Invalid postal code.",
  "payment.expiration.mustbe.future": "Expiration date must be in the future.",
  "payment.giftTender.apply": "You are about to navigate away from this section and have not applied your {0} yet. Please hit 'APPLY' before continuing.",
  "email.invalid": "Please enter a valid e-mail address in the format username@domain.com.",
  "email.mismatch": "Email address provided does not match, please correct the error in the highlighted field.",
  "errors.general": "There are errors in the highlighted fields below.",
  "inventory.lookup.notavailable": "Sorry, this feature is temporarily unavailable. Please try again in a few minutes.",
  "date.invalid": "You have entered an invalid date.",
  "promotion.shipping.method.invalid": "The selected delivery method is not valid with the promotion code {0}. You must select {1} to qualify. If you continue, the shipping promotion will be removed.",
  "social.security.last4digits.invalid": "You must include last 4 digits of Social Security Number. Please try again.",
  "shoppingbasket.noticeinfo.isHazmatSku": "This item is considered {0}. Please note that your order will ship Standard.",
  "shoppingbasket.noticeinfo.isHazmatSkuCA": "This item requires ground shipping due to hazardous material shipping restrictions.",
  "sku.notAvailable.ca": "This item cannot be shipped to Canada.",
  "sku.notAvailable": "This item cannot be shipped to the United States.",
  "storeLocator.results.none": "No stores are located in that area. Please try another search.",
  "storeLocator.input.bad": "Based on your search, we couldn't find a match. Please enter either a valid zip code, or a city and state.",
  "storeLocator.input.bad.ca": "Based on your search, we couldn't find a match. Please enter either a valid postal code, or a city and province.",
  "storeLocator.input.missing": "Please select a zip code or city / state to locate a astoria store near you.",
  "storeLocator.input.missing.ca": "Please select a postal code or city / province to locate a astoria store near you.",
  "validation.email.missing": "Please enter an email address.",
  "validation.creditcard.expiration" : "This card is expired. Please update your card information.",
  "validation.creditcard.number" : "We have detected an invalid card number. Please update the card number field.",
  "validation.postal.missing" : "Please enter a zip or postal code to find stores near you.",
  "validation.region.missing" : "Please select a state/region or enter a valid zip or postal code to find stores nearest to you.",
  "validation.postal.invalid.for.stores" : "Please enter a valid zip or postal code to find stores nearest to you.",
  "validation.forgotpassword.user.not.exist": "There was an error with your email/password combination. Please try again.<br/> Please include your email address before clicking on 'Forgot Password' link.",
  "validation.no.samples.selected": "You must select samples to add before clicking Add Samples",
  "user.registration.emailAlreadyExists": "An account already exists for {0}. Please sign in or choose another e-mail address.",
  "user.password.format.error": "Passwords must be 6 to 12 characters (letters or numbers) long. Special characters are allowed.",
  "user.password.mismatch": "The passwords you entered do not match. Please fix to continue.",
  "user.firstName.missing": "Missing first name.",
  "user.lastName.missing": "Missing last name.",
  "vibrt.spend_to_become": "Spend <b>{0}{1}</b> more to become a Very Important Beauty Insider (VIB).",
  "vibrt.qualifies_for_vib": "Your current order qualifies you for Very Important Beauty Insider (VIB) status.",
  "vibrt.spend_to_extend": "Spend <b>{0}{1}</b> more to extend VIB status through {2}.",
  "vibrt.extend_vib": "This order extends your VIB status through {0}.",
  "vibrt.learn_more": "Learn more",
  "vibrt.spend_to_become_rouge": "Spend {0} more to maintain your {1} status through {2}.",
  "vibrt.spend_to_unlock_rouge": "Spend just {0} more to unlock {1}, our new premium membership status.",
  "vibrt.spend_to_become_rouge_basket": "Almost there... spend just {0} more to maintain your {1} status through {2}.",
  "vibrt.qualifies_for_vib_rouge": "Purchase what's in your basket to unlock {0}, our new premium membership status.",
  "vibrt.requalifies_for_vib_rouge": "Another beautiful year is ahead... this order maintains your {0} status through {1}.",
  "vibrt.vib_rouge_requalifies": "Purchase what's in your basket to maintain your {0} status through {1}.",
  "vibrt.become.bi": "Become a Beauty Insider to save. {0}.",
  "vibrt.vib.title": "Very Important Beauty Insider (VIB)",
  "vibrt.vib.short_title": "VIB"
};
</script>
<script>
window.astoriaBundle.titles = {
  "storeLocator" : {
    "hq" : "astoria Store Locator - Find a astoria Store Near You | astoria",
    "events" : "astoria City Name | Store Name",
    "classes" : " Beauty & Makeup Classes | astoria ",
    "services" : " Beauty Services | astoria "
  }
};
</script>
<script src="/user_account/customer/templates/default/js/astoria.core.js"></script>
<script src="/user_account/customer/templates/default/js/astoria.js"></script>
<script src="/user_account/customer/templates/default/js/astoria.headerfooter.js"></script>
</body>
</html>
