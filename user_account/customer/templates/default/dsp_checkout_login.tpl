<div class="banner-container"></div>
<div class="page-intro">
	<h1 class="alt-book text-upper">Checkout Sign In<img src="/user_account/customer/templates/default/img/icon-flag-ca.png" width="30" height="20" class="flag-shadow"></h1>
</div>
<div class="main signin">
	<div class="signin-returning container">
        <p class="error-message"> [@ERROR_MESSAGE]</p>
		<h3 class="alt-book text-upper">Returning customer</h3>
		<form novalidate="novalidate" name="checkoutSignIn" action="" method="post">
			<div class="email container">
				<label for="email_returning">E-mail address</label>
				<input id="email_returning" maxlength="60" name="login" value="" class="form-control " type="email">
				<p class="help-block"></p>
			</div>
			<div class="password container">
				<label for="password">Password</label>
				<input id="password" name="password" value="" class="form-control " type="password" autocomplete="off">
                <button class="btn btn-primary btn-sign-in" name="sign-in" type="submit">sign in</button>
				<p class="help-block">
					<a href="#" class="reset-password">forgot password?</a>
				</p>
			</div>
		</form>
	</div>
	<div class="signin-new container">
		<h3 class="alt-book text-upper">New customer or signed-up in store?</h3>
		<form novalidate="novalidate" name="checkoutRegister" action="/checkout/accordions.jsp?_DARGS=/checkout/login.jsp.checkoutRegisterForm" method="post">
			<div class="email container">
				<label for="user_name">E-mail address</label>
				<input id="user_name" name="/atg/userprofiling/ProfileFormHandler.registrationEmail" value="" class="form-control " type="email">
				<button class="btn btn-primary" type="submit" name="continue">continue</button>
				<p class="help-block">Start here if you’re new to our web site or registered for Beauty Insider in a astoria store.</p>
			</div>
		</form>
	</div>
</div>
<div class="checkout-sidebar">
	<div class="my-basket">
		<h2 class="module-title">
			My Shopping Cart
			<a class="btn btn-primary btn-sm btn-back" href="/checkout">back</a>
		</h2>
		<span id="currency" data-currency="C$"></span>
		<div class="my-basket-content">
			<div class="product-list custom-scroll">
				<div class="products" style="display: block;">
                    [@LIST_CONTENT]
				</div>
			</div>
		</div>
		<div class="calculation">
			<ul class="container">
				<li>
					<span class="line-item">Merchandise Subtotal</span>
					<span class="currency">C$</span>
					<span class="cost merch-total">[@TOTAL_PRICE]</span>
				</li>
				<!--li class="discount" style="display:none;">
					<span class="line-item">Discounts</span>
					<span class="currency">C$</span>
					<span class="cost">[@DISCOUNT]</span>
				</li-->
				<li>
					<span class="line-item">Taxes</span>
					<span class="currency">C$</span>
					<span class="cost tax">[@TAX]</span>
				</li>
				<!--li class="subtotal" style="display:none;">
					<span class="line-item">Order Subtotal</span>
					<span class="currency">C$</span>
					<span class="cost sub">380.00</span>
				</li>
				<li class="credit store" style="display:none;">
					<span class="line-item">Store Credit</span>
					<span class="currency">-C$</span>
					<span class="cost"></span>
				</li>
				<li class="credit gift" style="display:none;">
					<span class="line-item">Gift Card</span>
					<span class="currency">-C$</span>
					<span class="cost"></span>
				</li>
				<li class="credit egift" style="display:none;">
					<span class="line-item">eGift Certificate</span>
					<span class="currency">-C$</span>
					<span class="cost"></span>
				</li-->
				<li class="total">
					<span class="line-item">Order Total</span>
					<span class="currency">C$</span>
					<span class="cost total">[@ALL_PRICE]</span>
				</li>
			</ul>
		</div>
	</div>
</div>