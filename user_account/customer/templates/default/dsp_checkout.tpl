<link rel="stylesheet" href="/user_account/customer/templates/default/css/basket.css">
<style>
	.fullwidth{
		width: 100% !important;
	}
</style>
<script>
    $(document).ready(function(){
        $("#error_message").hide();
        var check_count = '[@COUNT]';
        var check__ = '[@IS_LOGIN]';
        check_count = parseInt(check_count,10);
        check__ = parseInt(check__,10);
        if(check_count==0) $("#error_message").show();
        $("#link_check_out").on("click",function(){
            var link = $(this).attr("data-link");
            var count = $(".data_count").length;
            if(count<=0){
                $("#error_message").show("slow");
                return false;
            }
            else {
//                if(check__==0)
//                    $("#sign_in").trigger("click");
//                else window.location.href = link;
                    window.location.href = link;
            }
        });
        $("#link_check_out1").on("click",function(){
            var link = $(this).attr("data-link");
            var count = $(".data_count").length;
            if(count<=0){
                $("#error_message").show("slow");
                return false;
            }
            else {
//                if(check__==0)
//                    $("#sign_in").trigger("click");
//                else window.location.href = link;
                    window.location.href = link;
            }
        });
        $("#checkout-sign-in").on("click",function(){
            $("#sign_in").trigger("click");
        });
        $("#form_checkout").on("click",function(){
            var link = $(this).attr("data-link");
            var count = $(".data_count").length;
            if(count<=0){
                $("#error_message").show("slow");
                return false;
            }
            else {
//                if(check__==0)
//                    $("#sign_in").trigger("click");
//                else window.location.href = link;
                    window.location.href = link;
            }
        });
        $("#remove_all_samples").on("change",function(){
            var check = $(this).is(":checked");
            if(check){
                delete_all_sample("[@URL_CHECKOUT]checkout");
            }else{
                $(".add_remove_all").each(function(){
                    var this_id = $(this).attr("data-product-type");
                    $("#"+this_id).removeAttr("disabled");
                });
            }
        });
    });
</script>
<script>
    function product_quantity_keyup(){
        //product_quantity
        var v_hidden_price = $("#txt_hidden_price").val();
        var quan = $("#product_quantity").val();
        if(quan > 2147483647){
            quan = 1;
            $("#product_quantity").val(quan);
        }else if(quan <= 0){
            alert("Quantity must greater than 0");
            quan = 1;
            $("#product_quantity").val(quan);
        }
        var unit = $("#txt_unit_price").val();
        unit = parseFloat(unit);
        unit = unit.toFixed(2);
        var total = quan * unit;
        //alert(v_hidden_price);
        if(v_hidden_price==0) document.getElementById("spn_unit_price").innerHTML  = "$ "+unit;//$("#spn_unit_price").text("$ "+unit);
        if(v_hidden_price==0) document.getElementById("total_price_span").innerHTML  = total.toMoney();//$("#total_price_span").text(formatDollar(total));
        if(v_hidden_price==0) $("#total_price").val(total);
    }
</script>
<div class="content basket container">
  <h1 class="alt-book text-upper page-header" style="font-weight: bold;">
	My Shopping cart
	<!--img src="/user_account/customer/templates/default/img/icon-flag-ca.png" width="30" height="20" class="flag-shadow"-->
	</h1>
  <div id="main" class="maincontent fullwidth">
	<div class="basket-head container">
	  <div class="top-promo-msg">
		<p class="free-shipping ">
		  You now qualify for
		  <span class="free">free shipping!</span>
		</p>
		<p class="free-flash-shipping hide">
		  You now qualify for
		  <span class="free">free 2 Day shipping!</span>
		</p>
		<p class="free-flash-hazmat-shipping hide">
		  You now qualify for
		  <span class="free">free ground shipping!</span>
		</p>
		<p class="rouge-free-ship hide"> <b class="label-rouge">VIB Rouge</b>
		  members enjoy
		  <span class="free">free standard shipping</span>
		  on every order.
		</p>
		<p class="free-rouge-hazmat-shipping hide"> <b class="label-rouge">VIB Rouge</b>
		  members enjoy
		  <span class="free">Free Ground Shipping</span>
		</p>
	  </div>
	  <div class="basket-actions pull-right container">
		<ul>
		  <li class="action-checkout">
			<a href="javascript:void(0);" data-link="[@LINK_TO]" id="link_check_out1" class="btn btn-alt btn-lg btn-block">
			  checkout
			  <span class="arrow arrow-right"></span>
			</a>
		  </li>
		  <li>
			<a class="btn btn-link btn-shop" href="/">
			  <span class="arrow arrow-left"></span>
			  continue shopping
			</a>
		  </li>
		</ul>
	  </div>
	</div>
	<form id="contentForm" name="contentForm" action="/[@LINK_TO]" method="post">
  <div class="basket-contents" data-currency="C$">
	<div class="basket-items" id="add_item_to_here">
        <p class="error-message msg-basket-empty" id="error_message" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
            <b>Your shopping cart is currently empty.</b>
            [@NOT_SIGN_IN_MESSAGE]
        </p>
        <p></p>
        [@ITEM]
    </div>
</div>
<div class="basket-summary container">

<div class="basket-totals with-promo-box" style="width:500px;" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
  <div class="total-row container" style="display:none;">
	<div class="label" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">Merchandise Total</div>
	<div class="value">
	  $
	  <span class="merch-total"></span>
	</div>
  </div>

  <div class="total-row container giftcard" style="display:none;">
	<div class="label" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">Gift Card Purchase</div>
	<div class="value">
	  $
	  <span class="gift-card-total"></span>
	</div>
  </div>

  <div class="total-row container e-giftcard" style="display:none;">
	<div class="label" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">eGift Certificate Purchase</div>
	<div class="value">
	  $
	  <span class="egift-total"></span>
	</div>
  </div>

  <div class="total-row container discount" style="display:none;">
	<div class="label" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">Discounts</div>
	<div class="value">
	  -$
	  <span class="cost"></span>
	</div>
  </div>
  <div class="total-row subtotal container">
	<div class="label" style="font-size:0.9em;text-align: right;width: 100px;margin-left: 103px; font-family: fontsDosis; font-weight: 600; font-style: normal;">Order Total</div>
	<div class="value" style="text-align: center; margin-right: 125px;">
	  <span id="total_order_checkout" style="font-size:0.9em" class="sub">[@TOTAL_ORDER]</span>
	</div>
  </div>
</div>
</div>
<div class="basket-actions container" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
<ul class="actions-right pull-right">

<li class="action-checkout" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
			<a href="javascript:void(0);" data-link="[@LINK_TO]" id="link_check_out" class="btn btn-alt btn-lg btn-block">
			  checkout
			  <span class="arrow arrow-right"></span>
			</a>
		  </li>
	<a class="btn btn-link btn-shop" href="/">
	  <span class="arrow arrow-left"></span>
	  continue shopping
	</a>
  </li>
  <li class="basket-toggle" data-country="ca" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
	<div id="modal-ship-country-selector" class="modal modal-fixed">
	  <div class="modal-header">
		<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title text-center">Where do you want to ship?</h4>
		<h3 class="modal-subhead text-center alt-book">SELECT A COUNTRY.</h3>
	  </div>
	  <div class="modal-body text-center">
		<div class="country-select">
		  <button type="button" data-country="CA" data-msg="If you select Canadian shipping, you will be taken to the Canadian site.<br>
			Prices will be shown in
			<b>Canadian Dollars.</b>
			">
			<img src="/user_account/customer/templates/default/img/icon-flag-ca.png"></button>
		  <button type="button" data-country="US" data-msg="Continue with shipping to the US.">
			<img src="/user_account/customer/templates/default/img/icon-flag-us.png" width="60" height="40" class="flag-shadow"></button>
		  <p class="msg"></p>
		</div>
	  </div>

	  <div class="modal-actions text-center">
		<button type="button" class="btn btn-default btn-lg">Cancel</button>
		<button type="button" class="btn btn-primary btn-lg" disabled="disabled">
		  Select
		  <span class="arrow arrow-right"></span>
		</button>
	  </div>

	  <p class="foot-note">
		<b>Please Note:</b>
		Items that cannot be shipped to the location selected will be indicated in red. In order to check out, these items will need to be removed from your shipping shopping cart.
	  </p>

	</div>
	<div id="modal-ship-country-warning" class="modal modal-fixed">

	  <div class="modal-header">
		<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">
		  <span class="text-warning">NOTICE</span>
		</h4>
	  </div>

	  <div class="modal-body">
		<p>
		  One or more items in this shopping cart cannot be shipped to your location and will be removed.
		</p>
	  </div>

	  <div class="modal-footer">
		<button type="button" class="btn btn-default btn-lg">Cancel</button>
		<button type="button" class="btn btn-primary btn-lg">
		  Continue
		  <span class="arrow arrow-right"></span>
		</button>
	  </div>

	</div>
  </li>
</ul>
</div>
</form>
<form id="updateBasketForm" name="updateBasketForm" action="#" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
<input id="basketUpdateBasketBtn" style="display:none" name="/atg/commerce/order/purchase/CartModifierFormHandler.setOrder" value="update basket" type="submit">
</form>
<form id="removeForm" name="removeForm" action="/basket/basket.jsp?_DARGS=/basket/basketContent.jsp.removeForm" method="post">
<input id="basketRemoveBtn" style="display:none" name="/atg/commerce/order/purchase/CartModifierFormHandler.removeItemFromOrder" value="remove" type="submit">
</form>
<form id="moveToMyListForm" name="moveToMyListForm" action="/basket/basket.jsp?_DARGS=/basket/basketContent.jsp.moveToMyListForm" method="post">
<input id="basketMoveToMyListBtn" style="display:none" name="/astoria/commerce/profile/formhandler/astoriaMylistFormHandler.AddToMyListFromBasketPage" value="move to loves" type="submit">
</form>
[@LIST_BEAUTY_GRAB]
</div>
</div>
<div class="basket-footer" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;"></div>
</div>