<div id="sku_[@PRODUCT_ID]" data-skutype="[@PRODUCT_TYPE]" data-isbi="" data-bitype="None" data-productid="[@PRODUCT_id]" class="product-row container">
    <div class="product-image">
        <img width="42" height="42" src="[@PRODUCT_IMAGE]" alt="">
    </div>
    <div class="product-description">
        <h3>
            <div class="product-price">
									<span class="list-price">
										<span class="price ">[@PRODUCT_PRICE]</span>
									</span>
            </div>
								<span class="OneLinkNoTx">
									<span class="brand">[@BRAND_NAME]</span>
									<span class="product-name">[@PRODUCT_NAME]</span>
								</span>
        </h3>
        <div class="info-row">
								<span class="sku">
									<span class="label">Item #</span>
									<span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
								</span>
								<span class="qty">
									<span class="label">QTY</span>
									<span class="value">[@PRODUCT_QUANTITY]</span>
								</span>
        </div>
    </div>
</div>