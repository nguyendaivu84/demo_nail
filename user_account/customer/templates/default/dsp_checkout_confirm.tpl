<script>
    $(document).ready(function(){
        $(".btn_edit_shipping").on("click",function(){
            $('#shipping_information').hide();
            $("#shipping_contener").removeClass("module-content");
            $("#shipping_contener_child").removeClass("ship-form");

            $('#gift_contener').addClass("module-content");
            $('#payment_contener').hide();

            $('#payment_information').show();
            $('#gift_information').show();
        });
        $("#btn_save_ship_information").on("click",function(){
            var first_name = $("#first_name_shipping").val();
            var last_name = $("#last_name_shipping").val();
            var address1 = $("#address1_shipping").val();
            var address2 = $("#address2_shipping").val();
            var city = $("#city_shipping").val();
            var state = $("#state_shipping").val();
            var zip = $("#zip_shipping").val();
            var phone_1 = $("#phone_1_shipping").val();
            var phone_2 = $("#phone_2_shipping").val();
            var phone_3 = $("#phone_3_shipping").val();
            var phone_ext = $("#phone_ext_shipping").val();
            var shippingMethod = $("input:radio[name=shippingMethod]:checked").val();//$("#shippingMethod").val();
            var gift_check = $("#gift_check").val();
            var is_default = $("#is_default_shipping").is(":checked");
            if(is_default) is_default = 1; else is_default = 0;
            var data = { };
            data['first_name'] = first_name;
            data['last_name'] = last_name;
            data['address1'] = address1;
            data['address2'] = address2;
            data['city'] = city;
            data['state'] = state;
            data['zip'] = zip;
            data['phone_1'] = phone_1;
            data['phone_2'] = phone_2;
            data['phone_3'] = phone_3;
            data['phone_ext'] = phone_ext;
            data['is_default'] = is_default;
            data['shipping_method'] = shippingMethod;
            $("#change_ging").html(shippingMethod);
            data['gift_check'] = gift_check;
            update_shipping_information("[@URL_LINK]",JSON.stringify(data),1);
        });
        $("#continue").on("click",function(){
            var cc_first_name = $("#cc_first_name").val();
            var cc_last_name = $("#cc_last_name").val();
            var cc_number = $("#cc_number").val();
            var cc_expiration_month = $("#cc_expiration_month").val();
            var cc_expiration_year = $("#cc_expiration_year").val();
            var cc_code = $("#cc-code").val();
            var billing_address_same = $("input[name=billing_address_same]:checked").val();
            billing_address_same = parseInt(billing_address_same,10);
            var billing_address_1 = '';
            var billing_address_2 = '';
            var billing_city = '';
            var billing_state = '';
            var billing_zip = '';
            var billing_country = '';
            var billing_phone_1 = '';
            var billing_phone_2 = '';
            var billing_phone_3 = '';
            var save_cart = 0;
            var default_cart = 0;
            var billing_phone_ext = '';
            if(!billing_address_same){
                billing_address_1 = $("#billing_address_1").val();
                billing_address_2 = $("#billing_address_2").val();
                billing_city = $("#billing_city").val();
                billing_state = $("#billing_state").val();
                billing_zip = $("#billing_zip").val();
                billing_country = $("#billing_country").val();
                billing_phone_1 = $("#billing_phone_1").val();
                billing_phone_2 = $("#billing_phone_2").val();
                billing_phone_3 = $("#billing_phone_3").val();
                billing_phone_ext = $("#billing_phone_ext").val();
            }
            save_cart = $("#save_card").is(":checked");
            if(save_cart) save_cart = 1 ; else save_cart = 0;
            default_cart = $("#is_default_cart").is(":checked");
            if(default_cart) default_cart = 1 ; else default_cart = 0;
            var data = { };
            data['cc_first_name'] = cc_first_name;
            data['cc_last_name'] = cc_last_name;
            data['cc_number'] = cc_number;
            data['default_cart'] = default_cart;
            data['save_cart'] = save_cart;
            data['cc_expiration_month'] = cc_expiration_month;
            data['cc_expiration_year'] = cc_expiration_year;
            data['cc_code'] = cc_code;
            data['billing_address_same'] = billing_address_same;
            data['billing_address_1'] = billing_address_1;
            data['billing_address_2'] = billing_address_2;
            data['billing_city'] = billing_city;
            data['billing_state'] = billing_state;
            data['billing_zip'] = billing_zip;
            data['billing_country'] = billing_country;
            data['billing_phone_1'] = billing_phone_1;
            data['billing_phone_2'] = billing_phone_2;
            data['billing_phone_3'] = billing_phone_3;
            data['billing_phone_ext'] = billing_phone_ext;
            update_shipping_information("[@URL_LINK]",JSON.stringify(data),2);
        });
        $("#edit_shipping_address").on("click",function(){
            $("#first_name_shipping").val('');
            $("#last_name_shipping").val('');
            $("#address1_shipping").val('');
            $("#address2_shipping").val('');
            $("#city_shipping").val('');
            $("#state_shipping").val(0);
            $("#zip_shipping").val('');
            $("#phone_1_shipping").val('');
            $("#phone_2_shipping").val('');
            $("#phone_3_shipping").val('');
            $("#phone_ext_shipping").val('');
        });
        $(".btn_gift").on("click",function(){
            $('#gift_information').hide();
            $("#gift_contener").removeClass("module-content");
            $('#shipping_information').show();
            $('#payment_information').show();

            $('#shipping_contener').addClass("module-content");
            $('#payment_contener').hide();
        });
        $(".btn-payment").on("click",function(){
            $('#payment_information').hide();
            $("#payment_contener").show();

            $('#shipping_contener').addClass("module-content");
            $('#gift_contener').addClass("module-content");
            $('#gift_information').show();
            $('#shipping_information').show();
        });
        $("input[name=billing_address_same]").on("change",function(){
            var check = $(this).val();
            if(check == 0){
                $("#different-address").css("display","block");
            }else{
                $("#different-address").css("display","none");
            }
        });
        $("input[name=save_card]").on("change",function(){
            var check = $(this).is(":checked");
            if(check){
                $("#is_default_cart").prop("disabled",false);
            }else{
                $("#is_default_cart").prop("disabled",true);
            }
        });
    });
</script>
<div class="banner-container"> </div>
<div class="main" style=" font-family: fontsDosis; font-weight: 400; font-style: normal; font-size: 14px;">
    <div class="page-intro container">
        <h1 class="alt-book text-upper" style="text-transform:capitalize;font-weight: bold;">
          Checkout
        </h1>
    </div>
    <div id="shipping" class="checkout-module divided shipping summary">
        <div class="module-top">
            <h2 class="module-title">Ship To &amp; Delivery</h2>
        </div>
        <div id="shipping_information" class="module-summary container">
            <div class="module-left" data-shipping-id="">
            <h2 class="module-title">Ship To <span id="btn_shipping" data-type="shipping" class="btn btn-primary btn-sm btn-edit btn_edit_shipping">edit</span></h2>
            <div class="indent">
                <p class="default" >Default</p>
                <p class="OneLinkNoTx">
                 [@SHIPPING_INFO]
                </p>
                <p class="OneLinkNoTx">
                    [@SHIPPING_PHONE]
                </p>
            </div>
        </div>
            <div class="module-right">
            <h2 class="module-title">Delivery <span data-type="shipping" class="btn btn-primary btn-sm btn-edit btn_edit_shipping">edit</span></h2>
            <div class="indent">
                <p data-ship-method-summary=""><span id="change_ging">[@SHIPPING_METHOD]</span> Shipping</p>
            </div>
        </div>
        </div>
        <div id="shipping_contener" class="module-content container">
            <form novalidate="novalidate" name="shippingAccordion" action="" method="post">
                <div class="module-left">
                    <div id="shipping_contener_child" class="ship-form" style="">
                        <div class="form-group">
                            <label for="first_name_shipping"><span class="required">*</span> First name</label>
                            <input id="first_name_shipping" maxlength="33" name="first_name_shipping" value="[@FIRST_NAME]" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="last_name_shipping"><span class="required">*</span> Last name</label>
                            <input id="last_name_shipping" maxlength="33" name="last_name_shipping" value="[@LAST_NAME]" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="address1_shipping"><span class="required">*</span> Address line 1</label>
                            <input id="address1_shipping" maxlength="30" name="address1_shipping" value="[@ADDRESS_1]" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="address2_shipping">Address line 2</label>
                            <input id="address2_shipping" maxlength="30" name="address2_shipping" value="[@ADDRESS_2]" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="city_shipping"><span class="required">*</span> City</label>
                            <input id="city_shipping" maxlength="30" name="city_shipping" value="[@ADDRESS_CITY]" class="form-control city" type="text">
                        </div>
                        <div class="form-group">
                            <label for="state_shipping"><span class="required">*</span> Province</label>
                            <select id="state_shipping" style="" name="state_shipping" class="form-control form-control select-state cc_type">
                                <option value="0">select a province</option>
                                [@OPTION_STATE]
                            </select>
                        </div>
                        <div class="zip-country container form-group">
                            <label for="zip_shipping"><span class="required">*</span> Postal Code</label>
                            <div class="zip-code">
                                <input id="zip_shipping" maxlength="10" name="zip_shipping" value="[@POSTAL_CODE]" class="form-control zip" type="text">
                            </div>
                            <div class="country country-label">Country: Canada</div>
                        </div>
                        <div class="phone-controls container">
                            <div class="form-group">
                            <label for="phone_1_shipping"><span class="required">*</span> Phone</label>
                            <input id="phone_1_shipping" maxlength="3" pattern="*" name="phone_1_shipping" value="[@PHONE_1]" class="form-control phone-control" type="text">
                            <input id="phone_2_shipping" maxlength="3" pattern="*" name="phone_2_shipping" value="[@PHONE_2]" class="form-control phone-control" type="text">
                            <input id="phone_3_shipping" maxlength="4" pattern="*" name="phone_3_shipping" value="[@PHONE_3]" class="form-control phone-control" type="text">
                            </div>
                            <div class="form-group phone-ext">
                                <label for="phone_ext_shipping">Ext.</label>
                                <input id="phone_ext_shipping" maxlength="10" pattern="*" name="phone_ext_shipping" value="[@PHONE_EXT]" class="form-control phone-control" type="text">
                            </div>
                        </div>
                    </div>
                    <label class="set-default checkbox">
                        <input id="is_default_shipping" name="is_default_shipping" value="true" type="checkbox" checked>
                        Set as my default shipping address
                    </label>
                    <div class="delete-address" style="display:none">
                        <a id="clear_address" href="javascript:void(0)" class="btn btn-default btn-sm">Clear address</a>
                    </div>
                    <div class="edit-address-btn" style="display:block">
                        <button type="button" class="btn btn-default btn-sm btn-edit" id="edit_shipping_address" data-address_id="">Clear address</button>
                        <input type="reset" style="display: none" id="reset_btn" />
                    </div>
                    <div class="switch"><a href="#"></a></div>
                    <p class="required-label">* Required</p>
                </div>
                <div class="module-right">
                    <h2 class="module-title">Delivery</h2>
                    <div class="indent">
                    <div class="warning-message not-allowed-shipping-msg" style="display:none;">
                        <p>The selected delivery method is not valid with the promotion code &lt;promo code&gt;. You must select &lt;valid shipping methods&gt; to qualify. If you continue, the shipping promotion will be removed.</p>
                    </div>
                    <div class="delivery-method">
                        [@OPTION_METHOD_SHIPPING]
                        <p>* Handling fees may apply<br><br> </p>
                        <p>
                            <a rel="#pop-shipping" class="btn btn-default btn-sm pop-info" href="#">Shipping &amp; Handling Information</a>
                        </p>
                    </div>
                </div>
            </div>
                <div class="" style="float:right;padding-top: 50px;">
                    <button id="btn_save_ship_information" class="btn btn-alt btn-lg btn-continue" value="continue" name="continue" type="button">
                        Save Change <span class="arrow arrow-right"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="payment" class="checkout-module divided payment current">
        <div class="module-top"><h2 class="module-title">Payment &amp; Promotions</h2></div>
        <div id="payment_information" class="module-summary container">
            <div class="module-left">
            <h2 class="module-title">Payment <span data-type="payment" class="btn-payment btn btn-primary btn-sm btn-edit">edit</span></h2>
            <div class="indent">
                    None
            </div>
            </div>
        </div>
        <div id="payment_contener" class="module-content container">
        <form novalidate="novalidate" name="paymentAccordion" action="" method="post">
        <div class="module-left">
          <h2 class="module-title" data-before-errors="true">Payment</h2>
          <div class="indent">
            <p class="cc-types">
                <img src="/user_account/customer/templates/default/img/icon-visa.png" alt="VISA" width="33" height="21">
                <img src="/user_account/customer/templates/default/img/icon-mastercard.png" alt="MasterCard" width="33" height="21">
                <img src="/user_account/customer/templates/default/img/icon-amex.png" alt="American Express" width="33" height="21">
            </p>
            <div class="new-card-form">
            <div class="new-credit-card-info" style="display:block">
                <div class="cc-name">
                    <div class="form-group">
                        <label for="cc_firstName"><span class="required">*</span> First name</label>
                        <input id="cc_first_name" maxlength="33" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.firstName" value="" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.firstName" value=" " type="hidden"></div>
                    <div class="form-group">
                        <label for="cc_lastName"><span class="required">*</span> Last name</label>
                        <input id="cc_last_name" maxlength="33" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.lastName" value="" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.lastName" value=" " type="hidden"></div>
                    </div>
                    <div class="cc-info">
                    <input id="cc_type" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardType" value="" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardType" value=" " type="hidden"><div class="cc-info-field form-group">
                        <label for="cc_number"><span class="required">*</span> Card number</label>
                        <input id="cc_number" maxlength="20" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardNumber" value="" class="form-control cc_number" type="text" autocomplete="off"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardNumber" value=" " type="hidden"></div>
                    <div class="cc-info-field expiration-date form-group" style='display: block'>
                        <label for="cc_expirationMonth"><span class="required">*</span> Expiration date</label>
                       <select id="cc_expiration_month" name="cc_expiration_month" class="form-control cc-expirationMonth">
                           <option value="">month</option>
                           [@MONTH_OPTION]
                       </select>
                        <select id="cc_expiration_year" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.expirationYear" class="form-control cc-expirationYear">
                            <option value="">year </option>
                            [@YEAR_OPTION]
                        </select>
                    </div>
                    <div class="sec-code container form-group" style='display: block'>
                        <label for="cc-code"><span class="required">*</span> Security code</label>
                        <input id="cc-code" maxlength="4" name="cc_code" value="" class="form-control cc-code" type="text" autocomplete="off"><input name="_D:cc_code" value=" " type="hidden"><a href="#" class="icon icon-help" rel="#pop-sec-code"></a>
                    </div>
                </div>
            </div>
            <div class="billing-address container" style="display:block">
                <input id='hasastoriaHardgoodShippingGroup' value='true' type="hidden"/>
        <h3>Billing Address</h3>
        <label class="radio" id="billing-same-div">
             <input id="billing-same" name="billing_address_same" value="1" type="radio" checked>
             My shipping &amp; billing addresses are the same.
        </label>
        <div class="use-different">
          <label class="radio">
              <input id="billing-different" name="billing_address_same" value="0" type="radio">
              Use a different billing address.
          </label>
            <div class="different-address" id="different-address" style="display:none">
                <div class="new-address-form">
                    <div class="form-group">
                          <label for="billing_address_1"><span class="required">*</span> Address line 1</label>
                          <input id="billing_address_1" maxlength="30" name="billing_address_1" value="" class="form-control address " type="text">
                    </div>
                      <div class="form-group">
                          <label for="billing_address_2">Address line 2</label>
                          <input id="billing_address_2" maxlength="30" name="billing_address_2" value="" class="form-control address " type="text">
                      </div>
                      <div class="form-group">
                          <label for="billing_city"><span class="required">*</span> City</label>
                          <input id="billing_city" maxlength="30" name="billing_city" value="" class="form-control city " type="text">
                      </div>
                      <div class="form-group">
                          <label for="billing_state"><span class="required">*</span> <span>Province</span></label>
                          <select id="billing_state" style="" name="billing_state" class="form-control form-control select-state state ">
                              <option selected value="0">select a province</option>
                              [@STATE_OPTION]
                          </select>
                      </div>
                        <div class="zip-country container form-group">
                          <div class="zip-code">
                            <label for="billing_zip"><span class="required">*</span> <span>Zip Code</span></label>
                            <input id="billing_zip" maxlength="10" name="billing_zip" value="" class="form-control zip " type="text">
                        </div>
                        <div class="country">
                            <label for="billing_country">Country</label>
                            <select id="billing_country" name="billing_country" class="form-control ">
                                <option value="">select a country</option>
                                [@COUNTRY_OPTION]
                            </select>
                        </div>
                        </div>
                    <div class="phone-controls container">
                        <div class="form-group">
                            <label for="billing_phone_1"><span class="required">*</span> Phone</label>
                            <div id="isNotOnePhoneField">
                                <input id="billing_phone_1" maxlength="3" pattern="*" name="billing_phone_1" value="" class="form-control phone-control " type="text">
                                <input id="billing_phone_2" maxlength="3" pattern="*" name="billing_phone_2" value="" class="form-control phone-control " type="text">
                                <input id="billing_phone_3" maxlength="4" pattern="*" name="billing_phone_3" value="" class="form-control phone-control " type="text">
                                <input id="billing_phone_ext" maxlength="15" pattern="*" name="billing_phone_ext" value="" class="form-control phone-control  " type="text">
                            </div>
                            </div>
                            <div class="form-group phone-ext" style="display: none;" >
                                <label for="billing_phone_ext">Ext.</label>
                                <input id="billing_phone_ext" maxlength="10" pattern="*" name="billing_phone_ext" value="" class="form-control phone-control " type="text">
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <label class="save-card checkbox">
            <input name="save_card" id="save_card" type="checkbox">
            Save this card for future purchases
         </label>
           <label class="make-default checkbox">
             <input id="is_default_cart" name="is_default_cart" value="true" type="checkbox" disabled>
             Make this my default credit card
           </label>
         </div>

    </div>
          <p class="required-label">* Required</p>
          </div>
          <p class="payment-options"><a href="#" class="pop-info" rel="#pop-billing">billing and payment options</a></p>
        </div>
        <!-- div class="module-right">
            <div class="indent">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHTwYJKoZIhvcNAQcEoIIHQDCCBzwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAX9Ltm8Np11QsKDYk+fF/lGBSrh3GqQO/gQWoO38w/iP7tgc7N+m7QWIF2a0fOn1VB2d4/MDVq3ftqt8zO7umat6GDXQm/0I2MCuC79MnuCeArVYahrbMa37nee87+pvoRoDXvoKRdlTmos+ITa+xOLfU/Wu8jv9iByBCZX7vefzELMAkGBSsOAwIaBQAwgcwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIl71ppyl6BjyAgah5QcyamDo3VSvU7oyVgb1SierIp+9Zi98O+hV8CeKsWtINAozEKke9oC1d/2LTTOnMOsd1JLCOWJZwIm7Xb9rR70niyWi63C51vRfWGjhU9Hc7OH8SyFjRNH43LXoNFEFKBFqq4jhY1ucnE20XVgi6btD8Xrdkuf3J2aseKP/ujKcrphrUBhzZZ302A1ObPud2jHAlAClFmQMPyEhEIAcJ3exXGTnVL2mgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xNDA2MDkwMzI1MzBaMCMGCSqGSIb3DQEJBDEWBBTArTeJXmnahfTABA5p/aTRSJCowjANBgkqhkiG9w0BAQEFAASBgKHOCEWuIfKouqOQzYljVLLqRmi1DRyh65ee1jIZUiS0P3vTXbLO2/AEFM5yYE5mO15tRm1pqgxsnQtADJx3OKy+VeB94aYTrOSGxGgolu+2Y2prVo8fYbbPCJ0wq4c1Ql5PvDuxuZ05JH1bGvcuKPWncZq1Uvdf/TUHJ8ZzHUTf-----END PKCS7-----
">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
        </div-->
    <input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.basketPage" value="/basket/basket.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.basketPage" value=" " type="hidden"><div class="module-action">
    <button class="btn btn-alt btn-lg btn-continue" value="continue" name="continue" id="continue" type="submit">continue <span class="arrow arrow-right"></span></button>
    </div>
    <input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.addOrUpdate" value="continue" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.addOrUpdate" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.useForwards" value="true" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.useForwards" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.updatePaymentErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.updatePaymentErrorURL" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.newCreditCardErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.newCreditCardErrorURL" value=" " type="hidden"><input name="_DARGS" value="/checkout/updateCreditCard/checkout-payment.jsp" type="hidden"></input></form>
        </div>
    </div>
</div>
<div class="checkout-sidebar" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
<div class="my-basket" style="margin-top:40px">
    <h2 class="module-title">
        My Shopping cart
        <a class="btn btn-primary btn-sm btn-back" href="/basket/basket.jsp">back</a>
    </h2>
    <span id="currency" data-currency="C$"></span>
    <div class="my-basket-content">
        <div class="product-list custom-scroll">
            <div class="products" style="display: block;">
                [@LIST_BASKET_ITEM]
            </div>
        </div>
    </div>
    <div class="calculation">
        <ul class="container" style="font-size: 14px">
            <li>
                <span class="line-item">Merchandise Subtotal</span>
                <span class="currency">C$</span>
                <span class="cost merch-total">[@BASKET_PRICE]</span>
            </li>
            <li>
                <span class="line-item">Taxes</span>
                <span class="currency">C$</span>
                <span class="cost tax">[@TAX_PRICE]</span>
            </li>
            <li class="total">
                <span class="line-item">Order Total</span>
                <span class="currency">C$</span>
                <span class="cost total">[@TOTAL_PRICE]</span>
            </li>
        </ul>
    </div>
</div>
</div>
</div>