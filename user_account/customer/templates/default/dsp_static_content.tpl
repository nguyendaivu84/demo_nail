<link rel="stylesheet" href="[@URL_CUSTOMER]/css/reset.css">
<link rel="stylesheet" href="[@URL_CUSTOMER]/css/style.css">
<div class="main_ct">
    <ul class="breadcrumb">
        <li>[@DESCRIPTION]</li>
    </ul>
    <section class="sidebar">
        <div class="sidenav">
            <h2 class="nav-title">customer service help</h2>
            <h3 class="nav-subhead">Shopping astoria.com</h3>
            <ul class="nav nav-stacked">
                <li><a href="#">Finding Products</a></li>
                <li><a href="#">Beauty Advice &amp; Reviews</a></li>
                <li><a href="#">My Beauty Bag</a></li>
                <li><a href="#">Loves (Shopping List)</a></li>
                <li><a href="#">astoria inside JCPenney</a></li>
                <li><a href="#">International Websites</a></li>
            </ul>
            <h3 class="nav-subhead">Shopping astoria.com</h3>
            <ul class="nav nav-stacked">
                <li><a href="#">Finding Products</a></li>
                <li><a href="#">Beauty Advice &amp; Reviews</a></li>
                <li><a href="#">My Beauty Bag</a></li>
                <li><a href="#">Loves (Shopping List)</a></li>
                <li><a href="#">astoria inside JCPenney</a></li>
                <li><a href="#">International Websites</a></li>
            </ul>
        </div>
    </section>
    <div id="main" class="maincontent">
        <div class="html-component">
            [@STATIC_CONTENT]
        </div>
    </div>
    <p class="clear"></p>
</div>