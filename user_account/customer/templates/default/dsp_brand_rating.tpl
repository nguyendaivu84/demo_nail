<div class="tab-pane fade active in" id="bv-ratings">
	<div id="BVReviewsContainer" class="BVBrowserWebkit">
		<div id="BVRRWidgetID" class="BVRRRootElement BVRRWidget">
			<div id="BVRRContentContainerID" class="BVRRContainer" style="">
				<div class="BVRRSection BVRRQuickTakeSection" id="BVRRQuickTakeSectionID">
					<div class="BVRRHeader BVRRQuickTakeHeader" id="BVRRQuickTakeHeaderID">
						<span class="BVRRTitle BVRRQuickTakeTitle">Summary of Customer Ratings &amp; Reviews</span>
						<span class="BVRRQuickTakeHeaderHelpText">
							Check the boxes above to
							<span class="BVRRQuickTakeHeaderHelpTextKeyword BVRRQuickTakeHeaderHelpTextFilter">filter reviews</span>
						</span>
					</div>
					<div class="BVRRSpacer BVRRQuickTakeSpacer"></div>
					<table class="BVRRContent BVRRQuickTakeContent" id="BVRRQuickTakeContentID">
						<tbody>
							<tr class="BVRRQuickTakeTableRow BVRRQuickTakeSummaryRow">
								<td rowspan="2" class="BVRRQuickTakeSummary BVRRQuickTakeSummaryOneCloud" id="BVRRQuickTakeSummaryID">
									<div id="BVRRCustomQuickTakeRatingSummaryRatingContainer" class="BVRRRatingOverall">
										<div id="BVRRCustomQuickTakeRatingSummaryOverallLabel" class="BVRRLabel BVRRRatingNormalLabel">Overall Rating</div>
										<div id="BVRRCustomQuickTakeRatingSummaryOverallImage" class="BVRRRatingNormalImage">
											<img src="http://reviews.astoria.com/8723illuminate/4_7/5/rating.gif" alt="4.7 out of 5"></div>
										<div class="BVRRRatingNormalOutOf">
											<div id="BVRRCustomQuickTakeOutOfSentenceBlock">
												<span id="BVRRCustomQuickTakeRatingSummaryOverallRating" class="BVRRNumber BVRRRatingNumber">(4.7</span>
												<span id="BVRRCustomQuickTakeRatingSummaryOverallRatingSeparator" class="BVRRSeparatorText">out of</span>
												<span id="BVRRCustomQuickTakeRatingSummaryOverallRatingRange" class="BVRRNumber BVRRRatingRangeNumber">5)</span>
											</div>
											<span id="BVRRCustomQuickTakeRatingSummaryHistogram">
												<div id="BVRRRatingsHistogramButton_esocax29qhc0424svru3r3qnf_ID" class="BVRRRatingsHistogramButton">
													<div id="BVRRRatingsHistogramButtonScript_a4atqiojiv4aqmffx6b6rnejc_ID" class="BVRRRatingsHistogramButtonScript">
														<img src="http://reviews.astoria.com/static/8723illuminate/openRatingsHistogram.gif" alt="Open Ratings Snapshot" name="BV_TrackingTag_QuickTakeSummary_ExpandHistogram_P7411" class="BVRRRatingsHistogramButtonImage" onmouseover="bvHistogramMouseover(this, 'BVRRHistogramTimer_b2enojycsgssyy8m5u7rz0bz8_ID', 'BVRRRatingsHistogramButtonPopin_miyxreykm915chz3f9gh7du9r_ID', 'RatingsHistogramFrame');" onmouseout="bvHistogramMouseout('BVRRHistogramTimer_b2enojycsgssyy8m5u7rz0bz8_ID', 'BVRRRatingsHistogramButtonPopin_miyxreykm915chz3f9gh7du9r_ID', 1000);">
														<div id="BVRRRatingsHistogramButtonPopin_miyxreykm915chz3f9gh7du9r_ID" class="BVRRRatingsHistogramButtonPopin">
															<div class="BVRRHistogram">
																<div class="BVRRHistogramTitle">
																	<span class="BVRRCount BVRRNonZeroCount">
																		<span class="BVRRNumber">1,122</span>
																		reviews
																	</span>
																</div>
																<div class="BVRRHistogramContent">
																	<div class="BVRRHistogramBarRow BVRRHistogramBarRow5">
																		<span class="BVRRHistStarLabel BVRRHistStarLabel5">
																			<span class="BVRRHistStarLabelText">5 stars</span>
																		</span>
																		<div class="BVRRHistogramBar">
																			<div style="width: 80%" class="BVRRHistogramFullBar"></div>
																		</div>
																		<span class="BVRRHistAbsLabel">893</span>
																	</div>
																	<div class="BVRRHistogramBarRow BVRRHistogramBarRow4">
																		<span class="BVRRHistStarLabel BVRRHistStarLabel4">
																			<span class="BVRRHistStarLabelText">4 stars</span>
																		</span>
																		<div class="BVRRHistogramBar">
																			<div style="width: 13%" class="BVRRHistogramFullBar"></div>
																		</div>
																		<span class="BVRRHistAbsLabel">144</span>
																	</div>
																	<div class="BVRRHistogramBarRow BVRRHistogramBarRow3">
																		<span class="BVRRHistStarLabel BVRRHistStarLabel3">
																			<span class="BVRRHistStarLabelText">3 stars</span>
																		</span>
																		<div class="BVRRHistogramBar">
																			<div style="width: 3%" class="BVRRHistogramFullBar"></div>
																		</div>
																		<span class="BVRRHistAbsLabel">38</span>
																	</div>
																	<div class="BVRRHistogramBarRow BVRRHistogramBarRow2">
																		<span class="BVRRHistStarLabel BVRRHistStarLabel2">
																			<span class="BVRRHistStarLabelText">2 stars</span>
																		</span>
																		<div class="BVRRHistogramBar">
																			<div style="width: 2%" class="BVRRHistogramFullBar"></div>
																		</div>
																		<span class="BVRRHistAbsLabel">22</span>
																	</div>
																	<div class="BVRRHistogramBarRow BVRRHistogramBarRow1">
																		<span class="BVRRHistStarLabel BVRRHistStarLabel1">
																			<span class="BVRRHistStarLabelText">1 star</span>
																		</span>
																		<div class="BVRRHistogramBar">
																			<div style="width: 2%" class="BVRRHistogramFullBar"></div>
																		</div>
																		<span class="BVRRHistAbsLabel">25</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<noscript>
														&amp;lt;div class="BVRRRatingsHistogramButtonImage"&amp;gt;&amp;lt;a name="BV_TrackingTag_QuickTakeSummary_ExpandHistogram_P7411" target="_blank" href="http://reviews.astoria.com/8723illuminate/P7411/ratingsnapshot.htm"&amp;gt; &amp;lt;img src="http://reviews.astoria.com/static/8723illuminate/openRatingsHistogram.gif" alt="Open Ratings Snapshot" /&amp;gt;
&amp;lt;/a&amp;gt;&amp;lt;/div&amp;gt;
													</noscript>
												</div>
											</span>
										</div>
									</div>
									<div id="BVRRCustomQuickTakeLinkComponent">
										<div class="BVRRRatingSummaryLinks">
											<div id="BVRRRatingSummaryLinkWriteID" class="BVRRRatingSummaryLink BVRRRatingSummaryLinkWrite">
												<span class="BVRRRatingSummaryLinkWritePrefix"></span>
												<a title="Write a review" name="BV_TrackingTag_QuickTakeSummary_WriteReview_P7411" target="BVFrame" onclick="bvShowContentOnReturnPRR('8723illuminate', 'P7411', 'BVRRWidgetID');" href="http://reviews.astoria.com/8723illuminate/P7411/writereview.htm?format=embedded&amp;campaignid=BV_RATING_SUMMARY&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp">Write a review</a>
												<span class="BVRRRatingSummaryLinkWriteSuffix"></span>
											</div>
										</div>
									</div>
								</td>
								<td>
									<table>
										<tbody>
											<tr>
												<td class="BVDI_AFAttributeFilteringSectionCell" colspan="1">
													<div class="BVRRSpacer BVDI_AFAttributeFilteringBeforeSpacer"></div>
													<div class="BVDI BVDI_AF" id="BVRRAttributeFilteringSectionID">
														<div class="BVDIHeader BVDI_AFHeader" id="BVRRAttributeFilteringHeaderID">Filter Reviews by:</div>
														<div class="BVDIBody BVDI_AFBody" id="BVRRAttributeFilteringContentID">
															<div id="BVRRFilterAttribute1ID" class="BVDI_AFFilterAttribute BVDI_AFFilterAttributeFirst">
																<div class="BVDI_AFFilterAttributeHeader" data-bvname="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_skinType_Header" onclick="bvTriggerAttributeContent('BVRR', 'BVDI_AF', 1, bvrrFilteringMouseout);return false;">Skin Type</div>
																<div id="BVRRFilterAttributeContent1ID" class="BVDI_AFFilterAttributeContent BVDI_AFFilterAttributeSkinTypeContent BVDI_AFHidden">
																	<div class="BVDI_AFFilterAttributeClose">
																		<a href="#" class="BVDI_AFFilterAttributeCloseLink" id="BVRRFilterAttributeCloseLink1ID" onclick="bvHideAllAttributeContent('BVRR', 'BVDI_AF');return false;" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_skinType_Close">
																			Close
																			<img src="http://reviews.astoria.com/static/8723illuminate/filterOnCustom.gif" alt="" title=""></a>
																	</div>
																	<ul id="BVRRFilterAttributeSkinTypeListID" class="BVDI_AFAttributeFilteringList">
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinType0ID" data-bvtrack="filterName:SkinType" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=normal&amp;expandcontextdatadimensionfilter=skinType', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinTypeLabel">
																				<a title="Normal" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_skinType_EnableFilter_Normal" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinType', 0, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=normal&amp;expandcontextdatadimensionfilter=skinType">Normal</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinType1ID" data-bvtrack="filterName:SkinType" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=combination&amp;expandcontextdatadimensionfilter=skinType', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinTypeLabel">
																				<a title="Combination" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_skinType_EnableFilter_Combination" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinType', 1, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=combination&amp;expandcontextdatadimensionfilter=skinType">Combination</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinType2ID" data-bvtrack="filterName:SkinType" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=dry&amp;expandcontextdatadimensionfilter=skinType', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinTypeLabel">
																				<a title="Dry" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_skinType_EnableFilter_Dry" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinType', 2, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=dry&amp;expandcontextdatadimensionfilter=skinType">Dry</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinType3ID" data-bvtrack="filterName:SkinType" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=oily&amp;expandcontextdatadimensionfilter=skinType', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinTypeLabel">
																				<a title="Oily" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_skinType_EnableFilter_Oily" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinType', 3, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_skinType=oily&amp;expandcontextdatadimensionfilter=skinType">Oily</a>
																			</span>
																		</li>
																	</ul>
																</div>
															</div>
															<div id="BVRRFilterAttribute2ID" class="BVDI_AFFilterAttribute BVDI_AFFilterAttributeLast">
																<div class="BVDI_AFFilterAttributeHeader" data-bvname="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_Header" onclick="bvTriggerAttributeContent('BVRR', 'BVDI_AF', 2, bvrrFilteringMouseout);return false;">Age</div>
																<div id="BVRRFilterAttributeContent2ID" class="BVDI_AFFilterAttributeContent BVDI_AFFilterAttributeAgeContent BVDI_AFHidden">
																	<div class="BVDI_AFFilterAttributeClose">
																		<a href="#" class="BVDI_AFFilterAttributeCloseLink" id="BVRRFilterAttributeCloseLink2ID" onclick="bvHideAllAttributeContent('BVRR', 'BVDI_AF');return false;" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_Close">
																			Close
																			<img src="http://reviews.astoria.com/static/8723illuminate/filterOnCustom.gif" alt="" title=""></a>
																	</div>
																	<ul id="BVRRFilterAttributeAgeListID" class="BVDI_AFAttributeFilteringList">
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge0ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=13to17&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																				<a title="13-17" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_EnableFilter_13-17" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 0, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=13to17&amp;expandcontextdatadimensionfilter=age">13-17</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge1ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=18to24&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																				<a title="18-24" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_EnableFilter_18-24" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 1, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=18to24&amp;expandcontextdatadimensionfilter=age">18-24</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge2ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=25to34&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																				<a title="25-34" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_EnableFilter_25-34" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 2, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=25to34&amp;expandcontextdatadimensionfilter=age">25-34</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge3ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=35to44&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																				<a title="35-44" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_EnableFilter_35-44" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 3, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=35to44&amp;expandcontextdatadimensionfilter=age">35-44</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge4ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=45to54&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																				<a title="45-54" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_EnableFilter_45-54" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 4, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=45to54&amp;expandcontextdatadimensionfilter=age">45-54</a>
																			</span>
																		</li>
																		<li>
																			<input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge5ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=over54&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																			<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																				<a title="over 54" name="BV_TrackingTag_QuickTake_Summary_P7411_AttributeFilter_age_EnableFilter_over 54" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 5, false);" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;contextdatavalue_age=over54&amp;expandcontextdatadimensionfilter=age">over 54</a>
																			</span>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
													<div class="BVRRSpacer BVDI_AFAttributeFilteringAfterSpacer"></div>
												</td>
											</tr>
											<tr>
												<td class="BVRRQuickTakeTags BVRRQuickTakeProTags BVRRQuickTakeProTagsSummaryOneCloud" id="BVRRQuickTakeProTagsID">
													<div id="BVRRQuickTakeProContainerID" class="BVRRQuickTakeContainer BVRRQuickTakeProContainer">
														<div class="BVRRLabel BVRRQuickTakeLabel">Pros</div>
														<div class="BVRRValue BVRRQuickTakeValue" id="RRQuickTakeProTagsValuesID">
															<ul class="BVRRQuickTakeList">
																<li class="BVRRTag BVRRTag15 BVRRTagFilter">
																	<a title="See 566 reviews that were marked as 'lightly scented'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_lightly scented" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=lightly%20scented">lightly scented</a>
																	<span class="BVRRNote">(566)</span>
																</li>
																<li class="BVRRTag BVRRTag11 BVRRTagFilter">
																	<a title="See 416 reviews that were marked as 'scented'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_scented" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=scented">scented</a>
																	<span class="BVRRNote">(416)</span>
																</li>
																<li class="BVRRTag BVRRTag7 BVRRTagFilter">
																	<a title="See 253 reviews that were marked as 'rich'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_rich" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=rich">rich</a>
																	<span class="BVRRNote">(253)</span>
																</li>
																<li class="BVRRTag BVRRTag2 BVRRTagFilter">
																	<a title="See 74 reviews that were marked as 'absorbs quickly'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_absorbs quickly" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=absorbs%20quickly">absorbs quickly</a>
																	<span class="BVRRNote">(74)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 16 reviews that were marked as 'moisturizing'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_moisturizing" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=moisturizing">moisturizing</a>
																	<span class="BVRRNote">(16)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 7 reviews that were marked as 'light'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_light" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=light">light</a>
																	<span class="BVRRNote">(7)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 6 reviews that were marked as 'clean'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_clean" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=clean">clean</a>
																	<span class="BVRRNote">(6)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 5 reviews that were marked as 'amazing'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_amazing" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=amazing">amazing</a>
																	<span class="BVRRNote">(5)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 4 reviews that were marked as 'awesome'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_awesome" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=awesome">awesome</a>
																	<span class="BVRRNote">(4)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 4 reviews that were marked as 'clean and fresh'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_clean and fresh" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=clean%20and%20fresh">clean and fresh</a>
																	<span class="BVRRNote">(4)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 4 reviews that were marked as 'long lasting'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_long lasting" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=long%20lasting">long lasting</a>
																	<span class="BVRRNote">(4)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 4 reviews that were marked as 'non-greasy'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_non-greasy" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=non-greasy">non-greasy</a>
																	<span class="BVRRNote">(4)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 3 reviews that were marked as 'fast absorbing'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_fast absorbing" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=fast%20absorbing">fast absorbing</a>
																	<span class="BVRRNote">(3)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 3 reviews that were marked as 'firming'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_firming" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=firming">firming</a>
																	<span class="BVRRNote">(3)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 3 reviews that were marked as 'fresh'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_fresh" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=fresh">fresh</a>
																	<span class="BVRRNote">(3)</span>
																</li>
															</ul>
															<ul class="BVRRQuickTakeListMore" id="BVRRQuickTakeMoreProTagsP7411ID">
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 3 reviews that were marked as 'hydrating'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_hydrating" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=hydrating&amp;expandtagdimensionfilter=Pro">hydrating</a>
																	<span class="BVRRNote">(3)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 3 reviews that were marked as 'not greasy'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_not greasy" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=not%20greasy&amp;expandtagdimensionfilter=Pro">not greasy</a>
																	<span class="BVRRNote">(3)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 3 reviews that were marked as 'silky'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_silky" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=silky&amp;expandtagdimensionfilter=Pro">silky</a>
																	<span class="BVRRNote">(3)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'fabulous'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_fabulous" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=fabulous&amp;expandtagdimensionfilter=Pro">fabulous</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'fresh and clean'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_fresh and clean" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=fresh%20and%20clean&amp;expandtagdimensionfilter=Pro">fresh and clean</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'great moisturizer'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_great moisturizer" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=great%20moisturizer&amp;expandtagdimensionfilter=Pro">great moisturizer</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'not oily'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_not oily" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=not%20oily&amp;expandtagdimensionfilter=Pro">not oily</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'not overpowering'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_not overpowering" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=not%20overpowering&amp;expandtagdimensionfilter=Pro">not overpowering</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'smells gorgeous on my mother.'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_smells gorgeous on my mother." target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=smells%20gorgeous%20on%20my%20mother.&amp;expandtagdimensionfilter=Pro">smells gorgeous on my mother.</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'smells great'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_smells great" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=smells%20great&amp;expandtagdimensionfilter=Pro">smells great</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'smooth'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_smooth" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=smooth&amp;expandtagdimensionfilter=Pro">smooth</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'super moisturizing'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_super moisturizing" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=super%20moisturizing&amp;expandtagdimensionfilter=Pro">super moisturizing</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See 2 reviews that were marked as 'yummy'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_yummy" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=yummy&amp;expandtagdimensionfilter=Pro">yummy</a>
																	<span class="BVRRNote">(2)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See review that was marked as 'a little goes a long way'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_a little goes a long way" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=a%20little%20goes%20a%20long%20way&amp;expandtagdimensionfilter=Pro">a little goes a long way</a>
																	<span class="BVRRNote">(1)</span>
																</li>
																<li class="BVRRTag BVRRTag0 BVRRTagFilter">
																	<a title="See review that was marked as 'a natural clean smell- timeless'" name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_a natural clean smell- timeless" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;tag_Pro=a%20natural%20clean%20smell-%20timeless&amp;expandtagdimensionfilter=Pro">a natural clean smell- timeless</a>
																	<span class="BVRRNote">(1)</span>
																</li>
															</ul>
														</div>
														<div class="BVRRQuickTakeMore BVRRQuickTakeProTagsMore">
															<a name="BV_TrackingTag_QuickTake_Summary_P7411_Pro_TagFilter_ToggleSeeMore" class="BVRRQuickTakeMorelink" onclick="bvTriggerMoreFilteringItems('BVRRQuickTakeMoreProTagsP7411ID', 'See All', 'See Fewer', this);return false;" href="#">See All</a>
														</div>
														<div class="BVRRSpacer BVRRQuickTakeSpacer"></div>
														<div id="BVRRQuickTakeFilterSentenceID" class="BVRRTagFilterSentence">
															Check the boxes above to
															<span class="BVRRQuickTakeFooterHelpTextKeyword BVRRQuickTakeFooterHelpTextFilter">filter reviews</span>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>

							</tr>
						</tbody>
					</table>
				</div>
				<div id="BVRRDisplayContentID" class="BVRRDisplayContent">
					<div id="BVRRDisplayContentHeaderID" class="BVRRHeader BVRRDisplayContentHeader">
						<div class="BVRRDisplayContentHeaderContent">
							<span id="BVRRDisplayContentTitleID" class="BVRRTitle BVRRDisplayContentTitle"></span>
							<span id="BVRRDisplayContentSubtitleID" class="BVRRSubtitle BVRRDisplayContentSubtitle">
								<span id="BVRRDisplayContentLinkWriteID" class="BVRRContentLink BVRRDisplayContentLinkWrite">
									<a title="review this product" name="BV_TrackingTag_Review_Display_WriteReview" target="BVFrame" onclick="bvShowContentOnReturnPRR('8723illuminate', 'P7411', '');" href="http://reviews.astoria.com/8723illuminate/P7411/writereview.htm?format=embedded&amp;campaignid=BV_REVIEW_DISPLAY&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp">review this product</a>
								</span>
							</span>
							<span class="BVRRSortAndSearch">
								<span id="BVRRDisplayContentSortID" class="BVRRDisplayContentSort">
									<span id="BVRRDisplayContentSortPrefixID" class="BVRRLabel BVRRDisplayContentSortPrefix">sort by</span>
									<span class="BVRRSortSelectWidget">
										<select id="BVRRDisplayContentSelectBVFrameID" name="BV_TrackingTag_Review_Display_Sort" class="BVRRSelect BVRRDisplayContentSelect" onchange="if(window.BVAnalyticsTracker){BVAnalyticsTracker.fireActionEvent(this);};$BV.Internal.Requester.get(bvGetSelectListValue($bv('#BVRRDisplayContentSelectBVFrameID')[0]),'BVFrame',''); return false;">
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded" selected="selected" id="default-desc">Choose a sort order</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=submissionTime" id="submissionTime-desc">Date - Newest First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=submissionTime&amp;dir=asc" id="submissionTime-asc">Date - Oldest First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=rank" id="rank-desc">Expert Reviews First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=affiliation" id="affiliation-desc">Staff Reviews First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=contributorRank&amp;dir=asc" id="contributorRank-asc">Top Contributors First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=hasPhoto" id="hasPhoto-desc">Photo Reviews First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=hasVideo" id="hasVideo-desc">Video Reviews First</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=rating" id="rating-desc">Rating - High to Low</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=rating&amp;dir=asc" id="rating-asc">Rating - Low to High</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=helpfulness" id="helpfulness-desc">Helpfulness - High to Low</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=helpfulness&amp;dir=asc" id="helpfulness-asc">Helpfulness - Low to High</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=reviewTextLength" id="reviewTextLength-desc">Length - Long to Short</option>
											<option value="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;sort=reviewTextLength&amp;dir=asc" id="reviewTextLength-asc">Length - Short to Long</option>
										</select>
									</span>
									<span id="BVRRDisplayContentSortSuffixID" class="BVRRLabel BVRRDisplayContentSortSuffix"></span>
								</span>
							</span>
						</div>
					</div>
					<div id="BVRRDisplayContentBodyID" class="BVRRDisplayContentBody">
						<div id="BVRRDisplayContentReviewID_34325752" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewOdd BVRRDisplayContentReviewFirst BVRROdd BVRRFirst">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_2654792142" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/2654792142/profile.htm">
												<span class="BVRRNickname">Inventress</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_2654792142" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/2654792142/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_2654792142" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/2654792142/profile.htm">(read all my reviews)</a>
										</div>
									</div>
									<div class="BVRRBadges BVRRReviewBadges">
										<div class="BVRRBadgeGraphic BVRRReviewBadgeGraphic BVRRYesvibGraphic">
											<div class="BVRRBadgeLabel BVRRReviewBadgeLabel BVRRYesvibLabel"></div>
										</div>
										<div class="BVRRSpacer BVRRBadgeSpacer BVRRReviewBadgeSpacer"></div>
									</div>
									<div class="BVRRContextDataContainer">
										<div class="BVRRContextDataValueContainer BVRRContextDataValueageContainer BVContextDataAge18to24">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueagePrefix">Age:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueage">18-24</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueageSuffix"></span>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">Philosophy Amazing Grace Firming Body Emulsion</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">02.07.14</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewProsConsContainer">
											<div class="BVRRReviewProsContainer">
												<span class="BVRRLabel BVRRReviewProTagsPrefix">Quick Take:&nbsp;</span>
												<span class="BVRRValue BVRRReviewProTags">
													<span class="BVRRTag">scented</span>
													,
													<span class="BVRRTag">absorbs quickly</span>
													,
													<span class="BVRRTag">moisturizing</span>
												</span>
												<span class="BVRRLabel BVRRReviewProTagsSuffix"></span>
											</div>
										</div>
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph">
												<span class="BVRRReviewText">
													When I purchased Body Emulsion, I was looking for a moisturizing cream or lotion that wasn't greasy, could be used in both summer and winter, yet provided enough moisture for the skin not to feel dry in the cold months. I also wanted a scent that was light and did not interfere with my fragrances.
												</span>
											</div>
											<div class="BVRRReviewTextParagraph">
												<span class="BVRRReviewText">
													Amazing Grace Firming Body Emulsion is the best body lotion I have come across. The only thing I would change in it is to make its musky scent a lighter - then, it would be perfect. Now, it is 99% perfect. Even so, it does not interfere with my fragrances and I noticed that it even extends their longevity by as much as 2x, depending on the perfume. The lotion contains the magic ingredient of shea butter, an emollient on trend and for good reason. It also contains macadamia nut and olive oils. The cream moisturizes the body, dry regions like hands and feet, and is gentle enough even for the face without causing acne or sensitivity. Its a true all in one.
												</span>
											</div>
											<div class="BVRRReviewTextParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">Highly recommended for all ages and skin types.</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_34325752" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/34325752/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_34325752" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/34325752/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_34325752" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/34325752/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_34325752" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D34325752&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_34325752" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D34325752&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_34325752" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D34325752&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_34085809" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewEven BVRREven">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5370530221" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5370530221/profile.htm">
												<span class="BVRRNickname">LovnLife10</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5370530221" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5370530221/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5370530221" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5370530221/profile.htm">(read all my reviews)</a>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/2_0/5/rating.gif" class="BVImgOrSprite" alt="2 out of 5" title="2 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">2</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">Eh..not impressed with the firming piece</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">01.23.14</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													I bought it specifically for the firming component based upon reviews I had seen that it works. I have very minimal cellulite on the back of my legs and just needed it to be evened out. I have maybe 3-4 more days left in the bottle and have seen ZERO change. I'm giving it 2 stars since the smell is nice and it feels good on the skin. But definitely doesn't do a thing in firming.
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">2</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">3</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_34085809" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/34085809/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">2</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel2">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_34085809" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/34085809/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_34085809" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/34085809/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_34085809" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D34085809&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_34085809" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D34085809&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_34085809" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D34085809&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33717435" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewOdd BVRROdd">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1390336730" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1390336730/profile.htm">
												<span class="BVRRNickname">erwinhob</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1390336730" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1390336730/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1390336730" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1390336730/profile.htm">(read all my reviews)</a>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">Amazing</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">01.05.14</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													Love this lotion! It smells so good and is very creamy. Plus the large bottle will last me all year!
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33717435" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33717435/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33717435" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33717435/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33717435" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33717435/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33717435" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33717435&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33717435" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33717435&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33717435" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33717435&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33677731" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewEven BVRREven">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5899252181" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5899252181/profile.htm">
												<span class="BVRRNickname">Kaffbo</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5899252181" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5899252181/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5899252181" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5899252181/profile.htm">(read all my reviews)</a>
										</div>
									</div>
									<div class="BVRRBadges BVRRReviewBadges">
										<div class="BVRRBadgeGraphic BVRRReviewBadgeGraphic BVRRBeautyInsiderGraphic">
											<div class="BVRRBadgeLabel BVRRReviewBadgeLabel BVRRBeautyInsiderLabel"></div>
										</div>
										<div class="BVRRSpacer BVRRBadgeSpacer BVRRReviewBadgeSpacer"></div>
									</div>
									<div class="BVRRUserLocationContainer">
										<span class="BVRRLabel BVRRUserLocationPrefix">Location:</span>
										<span class="BVRRValue BVRRUserLocation">Kathy Beaudoin</span>
										<span class="BVRRLabel BVRRUserLocationSuffix"></span>
									</div>
									<div class="BVRRContextDataContainer">
										<div class="BVRRContextDataValueContainer BVRRContextDataValueskinTypeContainer BVContextDataSkinTypeNormal">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueskinTypePrefix">Skin Type:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueskinType">Normal</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueskinTypeSuffix"></span>
										</div>
										<div class="BVRRContextDataValueContainer BVRRContextDataValueageContainer BVContextDataAge45to54">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueagePrefix">Age:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueage">45-54</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueageSuffix"></span>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">AMAZING GRACE FIRMING BODY EMULSION IS FABULOUS!!</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">01.03.14</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewProsConsContainer">
											<div class="BVRRReviewProsContainer">
												<span class="BVRRLabel BVRRReviewProTagsPrefix">Quick Take:&nbsp;</span>
												<span class="BVRRValue BVRRReviewProTags">
													<span class="BVRRTag">absorbs quickly</span>
													,
													<span class="BVRRTag">scented</span>
													,
													<span class="BVRRTag">rich</span>
												</span>
												<span class="BVRRLabel BVRRReviewProTagsSuffix"></span>
											</div>
										</div>
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													I tried this in a sample size, then purchased the 8 oz. and worked my way up to the 32 oz. w/pump. I love 2 things about this product: it is scented to make you feel soap- smelling clean and it moisturizes very well without being too greasy. My husband is scent sensitive and even he likes for me to put this fragranced product on. The dog even smells clean when I pet him with my scented hands!
													<br>
													This is my one indulgence in life and yes, it is a tad pricey, but why not have something that you really enjoy to make life a little nicer and better smelling?
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">4</span>
														<span class="BVDISuffix">points</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">4</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">4</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33677731" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33677731/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">4</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33677731" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33677731/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33677731" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33677731/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33677731" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33677731&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33677731" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33677731&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33677731" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33677731&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33671782" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewOdd BVRROdd">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_979780239" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/979780239/profile.htm">
												<span class="BVRRNickname">mizlynn</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_979780239" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/979780239/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_979780239" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/979780239/profile.htm">(read all my reviews)</a>
										</div>
									</div>
									<div class="BVRRBadges BVRRReviewBadges">
										<div class="BVRRBadgeGraphic BVRRReviewBadgeGraphic BVRRBeautyInsiderGraphic">
											<div class="BVRRBadgeLabel BVRRReviewBadgeLabel BVRRBeautyInsiderLabel"></div>
										</div>
										<div class="BVRRSpacer BVRRBadgeSpacer BVRRReviewBadgeSpacer"></div>
									</div>
									<div class="BVRRUserLocationContainer">
										<span class="BVRRLabel BVRRUserLocationPrefix">Location:</span>
										<span class="BVRRValue BVRRUserLocation">New Orleans, LA</span>
										<span class="BVRRLabel BVRRUserLocationSuffix"></span>
									</div>
									<div class="BVRRContextDataContainer">
										<div class="BVRRContextDataValueContainer BVRRContextDataValueskinTypeContainer BVContextDataSkinTypeNormal">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueskinTypePrefix">Skin Type:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueskinType">Normal</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueskinTypeSuffix"></span>
										</div>
										<div class="BVRRContextDataValueContainer BVRRContextDataValueageContainer BVContextDataAge35to44">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueagePrefix">Age:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueage">35-44</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueageSuffix"></span>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle"></span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">01.02.14</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewProsConsContainer">
											<div class="BVRRReviewProsContainer">
												<span class="BVRRLabel BVRRReviewProTagsPrefix">Quick Take:&nbsp;</span>
												<span class="BVRRValue BVRRReviewProTags">
													<span class="BVRRTag">scented</span>
													,
													<span class="BVRRTag">absorbs quickly</span>
													,
													<span class="BVRRTag">rich</span>
												</span>
												<span class="BVRRLabel BVRRReviewProTagsSuffix"></span>
											</div>
										</div>
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													this is my favorite scent. i wear it all the time. love that the lotion comes in such a large bottle.
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVZero BVDI_FVLevel1">
														<span class="BVDIValue BVDINumber">0</span>
														<span class="BVDISuffix">points</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel1">
														<span class="BVDIValue BVDINumber">0</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel1">
														<span class="BVDIValue BVDINumber">0</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel1">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33671782" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33671782/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33671782" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33671782/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33671782" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33671782/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33671782" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33671782&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33671782" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33671782&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33671782" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33671782&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33620728" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewEven BVRREven">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1943091770" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1943091770/profile.htm">
												<span class="BVRRNickname">nicaboli</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1943091770" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1943091770/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1943091770" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1943091770/profile.htm">(read all my reviews)</a>
										</div>
									</div>
									<div class="BVRRBadges BVRRReviewBadges">
										<div class="BVRRBadgeGraphic BVRRReviewBadgeGraphic BVRRBeautyInsiderGraphic">
											<div class="BVRRBadgeLabel BVRRReviewBadgeLabel BVRRBeautyInsiderLabel"></div>
										</div>
										<div class="BVRRSpacer BVRRBadgeSpacer BVRRReviewBadgeSpacer"></div>
									</div>
									<div class="BVRRUserLocationContainer">
										<span class="BVRRLabel BVRRUserLocationPrefix">Location:</span>
										<span class="BVRRValue BVRRUserLocation">Middletown, DE</span>
										<span class="BVRRLabel BVRRUserLocationSuffix"></span>
									</div>
									<div class="BVRRContextDataContainer">
										<div class="BVRRContextDataValueContainer BVRRContextDataValueskinTypeContainer BVContextDataSkinTypeDry">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueskinTypePrefix">Skin Type:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueskinType">Dry</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueskinTypeSuffix"></span>
										</div>
										<div class="BVRRContextDataValueContainer BVRRContextDataValueageContainer BVContextDataAge35to44">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueagePrefix">Age:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueage">35-44</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueageSuffix"></span>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle"></span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">12.30.13</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewProsConsContainer">
											<div class="BVRRReviewProsContainer">
												<span class="BVRRLabel BVRRReviewProTagsPrefix">Quick Take:&nbsp;</span>
												<span class="BVRRValue BVRRReviewProTags">
													<span class="BVRRTag">scented</span>
													,
													<span class="BVRRTag">absorbs quickly</span>
													,
													<span class="BVRRTag">rich</span>
												</span>
												<span class="BVRRLabel BVRRReviewProTagsSuffix"></span>
											</div>
										</div>
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">love love love it!!!!!</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVNegative BVDI_FVLevel2">
														<span class="BVDIPrefix">-</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel1">
														<span class="BVDIValue BVDINumber">0</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel1">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33620728" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33620728/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel2">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33620728" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33620728/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33620728" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33620728/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33620728" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33620728&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33620728" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33620728&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33620728" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33620728&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33600683" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewOdd BVRROdd">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1091408403" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1091408403/profile.htm">
												<span class="BVRRNickname">Greenville</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1091408403" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1091408403/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_1091408403" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/1091408403/profile.htm">(read all my reviews)</a>
										</div>
									</div>
									<div class="BVRRBadges BVRRReviewBadges">
										<div class="BVRRBadgeGraphic BVRRReviewBadgeGraphic BVRRBeautyInsiderGraphic">
											<div class="BVRRBadgeLabel BVRRReviewBadgeLabel BVRRBeautyInsiderLabel"></div>
										</div>
										<div class="BVRRSpacer BVRRBadgeSpacer BVRRReviewBadgeSpacer"></div>
									</div>
									<div class="BVRRUserLocationContainer">
										<span class="BVRRLabel BVRRUserLocationPrefix">Location:</span>
										<span class="BVRRValue BVRRUserLocation">Greenville, SC</span>
										<span class="BVRRLabel BVRRUserLocationSuffix"></span>
									</div>
									<div class="BVRRContextDataContainer">
										<div class="BVRRContextDataValueContainer BVRRContextDataValueskinTypeContainer BVContextDataSkinTypeNormal">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueskinTypePrefix">Skin Type:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueskinType">Normal</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueskinTypeSuffix"></span>
										</div>
										<div class="BVRRContextDataValueContainer BVRRContextDataValueageContainer BVContextDataAgeOver54">
											<span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueagePrefix">Age:</span>
											<span class="BVRRValue BVRRContextDataValue BVRRContextDataValueage">over 54</span>
											<span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueageSuffix"></span>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/2_0/5/rating.gif" class="BVImgOrSprite" alt="2 out of 5" title="2 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">2</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">Disappointed</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">12.29.13</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewProsConsContainer">
											<div class="BVRRReviewProsContainer">
												<span class="BVRRLabel BVRRReviewProTagsPrefix">Quick Take:&nbsp;</span>
												<span class="BVRRValue BVRRReviewProTags">
													<span class="BVRRTag">supposed to be scented but is barely</span>
												</span>
												<span class="BVRRLabel BVRRReviewProTagsSuffix"></span>
											</div>
										</div>
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													I have used the Amazing Grace firming skin lotion for years and I'm very disappointed in the last bottle I received. There is not nearly the level of fragrance in the lotion that there always used to be. In fact, there is very little Amazing Grace fragrance in it at all. Sure seem like Philosophy is cutting corners in adding any fragrance and I will have to find another skin lotion to use instead.
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel3">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">7</span>
														<span class="BVDISuffix">points</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel3">
														<span class="BVDIValue BVDINumber">8</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel3">
														<span class="BVDIValue BVDINumber">9</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel3">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33600683" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33600683/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">8</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel2">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33600683" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33600683/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33600683" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33600683/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33600683" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33600683&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33600683" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33600683&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33600683" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33600683&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33568087" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewEven BVRREven">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5491709481" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5491709481/profile.htm">
												<span class="BVRRNickname">Lillyrose81</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5491709481" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5491709481/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5491709481" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5491709481/profile.htm">(read all my reviews)</a>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">love!</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">12.27.13</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													Major difference in using this as compared to drugstore lotions. I use this with the matching scrub and my skin has never felt so moisturized!
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33568087" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33568087/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33568087" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33568087/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33568087" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33568087/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33568087" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33568087&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33568087" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33568087&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33568087" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33568087&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33566179" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewOdd BVRROdd">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5640877162" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5640877162/profile.htm">
												<span class="BVRRNickname">LDThomas</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5640877162" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5640877162/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5640877162" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5640877162/profile.htm">(read all my reviews)</a>
										</div>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/4_0/5/rating.gif" class="BVImgOrSprite" alt="4 out of 5" title="4 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">4</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">Amazing Grace</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">12.27.13</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													I absolutely love the clean fresh smell of the lotion, perfumes and bath and shower gel. The lotion is very moisturizing and not greasy feeling. When i run out i will be buying more.
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33566179" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33566179/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33566179" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33566179/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33566179" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33566179/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33566179" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33566179&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33566179" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33566179&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33566179" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33566179&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
						<div id="BVRRDisplayContentReviewID_33547941" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewEven BVRRDisplayContentReviewLast BVRREven BVRRLast">
							<div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
							<div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
								<div class="BVRRReviewDisplayStyle3Summary">
									<div class="BVRRUserNicknameContainer">
										<span class="BVRRLabel BVRRUserNicknamePrefix"></span>
										<span class="BVRRValue BVRRUserNickname">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5468087153" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5468087153/profile.htm">
												<span class="BVRRNickname">Laneymom</span>
											</a>
										</span>
										<span class="BVRRLabel BVRRUserNicknameSuffix"></span>
										<div class="BVRRUserNicknameReadReviewsContainer">
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5468087153" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5468087153/profile.htm">
												<img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
											<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_5468087153" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/5468087153/profile.htm">(read all my reviews)</a>
										</div>
									</div>
									<div class="BVRRUserLocationContainer">
										<span class="BVRRLabel BVRRUserLocationPrefix">Location:</span>
										<span class="BVRRValue BVRRUserLocation">Milwaukee, WI</span>
										<span class="BVRRLabel BVRRUserLocationSuffix"></span>
									</div>
								</div>
								<div class="BVRRReviewDisplayStyle3Main">
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
									<div class="BVRRCustomMainReviewMetaWrapper">
										<div class="BVRRReviewRatingsContainer">
											<div class="BVRROverallRatingContainer">
												<div class="BVRRRatingContainerStar">
													<div class="BVRRRatingEntry BVRROdd">
														<div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
															<div class="BVRRLabel BVRRRatingNormalLabel"></div>
															<div class="BVRRRatingNormalImage">
																<img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
															<div class="BVRRRatingNormalOutOf">
																<span class="BVRRNumber BVRRRatingNumber">5</span>
																<span class="BVRRSeparatorText">out of</span>
																<span class="BVRRNumber BVRRRatingRangeNumber">5</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="BVRRReviewTitleContainer">
											<span class="BVRRLabel BVRRReviewTitlePrefix"></span>
											<span class="BVRRValue BVRRReviewTitle">Great Lotion!</span>
											<span class="BVRRLabel BVRRReviewTitleSuffix"></span>
										</div>
										<div class="BVRRReviewDateContainer">
											<span class="BVRRLabel BVRRReviewDatePrefix">-</span>
											<span class="BVRRValue BVRRReviewDate">12.26.13</span>
											<span class="BVRRLabel BVRRReviewDateSuffix"></span>
										</div>
									</div>
									<div class="BVRRReviewDisplayStyle3Content">
										<div class="BVRRReviewProsConsContainer">
											<div class="BVRRReviewProsContainer">
												<span class="BVRRLabel BVRRReviewProTagsPrefix">Quick Take:&nbsp;</span>
												<span class="BVRRValue BVRRReviewProTags">
													<span class="BVRRTag">absorbs quickly</span>
													,
													<span class="BVRRTag">scented</span>
													,
													<span class="BVRRTag">rich</span>
												</span>
												<span class="BVRRLabel BVRRReviewProTagsSuffix"></span>
											</div>
										</div>
										<div class="BVRRReviewTextContainer">
											<div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
												<span class="BVRRReviewText">
													Great moisturizing lotion that absorbs quickly and doesn't irritate my sensitive skin. Best of all I love the scent!
												</span>
											</div>
										</div>
										<div class="RRBeforeFeedbackContainerSpacer"></div>
										<div class="BVDI_FV">
											<div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
												<div class="BVDI_FVSum BVDI_FVSumHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIPrefix">+</span>
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">point</span>
													</span>
												</div>
												<div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
													<span class="BVDI_FVPositive BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">out of</span>
													</span>
													<span class="BVDI_FVTotal BVDI_FVLevel2">
														<span class="BVDIValue BVDINumber">1</span>
														<span class="BVDISuffix">found this review helpful.</span>
													</span>
												</div>
												<div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
													<span class="BVDIPrefix">Was this review helpful?</span>
													<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel2">
														<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_33547941" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33547941/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">Yes</span>
														</a>
														<span class="BVDISuffix">
															(
															<span class="BVDINumber">1</span>
															)
														</span>
													</span>
													<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
														<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_33547941" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33547941/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
															<span class="BVDILinkSpan">No</span>
														</a>
													</span>
												</div>
											</div>
											<div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
												<a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_33547941" data-bvjsref="http://reviews.astoria.com/8723illuminate/P7411/review/33547941/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
													<span class="BVDILinkSpan">Report inappropriate content</span>
												</a>
											</div>
										</div>
										<div class="RRBeforeSocialLinksContainerSpacer"></div>
										<div class="BVRRReviewSocialLinksContainer">
											<div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_33547941" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33547941&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_33547941" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33547941&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
											<a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_33547941" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP7411%2Freviews.htm%3FreviewID%3D33547941&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs607655-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
												<img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
										</div>
										<div class="RRCustomBeforeRecommendContainerSpacer"></div>
										<div class="RRBeforeClientResponseContainerSpacer"></div>
									</div>
									<div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
								</div>
								<div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
							</div>
							<div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
						</div>
					</div>
					<div id="BVRRDisplayContentFooterID" class="BVRRFooter BVRRDisplayContentFooter">
						<div class="BVRRPager BVRRPageBasedPager">
							<span class="BVRRPageLink BVRRPageNumber BVRRSelectedPageNumber">1</span>
							|
							<span class="BVRRPageLink BVRRPageNumber">
								<a title="2" name="BV_TrackingTag_Review_Display_PageNumber_2" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;page=2&amp;scrollToTop=true">2</a>
							</span>
							|
							<span class="BVRRPageLink BVRRPageNumber">
								<a title="3" name="BV_TrackingTag_Review_Display_PageNumber_3" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;page=3&amp;scrollToTop=true">3</a>
							</span>
							|
							<span class="BVRRPageLink BVRRPageNumber">
								<a title="4" name="BV_TrackingTag_Review_Display_PageNumber_4" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;page=4&amp;scrollToTop=true">4</a>
							</span>
							|
							<span class="BVRRPageLink BVRRPageNumber">
								<a title="5" name="BV_TrackingTag_Review_Display_PageNumber_5" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;page=5&amp;scrollToTop=true">5</a>
							</span>
							| ...
							<span class="BVRRPageLink BVRRPageNumber">
								<a title="113" name="BV_TrackingTag_Review_Display_PageNumber_113" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;page=113&amp;scrollToTop=true">113</a>
							</span>
							|
							<span class="BVRRPageLink BVRRNextPage">
								<a title="...next |" name="BV_TrackingTag_Review_Display_NextPage" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P7411/reviews.htm?format=embedded&amp;page=2&amp;scrollToTop=true">...next |</a>
							</span>
							<span class="BVRRPagerArrows"></span>
						</div>
					</div>
					<div class="BVRRSpacer BVRRDisplayContentSpacer"></div>
				</div>
			</div>
		</div>
	</div>
	<noscript>
		&lt;iframe src="http://reviews.astoria.com/8723Illuminate/P7411/reviews.htm?format=noscript" width="100%"&gt;&lt;/iframe&gt;&lt;br/&gt;
	</noscript>
</div>