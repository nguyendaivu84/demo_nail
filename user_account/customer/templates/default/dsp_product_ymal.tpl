<div class="ymal">
	<h3 class="section-title">[@ALSO_LIKE]</h3>
	<div class="carousel" data-speed="350" data-show="4" data-circle="false">
		<ul class="carousel-inner container">[@PRODUCT_SLIDE]

		</ul>
		<span class="carousel-control carousel-prev">
			<span class="arrow arrow-left arrow-large"></span>
		</span>
		<span class="carousel-control carousel-next">
			<span class="arrow arrow-right arrow-large "></span>
		</span>
	</div>
</div>