<script>
    $(document).ready(function(){
        $("#view_larger").on("click",function(){
            $("#popup_view_larger").bPopup({
                fadeSpeed: 'slow',
                followSpeed: 1500
                ,opacity:0
                ,escClose :true
            });
        });
        $('#modal-view-larger').elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750
        });
    });
</script>
<div class="pdp-sidebar">
	<div class="hero">
		<div class="hero-main-image">
			<img rel="#modal-view-larger" data-link="[@PRODUCT_IMAGE]" data-zoom-image="[@PRODUCT_IMAGE]" id="modal-view-larger" src="[@PRODUCT_IMAGE]" data-zoom="true" alt="[@PRODUCT_NAME_IMAGE]" width="250" height="250">
        </div>
		<div class="bi-logos">
			<img class="logo-bi" src="/images/logo-bi-v.png" width="14" height="152" alt="Beauty Insider" >
			<img class="logo-vib" src="/images/logo-vib-v.png" width="14" height="152" alt="VIB Beauty Insider" >
			<img class="logo-rouge" src="/images/logo-rouge-v.png" width="14" height="86" alt="VIB Rouge" ></div>
		<div class="seal">
			<img src="/images/seal-naturals.png" alt="heroimage" style="display: none">
        </div>
		<div class="messages">
			<p class="flags">[@LIMITED_EDITION_SIDEBAR]</p>
		</div>
		 
		<div id="sku1539543-thumbs" class="image-selector border container main-product-thumbnails" style="display:none">
            [@MINI_IMAGE]
			<br/>
		</div>
        [@VIEW_LAGER]
	</div>
	<!--  
	<div class="sidebar-extra">
		<div class="product-reviews container">
			<div id="BVCustomerRatings"></div>
			<div id="BVCustomSummaryActions">
				<div id="BVSecondaryCustomerRatings"></div>
				<div id="BVQASummaryBoxContainer"></div>
			</div>
		</div>
		<div class="share container">
			<div class="social-components">
				<div class="share-btn icon icon-facebook-large social-enabled" data-socialtype="fb" data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=">
					<a href="#" target="_blank"></a>
				</div>
				<div class="share-btn pin social-enabled icon icon-pinterest-large" data-socialtype="pinterest" data-socialurlprefix="http://pinterest.com/pin/create/button/?url=" data-socialurl="http://www.astoria.com/beauty-in-bloom-P384728?skuId=" data-socialnextskuid="" data-socialpreviousskuid="" data-socialmessage="bareMinerals - Beauty In Bloom #astoria" data-socialtracking="&om_mmc=oth-pinterest-dotcompinbuttons-2012">
					<a href="#" target="_blank" class="logo-pinterest"></a>
				</div>
				<div class="share-btn icon icon-twitter-large social-enabled" data-socialtype="twitter" data-socialurlprefix="https://twitter.com/share?url=" data-socialmessage="Beauty In Bloom - bareMinerals | http://www.astoria.com/beauty-in-bloom-P384728">
					<a href="#" target="_blank"></a>
				</div>
				<div class="share-btn icon icon-gplus-large social-enabled gplus" data-socialtype="gplus">
					<a href="#" class="g-plusone" data-href="" target="_blank"></a>
				</div>
			</div>
		</div>
	</div>-->
</div>