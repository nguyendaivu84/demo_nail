<style>
    .liveText {
        text-align: center;
    }
    .liveText h1, .liveText h3{
        font-family: 'avalon-bold', arial, sans-serif;
        font-weight: 700;
    }
    .liveText h2, .liveText h4{
        font-family: 'avalon-book', arial, sans-serif;
        font-weight: 400;
    }
    .liveText h1{
        margin: -10px 0px 7px 20px;
        font-size:32px;
        letter-spacing:10px;
    }
    .liveText h2{
        font-size:20px;
        letter-spacing:5.2px;
        margin-left:10px;
        margin-top:-15px;
        margin-bottom:0px;
    }
    .liveText h3{
        font-size:19px;
        letter-spacing:6.4px;
        margin-left:12px;
        margin-top:-17px;
        margin-bottom:0px;
    }
    .liveText h4{
        margin:0px 0 0 10px;
        font-size:14px;
        letter-spacing:3.4px;
    }
    .liveText p{
        font-size:14px;
        margin:3px 0 0 6px;
        margin-bottom:0px;
    }
    .liveText .ruleContainer{
        padding: 0px 345px;
        margin-bottom:30px;
    }
    .liveText .rule{
        border-bottom:2px solid #000000;
        height:32px;
    }
    /*Express Services Section */
    div.expressServices{
        padding-top:57px;
        margin-left:0px;
        width:758px;
        border-top:1px solid #e8e8e8;
    }
    .expressServices h3{
        font-size:20px;
        letter-spacing:6px
    }
    .expressServices h4{
        margin: -2px 0 0 6px;
        letter-spacing:4px;
    }
    .expressServices div{
        background-color:#e8e8e8;

        border-top:1px solid #e8e8e8;
    }
    .expressServices div p{
        font-size:12px;
        padding: 8px;
        letter-spacing:1px;
    }
    /*IQ Services Section */
    div.IQServices{
        padding-top:57px;
        margin-left:0px;
        width:758px;
        border-top:1px solid #e8e8e8;
    }
    .IQServices h3{
        font-size:20px;
        letter-spacing:6px
    }
    .IQServices h4{
        margin: -2px 0 0 6px;
        letter-spacing:4px;
    }
    .IQServices div{
        background-color:#e8e8e8;
        border-top:1px solid #e8e8e8;
    }
    .IQServices div p{
        font-size:12px;
        padding: 8px;
        letter-spacing:1px;
    }
    /*Seasonal Services Section */
    div.seasonalServices{
        margin-left:0px;
        width:777px;
    }
    .seasonalServices div{
        background-color:#e8e8e8;
        margin-top:38px;
    }
    .seasonalServices div p{
        font-size:12px;
        padding: 8px;
        letter-spacing:1px;
    }

    div.eventsLiveText{
        width:777px;
        text-align:center;
        margin-top:-11px;
    }
    .eventsLiveText {
        text-align: center;
    }
    .eventsLiveText h1, .eventsLiveText h3{
        font-family: 'avalon-bold', arial, sans-serif;
        font-weight: 700;
    }
    .eventsLiveText h2, .eventsLiveText h4{
        font-family: 'avalon-book', arial, sans-serif;
        font-weight: 400;
    }
    .eventsLiveText h1{
        margin: 0px 0px 7px 18px;
        font-size:32px;
        letter-spacing:9.9px;
    }
    .eventsLiveText h2{
        font-size:20px;
        letter-spacing:5.7px;
        margin:-14px 0 0 8px;
        font-family:'avalon-book', arial, sans-serif;
    }
    .eventsLiveText h3{
        font-size:19px;
        letter-spacing:6.4px;
        margin-left:12px;
        margin-top:-17px;
        margin-bottom:0px;
    }
    .eventsLiveText h4{
        margin:0px 0 0 10px;
        font-size:14px;
        letter-spacing:3.4px;
    }
    .eventsLiveText p{
        font-size:14px;
        margin:3px 0 0 12px;
        margin-bottom:0px;
        padding:0 130px;
        letter-spacing:.1px;
    }
    .eventsLiveText .eventsRuleContainer{
        padding: 0px 346px;
        margin-bottom:30px;
    }
    .eventsLiveText .eventsRule{
        border-bottom:2px solid #000000;
        margin-left:6px;
        height:32px;
    }
    div.classesLiveText{
        width:772px;
        text-align:center;
        margin-top:-11px;
    }
    .classesLiveText {
        text-align: center;
    }
    .classesLiveText h1, .classesLiveText h3{
        font-family: 'avalon-bold', arial, sans-serif;
        font-weight: 700;
    }
    .classesLiveText h2, .classesLiveText h4{
        font-family: 'avalon-book', arial, sans-serif;
        font-weight: 400;
    }
    .classesLiveText h1{
        margin: 0px 0px 7px 0px;
        font-size:32px;
        letter-spacing:10px;
    }
    .classesLiveText h2{
        font-size:20px;
        letter-spacing:5.7px;
        margin:-14px 0 0 8px;
        font-family:'avalon-demi', arial, sans-serif;
    }
    .classesLiveText h3{
        font-size:19px;
        letter-spacing:6.4px;
        margin-left:12px;
        margin-top:-17px;
        margin-bottom:0px;
    }
    .classesLiveText h4{
        margin:0px 0 0 10px;
        font-size:14px;
        letter-spacing:3.4px;
    }
    .classesLiveText p{
        font-size:14px;
        margin:3px 0 0 12px;
        margin-bottom:0px;
        padding:0 130px;
        letter-spacing:.1px;
    }
    .classesLiveText .classesRuleContainer{
        padding: 0px 346px;
        margin-bottom:0px;
    }
    .classesLiveText .classesRule{
        border-bottom:2px solid #000000;
        margin-left:6px;
        height:32px;
    }

    #store_hq_classes{
        margin-left:120px;
        margin-top:15px;
    }
    #store_hq_classes h2{
        font-family: 'avalon-bold',Helvetica,arial,sans-serif;
        font-size:16px;
        font-weight: 700;
        letter-spacing: 1px;
        margin-top:20px;
        margin-bottom:0px;
    }
    #store_hq_classes h3{
        font-family: 'avalon-book',arial,sans-serif;
        font-size: 17px !important;
        font-weight: 400;
        letter-spacing: 2px;
        margin-top:2px;
        margin-bottom:5px;
    }
    #store_hq_classes a{
        font-size:12px;
        color: #333333;
        display: block;
        margin-bottom:5px;
    }
    #store_hq_classes a:hover{
        color:#999999;
    }
    #store_hq_classes .innerTables {
        width: 150px;
        margin-left: 30px;
        height: 350px; /*This is what gets adjusted if the columns get taller, so border matches up*/
    }
    th, td {
        vertical-align: top;
    }
</style>
<script>
    var url = '[@URL]';
    $(document).ready(function(){
        $(".update").hide();
        $("#[@CURRENT_PAGE]").css('color','red');
    });
    function cancelUpdate(vname){
        $("#"+vname).hide();
    }
    function updateName(vname){
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var user_name = $("#user_name").val();
        var confirm_user_name = $("#confirm_user_name").val();
        var password = $("#password").val();
        var password_confirmation = $("#password_confirmation").val();
        $.ajax({
            url:url+"profile/my-information"
            ,type: 'POST'
            ,data    : {
                ajax:"update_information"
                ,txt_key:vname
                ,txt_first_name:first_name
                ,txt_last_name:last_name
                ,txt_user_name:user_name
                ,txt_confirm_user_name:confirm_user_name
                ,txt_password:password
                ,txt_password_confirmation:password_confirmation
            }
            ,beforeSend:function(){ }
            ,success:function(data){
                $("#error_message").html(data.message);
                if(data.error==0){
                    if(vname=='name') $("#full_name").html(first_name+" "+last_name);
                    if(vname=='email') $("#full_email").html(user_name);
                }
            }
            ,error:function(data){
//                alert(data.responseText);
            }
        });
    }
    function showUpdate(vname){
        $("#"+vname).show();
    }
</script>
<ul class="breadcrumb">
    <li><a href="/contentStore/mediaContentTemplate.jsp?mediaId=10000020">My Account</a></li>
    <li>[@DESCRIPTION]</li>
</ul>
<div class="content about">
    <div class="sidebar">
        <div class="sidenav">
            <h2 class="nav-title">My Account</h2>
            <ul class="nav nav-stacked">
                <li>
                    <a id="my-information" href="/profile/my-information">My information</a>
                </li>
                <!--li><a id="my-order" href="/profile/my-order" >My Order</a></li-->
            </ul>
        </div>
    </div>
    <div id="main" class="maincontent my-info">
        [@CONTENT]
    </div>
</div>