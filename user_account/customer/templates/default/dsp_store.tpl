<style>
	.liveText {
		text-align: center;
	}
	.liveText h1, .liveText h3{
		font-family: 'avalon-bold', arial, sans-serif;
		font-weight: 700;
	}
	.liveText h2, .liveText h4{
		font-family: 'avalon-book', arial, sans-serif;
		font-weight: 400;
	}
	.liveText h1{
		margin: -10px 0px 7px 20px;
		font-size:32px;
		letter-spacing:10px;
	}
	.liveText h2{
		font-size:20px;
		letter-spacing:5.2px;
		margin-left:10px;
		margin-top:-15px;
		margin-bottom:0px;
	}
	.liveText h3{
		font-size:19px;
		letter-spacing:6.4px;
		margin-left:12px;
		margin-top:-17px;
		margin-bottom:0px;
	}
	.liveText h4{
		margin:0px 0 0 10px;
		font-size:14px;
		letter-spacing:3.4px;
	}
	.liveText p{
		font-size:14px;
		margin:3px 0 0 6px;
		margin-bottom:0px;
	}
	.liveText .ruleContainer{
		padding: 0px 345px;
		margin-bottom:30px;
	}
	.liveText .rule{
		border-bottom:2px solid #000000;
		height:32px;
	}
	/*Express Services Section */
	div.expressServices{
	  padding-top:57px;
	  margin-left:0px;
	  width:758px;
	  border-top:1px solid #e8e8e8;
	}
	.expressServices h3{
	  font-size:20px;
	  letter-spacing:6px
	}
	.expressServices h4{
	  margin: -2px 0 0 6px;
	  letter-spacing:4px;
	}
	.expressServices div{
	  background-color:#e8e8e8;

	  border-top:1px solid #e8e8e8;
	}
	.expressServices div p{
	   font-size:12px;
	   padding: 8px;
	   letter-spacing:1px;
	}
	/*IQ Services Section */
	div.IQServices{
	  padding-top:57px;
	  margin-left:0px;
	  width:758px;
	  border-top:1px solid #e8e8e8;
	}
	.IQServices h3{
	  font-size:20px;
	  letter-spacing:6px
	}
	.IQServices h4{
	  margin: -2px 0 0 6px;
	  letter-spacing:4px;
	}
	.IQServices div{
	  background-color:#e8e8e8;
	  border-top:1px solid #e8e8e8;
	}
	.IQServices div p{
	   font-size:12px;
	   padding: 8px;
	   letter-spacing:1px;
	}
	/*Seasonal Services Section */
	div.seasonalServices{
	  margin-left:0px;
	  width:777px;
	}
	.seasonalServices div{
	  background-color:#e8e8e8;
	  margin-top:38px;
	}
	.seasonalServices div p{
	   font-size:12px;
	   padding: 8px;
	   letter-spacing:1px;
	}

	div.eventsLiveText{
		width:777px;
		text-align:center;
		margin-top:-11px;
		}
		.eventsLiveText {
			text-align: center;
		}
		.eventsLiveText h1, .eventsLiveText h3{
			font-family: 'avalon-bold', arial, sans-serif;
			font-weight: 700;
		}
		.eventsLiveText h2, .eventsLiveText h4{
			font-family: 'avalon-book', arial, sans-serif;
			font-weight: 400;
		}
		.eventsLiveText h1{
			margin: 0px 0px 7px 18px;
			font-size:32px;
			letter-spacing:9.9px;
		}
		.eventsLiveText h2{
			font-size:20px;
			letter-spacing:5.7px;
				margin:-14px 0 0 8px;
				font-family:'avalon-book', arial, sans-serif;
		}
		.eventsLiveText h3{
			font-size:19px;
			letter-spacing:6.4px;
			margin-left:12px;
			margin-top:-17px;
			margin-bottom:0px;
		}
		.eventsLiveText h4{
			margin:0px 0 0 10px;
			font-size:14px;
			letter-spacing:3.4px;
		}
		.eventsLiveText p{
			font-size:14px;
			margin:3px 0 0 12px;
			margin-bottom:0px;
				padding:0 130px;
				letter-spacing:.1px;
		}
		.eventsLiveText .eventsRuleContainer{
			padding: 0px 346px;
			margin-bottom:30px;
		}
		.eventsLiveText .eventsRule{
			border-bottom:2px solid #000000;
				margin-left:6px;
			height:32px;
		}
	div.classesLiveText{
	width:772px;
	text-align:center;
	margin-top:-11px;
	}
	.classesLiveText {
		text-align: center;
	}
	.classesLiveText h1, .classesLiveText h3{
		font-family: 'avalon-bold', arial, sans-serif;
		font-weight: 700;
	}
	.classesLiveText h2, .classesLiveText h4{
		font-family: 'avalon-book', arial, sans-serif;
		font-weight: 400;
	}
	.classesLiveText h1{
		margin: 0px 0px 7px 0px;
		font-size:32px;
		letter-spacing:10px;
	}
	.classesLiveText h2{
		font-size:20px;
		letter-spacing:5.7px;
			margin:-14px 0 0 8px;
			font-family:'avalon-demi', arial, sans-serif;
	}
	.classesLiveText h3{
		font-size:19px;
		letter-spacing:6.4px;
		margin-left:12px;
		margin-top:-17px;
		margin-bottom:0px;
	}
	.classesLiveText h4{
		margin:0px 0 0 10px;
		font-size:14px;
		letter-spacing:3.4px;
	}
	.classesLiveText p{
		font-size:14px;
		margin:3px 0 0 12px;
		margin-bottom:0px;
			padding:0 130px;
			letter-spacing:.1px;
	}
	.classesLiveText .classesRuleContainer{
		padding: 0px 346px;
		margin-bottom:0px;
	}
	.classesLiveText .classesRule{
		border-bottom:2px solid #000000;
			margin-left:6px;
		height:32px;
	}

	#store_hq_classes{
		margin-left:120px;
		margin-top:15px;
	}
	#store_hq_classes h2{
		font-family: 'avalon-bold',Helvetica,arial,sans-serif;
		font-size:16px;
		font-weight: 700;
		letter-spacing: 1px;
		margin-top:20px;
		margin-bottom:0px;
	}
	#store_hq_classes h3{
		font-family: 'avalon-book',arial,sans-serif;
		font-size: 17px !important;
		font-weight: 400;
		letter-spacing: 2px;
		margin-top:2px;
		margin-bottom:5px;
	}
	#store_hq_classes a{
		font-size:12px;
		color: #333333;
		display: block;
		margin-bottom:5px;
	}
	#store_hq_classes a:hover{
		color:#999999;
	}
	#store_hq_classes .innerTables {
		width: 150px;
		margin-left: 30px;
		height: 350px; /*This is what gets adjusted if the columns get taller, so border matches up*/
	}
	th, td {
		vertical-align: top;
	}
	</style>
<script>
    $(document).ready(function(){
        $("#[@CURRENT_PAGE]").css('color','red');
    });
</script>
<ul class="breadcrumb" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
    <li><a href="/contentStore/mediaContentTemplate.jsp?mediaId=10000020">About ASTORIA</a></li>
	<li>[@DESCRIPTION]</li>
</ul>
<div class="content about" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">

	<div class="sidebar">
		<div class="sidenav">
			<h2 class="nav-title">About ASTORIA</h2>
			<ul class="nav nav-stacked">
				<li>
					<a id="about-us" href="/store-content/about-us">About Us</a>
				</li>
				<li><a id="store" href="/store-content/store" >Store Locations</a></li>
				<li>
					<a id="term-of-use" href="/store-content/term-of-use">Term of uses</a>
				</li>
				<li>
					<a id="privacy-policy" href="/store-content/privacy-policy">Privacy Policy</a>
				</li>
                <li>
                    <a id="privacy-policy" href="/store-content/other-shipping-options">Other Shipping Options</a>
                </li>
                <li>
                    <a id="privacy-policy" href="/store-content/restriction">restriction</a>
                </li>
            </ul>
		</div>
	</div>
	<div id="main" class="maincontent">
		<div class="maincontent-content storehq-content">
            [@STATIC_CONTENT]
		</div>
	</div>
</div>