<label class="radio">
    <input id="[@OPTION_KEY]" class="shippingMethod" type="radio" value="[@OPTION_KEY]"  name="shippingMethod" [@SELECTED]>
    <span class="shipping-method hidden">
        [@OPTION_KEY]
    </span>
    <p class="error-message" id="shippingMethordOnlyOneWarn"></p>
    <b>[@OPTION_NAME]</b>
</label>