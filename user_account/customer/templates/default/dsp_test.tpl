<link rel="stylesheet" href="/user_account/customer/templates/default/css/search.css">
<ul class="breadcrumb">
    <li class="OneLinkNoTx">
        <h1>bareMinerals</h1>
    </li>
</ul>
<div class="content brand" data-social="true" data-brand="bareMinerals">
    <div class="sidebar">
        <div class="sidenav search-navigation">
            <div class="nav-title OneLinkNoTx">bareMinerals</div>
            <ul class="nav nav-stacked ">
                <li class="see-all">
                    <a data-brandid="5737" data-ref="900000" href="#">
                        See All
                        <span>(93)</span>
                    </a>
                </li>
                <li class="">
                    <a data-name="Makeup" data-node="1050000" href="#">
                        Makeup
                        <span> (75)</span>
                    </a>
                </li>
                <li class="">
                    <a data-name="Skin Care" data-node="1050055" href="#">
                        Skin Care
                        <span> (21)</span>
                    </a>
                </li>
                <li class="">
                    <a data-name="Bath & Body" data-node="1050077" href="#">
                        Bath & Body
                        <span> (1)</span>
                    </a>
                </li>
                <li class="">
                    <a data-name="Tools & Brushes" data-node="1050102" href="#">
                        Tools & Brushes
                        <span> (18)</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clear-all-filters top hidden">
            <h3 class="refinement-title">FILTER BY:</h3>
            <input class="btn btn-default" type="submit" value="clear all filters">
        </div>
        <div id="refinements" class="refinements-list"></div>
        <div class="clear-all-filters hidden">
            <input class="btn btn-default" type="submit" value="clear all filters">
        </div>
        <div class="social-links social-components">
            <div class="share-btn icon icon-facebook-large social-enabled " data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=" data-socialtype="fb">
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.astoria.com%2Fbareminerals&description=bareMinerals%20%23astoria&text=bareMinerals%20%23astoria&media=http%3A%2F%2Fwww.astoria.comundefined"></a>
            </div>
            <div id="pinterest-root" class="pin share-btn icon icon-pinterest-large">
                <a data-pin-do="buttonPin" href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.astoria.com%2Fbareminerals%3Fom_mmc%3Doth-pinterest-dotcompinbuttons-2012&media=http%3A%2F%2Fwww.astoria.com%2Fcontentimages%2Fbrands%2Fbareescentuals%2FBareMinerals_Hero_012214_Image.jpg&description=bareMinerals%20%23astoria&ref=http%3A%2F%2Fwww.astoria.com%2Fbareminerals&layout=none" data-pin-config="beside">
                    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png">
                </a>
            </div>
        </div>
    </div>

    <div id="main" class="maincontent">
        <div class="search-banner container">
            <div class="search-header"></div>
        </div>

        <div class="brand-box with-buttons flush container">
            <div class="box-1">
                <div class="inner">
                    <div class="brand-logo">
                        <a href="/bareminerals">
                            <img alt="bareMinerals" src="/contentimages/brands/bareescentuals/082212_bareminerals_logo.jpg">
                        </a>
                    </div>
                    <div class="brand-content">
                        <div class="desc desc-less">
                            <p>Bare Escentuals uses only the finest ingredients in their formulas. Their commitment to quality led to the creation of their award-winning makeup line, bareMinerals. Free of preservatives, fillers, or binders, bareMinerals represents the ideal mix of makeup and skincare—problem-solving cosmetics that perfect and pamper. </p>
                        </div>
                        <p>
                            <a class="desc-toggle" href="#">
                                <b>more</b>
                                <span class="arrow arrow-down arrow-mini"></span>
                            </a>
                        </p>
                        <div class="desc desc-more hide" style="display: block;">
                            <p>Bare Escentuals uses only the finest ingredients in their formulas. Their commitment to quality led to the creation of their award-winning makeup line, bareMinerals. Free of preservatives, fillers, or binders, bareMinerals represents the ideal mix of makeup and skincare—problem-solving cosmetics that perfect and pamper. Thanks to their zero-irritability factor, the gentle formulas are a dream come true for millions of women of all ages and skin types, especially those with sensitive skin, scars, rosacea, and acne. The proven clinical studies and customer testimonials speak for themselves. It’s makeup that works with your skin, not against it.</p>
                            <p>
                                <a class="desc-toggle" href="#">
                                    <b>less</b>
                                    <span class="arrow arrow-up arrow-mini"></span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-2">
                <img alt="bareMinerals" src="/contentimages/brands/bareescentuals/BareMinerals_Hero_012214_Image.jpg">
            </div>
            <div class="brand-right-nav">
                <h3>
                    <img width="180" height="22" alt="About the Brand" src="/images/about_the_brand.png">
                </h3>
                <ul class="nav nav-stacked">
                    <li>
                        <a href="/bareminerals?buttonMediaId=9600026&icid2=brand_button_bareMinerals_bareMinerals Ready">
                            <span class="link-inner">
                                bareMinerals Ready
                                <span class="arrow arrow-right arrow-mini"></span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="/bareminerals?buttonMediaId=9600026&icid2=brand_button_bareMinerals_bareMinerals Ready">
                            <span class="link-inner">
                                bareMinerals Ready
                                <span class="arrow arrow-right arrow-mini"></span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="/bareminerals?buttonMediaId=9600026&icid2=brand_button_bareMinerals_bareMinerals Ready">
                            <span class="link-inner">
                                bareMinerals Ready
                                <span class="arrow arrow-right arrow-mini"></span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="/bareminerals?buttonMediaId=9600026&icid2=brand_button_bareMinerals_bareMinerals Ready">
                            <span class="link-inner">
                                bareMinerals Ready
                                <span class="arrow arrow-right arrow-mini"></span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="/bareminerals?buttonMediaId=9600026&icid2=brand_button_bareMinerals_bareMinerals Ready">
                            <span class="link-inner">
                                bareMinerals Ready
                                <span class="arrow arrow-right arrow-mini"></span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="authored-content">
            <div class="imageComponent " data-lazyload="true">
                <img width="777" height="28" alt="Explore the Brand 2" src="/contentimages/brands/bareescentuals/BE_home_bar.jpg">
            </div>
            <a class="imageComponent OneLinkHide" title="" target="" data-lazyload="true" href="/buxom?icid2=Shop_Buxom_Link">
                <img width="777" height="24" src="/contentimages/brands/bareescentuals/bare_minerals_shop_buxom_link_052913.jpg">
            </a>
            <hr class="spacer5">
            <div class="imageComponent OneLinkHide" data-lazyload="true">
                <img width="777" height="279" usemap="#2990409_imageMap" alt="Bare Escentuals" src="/contentimages/brands/bareescentuals/052913_BE_Canada.jpg">
                <map name="2990409_imageMap">
                    <area alt="Bareminerals Shade Finder" href="/brand/brandStore.jsp?brandId=5737&buttonMediaId=11200028&icid2=BE_Canada_Banner" coords="280,0,521,277" shape="rect">
                    <area alt="Bareminerals Ready Foundation" href="/brand/brandStore.jsp?brandId=5737&buttonMediaId=9600026&icid2=BE_Canada_Banner" coords="529,1,775,276" shape="rect">
                </map>
            </div>
            <div class=" carousel-component display-4" data-lazyload="true" data-name="bareminerals_editorspicks_carousel" style="margin-bottom:30px;">
                <h2 class="carousel-title">Editors' Picks</h2>
                <div class="carousel" data-circle="false" data-show="4" data-speed="550">
                    <ul class="carousel-inner container" style="width: 1448px; left: 0px;">
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>
                        <li data-slide-index="1">
                            <div class="product-item" data-index="0">
                                <div class="product-image">
                                    <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_image">
                                        <img width="135" height="135" onerror="astoria.ui.error.productImage(this,"/images/image-not-available-135.png");" alt="bareMinerals - Beauty In Bloom" src="http://www.astoria.com/productimages/sku/s1539543-main-grid.jpg" style="display: inline;">
                                    </a>
                                    <a id="product-P384728" class="qlook" rel="#quick-look" href="#" name="sku-1539543">QUICK LOOK</a>
                                </div>
                                <div class="new-love">
                                    <span class="flag-new">NEW</span>
                                    <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
                                </div>
                                <a data-productid="P384728" href="/beauty-in-bloom-P384728?icid2=bareminerals_editorspicks_carousel_P384728_link">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> bareMinerals</span>
                                            Beauty In Bloom
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">44.00</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">91</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
                                </a>
                            </div>
                        </li>

                        <!-- end of li -->
                        <li>
                            <div class="product-item spacer"></div>
                        </li>
                    </ul>
                    <div class="carousel-control carousel-prev disabled">
                        <span class="icon icon-arrow-left"></span>
                    </div>
                    <div class="carousel-control carousel-next">
                        <span class="icon icon-arrow-right"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="fs-loader" class="hide" style="height: 745px; top: 0px; display: none;"></div>
    </div>
</div>