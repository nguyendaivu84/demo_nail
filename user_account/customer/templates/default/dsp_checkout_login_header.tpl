<html>
<head>
	<meta charset="utf-8">
	<title>[@HEADER_WEBSITE]</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=1024">
	<meta name="format-detection" content="telephone=no">
	<link rel="Shortcut Icon" type="image/ico" href="/images/icons/logos.ico" />
	<meta name="contextPath" content="">
	<meta name="imageBasePath" content="/images/">
	<meta name="bvctx" content="">
	<meta name="google-site-verification" content="DHEfxRvk5978fvUKaGrOCpRByg5EXyhJBxhEbM0rZKk" />
	<meta name="bi_status" content="NON_BI"/>
	<meta name="sitescope" content="ok"/>	
	<link rel="stylesheet" href="/user_account/customer/templates/default/css/checkout_login_main.css">
	<link rel="stylesheet" href="/user_account/customer/templates/default/css/checkout.css">
	<link rel="stylesheet" href="[@URL_CUSTOMER]/css/main.css">	
	<script src="/user_account/customer/templates/default/js/modernizr.js"></script>
	<script src="/user_account/customer/templates/default/js/jquery.js"></script>
	<script type="text/javascript" src="/javascripts/target/VisitorAPI.js"></script>
    <script src="[@URL_CUSTOMER]/js/bpopup.js"></script>
    <script src="/lib/js/common.js"></script>
</head>
<body id="checkout" class="checkout">
	<div class="wrapper">
		<div class="checkout-header">
			<div class="logo" style="text-align: center">
				
				<a href="/">
		                		<img src="/images/logo.png" alt="astoria" style="width:100px;"/><img style="height:100px;padding-left:10px;" src="/images/name.png" alt="astoria"/>
		                	</a>
			</div>
		</div>
		<div class="checkout-content checkout-signin clearfix">