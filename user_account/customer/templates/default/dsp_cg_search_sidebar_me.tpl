<div class="sidebar">
    <div class="sidenav search-navigation">
        <ul class="nav nav-stacked ">
            [@CATEGORY_SIDEBAR]
        </ul>
    </div>
    [@CATEGORY_RIGHT_ITEM]
    [@FILTER_BAR]

    <div class="social-links social-components">
        <div class="share-btn icon icon-facebook-large social-enabled hidden" data-socialtype="fb" data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=">
            <a href="#" target="_blank"></a>
        </div>
        <div id="pinterest-root" class="pin hidden share-btn icon icon-pinterest-large">
            <a data-pin-config="beside" href="#" data-pin-do="buttonPin">
                <img src="/user_account/customer/templates/default/img/pin_it_button.png"></a>
        </div>
    </div>

</div>
<!-- END SIDEBAR -->
