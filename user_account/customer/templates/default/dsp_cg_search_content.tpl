<div class="search-banner container">
	<div class="search-header"></div>
</div>
<div class="brand-box flush no-links container" style="display: none;"></div>
<div class="authored-content"></div>
<div class="search-results">
	<div class="total-items">
		<span style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">[@TOTAL_ITEM] </span>
		<br></div>
    [@PRODUCT_ITEM_HEADER]
	<div class="product-grid" style="display: block;">
        [@PRODUCT_ITEMS]
	</div>
    [@PRODUCT_ITEM_FOOTER]
</div>
<div id="fs-loader" class="hide" style="height: 683px; top: 0px; display: none;"></div>
<style type="text/css">
.flags-market {
font-size: 9px;
color: #999;
font-weight: 700;
line-height: 12px;
}
.flags-market .flag+.flag:before {
content: '•';
font-size: 8px;
margin-right: 3px;
}
.pagination {
position: relative;
margin-bottom: 20px;
}
</style>