	  <div class="banner-container">
		  </div>

	  <div class="main">
		  <div class="page-intro container">
				<h1 class="alt-book text-upper">
				  Checkout
				  <img src="/user_account/customer/templates/default/img/icon-flag-ca.png" width="30" height="20" class="flag-shadow">
					</h1>
				</div>

			<div id="shipping" class="checkout-module divided shipping summary">
		<div class="module-top"><h2 class="module-title">Ship To &amp; Delivery</h2></div>
		<div class="module-summary container">

		<div class="module-left" data-shipping-id="">
			<h2 class="module-title">Ship To <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>
			<div class="indent">
					<p class="default" >Default</p>
					<p class="OneLinkNoTx">
						123&#32;123<br>
						123<br>
						123<br>
						123,&nbsp;AB&nbsp;T2A 6A3<br>
						Canada</p>
					<p class="OneLinkNoTx">
						403-204-0224</p>
				</div>
			</div>

		<div class="module-right">
			<h2 class="module-title">Delivery <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>
			<div class="indent">
				<p data-ship-method-summary="">Standard Shipping</p>
			</div>
		</div>

	</div>

<div class="module-content container">

					<form novalidate="novalidate" name="shippingAccordion" action="/checkout/accordions.jsp?_DARGS=/checkout/shippingAddress/shippingAddress.jsp.shippingAccordion" method="post"><input name="_dyncharset" value="UTF-8" type="hidden"></input><input name="_dynSessConf" value="5082249045805785135" type="hidden"></input><input type="hidden" name="showPasswordAccordion" value="yes"/>
						<input name="isSavedBIRewards" type="hidden" value=""/>
						<input type="hidden" id="shippingMethod" value="1000020"/>
						<input name="/atg/commerce/order/purchase/astoriaShippingFormHandler.shippingGroupType" value="HardgoodShippingGroup" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.shippingGroupType" value=" " type="hidden"><input id="selectedAddressId" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.selectedAddressId" value="" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.selectedAddressId" value=" " type="hidden"><input id="shippingState" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.shippingState" value="New" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.shippingState" value=" " type="hidden"><input id="hasAddress" value="true"  type="hidden" />

						<div class="module-left">
						  <h2 class="module-title">Ship To</h2>
						  <div class="indent">
							<div class="address-actions">
					<a href="#" class="btn btn-default btn-sm btn-add-new-address ">add new address</a>
					</div>

				<div class="address editable-address"  style="display:none">
						<p class="OneLinkNoTx">
							123&#32;123<br>
							123<br>
							123<br>
							123,&nbsp;AB&nbsp;T2A 6A3<br>

				Canada
						<br>403-204-0224</p>

				</div>

			<div class="ship-form" style="display:block">
				<div class="form-group">
			<label for="first_name"><span class="required">*</span> First name</label>
			<input id="first_name" maxlength="33" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.firstName" value="123" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.firstName" value=" " type="hidden"></div>
	<div class="form-group">
			<label for="last_name"><span class="required">*</span> Last name</label>
			<input id="last_name" maxlength="33" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.lastName" value="123" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.lastName" value=" " type="hidden"></div>
	<div class="form-group">
			<label for="address1"><span class="required">*</span> Address line 1</label>
			<input id="address1" maxlength="30" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.address1" value="123" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.address1" value=" " type="hidden"></div>
	<div class="form-group">
			<label for="address2">Address line 2</label>
			<input id="address2" maxlength="30" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.address2" value="123" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.address2" value=" " type="hidden"></div>
	<div class="form-group">
			<label for="city"><span class="required">*</span> City</label>
			<input id="city" maxlength="30" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.city" value="123" class="form-control city" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.city" value=" " type="hidden"></div>
	<div class="form-group">
		<label for="state"><span class="required">*</span> Province</label>

			<input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.state" value=" " type="hidden"></input><select id="state" style="" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.state" class="form-control form-control select-state cc_type"><option value="">select a province<option selected value="AB">AB - Alberta<option value="BC">BC - British Columbia<option value="MB">MB - Manitoba<option value="NB">NB - New Brunswick<option value="NL">NL - Newfoundland and Labrador<option value="NS">NS - Nova Scotia<option value="NT">NT - Northwest Territories<option value="NU">NU - Nunavut<option value="ON">ON - Ontario<option value="PE">PE - Prince Edward Island<option value="QC">QC - Quebec<option value="SK">SK - Saskatchewan<option value="YT">YT - Yukon</select></div>
		  <div class="zip-country container form-group">
			<label for="zip"><span class="required">*</span> Postal Code</label>
			<div class="zip-code">
				<input id="zip" maxlength="10" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.postalCode" value="T2A 6A3" class="form-control zip" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.postalCode" value=" " type="hidden"></div>
			<div class="country country-label">Country: Canada</div>
			<input id="country" maxlength="10" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.country" value="CA" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.formParamMap.country" value=" " type="hidden"></div>
		 <div class="phone-controls container">
			<div class="form-group">
			<label for="phone_1"><span class="required">*</span> Phone</label>
			<input id="phone_1" maxlength="3" pattern="\d*" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.phoneNumber1" value="403" class="form-control phone-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.phoneNumber1" value=" " type="hidden"><input id="phone_2" maxlength="3" pattern="\d*" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.phoneNumber2" value="204" class="form-control phone-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.phoneNumber2" value=" " type="hidden"><input id="phone_3" maxlength="4" pattern="\d*" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.phoneNumber3" value="0224" class="form-control phone-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.phoneNumber3" value=" " type="hidden"></div>

			<div class="form-group phone-ext">
				<label for="phone_ext">Ext.</label>
				<input id="phone_ext" maxlength="10" pattern="\d*" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.ext" value="" class="form-control phone-control" type="text"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.ext" value=" " type="hidden"></div>
			</div>

</div>

		 <label class="set-default checkbox">
		   <input id="is_default" name="is_default" value="true" type="checkbox" checked><input name="_D:is_default" value=" " type="hidden">
		   Set as my default shipping address
		 </label>
		 <div class="delete-address" style="display:block">
			 <a href="#" class="btn btn-default btn-sm">Delete address</a>
		 </div>
		 <div class="edit-address-btn" style="display:none">
			<button type="button" class="btn btn-default btn-sm btn-edit" data-address_id="">edit</button>
		 </div>

		<div class="switch"><a href="#"></a></div>
		<p class="required-label">* Required</p>

</div>
						</div><div class="module-right">
						  <h2 class="module-title">Delivery</h2>
						  <div class="indent">
							<input id="isShippingConflict" type="hidden" value=""/>
				<span id="promotional_shipping_methods" class="hidden"></span>

				<div class="warning-message not-allowed-shipping-msg" style="display:none;"><p>The selected delivery method is not valid with the promotion code &lt;promo code&gt;. You must select &lt;valid shipping methods&gt; to qualify. If you continue, the shipping promotion will be removed.</p></div>
				<div class="delivery-method">

				<label class="radio">
							  <span class="shipping-method hidden">Standard Shipping</span>
							  <input name ="1000020" value="0.0" type="hidden" />
							  <p class="error-message" id="shippingMethordOnlyOneWarn"></p>
								  <input id="1000020" style="display:none" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.shippingMethod" value="1000020" class="shippingMethod" type="radio" checked><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.shippingMethod" value=" " type="hidden"><b>
	Standard Shipping:&nbsp;C$7.95* (5-10 business days)</b>

<br>
  FREE on orders over C$75.00*</label>
						<p>* handling fees may apply<br><br></p>
				<p><a rel="#pop-shipping" class="btn btn-default btn-sm pop-info" href="#">Shipping &amp; Handling Information</a></p>
				</div>
			<div class="shipping-gift">

	<h3>Is this a gift?</h3>

	<label class="radio">
	   <input id="gift-no" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.isGift" value="false" type="radio"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.isGift" value=" " type="hidden">
	   No
	</label>
	<label class="radio">
	   <input id="gift-yes" name="/atg/commerce/order/purchase/astoriaShippingFormHandler.isGift" value="true" type="radio" checked><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.isGift" value=" " type="hidden">
	   Yes
	</label>

  </div>

</div>
						</div><div class="module-action">
						   <input name="/atg/commerce/order/purchase/astoriaShippingFormHandler.basketPage" value="/basket/basket.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.basketPage" value=" " type="hidden"><button class="btn btn-alt btn-lg btn-continue" value="continue" name="continue" type="submit">continue <span class="arrow arrow-right"></span></button>
							<input name="/atg/commerce/order/purchase/astoriaShippingFormHandler.addSingleShippingAddress" value="Add NewAddress" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.addSingleShippingAddress" value=" " type="hidden"><input name="/atg/commerce/order/purchase/astoriaShippingFormHandler.useForwards" value="true" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.useForwards" value=" " type="hidden"><input name="/atg/commerce/order/purchase/astoriaShippingFormHandler.addSingleShippingAddressErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaShippingFormHandler.addSingleShippingAddressErrorURL" value=" " type="hidden"></div>
					<input name="_DARGS" value="/checkout/shippingAddress/shippingAddress.jsp.shippingAccordion" type="hidden"></input></form></div>
			</div>
<div id="gift-options" class='checkout-module divided gift-options current' style="">
		<div class="module-top"><h2 class="module-title">gift options</h2></div>


		<div class="module-summary container">
			<div class="module-left">
						<h2 class="module-title">Gift Packaging <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>

						<div class="indent">
							<p> &nbsp;<b>FREE</b></p>
							</div>
					</div>

					<div class="module-right">
						<h2 class="module-title">Gift Message <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>
						<div class="indent">
							<p>No Message</p>
								</div>
					</div>

				</div><div class="module-content container">

			<form name="giftOptions" action="/checkout/accordions.jsp?_DARGS=/checkout/giftOptions.jsp" method="post"><input name="_dyncharset" value="UTF-8" type="hidden"></input><input name="_dynSessConf" value="5082249045805785135" type="hidden"></input><input type="hidden" name="showPasswordAccordion" value="yes"/>
				<input name="isSavedBIRewards" type="hidden" value=""/>


				<div class="module-left">
					<h2 class="module-title">Gift Packaging</h2>

					<div class="indent">

						<h3>choose gift wrap</h3>

										<label class="wrap-option">
														<img height="92" width="92" alt="Standard" src="/user_account/customer/templates/default/img/s510002-main-Sgrid.jpg">
														<span class="radio">
															<input name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.CatalogRefIds" value="510002" type="radio" checked><input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.CatalogRefIds" value=" " type="hidden"><input name ="510002" value="1"  type="hidden" />
																Standard<strong> <span class="cost">FREE</span> </strong>
														</span>
													</label>
												<label class="wrap-option">
														<img height="92" width="92" alt="Gift Satchel" src="/user_account/customer/templates/default/img/s1169937-main-Sgrid.jpg">
														<span class="radio">
															<input name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.CatalogRefIds" value="1169937" type="radio"><input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.CatalogRefIds" value=" " type="hidden"><input name ="1169937" value="1"  type="hidden" />
																Gift Satchel<strong> <span class="cost">FREE</span> </strong>
														</span>
													</label>
												<p class="text-right"><a href="#" class="pop-info" rel="#pop-gift-wrap">learn more</a></p>
											<div id="pop-gift-wrap" class="modal ">
		<div class="modal-body">
			<div class='imageComponent ' data-lazyload='true' ><img src="/contentimages/checkout/Checkout_GiftPackaging_EvergreenPopup_Image.jpg" width="530" height="448" alt="GIFT WRAPPING Present your purchases in one of these classic packages" /></div></div>
		<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
		</div>
</div></div><div class="module-right">
					<h2 class="module-title">Gift Message</h2>
					<div class="indent">
						<h3>optional gift message <span>(250 characters max)</span></h3>
						<div class="gift-message">
							<img height="51" width="61" alert="gift card" src="/user_account/customer/templates/default/img/img-checkout-giftcard.png" class="pull-right">
							<input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.giftMessage" value=" " type="hidden"></input><textarea id="gf_gift_giftMessage" cols="40" maxlength="250" name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.giftMessage" class="form-control OneLinkNoTx" rows="9"></textarea></div>
					</div><div class="module-action">
					  <button class="btn btn-alt btn-lg btn-continue" value="continue" name="continue" type="submit">continue <span class="arrow arrow-right"></span></button>
					  <input name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.addItemToOrder" value="continue" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.addItemToOrder" value=" " type="hidden"><input name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.useForwards" value="true" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.useForwards" value=" " type="hidden"><input name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.addItemToOrderErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.addItemToOrderErrorURL" value=" " type="hidden"><input name="/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.basketPage" value="/basket/basket.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/astoriaGiftOptionCartModifierFormHandler.basketPage" value=" " type="hidden"></div>
				</div><input name="_DARGS" value="/checkout/giftOptions.jsp" type="hidden"></input></form></div>
	</div>
<div id="payment" class="checkout-module divided payment disabled">
		<div class="module-top"><h2 class="module-title">Payment &amp; Promotions</h2></div>
		<div class="module-summary container">

	<div class="module-left">
	 <h2 class="module-title">Payment <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>

	 <div class="indent">

				None
		</div>
		</div>

		<div class="module-right">
		  <h2 class="module-title">Promotions &amp; Gift Cards <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>
		  <div class="indent">

		   <dl>
			 <dd>
				</dd>
		   </dl>

		   </div>
		</div>
		</div>
<div class="module-content container">
			<form novalidate="novalidate" name="paymentAccordion" action="/checkout/accordions.jsp?_DARGS=/checkout/updateCreditCard/checkout-payment.jsp" method="post"><input name="_dyncharset" value="UTF-8" type="hidden"></input><input name="_dynSessConf" value="5082249045805785135" type="hidden"></input><input id="isSubmutPayment" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.isSubmut" value="false" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.isSubmut" value=" " type="hidden"><input id="paymentGroupState" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.paymentGroupState" value="New" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.paymentGroupState" value=" " type="hidden"><input type="hidden" name="showPasswordAccordion" value="yes"/>
		<input name="isSavedBIRewards" type="hidden" value=""/>

		<div class="module-left">
		  <h2 class="module-title" data-before-errors="true">Payment</h2>

		  <div class="indent">

			<input id="isSuccess" type="hidden" value="true" />
			<p class="cc-types">
				<img src="/user_account/customer/templates/default/img/icon-visa.png" alt="VISA" width="33" height="21">
				<img src="/user_account/customer/templates/default/img/icon-mastercard.png" alt="MasterCard" width="33" height="21">
				<img src="/user_account/customer/templates/default/img/icon-amex.png" alt="American Express" width="33" height="21">
				</p>

			<div class="new-card-form">
			<div class="new-credit-card-info" style="display:block">
				<div class="cc-name">

	  <div class="form-group">
			<label for="cc_firstName"><span class="required">*</span> First name</label>
			<input id="cc_first_name" maxlength="33" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.firstName" value="" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.firstName" value=" " type="hidden"></div>
	  <div class="form-group">
			<label for="cc_lastName"><span class="required">*</span> Last name</label>
			<input id="cc_last_name" maxlength="33" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.lastName" value="" class="form-control" type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.lastName" value=" " type="hidden"></div>
	</div>
	<div class="cc-info">
		<input id="cc_type" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardType" value="" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardType" value=" " type="hidden"><div class="cc-info-field form-group">
			<label for="cc_number"><span class="required">*</span> Card number</label>
			<input id="cc_number" maxlength="20" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardNumber" value="" class="form-control cc_number" type="text" autocomplete="off"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.creditCardNumber" value=" " type="hidden"></div>
		<div class="cc-info-field expiration-date form-group" style='display: block'>
			<label for="cc_expirationMonth"><span class="required">*</span> Expiration date</label>
			<input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.expirationMonth" value=" " type="hidden"></input><select id="cc_expiration_month" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.expirationMonth" class="form-control cc-expirationMonth"><option value="">month<option value="1">01<option value="2">02<option value="3">03<option value="4">04<option value="5">05<option value="6">06<option value="7">07<option value="8">08<option value="9">09<option value="10">10<option value="11">11<option value="12">12</select><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.expirationYear" value=" " type="hidden"></input><select id="cc_expiration_year" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.expirationYear" class="form-control cc-expirationYear"><option value="">year<option value="2014">2014<option value="2015">2015<option value="2016">2016<option value="2017">2017<option value="2018">2018<option value="2019">2019<option value="2020">2020<option value="2021">2021<option value="2022">2022<option value="2023">2023</select></div>
		<div class="sec-code container form-group" style='display: block'>
	<label for="cc-code"><span class="required">*</span> Security code</label>
	<input id="cc-code" maxlength="4" name="cc_code" value="" class="form-control cc-code" type="text" autocomplete="off"><input name="_D:cc_code" value=" " type="hidden"><a href="#" class="icon icon-help" rel="#pop-sec-code"></a>
</div>
</div>
</div>
			<div class="billing-address container" style="display:block">
				<input id='hasastoriaHardgoodShippingGroup' value='true' type="hidden"/>
<h3>Billing Address</h3>
<label class="radio" id="billing-same-div">
			 <input id="billing-same" name="billing_address_same" value="true" type="radio" checked><input name="_D:billing_address_same" value=" " type="hidden">
			 My shipping &amp; billing addresses are the same.
		</label>
		<div class="use-different">
		  <label class="radio">
			  <input id="billing-different" name="billing_address_same" value="false" type="radio"><input name="_D:billing_address_same" value=" " type="hidden">
			  Use a different billing address.
		  </label>
			<div class="different-address" style="display:none">
		<div class="new-address-form">
			<div class="form-group">
				  <label for="billing_address_1"><span class="required">*</span> Address line 1</label>
				  <input id="billing_address_1" maxlength="30" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.address1" value="" class="form-control address " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.address1" value=" " type="hidden"></div>
			  <div class="form-group">
				  <label for="billing_address_2">Address line 2</label>
				  <input id="billing_address_2" maxlength="30" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.address2" value="" class="form-control address " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.address2" value=" " type="hidden"></div>
			  <div class="form-group">
				  <label for="billing_city"><span class="required">*</span> City</label>
				  <input id="billing_city" maxlength="30" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.city" value="" class="form-control city " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.city" value=" " type="hidden"></div>
			  <div class="form-group">
				  <label for="billing_state"><span class="required">*</span> <span>Province</span></label>
				  <input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.state" value=" " type="hidden"></input><select id="billing_state" style="" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.state" class="form-control form-control select-state state "><option selected value="">select a province<option value="AB">AB - Alberta<option value="BC">BC - British Columbia<option value="MB">MB - Manitoba<option value="NB">NB - New Brunswick<option value="NL">NL - Newfoundland and Labrador<option value="NS">NS - Nova Scotia<option value="NT">NT - Northwest Territories<option value="NU">NU - Nunavut<option value="ON">ON - Ontario<option value="PE">PE - Prince Edward Island<option value="QC">QC - Quebec<option value="SK">SK - Saskatchewan<option value="YT">YT - Yukon</select></div>
				<div class="zip-country container form-group">
				  <div class="zip-code">
					<label for="billing_zip"><span class="required">*</span> <span>Zip Code</span></label>
					<input id="billing_zip" maxlength="10" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.postalCode" value="" class="form-control zip " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.postalCode" value=" " type="hidden"></div>
				  <input id="foreign_order_service" maxlength="10" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.foreignOrderService" value="BF2" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.foreignOrderService" value=" " type="hidden"><div class="country">
							  <label for="billing_country">Country</label>
								<input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.country" value=" " type="hidden"></input><select id="billing_country" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.country" class="form-control "><option value="">select a country<option value="AR">Argentina<option value="AU">Australia<option value="BH">Bahrain<option value="BR">Brazil<option value="CA">Canada<option value="CN">China<option value="DK">Denmark<option value="FR">France<option value="DE">Germany<option value="HK">Hong Kong<option value="IN">India<option value="ID">Indonesia<option value="IE">Ireland<option value="IL">Israel<option value="IT">Italy<option value="JP">Japan<option value="KR">Korea<option value="MX">Mexico<option value="NZ">New Zealand<option value="NO">Norway<option value="RU">Russia<option value="SA">Saudi Arabia<option value="SE">Sweden<option value="CH">Switzerland<option value="TW">Taiwan<option value="TR">Turkey<option value="AE">United Arab Emirates<option value="GB">United Kingdom<option value="US">United States</select></div>
						</div>
	<div class="phone-controls container">
	  <div class="form-group">
		<label for="billing_phone_1"><span class="required">*</span> Phone</label>
		<div id="isNotOnePhoneField">
			<input id="billing_phone_1" maxlength="3" pattern="\d*" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber_1" value="" class="form-control phone-control " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber_1" value=" " type="hidden"><input id="billing_phone_2" maxlength="3" pattern="\d*" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber_2" value="" class="form-control phone-control " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber_2" value=" " type="hidden"><input id="billing_phone_3" maxlength="4" pattern="\d*" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber_3" value="" class="form-control phone-control " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber_3" value=" " type="hidden"></div>
		<div id="isOnePhoneField">
				<input id="phone" maxlength="15" pattern="\d*" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber" value="" class="form-control " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.phoneNumber" value=" " type="hidden"></div>
	  </div>
	  <div class="form-group phone-ext" style="display: none;" >
		<label for="billing_phone_ext">Ext.</label>
		<input id="billing_phone_ext" maxlength="10" pattern="\d*" name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.extNumber" value="" class="form-control phone-control " type="text"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.formParamMap.extNumber" value=" " type="hidden"></div>
	</div>
	</div></div>
</div>
		<label class="save-card checkbox">
			<input name="save_card" value="true" type="checkbox"><input name="_D:save_card" value=" " type="hidden">
			Save this card for future purchases
		 </label>
		   <label class="make-default checkbox">
			 <input id="is_default" name="is_default" value="true" type="checkbox" disabled><input name="_D:is_default" value=" " type="hidden">
			 Make this my default credit card
		   </label>
		 </div>
			</div><p class="required-label">* Required</p>
		  </div>
		  <p class="payment-options"><a href="#" class="pop-info" rel="#pop-billing">billing and payment options</a></p>
		</div>

		<div class="module-right promotions">
		<h2 class="module-title">Promotions &amp; Gift Cards</h2>
		<div class="indent">
		  <h3 class="promo-hd expand"><a id="promolink" href="#">Have a promotion code? <span>+</span></a></h3>

<div class="add-promo-code hide">
  <h3 class="promo-hd">Use a promotion code</h3>
  <div class="container form-group">
		<input maxlength="40" class="form-control cart text promo-code" name="promo_code" id="promo_code" type="text" autocomplete="off">
		<button class="btn btn-primary btn-apply-code" name="apply-code" id="apply-code" value="Apply" type="button" class="apply">apply</button>
	</div>
	<p class="below-promo"><a href="#" rel="#modal-promo-code" class="btn btn-link btn-sm help-link">view current promos <span class="arrow arrow-right arrow-mini"></span></a></p></div>

</div>
	  </div>
		<input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.basketPage" value="/basket/basket.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.basketPage" value=" " type="hidden"><div class="module-action">
		<button class="btn btn-alt btn-lg btn-continue" value="continue" name="continue" type="submit">continue <span class="arrow arrow-right"></span></button>
	</div>
	<input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.addOrUpdate" value="continue" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.addOrUpdate" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.useForwards" value="true" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.useForwards" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.updatePaymentErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.updatePaymentErrorURL" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CreateCreditCardFormHandler.newCreditCardErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CreateCreditCardFormHandler.newCreditCardErrorURL" value=" " type="hidden"><input name="_DARGS" value="/checkout/updateCreditCard/checkout-payment.jsp" type="hidden"></input></form></div>
			</div>
<div id="password" class='checkout-module password disabled'>
	<div class="module-top"><h2 class="module-title">password <span class="btn btn-primary btn-sm btn-edit">edit</span></h2></div>
	<div class="module-summary container">
			<span class="OneLinkNoTx">
			  123&nbsp;123<br>
					adslfahskj@yahoo.com</span>
			<br>
			  &nbsp;<span class="OneLinkNoTx"></span>
	</div><div class="module-content container">

		<div class="indent">
			<form name="passwordAccordion" action="/checkout/accordions.jsp?_DARGS=/checkout/password.jsp" method="post"><input name="_dyncharset" value="UTF-8" type="hidden"></input><input name="_dynSessConf" value="5082249045805785135" type="hidden"></input><input type="hidden" name="showPasswordAccordion" value="yes"/>
				<input name="isSavedBIRewards" type="hidden" value=""/>
				<p class="intro-text">Set up your password and security questions to complete your order. <br>Passwords must contain between 6 and 12 characters.</p>

				<div class="user-info-summary" >
					<p id="namesummary">Name:&nbsp;123&nbsp;123&nbsp;&nbsp;&nbsp;<a class="edit-email" href="#">edit</a></p>
					<p> Email address:&nbsp;adslfahskj@yahoo.com</p>
				</div>


				<div class="edit-name-email" >
					<input id="hidden_firstName" value="123" type="hidden"/>
							<input id="hidden_lastName" value="123" type="hidden"/>
							<input id="hidden_newCustomerEmail" value="adslfahskj@yahoo.com" type="hidden"/>
							<fieldset class="two-col container">
								<div class="col form-group">
									<label for="first_name"><span class="required">*</span> First name</label>
									<input id="first_name" maxlength="33" name="/atg/userprofiling/ProfileFormHandler.value.firstName" value="123" class="form-control " type="text"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.firstName" value=" " type="hidden"></div>
								<div class="col form-group">
									<label for="last_name"><span class="required">*</span> Last name</label>
									<input id="last_name" maxlength="33" name="/atg/userprofiling/ProfileFormHandler.value.lastName" value="123" class="form-control " type="text"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.lastName" value=" " type="hidden"></div>
							</fieldset>
						</div>

				<input id="email" name="/atg/userprofiling/ProfileFormHandler.value.login" value="adslfahskj@yahoo.com" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.login" value=" " type="hidden"><input id="email_confirm" name="/atg/userprofiling/ProfileFormHandler.value.email" value="adslfahskj@yahoo.com" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.email" value=" " type="hidden"><fieldset class="two-col container">
					<div class="col form-group">
						<label for="password"><span class="required">*</span> Password</label>
						<input id="password" name="/atg/userprofiling/ProfileFormHandler.value.password" value="" class="form-control " type="password" autocomplete="off"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.password" value=" " type="hidden"></div>
					<div class="col form-group">
						<label for="password_confirm"><span class="required">*</span> Confirm password <a href="#" class="icon icon-help" rel="#pop-password"></a></label>
						<input id="password_confirm" name="/atg/userprofiling/ProfileFormHandler.value.confirmPassword" value="" class="form-control " type="password" autocomplete="off"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.confirmPassword" value=" " type="hidden"><div id="pop-password" class="modal modal-small">
  <div class="modal-header">
	<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title">Password</h4>
  </div>
  <div class="modal-body">
	<p>To keep your astoria.com account secure, you must create a unique password. It should be between 6 and 12 characters.</p>
	<p>You can always update your password in the future on the "My Account" page.</p>
  </div>
</div>
</div>
				</fieldset>

				<fieldset class="two-col container">
					<div class="col form-group">
						<label for="security_question"><span class="required">*</span> Security question</label>
						<input name="_D:/atg/userprofiling/ProfileFormHandler.value.securityQuestion" value=" " type="hidden"></input><select id="security-question" name="/atg/userprofiling/ProfileFormHandler.value.securityQuestion" class="form-control"><option value="Your favorite hairstyle?">Your favorite hairstyle?<option value="Your beauty idol's name?">Your beauty idol's name?<option value="Your favorite scent?">Your favorite scent?<option value="Your father's birthday (mm/dd/yy)?">Your father's birthday (mm/dd/yy)?<option value="Your grandmother's first name?">Your grandmother's first name?<option value="Your mother's maiden name?">Your mother's maiden name?<option value="Your mother's birthday (mm/dd/yy)?">Your mother's birthday (mm/dd/yy)?<option value="The name of your elementary school">The name of your elementary school<option value="Last 4 digits of your Social Security #?">Last 4 digits of your Social Security #?</select></div>
					<div class="col form-group">
						<label for="security_answer"><span class="required">*</span> Answer <a href="#" class="icon icon-help" rel="#pop-security-question"></a></label>
						<input id="security_answer" maxlength="40" name="/atg/userprofiling/ProfileFormHandler.value.securityAnswer" value="" class="form-control " type="text"><input name="_D:/atg/userprofiling/ProfileFormHandler.value.securityAnswer" value=" " type="hidden"><div id="pop-security-question" class="modal modal-small">
  <div class="modal-header">
	<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title">security question and answer</h4>
  </div>
  <div class="modal-body">
	<p>In case you forget your password while signing in to your account, we ask you to provide an additional security reminder.</p>
	<p><b>How it works:</b></p>
	<ul>
	  <li> Choose one question from the Security Question drop down and enter the answer in the Answer field.</li>
	  <li>For example: What is your father's birthday? Answer: 10/10/1944.</li>
	</ul>
	<p>The next time you visit astoria.com and forget your password, we will show you your Security Question. When you enter the correct answer, you can reset your password.</p>
  </div>
</div>
</div>
				</fieldset>

				<div class="email-signup">
					<label class="radio">
						<input id="email-signup-yes" name="email_signup" value="true" type="radio" checked><input name="_D:email_signup" value=" " type="hidden">
						Yes! Send me exclusive offers, beauty trend alerts, and store news via email.
					</label>
					<label class="radio">
						<input id="email-signup-no" name="email_signup" value="false" type="radio"><input name="_D:email_signup" value=" " type="hidden">
						No, I prefer not to receive email offers at this time.
					</label>
				</div>

				<div class="module-action">
					<input name="/atg/userprofiling/ProfileFormHandler.isConfirmEmail" value="true" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.isConfirmEmail" value=" " type="hidden"><input name="/atg/userprofiling/ProfileFormHandler.confirmPassword" value="true" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.confirmPassword" value=" " type="hidden"><input name="/atg/userprofiling/ProfileFormHandler.basketPage" value="/basket/basket.jsp" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.basketPage" value=" " type="hidden"><button class="btn btn-alt btn-lg btn-continue" name="continue" type="submit">continue <span class="arrow arrow-right"></span></button>
							<input type="hidden" name="showPasswordAccordion" value="yes"/>
							<input name="/atg/userprofiling/ProfileFormHandler.registrationType" value="astoriaAccountRegistrationNormal" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.registrationType" value=" " type="hidden"><input name="/atg/userprofiling/ProfileFormHandler.createForCheckout" value="continue" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.createForCheckout" value=" " type="hidden"><input id="submitError" name="/atg/userprofiling/ProfileFormHandler.submitError" value="" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.submitError" value=" " type="hidden"></div>

			<input name="_DARGS" value="/checkout/password.jsp" type="hidden"></input></form><p class="required-label">* Required</p>
		</div></div></div>
<div id="order" class='checkout-module order disabled'>
	<div class="module-top">
	  <h2 class="module-title">place order</h2>
	</div>
		<div class="module-content container">
	  <form name="placeOrder" action="/checkout/accordions.jsp?_DARGS=/checkout/placeOrder.jsp" method="post"><input name="_dyncharset" value="UTF-8" type="hidden"></input><input name="_dynSessConf" value="5082249045805785135" type="hidden"></input><input type="hidden" name="form_variable" id="form_variable"/>
		<input type="hidden" name="showPasswordAccordion" value="yes"/>
				<input name="isSavedBIRewards" type="hidden" value=""/>
				<div class="module-action pull-right">
		  <input name="/atg/commerce/order/purchase/CommitOrderFormHandler.basketPage" value="/basket/basket.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CommitOrderFormHandler.basketPage" value=" " type="hidden"><button class="btn btn-alt btn-lg btn-place-order" value="continue" name="/atg/commerce/order/purchase/CommitOrderFormHandler.CommitOrder" type="submit">place order</button>
					<input name="place order" value="place order" type="hidden"><input name="_D:place order" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CommitOrderFormHandler.placeOrderPage" value="/checkout/placeOrder.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CommitOrderFormHandler.placeOrderPage" value=" " type="hidden"></div>

		<p class="po-msg">
		  Please review your order information.<br>
		  If everything is correct, click the Place Order button.</p>

				<p class="borderfree-terms text-muted">By placing your order, you agree to the <a href="http://www.borderfree.com/consumer-terms" target="_blank">Terms &amp; Conditions</a> and <a href="http://www.borderfree.com/consumer-privacy" target="_blank">Privacy Policy</a> of <a href="http://www.borderfree.com/contact" target="_blank">BorderFree</a>,<br>astoria’s international fulfillment service.</p>
				<input name="/atg/commerce/order/purchase/CommitOrderFormHandler.CommitOrderSuccessURL" value="/profile/orderConfirmation/orderConfirmation.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CommitOrderFormHandler.CommitOrderSuccessURL" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CommitOrderFormHandler.CommitOrderErrorURL" value="/checkout/accordions.jsp" type="hidden"><input name="_D:/atg/commerce/order/purchase/CommitOrderFormHandler.CommitOrderErrorURL" value=" " type="hidden"><input name="/atg/commerce/order/purchase/CommitOrderFormHandler.useForwards" value="true" type="hidden"><input name="_D:/atg/commerce/order/purchase/CommitOrderFormHandler.useForwards" value=" " type="hidden"><input name="_DARGS" value="/checkout/placeOrder.jsp" type="hidden"></input></form></div></div><script src="/javascripts/jscCollection.js"></script>
  <script>
  $(function() {
	 util.parse('form_variable');
	});
  </script>

</div>
	  <div class="checkout-sidebar">
	  	<div class="my-basket" style="margin-top:40px">
			<h2 class="module-title">
				My Basket
				<a class="btn btn-primary btn-sm btn-back" href="/basket/basket.jsp">back</a>
			</h2>
			<span id="currency" data-currency="C$"></span>
			<div class="my-basket-content">
				<div class="product-list custom-scroll">
					<div class="products" style="display: block;">
						<div id="sku1539543" data-skutype="Standard" data-isbi="" data-bitype="None" data-productid="P384728" class="product-row container">
							<div class="product-image">
								<img width="42" height="42" src="/user_account/customer/templates/default/img/s1539543-main-thumb.jpg" alt="">
							</div>
							<div class="product-description">
								<h3>
									<div class="product-price">

										<span class="list-price">
											<span class="price ">C$176.00</span>
										</span>

									</div>
									<span class="OneLinkNoTx">

										<span class="brand">bareMinerals</span>
										<span class="product-name">Beauty In Bloom</span>

									</span>
								</h3>

								<div class="info-row">
									<span class="sku">
										<span class="label">Item #</span>
										<span class="value OneLinkNoTx">1539543</span>
									</span>
									<span class="qty">
										<span class="label">QTY</span>
										<span class="value">4</span>
									</span>
								</div>

							</div>
						</div>
						<div id="sku562082" data-skutype="Standard" data-isbi="" data-bitype="None" data-productid="P12295" class="product-row container">
							<div class="product-image">
								<img width="42" height="42" src="/user_account/customer/templates/default/img/s1539543-main-thumb.jpg" alt=""></div>
							<div class="product-description">
								<h3>
									<div class="product-price">

										<span class="list-price">
											<span class="price ">C$204.00</span>
										</span>

									</div>
									<span class="OneLinkNoTx">

										<span class="brand">Fresh</span>
										<span class="product-name">Sugar Face Polish</span>

									</span>
								</h3>

								<div class="info-row">
									<span class="sku">
										<span class="label">Item #</span>
										<span class="value OneLinkNoTx">562082</span>
									</span>
									<span class="qty">
										<span class="label">QTY</span>
										<span class="value">3</span>
									</span>
								</div>

							</div>
						</div>
						<div id="sku1097310" data-skutype="Sample" data-isbi="" data-bitype="None" data-productid="P370205" class="product-row container">
							<div class="product-image">
								<img width="42" height="42" src="/user_account/customer/templates/default/img/s1539543-main-thumb.jpg" alt="">
							</div>
							<div class="product-description">
								<h3>
									<div class="product-price">

										<span class="list-price">
											<span class="price ">FREE</span>
										</span>

									</div>
									<span class="OneLinkNoTx">

										<span class="brand">Prada</span>
										<span class="product-name">Prada Amber Pour Homme</span>

									</span>
								</h3>

								<div class="info-row">
									<span class="sku">
										<span class="label">Item #</span>
										<span class="value OneLinkNoTx">1097310</span>
									</span>
									<span class="qty">
										<span class="label">QTY</span>
										<span class="value">1</span>
									</span>
								</div>

							</div>
						</div>
						<div id="sku1133024" data-skutype="Sample" data-isbi="" data-bitype="None" data-productid="P370205" class="product-row container">
							<div class="product-image">
								<img width="42" height="42" src="/user_account/customer/templates/default/img/s1539543-main-thumb.jpg" alt="">
							</div>
							<div class="product-description">
								<h3>
									<div class="product-price">

										<span class="list-price">
											<span class="price ">FREE</span>
										</span>

									</div>
									<span class="OneLinkNoTx">

										<span class="brand">Jean Paul Gaultier</span>
										<span class="product-name">LE MALE</span>

									</span>
								</h3>

								<div class="info-row">
									<span class="sku">
										<span class="label">Item #</span>
										<span class="value OneLinkNoTx">1133024</span>
									</span>
									<span class="qty">
										<span class="label">QTY</span>
										<span class="value">1</span>
									</span>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="calculation">
				<ul class="container">
					<li>
						<span class="line-item">Merchandise Subtotal</span>
						<span class="currency">C$</span>
						<span class="cost merch-total">380.00</span>
					</li>
					<li class="discount" style="display:none;">
						<span class="line-item">Discounts</span>
						<span class="currency">-C$</span>
						<span class="cost"></span>
					</li>

					<li>
						<span class="line-item">Shipping &amp; Handling</span>
						<span class="currency" style="display:none;">C$</span>
						<span class="free shipping">FREE</span>
					</li>
					<li>
						<span class="line-item">Taxes</span>
						<span class="currency">C$</span>
						<span class="cost tax">19.66</span>
					</li>
					<li>
						<span class="line-item">Duty</span>
						<span class="currency">C$</span>
						<span class="cost tax">10.32</span>
					</li>
					<li class="subtotal" style="display:none;">
						<span class="line-item">Order Subtotal</span>
						<span class="currency">C$</span>
						<span class="cost sub">409.98</span>
					</li>

					<li class="credit store" style="display:none;">
						<span class="line-item">Store Credit</span>
						<span class="currency">-C$</span>
						<span class="cost"></span>
					</li>

					<li class="credit gift" style="display:none;">
						<span class="line-item">Gift Card</span>
						<span class="currency">-C$</span>
						<span class="cost"></span>
					</li>

					<li class="credit egift" style="display:none;">
						<span class="line-item">eGift Certificate</span>
						<span class="currency">-C$</span>
						<span class="cost"></span>
					</li>

					<li class="total">
						<span class="line-item">Order Total</span>
						<span class="currency">C$</span>
						<span class="cost total">409.98</span>
					</li>
				</ul>
			</div>
		</div>
	</div>