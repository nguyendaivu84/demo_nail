<div class="refinements checkboxes   ">
    <a href="#" class="clear-link hide" data-multi="" style="display: none;">clear filter</a>
    <h4 class="refinement-subhead">[@NAME_FILTER]</h4>
    <div class="checkbox-list  custom-scroll">
        <label class="checkbox">
            [@FILTER_ALL_ITEM]
        </label>
    </div>
</div>

