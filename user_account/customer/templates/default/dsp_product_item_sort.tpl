<script>
    function change_sort(_val,event){
        if(_val==1){
            window.location.href='[@URL_SORT]'+'&sort='+$("#sortBy_"+event).val();
        }else{
            window.location.href='[@URL_PAGESIZE]'+'&num_row='+$("#pageSize_"+event).val();
        }
    }
</script>
<div class="pagination paging-bottom container">
    <div class="page-numbers">
        <ul>[@PAGING]</ul>
    </div>
    <div class="sort-by">
        <label class="inline" >[@LANGUAGE_SORT_BY]</label>
        <select id="sortBy_[@INDEX]" name="sortBy_[@INDEX]" class="form-control sortBy" onchange="change_sort(1,'[@INDEX]');" >
            [@PRODUCT_SORT_OPTION]
        </select>
    </div>
    <div class="view">
        <label class="inline">[@LANGUAGE_VIEW]</label>
        <select name="pageSize_[@INDEX]" id="pageSize_[@INDEX]"  class="form-control" onchange="change_sort(2,'[@INDEX]');">
            [@NUMBER_ROW_OPTION]
        </select>
    </div>
</div>