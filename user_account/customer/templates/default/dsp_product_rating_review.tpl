<div id="ratings-reviews" class="ratings-reviews">
	<div class="ratings-content">
		<h3 class="section-title">[@RATING_REVIEWS]</h3>
		<ul class="actions list-unstyled">
			<li>
				<a href="http://reviews.astoria.com/8723illuminate/P384728/writereview.htm?format=embedded&amp;campaignid=BV_RATING_SUMMARY&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp"> <b><span>»</span>
						[@WRITE_REVIEW]</b>
				</a>
			</li>
			<li>
				<a href="http://answers.astoria.com/answers/8723illuminate/product/P384728/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP384728%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp"> <b><span>»</span>
						[@ASK_QUESTION]</b>
				</a>
			</li>
		</ul>
		<ul class="nav nav-tabs product-tabs">
			<li class="active ratings">
				<a href="#bv-ratings" data-toggle="tab">[@TAB_RATING_REVIEW]</a>
			</li>
			<li class="q-a">
				<a href="#bv-qa" data-toggle="tab">[@TAB_Q_A]</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active fade in" id="bv-ratings">
				<div id="BVReviewsContainer" class="BVBrowserWebkit">
					<div id="BVRRWidgetID" class="BVRRRootElement BVRRWidget">
						<div id="BVRRContentContainerID" class="BVRRContainer">
                            [@RATING_FORM]
							<div id="BVRRDisplayContentID" class="BVRRDisplayContent">
								<div id="BVRRDisplayContentHeaderID" class="BVRRHeader BVRRDisplayContentHeader">
									<div class="BVRRDisplayContentHeaderContent">
										<span id="BVRRDisplayContentTitleID" class="BVRRTitle BVRRDisplayContentTitle"></span>
										<span id="BVRRDisplayContentSubtitleID" class="BVRRSubtitle BVRRDisplayContentSubtitle">
											<span id="BVRRDisplayContentLinkWriteID" class="BVRRContentLink BVRRDisplayContentLinkWrite">
												<a title="review this product" name="BV_TrackingTag_Review_Display_WriteReview" target="BVFrame" onclick="bvShowContentOnReturnPRR('8723illuminate', 'P384728', '');" href="http://reviews.astoria.com/8723illuminate/P384728/writereview.htm?format=embedded&amp;campaignid=BV_REVIEW_DISPLAY&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp">review this product</a>
											</span>
										</span>
										<span class="BVRRSortAndSearch">
											<span id="BVRRDisplayContentSortID" class="BVRRDisplayContentSort">
												<span id="BVRRDisplayContentSortPrefixID" class="BVRRLabel BVRRDisplayContentSortPrefix">sort by</span>
												<span class="BVRRSortSelectWidget">
													<select id="BVRRDisplayContentSelectBVFrameID" name="BV_TrackingTag_Review_Display_Sort" class="BVRRSelect BVRRDisplayContentSelect" onchange="">
                                                        [@SORT_RATING_ITEM]
													</select>
												</span>
												<span id="BVRRDisplayContentSortSuffixID" class="BVRRLabel BVRRDisplayContentSortSuffix"></span>
											</span>
										</span>
									</div>
								</div>
								<div id="BVRRDisplayContentBodyID" class="BVRRDisplayContentBody">
									[@RATING_ITEMS]
								</div>
								<div id="BVRRDisplayContentFooterID" class="BVRRFooter BVRRDisplayContentFooter">
									<div class="BVRRPager BVRRPageBasedPager">
                                        [@RATING_ITEMS_PAGETION]
									</div>
								</div>
								<div class="BVRRSpacer BVRRDisplayContentSpacer"></div>
							</div>
						</div>
					</div>
				</div>
				<noscript>
					&lt;iframe src="http://reviews.astoria.com/8723Illuminate/P384728/reviews.htm?format=noscript" width="100%"&gt;&lt;/iframe&gt;&lt;br/&gt;
				</noscript>
			</div>
			<div class="tab-pane fade" id="bv-qa">
				<div id="BVQAContainer" class="BVBrowserWebkit" style="">
					<div id="BVQAWidgetID" class="BVQAWidget">
						<!-- START QUESTION AND ANSWER -->
                        <div id="BVQAHomePageID" class="BVQAWidgetWrapper BVQAHomePage">
                            <div id="BVQAHeaderID" class="BVQAHeader">
                                <div id="BVQAQuestionAndAnswerCountID" class="BVQAQuestionAndAnswerCount">
                                    Questions (
									<span class="BVQACount BVQANonZeroCount">
										See All Q's
										<span class="BVQANumberSpan">
											(
											<span class="BVQANumber">2</span>
											)
										</span>
									</span>
                                    ) &nbsp; Answers (
									<span class="BVQACount BVQANonZeroCount">
										&amp; A's
										<span class="BVQANumberSpan">
											(
											<span class="BVQANumber">9</span>
											)
										</span>
									</span>
                                    )
                                </div>
                                <h1 id="BVQAHeaderTitleID" class="BVQATitle BVQAHeaderTitle"></h1>
                                <h2 id="BVQAHeaderSubTitleID" class="BVQASubTitle BVQAHeaderSubTitle">Ask your questions. Share your answers.</h2>
                            </div>
                            <div id="BVQAMainID" class="BVQAMain BVQAMainView">
                                <div class="BVQAPageTabs">
                                    <div class="BVQAPageTabSpacerLeft">&nbsp;</div>
                                    <div id="BVQAPageTabHomeID" class="BVQAPageTab BVQASelectedPageTab">HOME</div>
                                    <div class="BVQAPageTabSpacerMiddle">&nbsp;</div>
                                    <div id="BVQAPageTabBrowseID" class="BVQAPageTab" onclick="$BV.Internal.Requester.get('http://answers.astoria.com/answers/8723illuminate/product/P384728/questions.htm?format=embedded','BVQAFrame',''); return false;">
                                        <a title="BROWSE" name="BV_TrackingTag_QA_Display_BrowseTab" target="BVQAFrame" class="BVQAPageTabLink" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/questions.htm?format=embedded">BROWSE</a>
                                    </div>
                                    <div class="BVQAPageTabSpacerMiddle">&nbsp;</div>
                                    <div id="BVQAPageTabSearchID" class="BVQAPageTab" onclick="$BV.Internal.Requester.get('http://answers.astoria.com/answers/8723illuminate/product/P384728/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__','BVQAFrame',''); return false;">
                                        <a title="SEARCH" name="BV_TrackingTag_QA_Display_SearchTab" target="BVQAFrame" class="BVQAPageTabLink" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__">SEARCH</a>
                                    </div>
                                    <div class="BVQAPageTabSpacerRight">&nbsp;</div>
                                </div>
                                <div class="BVQASearchForm">
                                    <div class="BVQASearchFormText">
                                        Search:
                                        <input type="text" id="BVQASearchFormTextInputID" maxlength="150" class="BVQASearchFormTextInput BVQASearchFormTextInputDefault" onkeypress="return bvDisableReturn(event, function(){bvqaSubmitSearch('http://answers.astoria.com/answers/8723illuminate/product/P384728/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__','','');BVAnalyticsTracker.fireActionEvent(document.getElementById('BVQASearchFormSubmitButtonID'));});" onfocus="this.value = ''; this.className = 'BVQASearchFormTextInput'; this.onfocus = null;"></div>
                                    <div class="BVQASearchFormSubmit">
                                        <button id="BVQASearchFormSubmitButtonID" class="BVQASearchFormSubmitButton" name="BV_TrackingTag_QA_Display_SearchButton" onclick="bvqaSubmitSearch('http://answers.astoria.com/answers/8723illuminate/product/P384728/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__','','');return false;" data-bvtrack="eName:Search"></button>
                                    </div>
                                    <div id="BVQAAskQuestionHeaderID" class="BVQAAskQuestion">
                                        <a title="Ask a New Question&nbsp;" target="BVQAFrame" onclick="return typeof(BVQAOnSubmit)=='function' ? BVQAOnSubmit(this.href, 'QUESTION_SUBMISSION') : true;" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP384728%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp">
                                            <img src="http://answers.astoria.com/answers/8723illuminate/static/buttonAskANewQuestion.gif" alt="Ask a New Question&nbsp;"></a>
                                    </div>
                                </div>
                                <div id="BVQAQuestionsID0" class="BVQAQuestions">
                                    <div id="BVQAQuestionGroupHeaderFirstID" class="BVQAQuestionGroupHeader BVQAQuestionGroupHeaderFirst">Questions with most helpful answers:</div>
                                    <div id="BVQAQuestionAndAnswers1155502" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
                                        <div id="BVQAQuestionHeader01155502" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
                                            <div class="BVQAQuestionHeaderBullet">&nbsp;</div>
                                            <div class="BVQAQuestionAnswersCount">
												<span class="BVQACount BVQANonZeroCount">
													<span class="BVQANumber">6</span>
													answers
												</span>
                                            </div>
                                            <h1 class="BVQAQuestionSummary">
                                                <a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_1155502" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['1383210','1378666','1377477','1357830','1355757','1351321'],'questionIds':['1155502']});" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/questions.htm?format=embedded&amp;sort=helpfula&amp;expandquestion=1155502">Are these full size products?</a>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="BVQAQuestionsID1" class="BVQAQuestions">
                                    <div class="BVQAQuestionGroupHeader">Can you answer these questions?</div>
                                    <div id="BVQAQuestionAndAnswers1176477" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
                                        <div id="BVQAQuestionHeader11176477" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
                                            <div class="BVQAQuestionHeaderBullet">&nbsp;</div>
                                            <div class="BVQAQuestionAnswersCount">
												<span class="BVQACount BVQANonZeroCount">
													<span class="BVQANumber">3</span>
													answers
												</span>
                                            </div>
                                            <h1 class="BVQAQuestionSummary">
                                                <a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_1176477" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['1383208','1381937','1381605'],'questionIds':['1176477']});" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/questions.htm?format=embedded&amp;sort=answers&amp;dir=asc&amp;expandquestion=1176477">
                                                    Does this set include the double-ended brush shown in the picture?
                                                </a>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="BVQAAskQuestionHeaderID" class="BVQAAskQuestion">
                                    <a title="Ask a New Question&nbsp;" name="BV_TrackingTag_QA_Display_AskQuestion" target="BVQAFrame" onclick="bvShowContentOnReturnQA('8723illuminate', 'P384728', 'BVQAAskQuestionID');return typeof(BVQAOnSubmit)=='function' ? BVQAOnSubmit(this.href, 'QUESTION_SUBMISSION') : true;" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP384728%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp">
                                        <img src="http://answers.astoria.com/answers/8723illuminate/static/buttonAskANewQuestion.gif" alt="Ask a New Question&nbsp;" title="Ask a New Question&nbsp;"></a>
                                </div>
                            </div>
                            <div id="BVQAFooterID" class="BVQAFooter">
                                <div id="BVQAGuidelinesID" class="BVQAGuidelines">
                                    <a name="BV_TrackingTag_QA_Display_QAndAGuidelines" href="http://answers.astoria.com/answers/8723illuminate/content/guidelines.htm" onclick="window.open(this.href,null,'left=50,top=50,width=500,height=500,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" title="Product Q&amp;A guidelines" id="BVQAGuidelinesLinkID" class="BVQAPopupLink BVQAGuidelinesLink">See full product Q&amp;A guidelines</a>
                                </div>
                                <div id="BVQAAskQuestionID" class="BVQAAskQuestion">
                                    <a title="Ask a New Question&nbsp;" name="BV_TrackingTag_QA_Display_AskQuestion" target="BVQAFrame" onclick="bvShowContentOnReturnQA('8723illuminate', 'P384728', 'BVQAAskQuestionID');return typeof(BVQAOnSubmit)=='function' ? BVQAOnSubmit(this.href, 'QUESTION_SUBMISSION') : true;" href="http://answers.astoria.com/answers/8723illuminate/product/P384728/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP384728%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp">
                                        <img src="http://answers.astoria.com/answers/8723illuminate/static/buttonAskANewQuestion.gif" alt="Ask a New Question&nbsp;" title="Ask a New Question&nbsp;"></a>
                                </div>
                            </div>
                        </div>
                        <!-- END -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>var bvProdId = "P384728";</script>
	<div id="BVSubmissionParameters" style="display: none;"></div>
    <div id="BVQACustomerID" style="display: none;">__USERID__</div>
	<!--div id="BVContainerPageURL" style="display: none;">http://www.astoria.com/product/bv/review.jsp</div>
	<div id="BVQASubmissionURL" style="display: none;">http://www.astoria.com/product/bv/askAnswer.jsp</div-->
	<iframe id="BVFrame" name="BVFrame" src="//reviews.astoria.com/8723Illuminate/P384728/reviews.htm?format=embedded" style="visibility: hidden; width: 1px; height: 1px; position: absolute; left: -999px; top: -999px;"></iframe>
	<iframe id="BVQAFrame" name="BVQAFrame" src="//answers.astoria.com/answers/8723Illuminate/product/P384728/questionshome.htm?format=embedded" style="visibility: hidden; width: 1px; height: 1px; position: absolute; left: -999px; top: -999px;"></iframe>
</div>