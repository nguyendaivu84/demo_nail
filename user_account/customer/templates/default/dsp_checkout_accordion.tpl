<script>
    $(document).ready(function(){
        $("#state").on("change",function(){
            update_tax_price($("#state option:selected").val(),'[@URL]');
        });
        $("#btn_yes").on("click",function(){
            $("#checkout-signout-hidden").trigger("click");
        });
        $("#btn_no").on("click",function(){
            $("#pop-signout-warning").bPopup().close();
            $("#pop-signout-warning").bPopup().close();
        });
        $("#checkout-signout").on("click",function(){
            $("#pop-signout-warning").bPopup();
        });
    });
</script>
<div class="banner-container"> </div>
<div class="main" style=" font-family: fontsDosis; font-weight: 400; font-style: normal; font-size: 14px;">
    <div class="page-intro container">
        <h1  style="text-transform:capitalize;font-weight: bold;">
          Checkout
        </h1>
        <p class="greeting" style="[@DISPLAY]" >
            <span class="OneLinkNoTx">[@USER_NAME]</span>
            <button name="checkout-signout" id="checkout-signout" class="btn btn-primary btn-sm btn-back" type="submit">sign out</button>
            <form method="post" action="" style="text-align: right">
                <button style="display: none" name="checkout-signout-hidden" id="checkout-signout-hidden" class="btn btn-primary btn-sm btn-back" type="submit">sign out</button>
            </form>
        </p>
    </div>
    <div id="shipping" class="checkout-module divided shipping current" >
        <div class="module-top">
            <h2 class="module-title">Ship To &amp; Delivery</h2>
        </div>
        <div class="module-summary container">
            <div class="module-left" data-shipping-id="">
                <h2 class="module-title">Ship To <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>
            </div>
        <div class="module-right">
            <h2 class="module-title">Delivery <span class="btn btn-primary btn-sm btn-edit">edit</span></h2>
            <div class="indent">
                <p data-ship-method-summary=""></p>
            </div>
        </div>
    </div>
    <div class="module-content container">
        <form novalidate="novalidate" name="shippingAccordion" action="" method="post">
            <div><p style="color:red">[@ERROR_MESSAGE]</p></div>
            <div class="module-left">
                <h2 class="module-title">Ship To</h2>
                <div class="indent">
                    <div class="address-actions">
                        <a href="#" class="btn btn-default btn-sm btn-add-new-address hidden">add new address</a>
                    </div>
                <div class="address editable-address"  style="display:none">
                    <p class="OneLinkNoTx"> </p>
                </div>
                <div class="ship-form" style="display:block">
                    <div class="form-group">
                        <label for="first_name"><span class="required">*</span> Email</label>
                        <input id="user_email" maxlength="33" name="user_email" value="[@USER_EMAIL]" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="first_name"><span class="required">*</span> First name</label>
                        <input id="first_name" maxlength="33" name="first_name" value="[@FIRST_NAME]" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="last_name"><span class="required">*</span> Last name</label>
                        <input id="last_name" maxlength="33" name="last_name" value="[@LAST_NAME]" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="address1"><span class="required">*</span> Address line 1</label>
                        <input id="address1" maxlength="30" name="address1" value="[@ADDRESS_LINE_1]" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="address2">Address line 2</label>
                        <input id="address2" maxlength="30" name="address2" value="[@ADDRESS_LINE_2]" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="city"><span class="required">*</span> City</label>
                        <input id="city" maxlength="30" name="city" value="[@ADDRESS_CITY]" class="form-control city" type="text">
                    </div>
                    <div class="form-group">
                        <label for="state"><span class="required">*</span> Province:</label>
                        <select id="state" style="" name="state" class="form-control form-control select-state cc_type">
                            <!--option selected value="">select a province</option-->
                            [@STATE_OPTION]
                        </select>
                    </div>
                    <div class="zip-country container form-group">
                        <label for="zip"><span class="required">*</span> Postal Code</label>
                        <div class="zip-code">
                            <input id="zip" maxlength="10" name="zip" value="[@POSTAL_CODE]" class="form-control zip" type="text">
                        </div>
                    <div class="country country-label">Country: Canada</div>
                        <input id="country" maxlength="10" name="country" value="CA" type="hidden">
                    </div>
                    <div class="phone-controls container">
                        <div class="form-group">
                            <label for="phone_1"><span class="required">*</span> Phone</label>
                            <input id="phone_1" maxlength="3" pattern="*" name="phone_1" value="[@PHONE_1]" class="form-control phone-control" type="text">
                            <input id="phone_2" maxlength="3" pattern="*" name="phone_2" value="[@PHONE_2]" class="form-control phone-control" type="text">
                            <input id="phone_3" maxlength="4" pattern="*" name="phone_3" value="[@PHONE_3]" class="form-control phone-control" type="text">
                        </div>
                        <div class="form-group phone-ext">
                            <label for="phone_ext">Ext.</label>
                            <input id="phone_ext" maxlength="10" pattern="*" name="phone_ext" value="[@PHONE_EXT]" class="form-control phone-control" type="text">
                        </div>
                    </div>
                </div>
            <label class="set-default checkbox">
                <input id="is_default" name="is_default" value="1" type="checkbox">
                <input id="default_address_index"  name="default_address_index" value="[@DEFAULT_ADDRESS]" type="hidden"/>
                Set as my default shipping address
            </label>
            <div class="delete-address" style="display:block">
                <a href="#" class="btn btn-primary btn-sm btn-back">Delete address</a>
            </div>
            <div class="edit-address-btn" style="display:none">
                <button type="button" class="btn btn-default btn-sm btn-edit" data-address_id="">edit</button>
            </div>
            <div class="switch"><a href="#"></a></div>
            <p class="required-label">* Required</p>
        </div>
    </div>
            <div class="module-right">
                  <h2 class="module-title">Delivery</h2>
                  <div class="indent">
                    <div class="warning-message not-allowed-shipping-msg" style="display:none;">
                        <p>The selected delivery method is not valid with the promotion code &lt;promo code&gt;. You must select &lt;valid shipping methods&gt; to qualify. If you continue, the shipping promotion will be removed.</p>
                    </div>
                    <div class="delivery-method">
                        [@OPTION_METHOD]
                        <p>* Handling fees may apply<br><br> </p>
                        <!--p><a rel="#pop-shipping" class="btn btn-default btn-sm pop-info" href="#">Shipping &amp; Handling Information</a></p-->
                    </div>
                  </div>
                </div>
            <div class="module-action">
                <button class="btn btn-alt btn-lg btn-continue" value="continue" name="continue" type="submit">continue <span class="arrow arrow-right"></span></button>
            </div>
        </form>
    </div>
</div>
</div>
<div class="checkout-sidebar" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
<div class="my-basket" style="margin-top:40px">
    <h2 class="module-title">
        My Shopping cart
        <a class="btn btn-primary btn-sm btn-back" href="/checkout">back</a>
    </h2>
    <span id="currency" data-currency="C$"></span>
    <div class="my-basket-content">
        <div class="product-list custom-scroll">
            <div class="products" style="display: block;">
                [@BASKET_LIST_ITEM]
            </div>
        </div>
    </div>
    <div class="calculation">
        <ul class="container" style="font-size:14px">
            <li>
                <span class="line-item">Merchandise Subtotal</span>
                <span class="currency">C$</span>
                <span id="total_product_sell_price" class="cost merch-total">[@PRODUCT_ALL_PRICE]</span>
            </li>
            <li>
                <span class="line-item">Taxes</span>
                <span class="currency">C$</span>
                <span id="total_tax" class="cost tax">[@TAX_PRICE]</span>
            </li>
            <li class="total">
                <span class="line-item">Order Total</span>
                <span class="currency">C$</span>
                <span id="total_price_tax" class="cost total">[@TOTAL_PRICE]</span>
            </li>
        </ul>
    </div>
</div>
</div>
<div style="display:none" id="pop-signout-warning" class="modal modal-small" style="top: 274.5px; margin-left: -164px; display: block;">
    <div class="modal-header">
        <h4 class="modal-title">Sign Out</h4>
    </div>
    <div class="modal-body signout-warning">
        <p class="warning-message large">If you choose to sign out now, the information you have supplied in checkout will not be saved. Do you want to sign out?</p>
    </div>
    <div class="modal-footer">
        <form action="" name="signout-warning-popup">
            <button class="btn btn-primary btn-lg btn-yes" id="btn_yes" name="yes" value="yes" type="button">yes</button>
            <button class="btn btn-default btn-lg btn-no" id="btn_no" name="no" value="no" type="button">no</button>
        </form>
    </div>
</div>
</div>