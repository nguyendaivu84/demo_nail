<link rel="stylesheet" href="/user_account/customer/templates/default/css/search.css">
<div class="main-wrap wrapper">
	<ul  class="breadcrumb" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
		[@CATEGORY_UL]
	</ul>
	<div class="content">
		[@SEARCH_SIDEBAR]
		<div id="main" class="maincontent">[@SEARCH_CONTENT]</div>
	</div>
	<img id="loader-precached" src="/images/loader_black_155_V2.gif" width="155" height="155" alt="" class="hidden">
</div>