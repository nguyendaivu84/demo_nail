<div class=" carousel-component  display-4"  data-name="carousel hp_CAN_editorspicks_carousel" data-lazyload='true'>
    <h2 class="carousel-title">[@GROUP_NAME]</h2>
    <!--a class="view-all" href="/[@CAT_SLUGGER]/">see more</a-->
    <div class="carousel" data-speed="550" data-show="4" data-circle="true" >
        <ul class="carousel-inner container">[@SLIDER_ITEM_CONTENT]</ul>
        <div class="carousel-control carousel-prev">
            <span class="icon icon-arrow-left"></span>
        </div>
        <div class="carousel-control carousel-next">
            <span class="icon icon-arrow-right"></span>
        </div>
    </div>
</div>