<div class="sidebar">
    <div class="sidenav search-navigation">
        <ul class="nav nav-stacked ">
            [@DSP_CG_SEARCH_SIDEBAR_CATEGORY]
        </ul>
    </div>
    <div class="clear-all-filters top" style="display: block;">
        <h3 class="refinement-title">FILTER BY:</h3>
        <input type="submit" value="clear all filters" class="btn btn-default">
    </div>
    <div id="refinements" class="refinements-list"><div class="refinements checkboxes   ">
            <a href="#" class="clear-link hide" data-multi="">clear filter</a>
            <h4 class="refinement-subhead">Brand</h4>
            <div class="checkbox-list custom-scroll">
                [@DSP_CG_SEARCH_SIDEBAR_BRAND_ITEM]
            </div>

        </div>
        <div class="refinements slider-bar">
            <a href="#" class="clear-link hide">clear filter</a>
            <h4 class="refinement-subhead">price range</h4>

            <div class="slider-output">
                <input type="text" class="form-control slider-amount" readonly="readonly"></div>
            <div id="price range" class="slider-range refinement ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false">
                <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
                <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"></a>
            </div>
            <div class="orig-range container">
                <div class="price-min">
                    <span>C$20</span>
                </div>
                <div class="price-max">
                    <span>C$95</span>
                </div>
            </div>
        </div>
    </div>

    <div class="clear-all-filters" style="display: block;">
        <input type="submit" value="clear all filters" class="btn btn-default">
    </div>

    <div class="social-links social-components">
        <div class="share-btn icon icon-facebook-large social-enabled hidden" data-socialtype="fb" data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=">
            <a href="#" target="_blank"></a>
        </div>
        <div id="pinterest-root" class="pin hidden share-btn icon icon-pinterest-large">
            <a data-pin-config="beside" href="#" data-pin-do="buttonPin">
                <img src="/user_account/customer/templates/default/img/pin_it_button.png"></a>
        </div>
    </div>
</div>
<!-- END SIDEBAR -->