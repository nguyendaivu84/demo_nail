<div class="product-item">
    <div class="product-image">
        <a href="/[@PRODUCT_SLUGGER]">
            <img src="[@PRODUCT_IMAGE]" style="width:135px" alt="[@PRODUCT_NAME]">
        </a>
    </div>
    <div class="new-love">
        [@PRODUCT_NEW]
    </div>
    <a href="/[@PRODUCT_SLUGGER]">
    	<span class="product-info">
			<div class="name OneLinkNoTx" style="min-height: 50px;">
                <span class="brand OneLinkNoTx">[@PRODUCT_NAME]</span>
                <span>[@PRODUCT_DES]</span>
            </div>
		</span>
    </a>
</div>