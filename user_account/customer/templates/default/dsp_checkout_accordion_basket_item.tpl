<div id="sku_[@PRODUCT_ID]" class="product-row container" data-productid="[@PRODUCT_ID]" data-bitype="None" data-isbi="" data-skutype="[@PRODUCT_TYPE]">
    <div class="product-image">
        <img width="42" height="42" alt="" src="[@PRODUCT_IMAGE]">
    </div>
    <div class="product-description">
        <h3 style="font-family: fontsDosis; font-weight: 550; font-style: normal;font-size:14px;text-transform: uppercase;">
            <div class="product-price">
                <span class="list-price">
                    <span class="price">[@PRODUCT_PRICE]</span>
                </span>
            </div>
            <span class="OneLinkNoTx">
                <span class="brand">[@BRAND_NAME]</span>
                    <span class="product-name">[@PRODUCT_NAME]</span>
                </span>
        </h3>
        <div class="info-row" style="font-family: fontsDosis; font-weight: 400; font-style: normal;font-size:14px;">
            <span class="sku">
                <!--  <span class="label">Item #</span>-->
                <span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
            </span>
            <span class="qty">
                <span class="label" style="font-family: fontsDosis; font-weight: 400; font-style: normal;font-size:14px;">QTY</span>
                <span class="value" style="font-family: fontsDosis; font-weight: 400; font-style: normal;font-size:14px; float:right;">[@PRODUCT_QUANTITY]</span>
            </span>
        </div>
    </div>
</div>