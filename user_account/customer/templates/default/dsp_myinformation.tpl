<link rel="stylesheet" href="/user_account/customer/templates/default/css/account.css">
<ul class="breadcrumb">
	<li>
		<a href="/profile/MyAccount/myAccountLoading.jsp">My Account</a>
	</li>
	<li>My Information</li>
</ul>
<div class="content ng-scope" ng-app="astoria">

	<div class="sidebar">
		<div class="sidenav">
			<h2 class="nav-title">
				<a href="/profile/beautyInsider/?mediaId=13700022">Beauty Insider</a>
			</h2>
			<ul class="nav nav-stacked">
				<li>
					<a class="" href="/profile/beautyInsider/rewards.jsp?mediaId=16800018">Rewards Boutique</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/personalized/personalized.jsp?mediaId=16800020">Personalized Recommendations</a>
				</li>
				<li>
					<a class="" href="/profile/beautyInsider/?tab=bi&amp;mediaId=17500017">About Beauty Insider</a>
				</li>
				<li>
					<a class="" href="/profile/beautyInsider/?tab=vib&amp;mediaId=11700041">About VIB</a>
				</li>
				<li>
					<a class="" href="/profile/beautyInsider/?tab=rouge&amp;mediaId=19200029">About VIB Rouge</a>
				</li>
			</ul>
			<h2 class="nav-title">
				<a href="/profile/myBeautyBag/?mediaId=18300038">My Beauty Bag</a>
			</h2>
			<ul class="nav nav-stacked">
				<li>
					<a class="" href="/profile/purchaseHistory/purchaseHistory.jsp?mediaId=16800024">Purchases</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/MyList/myList.jsp?mediaId=16900028">Loves</a>
				</li>
				<li>
					<a class="" href="/contentStore/mediaContentTemplate.jsp?mediaId=20600034">Share the Loves</a>
				</li>
			</ul>
			<h2 class="nav-title">
				<a href="/profile/MyAccount/myAccountLoading.jsp?mediaId=16500028">My Account</a>
			</h2>
			<ul class="nav nav-stacked">
				<li>My Information</li>
				<li>
					<a class="" href="/profile/orders/orderHistory.jsp?mediaId=16800022">Orders</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/PaymentMethods/paymentMethods.jsp?mediaId=16800030">Payments &amp; Credits</a>
				</li>
				<li>
					<a class="" href="/profile/MyAccount/Subscriptions/subscriptions.jsp?mediaId=16800028">Subscription</a>
				</li>
			</ul>
		</div>
		<div class="link-group ">
			<ul class="nav nav-stacked nav-link-group">
				<li>
					<a href="/contentStore/mediaContentTemplate.jsp?mediaId=9300044&amp;icid2=My Accont Help Link" class="linkComponent ">Help</a>
				</li>
			</ul>
		</div>

	</div>
	<div id="main" class="maincontent my-info">
		<div class="page-intro">
			<h1>my information</h1>
		</div>
		<div class="maincontent-content">
			<div class="account-module account-info">
				<h2>Account Information</h2>

				<form name="updateAccountInformation" action="/profile/MyAccount/MyInformation/includes/?_DARGS=/profile/MyAccount/MyInformation/includes/accountInformation.jsp#" method="post" class="ng-pristine ng-valid">
					<input name="_dyncharset" value="UTF-8" type="hidden">
					<input name="_dynSessConf" value="8534888244198422714" type="hidden">
					<div class="name">
						<p class="summary">
							<span class="label">Name:</span>
							<span class="value">
								<span class="OneLinkNoTx">123 123&nbsp;123123123</span>
							</span>
							<a id="editName" href="#" rel="0" class="edit" data-update="name">edit</a>
						</p>
						<div class="update" style="display:block">
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="first_name">First name:</label>
									<input id="first_name" name="first_name" value="" class="form-control" type="text" mouseev="true" keyev="true" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==); padding-right: 0px; background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
									<input name="_D:first_name" value=" " type="hidden"></div>
								<div class="form-group column">
									<label for="last_name">Last name:</label>
									<input id="last_name" name="last_name" value="" class="form-control" type="text">
									<input name="_D:last_name" value=" " type="hidden"></div>
							</fieldset>
							<div class="buttons container">
								<button class="btn btn-primary btn-update" value="name" name="name" type="submit">update</button>
								<button class="btn btn-default btn-cancel" value="cancel" name="cancel" type="cancel">cancel</button>
							</div>
						</div>
					</div>

					<div class="email">
						<p class="summary">
							<span class="label">E-mail Address:</span>
							<span class="value">
								<span class="OneLinkNoTx">mysuperbibi@gmail.com</span>
							</span>
							<a href="#" id="editEmail" class="edit" data-update="email">edit</a>
						</p>
						<div class="update">
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="user_name">E-mail address:</label>
									<input id="user_name" maxlength="60" name="user_name" value="" class="form-control" type="email">
									<input name="_D:user_name" value=" " type="hidden"></div>
								<div class="form-group column">
									<label for="confirm_user_name">Confirm e-mail address:</label>
									<input id="confirm_user_name" name="confirm_user_name" value="" class="form-control" type="email">
									<input name="_D:confirm_user_name" value=" " type="hidden"></div>
							</fieldset>
							<div class="buttons container">
								<button class="btn btn-primary btn-update" value="email" name="email" type="submit">update</button>
								<button class="btn btn-default btn-cancel" value="cancel" name="cancel" type="cancel">cancel</button>
							</div>
						</div>
					</div>

					<div class="password">
						<p class="summary">
							<span class="label">Password:</span>
							<span class="value OneLinkNoTx">*******</span>
							<a id="editPass" href="#" class="edit" data-update="password">edit</a>
						</p>
						<div class="update">
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="password">Password:</label>
									<input id="password" name="password" value="" class="form-control" type="password" autocomplete="off" mouseev="true" keyev="true" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=); padding-right: 0px; background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
									<input name="_D:password" value=" " type="hidden"></div>
								<div class="form-group column">
									<label for="password_confirmation">Confirm password:</label>
									<input id="password_confirmation" name="password_confirmation" value="" class="form-control" type="password" autocomplete="off" mouseev="true" keyev="true" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=); padding-right: 0px; background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
									<input name="_D:password_confirmation" value=" " type="hidden"></div>
							</fieldset>
							<div class="buttons container">
								<button class="btn btn-primary btn-update" value="password" name="password" type="submit">update</button>
								<button class="btn btn-default btn-cancel" value="cancel" name="cancel" type="cancel">cancel</button>
							</div>
						</div>
					</div>

					<div class="security">
						<p class="summary">
							<span class="label">Security Question:</span>
							<span class="value">Your favorite scent?</span>
							<a id="editQuest" href="#" rel="1" class="edit" data-update="security_question">edit</a>
						</p>
						<div class="update">
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="security_question">Security question:</label>
									<input name="_D:security_question" value=" " type="hidden">
									<select id="securityQuestion" name="security_question" class="form-control">
										<option value="Your favorite hairstyle?">Your favorite hairstyle?</option>
										<option value="Your beauty idol's name?">Your beauty idol's name?</option>
										<option value="Your favorite scent?">Your favorite scent?</option>
										<option value="Your father's birthday (mm/dd/yy)?">Your father's birthday (mm/dd/yy)?</option>
										<option value="Your grandmother's first name?">Your grandmother's first name?</option>
										<option value="Your mother's maiden name?">Your mother's maiden name?</option>
										<option value="Your mother's birthday (mm/dd/yy)?">Your mother's birthday (mm/dd/yy)?</option>
										<option value="The name of your elementary school">The name of your elementary school</option>
										<option value="Last 4 digits of your Social Security #?">Last 4 digits of your Social Security #?</option>
									</select>
								</div>
								<div class="form-group column">
									<label for="security_answer">Answer:</label>
									<input id="securityAnswer" name="security_answer" value="" class="form-control" type="text">
									<input name="_D:security_answer" value=" " type="hidden"></div>
							</fieldset>
							<div class="buttons container">
								<input name="fragmentForUpdate" value="" type="hidden">
								<input name="_D:fragmentForUpdate" value=" " type="hidden">
								<input name="/atg/userprofiling/ProfileFormHandler.useForwards" value="true" type="hidden">
								<input name="_D:/atg/userprofiling/ProfileFormHandler.useForwards" value=" " type="hidden">
								<input name="/atg/userprofiling/ProfileFormHandler.updateMyInformationSuccessURL" value="/profile/MyAccount/MyInformation/json/updateMyInformationResponse.jsp" type="hidden">
								<input name="_D:/atg/userprofiling/ProfileFormHandler.updateMyInformationSuccessURL" value=" " type="hidden">
								<input name="/atg/userprofiling/ProfileFormHandler.updateMyInformationErrorURL" value="/profile/MyAccount/MyInformation/json/updateMyInformationResponse.jsp" type="hidden">
								<input name="_D:/atg/userprofiling/ProfileFormHandler.updateMyInformationErrorURL" value=" " type="hidden">
								<input name="/atg/userprofiling/ProfileFormHandler.updateProfileInformation" value="continue" type="hidden">
								<input name="_D:/atg/userprofiling/ProfileFormHandler.updateProfileInformation" value=" " type="hidden">
								<button class="btn btn-primary btn-update" value="security_question" name="security_question" type="submit">update</button>
								<button class="btn btn-default btn-cancel" value="cancel" name="cancel" type="cancel">cancel</button>
							</div>
						</div>
					</div>

					<input name="_DARGS" value="/profile/MyAccount/MyInformation/includes/accountInformation.jsp" type="hidden"></form>
			</div>
			<div class="account-module bi-profile">
				<h2>Beauty Insider Profile</h2>

				<div class="summary">
					<div class="columns container">

						<dl class="column">
							<dt>Birth date:</dt>
							<dd class="birth_date">January&nbsp;1, 1984</dd>

							<dt>gender:</dt>
							<dd class="gender"></dd>

							<dt>eye color:</dt>
							<dd class="eye_color"></dd>

						</dl>

						<dl class="column">

							<dt>skin type:</dt>
							<dd class="skin_type"></dd>

							<dt>skincare concerns:</dt>
							<dd class="skin_concerns"></dd>

							<dt>skin tone:</dt>
							<dd class="skin_tone"></dd>

						</dl>

						<dl class="column">

							<dt>hair color:</dt>
							<dd class="hair_color"></dd>

							<dt>your hair:</dt>
							<dd class="hair_desc"></dd>

							<dt>hair concerns:</dt>
							<dd class="hair_concerns"></dd>

						</dl>

					</div>
					<!-- .columns -->

					<p> <strong>Tell us about yourself. Answer a few short beauty profile questions and we can
							<br>recommend products just for you.</strong>
					</p>
					<p>
						<a class="btn btn-primary js-edit-bi-profile" href="#">
							<span class="btn-text">Answer Profile Questions</span>
						</a>
					</p>
				</div>
				<!-- .summary -->
				<div class="profile-edit">
					<p>
						Please answer the questions below to get personalized product recommendations.
					</p>
					<form name="personalizeProduct" action="/profile/MyAccount/MyInformation/myInformation.jsp?_DARGS=/profile/MyAccount/MyInformation/includes/biProfile.jsp" method="post" class="ng-pristine ng-valid">
						<input name="_dyncharset" value="UTF-8" type="hidden">
						<input name="_dynSessConf" value="8534888244198422714" type="hidden">
						<div class="columns container">
							<div class="column">
								<div class="inner">
									<fieldset>
										<h3>birth year</h3>
										<input name="_D:birth_year" value=" " type="hidden">
										<select name="birth_year" class="form-control">
											<option value="">year</option>
											<option value="">-----</option>
											<option value="2001">2001</option>
											<option value="2000">2000</option>
											<option value="1999">1999</option>
											<option value="1998">1998</option>
											<option value="1997">1997</option>
											<option value="1996">1996</option>
											<option value="1995">1995</option>
											<option value="1994">1994</option>
											<option value="1993">1993</option>
											<option value="1992">1992</option>
											<option value="1991">1991</option>
											<option value="1990">1990</option>
											<option value="1989">1989</option>
											<option value="1988">1988</option>
											<option value="1987">1987</option>
											<option value="1986">1986</option>
											<option value="1985">1985</option>
											<option selected="" value="1984">1984</option>
											<option value="1983">1983</option>
											<option value="1982">1982</option>
											<option value="1981">1981</option>
											<option value="1980">1980</option>
											<option value="1979">1979</option>
											<option value="1978">1978</option>
											<option value="1977">1977</option>
											<option value="1976">1976</option>
											<option value="1975">1975</option>
											<option value="1974">1974</option>
											<option value="1973">1973</option>
											<option value="1972">1972</option>
											<option value="1971">1971</option>
											<option value="1970">1970</option>
											<option value="1969">1969</option>
											<option value="1968">1968</option>
											<option value="1967">1967</option>
											<option value="1966">1966</option>
											<option value="1965">1965</option>
											<option value="1964">1964</option>
											<option value="1963">1963</option>
											<option value="1962">1962</option>
											<option value="1961">1961</option>
											<option value="1960">1960</option>
											<option value="1959">1959</option>
											<option value="1958">1958</option>
											<option value="1957">1957</option>
											<option value="1956">1956</option>
											<option value="1955">1955</option>
											<option value="1954">1954</option>
											<option value="1953">1953</option>
											<option value="1952">1952</option>
											<option value="1951">1951</option>
											<option value="1950">1950</option>
											<option value="1949">1949</option>
											<option value="1948">1948</option>
											<option value="1947">1947</option>
											<option value="1946">1946</option>
											<option value="1945">1945</option>
											<option value="1944">1944</option>
											<option value="1943">1943</option>
											<option value="1942">1942</option>
											<option value="1941">1941</option>
											<option value="1940">1940</option>
											<option value="1939">1939</option>
											<option value="1938">1938</option>
											<option value="1937">1937</option>
											<option value="1936">1936</option>
											<option value="1935">1935</option>
											<option value="1934">1934</option>
											<option value="1933">1933</option>
											<option value="1932">1932</option>
											<option value="1931">1931</option>
											<option value="1930">1930</option>
											<option value="1929">1929</option>
											<option value="1928">1928</option>
											<option value="1927">1927</option>
											<option value="1926">1926</option>
											<option value="1925">1925</option>
											<option value="1924">1924</option>
											<option value="1923">1923</option>
											<option value="1922">1922</option>
											<option value="1921">1921</option>
											<option value="1920">1920</option>
											<option value="1919">1919</option>
											<option value="1918">1918</option>
											<option value="1917">1917</option>
											<option value="1916">1916</option>
											<option value="1915">1915</option>
											<option value="1914">1914</option>
											<option value="1913">1913</option>
											<option value="1912">1912</option>
											<option value="1911">1911</option>
											<option value="1910">1910</option>
											<option value="1909">1909</option>
											<option value="1908">1908</option>
											<option value="1907">1907</option>
											<option value="1906">1906</option>
											<option value="1905">1905</option>
											<option value="1904">1904</option>
											<option value="1903">1903</option>
											<option value="1902">1902</option>
											<option value="1901">1901</option>
											<option value="1900">1900</option>
											<option value="Before 1900">Before 1900</option>
										</select>
									</fieldset>
									<fieldset>
										<h3>gender</h3>
										<label class="radio">
											<input name="gender" value="female" type="radio">
											<input name="_D:gender" value=" " type="hidden">female</label>
										<label class="radio">
											<input name="gender" value="male" type="radio">
											<input name="_D:gender" value=" " type="hidden">male</label>
									</fieldset>
									<fieldset>
										<h3>eye color</h3>
										<input name="_D:eye_color" value=" " type="hidden">
										<select name="eye_color" class="form-control">
											<option selected="" value="">select</option>
											<option value="blue">blue</option>
											<option value="brown">brown</option>
											<option value="green">green</option>
											<option value="hazel">hazel</option>
											<option value="grey">grey</option>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="column">
								<div class="inner">
									<fieldset>
										<h3>skin type</h3>
										<input name="_D:skin_type" value=" " type="hidden">
										<select name="skin_type" class="form-control">
											<option selected="" value="">select</option>
											<option value="normalSk">normal</option>
											<option value="comboSk">combination</option>
											<option value="drySk">dry</option>
											<option value="oilySk">oily</option>
										</select>
									</fieldset>
									<fieldset>
										<h3>skincare concerns</h3>
										<label class="checkbox">
											<input name="skin_concerns" value="sunDamage" type="checkbox">
											<input name="_D:skin_concerns" value=" " type="hidden">Sun Damage</label>
										<label class="checkbox">
											<input name="skin_concerns" value="acne" type="checkbox">
											<input name="_D:skin_concerns" value=" " type="hidden">Acne</label>
										<label class="checkbox">
											<input name="skin_concerns" value="skinTone" type="checkbox">
											<input name="_D:skin_concerns" value=" " type="hidden">Uneven Skin Tones</label>
										<label class="checkbox">
											<input name="skin_concerns" value="dullness" type="checkbox">
											<input name="_D:skin_concerns" value=" " type="hidden">Dullness</label>
										<label class="checkbox">
											<input name="skin_concerns" value="aging" type="checkbox">
											<input name="_D:skin_concerns" value=" " type="hidden">Aging</label>
										<label class="checkbox">
											<input name="skin_concerns" value="sens" type="checkbox">
											<input name="_D:skin_concerns" value=" " type="hidden">Sensitivity</label>
									</fieldset>
									<fieldset>
										<h3>skin tone</h3>
										<div class="skintone-selector">
											<div class="skintone-selector-content container">
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="porcelain" src="/user_account/customer/templates/default/img/porcelain.jpg"></a>
													<span>porcelain</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="fair" src="/user_account/customer/templates/default/img/fair.jpg"></a>
													<span>fair</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="light" src="/user_account/customer/templates/default/img/light.jpg"></a>
													<span>light</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="medium" src="/user_account/customer/templates/default/img/med.jpg"></a>
													<span>medium</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="tan" src="/user_account/customer/templates/default/img/tan.jpg"></a>
													<span>tan</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="olive" src="/user_account/customer/templates/default/img/olive.jpg"></a>
													<span>olive</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="deep" src="/user_account/customer/templates/default/img/deep.jpg"></a>
													<span>deep</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="dark" src="/user_account/customer/templates/default/img/dark.jpg"></a>
													<span>dark</span>
												</div>
												<div class="">
													<a href="#" class="skintone-thumb">
														<img height="36" width="36" alt="ebony" src="/user_account/customer/templates/default/img/ebony.jpg"></a>
													<span>ebony</span>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="column">
								<div class="inner">
									<fieldset>
										<h3>hair color</h3>
										<input name="_D:hair_color" value=" " type="hidden">
										<select name="hair_color" class="form-control">
											<option selected="" value="">select</option>
											<option value="blonde">blonde</option>
											<option value="brunette">brunette</option>
											<option value="auburn">auburn</option>
											<option value="black">black</option>
											<option value="red">red</option>
											<option value="gray">gray</option>
										</select>
									</fieldset>
									<fieldset>
										<h3>your hair</h3>
										<label class="checkbox">
											<input name="hair_desc" value="normalHr" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Normal</label>
										<label class="checkbox">
											<input name="hair_desc" value="dryHr" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Dry</label>
										<label class="checkbox">
											<input name="hair_desc" value="oilyHr" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Oily</label>
										<label class="checkbox">
											<input name="hair_desc" value="coarseHr" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Coarse</label>
										<label class="checkbox">
											<input name="hair_desc" value="fineHr" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Fine</label>
										<label class="checkbox">
											<input name="hair_desc" value="straight" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Straight</label>
										<label class="checkbox">
											<input name="hair_desc" value="wavy" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Wavy</label>
										<label class="checkbox">
											<input name="hair_desc" value="chemHr" type="checkbox">
											<input name="_D:hair_desc" value=" " type="hidden">Chemically Treated</label>
									</fieldset>
									<fieldset>
										<h3>hair concerns</h3>
										<label class="checkbox">
											<input name="hair_concerns" value="dandruffHr" type="checkbox">
											<input name="_D:hair_concerns" value=" " type="hidden">Dandruff</label>
										<label class="checkbox">
											<input name="hair_concerns" value="damaged" type="checkbox">
											<input name="_D:hair_concerns" value=" " type="hidden">Damaged</label>
										<label class="checkbox">
											<input name="hair_concerns" value="frizzHr" type="checkbox">
											<input name="_D:hair_concerns" value=" " type="hidden">Frizz</label>
										<label class="checkbox">
											<input name="hair_concerns" value="volumizing" type="checkbox">
											<input name="_D:hair_concerns" value=" " type="hidden">Volumizing</label>
										<label class="checkbox">
											<input name="hair_concerns" value="thinHr" type="checkbox">
											<input name="_D:hair_concerns" value=" " type="hidden">Thinning</label>
										<label class="checkbox">
											<input name="hair_concerns" value="oiliness" type="checkbox">
											<input name="_D:hair_concerns" value=" " type="hidden">Oiliness</label>
									</fieldset>
								</div>
							</div>
						</div>
						<p>
							<button class="btn btn-primary btn-update" value="update" type="submit">update profile</button>
						</p>
						<input name="_DARGS" value="/profile/MyAccount/MyInformation/includes/biProfile.jsp" type="hidden"></form>
				</div>
				<p class="help-block">
					Tell us about yourself, answer a few short beauty profile questions and we can recommend products just for you.
				</p>
			</div>
			<div class="account-module color-iq">
				<a href="/IQ/color/" class="pull-right more-link">
					<span class="icon icon-help-lg"></span>
					<span class="text-underline">learn more</span>
				</a>
				<h2>color iq</h2>
				<div class="skintones container"></div>
				<form action="" id="add-skintone" class="ng-pristine ng-valid">
					<p class="error-message hide">The SkinTone shade entered is not valid. Please try again.</p>
					<label for="input-skintone">Add a new SkinTone number:</label>
					<input type="text" placeholder="enter SkinTone number" id="input-skintone" class="form-control">
					<button type="submit" class="btn btn-primary">
						GO
						<span class="arrow arrow-right arrow-mini"></span>
					</button>
				</form>
			</div>
			<div id="module-address-book" class="account-module address-book container ng-scope" data-maxaddresses="10" ng-controller="UserAddressController">

				<h2>Address Book</h2>

				<div ng-show="editedAddress" class="add-address-form ng-hide">
					<!-- ngRepeat: err in (errors | flattenErrors | unique) -->

					<form id="newShippingAddressForm" method="post" action="blank.jsp" name="newShippingAddress" class="ng-pristine ng-valid">

						<input type="hidden" name="id" ng-model="editedAddress.id" class="ng-pristine ng-valid">
						<div class="add-update-info">
							<span class="requirements" style="display: none;">
								<span>*</span>
								Required
							</span>
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="firstName">
										<span class="required">*</span>
										First name
									</label>
									<input ng-model="editedAddress.first_name" type="text" name="first_name" ng-class="{'error': errors.field.first_name!==undefined}" class="form-control ng-pristine ng-valid"></div>
								<div class="form-group column">
									<label for="lastName">
										<span class="required">*</span>
										Last name
									</label>
									<input ng-model="editedAddress.last_name" type="text" name="last_name" ng-class="{'error': errors.field.last_name!==undefined}" class="form-control ng-pristine ng-valid"></div>
							</fieldset>
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="address1">
										<span class="required">*</span>
										Address line 1
									</label>
									<input ng-model="editedAddress.address1" type="text" name="address1" ng-class="{'error': errors.field.address1!==undefined}" maxlength="30" class="form-control ng-pristine ng-valid"></div>
								<div class="form-group column">
									<label for="address2">Address line 2</label>
									<input ng-model="editedAddress.address2" type="text" name="address2" ng-class="{'error': errors.field.address2!==undefined}" maxlength="30" class="form-control ng-pristine ng-valid"></div>
							</fieldset>
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="city">
										<span class="required">*</span>
										City
									</label>
									<input ng-model="editedAddress.city" type="text" name="city" maxlength="30" ng-class="{'error': errors.field.city!==undefined}" class="form-control ng-pristine ng-valid"></div>
								<div class="form-group column">
									<label for="state" state-label="editedAddress.country">
										<span class="required">*</span>
										<span></span>
									</label>

									<input type="text" id="state" ng-show="isFreeStates" ng-model="editedAddress.state" ng-class="{'error': errors.field.state!==undefined}" class="form-control ng-pristine ng-valid ng-hide">

									<select id="state" ng-hide="isFreeStates" ng-model="editedAddress.state" country-states="editedAddress.country" class="form-control ng-pristine ng-valid" ng-class="{'error': errors.field.state!==undefined}">
										<option value="? undefined:undefined ?"></option>
									</select>

								</div>
							</fieldset>
							<fieldset class="two-column container">
								<div class="form-group column">
									<label for="zip">
										<span class="required">*</span>
										<span zip-label="editedAddress.country"></span>
									</label>
									<input ng-model="editedAddress.zip" type="text" name="zip" ng-class="{'error': errors.field.zip!==undefined}" maxlength="10" class="form-control zip ng-pristine ng-valid"></div>
								<div class="form-group column">

									<label for="country">
										<span class="required">*Country</span>
									</label>
									<div class="country-static"></div>
									<select name="country" id="country" ng-change="onCountryChange()" ng-model="editedAddress.country" class="form-control ng-pristine ng-valid">
										<option selected="selected" value="">select a country</option>
										<option value="US">United States</option>
										<option value="CA">Canada</option>
										<option value="JP">Japan</option>
										<option value="KR">Korea, Republic of</option>
										<option value="GB">United Kingdom</option>
									</select>
								</div>
							</fieldset>
							<fieldset class="phone-controls container">
								<div class="form-group">
									<label for="phoneNumber">
										<span class="required">*</span>
										Phone
									</label>
									<div ng-switch="" on="editedAddress.isUSorCA()">
										<!-- ngSwitchWhen: true -->
										<!-- ngSwitchDefault:  -->
										<div ng-switch-default="" class="ng-scope">
											<input ng-model="editedAddress.phone" name="phone" maxlength="15" type="text" class="form-control ng-pristine ng-valid" ng-class="{'error': errors.field.phone!==undefined}"></div>
									</div>
								</div>
								<div ng-show="editedAddress.isUSorCA()" class="form-group phone-ext ng-hide">
									<label for="phone_ext">Ext.</label>
									<input ng-model="editedAddress.phone_ext" type="text" maxlength="10" id="phone_ext" class="form-control phone-control ng-pristine ng-valid" ng-class="{'error': errors.field.phone_ext!==undefined}"></div>
							</fieldset>
							<label class="checkbox">
								<input ng-model="editedAddress.is_default" type="checkbox" name="is_default" id="is_default" class="ng-pristine ng-valid">Choose as default shipping address</label>
							<ul class="address-actions container">
								<li ng-hide="editedAddress.id" id="newAddress" class="">
									<button class="btn btn-primary btn-add-address" type="button" name="add-address" ng-click="submitAddress($event)">add address</button>
								</li>
								<li ng-show="editedAddress.id" id="updateAddress" class="ng-hide">
									<button class="btn btn-primary btn-update" type="button" name="update" ng-click="submitAddress($event)">update</button>
								</li>
								<li>
									<button ng-click="cancelEdit()" class="btn btn-default btn-cancel" type="button" name="cancel">cancel</button>
								</li>
							</ul>
						</div>

					</form>
				</div>

				<ul ng-hide="editedAddress" class="address-actions container">
					<li>
						<button ng-click="editNewAddress($event)" class="btn btn-primary btn-add-new-address" type="button">add new address</button>
					</li>
				</ul>

				<div class="addresses container">
					<!-- ngRepeat: address in addresses -->
				</div>

				<script id="addresses" type="text/json">[]</script>
				<div class="addresses container full-view"></div>
			</div>
			<div id="modal-confirm" class="modal">
				<div class="modal-header">
					<h4 class="modal-title">delete address</h4>
				</div>
				<div class="modal-body">
					<p class="warning-message large">
						Are you sure you would like to permanently delete your address?
					</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary btn-lg btn-yes" type="submit" value="yes" name="yes">yes</button>
					<button class="btn btn-default btn-lg btn-no" type="cancel" value="no" name="no">no</button>
				</div>
			</div>
		</div>
	</div>
</div>