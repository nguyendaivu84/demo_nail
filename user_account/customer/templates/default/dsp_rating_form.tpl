<div class="BVRRSection BVRRQuickTakeSection" id="BVRRQuickTakeSectionID">
<div class="BVRRHeader BVRRQuickTakeHeader" id="BVRRQuickTakeHeaderID">
    <span class="BVRRTitle BVRRQuickTakeTitle">Summary of Customer Ratings &amp; Reviews</span>
									<span class="BVRRQuickTakeHeaderHelpText">
										Check the boxes above to
										<span class="BVRRQuickTakeHeaderHelpTextKeyword BVRRQuickTakeHeaderHelpTextFilter">filter reviews</span>
									</span>
</div>
<div class="BVRRSpacer BVRRQuickTakeSpacer"></div>
<table class="BVRRContent BVRRQuickTakeContent" id="BVRRQuickTakeContentID" style="">
<tbody>
<tr class="BVRRQuickTakeTableRow BVRRQuickTakeSummaryRow">
<td rowspan="2" class="BVRRQuickTakeSummary BVRRQuickTakeSummaryOneCloud" id="BVRRQuickTakeSummaryID">
    <div id="BVRRCustomQuickTakeRatingSummaryRatingContainer" class="BVRRRatingOverall">
        <div id="BVRRCustomQuickTakeRatingSummaryOverallLabel" class="BVRRLabel BVRRRatingNormalLabel">Overall Rating</div>
        <div id="BVRRCustomQuickTakeRatingSummaryOverallImage" class="BVRRRatingNormalImage">
            <img src="http://reviews.astoria.com/8723illuminate/4_5/5/rating.gif" alt="4.5 out of 5"></div>
        <div class="BVRRRatingNormalOutOf">
            <div id="BVRRCustomQuickTakeOutOfSentenceBlock">
                <span id="BVRRCustomQuickTakeRatingSummaryOverallRating" class="BVRRNumber BVRRRatingNumber">(4.5</span>
                <span id="BVRRCustomQuickTakeRatingSummaryOverallRatingSeparator" class="BVRRSeparatorText">out of</span>
                <span id="BVRRCustomQuickTakeRatingSummaryOverallRatingRange" class="BVRRNumber BVRRRatingRangeNumber">5)</span>
            </div>
														<span id="BVRRCustomQuickTakeRatingSummaryHistogram">
															<div id="BVRRRatingsHistogramButton_mh6aqdov5vr6px0tvhijg2qcw_ID" class="BVRRRatingsHistogramButton">
                                                                <div id="BVRRRatingsHistogramButtonScript_l9ulxfrr3gxkta72t8kogvupk_ID" class="BVRRRatingsHistogramButtonScript">
                                                                    <img src="http://reviews.astoria.com/static/8723illuminate/openRatingsHistogram.gif" alt="Open Ratings Snapshot" name="BV_TrackingTag_QuickTakeSummary_ExpandHistogram_P384728" class="BVRRRatingsHistogramButtonImage" onmouseover="bvHistogramMouseover(this, 'BVRRHistogramTimer_6hglwtmrc0e9l5l2pts7o9u8b_ID', 'BVRRRatingsHistogramButtonPopin_mcj8unado3c3frclkv4fdknfs_ID', 'RatingsHistogramFrame');" onmouseout="bvHistogramMouseout('BVRRHistogramTimer_6hglwtmrc0e9l5l2pts7o9u8b_ID', 'BVRRRatingsHistogramButtonPopin_mcj8unado3c3frclkv4fdknfs_ID', 1000);">
                                                                    <div id="BVRRRatingsHistogramButtonPopin_mcj8unado3c3frclkv4fdknfs_ID" class="BVRRRatingsHistogramButtonPopin">
                                                                        <div class="BVRRHistogram">
                                                                            <div class="BVRRHistogramTitle">
																				<span class="BVRRCount BVRRNonZeroCount">
																					<span class="BVRRNumber">45</span>
																					reviews
																				</span>
                                                                            </div>
                                                                            <div class="BVRRHistogramContent">
                                                                                <div class="BVRRHistogramBarRow BVRRHistogramBarRow5">
																					<span class="BVRRHistStarLabel BVRRHistStarLabel5">
																						<span class="BVRRHistStarLabelText">5 stars</span>
																					</span>
                                                                                    <div class="BVRRHistogramBar">
                                                                                        <div style="width: 58%" class="BVRRHistogramFullBar"></div>
                                                                                    </div>
                                                                                    <span class="BVRRHistAbsLabel">26</span>
                                                                                </div>
                                                                                <div class="BVRRHistogramBarRow BVRRHistogramBarRow4">
																					<span class="BVRRHistStarLabel BVRRHistStarLabel4">
																						<span class="BVRRHistStarLabelText">4 stars</span>
																					</span>
                                                                                    <div class="BVRRHistogramBar">
                                                                                        <div style="width: 36%" class="BVRRHistogramFullBar"></div>
                                                                                    </div>
                                                                                    <span class="BVRRHistAbsLabel">16</span>
                                                                                </div>
                                                                                <div class="BVRRHistogramBarRow BVRRHistogramBarRow3">
																					<span class="BVRRHistStarLabel BVRRHistStarLabel3">
																						<span class="BVRRHistStarLabelText">3 stars</span>
																					</span>
                                                                                    <div class="BVRRHistogramBar">
                                                                                        <div style="width: 4%" class="BVRRHistogramFullBar"></div>
                                                                                    </div>
                                                                                    <span class="BVRRHistAbsLabel">2</span>
                                                                                </div>
                                                                                <div class="BVRRHistogramBarRow BVRRHistogramBarRow2 BVRRHistogramBarRowZero">
																					<span class="BVRRHistStarLabel BVRRHistStarLabel2">
																						<span class="BVRRHistStarLabelText">2 stars</span>
																					</span>
                                                                                    <div class="BVRRHistogramBar">
                                                                                        <div style="width: 0%" class="BVRRHistogramFullBar"></div>
                                                                                    </div>
                                                                                    <span class="BVRRHistAbsLabel">0</span>
                                                                                </div>
                                                                                <div class="BVRRHistogramBarRow BVRRHistogramBarRow1">
																					<span class="BVRRHistStarLabel BVRRHistStarLabel1">
																						<span class="BVRRHistStarLabelText">1 star</span>
																					</span>
                                                                                    <div class="BVRRHistogramBar">
                                                                                        <div style="width: 2%" class="BVRRHistogramFullBar"></div>
                                                                                    </div>
                                                                                    <span class="BVRRHistAbsLabel">1</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <noscript>
                                                                    &amp;lt;div class="BVRRRatingsHistogramButtonImage"&amp;gt;&amp;lt;a name="BV_TrackingTag_QuickTakeSummary_ExpandHistogram_P384728" target="_blank" href="http://reviews.astoria.com/8723illuminate/P384728/ratingsnapshot.htm"&amp;gt; &amp;lt;img src="http://reviews.astoria.com/static/8723illuminate/openRatingsHistogram.gif" alt="Open Ratings Snapshot" /&amp;gt;
                                                                    &amp;lt;/a&amp;gt;&amp;lt;/div&amp;gt;
                                                                </noscript>
                                                            </div>
														</span>
        </div>
    </div>
    <div id="BVRRCustomQuickTakeLinkComponent">
        <div class="BVRRRatingSummaryLinks">
            <div id="BVRRRatingSummaryLinkWriteID" class="BVRRRatingSummaryLink BVRRRatingSummaryLinkWrite">
                <span class="BVRRRatingSummaryLinkWritePrefix"></span>
                <a title="Write a review" name="BV_TrackingTag_QuickTakeSummary_WriteReview_P384728" target="BVFrame" onclick="bvShowContentOnReturnPRR('8723illuminate', 'P384728', 'BVRRWidgetID');" href="http://reviews.astoria.com/8723illuminate/P384728/writereview.htm?format=embedded&amp;campaignid=BV_RATING_SUMMARY&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp">Write a review</a>
                <span class="BVRRRatingSummaryLinkWriteSuffix"></span>
            </div>
        </div>
    </div>
</td>
<td>
<table>
<tbody>
<tr>
    <td class="BVDI_AFAttributeFilteringSectionCell" colspan="1">
        <div class="BVRRSpacer BVDI_AFAttributeFilteringBeforeSpacer"></div>
        <div class="BVDI BVDI_AF" id="BVRRAttributeFilteringSectionID">
            <div class="BVDIHeader BVDI_AFHeader" id="BVRRAttributeFilteringHeaderID">Filter Reviews by:</div>
            <div class="BVDIBody BVDI_AFBody" id="BVRRAttributeFilteringContentID">
                <div id="BVRRFilterAttribute1ID" class="BVDI_AFFilterAttribute BVDI_AFFilterAttributeFirst">
                    <div class="BVDI_AFFilterAttributeHeader" data-bvname="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_skinTone_Header" onclick="bvTriggerAttributeContent('BVRR', 'BVDI_AF', 1, bvrrFilteringMouseout);return false;">Skin Tone</div>
                    <div id="BVRRFilterAttributeContent1ID" class="BVDI_AFFilterAttributeContent BVDI_AFFilterAttributeSkinToneContent BVDI_AFHidden">
                        <div class="BVDI_AFFilterAttributeClose">
                            <a href="#" class="BVDI_AFFilterAttributeCloseLink" id="BVRRFilterAttributeCloseLink1ID" onclick="bvHideAllAttributeContent('BVRR', 'BVDI_AF');return false;" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_skinTone_Close">
                                Close
                                <img src="http://reviews.astoria.com/static/8723illuminate/filterOnCustom.gif" alt="" title=""></a>
                        </div>
                        <ul id="BVRRFilterAttributeSkinToneListID" class="BVDI_AFAttributeFilteringList">
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinTone0ID" data-bvtrack="filterName:SkinTone" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=fair&amp;expandcontextdatadimensionfilter=skinTone', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinToneLabel">
																							<a title="Fair" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_skinTone_EnableFilter_Fair" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinTone', 0, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=fair&amp;expandcontextdatadimensionfilter=skinTone">Fair</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinTone1ID" data-bvtrack="filterName:SkinTone" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=light&amp;expandcontextdatadimensionfilter=skinTone', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinToneLabel">
																							<a title="Light" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_skinTone_EnableFilter_Light" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinTone', 1, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=light&amp;expandcontextdatadimensionfilter=skinTone">Light</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinTone2ID" data-bvtrack="filterName:SkinTone" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=medium&amp;expandcontextdatadimensionfilter=skinTone', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinToneLabel">
																							<a title="Medium" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_skinTone_EnableFilter_Medium" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinTone', 2, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=medium&amp;expandcontextdatadimensionfilter=skinTone">Medium</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxSkinTone3ID" data-bvtrack="filterName:SkinTone" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=olive&amp;expandcontextdatadimensionfilter=skinTone', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeSkinToneLabel">
																							<a title="Olive" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_skinTone_EnableFilter_Olive" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'SkinTone', 3, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_skinTone=olive&amp;expandcontextdatadimensionfilter=skinTone">Olive</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" value="true" disabled="disabled">
                                <span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeLabelDisabled">Deep</span>
                            </li>
                            <li>
                                <input type="checkbox" value="true" disabled="disabled">
                                <span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeLabelDisabled">Dark</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="BVRRFilterAttribute2ID" class="BVDI_AFFilterAttribute">
                    <div class="BVDI_AFFilterAttributeHeader" data-bvname="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_Header" onclick="bvTriggerAttributeContent('BVRR', 'BVDI_AF', 2, bvrrFilteringMouseout);return false;">Eye Color</div>
                    <div id="BVRRFilterAttributeContent2ID" class="BVDI_AFFilterAttributeContent BVDI_AFFilterAttributeEyeColorContent BVDI_AFHidden">
                        <div class="BVDI_AFFilterAttributeClose">
                            <a href="#" class="BVDI_AFFilterAttributeCloseLink" id="BVRRFilterAttributeCloseLink2ID" onclick="bvHideAllAttributeContent('BVRR', 'BVDI_AF');return false;" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_Close">
                                Close
                                <img src="http://reviews.astoria.com/static/8723illuminate/filterOnCustom.gif" alt="" title=""></a>
                        </div>
                        <ul id="BVRRFilterAttributeEyeColorListID" class="BVDI_AFAttributeFilteringList">
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxEyeColor0ID" data-bvtrack="filterName:EyeColor" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=blue&amp;expandcontextdatadimensionfilter=eyeColor', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeEyeColorLabel">
																							<a title="Blue" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_EnableFilter_Blue" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'EyeColor', 0, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=blue&amp;expandcontextdatadimensionfilter=eyeColor">Blue</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxEyeColor1ID" data-bvtrack="filterName:EyeColor" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=brown&amp;expandcontextdatadimensionfilter=eyeColor', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeEyeColorLabel">
																							<a title="Brown" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_EnableFilter_Brown" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'EyeColor', 1, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=brown&amp;expandcontextdatadimensionfilter=eyeColor">Brown</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxEyeColor2ID" data-bvtrack="filterName:EyeColor" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=green&amp;expandcontextdatadimensionfilter=eyeColor', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeEyeColorLabel">
																							<a title="Green" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_EnableFilter_Green" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'EyeColor', 2, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=green&amp;expandcontextdatadimensionfilter=eyeColor">Green</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxEyeColor3ID" data-bvtrack="filterName:EyeColor" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=hazel&amp;expandcontextdatadimensionfilter=eyeColor', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeEyeColorLabel">
																							<a title="Hazel" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_EnableFilter_Hazel" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'EyeColor', 3, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=hazel&amp;expandcontextdatadimensionfilter=eyeColor">Hazel</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxEyeColor4ID" data-bvtrack="filterName:EyeColor" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=gray&amp;expandcontextdatadimensionfilter=eyeColor', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeEyeColorLabel">
																							<a title="Gray" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_eyeColor_EnableFilter_Gray" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'EyeColor', 4, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_eyeColor=gray&amp;expandcontextdatadimensionfilter=eyeColor">Gray</a>
																						</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="BVRRFilterAttribute3ID" class="BVDI_AFFilterAttribute BVDI_AFFilterAttributeLast">
                    <div class="BVDI_AFFilterAttributeHeader" data-bvname="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_Header" onclick="bvTriggerAttributeContent('BVRR', 'BVDI_AF', 3, bvrrFilteringMouseout);return false;">Age</div>
                    <div id="BVRRFilterAttributeContent3ID" class="BVDI_AFFilterAttributeContent BVDI_AFFilterAttributeAgeContent BVDI_AFHidden">
                        <div class="BVDI_AFFilterAttributeClose">
                            <a href="#" class="BVDI_AFFilterAttributeCloseLink" id="BVRRFilterAttributeCloseLink3ID" onclick="bvHideAllAttributeContent('BVRR', 'BVDI_AF');return false;" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_Close">
                                Close
                                <img src="http://reviews.astoria.com/static/8723illuminate/filterOnCustom.gif" alt="" title=""></a>
                        </div>
                        <ul id="BVRRFilterAttributeAgeListID" class="BVDI_AFAttributeFilteringList">
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge0ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=13to17&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																							<a title="13-17" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_EnableFilter_13-17" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 0, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=13to17&amp;expandcontextdatadimensionfilter=age">13-17</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge1ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=18to24&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																							<a title="18-24" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_EnableFilter_18-24" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 1, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=18to24&amp;expandcontextdatadimensionfilter=age">18-24</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge2ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=25to34&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																							<a title="25-34" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_EnableFilter_25-34" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 2, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=25to34&amp;expandcontextdatadimensionfilter=age">25-34</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge3ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=35to44&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																							<a title="35-44" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_EnableFilter_35-44" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 3, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=35to44&amp;expandcontextdatadimensionfilter=age">35-44</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge4ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=45to54&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																							<a title="45-54" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_EnableFilter_45-54" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 4, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=45to54&amp;expandcontextdatadimensionfilter=age">45-54</a>
																						</span>
                            </li>
                            <li>
                                <input type="checkbox" id="BVRRFilterAttributeItemCheckboxAge5ID" data-bvtrack="filterName:Age" value="true" class="BVDI_AFFilterAttributeItemCheckbox BVDI_AFFilterAttributeItemCheckboxUnselected" onclick="$BV.Internal.Requester.get('http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=over54&amp;expandcontextdatadimensionfilter=age', 'BVFrame','');">
																						<span class="BVDI_AFFilterAttributeLabel BVDI_AFFilterAttributeAgeLabel">
																							<a title="over 54" name="BV_TrackingTag_QuickTake_Summary_P384728_AttributeFilter_age_EnableFilter_over 54" target="BVFrame" onclick="bvCheckAttributeFilteringBox('BVRR', 'Age', 5, false);" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;contextdatavalue_age=over54&amp;expandcontextdatadimensionfilter=age">over 54</a>
																						</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="BVRRSpacer BVDI_AFAttributeFilteringAfterSpacer"></div>
    </td>
</tr>
<tr>
    <td class="BVRRQuickTakeTags BVRRQuickTakeProTags BVRRQuickTakeProTagsSummaryOneCloud" id="BVRRQuickTakeProTagsID">
        <div id="BVRRQuickTakeProContainerID" class="BVRRQuickTakeContainer BVRRQuickTakeProContainer">
            <div class="BVRRLabel BVRRQuickTakeLabel">Pros</div>
            <div class="BVRRValue BVRRQuickTakeValue" id="RRQuickTakeProTagsValuesID">
                <ul class="BVRRQuickTakeList">
                    <li class="BVRRTag BVRRTag15 BVRRTagFilter">
                        <a title="See 25 reviews that were marked as 'good value'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_good value" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=good%20value">good value</a>
                        <span class="BVRRNote">(25)</span>
                    </li>
                    <li class="BVRRTag BVRRTag12 BVRRTagFilter">
                        <a title="See 20 reviews that were marked as 'travel size'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_travel size" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=travel%20size">travel size</a>
                        <span class="BVRRNote">(20)</span>
                    </li>
                    <li class="BVRRTag BVRRTag2 BVRRTagFilter">
                        <a title="See 3 reviews that were marked as 'perfect for spring'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_perfect for spring" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=perfect%20for%20spring">perfect for spring</a>
                        <span class="BVRRNote">(3)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'beautiful colors'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_beautiful colors" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=beautiful%20colors">beautiful colors</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'essential arsenal'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_essential arsenal" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=essential%20arsenal">essential arsenal</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'flattering'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_flattering" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=flattering">flattering</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'fun colors'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_fun colors" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=fun%20colors">fun colors</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'glowy'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_glowy" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=glowy">glowy</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'good pigments'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_good pigments" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=good%20pigments">good pigments</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'high quality'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_high quality" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=high%20quality">high quality</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'highly pigmented'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_highly pigmented" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=highly%20pigmented">highly pigmented</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'perfect blush!'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_perfect blush!" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=perfect%20blush%21">perfect blush!</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'radiant orchid'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_radiant orchid" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=radiant%20orchid">radiant orchid</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'rose-hued'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_rose-hued" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=rose-hued">rose-hued</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                    <li class="BVRRTag BVRRTag1 BVRRTagFilter">
                        <a title="See review that was marked as 'small'" name="BV_TrackingTag_QuickTake_Summary_P384728_Pro_TagFilter_small" target="BVFrame" href="http://reviews.astoria.com/8723illuminate/P384728/reviews.htm?format=embedded&amp;tag_Pro=small">small</a>
                        <span class="BVRRNote">(1)</span>
                    </li>
                </ul>
            </div>
            <div class="BVRRSpacer BVRRQuickTakeSpacer"></div>
            <div id="BVRRQuickTakeFilterSentenceID" class="BVRRTagFilterSentence">
                Check the boxes above to
                <span class="BVRRQuickTakeFooterHelpTextKeyword BVRRQuickTakeFooterHelpTextFilter">filter reviews</span>
            </div>
        </div>
    </td>
</tr>
</tbody>
</table>
</td>

</tr>
</tbody>
</table>
</div>