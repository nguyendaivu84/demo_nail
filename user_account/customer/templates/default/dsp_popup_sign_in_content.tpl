<!DOCTYPE html>
<html id="html_" class="locale-ca user-unrecognized js no-touch postmessage hashchange history localstorage sessionstorage" >
<head>
    <meta charset="utf-8">
    <title>Sign in form</title>
    <meta content="" name="description">
    <meta content="width=520" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <meta content="" name="contextPath">
    <meta content="/images/" name="imageBasePath">
    <meta content="" name="bvctx">
    <meta content="DHEfxRvk5978fvUKaGrOCpRByg5EXyhJBxhEbM0rZKk" name="google-site-verification">
    <meta content="NON_BI" name="bi_status">
    <meta content="ok" name="sitescope">
    <link href="[@URL_TEMP]css/login-fix.css" rel="stylesheet">
    <style>
        html { min-width: 0; }
        body { padding: 0; overflow: hidden; }
    </style>
    <script src="[@URL_TEMP]js/modernizr.js"></script>
    <script src="[@URL_TEMP]js/jquery.js"></script>
    <script src="[@URL]lib/js/common.js"></script>
</head>
<style>
    .hide_{
        display: none;
    }
</style>
<script>
    $(document).ready(function(){
        var is_login = '[@IS_LOGIN]';
        is_login = parseInt(is_login,10);
        if(is_login){
            window.parent.location.reload();
        }
        $("#forgot_password").on("click",function(){
            close_popup('popupcontent_sign_in');
            parent.$("#reset_password").trigger("click");
        });
        $("input").on("keypress",function(e){
            if(e.keyCode==13 || e.which ==13){
                e.preventDefault();
                $("#sign_in").trigger("click");
            }
        });
    });
</script>
<body id="secureBody">
<div id="signin-pop" class="signin" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
    <div class="modal-header">
        <button class="icon icon-close" onclick="close_popup('popupcontent_sign_in');" aria-hidden="true" data-dismiss="modal" style="display: inline" type="button"></button>
        <h4 class="modal-title">Sign in</h4>
    </div>
    <div class="modal-body">
        <table>
            <tr>
                <td>
                    <div class="signin-returning">

                        <h2>I do not have an account</h2>
                        <p><strong>Sign up at &nbsp;- Astoria Nail Supply and:</strong></p>
                        <ul>
                            <li>Check order status</li>
                            <li>View previous orders and invoices</li>
                            <li>Retrieve previous shopping cart</li>
                        </ul>
                        <button onclick="close_sign_in_open();" class="btn btn-primary btn-lg btn-register" id="register_bnt" name="register_bnt" type="submit">register</button>
                    </div>
                </td>
                <td style="vertical-align:justify;padding-right: 20px; padding-bottom: 80px;"><h3>OR</h3></td>
                <td>
                    <div class="signin-returning">
                        <h2>I am a registered customer</h2>
                        <form id="loginPopupMainForm" method="post" action="" name="loginPopup">
                            <p style="[@DISPLAY]" class="error-message">[@ERROR]</p>
                            <div class="email container">
                                <label for="user_name">E-mail address</label>
                                <input id="user_name" name="user_name" class="form-control" type="email" value="[@EMAIL]" >
                            </div>
                            <div class="password container">
                                <label for="password">Password</label>
                                <input id="password" class="form-control" type="password" autocomplete="off" value="[@PASSWORD]"  name="password">
                                <p class="help-block">
                                    <a class="reset-password" id="forgot_password" href="javascript:void(0)">forgot password?</a>
                                </p>
                                <input id="forgotPasswordLinkDo" type="submit" value="ForgotPasswordLink" name="ForgotPasswordLink" style="display:none;">
                            </div>
                            <button class="btn btn-primary btn-lg btn-register" data-ajaxlink="" name="sign_in" id="sign_in" type="submit">sign in</button>

                        </form>
                    </div>
                <td>
            </tr>
        </table>

    </div>
</div>
<div id="pop-terms" class="modal" style="display: none;"> </div>
</body>
</html>