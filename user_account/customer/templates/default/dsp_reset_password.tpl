<!DOCTYPE html>
<html class="locale-ca user-unrecognized js no-touch postmessage hashchange history localstorage sessionstorage" >
<head>
    <meta charset="utf-8">
    <title>Sign in form</title>
    <meta content="" name="description">
    <meta content="width=520" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <link rel="Shortcut Icon" type="image/ico" href="/images/icons/logos.ico" />
    <meta content="" name="contextPath">
    <meta content="/images/" name="imageBasePath">
    <meta content="" name="bvctx">
    <meta content="DHEfxRvk5978fvUKaGrOCpRByg5EXyhJBxhEbM0rZKk" name="google-site-verification">
    <meta content="NON_BI" name="bi_status">
    <meta content="ok" name="sitescope">
    <link href="[@URL_TEMP]css/main.css" rel="stylesheet">
    <style>
        html { min-width: 0; }
        body { padding: 0; overflow: hidden; }
    </style>
    <script src="[@URL_TEMP]/js/modernizr.js"></script>
    <script src="[@URL_TEMP]/js/jquery.js"></script>
    <script src="[@URL_TEMP]/js/bpopup.js"></script>
    <script src="[@URL]lib/js/common.js" ></script>
    <script>
        $(document).ready(function(){
            $("#send_success").hide();
            $("#register_bnt").on("click",function(){
                var user_name = $("#user_name").val();
                if(user_name==''){
                    alert("email cannot empty!");
                    return false;
                }
                if(!isValidEmail(user_name)){
                    alert("Email is not valid!");
                    return false;
                }
                sendEmailResetPassword('[@URL]',user_name);
                $("#user-reg-popup").hide();
                $("#send_success").show();
            });
        });
    </script>
</head>
<script>
</script>
    <body id="secureBody">
        <div id="forget-password-pop" class="pop-register" >
            <div class="modal-header">
                <button class="icon icon-close" onclick="close_popup('popupcontent_reset_password');" aria-hidden="true" data-dismiss="modal" style="display: inline" type="button"></button>
                <h4 class="modal-title">Reset password</h4>
            </div>
            <form id="user-reg-popup" method="post" action="" name="POSRegistration" novalidate="novalidate">
                Please provide your email
                <div class="modal-body">
                <span class="form-start"></span>
                <div class="sign-up">
                <fieldset class="two-column email-section">
                <div class="">
                    <label for="user_name">
                        <span class="required">*</span>
                        E-mail address
                    </label>
                    <input id="user_name" class="form-control" style="height:30px;" placeholder="Email address" value="[@EMAIL]" type="email" value="" name="user_name">
                    <button class="btn btn-primary btn-lg btn-register" id="register_bnt" name="register_bnt" type="button">Reset password</button>
                </div>
                </fieldset>
            </div>

            </div>
            </form>
            <div id="send_success" class="modal-body">
                <span class="form-start"></span>
                <div class="sign-up">
                   Random password has been sent to your email!
                </div>

            </div>
        </div>
    </body>
</html>