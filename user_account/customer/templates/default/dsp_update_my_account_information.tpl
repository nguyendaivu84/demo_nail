<style>
    .page-intro h1 {
        margin-left: 0;
        padding-left: 17px;
        border-bottom: 5px solid #000;
    }
</style>
<div class="page-intro">
    <h1>my information</h1>
</div>
<div class="maincontent-content">
    <div class="account-module account-info">
        <h2>Account Information</h2>
        <p id="error_message" class="error-message" style="color:red !important;font-size: 16px;">.</p>
        <form id="updateAccountInformation" method="post" class="ng-pristine ng-valid">

            <div class="name">
                <p class="summary">
                    <span class="label">Name:</span>
                    <span id="full_name" class="value"><span class="OneLinkNoTx">[@FULL_NAME]</span></span>
                    <button id="editName" data-update="name" onclick="showUpdate('name');" class="btn btn-primary btn-update" value="name" name="editName" type="button">update</button>
                </p>
                <div id="name" class="update">
                    <fieldset class="two-column container">
                        <div class="form-group column">
                            <label for="first_name">First name:</label>
                            <input id="first_name" name="first_name" value="[@FIRST_NAME]" class="form-control" type="text"><input name="_D:first_name" value=" " type="hidden"></div>
                        <div class="form-group column">
                            <label for="last_name">Last name:</label>
                            <input id="last_name" name="last_name" value="[@LAST_NAME]" class="form-control" type="text"><input name="_D:last_name" value=" " type="hidden"></div>
                    </fieldset>
                    <div class="buttons container">
                        <button class="btn btn-primary btn-update" onclick="updateName('name')" value="name" name="name" type="button">update</button>
                        <button class="btn btn-default btn-cancel" onclick="cancelUpdate('name')" value="cancel" name="cancel" type="button">cancel</button>
                    </div>
                </div>
            </div>

            <div  class="email">
                <p class="summary">
                    <span class="label">E-mail Address:</span>
                    <span id="full_email" class="value"><span class="OneLinkNoTx">[@EMAIL]</span></span>
                    <button id="editEmail" data-update="name" onclick="showUpdate('email');" class="btn btn-primary btn-update" value="name" name="editEmail" type="button">update</button>
                </p>
                <div id="email" class="update">
                    <fieldset class="two-column container">
                        <div class="form-group column">
                            <label for="user_name">E-mail address:</label>
                            <input id="user_name" maxlength="60" name="user_name" value="" class="form-control" type="email"></div>
                        <div class="form-group column">
                            <label for="confirm_user_name">Confirm e-mail address:</label>
                            <input id="confirm_user_name" name="confirm_user_name" value="" class="form-control" type="email"></div>
                    </fieldset>
                    <div class="buttons container">
                        <button class="btn btn-primary btn-update" onclick="updateName('email')" value="email" name="email" type="button">update</button>
                        <button class="btn btn-default btn-cancel" onclick="cancelUpdate('email')" value="cancel" name="cancel" type="button">cancel</button>
                    </div>
                </div>
            </div>
            <div  class="password">
                <p class="summary">
                    <span class="label">Password:</span>
                    <span class="value OneLinkNoTx">*******</span>
                    <button id="editPass" data-update="pass" onclick="showUpdate('passwordUpdate');" class="btn btn-primary btn-update" value="name" name="editPass" type="button">update</button>
                </p>
                <div id="passwordUpdate" class="update">
                    <fieldset class="two-column container">
                        <div class="form-group column">
                            <label for="password">Password:</label>
                            <input id="password" name="password" value="" class="form-control" type="password" autocomplete="off"></div>
                        <div class="form-group column">
                            <label for="password_confirmation">Confirm password:</label>
                            <input id="password_confirmation" name="password_confirmation" value="" class="form-control" type="password" autocomplete="off"></div>
                    </fieldset>
                    <div class="buttons container">
                        <button class="btn btn-primary btn-update" onclick="updateName('passwordUpdate')" value="password" name="password" type="button">update</button>
                        <button class="btn btn-default btn-cancel" onclick="cancelUpdate('passwordUpdate')" value="cancel" name="cancel" type="button">cancel</button>
                    </div>
                </div>
            </div>
            <!--div id="module-address-book" class="account-module address-book container ng-scope" data-maxaddresses="10" ng-controller="UserAddressController">

                <h2>Address Book</h2>

                <div ng-show="editedAddress" class="add-address-form ng-hide">

                    <form id="newShippingAddressForm" method="post" action="blank.jsp" name="newShippingAddress" class="ng-pristine ng-valid">

                        <input type="hidden" name="id" ng-model="editedAddress.id" class="ng-pristine ng-valid">
                        <div class="add-update-info">
                            <span class="requirements" style="display: none;"><span>*</span>Required</span>
                            <fieldset class="two-column container">
                                <div class="form-group column">
                                    <label for="firstName"><span class="required">*</span>First name</label>
                                    <input ng-model="editedAddress.first_name" type="text" name="first_name" ng-class="{'error': errors.field.first_name!==undefined}" class="form-control ng-pristine ng-valid">
                                </div>
                                <div class="form-group column">
                                    <label for="lastName"><span class="required">*</span>Last name</label>
                                    <input ng-model="editedAddress.last_name" type="text" name="last_name" ng-class="{'error': errors.field.last_name!==undefined}" class="form-control ng-pristine ng-valid">
                                </div>
                            </fieldset>
                            <fieldset class="two-column container">
                                <div class="form-group column">
                                    <label for="address1"><span class="required">*</span>Address line 1</label>
                                    <input ng-model="editedAddress.address1" type="text" name="address1" ng-class="{'error': errors.field.address1!==undefined}" maxlength="30" class="form-control ng-pristine ng-valid">
                                </div>
                                <div class="form-group column">
                                    <label for="address2">Address line 2</label>
                                    <input ng-model="editedAddress.address2" type="text" name="address2" ng-class="{'error': errors.field.address2!==undefined}" maxlength="30" class="form-control ng-pristine ng-valid">
                                </div>
                            </fieldset>
                            <fieldset class="two-column container">
                                <div class="form-group column">
                                    <label for="city"><span class="required">*</span>City</label>
                                    <input ng-model="editedAddress.city" type="text" name="city" maxlength="30" ng-class="{'error': errors.field.city!==undefined}" class="form-control ng-pristine ng-valid">
                                </div>
                                <div class="form-group column">
                                    <label for="state" state-label="editedAddress.country"><span class="required">*</span><span></span></label>

                                    <input type="text" id="state" ng-show="isFreeStates" ng-model="editedAddress.state" ng-class="{'error': errors.field.state!==undefined}" class="form-control ng-pristine ng-valid ng-hide">

                                    <select id="state" ng-hide="isFreeStates" ng-model="editedAddress.state" country-states="editedAddress.country" class="form-control ng-pristine ng-valid" ng-class="{'error': errors.field.state!==undefined}"><option value="? undefined:undefined ?"></option>
                                    </select>

                                </div>
                            </fieldset>
                            <fieldset class="two-column container">
                                <div class="form-group column">
                                    <label for="zip"><span class="required">*</span><span zip-label="editedAddress.country"></span></label>
                                    <input ng-model="editedAddress.zip" type="text" name="zip" ng-class="{'error': errors.field.zip!==undefined}" maxlength="10" class="form-control zip ng-pristine ng-valid">
                                </div>
                                <div class="form-group column">

                                    <label for="country"><span class="required">*Country</span></label><div class="country-static"></div>
                                    <select name="country" id="country" ng-change="onCountryChange()" ng-model="editedAddress.country" class="form-control ng-pristine ng-valid">
                                        <option selected="selected" value="">select a country</option>
                                        <option value="US">United States</option>
                                        <option value="CA">Canada</option>
                                        <option value="DE">Germany</option>
                                        <option value="JP">Japan</option>
                                        <option value="KR">Korea, Republic of</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="NO">Norway</option>
                                        <option value="GB">United Kingdom</option>
                                    </select>
                                </div>
                            </fieldset>
                            <fieldset class="phone-controls container">
                                <div class="form-group">
                                    <label for="phoneNumber"><span class="required">*</span>Phone</label>
                                    <div ng-switch="" on="editedAddress.isUSorCA()">

                                       <div ng-switch-default="" class="ng-scope">
                                            <input ng-model="editedAddress.phone" name="phone" maxlength="15" type="text" class="form-control ng-pristine ng-valid" ng-class="{'error': errors.field.phone!==undefined}">
                                        </div>
                                    </div>
                                </div>
                                <div ng-show="editedAddress.isUSorCA()" class="form-group phone-ext ng-hide">
                                    <label for="phone_ext">Ext.</label>
                                    <input ng-model="editedAddress.phone_ext" type="text" maxlength="10" id="phone_ext" class="form-control phone-control ng-pristine ng-valid" ng-class="{'error': errors.field.phone_ext!==undefined}">
                                </div>
                            </fieldset>
                            <label class="checkbox">
                                <input ng-model="editedAddress.is_default" type="checkbox" name="is_default" id="is_default" class="ng-pristine ng-valid">
                                Choose as default shipping address
                            </label>
                            <ul class="address-actions container">
                                <li ng-hide="editedAddress.id" id="newAddress" class="">
                                    <button class="btn btn-primary btn-add-address" type="button" name="add-address" ng-click="submitAddress($event)">add address</button>
                                </li>
                                <li ng-show="editedAddress.id" id="updateAddress" class="ng-hide">
                                    <button class="btn btn-primary btn-update" type="button" name="update" ng-click="submitAddress($event)">update</button>
                                </li>
                                <li>
                                    <button ng-click="cancelEdit()" class="btn btn-default btn-cancel" type="button" name="cancel">cancel</button>
                                </li>
                            </ul>
                        </div>

                    </form>
                </div>

                <ul ng-hide="editedAddress" class="address-actions container">
                    <li><button ng-click="editNewAddress($event)" class="btn btn-primary btn-add-new-address" type="button">add new address</button></li>
                </ul>

                <div class="addresses container">

                </div>

                <script id="addresses" type="text/json">
		[]</script>
                <div class="addresses container full-view">

                </div>
            </div>
            <div id="modal-confirm" class="modal">
                <div class="modal-header">
                    <h4 class="modal-title">delete address</h4>
                </div>
                <div class="modal-body">
                    <p class="warning-message large">Are you sure you would like to permanently delete your address?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-lg btn-yes" type="submit" value="yes" name="yes">yes</button>
                    <button class="btn btn-default btn-lg btn-no" type="cancel" value="no" name="no">no</button>
                </div>
            </div-->
        </form>
    </div>
</div>
