<ul class="breadcrumb"></ul>
<div class="content free-samples container">
	<div class="sidebar">
		<div class="sidenav">
			<h2 class="nav-title">Offers</h2>
			<ul class="nav nav-stacked">
				<li>Free Samples</li>
			</ul>
		</div>
	</div>
	<div id="main" class="maincontent">
		<div class="page-intro">
			<h1>Samples</h1>
		</div>
		<div class="maincontent-content samples">
			<form id="samples" name="sample" action="/offers/samples.jsp?_DARGS=/offers/samples.jsp.samples" method="post">
				<div class="samples-content">
					<h2 class="alt-book">Select 3 free samples, then add to the basket</h2>
					<div class="sample-items container">
						<label for="sample-1339449" class="sample-item custom-radio">
							<input id="sample-1339449" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1339449" class="sample-1339449" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1339449-main-grid.jpg" title="Soy Face Cleanser" width="135" height="135">
								<a id="sku-1339449" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Fresh Soy Face Cleanser - 0.14 oz</span>
						</label>
						<label for="sample-1133024" class="sample-item custom-radio">
							<input id="sample-1133024" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1133024" class="sample-1133024" type="checkbox" checked="">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1133024-main-grid.jpg" title="LE MALE" width="135" height="135">
								<a id="sku-1133024" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Jean Paul Gaultier LE MALE Eau de Toilette - 0.04 oz</span>
						</label>
						<label for="sample-1592799" class="sample-item custom-radio">
							<input id="sample-1592799" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1592799" class="sample-1592799" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1592799-main-grid.jpg" title="GOLD" width="135" height="135">
								<a id="sku-1592799" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">JAY Z GOLD - 0.05 oz Eau de Toilette Spray</span>
						</label>
						<label for="sample-1503499" class="sample-item custom-radio">
							<input id="sample-1503499" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1503499" class="sample-1503499" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1503499-main-grid.jpg" title="Déclaration d'Un Soir" width="135" height="135">
								<a id="sku-1503499" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">
								Cartier Declaration d'Un Soir - 0.05 oz Eau de Toilette Spray
							</span>
						</label>
						<label for="sample-1590934" class="sample-item custom-radio">
							<input id="sample-1590934" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1590934" class="sample-1590934" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1590934-main-grid.jpg" title="Guilty Black Pour Homme" width="135" height="135">
								<a id="sku-1590934" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Gucci Guilty Black Pour Homme - 0.06 oz Eau de Toilette</span>
						</label>
						<label for="sample-1580141" class="sample-item custom-radio">
							<input id="sample-1580141" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1580141" class="sample-1580141" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1580141-main-grid.jpg" title="Phytobaume Hydration Express Conditioner " width="135" height="135">
								<a id="sku-1580141" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Phyto Phytobaume Volume Express Conditioner - 0.4 oz</span>
						</label>
						<label for="sample-1520774" class="sample-item custom-radio">
							<input id="sample-1520774" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1520774" class="sample-1520774" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1520774-main-grid.jpg" title="CC Cream Broad Spectrum SPF 30 Complexion Corrector" width="135" height="135">
								<a id="sku-1520774" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">
								Peter Thomas Roth CC Cream Broad Spectrum SPF 30 Complexion Corrector - 3 x 0.02 oz in Light to Medium, Medium to Tan, Deep
							</span>
						</label>
						<label for="sample-1507102" class="sample-item custom-radio">
							<input id="sample-1507102" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1507102" class="sample-1507102" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1507102-main-grid.jpg" title="J'adore Voile de Parfum" width="135" height="135">
								<a id="sku-1507102" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Dior J'adore Voile de Parfum - 0.03 oz Eau de Toilette Spray</span>
						</label>
						<label for="sample-1295849" class="sample-item custom-radio">
							<input id="sample-1295849" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1295849" class="sample-1295849" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1295849-main-grid.jpg" title="Daisy" width="135" height="135">
								<a id="sku-1295849" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Marc Jacobs Daisy Eau De Toilette - 0.04 oz</span>
						</label>
						<label for="sample-1599463" class="sample-item custom-radio">
							<input id="sample-1599463" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1599463" class="sample-1599463" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1599463-main-grid.jpg" title="Do Not Age with Dr. Brandt Transforming Pearl Serum" width="135" height="135">
								<a id="sku-1599463" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">
								dr. brandt Do Not Age with Dr. Brandt Transforming Pearl Serum - 0.034 oz
							</span>
						</label>
						<label for="sample-1445543" class="sample-item custom-radio">
							<input id="sample-1445543" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1445543" class="sample-1445543" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1445543-main-grid.jpg" title="Rain" width="135" height="135">
								<a id="sku-1445543" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">Clean Rain - 0.03 oz Eau de Parfum</span>
						</label>
						<label for="sample-1580844" class="sample-item custom-radio">
							<input id="sample-1580844" name="/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value="1580844" class="sample-1580844" type="checkbox">
							<input name="_D:/atg/commerce/order/purchase/astoriaSampleCartModifierFormHandler.catalogRefIds" value=" " type="hidden">
							<span class="input"></span>
							<span class="product-image">
								<img src="/user_account/customer/templates/default/img/s1580844-main-grid.jpg" title="Brow Powder Duo" width="135" height="135">
								<a id="sku-1580844" rel="#mini-quick-look" href="#" class="qlook">QUICK LOOK</a>
							</span>
							<span class="OneLinkNoTx">
								Anastasia Beverly Hills Brow Powder Duo Brow Powder - 0.0002 oz in Dark Brown
							</span>
						</label>
					</div>
					<div class="samples-foot container">
						<div class="add-samples pull-right">
							<button class="btn btn-alt btn-lg btn-add-to-basket" value="add to basket" name="add-to-basket" type="submit">add to basket</button>
						</div>
						<p>You can confirm your chosen samples during checkout.</p>
						<p>
							Only one of each sample can be added per order.
							<span class="warning-message">Substitutions will be made for out-of-stock samples.</span>
						</p>
					</div>
				</div>
				<input name="_DARGS" value="/offers/samples.jsp.samples" type="hidden"></form>
		</div>
	</div>
</div>