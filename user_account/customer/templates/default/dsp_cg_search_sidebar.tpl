<div class="sidebar">
    <div style="[@STYLE]" class="sidenav search-navigation">
        <ul class="nav nav-stacked" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
            [@DSP_CG_SEARCH_SIDEBAR_CATEGORY]
        </ul>
    </div>
    <div class="social-links social-components">
        <div class="share-btn icon icon-facebook-large social-enabled hidden" data-socialtype="fb" data-socialurlprefix="https://www.facebook.com/sharer/sharer.php?u=">
            <a href="#" target="_blank"></a>
        </div>
        <div id="pinterest-root" class="pin hidden share-btn icon icon-pinterest-large">
            <a data-pin-config="beside" href="#" data-pin-do="buttonPin">
                <img src="/user_account/customer/templates/default/img/pin_it_button.png"></a>
        </div>
    </div>
</div>