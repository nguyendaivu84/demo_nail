<!DOCTYPE html>
<html class="locale-ca user-unrecognized js no-touch postmessage hashchange history localstorage sessionstorage" >
<head>
    <link rel="stylesheet" href="[@URL_CUSTOMER]/css/main.css">
    <script src="[@URL_CUSTOMER]/js/jquery.js"></script>
    <script src="[@URL_CUSTOMER]/js/bpopup.js"></script>
    <script src="[@URL]/lib/js/common.js"></script>
    <script>
        $(document).ready(function(){
            $("a.more").on("click",function(){
                var link = $(this).attr("data-link");
                window.parent.location.href = '[@URL]'+link;
            });
        });
    </script>
    <style>
        .quick-look .left-column {
            text-align: center;
            width: 163px;
        }
         .product-text {
            border-bottom-color: #000000;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            font-size: 12px;
            line-height: 16px;
            margin-bottom: 10px;
            padding-bottom: 5px;
             width:50%;
        }
        .quick-look .left-column, .quick-look .product-description {
            float: left;
            position: relative;
        }
        .quick-look .product-description {
            margin-left: 30px;
            width: 312px;
        }
        .quick-look .left-column, .quick-look .product-description {
            float: left;
            position: relative;
        }
        .icon-close {
            background-position: -84px -31px;
            cursor: pointer;
            height: 15px;
            width: 15px;
            position: absolute;
            right: 0px;
            top: 0px;
        }
        .stars{
            float: left;
            margin-top: 2px;
        }
        .text-underline{
            float: left;
            margin-right: 10px;
        }
    </style>
</head>
<body id="home" class="home" data-social="true">
<div id="quick-look" class="" style="top: 774.5px; display: block;margin-top: 20px;">
    <div class="container" style="height: auto;">
        <div class="modal-body container hide" style="display: block; width:100%">
            <div style="width:15%;float:left" class="left-column">
                <a href="[@PRODUCT_SLUGGER]">
                    <img id="modal-view-larger" rel="#modal-view-larger" data-link="[@PRODUCT_IMAGE]" class="hero-image" width="135" height="135" src="[@PRODUCT_IMAGE]" alt="">
                </a>
                <div class="messages">
                    <p class="flags-market" style="text-align: center">
                        <span class="flag flag-ex">exclusive</span>
                    </p>
                </div>
                <div class="seal">
                    <div class="seals-bi">
                        <span class="sprite seal-bi seal-bi-sm hidden" data-bi-lvl="BI"></span>
                        <span class="sprite seal-bi seal-vib-sm hidden" data-bi-lvl="VIB"></span>
                        <span class="sprite seal-bi seal-rouge-sm hidden" data-bi-lvl="Rouge"></span>
                    </div>
                </div>
            </div>
            <div style="width:80%;float:left"  class="product-description container" data-brand="Dolce & Gabbana">
                <h4 class="ql-product-name">
                    <a href="javascript:void(0)" class="more" data-link="[@PRODUCT_SLUGGER]">
                        <span class="product-name OneLinkNoTx">
                            <span class="brand">[@PRODUCT_BANNER]</span>
                            [@PRODUCT_NAME]
                        </span>
                    </a>
                    <span class="separator">|</span>
                    <span class="sku-price">
                        <span class="list-price">
                            <span class="currency">C</span>
                            <span class="price">[@PRODUCT_PRICE](C[@PRODUCT_PRICE_DISCOUNT] Value)</span>
                        </span>
                    </span>
                </h4>
                <div class="product-text">
                    <p>
                        [@PRODUCT_DETAIL]
                        <br>
                        <a class="see-more more" data-link="[@PRODUCT_SLUGGER]" href="javascript:void(0)" class="more"> see more </a>
                    </p>
                </div>
                <div class="info-row">
                    <span class="sku">
                    <span class="label">Item #</span>
                    <span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
                    </span>
                    <span class="size">
                    <span class="label">Size</span>
                    <span class="value">0.8 oz</span>
                    </span>
                </div>
                <div class="sku-selector swatch-sq-lg">
                    <div class="sku-selector-content container">
                        [@PRODUCT_SLIDE_IMAGE]
                    </div>
                </div>
                <div class="product-actions">
                    <form id="addToBasketForm" method="post" action="" name="addToBasketForm">
                        <div class="button-holder container">
                            <div class="product-quantity" style="float:left; margin-right: 10px;">
                                <label class="inline" for="563320">QTY</label>
                                <select id="ql_quantity" class="form-control OneLinkNoTx quantitySelector ">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <button style="float:left;width:125px; margin-right: 10px" class="btn btn-alt btn-lg btn-block btn-add-to-basket" onclick="add_to_basket('[@PRODUCT_ID]',ql_quantity.value,'[@URL][@PRODUCT_SLUGGER]',0); " type="button" name="add-to-basket">Add to cart</button>
                            <!--button data-check="[@IS_ON_LOVE]" style="width:130px; margin-top: 1px" onmouseout="change_text_love('[@IS_ON_LOVE]','[@PRODUCT_SLUGGER]');" onmouseover="change_text_unlove('[@IS_ON_LOVE]','[@PRODUCT_SLUGGER]');" id="P_[@PRODUCT_SLUGGER]" onclick="add_item_to_love_list('[@PRODUCT_SLUGGER]','add_love');"  class="btn btn-love btn-block btn-lg hover-enable [@CLASS_LOVE]" data-product_id="P12420" type="button">
                                <span class="btn-inner">
                                    <!--span class="icon icon-love"></span-->
                                    <!--span id="text_[@PRODUCT_SLUGGER]" class="btn-text">[@TEXT_LOVE]</span>
                                </span>
                            </button-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <button class="icon icon-close" onclick="close_popup('popup_quick_look');" aria-hidden="true" data-dismiss="modal" type="button"></button>
</div>
</body>
</html>