<!doctype html>
<!--[if lt IE 9]><html class="oldie" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Tony nail</title>
    <link rel="stylesheet" href="[@URL_CUSTOMER]/css/main.css">
    <link rel="stylesheet" href="[@URL_CUSTOMER]/css/account.css">
    <style>
        html { min-width: 0; }
        body { padding: 0 20px; width: 606px; overflow: scroll}
        .masthead-print { margin-bottom: 20px; }
        .page-actions { text-align: right; }
        .table-order-detail .item-image { width: 70px; }
    </style>
    <style>
        @media print {
            .page-actions { display: none; }
            .table td { border-color: #ccc; }
        }
    </style>
    <script src="[@URL_CUSTOMER]js/modernizr.js"></script>
</head>
<body>

<div class="masthead-print">
    <img height="89" width="566" alt="astoria" src="[@URL]images/logo.png">
</div>

<p class="page-actions container">
    <a href="#" class="btn btn-primary" onclick="window.print()">print list</a>
    <a href="#" class="btn btn-default" onclick="window.close()">close window</a>
</p>

<table class="table table-order-detail">
    <thead>
    <tr>
        <th class="item-image">Item</th>
        <th class="item-description"></th>
        <th class="price">Price</th>
    </tr>
    </thead>
    <tbody>
        [@TR_TABLE]
    </tbody>
</table>

</body>
</html>
