<!DOCTYPE html>
<html class="locale-ca user-unrecognized js no-touch postmessage hashchange history localstorage sessionstorage" >
    <head>
        <meta charset="utf-8">
        <title>Sign in form</title>
        <meta content="" name="description">
        <meta content="width=520" name="viewport">
        <meta content="telephone=no" name="format-detection">
        <meta content="" name="contextPath">
        <meta content="/images/" name="imageBasePath">
        <meta content="" name="bvctx">
        <meta content="DHEfxRvk5978fvUKaGrOCpRByg5EXyhJBxhEbM0rZKk" name="google-site-verification">
        <meta content="NON_BI" name="bi_status">
        <meta content="ok" name="sitescope">
        <link href="[@URL_TEMP]css/main.css" rel="stylesheet">
        <style>
            html { min-width: 0; }
            body { padding: 0; overflow: hidden; }
        </style>
        <script src="[@URL_TEMP]/js/modernizr.js"></script>
        <script src="[@URL_TEMP]/js/jquery.js"></script>
        <script src="[@URL_TEMP]/js/bpopup.js"></script>
        <script src="[@URL]lib/js/common.js" ></script>
    </head>
    <script>
       $(document).ready(function(){
           if('[@ERROR_EXISTED_STYLE]'!='hidden') parent.$("#login").css('min-height',300);
//           parent.$("#sign-up").css('min-height',300);
           $("#help_icon_confirm_passoword").on("click",function(){
               $("#pop-password").bPopup({
                       opacity:0
                       ,escClose :true
                   }
               );
           });
           $("#link_term_conditions").on("click",function(){
               $("#pop-social-terms").bPopup({
                           opacity:0
                           ,escClose :true
                       }
               );
           });
           $("#checkbox_icon").on("click",function(){
               $("#optOut").bPopup({
                           opacity:0
                           ,escClose :true
                       }
               );
           });

           var is_login = '[@IS_LOGIN]';
           is_login = parseInt(is_login,10);
           if(is_login) window.parent.location.reload();
           $("#user-reg-popup").on("submit",function(){
                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var user_name = $("#user_name").val();
                var confirm_user_name = $("#confirm_user_name").val();
                var password = $("#password").val();
                var password_confirmation = $("#password_confirmation").val();
                var security_answer = $("#security_answer").val();
                if(first_name =='' || last_name=='' || user_name=='' ||  confirm_user_name=='' || password=='' || password_confirmation=='' || security_answer =='' ){
                    alert("* fields is required");
                    return false;
                }
                if(!isValidEmail(user_name)){
                    alert("Email is invalid!");
                    return false;
                }
                if(!isValidEmail(confirm_user_name)) {
                    alert("Confirm email is invalid!"); return false;
                }
                if(password.length >12 || password < 6){
                    alert("Password must between 6 and 12 letter"); return false;
                }
                if(user_name!=confirm_user_name) {
                    alert("Confirm email not match"); return false;
                }
               if(!$("#tc-agree").is(":checked")){
//                   alert("You must agree with Tony Term & Condition ");
//                   return false;
               }
           });
           $("input").on("keypress",function(e){
               if(e.keyCode==13 || e.which ==13){
                   e.preventDefault();
                   $("#register_bnt").trigger("click");
               }
           });
       });
    </script>
    <body id="secureBody" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
        <div id="register-pop" class="pop-register" >
            <form id="user-reg-popup" method="post" action="" name="POSRegistration" novalidate="novalidate">
            <div class="modal-header">
                <button onclick="close_popup('popupcontent_register');" class="icon icon-close" aria-hidden="true" data-dismiss="modal" style="display: inline" type="button"></button>
                <span class="label-required">*Required</span>
                <h4 class="modal-title">register with us</h4>
            </div>
            <div class="modal-body">
            <p class="pos [@ERROR_EXISTED_STYLE] warning-message">
                We think you already registered for
                <span class="OneLinkNoTx">Beauty Insider</span>
                in a Astoria Nails Supply store because we recognize your email address
                <b>
                    <span class="user-name"></span>
                </b>
                . Please fill out the information below to complete your profile.
                <br>
                <br>
                Not you? To clear the registration information and start over,
                <a class="clear-pos" onclick="close_register_open_sign_in();" href="javascript::void(0);">click here</a>
                .
            </p>
            <span class="form-start"></span>
            <div class="sign-up">
            <fieldset class="two-column container">
                <div class="form-group column">
                    <label for="first_name">
                        <span class="required">*</span>
                        First name
                    </label>
                    <input id="first_name" value="[@FIRST_NAME]" class="form-control" type="text" value="" name="first_name">
                </div>
                <div class="form-group column">
                    <label for="last_name">
                        <span class="required">*</span>
                        Last name
                    </label>
                    <input id="last_name" value="[@LAST_NAME]" class="form-control" type="text" value="" name="last_name">
                </div>
            </fieldset>
            <fieldset class="two-column email-section">
                <div class="form-group column">
                    <label for="user_name">
                        <span class="required">*</span>
                        E-mail address
                    </label>
                    <input id="user_name" class="form-control" value="[@EMAIL]" type="email" value="" name="user_name">
                </div>
                <div class="form-group column">
                    <label for="confirm_user_name">
                        <span class="required">*</span>
                        Confirm e-mail address
                    </label>
                    <input id="confirm_user_name" class="form-control" type="email" value="[@EMAIL]" name="confirm_user_name">
                </div>
            </fieldset>
            <fieldset class="two-column container">
                <div class="form-group column">
                    <label for="password">
                        <span class="required">*</span>
                        Password
                    </label>
                    <input id="password" class="form-control" type="password" autocomplete="off" value="" name="password">
                </div>
                <div class="form-group column">
                    <label for="password_confirmation">
                        <span class="required">*</span>
                        Confirm password
                        <a class="icon icon-help" id="help_icon_confirm_passoword" rel="#confirm-password-pop" href="#"></a>
                    </label>
                    <input id="password_confirmation" class="form-control" type="password" autocomplete="off" value="" name="password_confirmation">
                    <div id="pop-password" class="modal modal-small">
                        <div class="modal-header">
                            <button class="icon icon-close" onclick="close_popup_lv1('pop-password');" aria-hidden="true" data-dismiss="modal" type="button"></button>
                            <h4 class="modal-title">Password</h4>
                        </div>
                        <div class="modal-body">
                            <p>To keep your astoria.com account secure, you must create a unique password. It should be between 6 and 12 characters.</p>
                            <p>You can always update your password in the future on the "My Account" page.</p>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div id="pop-changing-password" class="modal">
                <div class="modal-header">
                    <button class="icon icon-close" onclick="close_popup_lv1('pop-changing-password');" aria-hidden="true" data-dismiss="modal" type="button"></button>
                    <h4 class="modal-title">changing your password</h4>
                </div>
                <div class="modal-body"> </div>
            </div>
            <div id="pop-social-terms" class="modal">
            <div class="modal-header">
                <button id="term_and_conditions" onclick="close_popup_lv1('pop-social-terms');" class="icon icon-close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">legal terms & conditions</h4>
            </div>
                <div  class="modal-body">
                    <div class="modal-scroll custom-scroll">
                        <div class="html-component">
                            <div class="indent">
                                <p>
                                    <span style=" font-size: medium;">
                                    <b>Terms of Use</b>
                                    </span>
                                </p>
                                <p>
                                    <br>
                                    Last Updated September 18, 2012
                                    <br>
                                    <br>
                                    These are the terms of use ("Terms of Use") for your use of services or features on the sites owned and controlled by astoria USA, Inc. ("astoria"), including www.astoria.com, http://theglossy.astoria.com, and www.ttbeauty.com (the "Sites"). We may add additional Sites from time to time as we expand our beauty offerings and these Terms of Use will govern those new Sites when added. You may be accessing our Sites from a computer or mobile phone device (through an iPhone application, for example) and these Terms of Use govern your use of our Sites and your conduct, regardless of the means of access. You may be using our interactive services ("Interactive Services"), such as our BeautyTalk community forum, Ratings & Reviews (the "RR Service"), the Question & Answer service (the "Q&A Service") and these Terms of Use govern your use of those Interactive Services.
                                </p>
                                <p> </p>
                                <p>We also like to interact with you on third party sites where we post content or invite your feedback, such as www.facebook.com/astoria, www.twitter.com/astoria, and www.youtube.com/astoria ("Third Party Sites"). Our Terms of Use and other areas of our Sites provide guidelines ("Guidelines") and rules and regulations ("Rules") in connection with our Interactive Services, including services that involve Third Party Sites, but astoria does not control those Third Party Sites, and these Terms of Use, our Guidelines and our Rules do not apply to companies that astoria does not own or control, or to the actions of people that astoria does not employ or manage. You should always check the terms of use posted on Third Party Sites.</p>
                                <p> </p>
                                <p>
                                    By using the Sites, you signify your agreement to these Terms of Use, our
                                    <a <u="" href="/customerService/customerServiceTemplate.jsp?mediaId=12300066"> Privacy Policy</a>
                                    and our Guidelines and Rules, whether or not you have read them. If you do not agree with any of these, you should not use our Sites.
                                </p>
                                <p> </p>
                                <p>astoria reserves the right to change or modify any of the terms and conditions contained in the Terms of Use, Guidelines and Rules from time to time at any time, without notice, and in its sole discretion. If astoria decides to change these Terms of Use, astoria will post a new version on the Sites and update the date set forth above. Any changes or modifications to these Terms of Use, Guidelines or Rules will be effective upon posting of the revisions. Your continued use of the Sites following posting of any changes or modifications constitutes your acceptance of such changes or modifications and if you do not agree with these changes or modifications, you must immediately cease using the Sites. For this reason, you should frequently review these Terms of Use, our Guidelines and Rules and any other applicable policies, including their dates, to understand the terms and conditions that apply to your use of the Sites.</p>
                                <p> </p>
                                <p>
                                    <b>Copyright</b>
                                    <br>
                                    All design, text, graphics, logos, button icons, images, audio and video clips, the selection and arrangement thereof, and all software on the Sites is Copyright (c) 1999-2012 astoria USA, Inc., ALL RIGHTS RESERVED. The compilation (meaning the collection, arrangement and assembly) of all content on the Sites is the exclusive property of astoria and protected by U.S. and international copyright laws. All software used on the Sites is the property of astoria or its software suppliers and is protected by U.S. and international copyright laws. Permission is granted to electronically copy and to print in hard copy portions of the Sites for the sole purpose of placing an order with astoria, using the Interactive Services or using the Sites as a shopping resource. Any other use of materials on the Sites - including reproduction for purposes other than those permitted above, modification, distribution, republishing, transmission, display or performance - without the prior written permission of astoria is strictly prohibited.
                                </p>
                                <p> </p>
                                <p>
                                    <b>Trademarks</b>
                                    <br>
                                    astoria.com, ttbeauty.com and all page headers, custom graphics and button icons are service marks, trademarks, and/or trade dress of astoria and may not be used in connection with any product or service that is not offered by astoria in any manner that is likely to cause confusion among customers, or in any manner that disparages or discredits astoria. All other trademarks, product names and company names or logos cited herein are the property of their respective owners.
                                </p>
                                <p> </p>
                                <p>
                                    <b>Product Information</b>
                                    <br>
                                    The products displayed on the Sites can be ordered and delivered only within the U.S. and Canada. astoria products displayed on the Sites may be available in select astoria retail stores in the U.S. and certain foreign markets. All prices displayed on the Sites are quoted in U.S. Dollars and are valid and effective only in the U.S. If any minor uses any goods or product from astoria it should be only after the legal or parental guardian has discussed the product with the minor's doctor. Reference to any products, services, processes or other information by trade name, trademark, manufacturer, supplier or otherwise does not constitute or imply endorsement, sponsorship or recommendation thereof by astoria.
                                </p>
                                <p> </p>
                                <p>All material and information presented by astoria is intended to be used for personal educational or informational purposes only. The statements made about products have not been evaluated by the U.S. Food and Drug Administration and the results reported, if any, may not necessarily occur in all individuals. The statements and products are not intended to diagnose, treat, cure or prevent any condition or disease. All products should be used strictly in accordance with their instructions, precautions and guidelines. You should always check the ingredients for products to avoid potential allergic reactions. Use of the Sites is not meant to serve as a substitute for professional medical advice: these Sites are solely online stores for specialty beauty products. Please consult with your own physician or health care practitioner regarding the use of any goods, products or information received from the Sites before using or relying on them. Your physician or health care practitioner should address any and all medical questions, concerns and decisions regarding the possible treatment of any medical condition. astoria does not give or intend to give any answers to medical related questions and these Sites do not replace any medical professional or medical resource. astoria does not represent itself as a physician nor is this implied. No prescription medications or medical treatments are intentionally provided on the Sites. IF YOU ARE IN NEED OF MEDICAL ATTENTION, CALL 911 OR YOUR PHYSICIAN IMMEDIATELY.</p>
                                <p> </p>
                                <p>The products available on the Sites and the Interactive Services, including any samples astoria may provide to you, are for your personal use only. You may not sell or resell any products you purchase or otherwise receive from astoria. astoria reserves the right, with or without notice, to cancel or reduce the quantity of any order to be filled or products to be provided to you that may result in a violation of these Terms of Use, as determined by astoria in its sole discretion.</p>
                                <p> </p>
                                <p>
                                    <b>Color Information</b>
                                    <br>
                                    While astoria has tried to accurately display the colors of products, the actual colors you see will depend on your monitor and may not be accurate.
                                </p>
                                <p> </p>
                                <p>
                                    <b>Responsibility for your Content</b>
                                    <br>
                                    You are solely responsible for all content that you upload, post, email or otherwise transmit via or to the Sites, through our Interactive Services or otherwise, including the submission of product ratings and reviews and all other data, profile information, documents, text, software, applications, music, sound, photographs, graphics, video, messages, ratings, forum postings, comments, questions, answers or other materials (collectively, "Content"). We will not accept Content from you unless you are a registered user of the Sites.
                                </p>
                                <p> </p>
                                <p>
                                    <b>Your Use of Content on the Sites and Limitation of Liability</b>
                                    <br>
                                    astoria provides the Sites and the Interactive Services and all other applications and services on the Sites as a forum only. astoria is not liable for any statements, representations, or Content provided by its users in any public forum on the Sites or any Third Party Site, including without limitation through the Interactive Services. Any Content, if displayed, is displayed for entertainment and informational purposes only. More generally, Content posted via or on the Sites or any Third Party Site, including through the Interactive Services, is not controlled by astoria. astoria cannot guarantee the accuracy, integrity or quality of such Content. You understand that by using the Interactive Services, you may be exposed to Content that may be offensive, indecent or objectionable and astoria shall not be liable for any such Content and that the risk of harm or damage from the foregoing rests entirely with you. Under no circumstances will astoria be liable in any way for any Content, including, but not limited to, for (i) any errors or omissions in any Content; or (ii) any loss or damage (including, without limitation, personal injury or property damage) of any kind incurred as a result of the use of any Content posted, emailed or otherwise transmitted via or to the Sites or through the Interactive Services.
                                </p>
                                <p> </p>
                                <p>You may access the Content and any other content on the Sites only as permitted under these Terms of Use and the Privacy Policy and you agree to not engage in the use, copying or distribution of any of the Content other than as expressly provided herein.</p>
                                <p> </p>
                                <p>You agree not to circumvent, disable or otherwise interfere with security-related features of the Site or features that prevent or restrict use of any Content or enforce limitations on use of the Site or the Content therein. You may not interfere with or disrupt the Sites, or servers or networks connected to the Sites, or disobey any requirements, procedures, policies or regulations of networks connected to the Sites, including by using any device, software or routine to bypass robot exclusion headers. astoria reserves all rights not expressly granted in and to the Content. When using the Interactive Services, you may not disrupt the normal flow of dialogue, cause a screen to "scroll" faster than other users are able to type, or otherwise act in a manner that negatively affects other users' ability to engage in real time exchanges.</p>
                                <p> </p>
                                <p>
                                    <b>Your Content Submissions</b>
                                    <br>
                                    By submitting Content to astoria, you represent and warrant that:
                                </p>
                                <div class="indent">
                                    <p>
                                        ♦ You understand you are participating in a public forum and that your Content will be available to all other users of the Sites, the Interactive Services and potentially Third Party Sites;
                                        <br>
                                        ♦ You are the sole author and owner of the intellectual property and other rights thereto (or have the necessary licenses, rights, consents and permissions to use and authorize astoria to use all intellectual property and other rights thereto to enable inclusion and use of the Content in the manner contemplated by the Sites and these Terms of Use);
                                        <br>
                                        ♦ All "moral rights" that you may have in such Content have been voluntarily waived by you and you do not require that any personally identifying information be used in connection with the Content that you submit, or any derivative works of or upgrades or updates thereto;
                                        <br>
                                        ♦ All Content that you post is accurate;
                                        <br>
                                        ♦ You are at least 13 years old and, if you are a minor, that you have obtained the consent of your parent or legal guardian to use the Site and agree to these Terms of Use; and that " use of the Content you supply does not violate these Terms of Use and will not cause injury to any person or entity.
                                    </p>
                                </div>
                                <p> </p>
                                <p>You also represent and warrant that any Content you submit:</p>
                                <div class="indent">
                                    ♦ Is not false, inaccurate or misleading;
                                    <br>
                                    ♦ Does not harm minors;
                                    <br>
                                    ♦ Does not infringe any copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy of any person or entity;
                                    <br>
                                    ♦ Does not violate any obligations you may have with respect to such Content under any law or under contractual or fiduciary relationships (such as, but not limited to, inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements);
                                    <br>
                                    ♦ Does not violate any law, statute, ordinance or regulation (including, but not limited to, those governing export control, consumer protection, unfair competition, anti-discrimination or false advertising);
                                    <br>
                                    ♦ Is not, or would not reasonably be considered to be, unlawful, harmful, defamatory, libelous, vulgar, obscene, invasive of another's privacy, hateful, racially or religiously biased or offensive, abusive, tortious, threatening or harassing to any individual, partnership or corporation;
                                    <br>
                                    ♦ Is not submitted for compensation or other consideration from any third party; " does not include any information that references other websites, addresses, email addresses, contact information or phone numbers;
                                    <br>
                                    ♦ Complies in all respects with these Terms of Use, our Privacy Policy and all Guidelines and Rules;
                                    <br>
                                    ♦ Is not unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation; and
                                    <br>
                                    ♦ Does not contain any computer viruses, worms or other potentially damaging computer programs or files.
                                    <p></p>
                                </div>
                                <p> </p>
                                <p>astoria does not endorse any Content or any opinion, recommendation or advice expressed therein, and astoria disclaims all liability with respect to the Content.</p>
                                <p> </p>
                                <p>If your Content includes ideas, suggestions, documents or proposals to astoria through the Interactive Services, (a) such Content is not confidential or proprietary and astoria has no obligation of confidentiality, express or implied, with respect thereto; (b) astoria may have something similar to that Content already under consideration or development; and (c) you are not entitled to compensation, payment or reimbursement of any kind for such Content from astoria under any circumstances unless you are otherwise notified by astoria in writing.</p>
                                <p> </p>
                                <p>For any Content that you submit, you grant astoria a worldwide, perpetual, irrevocable, royalty-free, sublicenseable and transferable right and license to use, reproduce, communicate, distribute, copy, modify, delete in its entirety, edit, adapt, publish, translate, publicly display, publicly perform, use, create derivative works from and/or sell and/or distribute such Content and/or incorporate such Content into any form, medium or technology whether now or hereafter known throughout the world without compensation to you. This license will survive the termination of these Terms of Use and your use of the Site.</p>
                                <p> </p>
                                <p>
                                    <b>Guidelines for Interactive Services</b>
                                    <br>
                                    We want to know what you think of the products you've tried, bought, know and love and we encourage you to use our Interactive Services. When writing a review for the RR Service, posing a question on the Q&A Service or participating on BeautyTalk, please consider the following guidelines:
                                </p>
                                <div class="indent">
                                    ♦ Focus on the product and your individual experience using it
                                    <br>
                                    ♦ Provide details about why you like or dislike a product; and
                                    <br>
                                    ♦ All submitted reviews, comments and questions are read by our moderators and are subject to these Terms of Use.
                                    <p></p>
                                </div>
                                    <p> </p>
                                    <p>You may not use our Interactive Services to impersonate any person or entity, including, without limitation, any astoria official, forum leader, guide or host, or to falsely state or otherwise misrepresent your affiliation with a person or entity. You may not use the Interactive Services to collect or store personal data about other users.</p>
                                    <p> </p>
                                    <p>As noted above, astoria reserves the right to not post a review or to withdraw a posted review for any reason. Your review will be excluded if it violates Guidelines or the provisions in these Terms of Use regarding submission of Content generally.</p>
                                    <p> </p>
                                    <p>
                                        <b>Third Party Content and Third Party Sites</b>
                                        <br>
                                        astoria may provide content of third parties ("Third Party Content") or links to Third Party Sites as a service to those interested in this information. astoria does not monitor, approve or have any control over any Third Party Content or the Third Party Sites and the inclusion of links to Third Party Content or Third Party Sites does not imply any association or relationship between astoria and such third party. astoria does not guarantee, endorse or adopt the accuracy or completeness of any Third Party Content or any Third Party Site. astoria is not responsible for updating or reviewing Third Party Content or Third Party Sites. You use Third Party Content and Third Party Sites at your own risk. Third Party Content, including comments from third party users submitted to astoria through the Interactive Services, do not necessarily reflect the views of astoria.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Mobile Services</b>
                                        <br>
                                        If you access the Sites via your mobile phone (through an iPhone application, for example), we do not currently charge for this access. Please be aware that your carrier's normal rates and fees, such as text messaging fees or data charges, will still apply.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Modification of Content</b>
                                        <br>
                                        All Content that you submit is not confidential and may be used at astoria's sole discretion. astoria may or may not pre-screen Content. However, astoria and its designees will have the right (but not the obligation) in their sole discretion to pre-screen, change, condense or delete any Content on the Sites. In particular, astoria and its designees will have the right to remove any Content that astoria deems, in its sole discretion, to violate the Guidelines, or any other provision of these Terms of Use or is otherwise objectionable. astoria does not guarantee that you will have any recourse through astoria to edit or delete any Content you have submitted. astoria reserves the right to incorporate any Content you have submitted into any account you may have, now or in the future, as a registered user of the Sites. Ratings, written comments, questions and answers are generally posted within two to four business days. However, astoria reserves the right to remove or to refuse to post any submission for any reason. You acknowledge that you, not astoria, are responsible for the contents of any Content you submit. None of the Content that you submit shall be subject to any obligation of confidence on the part of astoria, its agents, subsidiaries, affiliates, partners or third-party service providers and their respective directors, officers and employees.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Reservation of Rights</b>
                                        <br>
                                        astoria reserves the right, at any time, without notice and in its sole discretion, to terminate your license to use the Interactive Services and the Sites and to block or prevent your future access to and use of the Interactive Services and the Sites. astoria may access, preserve and disclose your account information and Content if required to do so by law or in a good faith belief that such access, preservation or disclosure is reasonably necessary to (i) comply with legal process; (ii) enforce these Terms of Use, (iii) respond to claims that any Content violates the rights of third parties, (iv) respond to your requests for customer service, or (v) protect the rights, property or personal safety of astoria (and its employees), its users and the public.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Disclaimer</b>
                                        <br>
                                        astoria IS PROVIDING THE SITES, THEIR CONTENTS AND THE INTERACTIVE SERVICES ON AN "AS-IS" BASIS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, WITH RESPECT TO THE OPERATION OF THE SITES OR INTERACTIVE SERVICES, THE INFORMATION, CONTENT, MATERIALS OR PRODUCTS, INCLUDED ON THE SITES OR AS PART OF THE INTERACTIVE SERVICES. TO THE FULLEST EXTENT PERMITTED BY LAW, astoria DISCLAIMS ALL SUCH REPRESENTATIONS AND WARRANTIES, INCLUDING FOR EXAMPLE WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. IN ADDITION, astoria DOES NOT REPRESENT OR WARRANT THAT THE INFORMATION ACCESSIBLE VIA THE SITES OR THE INTERACTIVE SERVICES IS ACCURATE, COMPLETE OR CURRENT. Price and availability information is subject to change without notice.
                                    </p>
                                    <p> </p>
                                    <p>astoria WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING OUT OF OR IN CONNECTION WITH THE USE OF THE SITES OR THE INTERACTIVE SERVICES. THIS IS A COMPREHENSIVE LIMITATION OF LIABILITY THAT APPLIES TO ALL DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOSS OF DATA, INCOME OR PROFIT, LOSS OF OR DAMAGE TO PROPERTY AND CLAIMS OF THIRD PARTIES.</p>
                                    <p> </p>
                                    <p>
                                        <b>Indemnification</b>
                                        <br>
                                        You agree to defend, indemnify and hold harmless astoria (and its officers, directors, agents, subsidiaries, joint ventures, employees and third-party service providers), from all claims, demands, losses, liabilities, costs, expenses, obligations and damages of every kind and nature, known and unknown, including reasonable legal fees, arising out of (a) your use of and access to the Sites and the Interactive Services; (b) your violation of any term of these Terms of Use; (c) a breach of your representations and warranties set forth above regarding Content; (d) your violation of any law or the rights of a third party (including, without limitation, any copyright, property or privacy right); or (e) any claim that any Content you submitted caused damage to a third party. This indemnification obligation will survive the termination of these Terms of Use and your use of the Sites and the Interactive Services.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Applicable Law</b>
                                        <br>
                                        The Sites and Interactive Services are created and controlled by astoria in the State of California, U.S.A. As such, the laws of the State of California will govern these Terms of Use, without giving effect to any principles of conflicts of laws. astoria reserves the right to make changes to its Web site and these Terms of Use at any time.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Our Address</b>
                                        <br>
                                        astoria USA, Inc.
                                        <br>
                                        First Market Tower
                                        <br>
                                        525 Market Street, 11th Floor
                                        <br>
                                        San Francisco, CA 94105
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>DMCA</b>
                                        <br>
                                        If you believe your work or content has been copied and posted to the Sites in a way that constitutes copyright infringement, please provide astoria's designated copyright agent the following written information in accordance with the Digital Millennium Copyright Act (the "DMCA"):
                                        <br>
                                        An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest;
                                        <br>
                                        A description of the copyrighted work that you claim has been infringed upon;
                                        <br>
                                        A description of where the material that you claim is infringing is located on the Sites;
                                        <br>
                                        A statement by you that you have a good-faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;
                                        <br>
                                        Your name, address, telephone number and email address (if available); and
                                        <br>
                                        A statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or are authorized to act on the copyright owner's behalf.
                                    </p>
                                    <p> </p>
                                    <p>
                                        astoria's DMCA designated copyright agent for notice of claims of copyright infringement on this Web site is our General Counsel, Eric Baysinger, who can be reached as follows:
                                        <br>
                                        astoria USA, Inc.
                                        <br>
                                        Legal Department
                                        <br>
                                        525 Market Street, 11th Floor
                                        <br>
                                        San Francisco, CA 94105
                                        <br>
                                        E-mail: copyright@astoria.com
                                        <br>
                                        1-877-737-4672
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>Fraud Protection Program</b>
                                        <br>
                                        As part of our order processing procedures, we screen all received orders for fraud or other types of unauthorized or illegal activity. We reserve the right to refuse to process an order due to suspected fraud or unauthorized or illegal activity. If such is the case, we may reject your order or our Customer Service department may call you at the phone number you provided (or use your email address) to confirm your order. We also reserve the right to cancel any accounts or refuse to ship to certain addresses due to suspected fraud or unauthorized or illegal activity. We take these measures to protect our customers as well as ourselves from fraud or other unauthorized or illegal activity.
                                    </p>
                                    <p> </p>
                                    <p>
                                        <b>General</b>
                                        <br>
                                        Any claim or dispute between you and astoria that arises in whole or in part from the Sites or the Interactive Services shall be decided exclusively by a court of competent jurisdiction located in San Francisco County, California. If any provision of these Terms of Use is deemed invalid by a court of competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms of Use, which shall remain in full force and effect. No waiver of any term of these Terms of Use shall be deemed a further or continuing waiver of such term or any other term, and astoria's failure to assert any right or provision under these Terms of Use shall not constitute a waiver of such right or provision. It is the express wish of the parties that these Terms of Use and all related documents be drawn up in English. C'est la volonté expresse des parties que cette convention ainsi que tous les documents qui s'y rattachent soient rédigés en anglais. TO THE EXTENT PERMITTED BY APPLICABLE LAWS, YOU AND astoria AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF OR RELATED TO THE SITES AND THE INTERACTIVE SERVICES MUST COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES. OTHERWISE, SUCH CAUSE OF ACTION IS PERMANENTLY BARRED.
                                    </p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button class="btn btn-primary btn-lg btn-register" id="register_bnt" name="register_bnt" type="submit">register</button>
            </div>
            </div>
            </div>
            </form>
        </div>
    </body>
</html>