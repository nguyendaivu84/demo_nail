<!doctype html>
<!--[if lt IE 9]>
<html class="locale-ca user-unrecognized oldie no-js" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" >
	<![endif]-->
	<!--[if gt IE 8]>
	<!-->
	<html class="locale-ca user-unrecognized no-js" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" >
		<!--<![endif]-->
<head>
		<meta charset="utf-8">
        <base href="[@URL]" />
		<title>[@HEADER_TITLE]</title>
		<meta name="description" content="Explore the largest and most diverse selection of beauty products from astoria. Buy makeup, cosmetics, skincare and other products from beauty brands like Clarisonic, OPI, Guerlain and Clinique. You are bound to find something that is uniquely you.">
		<meta name="viewport" content="width=1024">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
        <link rel="Shortcut Icon" type="image/ico" href="/images/icons/logos.ico" >
		<meta name="format-detection" content="telephone=no">
		<meta name="contextPath" content="">
		<meta name="imageBasePath" content="/images/">
		<meta name="bvctx" content="">
		<meta name="google-site-verification" content="DHEfxRvk5978fvUKaGrOCpRByg5EXyhJBxhEbM0rZKk" />
		<meta name="bi_status" content="NON_BI"/>
		<meta name="sitescope" content="ok"/>
		<link rel="stylesheet" href="[@URL_CUSTOMER]/css/main.css">
		<meta name="msvalidate.01" content="64B516ECEB8C679D13D17E1975734348" />
		<meta property="og:type" content="website">
		<meta property="og:title" content="Shop Makeup, Fragrance, Skincare & More Beauty | astoria">
		<meta property="og:image" content="/user_account/customer/templates/default/img/logo-astoria-square.png">
		<script>
<!--
var gomez={
gs: new Date().getTime(),
acctId:"AC9D98",
pgId:"",
grpId:""
};
//-->
</script>
<script src="[@URL_CUSTOMER]/js/modernizr.js"></script>
<script src="[@URL_CUSTOMER]/js/jquery.js"></script>
<script src="[@URL_CUSTOMER]/js/bpopup.js"></script>
<script src="[@URL]/lib/js/common.js"></script>
<script src="[@URL]/lib/js/jquery.elevatezoom.js"></script>
<script>
    $(document).ready(function(){
        $("#register").on("click",function(){
            $("#popupcontent_register").bPopup({
                contentContainer:'#popupcontent_register',
                loadUrl: '[@URL]register/data'
                ,opacity:0
                ,fadeSpeed: 'slow',
                followSpeed: 1500
                ,escClose :true
            });
        });
        $("#reset_password").on("click",function(){
            $("#popupcontent_reset_password").bPopup({
                contentContainer:'#popupcontent_reset_password',
                loadUrl: '[@URL]resetpassword/data'
                ,opacity:0
                ,fadeSpeed: 'slow',
                followSpeed: 1500
                ,escClose :true
            });
        });
        $("a[rel='click_to_quick_look']").on("click",function(){
            var pname = $(this).attr("data-product-name");
            $("#popup_quick_look").bPopup({
                contentContainer:'#popup_quick_look',
                opacity:0
                ,loadUrl: '[@URL]popupproduct_data/data/'+pname
                ,fadeSpeed: 'slow',
                followSpeed: 1500
                , modalClose: true
                ,escClose :true
            });
            return false;
        });
        $(".sign-out").on("click",function(){
            window.location.href="[@URL]logout";
        });
        $("#sign_in").on("click",function(){
            $("#popupcontent_sign_in").bPopup({
                contentContainer:'#popupcontent_sign_in',
                loadUrl: '[@URL]sign_in/data'
                ,opacity:0
                ,fadeSpeed: 'slow',
                followSpeed: 1500
                ,escClose :true
            });
        });
    });
</script>
</head>
<style>
    .navbar-nav > li.col-4 .drop {
        min-width: 355px !important;
    }
    .navbar .col + .col {
        margin-left: 0px;
    }
</style>
<body id="home" class="home" data-social="true">
    <!-- <div class="my-astoria">
        <form id="myAccountBoxForm" name="myAccount" action="/common/header/?_DARGS=/common/header/myastoria.jsp#" method="post">
            <div class="wrapper container">
                <ul class="nav my-seph-left container indent_left_top">
                    <li>
                        <a href="/store-content/stores">
                            <span class="icon icon-store-ko"></span>
                            [@LANGUAGE_STORES]
                        </a>
                    </li>
                    <li id="astoriaBoxItemsArea">
                        <a href="/checkout">
                            <span class="icon icon-basket"></span>
                            <span id="basket_count" class="item-count">[@BASKET_COUNT]</span>
                            [@LANGUAGE_BASKET]
                        </a>
                    </li>
                </ul>
                <div class="my-seph-right">
                    <ul class="nav container my-seph-left indent_header ul_account">
                        [@HEADER_LOGIN]
                    </ul>
                    <div class="social container my-seph-right">
                        <div class="icon icon-facebook social-enabled" data-socialtype="fb" data-socialurlprefix="">
                            <a href="#" target="_blank"></a>
                        </div>
                        <a href="" class="icon icon-twitter"></a>
                        <a href="" class="icon icon-pinterest"></a>
                    </div>
                </div>
            </div>
        </form>
    </div> -->
<!-- END TOP HEADER -->
    <div class="site-header wrapper">
        <div class="banner"></div>
        <div class="masthead container" style="height:102px;">
<div  class="logo" >
            <div id="logos">                <a href="/">
                 <img src="[@IMAGE_LOGO]" alt="astoria" style="width:100px;" />
                                 </a>
            </div>
            <div id="logoss">
            <a href="/">
                  <img  style="height:100px; " alt="astoria" src="[@IMAGE_NAME]" alt="astoria" />
                </a>
            </div>
               </div> 
                
  <div class="site-search1" >
                    <div class="sign-in-control" >
                                <span class="span-control">
                                        <a href="/store-content/stores">
                                            <span class="icon icon-store"></span>
                                            [@LANGUAGE_STORES]
                                        </a>
                                </span>
                                <span class="span-control">
                                    <a href="/checkout">
                                        <span class="icon icon-basket"></span>
                                        <span id="basket_count" class="item-count">[@BASKET_COUNT]</span>
                                        [@LANGUAGE_BASKET]
                                    </a>
                                </span>
                                [@HEADER_LOGIN]
                                
                    </div>
                    <div style="float:left;text-align: left;padding-left:15px;width:300px;line-height:55px;" class="promo-area">

                            <div class="img" style="cursor: pointer;padding-left:90px;">
                                [@FREE_SHIPPING_TEXT]
                            </div>

                            <div id="modal-shipping-details" style="padding-right:30px;"class="modal modal-small modal-shipping-details"data-ict="promo_module">
                                    <div class="modal-header">
                                        <button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Canada shipping details</h4>
                                    </div>
                                    [@HEADER_FREE_SHIPPING]
                            </div>
                    </div>
                    <div id="search_a">
                                         <form onsubmit="search_string($('#site-search-field').val()); return false;"  class="site-search">
                         <input type="search" id="site-search-field" autocomplete="off" maxlength="70" placeholder="[@SEARCH_PLACEHOLDER]" class="form-control">
                         <button class="btn btn-primary btn-icon btn-search" type="submit">
                             <span class="icon icon-search"></span>
                         </button>

                     </form>
                     </div>
                     
            </div>

            <!-- <span class="site-search" style="float:left;">
                 <table>
                     <tr>
                         <td>
                            <a href="/store-content/stores">
                                <span class="icon icon-store"></span>
                                [@LANGUAGE_STORES]
                            </a>
                        </td>
                        <td>
                              <a href="/checkout">
                                    <span class="icon icon-basket"></span>
                                    <span id="basket_count" class="item-count">[@BASKET_COUNT]</span>
                                    [@LANGUAGE_BASKET]
                                </a>
                        </td>
                     </tr>
                     <tr><td>&nbsp;</td></tr>
                     [@HEADER_LOGIN]
                 </table>

            </span> -->


        </div>
    <!-- END MIDDLE AND SEARCH CONTENT -->
        <div class="navbar" style="margin-top: 20px; font-size: 0.92em;  padding-left: 16px !important;padding-right: 18px !important;">
            <ul class="nav navbar-nav">
                [@CATEGORY_LIST]
                <li class="nav-11 nav-w-drop col-5">
                <div class="drop">
                    <div class="head">
                        <ul class="nav-alpha">
                            [@CAT_START_BY_CHARACTER]
                        </ul>
                    </div>
                    <div class="brand-lists container OneLinkNoTx">
                        [@CAT_START_BY_CHARACTER_ITEM]
                    </div>
                </div>
                <a href="/brand/list.jsp" class="link-top" >Category</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="dv_pu" id="popupcontent_register" style="display: none; background-color: white; min-width: 700px;"></div>
    <div class="dv_pu" id="popupcontent_sign_in" style="display: none; background-color: white; min-width:700px;"></div>
    <div class="dv_pu" id="popupcontent_reset_password" style="display: none; background-color: white; min-width:700px;"></div>
    <div class="dv_pu" id="popup_quick_look" style="display: none; background-color: white; min-width:700px; max-height: 500px"></div>
    <input type="button" id="reset_password" name="reset_password" style="display: none" />
    <div id="notice_popup_add_basket" class="inline-basket">
        <div class="inline-basket-inner">
            <span id="currency" class="hidden" data-currency="C$"></span>
            <h2 class="alt-book">just added to shopping cart</h2>
            <div class="basket-line-items">
                <div id="sku1572627" class="product-row container" data-productid="P384887" data-bitype="None" data-isbi="" data-skutype="Standard" style="display: block;">
                    <div class="product-image">
                        <img width="42" height="42" alt="" id="popup_add_basket_item_image" src="/productimages/sku/s1572627-main-thumb.jpg">
                    </div>
                    <div class="product-description">
                        <h3>
                            <div class="product-price">
                                <span class="list-price">
                                    <span id="popup_add_basket_item_price" class="price ">C$82.00</span>
                                </span>
                            </div>
                            <span class="OneLinkNoTx">
                                <span id="popup_add_basket_item_brand_name" class="brand">UINIQUE COLLECTION</span>
                                <span id="popup_add_basket_item_name" class="product-name">Art At Your Fingertips Nail Kit</span>
                            </span>
                        </h3>
                        <div class="info-row">
                            <span class="sku">
                                <span class="label">Item #</span>
                                <span id="popup_add_basket_item_code" class="value OneLinkNoTx">1572627</span>
                            </span>
                            <span class="qty">
                                <span class="label">QTY</span>
                                <span id="popup_add_basket_item_quantity" class="value">2</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="actions container">
                <a href="/" class="btn btn-link btn-continue-shopping">
                    <span class="arrow arrow-left arrow-mini"></span>
                    Continue Shopping
                </a>
                <a href="/checkout" class="btn btn-primary btn-my-list hide">
                    Shopping List
                    <span class="arrow arrow-right arrow-mini"></span>
                </a>
                <a href="/checkout" class="btn btn-primary btn-checkout">
                    Shopping cart
                    <span class="arrow arrow-right arrow-mini"></span>
                </a>
            </div>
        </div>
        <div class="inline-basket-msg hide">
            <h4></h4>
            <p></p>
        </div>
        <span class="icon icon-close" onclick="close_popup('notice_popup_add_basket');"></span>
    </div>
   
    <div id="main_wrap" class="main-wrap wrapper">