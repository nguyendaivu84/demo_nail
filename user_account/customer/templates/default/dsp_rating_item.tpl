<div id="BVRRDisplayContentReviewID_34361887" class="BVRRContentReview BVRRDisplayContentReview BVDIContentNative BVRRContentReviewNative BVRRDisplayContentReviewOdd BVRRDisplayContentReviewFirst BVRROdd BVRRFirst">
    <div class="BVRRSeparator BVRRSeparatorContentBodyTop"></div>
    <div id="BVSubmissionPopupContainer" class="BVRRReviewDisplayStyle3">
        <div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerTop"></div>
        <div class="BVRRReviewDisplayStyle3Summary">
            <div class="BVRRUserNicknameContainer">
                <span class="BVRRLabel BVRRUserNicknamePrefix"></span>
													<span class="BVRRValue BVRRUserNickname">
														<a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_2499762026" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/2499762026/profile.htm">
                                                            <span class="BVRRNickname">BeautyWhispers</span>
                                                        </a>
													</span>
                <span class="BVRRLabel BVRRUserNicknameSuffix"></span>
                <div class="BVRRUserNicknameReadReviewsContainer">
                    <a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_2499762026" class="BVRRUserProfileImageLink" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/2499762026/profile.htm">
                        <img src="http://reviews.astoria.com/static/8723illuminate/more_reviews_icon.gif" alt="(read all my reviews)"></a>
                    <a data-bvtrack="eName:ProfileLink" name="BV_TrackingTag_Review_Display_ReadAllReviews_2499762026" onclick="var popupWin = parent.window.open(this.href, null, 'resizable=1,status=1,scrollbars=1,width=800,height=600,top=0'); popupWin.focus(); event.preventDefault?event.preventDefault():event.returnValue = false;" href="http://reviews.astoria.com/8723illuminate/2499762026/profile.htm">(read all my reviews)</a>
                </div>
            </div>
            <div class="BVRRBadges BVRRReviewBadges">
                <div class="BVRRBadgeGraphic BVRRReviewBadgeGraphic BVRRYesvibrGraphic">
                    <div class="BVRRBadgeLabel BVRRReviewBadgeLabel BVRRYesvibrLabel"></div>
                </div>
                <div class="BVRRSpacer BVRRBadgeSpacer BVRRReviewBadgeSpacer"></div>
            </div>
            <div class="BVRRUserLocationContainer">
                <span class="BVRRLabel BVRRUserLocationPrefix">Location:</span>
                <span class="BVRRValue BVRRUserLocation">Sacramento, CA</span>
                <span class="BVRRLabel BVRRUserLocationSuffix"></span>
            </div>
            <div class="BVRRContextDataContainer">
                <div class="BVRRContextDataValueContainer BVRRContextDataValueskinToneContainer BVContextDataSkinToneMedium">
                    <span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueskinTonePrefix">Skin Tone:</span>
                    <span class="BVRRValue BVRRContextDataValue BVRRContextDataValueskinTone">Medium</span>
                    <span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueskinToneSuffix"></span>
                </div>
                <div class="BVRRContextDataValueContainer BVRRContextDataValueeyeColorContainer BVContextDataEyeColorHazel">
                    <span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueeyeColorPrefix">Eye Color:</span>
                    <span class="BVRRValue BVRRContextDataValue BVRRContextDataValueeyeColor">Hazel</span>
                    <span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueeyeColorSuffix"></span>
                </div>
                <div class="BVRRContextDataValueContainer BVRRContextDataValueageContainer BVContextDataAge45to54">
                    <span class="BVRRLabel BVRRContextDataValuePrefix BVRRContextDataValueagePrefix">Age:</span>
                    <span class="BVRRValue BVRRContextDataValue BVRRContextDataValueage">45-54</span>
                    <span class="BVRRLabel BVRRContextDataValueSuffix BVRRContextDataValueageSuffix"></span>
                </div>
            </div>
        </div>
        <div class="BVRRReviewDisplayStyle3Main">
            <div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerTop"></div>
            <div class="BVRRCustomMainReviewMetaWrapper">
                <div class="BVRRReviewRatingsContainer">
                    <div class="BVRROverallRatingContainer">
                        <div class="BVRRRatingContainerStar">
                            <div class="BVRRRatingEntry BVRROdd">
                                <div id="BVRRRatingOverall_Review_Display" class="BVRRRating BVRRRatingNormal BVRRRatingOverall">
                                    <div class="BVRRLabel BVRRRatingNormalLabel"></div>
                                    <div class="BVRRRatingNormalImage">
                                        <img src="http://reviews.astoria.com/8723illuminate/5_0/5/rating.gif" class="BVImgOrSprite" alt="5 out of 5" title="5 out of 5"></div>
                                    <div class="BVRRRatingNormalOutOf">
                                        <span class="BVRRNumber BVRRRatingNumber">5</span>
                                        <span class="BVRRSeparatorText">out of</span>
                                        <span class="BVRRNumber BVRRRatingRangeNumber">5</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="BVRRReviewTitleContainer">
                    <span class="BVRRLabel BVRRReviewTitlePrefix"></span>
                    <span class="BVRRValue BVRRReviewTitle">WONDERFUL Set! Must Buy!</span>
                    <span class="BVRRLabel BVRRReviewTitleSuffix"></span>
                </div>
                <div class="BVRRReviewDateContainer">
                    <span class="BVRRLabel BVRRReviewDatePrefix">-</span>
                    <span class="BVRRValue BVRRReviewDate">02.10.14</span>
                    <span class="BVRRLabel BVRRReviewDateSuffix"></span>
                </div>
            </div>
            <div class="BVRRReviewDisplayStyle3Content">
                <div class="BVRRReviewTextContainer">
                    <div class="BVRRReviewTextParagraph BVRRReviewTextFirstParagraph BVRRReviewTextLastParagraph">
															<span class="BVRRReviewText">
																I happened to be browsing in astoria a few weeks ago and came across this beauty! I had a gift card to spend from the holidays. I began to swatch the colors in this kit and they were truly adorable! Highly pigmented, blended well and the combo of peach/coral with purple was stunning! I've been having so much fun wearing these products even though it is still winter. I love these kits by Bare Minerals because it is a GREAT way to try some of their products. All these sizes are small, a bit bigger than a sample size - but I have been wearing these for a while and have not made a dent into them! If you are a fan of the corals or purples - you will want to try this kit! A must have for any Bare Minerals fan or one desiring to try Bare Minerals!
															</span>
                    </div>
                </div>
                <div class="RRBeforeFeedbackContainerSpacer"></div>
                <div class="BVDI_FV">
                    <div class="BVDI_FVVoting BVDI_FVVotingHelpfulness">
                        <div class="BVDI_FVSum BVDI_FVSumHelpfulness">
																<span class="BVDI_FVZero BVDI_FVLevel1">
																	<span class="BVDIValue BVDINumber">0</span>
																	<span class="BVDISuffix">points</span>
																</span>
                        </div>
                        <div class="BVDI_FVCounts BVDI_FVCountsHelpfulness">
																<span class="BVDI_FVPositive BVDI_FVLevel1">
																	<span class="BVDIValue BVDINumber">0</span>
																	<span class="BVDISuffix">out of</span>
																</span>
																<span class="BVDI_FVTotal BVDI_FVLevel1">
																	<span class="BVDIValue BVDINumber">0</span>
																	<span class="BVDISuffix">found this review helpful.</span>
																</span>
                        </div>
                        <div class="BVDI_FVVotes BVDI_FVVotesHelpfulness">
                            <span class="BVDIPrefix">Was this review helpful?</span>
																<span class="BVDI_FVVote BVDI_FVPositive BVDI_FVLevel1">
																	<a title="Yes" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddPositiveFeedback_34361887" data-bvjsref="http://reviews.astoria.com/8723illuminate/P384728/review/34361887/positive.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
                                                                        <span class="BVDILinkSpan">Yes</span>
                                                                    </a>
																</span>
																<span class="BVDI_FVVote BVDI_FVNegative BVDI_FVLevel1">
																	<a title="No" data-bvcfg="" name="BV_TrackingTag_Review_Display_AddNegativeFeedback_34361887" data-bvjsref="http://reviews.astoria.com/8723illuminate/P384728/review/34361887/negative.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink BVDIValue" href="javascript://">
                                                                        <span class="BVDILinkSpan">No</span>
                                                                    </a>
																</span>
                        </div>
                    </div>
                    <div class="BVDI_FVReportLink BVDI_FVReportLinkInappropriate">
                        <a title="Report" data-bvcfg="" name="BV_TrackingTag_Review_Display_Inappropriate_34361887" data-bvjsref="http://reviews.astoria.com/8723illuminate/P384728/review/34361887/inappropriate.djs?format=embeddedhtml&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Fbeauty-in-bloom-P384728%3Ficid2%3Dcarousel_hp_020514_ValueSets_CAN_carousel_P384728_image&amp;innerreturn=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2Freview.jsp" class="BVDILink " href="javascript://">
                            <span class="BVDILinkSpan">Report inappropriate content</span>
                        </a>
                    </div>
                </div>
                <div class="RRBeforeSocialLinksContainerSpacer"></div>
                <div class="BVRRReviewSocialLinksContainer">
                    <div class="BVRRLabel BVRRReviewBookmarkingLabel">Share with friends</div>
                    <a name="BV_TrackingTag_Review_Display_SocialBookmarkFacebook_34361887" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkFacebook" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=626,height=436,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Facebook&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3FreviewID%3D34361887&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs1539543-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
                        <img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-facebook.gif" alt="Facebook" title="Add to Facebook"></a>
                    <a name="BV_TrackingTag_Review_Display_SocialBookmarkTwitter_34361887" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkTwitter" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=795,height=700,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Twitter&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3FreviewID%3D34361887&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs1539543-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
                        <img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-twitter.gif" alt="Twitter" title="Tweet this"></a>
                    <a name="BV_TrackingTag_Review_Display_SocialBookmarkPinterest_34361887" target="_blank" class="BVRRSocialBookmarkingSharingLink BVRRSocialBookmarkingSharingLinkPinterest" onclick="this.href=bvReplaceTokensInSocialURL(this.href);window.open(this.href,'','left=0,top=0,width=700,height=600,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" onfocus="this.href=bvReplaceTokensInSocialURL(this.href);" rel="nofollow" href="http://reviews.astoria.com/8723illuminate/share.htm?site=Pinterest&amp;url=http%3A%2F%2Freviews.astoria.com%2F8723illuminate%2FP384728%2Freviews.htm%3FreviewID%3D34361887&amp;title=__TITLE__&amp;robot=__ROBOT__&amp;image=http%3A%2F%2Fwww.astoria.com%2Fproductimages%2Fsku%2Fs1539543-main-grid.jpg" onmouseover="this.href=bvReplaceTokensInSocialURL(this.href);">
                        <img width="16" height="16" class="BVRRSocialBookmarkLinkImage" src="http://reviews.astoria.com/static/8723illuminate/link-pinterest.gif" alt="Pinterest" title="Pin It!"></a>
                </div>
                <div class="RRCustomBeforeRecommendContainerSpacer"></div>
                <div class="RRBeforeClientResponseContainerSpacer"></div>
            </div>
            <div class="BVRRReviewDisplayStyle3MainSpacer BVRRReviewDisplayStyle3MainSpacerBottom"></div>
        </div>
        <div class="BVRRReviewDisplayStyle3Spacer BVRRReviewDisplayStyle3SpacerBottom"></div>
    </div>
    <div class="BVRRSeparator BVRRSeparatorContentBodyBottom"></div>
</div>