<li data-slide-index="1">
    <div class="product-item" data-index="0">
        <div class="product-image">
            <a data-productid="P384728" href="/[@PRODUCT_SLUGGER]">
                <img width="135" height="135" src="[@PRODUCT_IMAGE]" style="display: inline;">
            </a>
            <a id="product-P384728" data-product-name="[@PRODUCT_SLUGGER]" class="qlook" rel="click_to_quick_look" href="javascript:void(0);" name="sku-1539543">QUICK LOOK</a>
        </div>
        <div class="new-love">
            <span class="flag-new">NEW</span>
            <a class="icon icon-love hover-enable" data-sku_number="1539543" data-product_id="P384728" title="" href="#" data-original-title="LOVE IT"></a>
        </div>
        <a data-productid="P384728" href="/[@PRODUCT_SLUGGER]">
                                   <span class="product-info">
                                       <span class="name OneLinkNoTx">
                                            <span class="brand"> [@BRAND_NAME]</span>
                                            [@PRODUCT_NAME]
                                       </span>
                                       <span class="product-price">
                                           <span class="list-price">
                                                <span class="currency">C$</span>
                                                <span class="price">[@PRODUCT_SELL_PRICE]</span>
                                           </span>
                                           <span class="value-price">
                                            (
                                                <span class="currency">C$</span>
                                                <span class="price">[@PRODUCT_COST_PRICE]</span>
                                            value)
                                           </span>
                                           <span class="flags"> limited edition </span>
                                       </span>
                                       <span class="stars">
                                            <span style="width: 49.5px;"></span>
                                       </span>
                                   </span>
        </a>
    </div>
</li>