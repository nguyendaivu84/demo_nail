<div class="clear-all-filters top " style="display: block;"><!-- link-group sidenav mwts -->
    <h3 class="refinement-title">FILTER BY:</h3>
    <input type="submit" value="clear all filters" class="btn btn-default"></div>
<div id="refinements" class="refinements-list">
    [@FILTER_CONTENT]
    <div class="refinements slider-bar">
        <a href="#" class="clear-link hide" style="display: none;">clear filter</a>
        <h4 class="refinement-subhead">price range</h4>

        <div class="slider-output">
            <input type="text" class="form-control slider-amount" readonly="readonly"></div>
        <div id="price range" class="slider-range refinement ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false">
            <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
            <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
            <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"></a>
        </div>
        <div class="orig-range container">
            <div class="price-min">
                <span>C$100</span>
            </div>
            <div class="price-max">
                <span>C$155</span>
            </div>
        </div>
    </div>
</div>
<div class="clear-all-filters" style="display: block;">
    <input type="submit" value="clear all filters" class="btn btn-default">
</div>