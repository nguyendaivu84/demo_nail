<link rel="stylesheet" href="[@URL_DEFAULT]/css/main.css">
<link rel="stylesheet" href="[@URL_DEFAULT]/css/product-detail.css">
<link rel="stylesheet" href="[@URL_DEFAULT]/css/bazaarvoice.css">
<link rel="stylesheet" href="[@URL_DEFAULT]/css/bazaarvoiceQA.css">
<style>
    #popup_view_larger {
        border: 2px solid #a1a1a1;
        background: #dddddd;
        width: 300px;
        border-radius: 9.5px;
        box-shadow: 3px 2px 2px 3px #888888;
    }
</style>
<script>
    function product_quantity_keyup(){
        //product_quantity
        var v_hidden_price = $("#txt_hidden_price").val();
        var quan = $("#product_quantity").val();
        if(quan > 2147483647){
            quan = 1;
            $("#product_quantity").val(quan);
        }else if(quan <= 0){
            alert("Quantity must greater than 0");
            quan = 1;
            $("#product_quantity").val(quan);
        }
        var unit = $("#txt_unit_price").val();
        unit = parseFloat(unit);
        unit = unit.toFixed(2);
        var total = quan * unit;
        //alert(v_hidden_price);
        if(v_hidden_price==0) document.getElementById("spn_unit_price").innerHTML  = "$ "+unit;//$("#spn_unit_price").text("$ "+unit);
        if(v_hidden_price==0) document.getElementById("total_price_span").innerHTML  = total.toMoney();//$("#total_price_span").text(formatDollar(total));
        if(v_hidden_price==0) $("#total_price").val(total);
    }
</script>
<div id="fb-root"></div>
<ul class="breadcrumb">
	<li><a href="/[@PARENT_SLUGGER]/" class="font-dosis-normal">[@CATEGORY_PARENT_NAME]</a></li>
	<!-- <li><a href="/[@HEADLINE_SLUGGER]/">[@CATEGORY_HEADLINE_NAME]</a></li> -->
	<li class="current"><a href="/[@CHILD_SLUGGER]/" class="font-dosis-normal">[@CATEGORY_CHILD_NAME]</a></li>
</ul>
<div id="product-content" class="pdp container" data-brand="bareMinerals">
	<div id="main" class="maincontent">
	<!--<div class="brand-banner">
		</div>-->
		<div class="product-information container">
			<form id="productAction_FormId" name="standardProductPage" action="" method="post">
				<div class="product-detail container">
					<div id="primarySkuInfoArea" class="container">
						<div class="product-description container">
							<div class="purchase-info" id="primarySkuInfo_price">
								<div class="container">
								 <!-- <div class="product-quantity" style="margin-top:10px;">
										<label class="inline" for="1539543">QTY</label>
                                        <input id="product_quantity" onkeypress="return keyNumber(event);" onkeyup="product_quantity_keyup();" onchange="product_quantity_keyup();" class="form-control OneLinkNoTx quantitySelector " value = 1 type="text" maxlength="3" style="width:30px;" />
										<!--select id="product_quantity" class="form-control OneLinkNoTx quantitySelector "  >
											<option value="1" selected="selected" >1</option>
											<option value="2" >2</option>
											<option value="3" >3</option>
											<option value="4" >4</option>
											<option value="5" >5</option>
											<option value="6" >6</option>
											<option value="7" >7</option>
											<option value="8" >8</option>
											<option value="9" >9</option>
											<option value="10" >10</option>
										</select-->
                                    <!--  </div>
                                    -->
									<span class="sku-price" style=" font-family: fontsDosis">
										<span class="list-price">
											<span class="font-dosis-normal"  style="font-size: 15px;">C</span>
											<span class="font-dosis-normal"  style="font-size: 15px;">[@PRODUCT_PRICE]</span>
										</span>
										<span class="value-price" style=" font-family: fontsDosis">
											(
											<span class="currency" class="font-dosis-normal">C</span>
											<span class="price" class="font-dosis-normal"> [@PRODUCT_PRICE_DISCOUNT]</span>
											value)
										</span>
									</span>
								</div>
								<div class="sku-messaging">
									<p class="msg-free-ship sku-free-shipping hide">free shipping</p>
									<p class="msg-free-ship sku-free-flash-shipping hide">free 2-day shipping</p>
									<p class="msg-free-ship sku-free-flash-hazmat-shipping hide">free ground shipping</p>
									<p class="msg-free-ship sku-free-rouge-shipping hide">rouge free shipping</p>
									<p class="msg-free-ship sku-free-rouge-hazmat-shipping hide">rouge free ground shipping</p>
									<div class="out-of-stock" style="display: none;"> <b>not in stock</b>
										<span>:</span>
										<a href="#" data-sku_number="1539543" class="btn-email-when-in-stock"></a>
									</div>
								</div>
							</div>
							<h1 class="OneLinkNoTx">
								<span class="brand-name text-upper">[@BRAND_NAME]</span>
								<br/>
								[@PRODUCT_NAME]
							</h1>
                            <h1>
                                [@PRODUCT_SKU]
                            </h1>
							<div class="info-row" style=" font-family: fontsDosis; color:#fff" >
								<span class="sku" style=" font-family: fontsDosis">
									<span class="label" style=" font-family: fontsDosis; color:#fff">Item #</span>
									<span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
								</span>
								<span class="size" style="display:none">
									<span class="label">Size</span>
									<span class="value"></span>
								</span>
							</div>
						</div>
					</div>
				</div>
			</form>
	<!-- <div class="product-actions">
				<p class="bi-exclusive " style="display:none;">
					<span class="OneLinkNoTx">none</span>
					exclusive
				</p>
				<p class="msg-restricted-country text-warning" style="display: none;">
					Not available in
			                Canada
				</p>
				<p class="qualify" style="display: none;">
					You must be a <b class="OneLinkNoTx bi-level ">none</b>
					to qualify for this product.
					<br>
					<span class="sign-in"  style="display: none;">
						<a href="#" > <b>sign in</b>
						</a>
						or
					</span>
					<span class="learn-rouge"  style="display: none;">
						<a href="/profile/beautyInsider/?tab=rouge">
							<b>learn more</b>
						</a>
					</span>
					<span class="learn-vib"  style="display: none;">
						<a href="/profile/beautyInsider/?tab=vib">
							<b>learn more</b>
						</a>
					</span>
					<span class="sign-up"  style="display: none;">
						<a href="#" class="js-register">
							<b>sign up</b>
						</a>
					</span>
				</p>
				<form id="addToBasketForm" name="addToBasketForm" action="/" method="post">
					<input id="addToBasketBtn" style="display: none;" name="/atg/commerce/order/purchase/CartModifierFormHandler.addItemToOrder" type="submit">
					<button onclick="add_to_basket('[@PRODUCT_ID]',$('#product_quantity').val(),'[@PRODUCT_URL]',0);" class="btn btn-alt btn-lg btn-block btn-add-to-basket"  name="add-to-basket" type="button" >[@ADD_TO_BASKET]</button>
				</form>
			</div>-->

			<form id="addToBasketForm" name="addToBasketForm" action="/" method="post">
				<input id="addToBasketBtn" style="display: none;" name="/atg/commerce/order/purchase/CartModifierFormHandler.addItemToOrder" type="submit">
                <input id="product_quantity" onkeypress="return keyNumber(event);" onkeyup="product_quantity_keyup();" onchange="product_quantity_keyup();" class="form-control OneLinkNoTx quantitySelector " value = 1 type="text" maxlength="3" style="width:30px; float:right; font-size: 15px;" />
				<label class="inline" for="1539543" style=" font-family: fontsDosis; float: right; font-size: 15px; font-weight:500;text-align:center;">QTY</label>
				<span style="float:right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<button style=" float: right;width:25% ;display:inline;" onclick="add_to_basket('[@PRODUCT_ID]',$('#product_quantity').val(),'[@PRODUCT_URL]',0);" class="btn btn-alt btn-lg btn-block btn-add-to-basket"  name="add-to-basket" type="button" >[@ADD_TO_BASKET]</button>
				
				<!--select id="product_quantity" class="form-control OneLinkNoTx quantitySelector "  >
					<option value="1" selected="selected" >1</option>
					<option value="2" >2</option>
					<option value="3" >3</option>
					<option value="4" >4</option>
					<option value="5" >5</option>
					<option value="6" >6</option>
					<option value="7" >7</option>
					<option value="8" >8</option>
					<option value="9" >9</option>
					<option value="10" >10</option>
				</select-->
             
			</form>
			 
		</div>
		
		<!--<ul class="nav nav-tabs product-tabs">
			<li class="active">
				<a href="#product-description" data-toggle="tab">[@DESCRIPTION]</a>
			</li>
		</ul>  -->
	<!--	<div class="tab-content product-tab-content">
		 	<div class="tab-pane fade in active container" id="product-description">
	 			<div class="pane-left">
                    [@PRODUCT_DES]
				</div>

			<div class="pane-right"></div>
			</div>
		<div class="tab-pane fade container" id="product-ingredients">
			<div class="pane-left">
				<h3 id="ingredients"></h3>
                [@PRODUCT_ING]
			</div>
		</div>
		<div class="tab-pane fade container" id="product-shipping">
			<div class="pane-left shipping-info">
				<p id="shippingInfo_hazmat" style="display:none;">
					Certain items considered Hazardous Materials (HAZMAT) including aerosols and alcohol-based products (e.g., pressurized spray cans, hairspray, nail polish remover, nail polish etc.) are required by the U.S. Department of Transportation to be shipped by Ground-only transport, due to air transport restrictions and regulations. For these shipments, you will need to choose UPS Ground, or USPS Ground in Checkout, for shipments in the continental United States. For P.O. Box addresses, USPS Ground is the only shipping method available. Regrettably, these items cannot be shipped to either Alaska or Hawaii.
				</p>
				<p id="shippingInfo_isGiftWrappable" style="display:none;">This product cannot be gift wrapped.</p>
				<p class="sku-shippinginfo" style="display:none;"></p>
				<p>
					Get more information about our
					<a href="#" rel="#pop-shipping" class="pop-info">shipping rates, schedules, methods, and restrictions.</a>
					.
				</p>
				<div id="pop-shipping" class="modal pop-shipping">
					<div class="modal-header">
						<button type="button" class="icon icon-close" data-dismiss="modal" style="display: inline" aria-hidden="true"></button>
						<h4 class="modal-title">Shipping &amp; Handling Information</h4>
					</div>
					<div class="modal-body">
						<div class="modal-scroll custom-scroll">
							<div class="html-component">
								<style>
									table.with-borders td,  table.with-borders th{
									  border: 1px solid #8A8A8A;
									  padding: 7px;
									  vertical-align: middle;
									}
									table.with-borders th{
									  background: none repeat scroll 0 0 #D5D5D5;
									  border-bottom: 0 none;
									  font-weight: bold;
									}
									table td, table th {
									text-align: center;
									}
									.shippingInfo th{
									width: 150px;
									}
									.shippingInfo {
									text-align: center;
									}
								</style>
							</div>
                            <div class="html-component">
                                <div class="indent">
                                    <h3>Shipping Information (Canada)</h3>
                                    Orders must be placed by 12pm EST to start processing on the same day. If an order is placed after the deadline, it will be processed the next business day.
                                    <br>
                                    <br>
                                    <table class="with-borders shippingInfo">
                                        <tr>
                                            <th>
                                                Shipping Method
                                                <br></th>
                                            <th>Costs</th>
                                            <th>
                                                Total Delivery Time
                                                <br>
                                                (including processing
                                                <br>time)</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Standard Shipping
                                                <br>
                                                (Orders C$75 and over)
                                                <br></td>
                                            <td align="center">FREE</td>
                                            <td align="center">5 &ndash; 10 business days</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Standard Shipping
                                                <br>
                                                (Orders under C$75)
                                                <br></td>
                                            <td align="center">C$7.95</td>
                                            <td>5 &ndash; 10 business days</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Express Shipping
                                                <br></td>
                                            <td align="center">C$11.95</td>
                                            <td>3 &ndash; 8 business days</td>
                                        </tr>
                                    </table>
                                </div>
                                <br>
                                <hr class="solid7 indent">
                                <div class="indent">
                                    <h3>Free Shipping</h3>
                                    astoria offers <b>FREE</b>
                                    Standard Shipping on all merchandise orders that are C$75 and over (excluding taxes)-- no promotion code needed.
                                </div>
                                <h3 class="indent">Business Days</h3>
                                <div class="indent">
                                    All orders ship Monday - Friday, excluding federal holidays within Canada and the United States.
                                </div>

                                <div id="Hazmat">&nbsp;</div>
                                <div class="indent">
                                    <h3>Restricted Hazardous Items</h3>
                                    Certain items considered hazardous materials including aerosols and alcohol-based products (e.g., pressurized spray cans, hairspray, nail polish remover, etc.) are restricted and required to be shipped by standard ground-only transport, due to air transport restrictions and regulations.
                                </div>
                                <br>
                                <hr class="solid7 indent">
                                <br>
                                <div class="indent">
                                    For more information or questions, please
                                    <a href="mailto:customerservice@astoria.com?icid2=CS_LandingPage_CustomerService_IMAGE"> <u>email</u>
                                    </a>
                                    or call <b>1- 877-astoria (1-877-737-4672).
                                        <br>
                                        <br>
                                        Monday â€“ Friday: 6am â€“ 9pm PST, Saturday and Sunday: 8am â€“ 5pm PST
                                    </div></b>
                                <br></div>
	</div>
	</div>
	</div>
	<div class="modal modal-small" id="modal-hazmat">
	<div class="modal-header">
	<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title">Whatâ€™s Hazmat?</h4>
	</div>
	<div class="modal-body">
	<p>
	Items considered Hazardous Materials (HAZMAT) including aerosols and alcohol-based products (e.g., pressurized spray cans, hairspray, nail polish remover, nail polish etc.) are required by the U.S. Department of Transportation to be shipped by Ground-only transport, due to air transport restrictions and regulations. For these shipments, we recommend UPS Ground. USPS Ground is also an option for shipments in the continental United States. For P.O. Box addresses, USPS Ground (Parcel Post) is the only shipping method available. Regrettably, these items cannot be shipped to Alaska, Hawaii, or Puerto Rico.
	</p>
	</div>
	</div>
	</div>
	</div>
	</div>-->
	[@PRODUCT_YMAL]
</div>

	[@PRODUCT_SIDEBAR]
	<div class="hidden">
		<span class="bi-product-name"></span>
		<span class="recommendation-copy"> <b>What it is:</b>
			<br>
			A seven-piece collection off fresh-faced pinks and plums for the eyes, lips, and complexion.
			<br>
			<br> <b>What it does:</b>
			<br>
			Create a radiant, ethereal look just in time for spring. Mix, match, and layer these loose and solid mineral colors for a soft, fresh-faced effect.
		</span>
	</div>
</div>
<ul class="nav nav-tabs product-tabs">
			<li class="active">
				<a href="#product-description" data-toggle="tab">[@DESCRIPTION]</a>
			</li>
</ul>
<div class="tab-content product-tab-content">
		 	<div class="tab-pane fade in active container" id="product-description">
	 			<div class="pane-left" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
                    [@PRODUCT_DES]
				</div>

			<div class="pane-right"></div>
			</div>
		<div class="tab-pane fade container" id="product-ingredients">
			<div class="pane-left">
				<h3 id="ingredients"></h3>
                [@PRODUCT_ING]
			</div>
		</div>
		<div class="tab-pane fade container" id="product-shipping">
			<div class="pane-left shipping-info">
				<p id="shippingInfo_hazmat" style="display:none;">
					Certain items considered Hazardous Materials (HAZMAT) including aerosols and alcohol-based products (e.g., pressurized spray cans, hairspray, nail polish remover, nail polish etc.) are required by the U.S. Department of Transportation to be shipped by Ground-only transport, due to air transport restrictions and regulations. For these shipments, you will need to choose UPS Ground, or USPS Ground in Checkout, for shipments in the continental United States. For P.O. Box addresses, USPS Ground is the only shipping method available. Regrettably, these items cannot be shipped to either Alaska or Hawaii.
				</p>
				<p id="shippingInfo_isGiftWrappable" style="display:none;">This product cannot be gift wrapped.</p>
				<p class="sku-shippinginfo" style="display:none;"></p>
				<p>
					Get more information about our
					<a href="#" rel="#pop-shipping" class="pop-info">shipping rates, schedules, methods, and restrictions.</a>
					.
				</p>
				<div id="pop-shipping" class="modal pop-shipping">
					<div class="modal-header">
						<button type="button" class="icon icon-close" data-dismiss="modal" style="display: inline" aria-hidden="true"></button>
						<h4 class="modal-title">Shipping &amp; Handling Information</h4>
					</div>
					<div class="modal-body">
						<div class="modal-scroll custom-scroll">
							<div class="html-component">
								<style>
									table.with-borders td,  table.with-borders th{
									  border: 1px solid #8A8A8A;
									  padding: 7px;
									  vertical-align: middle;
									}
									table.with-borders th{
									  background: none repeat scroll 0 0 #D5D5D5;
									  border-bottom: 0 none;
									  font-weight: bold;
									}
									table td, table th {
									text-align: center;
									}
									.shippingInfo th{
									width: 150px;
									}
									.shippingInfo {
									text-align: center;
									}
								</style>
							</div>
                            <div class="html-component">
                                <div class="indent">
                                    <h3>Shipping Information (Canada)</h3>
                                    Orders must be placed by 12pm EST to start processing on the same day. If an order is placed after the deadline, it will be processed the next business day.
                                    <br>
                                    <br>
                                    <table class="with-borders shippingInfo">
                                        <tr>
                                            <th>
                                                Shipping Method
                                                <br></th>
                                            <th>Costs</th>
                                            <th>
                                                Total Delivery Time
                                                <br>
                                                (including processing
                                                <br>time)</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Standard Shipping
                                                <br>
                                                (Orders C$75 and over)
                                                <br></td>
                                            <td align="center">FREE</td>
                                            <td align="center">5 &ndash; 10 business days</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Standard Shipping
                                                <br>
                                                (Orders under C$75)
                                                <br></td>
                                            <td align="center">C$7.95</td>
                                            <td>5 &ndash; 10 business days</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Express Shipping
                                                <br></td>
                                            <td align="center">C$11.95</td>
                                            <td>3 &ndash; 8 business days</td>
                                        </tr>
                                    </table>
                                </div>
                                <br>
                                <hr class="solid7 indent">
                                <div class="indent">
                                    <h3>Free Shipping</h3>
                                    astoria offers <b>FREE</b>
                                    Standard Shipping on all merchandise orders that are C$75 and over (excluding taxes)-- no promotion code needed.
                                </div>
                                <h3 class="indent">Business Days</h3>
                                <div class="indent">
                                    All orders ship Monday - Friday, excluding federal holidays within Canada and the United States.
                                </div>

                                <div id="Hazmat">&nbsp;</div>
                                <div class="indent">
                                    <h3>Restricted Hazardous Items</h3>
                                    Certain items considered hazardous materials including aerosols and alcohol-based products (e.g., pressurized spray cans, hairspray, nail polish remover, etc.) are restricted and required to be shipped by standard ground-only transport, due to air transport restrictions and regulations.
                                </div>
                                <br>
                                <hr class="solid7 indent">
                                <br>
                                <div class="indent">
                                    For more information or questions, please
                                    <a href="mailto:customerservice@astoria.com?icid2=CS_LandingPage_CustomerService_IMAGE"> <u>email</u>
                                    </a>
                                    or call <b>1- 877-astoria (1-877-737-4672).
                                        <br>
                                        <br>
                                        Monday â€“ Friday: 6am â€“ 9pm PST, Saturday and Sunday: 8am â€“ 5pm PST
                                    </div></b>
                                <br></div>
	</div>
	</div>
	</div>
	<div class="modal modal-small" id="modal-hazmat">
	<div class="modal-header">
	<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title">Whatâ€™s Hazmat?</h4>
	</div>
	<div class="modal-body">
	<p>
	Items considered Hazardous Materials (HAZMAT) including aerosols and alcohol-based products (e.g., pressurized spray cans, hairspray, nail polish remover, nail polish etc.) are required by the U.S. Department of Transportation to be shipped by Ground-only transport, due to air transport restrictions and regulations. For these shipments, we recommend UPS Ground. USPS Ground is also an option for shipments in the continental United States. For P.O. Box addresses, USPS Ground (Parcel Post) is the only shipping method available. Regrettably, these items cannot be shipped to Alaska, Hawaii, or Puerto Rico.
	</p>
	</div>
	</div>
	</div>
	</div>
	</div>
<span id="current-product" data-id="P384728" class="hidden"></span>
<!-- dung cho trang product -->
<div id="popup_view_larger" style="display: none; background-color: white; min-width: 900px;">
    <div style="height: 500px;width:500px;float:left" class="">
        <div class="block_center" style="text-align: center; width: 550px; height: 500px; display: table-cell; vertical-align: middle;">
            <img id="popup_image" width="450" style="padding:20px;" src="[@PRODUCT_IMAGE]" alt="">
        </div>
    </div>
    <div style="height: 500px;float:left;width:400px;">
        <button onclick="close_popup('popup_view_larger');" class="icon icon-close" aria-hidden="true" data-dismiss="modal" style="display: inline; float:right;margin-bottom: -1px;margin-top: 20px;margin-right: 20px;" type="button"></button>
        <div style="height: 300px;width:360px;float:left; margin-right: 40px;margin-top: 20px;" class="product-description container">
            <div style="width:100%;float:left">
                <div style="width:40%;float:left;font-size: 17px;">[@PRODUCT_NAME]</div>
                <div class="purchase-info" style="width:55%;float:left">
            <span class="sku-price">
                <span class="list-price">
                    <span class="currency" style="font-size: 13px;">C</span>
                    <span class="price" style="font-size: 13px;">[@PRODUCT_PRICE]</span>
                </span>
                <span class="sale-price" style="display: none;">
                    <span class="currency" style="font-size: 13px;">C</span>
                    <span class="price" style="font-size: 13px;">[@PRODUCT_PRICE_DISCOUNT]</span>
                </span>
            </span>
                    <div class="out-of-stock" style="display: none;">
                        <b>not in stock</b>
                        <span style="display: none;">:</span>
                        <a class="btn-email-when-in-stock" data-sku_number="1539543" href="#" style="display: none;">e-mail when in-stock</a>
                    </div>
                </div>
            </div>

            <div class="info-row">
            <span class="sku" style="display:none">
                <span class="label">Item #</span>
                <span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
            </span>
            <span class="size" style="display:none">
                <span class="label" style="display: none;">Size</span>
                <span class="value"></span>
            </span>
            </div>
        </div>
        <div style="height: 200px;width:360px;float:left;">
            <p class="flags-market">
                <span class="flag flag-le">[@LIMITED_EDITION]</span>
            </p>
            <div class="product-views container">
                <h3>VIEWS</h3>
                <div id="sku1539543-thumbslarger" class="image-selector border">
                    [@POPUP_ITEM]
                </div>
            </div>
        </div>
    </div>
</div>