<div class="tab-pane fade" id="bv-qa">
	<div id="BVQAContainer" style="" class="BVBrowserWebkit">
		<div id="BVQAWidgetID" class="BVQAWidget">
			<div id="BVQAHomePageID" class="BVQAWidgetWrapper BVQAHomePage">
				<div id="BVQAHeaderID" class="BVQAHeader">
					<div id="BVQAQuestionAndAnswerCountID" class="BVQAQuestionAndAnswerCount">
						Questions (
						<span class="BVQACount BVQANonZeroCount">
							See All Q's
							<span class="BVQANumberSpan">
								(
								<span class="BVQANumber">35</span>
								)
							</span>
						</span>
						) &nbsp; Answers (
						<span class="BVQACount BVQANonZeroCount">
							&amp; A's
							<span class="BVQANumberSpan">
								(
								<span class="BVQANumber">100</span>
								)
							</span>
						</span>
						)
					</div>
					<h1 id="BVQAHeaderTitleID" class="BVQATitle BVQAHeaderTitle"></h1>
					<h2 id="BVQAHeaderSubTitleID" class="BVQASubTitle BVQAHeaderSubTitle">Ask your questions. Share your answers.</h2>
				</div>
				<div id="BVQAMainID" class="BVQAMain BVQAMainView">
					<div class="BVQAPageTabs">
						<div class="BVQAPageTabSpacerLeft">&nbsp;</div>
						<div id="BVQAPageTabHomeID" class="BVQAPageTab BVQASelectedPageTab">HOME</div>
						<div class="BVQAPageTabSpacerMiddle">&nbsp;</div>
						<div id="BVQAPageTabBrowseID" class="BVQAPageTab" onclick="$BV.Internal.Requester.get('http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded','BVQAFrame',''); return false;">
							<a title="BROWSE" name="BV_TrackingTag_QA_Display_BrowseTab" target="BVQAFrame" class="BVQAPageTabLink" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded">BROWSE</a>
						</div>
						<div class="BVQAPageTabSpacerMiddle">&nbsp;</div>
						<div id="BVQAPageTabSearchID" class="BVQAPageTab" onclick="$BV.Internal.Requester.get('http://answers.astoria.com/answers/8723illuminate/product/P7411/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__','BVQAFrame',''); return false;">
							<a title="SEARCH" name="BV_TrackingTag_QA_Display_SearchTab" target="BVQAFrame" class="BVQAPageTabLink" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__">SEARCH</a>
						</div>
						<div class="BVQAPageTabSpacerRight">&nbsp;</div>
					</div>
					<div class="BVQASearchForm">
						<div class="BVQASearchFormText">
							Search:
							<input type="text" id="BVQASearchFormTextInputID" maxlength="150" class="BVQASearchFormTextInput BVQASearchFormTextInputDefault" onkeypress="return bvDisableReturn(event, function(){bvqaSubmitSearch('http://answers.astoria.com/answers/8723illuminate/product/P7411/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__','','');BVAnalyticsTracker.fireActionEvent(document.getElementById('BVQASearchFormSubmitButtonID'));});" onfocus="this.value = ''; this.className = 'BVQASearchFormTextInput'; this.onfocus = null;"></div>
						<div class="BVQASearchFormSubmit">
							<button id="BVQASearchFormSubmitButtonID" class="BVQASearchFormSubmitButton" name="BV_TrackingTag_QA_Display_SearchButton" onclick="bvqaSubmitSearch('http://answers.astoria.com/answers/8723illuminate/product/P7411/searchquestions.htm?format=embedded&amp;search=__SEARCHTEXT__','','');return false;" data-bvtrack="eName:Search"></button>
						</div>
						<div id="BVQAAskQuestionHeaderID" class="BVQAAskQuestion">
							<a title="Ask a New Question&nbsp;" target="BVQAFrame" onclick="return typeof(BVQAOnSubmit)=='function' ? BVQAOnSubmit(this.href, 'QUESTION_SUBMISSION') : true;" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP7411%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp">
								<img src="http://answers.astoria.com/answers/8723illuminate/static/buttonAskANewQuestion.gif" alt="Ask a New Question&nbsp;"></a>
						</div>
					</div>
					<div id="BVQAQuestionsID0" class="BVQAQuestions">
						<div id="BVQAQuestionGroupHeaderFirstID" class="BVQAQuestionGroupHeader BVQAQuestionGroupHeaderFirst">Questions with most helpful answers:</div>
						<div id="BVQAQuestionAndAnswers365385" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader0365385" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_365385" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['1034186','425297'],'questionIds':['365385']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=helpfula&amp;expandquestion=365385">Ingredients please</a>
								</h1>
							</div>
						</div>
						<div class="BVQAQuestionDivider"></div>
						<div id="BVQAQuestionAndAnswers912521" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader0912521" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">3</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_912521" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['1354816','1167775','1076024'],'questionIds':['912521']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=helpfula&amp;expandquestion=912521">What happened to the scent?</a>
								</h1>
							</div>
						</div>
						<div class="BVQAQuestionDivider"></div>
						<div id="BVQAQuestionAndAnswers62077" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader062077" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_62077" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['260662','42832'],'questionIds':['62077']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=helpfula&amp;expandquestion=62077">Does this product contain lanolin?</a>
								</h1>
							</div>
						</div>
						<div class="BVQAQuestionDivider"></div>
						<div id="BVQAQuestionAndAnswers81517" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader081517" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_81517" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['245666','55731'],'questionIds':['81517']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=helpfula&amp;expandquestion=81517">Does the 24 oz. bottle come with a pump dispenser??</a>
								</h1>
							</div>
						</div>
					</div>
					<div id="BVQAQuestionsID1" class="BVQAQuestions">
						<div class="BVQAQuestionGroupHeader">Can you answer these questions?</div>
						<div id="BVQAQuestionAndAnswers70712" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader170712" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_70712" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['245668','47663'],'questionIds':['70712']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=answers&amp;dir=asc&amp;expandquestion=70712">Firming?</a>
								</h1>
							</div>
						</div>
						<div class="BVQAQuestionDivider"></div>
						<div id="BVQAQuestionAndAnswers121536" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader1121536" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_121536" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['245664','81966'],'questionIds':['121536']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=answers&amp;dir=asc&amp;expandquestion=121536">Too much fragrance?</a>
								</h1>
							</div>
						</div>
						<div class="BVQAQuestionDivider"></div>
						<div id="BVQAQuestionAndAnswers70265" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader170265" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_70265" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['245669','52818'],'questionIds':['70265']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=answers&amp;dir=asc&amp;expandquestion=70265">regarding Amazing Grace lotion:</a>
								</h1>
							</div>
						</div>
						<div class="BVQAQuestionDivider"></div>
						<div id="BVQAQuestionAndAnswers145835" class="BVQAQuestionAndAnswers BVQAQuestionAndAnswersProduct BVQAQuestionAndAnswersNative">
							<div id="BVQAQuestionHeader1145835" class="BVQAQuestionHeader BVQAQuestionHeaderClosed">
								<div class="BVQAQuestionHeaderBullet">&nbsp;</div>
								<div class="BVQAQuestionAnswersCount">
									<span class="BVQACount BVQANonZeroCount">
										<span class="BVQANumber">2</span>
										answers
									</span>
								</div>
								<h1 class="BVQAQuestionSummary">
									<a title="View Question" name="BV_TrackingTag_QA_Display_HomteTab_QuestionSelect_145835" target="BVQAFrame" onclick="BVAnalyticsTracker.fireActionEvent(this, 'BV_TrackingTag_Question_questionExpansion',{'answerIds':['240121','97889'],'questionIds':['145835']});" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/questions.htm?format=embedded&amp;sort=answers&amp;dir=asc&amp;expandquestion=145835">Weight loss</a>
								</h1>
							</div>
						</div>
					</div>
					<div id="BVQAAskQuestionHeaderID" class="BVQAAskQuestion">
						<a title ="Ask a New Question&nbsp;" name="BV_TrackingTag_QA_Display_AskQuestion" target="BVQAFrame" onclick="bvShowContentOnReturnQA('8723illuminate', 'P7411', 'BVQAAskQuestionID');return typeof(BVQAOnSubmit)=='function' ? BVQAOnSubmit(this.href, 'QUESTION_SUBMISSION') : true;" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP7411%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp">
							<img src="http://answers.astoria.com/answers/8723illuminate/static/buttonAskANewQuestion.gif" alt="Ask a New Question&nbsp;" title="Ask a New Question&nbsp;"></a>
					</div>
				</div>
				<div id="BVQAFooterID" class="BVQAFooter">
					<div id="BVQAGuidelinesID" class="BVQAGuidelines">
						<a name="BV_TrackingTag_QA_Display_QAndAGuidelines" href="http://answers.astoria.com/answers/8723illuminate/content/guidelines.htm" onclick="window.open(this.href,null,'left=50,top=50,width=500,height=500,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" title="Product Q&amp;A guidelines" id="BVQAGuidelinesLinkID" class="BVQAPopupLink BVQAGuidelinesLink">See full product Q&amp;A guidelines</a>
					</div>
					<div id="BVQAAskQuestionID" class="BVQAAskQuestion">
						<a title="Ask a New Question&nbsp;" name="BV_TrackingTag_QA_Display_AskQuestion" target="BVQAFrame" onclick="bvShowContentOnReturnQA('8723illuminate', 'P7411', 'BVQAAskQuestionID');return typeof(BVQAOnSubmit)=='function' ? BVQAOnSubmit(this.href, 'QUESTION_SUBMISSION') : true;" href="http://answers.astoria.com/answers/8723illuminate/product/P7411/askquestion.htm?format=embedded&amp;campaignid=BV_QA_HOME&amp;sessionparams=__BVSESSIONPARAMS__&amp;return=http%3A%2F%2Fwww.astoria.com%2Famazing-grace-firming-body-emulsion-P7411%3FskuId%3D607655&amp;innerreturn=http%3A%2F%2Fanswers.astoria.com%2Fanswers%2F8723illuminate%2Fproduct%2FP7411%2Fquestionshome.htm%3Fformat%3Dembedded&amp;user=__USERID__&amp;userdisplayname=__USERDISPLAYNAME__&amp;authsourcetype=__AUTHTYPE__&amp;submissionparams=__BVSUBMISSIONPARAMETERS__&amp;submissionurl=http%3A%2F%2Fwww.astoria.com%2Fproduct%2Fbv%2FaskAnswer.jsp">
							<img src="http://answers.astoria.com/answers/8723illuminate/static/buttonAskANewQuestion.gif" alt="Ask a New Question&nbsp;" title="Ask a New Question&nbsp;"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>