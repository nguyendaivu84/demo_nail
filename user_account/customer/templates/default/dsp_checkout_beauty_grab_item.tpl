<div class="grab-item [@BEAUTY_GRAB_ITEM_CLASS] container">
	<div class="grab-img">
		<div class="product-image">
			<img src="[@PRODUCT_IMAGE]" width="97" height="97">
			<a id="sku_1552876" href="javascript:void(0);" data-product-name="[@PRODUCT_SLUGGER]" class="qlook" rel="click_to_quick_look">QUICK LOOK</a>
		</div>
	</div>
	<div class="grab-info">
		<p>
			<span class="brand OneLinkNoTx">[@BRAND_NAME]</span>
			<br>
			<span class="product-name OneLinkNoTx">[@PRODUCT_NAME]</span>
			<br>
			<span class="sale-price">C[@PRODUCT_PRICE](C[@PRODUCT_PRICE_DISCOUNT] Value)</span>
		</p>
		<button class="btn btn-default btn-add-to-basket" data-sku_number="1552876" id="addSweetSteal_href" name="add-to-basket" value="add to basket"
                onclick="add_to_basket('[@PRODUCT_ID]',1,'[@ADD_URL]',1);"
                type="button">Add To Basket</button>
	</div>
</div>