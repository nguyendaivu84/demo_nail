var wa = wa || {};

wa.config = {
  RELEASE: '20140114',  // update with every release
  BASE_PATH: '/javascripts/analytics/',

  /***
   wa.min.js must detect how to include other libraries when it is
   included on dev/qa environments and  by third parties includeStates
   defines how the other wa JS file should be referenced
   */
  includeStates: [{
    NAME: 'preproduction',
    PROD_MODE: false,
    HOSTS: [
      'localhost',  // local dev
      'qa.astoria.com', // qa
      'preview.qa.astoria.com', // preview
      'staging.merchantmail.net', // acxiom pre-production
      'sandbox.qa.astoria.com', // qa
      'origin.qa.astoria.com', // qa
      'staging.illuminate.astoria.com', // staging
      'staging.astoria.com',
      'dev.illuminate.astoria.com', // dev
      'dev.astoria.com',
      'qa.illuminate.astoria.com', // qa
      'qa.astoria.com', // qa
      'm-qa.astoria.com',  // mobile qa
      'm-qa.illuminate.astoria.com', // mobile qa
      'community.qa.astoria.com', // community qa
      'ebf.astoria.com', // EBF environment
      'test.skedge.me', // online res test
      'dev.reserve.astoria.com', // online res test
	  'dev-canada.astoria.com',
	  'fr-dev-canada.astoria.com',
	  'qa-canada.astoria.com',
	  'fr-qa-canada.astoria.com',
      'fr-astoria-dev.onelink-translations.com',
      'fr-astoria-qa.onelink-translations.com',
      new RegExp( '\\d+\\.\\d+\\.\\d+\\.\\d+' ) // RegExp for all ips
    ],
    INCLUDE_HOST: '' // defaults to relative include with port used
  }, {
    NAME: 'online reservations development/test',
    PROD_MODE: false,
    HOSTS: ['test.skedge.me', 'dev.reserve.astoria.com', 'dev-m-reserve.astoria.com'],
    INCLUDE_HOST: 'dev.illuminate.astoria.com'
  }, {
    NAME: 'online reservations production',
    PROD_MODE: true,
    HOSTS: ['www.skedge.me', 'skedge.me', 'reserve.astoria.com', 'm-reserve.astoria.com'],
    INCLUDE_HOST: 'www.astoria.com'
  }, {
    NAME: 'external preproduction',
    PROD_MODE: false,
    HOSTS: ['m-qa.astoria.com','community.qa.astoria.com','m-qa.illuminate.astoria.com'],
    INCLUDE_HOST: 'qa.illuminate.astoria.com' // external QA should pull wa2 from astoria QA server
  }, {
    NAME: 'astoria 2.0 beta',
    PROD_MODE: true,
    HOSTS: ['beta.astoria.com','illuminate.becho.net'],
    INCLUDE_HOST: 'beta.astoria.com'
  }, {
    NAME: 'production',
    PROD_MODE: true,
    HOSTS: [
      'www.astoria.com',
      'astoria.com',
      'shop.astoria.com',
      'community.astoria.com',
      'm.astoria.com',
      'reviews.astoria.com',
      's.brandingbrandmobile.com',
      'www.astoriacanada.com',
      'birthday.astoria.com',
      'astorialove.com', // promo microsite
      'www.astorialove.com', // promo microsite
      'astorialove.ca',
      'www.astorialove.ca',
      'app.astoria.com',
	  'canada.astoria.com',
	  'www.astoria.ca',
	  'astoria.ca',
	  'fr-canada.astoria.com',
	  'coloroftheyear.astoria.com'
    ],
    INCLUDE_HOST: 'www.astoria.com'
  }]

};

/** @constructor */
wa.lineItem = function (productId, productName, skuId, skuName, quantity, price, special, biType, notInStock, isAncillary) {
  this.productId = productId;
  this.productName = productName;
  this.skuId = skuId;
  this.skuName = skuName;
  this.quantity = quantity;
  this.price = price;
  this.special = special ? special.toLowerCase() : "";
  this.biType = biType ? biType.toLowerCase() : "unspecified";
  this.notInStock = notInStock;
  this.isAncillary = isAncillary;
};

/** @constructor */
wa.Payment = function (type, value) {
  this.type = type;
  this.value = value;
};

/** @constructor */
wa.Refinement = function (type, value) {
  this.type = type;
  this.value = value;
};

// function called on tracked events
wa.tries = 0;
wa.action = function(obj) {
  try {
    wa.trackAction(obj);
  } catch (e) {
    // sometimes the wa2 file is not loaded when this is called; try again
    if(wa.tries < 10){
      setTimeout(function(){wa.action(obj);}, 1000);
      wa.tries++;
    }
  }
};

wa.setCookie = function (name, value, days) {
  try {
    var expires = '';
    if (!wa.isEmpty(days)) {
      var dt = new Date();
      dt.setTime(dt.getDate() + days);
      expires = '; expires=' + dt.toGMTString();
    }
    document.cookie = name + '=' + escape(value) + expires + '; path=/';

  } catch (e) {}
};

wa.getCookie = function (name) {
  try {
    var results = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    var result = ( results && results.length > 2 ) ? unescape(results[2]) : "";
    if (results) {
      return result;
    } else {
      return null;
    }
  } catch (e) {}
};

wa.deleteCookie = function (name) {
  this.setCookie(name, '', -1);
};

wa.loadScript = function (js,callback) {
  try {
    var b = document.getElementsByTagName('head')[0];
    var s = document.createElement('script');

    if (s.readyState){  // IE
      s.onreadystatechange = function() {
        if (s.readyState == "loaded" || s.readyState == "complete") {
          s.onreadystatechange = null;
          callback();
        }
      };
    } else {  // Others
      s.onload = function(){
        callback();
      };
    }
    s.type = 'text/javascript';
    s.src = js;
    b.appendChild(s);
  } catch (e) {}
};

wa.isEmpty = function (a) {
  return a === null || typeof a == 'undefined' || a === '';
};

wa.inStringRegExpList = function (a,v) {
  for (var i = a.length; i--;) {
    if (typeof a[i] == 'string' && a[i] == v) {
      return true;
    } else if (a[i] instanceof RegExp && a[i].test(v)) {
      return true;
    }
  }
  return false;
};

wa.getQueryParam = function(name, href) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var result = RegExp('[\\?&]' + name + '=([^&#]*)').exec(href);
  return result === null ? '' : decodeURIComponent(result[1].replace(/\+/g, ' '));
};

// cleanup data being passed into SiteCatalyst
// (perform recursive URL-decoding, etc.)
wa.clean = function(txt) {
  // called 3x because some content is encoded multiple times
  txt = decodeURIComponent(decodeURIComponent(decodeURIComponent(txt)));
  return txt;
};

// called twice to make sure both document ready and script loaded
wa.initOnReady = function() {
  if (wa.config.ready) {
    if((typeof wa !== 'undefined') && ("waInitReady" in wa)) {
        wa.init();
    } else {
        $("body").one("waInitReady", function(){wa.init();});
    }
  } else {
    wa.config.ready=true;
  }
};

  // new function to load wa2
  wa.loadWa2 = function() {
  var host = location.hostname.toLowerCase();

  // determine what environment for include paths
  var includeStateFound = false;
  var i = wa.config.includeStates.length;
  while (i-- && !includeStateFound) {
    var state = wa.config.includeStates[i];
    includeStateFound = (wa.inStringRegExpList(state.HOSTS, host)) ? true : includeStateFound;
    if (includeStateFound) {
      wa.config.includeName = state.NAME;
      wa.config.includeHost = state.INCLUDE_HOST;
      wa.config.prodMode = state.PROD_MODE;
    }
  }
  if (!includeStateFound) {
    wa.config.includeName = 'no include state found';
    wa.config.includeHost = "";
    wa.config.prodMode = false;
  }

  var locationPort = '';
  if(!wa.isEmpty(location.port)) {
	  locationPort = ':'+location.port;
  } 
  
  // if no host specified, include relatively with port for dev instances
  wa.config.baseURL = (wa.isEmpty(wa.config.includeHost)) ? '//' + host + locationPort : '//' +
	wa.config.includeHost;
  wa.config.baseURL += wa.config.BASE_PATH;

  // non blocking loading of wa library
  wa.loadScript(wa.config.baseURL + 'wa2.js?release=' + wa.config.RELEASE,
	function() {
	    wa.config.ready = true;
	    wa.initOnReady();
	}
  );

  // make sure both document ready and library loaded before initialization
  try {
    jQuery(document).ready(

        function () {
		//wa.initOnReady();
        });
    wa.config.jQueryFound = true;
  } catch (e) {
    wa.config.jQueryFound = false;
    if (window.addEventListener) {
      //window.addEventListener('load', wa.initOnReady, false);
    } else if (window.attachEvent) {
      //window.attachEvent('onload', wa.initOnReady);
    }
  }

};
