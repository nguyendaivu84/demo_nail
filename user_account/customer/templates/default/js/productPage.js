// dependent upon jQuery
document.domain = "astoria.com";
var contextPath = $("meta[name='contextPath']").attr("content");
var bvContextPath = $("meta[name='bvctx']").attr("content");
var urlPrefix = document.location.protocol + "//";

function bvShowTab(application, displayCode, subjectId, deepLinkId) {
  if (application == 'QA') {
    BV_changeTabs("askAndAnswer");
  }
  else if (application == 'PRR') {
    BV_changeTabs("ratingsReviews");
  }
}

function BVQADisplayed(questionCount, answerCount) {
  if (questionCount > 0) {
    var bvALPLink = document.getElementById("BVALPLinkContainer");
    if (bvALPLink) { bvALPLink.style.display = "block"; }
  }
}

function ratingsDisplayed(totalReviewsCount, avgRating, ratingsOnlyReviewCount, recommendPercentage, productID) {
  if (totalReviewsCount == 0) {
    //var bvRevCntr = document.getElementById("BVReviewsContainer");
    var bvSVPLink = document.getElementById("BVSVPLinkContainer");
    //if (bvRevCntr) { bvRevCntr.style.display = "none"; }
    if (bvSVPLink) { bvSVPLink.style.display = "none"; }
  }
}

function bvLoadRR() {
  // Start Configuration
  var bvPage = '//reviews.astoria.com'+bvContextPath+'/8723Illuminate/'+ bvProdId +'/reviews.htm?format=embedded';
  var URLParamNames = {
    reviewID : 'featurereview',
    reviewPage : 'reviewspage'
  };
  // End Configuration

  var getURLParam = function(name) {
    if (!name) return null;
    var match = (new RegExp('[?&]' + name + '=([^&#]+)')).exec(window.location.search);
    return match ? decodeURIComponent(match[1]) : null;
  };

  bvReviewID = getURLParam(URLParamNames.reviewID);
  bvReviewPage = getURLParam(URLParamNames.reviewPage);

  if (/^[0-9]+$/.test(bvReviewID)) {
    bvPage += '&reviewid=' + bvReviewID;
  } else if (/^[0-9]+$/.test(bvReviewPage)) {
    bvPage += '&page=' + bvReviewPage;
  }

  document.getElementById('BVFrame').src = bvPage;
//  alert(bvPage)

  //Timeout for review load.  Consider reviews unavailable if not loaded within 15 seconds
  setTimeout("BVcheckLoadState()", 15000);
}

function bvLoadQA() {
  var bvQAFrameSrc = '//answers.astoria.com'+bvContextPath+'/answers/8723Illuminate/product/'+ bvProdId+'/questionshome.htm?format=embedded';
  var bvQuestionIDName = 'featurequestion';

  var bvoice_user = document.getElementById('BVQACustomerID').innerHTML;

  var bvQuestionIDRegex = new RegExp('[?&]' + bvQuestionIDName + '=([^&#]+)');11
  var bvQuestionIDMatch = bvQuestionIDRegex.exec(window.location.search);
  var bvQuestionID = bvQuestionIDMatch ? decodeURIComponent(bvQuestionIDMatch[1]) : null;

  bvQAFrameSrc = /^[0-9]+$/.test(bvQuestionID) ? bvQAFrameSrc.replace('/questionshome.htm', '/questions.htm') + '&expandQuestion=' + bvQuestionID : bvQAFrameSrc;

  var bvPageMatch = /[?&]bvpage=([^&#]+)/.exec(window.location.search);
  var bvPage = bvPageMatch ? decodeURIComponent(bvPageMatch[1]) : null;
  var bvRegex = new RegExp('^' + window.location.protocol + '\/\/([A-Za-z0-9-.]+[.])?' + document.domain + '\/');

  if (bvPage && bvRegex.test(bvPage)) {
    document.getElementById('BVQAFrame').src = bvPage.replace(/__USERID__/, bvoice_user);
  } else {
    document.getElementById('BVQAFrame').src = bvQAFrameSrc;
  }

  //Timeout for QA load.  Consider content unavailable if not loaded within 15 seconds
  setTimeout("BVcheckQALoadState()", 15000);
}

// I might have to move this var to the prod page. Need to test more.
var BVisLoaded = false;

function BVcheckLoadState() {
  if(!BVisLoaded) {
    var page = document.getElementById('BVFrame').src;
    document.getElementById('BVFrame').src='//reviews.astoria.com'+bvContextPath+'/logging?page=' + escape(page);
    document.getElementById('BVReviewsContainer').innerHTML = '<!-- Review retrieval timed out -->';
  }
}

var BVQAisLoaded = false;

function BVcheckQALoadState() {
  if(!BVQAisLoaded) {
    var page = document.getElementById('BVQAFrame').src;
    document.getElementById('BVQAFrame').src='//answers.astoria.com'+bvContextPath+'/logging?page=' + escape(page);
    document.getElementById('BVQAContainer').innerHTML = "<!-- QA retrieval timed out -->";
  }
}

function BV_changeTabs(tab){
  if(tab == "ratingsReviews"){
    $(".ratings-reviews li.ratings a").trigger("click");
    window.scrollTo(0, $(".ratings-reviews li.ratings a").offset().top - 50);
  } else if(tab == "askAndAnswer") {
    $(".ratings-reviews li.q-a a").trigger("click");
    window.scrollTo(0, $(".ratings-reviews li.q-a a").offset().top - 50);
  }
}

function setContanerPageUrl() {
  if(document.getElementById("BVContainerPageURL")){
    document.getElementById("BVContainerPageURL").innerHTML = urlPrefix + location.host + contextPath + "/product/bv/review.jsp";
    bvLoadRR();

  }
  if(document.getElementById("BVQASubmissionURL")){
    document.getElementById("BVQASubmissionURL").innerHTML = urlPrefix + location.host + contextPath + "/product/bv/askAnswer.jsp";
    bvLoadQA();
  }
}

function setExtraWriteReviewAndQALinks(){
  if($(".ratings-content").length > 0){
    if(BVisLoaded && BVQAisLoaded){
      var links = $(".ratings-content .actions a");

      if($("#BVRRRatingSummaryLinkWriteID").length > 0) {
          links[0].href = $("#BVRRRatingSummaryLinkWriteID a")[0].href;
      }
      if($("#BVRRRatingSummaryLinkWriteFirstID").length > 0) {
          links[0].href = $("#BVRRRatingSummaryLinkWriteFirstID a")[0].href;
      }
      if($("#BVQASummaryBoxAskQuestionID"). length > 0) {
          links[1].href = $("#BVQASummaryBoxAskQuestionID a")[0].href;
      }
      if($("#BVQASummaryBoxAskFirstQuestionID").length > 0) {
          links[1].href = $("#BVQASummaryBoxAskFirstQuestionID a")[0].href;
      }
    } else {
      setTimeout("setExtraWriteReviewAndQALinks()", 2000);
    }
  }
}

setContanerPageUrl();
setExtraWriteReviewAndQALinks();