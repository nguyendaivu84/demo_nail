/*
  modified version of jCarouselLite 1.0.1
  fixes disabling arrows and height cutoff issues
  note: horizontal carousels only
*/
(function ($) {
    $.fn.jCarouselLite = function (options) {
        var o = $.extend({}, $.fn.jCarouselLite.defaults, options);
        return this.each(function () {
            var running = false,
                animCss = "left",
                sizeCss = "width",
                timer = null,
                delayInput = null,
                scrollCount = 0;
            var div = $(this),
                ul = div.find('ul'),
                tLi = ul.children('li'),
                tl = tLi.length,
                v = o.visible,
                isAutoScroll = false,
                buttonClicked = null;
            o.start = Math.min(o.start, tLi.length - 1);
            o.circular = tLi.length > v ? o.circular : false;

            if (o.circular) {
              var first = tLi.slice(0, v).clone(true);
              var last = tLi.slice(tl - v - 1 + 1).clone(true);
              ul.prepend(last).append(first);
                o.start += v;
            }


            var li = ul.children('li'),
                itemLength = li.length,
                curr = o.start;
            var liSize = width(li);
            if(liSize == 0) { // hack to init hidden carousels correclty
              var newCar = ul.closest(".carousel-component").clone();
              newCar.css({position: "absolute", left: "-1000px", display: "block"});
              $('body').append(newCar);
              liSize = width($("li", newCar));
              newCar.remove();
            }
          /*var liHeight = height(li);
          $(li).each(function(){
            var val = parseInt($(this).height());
            if (liHeight === undefined || liHeight < val) {
              liHeight = val;
            }
          });
          */
            var ulSize = liSize * itemLength;
            var divSize = liSize * v;
            ul.css(sizeCss, ulSize + "px").css(animCss, -(curr * liSize));
            $.each(['btnPrev', 'btnNext'], function (index, btn) {
                if (o[btn]) {
                    o['$' + btn] = $.isFunction(o[btn]) ? o[btn].call(div[0]) : $(o[btn]);
                    o['$' + btn].click(function (e, isAuto) {
                      buttonClicked = e.target;
                      isAutoScroll = isAuto;
                        var step = index == 0 ? curr - o.scroll : curr + o.scroll;
                        return go(step);
                    });
                }
            });
            //div.css({"height": liHeight});
            if (!o.circular) {
                if (o.btnPrev && o.start == 0) {
                    o.$btnPrev.addClass(o.btnDisabledClass);
                }
                if (o.btnNext && o.start + o.visible >= itemLength) {
                    o.$btnNext.addClass(o.btnDisabledClass);
                }
                if (o.visible >= itemLength) {
                    o.$btnPrev.css('visibility', 'hidden');
                    o.$btnNext.css('visibility', 'hidden');
                }
            }
            if (o.btnGo) {
                $.each(o.btnGo, function (i, val) {
                    $(val).click(function () {
                      return go(o.circular ? (o.visible+i-1) : i);
                        //return go(o.circular ? o.visible + i : i);
                    });
                });
            }
            if (o.mouseWheel && div.mousewheel) {
                div.mousewheel(function (e, d) {
                    return d > 0 ? go(curr - o.scroll) : go(curr + o.scroll);
                });
            }
            if (o.auto) {
                var setAutoAdvance, advanceCounter = 0,
                    autoStop = iterations(tl, o);
                var advancer = function () {
                    setAutoAdvance = setTimeout(function () {
                        if (!autoStop || autoStop > advanceCounter) {
                            go(curr + o.scroll);
                            advanceCounter++;
                            advancer();
                        }
                    }, o.timeout + o.speed);
                };
                advancer();
                $(document).bind('pauseCarousel', function (event) {
                    clearTimeout(setAutoAdvance);
                    $(event.target).data('paused', true);
                }).bind('resumeCarousel', function (event) {
                    advancer();
                    $(event.target).data('paused', false);
                });
                if (o.pause) {
                    div.mouseenter(function () {
                        div.trigger('pauseCarousel');
                    }).mouseleave(function () {
                        div.trigger('resumeCarousel');
                    });
                }
            }
            if(o.isSlideShow){
                delayInput = $("input[type='hidden']", div);
                if(delayInput.length > 0 && o.animationType > 0){
                    timer = setTimeout(function(){fireClick(delayInput);}, delayInput.val());
                }
            }

            function fireClick(input){
                input.siblings(".carousel-next").trigger("click", true);
            }

            function vis() {
                return li.slice(curr).slice(0, v);
            }
            function go(to) {
                if (!running) {
                    scrollCount++;
                    if(timer){
                        clearTimeout(timer);
                        timer = null;
                    }
                    if (o.beforeStart) {
                        o.beforeStart.call(this, vis(), isAutoScroll, buttonClicked);
                    }
                    if (o.circular) {
                        if (to <= o.start - v - 1) {
                            ul.css(animCss, -((itemLength - (v * 2)) * liSize) + "px");
                            curr = to == o.start - v - 1 ? itemLength - (v * 2) - 1 : itemLength - (v * 2) - o.scroll;
                        } else if (to >= itemLength - v + 1) {
                            ul.css(animCss, -((v) * liSize) + "px");
                            curr = to == itemLength - v + 1 ? v + 1 : v + o.scroll;
                        } else {
                            curr = to;
                        }
                    } else {
                        o.$btnPrev.toggleClass(o.btnDisabledClass, o.btnPrev && to <= 0);
                        o.$btnNext.toggleClass(o.btnDisabledClass, o.btnNext && to >= itemLength - v);
                        if (to < 0) {
                            curr = 0;
                        } else if (to > itemLength - v) {
                            curr = itemLength - v;
                        } else {
                            curr = to;
                        }
                    }
                    running = true;
                    ul.animate(animCss == "left" ? {
                        left: -(curr * liSize)
                    } : {
                        top: -(curr * liSize)
                    }, o.speed, o.easing, function () {
                        if (o.afterEnd) {
                            o.afterEnd.call(this, vis(), curr, o.btnGo);
                        }
                        running = false;
                    });
                    if(!timer && delayInput && (o.animationType == 1 || (o.animationType == 2 && tl > scrollCount))){
                        timer = setTimeout(function(){fireClick(delayInput);}, delayInput.val());
                    }

                }
                return false;
            }
        });
    };
    $.fn.jCarouselLite.defaults = {
        btnPrev: null,
        btnNext: null,
        btnDisabledClass: 'disabled',
        btnGo: null,
        mouseWheel: false,
        speed: 450,
        easing: null,
        auto: false,
        autoStop: false,
        timeout: 4000,
        pause: true,
        circular: false,
        visible: 3,
        start: 0,
        scroll: 1,
        beforeStart: null,
        afterEnd: null,
        isSlideShow: false,
        animation: 'slow',
        animationType: 0
    };

    function css(el, prop) {
        return parseInt($.css(el[0], prop), 10) || 0;
    }
    function width(el) {
        return el[0].offsetWidth + css(el, 'marginLeft') + css(el, 'marginRight');
    }
    function height(el) {
        return el[0].offsetHeight + css(el, 'marginTop') + css(el, 'marginBottom');
    }
    function iterations(itemLength, options) {
        return options.autoStop && (options.circular ? options.autoStop : Math.min(itemLength, options.autoStop));
    }
})(jQuery);