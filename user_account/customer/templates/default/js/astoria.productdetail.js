//     astoria Product Detail page

//     (c) 2012  S E P H O R A


(function($) {
  
  "use strict"

  $.extend(astoria.ui.controller, {
    
    FlashController: function(options) {
      astoria.logger.log("FlashController");
      this.scope = options.scope;
      var self = this,
        addToCart = "button.btn-add-to-basket",
        scope = self.scope;
      
      $(addToCart,scope).attr("data-custom-enable", true);
      $.extend(self, new astoria.ui.controller.ShoppingCartController())
        .addToBasket(addToCart, function(source){
          return $(source).closest("form").serializeArray();
        });
        
      $("#accept-terms", scope).change(function() {
        $(addToCart, scope).prop("disabled", !$(this).is(":checked"));
        $('.msg-accept', scope).toggleClass('hide');
      });
      self.initCartUtils();//needed to get the templates
    }, // FlashController - move to separate file when this becomes too big

    // Controller for product pages.
    ProductDetailController: function() {

      var self = this;
      $.extend(this, astoria.util.HtmlUtils,
              astoria.ui.controller.ThumbnailController, {

        updateReminderStoreLocator: function(sku) {//deprecated?

          var settings = this.infoSettings;

          if (sku.is_show_find_in_store) {
            $(settings.productActions + " .find").find('label').text('Find in store').removeClass('online-only').end()
                    .find('input').removeAttr('disabled').show().end()
                    .find('button').removeAttr('disabled').show().end();
          } else if (sku.is_online_only) {
            $(settings.productActions + " .find").find('label').text('Only available online').end()
                    .find('input').attr('disabled', 'disabled').hide(0, function(){$(settings.productActions + " .find").find('label').addClass('online-only');}).end()
                    .find('button').attr('disabled', 'disabled').hide().end();
          } else {
            $(settings.productActions + ' .find').hide();
          }
        },

        skuForEmail: function(origin) {
          if( $(origin).closest("#primarySkuInfoArea").length===0 ) {
            //must be from a vertical or collection
            return { sku_number: $(origin).attr("data-sku_number") };
          } else {
            return this.currentSku();
          }
        },

        resolveItems: astoria.ui.controller.resolveItems
      });
      self.thumbScope = ".main-product-thumbnails";
    }, // ProductDetailController

    // Controller to handle events for store searches.
    FindInStoreController: function(productController) {
      
      var self = this,
        store = new astoria.util.storage.ProductStorage, marker,
        errorBuilder = new astoria.ui.error.ErrorBuilder({top: "#find-in-store", globalInsertBefore: "div.search-stores div.container:first" }),
        goErrorBuilder = new astoria.ui.error.ErrorBuilder({topElement: ".find", globalInsertAfter: "*:first" }),
        cont = $("#find-in-store"),
        remoteFindInStore = astoria.ui.Store.findInStore;

      $.extend(self, astoria.util.MapUtils, astoria.ui.ViewHelper,
              astoria.util.HtmlUtils,
              astoria.util.Validates,
              astoria.util.CountrySupport);

      self.validator = new astoria.validation.Validator();
      self.validator.add(new astoria.validation.ZipCodeValidator("input[name='zip']", { anyCountry: true }));

      // Add all addresses as markers.
      function markAndPosition(store) {
        var storeName = self.storeNameFrom(store),
            lat = store.data("lat"),
            lng = store.data("lng"),
            address = $(".store-name", store).text();
        $('#find-in-store-map').find('.modal-title').text(address);

        function miles() {
          var val = $("select[name='within']").val(),
              isMiles = !(/km/i).test($("select[name='within'] option:selected").text());
          return { miles: isMiles ? parseInt(val) : val / 1.6,
            unit: isMiles ? "mile" : "km" };
        }

        var latLg = new google.maps.LatLng(lat, lng);
        var theMap = self.repositionMap(latLg, miles()); // main address for positioning
        if (marker) {
          marker.setMap(null);
        }
        marker = new google.maps.Marker({
          position: latLg,
          map: theMap,
          title: storeName
        });
      }

      var showFlame = function() {
          $('#find-in-store div.search-stores').addClass('loading')
          $(document).one("ajaxComplete", function() {
            $('#find-in-store div.search-stores').removeClass('loading');
          });
        },

        clearResults = function() {
          return cont.find('.search-stores').empty();
        },

        removeStoreList = function() {
          cont.find(".modal-scroll.custom-scroll").remove();
        };

      $(".find form").submit(function(e) {
        e.preventDefault();
        var trigger = $(this),
          field = $(trigger).children("input"),
          zip = field.val();

        goErrorBuilder.withNoErrors(self.validator.validate(), {
          success: function() {

            $("#find-in-store .error-message").remove();
            self.findInStore(zip, -1);
          },
          error: function() {
            removeStoreList();
          }
        });
      });

      $("#find-in-store").on("click", '.btn-search-again', function(e) {
        $("#find-in-store .error-message").slideUp(function() {
          $(this).remove();
        });

        errorBuilder.withNoErrors(self.validator.validate({top: "#find-in-store"}), {
          success: function() {
              self.findInStore(cont.find("input[name='zip']").val(), cont.find('select').val());
          }, error: function() {
            removeStoreList();
          }
        });
        e.preventDefault();
      });

      // instrument the maps
      $("#find-in-store").on("click", "a.store-map-link", function(e){
        e.preventDefault();
        markAndPosition($(this).closest(".one-store"));
        $('body').trigger("layover", [{title:"view store map"}]);
        astoria.util.PopupUtils.displayCenteredPopup($("#find-in-store-map"), {backdrop: true});
      });

      var onError = function() {
        var isSearchBox = $(".search-stores .container:first").length > 0,
          error = $("<br>"+astoria.ui.error.makeError(astoria.i18n.t("inventory.lookup.notavailable")));
        if ( isSearchBox ) {
          error.insertAfter($(".search-stores .container:first"));
          cont.find('.modal-scroll').remove();
         } else {
            cont.find('.search-stores').append(error);
         }
        astoria.util.PopupUtils.displayCenteredPopup("#find-in-store", {backdrop: true});
      };

      $.extend(self, {
        findInStore: function(zip, within) {

          function findOnServer(longLat) {
            astoria.util.PopupUtils.displayCenteredPopup($("#find-in-store"), {backdrop: true});
            showFlame();
            remoteFindInStore({ sku_id: productController.currentSku().id,
              zip: zip,
              within: within,
              latitude: longLat ? longLat.lat() : undefined,
              longitude: longLat ? longLat.lng() : undefined }, function(resp, _, xhr) {
                if ( xhr.status !== 200 ) {
                  onError();
                  return;
                }

                clearResults().append($(resp));

                self.updateSkuInfo();
                $("body").trigger("findInStore", [zip, resp]);
              }, onError);
          }

          self.longLatFrom(astoria.conf.CAREG.test(zip) ? zip + ", Canada" : zip, function(longLat) {

            findOnServer(longLat);
          }, function() {
            findOnServer(false);
          });
        },
        // Update the sku and product information.
        // Must be called every time the popup is shown for a sku, but not when
        // user repeats a search for the same sku.
        updateSkuInfo: function() {
          var sku = productController.currentSku(),
            doUpdate = function(product) {
              $('#find-in-store').find('h3 span:first').html(product.brand_name).end()
                    .find('h3 span:last').html(product.display_name).end()
                    .find(".sku span:last").text(sku.sku_number).end()
                    .find('.info-row:last .value').text(sku.variation_value || "").end()
                    .find('img').attr('src', self.prodImageFor(sku.inline_image));
            };
          //if ( "primary_product" in sku ) {
          if ( "primary_product" in productController ) {
            doUpdate(productController.primary_product); // only default Sku has it, fix so productFor is not necessary
          }	else {
            store.productFor(sku.primary_product_id, function() {
              doUpdate(this);
            });
          }
        }
      });
    },
    // controller for the large view
    LargeView: function(settings, skuStorage) {
      this.scope = settings.scope;
      settings = $.extend(settings, { dialog: '#modal-view-larger',
                          top: '#modal-view-larger'});

      settings = $.extend(settings, { beautyInsiderMember: false,
                          skuPrice: ".product-description .product-price:first span.price",
                          heroImage: settings.dialog + ' ' + '.product-image img',
                          heroMessages: settings.dialog,
                          imageSelector: settings.dialog + ' .image-selector',
                          skuSize: '.size .value',
                          thumbnails: '.product-thumb',
                          beautyInsider: settings.dialog + ' ' + settings.beautyInsider,
                          price: ".sku-price:has(.list-price .price)",
                          seal: settings.dialog + ' ' + settings.seal,
                          swatches: ' .sku-selector-content .product-thumb',
                          notifyEmail: ".btn-email-when-in-stock, .btn-change-in-stock-email"
                        });

      $.extend(this, astoria.ui.controller.ThumbnailController,
              new astoria.ui.controller.EmailWhenBackInStockController,
              new astoria.ui.product.ProductView(settings, skuStorage));
      var self = this;
      this.instance = 'LargeView';
      this.thumbScope = settings.dialog+" .product-views";
      function currentSwatches() {
        return $(settings.dialog + ' div.sku-selector-content .product-thumb');
      }

      self.resolveHeroImages = function(sku) {
        return self.resolveImages(sku.large_images);
      };

      // change the current swatch selection and remember sku
      self.rememberSku = function(sku) {
        this.sku = sku;
        currentSwatches().removeClass('current');
        self.swatchById(sku.id, currentSwatches).addClass('current');
      };

      //
      self.updateAll = function(options) {
        self.updateAllExceptAction(options);
      };
      // restores the views from the saved sku
      self.restore = function() {
        self.updateAllExceptAction(self.sku);
      };

      self.updateThumbnailsView = function(sku, activeIndex) {
        self.thumbnailUpdate(sku, activeIndex, function(noShow) {
          $(settings.dialog + ' .product-views h3')[noShow ? 'hide' : 'show']();
        });
      };
    }, // astoria.ui.controller.LargeView

    ImageZoomController : function(){
      var image = $(".hero-main-image > img");
      if(image.attr("data-zoom")){
        var largeImage = $("<img src='"+image.attr("src").replace("hero", "zoom")+"'/>");
        var zoomWindow = $("<div class='zoom-window'></div>");
        var glass = $("<div class='glass'></div>");
        var glassImage = image.clone();
        var mask = $("<div class='mask'></div>");
        var backdrop = $("<div class='zoom-backdrop'><div>");
        var imageContainer = $(".hero-main-image");
        var toolTip = $("<div class='zoom-tooltip'>CLICK TO ZOOM</div>");
        var animationDone = true;
        var imageLoaded = false;
        var showTip = false;

        imageContainer.append(glass);
        $("body").append(toolTip);
        glass.append(glassImage);
        imageContainer.append(mask);
        zoomWindow.append(largeImage);
        $("body").append(zoomWindow);
        $("body").append(backdrop);


        var getPageX = function(e){
          return Modernizr.touch ? e.originalEvent.changedTouches[0].pageX : e.pageX;
        }

        var getPageY = function(e){
          return Modernizr.touch ? e.originalEvent.changedTouches[0].pageY : e.pageY;
        }

        var closeZoomWindow = function(e) {
          e.preventDefault();
          e.stopPropagation();
          if(animationDone){
            backdrop.hide();
            animationDone = false;
            zoomWindow.animate({top:(getPageY(e)-42)+"px", left:(getPageX(e)-42)+"px", width:"84px", height:"84px", opacity:.25}, 500, function(){
              glass.hide();
              mask.css({display:"none"});
              zoomWindow.hide();
              animationDone = true;
              if(!Modernizr.touch){
                moveTip(e);
              }
            });
          }
        }

        var moveGlass = function(e){
          e.preventDefault();
          e.stopPropagation();
          var offset = image.offset();
          var relX = getPageX(e) - offset.left;
          var relY = getPageY(e) - offset.top;
          glass.css({left:(relX - 42) +"px", top:(relY - 42) + "px"});
          glassImage.css({left:-(relX - 42) +"px", top:-(relY - 42) + "px"});
          largeImage.css({left:-(((relX/250) * 1500) - 250) +"px", top:-(((relY/250) * 1500) - 250) + "px"});
        }

        var openZoomWindow = function(e) {
          e.preventDefault();
          e.stopPropagation();
          if(imageLoaded){
            if(animationDone){
              if(Modernizr.touch){
                backdrop.show();
              }
              toolTip.hide();
              glass.show();
              mask.css({display:"block", opacity:".25"});
              var pos = $(".wrapper").position();
              var extraMargin = $(".wrapper").css("margin-left").replace("px" , "");
              var leftPos = 335 + (isNaN(extraMargin) ? pos.left : pos.left + (extraMargin - 0));
              animationDone = false;
              zoomWindow.css({top:(getPageY(e) -42)+"px", left:(getPageX(e) -42)+"px", width:"84px", height:"84px", display:"block", opacity:.25});
              zoomWindow.animate({top:(145 + pos.top)+"px", left:leftPos+"px", width:"500px", height:"500px", opacity:1}, 500, function(){
                animationDone = true;
              });
            }
          }
        }

        var moveTip = function(e) {
          var relX = e.pageX + 20;
          var relY = e.pageY - 10;
          toolTip.css({top:relY+"px", left:relX+"px"});
        }

        var disableZoom = function() {
          imageLoaded = false;
          showTip = false;
          image.css("cursor", "default");
        }

        image.load(function(){
          disableZoom();
          if($(this).attr("src").indexOf("not-available") == -1){
            largeImage.attr("src", $(this).attr("src").replace("hero", "zoom"));
          }
        });

        largeImage.on("error", disableZoom);
        largeImage.load(function(){ imageLoaded = true; showTip = true; image.css("cursor", "crosshair"); });

        if(Modernizr.touch){
          imageContainer.on("touchstart", openZoomWindow);
          imageContainer.on("touchstart", moveGlass);
          imageContainer.on("touchend", closeZoomWindow);
          imageContainer.on("touchmove", moveGlass);
          backdrop.click(function(){
                  zoomWindow.hide();
                  mask.hide();
                  glass.hide();
                  $(this).hide();
                });
        } else {
          imageContainer.hover(function(){}, closeZoomWindow);
          imageContainer.mousemove(moveGlass);
          glass.click(closeZoomWindow);
          image.click(openZoomWindow);
          image.mousemove(moveTip);
          image.hover(function(){if(showTip){ toolTip.show();}}, function(){toolTip.hide();});
        }
      }
    }

  });
  
  astoria.app.initializer(function() {
    
    $('.product-tabs a:first').tab('show');
    
    astoria.util.PopupUtils.showInfoPopups(".maincontent .pop-info");
    
    if($("#flash-product-content").length) {
      return; //Flash pages initialize from HTML directives
    }
    
    var thumbClone = $(".image-selector a:first").clone(),
      settings = {
        beautyInsiderMember: false, // this depends on the current user
        ingredientVariationVal: "#ingredients",
        thumbClone: thumbClone
      };

    var skuStorage = new astoria.util.storage.SKUStorage();
    var quantField = ".product-quantity .quantity",
      views = $.extend({}, new astoria.ui.product.ProductView(settings, skuStorage),
            new astoria.ui.controller.ProductDetailController,
            astoria.ui.controller.SupportsLoves,
            new astoria.ui.controller.EmailWhenBackInStockController,
            astoria.util.HtmlUtils,
            new astoria.ui.controller.ShoppingCartController({renderFieldError: function() {
              $(quantField).addClass("error");
              }, show: function() {
                $(quantField).removeClass("error");
              }
              }),
            new astoria.ui.controller.BiExclusiveController,
    { instance: "ProductDetailController" });
    views.scope = views.infoSettings.top;
    views.init();
    
    views.saveInitial()
    .love(".btn-love", true);
    
    views.addToBasket('.btn-add-to-basket, .btn-buy-the-collection')
            .onSuccessfulAdd(function(source, cart, _, isCollection) {
      views.updateOnAdd(cart, _, isCollection);
    });

    var validator = new astoria.validation.Validator;
    validator.add(new astoria.validation.ItemQuantityValidator({quantity: quantField}));
    
    views.registerEmailStock();
    views.validator = validator;

    var settings2 = {thumbClone: thumbClone, scope: "#modal-view-larger"};

    var largeView = new astoria.ui.controller.LargeView(settings2, skuStorage);
    largeView.init();
    largeView.saveInitial();
		largeView.registerEmailStock();

    new astoria.ui.controller.FindInStoreController(views);

    $("body").bind("backInStockEmailNotification", views.onEmailWhenBackInStockNotification);

    function currentSku() {
      return views.currentSku();
    }

    views.initScroll();

    // --------------------- view larger events ----------------

    $('.view-larger-trigger').on("click",  "> a", function(e) {
      e.preventDefault();
      largeView.rememberSku(views.sku);
      largeView.primary_product = views.primary_product;
      var index = views.currentThumbnailIndex();

      astoria.util.PopupUtils.displayCenteredPopup("#modal-view-larger", {backdrop: true,
        onClose: function(){
          views.rememberSku(largeView.sku);
          var index = largeView.currentThumbnailIndex();
          if($(".product-information.collection").length > 0){
            views.updateForCollection({sku: largeView.sku, thumbIndex: index});
          } else {
            views.updateAll({sku: largeView.sku, thumbIndex: index});
          }
          views.initScroll();
        }
    });
      largeView.updateAll({sku: views.sku, thumbIndex: index});
      astoria.dimensions.setAllWidths();
      largeView.initScroll();
    });

    astoria.ui.controller.ImageZoomController();

    $(".recently-viewed [data-productid], .ymal [data-productid]").click(function(e){
      astoria.analytics.analyze("sendData", ["pfm", "product"], true);
    });

  });//initializer

})(jQuery);
