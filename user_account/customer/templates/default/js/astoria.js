(function($) {
	var astoria = window.astoria = (window.astoria || {}),
		analyze = $.noop;
	astoria.analyze = analyze;

	function isHostMethod(object, property) {
		var t = typeof object[property];
		return t == "function" || ( !! (t == "object" && object[property])) || t == "unknown"
	}
	var on, off;
	if (isHostMethod(window, "addEventListener")) {
		on = function(target, type, listener) {
			target.addEventListener(type, listener, false)
		};
		off = function(target, type, listener) {
			target.removeEventListener(type, listener, false)
		}
	} else {
		on = function(object, sEvent, fpNotify) {
			object.attachEvent("on" + sEvent, fpNotify)
		};
		off = function(object, sEvent, fpNotify) {
			object.detachEvent("on" + sEvent, fpNotify)
		}
	}
	astoria.module = function(requested, fn) {
		var mods = requested.split("."),
			module = astoria;
		if (mods[0] === "astoria") {
			mods.shift()
		}
		mods.forEach(function(m) {
			module = (module[m] = module[m] || {})
		});
		if (fn) {
			fn(module)
		}
		return module
	};
	astoria.module("event", function(event) {
		event.publish = event.trigger = function(name, params) {
			astoria.logger.log("astoria.event.publish ", name, params);
			$("body").trigger(jQuery.Event(name), params)
		};
		event.subscribe = function(eventName, fn, once) {
			$("body")[once ? "one" : "bind"](eventName, fn)
		};
		event.resubscribe = function(eventName, fn) {
			$("body").off(eventName, fn).on(eventName, fn)
		}
	});
	var contextPath = $("meta[name='contextPath']").attr("content") || "";
	$.fn.tap = function(fn) {
		var self = $(this);
		if ($.isFunction(fn)) {
			fn.call(self, self)
		}
		return this
	};
	if (typeof $.fn.trim === "undefined") {
		$.fn.trim = function() {
			this.each(function() {
				$(this).val($.trim($(this).val()))
			});
			return this
		}
	}

	function getFormData(formId) {
		var params = {}, form = typeof formId === "string" ? $("#" + formId) : $(formId);
		form.find(":input").each(function() {
			if ($(this).attr("name")) {
				var type = $(this).attr("type");
				if ("checkbox" == type) {
					if ($(this).attr("checked") === true) {
						params[$(this).attr("name")] = $(this).val()
					}
				} else {
					if ("radio" == type) {
						if ($(this).attr("checked") === true) {
							params[$(this).attr("name")] = $(this).val()
						}
					} else {
						params[$(this).attr("name")] = $(this).val()
					}
				}
			}
		});
		return params
	}

	function resolveItems(source, items) {
		var data = null,
			isCollection = $.isFunction(this.isCollection) ? this.isCollection() : false,
			currentSkuId = $.isFunction(this.currentSkuId) ? this.currentSkuId() : undefined,
			hasSwatchs = $(".sku-selector-content").is(":visible");
		data = $(source).closest("form").serializeArray();
		$(".purchase-info .product-quantity .quantity").each(function(index) {
			var elemName = $(this).attr("name");
			if (index == 0 && hasSwatchs) {
				var match = elemName.match(/(.*itemsMap\.)(.*)/);
				if (match && currentSkuId) {
					elemName = match[1] + currentSkuId
				}
			}
			if ($.isArray(data)) {
				data.push({
					name: elemName,
					value: isCollection ? 1 : $(this).val()
				})
			} else {
				data[elemName] = isCollection ? 1 : $(this).val()
			}
		});
		data.push({
			name: "/atg/commerce/order/purchase/CartModifierFormHandler.addItemToOrder",
			value: "add"
		});
		data = $.grep(data, function(field) {
			return field.name.indexOf("CartModifierFormHandler.removeItemFromOrder") == -1
		});
		return data
	}(function() {
		var coreJail = {
			timeout: 50,
			offset: 250,
			callbackAfterEachImage: function(img, options) {},
			callback: function(options) {
				astoria.logger.log("@core.JAIL callback - all JAILs released.")
			}
		}, jailConfigs = {
				mainContent: $.extend({}, coreJail, {
					placeholder: "/images/blank.gif"
				})
			};
		astoria.jail = {
			lazyImages: function(images, opts) {
				$(images).jail($.extend({}, jailConfigs.mainContent, opts))
			},
			markForceLoad: function(fn) {
				var items = fn(12);
				(items || []).forEach(function(i) {
					i.forceLoad = true
				})
			}
		}
	})();
	$.fn.carousel = function() {
		$(this).each(function() {
			try {
				$(this).initCarousel()
			} catch (e) {
				astoria.logger.log(e)
			}
		});
		return this
	};
	$.fn.initCarousel = function() {
		var carousel = $(this);
		var isSlideShow = carousel.hasClass("slideshow");
        var auto_slide = false;
        if(isSlideShow){
            auto_slide = true;
        }
		var productCnt = $("ul li", carousel).length;
		var circle = $(carousel).data("circle");
		var scrollSpeed = parseInt($(carousel).data("speed"));
		var animation = parseInt($(carousel).data("animation"));
		var productVisibleCnt = parseInt($(carousel).data("show"));
		var maxSlide = Math.floor(productCnt / productVisibleCnt);
		var title = carousel.siblings("h2, h3").text().trim() ? carousel.siblings("h2, h3").text().trim().toLowerCase() : isSlideShow ? carousel.attr("data-name") : carousel.closest(".carousel-component, .bi-rewards-component, .bi-exclusives-component, .image-carousel").attr("data-name").toLowerCase();
		var slideNumber = 1;
		var jailHandler = function(slideNum, button) {
			button = ($(button).hasClass("carousel-control")) ? $(button) : $(button).closest(".carousel-control");
			slideNum = button.hasClass("carousel-prev") ? slideNum - 1 : slideNum + 1;
			if (slideNum == 0 || slideNum > maxSlide) {
				slideNum = button.hasClass("carousel-prev") ? maxSlide : 1
			}
			var imgs = carousel.find("[data-index='" + slideNum + "'] img[data-src], [data-slide-index='" + slideNum + "'] img[data-src]");
			imgs.each(function(i, el) {
				$(el).attr("src", $(el).data("src"))
			})
		};
		if (!Modernizr.touch) {
			$("a[data-productid]", carousel).click(function(e) {
				astoria.analytics.analyze("productClickThrough", [title, slideNumber, $(e.delegateTarget).attr("data-productId")], true)
			})
		}
		$(".product-item", carousel).hover(function() {
			$(this).addClass("hovering")
		}, function() {
			$(this).removeClass("hovering")
		});
		if (carousel.find("div.product-item[points]").length === 0) {
			new astoria.ui.controller.ShoppingCartController({
				scope: carousel
			}).addToBasket(".product-item .btn", function(source) {
				return $(source).closest("form").serializeArray()
			})
		}
		var ctrl = new astoria.ui.controller.CarouselController({
			scope: carousel,
			isCarousel: true
		});
		$(".count", carousel).html("1/" + productCnt);
		carousel.jCarouselLite({
			btnNext: $(carousel).children(".carousel-next"),
			btnPrev: $(carousel).children(".carousel-prev"),
//			easing: "easeInOutQuart",
			speed: 1500,
            auto: auto_slide,
            easing: '',
            timeout: 3000,
			circular: circle,
			visible: productVisibleCnt,
			scroll: productVisibleCnt,
			animationType: animation,
			beforeStart: isSlideShow ? function(list, isAutoScrolled, button) {
				if (!isAutoScrolled) {
					var direction = $(button).closest(".carousel-next, .carousel-prev").attr("analytics");
					var slideNum = $(list).data("slide-index");
					astoria.analytics.analyze("slideshowPager", [title, slideNum, direction])
				}
				jailHandler(slideNum, button)
			} : function(list, _, trigger) {
				var slideNumber = $(list).data("slide-index");
				astoria.analytics.analyze("carouselArrowClick", [title, trigger, slideNumber]);
				jailHandler(slideNumber, trigger)
			},
			isSlideShow: isSlideShow,
			afterEnd: function(li) {
				$(".count", carousel).html($(li).data("slide-index") + "/" + productCnt);
				ctrl.enableTooltips()
			}
		});
		if (Modernizr.touch && carousel.find("ul.carousel-inner > li").length >= productVisibleCnt) {
			var isSwipe = false,
				lastX, lastY;
			$(carousel).bind("touchstart", function(evt) {
				lastX = evt.originalEvent.changedTouches[0].pageX;
				lastY = evt.originalEvent.changedTouches[0].pageY;
				astoria.logger.log("Swipe touchstart ", lastX, lastY)
			}).bind("touchmove", function(e) {
				var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0],
					currX = touch.pageX,
					currY = touch.pageY,
					c = $(this);
				isSwipe = lastX ? (Math.abs(lastX - currX) > Math.abs(lastY - currY)) : true;
				if (lastX && isSwipe) {
					setTimeout(function() {
						c.find(lastX - currX > 0 ? "div.carousel-next" : "div.carousel-prev").click();
						lastX = currX;
						lastY = currY
					}, 2)
				}
			})
		}
	};
	$.fn.modalVideo = function() {
		$(this).click(function() {
			var img = $(this),
				modal = $("#modal-content-component"),
				content = img.next().clone(),
				width = content.children("input[name='width']"),
				height = content.children("input[name='height']");
			modal.children(".modal-body").html(content);
			width.remove();
			height.remove();
			modal.addClass("modal-video").css({
				width: width.val() + "px",
				minHeight: height.val() + "px"
			});
			astoria.util.PopupUtils.displayCenteredPopup(modal, {
				backdrop: true,
				backdropDark: true,
				afterDisplay: function() {
					if (content.find("input").length > 0) {
						content.append("<div id='ooyallaContainer'></div>");
						content.append("<script src='" + content.find("input").val() + "'><\/script>")
					}
					if (content.find("video").length > 0) {
						content.find("video")[0].play()
					}
				},
				onClose: function() {
					content.html("")
				}
			});
			astoria.analytics.analyze("videoPopup", [img.closest(".videoPopComponent").attr("data-name")])
		});
		return this
	};
	$.fn.video = function() {
		var idx = 1;
		$(this).click(function() {
			var img = $(this);
			var target = img.next();
			var targetId = "target" + idx;
			target.attr("id", targetId);
			idx++;
			img.remove();
			target.show();
			var urlInput = target.children("input[name='url']");
			var video = target.children("video");
			if (urlInput.length > 0) {
				target.append(img);
				var url = urlInput.val() + targetId;
				target.children("input").remove();
				target.after("<script src='" + url + "'><\/script>")
			}
			if (video.length > 0) {
				video[0].play()
			}
		});
		return this
	};
	$.fn.dynamicOoyala = function() {
		$(this).each(function(idx) {
			var container = $(this).parent();
			var input = $(this).remove();
			var targetId = "dynOoyala" + idx;
			container.append("<div id='" + targetId + "'></div>");
			var url = input.val() + targetId;
			container.append("<script src='" + url + "'><\/script>")
		})
	};
	$.fn.storeSearchComponent = function() {
		var errors = $(this).find(".error-message");
		var options = {
			scope: this,
			analytics: "component",
			error: function() {
				var errormessage = astoria.i18n.t("storeLocator.input.bad");
				errors.text(errormessage).slideDown()
			}
		};
		new astoria.ui.controller.StoreSearchController(options)
	};
	$.fn.populate = function(data, options) {
		var top = this,
			conf = $.extend({
				prefix: ""
			}, options || {});
		$.each(data, function(key, value) {
			try {
				var target;
				if (typeof(conf.entity) !== "undefined") {
					target = $("[name='" + conf.entity + "[" + key + "]']", top)
				} else {
					target = conf.byId ? ($("#" + conf.prefix + key).length > 0 ? $("#" + conf.prefix + key) : $("#" + conf.prefix + key + "_" + value)) : $("[name='" + key + "']", top)
				} if (target.length > 0) {
					if (target.is(":radio")) {
						conf.byId ? target.attr("checked", true) : target.filter("[value='" + value + "']").attr("checked", true)
					} else {
						if (target.is(":checkbox")) {
							target.attr("checked", Boolean(value))
						} else {
							target.val(value)
						}
					} if (target.val() !== value && conf.exception && $.isFunction(conf.exception[key])) {
						conf.exception[key].call(target, value)
					}
				} else {
					if (conf.exception && conf.exception[key]) {
						$.each(conf.exception[key], function(key, q) {
							if (value) {
								var field = conf.byId ? $("#" + key) : $("[name='" + key + "']", top);
								field.val(q[1] ? value.substr(q[0], q[1]) : value.substr(q[0]))
							}
						})
					}
				}
			} catch (e) {}
		});
		return this
	};
	$.fn.withoutAncillary = function() {
		return $(this).filter(function() {
			return $(this).parents(".ancillary-products").length === 0
		})
	};
	$.fn.clearForm = function() {
		this.find(":input").each(function() {
			switch (this.type) {
				case "select-multiple":
				case "select-one":
					this.selectedIndex = 0;
					break;
				case "text":
				case "textarea":
				case "password":
					$(this).val("");
					break;
				case "checkbox":
				case "radio":
					this.checked = false
			}
		});
		return this
	};
	$.fn.clearError = function() {
		this.find(":input").each(function() {
			$(this).removeClass("error")
		});
		return this
	};
	$.fn.disableUntilAjaxComplete = function(success) {
		var a = $(this);
		if (a.hasClass("disabled")) {
			return
		} else {
			a.addClass("disabled");
			$(document).one("ajaxComplete", function() {
				a.removeClass("disabled")
			});
			if ($.isFunction(success)) {
				success()
			}
		}
	};

	function closePopups() {
		$(".modal:visible .icon-close").click()
	}
	astoria.logger = {
		log: $.noop,
		exec: $.noop,
		diff: $.noop,
		init: function() {
			var log = $.noop,
				val, param = window.location.search.slice(1).split("&").filter(function(e) {
					return /debug=/.test(e)
				}),
				isDebug = param[0] && ((val = param[0].split("=")[1]) === "true" || val === "1");
			if (isDebug) {
				var lastStamp = (new Date()).getTime(),
					diff = function() {
						var next = (new Date()).getTime(),
							ret = next - lastStamp;
						lastStamp = next;
						return " diff: " + ret
					};
				if (/MSIE\s7/.test(navigator.userAgent)) {
					$("body").prepend('<a id="__clear" href="#">clear</a><textarea rows="10" cols="80" id="logs" style="border: solid green 4px;"></textarea>');
					var logs = $("#logs"),
						t = document.getElementById("logs");
					$("#__clear").click(function(e) {
						e.preventDefault();
						logs.val("")
					});
					log = function() {
						logs.val(logs.val() + "\n\r" + Array.prototype.slice.call(arguments, 0).join(" "));
						t.scrollTop = t.scrollHeight
					}
				} else {
					if ("console" in window && window.console.log) {
						log = function() {
							window.console.log(Array.prototype.slice.call(arguments, 0).join(" "))
						}
					}
				}
				var exec = function(fn) {
					fn()
				};
				$.extend(this, {
					log: log,
					exec: exec,
					diff: diff
				})
			}
		}
	};
	astoria.ajax = {
		processMeta: function(resp) {
			var p = !("_loaded" in astoria.meta) && $.isPlainObject(resp) && ("meta" in resp);
			if (p) {
				astoria.logger.log("processMeta saving meta ", resp.meta);
				astoria.meta = resp.meta;
				astoria.meta._loaded = true;
				astoria.template || (astoria.template = {});
				astoria.template.meta || (astoria.template.meta = {});
				$.extend(astoria.template.meta, astoria.meta)
			}
		},
		DisablingAjax: {
			initAjaxDisable: function(scope) {
				scope = scope || this.scope || this.top;
				if (typeof scope === "undefined") {
					scope = $("body")
				}
				$("[data-ajaxlink]", scope).click(function() {
					var sel = $($(this).attr("data-ajaxlink") || "[data-ajaxlink]"),
						enableAll = function(enable) {
							var curr;
							for (var i = 0, l = sel.length; i < l; i++) {
								curr = $(sel[i]);
								if (curr.is("button")) {
									curr.attr("disabled", !enable)
								} else {
									if (curr.is("a")) {
										if (enable) {
											curr.attr("href", curr.data("href"));
											curr.removeData("href")
										} else {
											curr.data("href", curr.attr("href"));
											curr.removeAttr("href")
										}
									} else {
										$.error({
											message: "Unsupported tag."
										})
									}
								}
							}
						}, before = function() {
							enableAll(false)
						}, after = function(xhr) {
							enableAll(true)
						};
					$.ajaxSetup({
						beforeSend: function(xhr) {
							$.ajaxSetup({
								beforeSend: function() {}
							});
							before();
							xhr.success(after);
							xhr.error(after)
						}
					})
				})
			}
		},
		stopWait: function(element) {
			$("#loader").remove();
			$(element).css("opacity", "1.0")
		}
	};
	(function() {
		var options = {
			success: astoria.ajax.processMeta,
			complete: function(xhr, textStatus) {
				if (xhr.status === 401) {
					astoria.util.Redirecting.fatalAjaxError()
				}
			}
		};
		$.ajaxSetup(options);
		$("meta[name='is_pinterest_enabled']").tap(function(pin) {
			astoria.meta || (astoria.meta = {});
			astoria.meta.is_pinterest = pin.length ? pin.attr("content") !== "false" : true
		})
	})();
	["Post", "Get", "Delete"].forEach(function(verb) {
		astoria.ajax["makeAjax" + verb] = function(url, opts) {
			opts || (opts = {});
			var optsSuccess;
			if (opts.success) {
				optsSuccess = opts.success;
				delete opts.success
			}
			return (function() {
				var pending = false;
				return function() {
					var args = Array.prototype.slice.call(arguments, 0),
						data, success, error, ropts;
					switch (args.length) {
						case 1:
							if ($.isFunction(args[0])) {
								success = args[0]
							} else {
								data = args[0]
							}
							break;
						case 2:
							if (!$.isFunction(args[0])) {
								data = args[0];
								if ($.isFunction(args[1])) {
									success = args[1]
								} else {
									ropts = args[1]
								}
							} else {
								success = args[0];
								if ($.isFunction(args[1])) {
									error = args[1]
								} else {
									ropts = args[1]
								}
							}
							break;
						case 3:
							if (!$.isFunction(args[0])) {
								data = args[0];
								success = args[1]
							} else {
								success = args[0]
							} if ($.isPlainObject(args[1])) {
								ropts = args[1]
							} else {
								if (typeof data === "undefined") {
									error = args[1]
								}
							} if ($.isPlainObject(args[2])) {
								ropts = args[2]
							} else {
								error = args[2]
							}
							break;
						case 4:
							data = args[0];
							success = args[1];
							error = args[2];
							ropts = args[3];
							break;
						default:
					}
					var ajax = $.ajax(url, $.extend({
						type: verb,
						data: data,
						beforeSend: function(jqXHR, settings) {
							if (pending) {
								return false
							} else {
								pending = true;
								if (optsSuccess) {
									jqXHR.done(optsSuccess)
								}
								jqXHR.done(success).always(function() {
									astoria.logger.log(url, " ajax pending false");
									pending = false
								});
								if ($.isFunction(error)) {
									jqXHR.fail(error)
								}
							}
						}
					}, opts, ropts || {}));
					return ajax
				}
			})()
		}
	});
	astoria.conf = {
		contextPath: contextPath,
		basketAddUrl: contextPath + "/global/json/basketAdd.jsp",
		myListMoveToMyListUrl: contextPath + "/global/json/myListMoveToMyList.jsp",
		cartRemoveItemUrl: contextPath + "/global/json/cartRemoveItem.jsp",
		cartUpdateQuantUrl: contextPath + "/global/json/cartUpdateQuant.jsp",
		cartAddSamplesUrl: contextPath + "/global/json/cartAddSamples.jsp",
		cartRemoveAllSamplesUrl: contextPath + "/global/json/cartRemoveAllSamples.jsp",
		myListEmailToFriendUrl: contextPath + "/global/json/myListEmailToFriend.jsp",
		basketUrl: contextPath + "/basket/basket.jsp",
		securePopup: "/popups/securePopup.jsp",
		brandUrl: contextPath + "/brands/",
		productVideo: contextPath + "/product/include/showVedio.jsp",
		autoSearchUrl: "/products/",
		productEmailStockUrl: contextPath + "/product/include/notificationRequestPopup.jsp?skuId=:id",
		productDetailUrl: contextPath + "/product/productDetail.jsp",
		quickLookUrl: contextPath + "/product/include/quickLook/ql.jsp?skuId=:id",
		miniQuickLookUrl: contextPath + "/product/include/quickLook/miniQL.jsp?skuId=:id",
		myAccountLandingUrl: contextPath + "/profile/MyAccount/myAccountLoading.jsp",
		skuUrl: contextPath + "/global/json/getSkuJson.jsp?skuId=:id",
		skuIndexUrl: "",
		findUserUrl: contextPath + "/profile/login/findUser.jsp",
		userUrl: contextPath + "/profile/MyAccount/MyInformation/json/getProfileInfomationByJson.jsp",
		registrationResponse: contextPath + "/profile/registration/popup/response/registrationResponse.jsp",
		updateUserUrl: contextPath + "/profile/MyAccount/include/blank.jsp",
		loginPopupUrl: contextPath + "/popups/popupLogin.jsp",
		logoutResponseUrl: contextPath + "/profile/login/response/logoutResponse.jsp",
		loginValidationMsgUrl: contextPath + "/profile/login/response/loginValidationMsg.jsp",
		registrationOrBIDialog: contextPath + "/popups/popupSignup.jsp ",
		resetPasswordResponseUrl: contextPath + "/popups/popupForgetPassword.jsp",
		imgPrefix: $("meta[name='imageBasePath']").attr("content"),
		prodImgPrefix: "",
		registerBackInStockEmailUrl: contextPath + "/product/include/",
		emailSignup: contextPath + "/common/footer/saveSignUpEmail.jsp",
		localeCookie: "site_locale",
		langCookie: "site_language",
		shipCookie: "ship_country",
		contactUsCommentLength: 1000,
		personalizedPicks: contextPath + "/global/json/getPersonalizedBIInfo.jsp",
		restContentURL: "/rest/media/"
	};
	astoria.i18n = (function() {
		var messages = window.astoriaBundle;
		return {
			init: function() {
				$("html").find("script[type='text/json'][data-bundle='true']").each(function(_, e) {
					e = $(e);
					try {
						var b = JSON.parse(e.html().trim());
						$.extend(messages, b)
					} catch (e) {
						astoria.logger.log(e)
					}
				})
			},
			t: function(key) {
				var ctry = astoria.util.CookieUtils.currentCountry(),
					ret = messages[key + (ctry !== "us" ? ("." + ctry) : "")],
					args = Array.prototype.slice.call(arguments, 0),
					par, fn;
				if (typeof ret === "undefined") {
					ret = messages[key]
				}
				args.shift();
				while (args.length) {
					par = args.pop();
					if ($.isFunction(par)) {
						fn = par
					} else {
						ret = ret.replace(new RegExp("\\{" + args.length + "\\}", "g"), par)
					}
				}
				if (fn) {
					fn(ret)
				}
				return ret
			},
			registerMessage: function(key, msg) {
				messages[key] = msg
			}
		}
	})();
	astoria.module("ui");
	astoria.ui.ViewHelper = {
		imageFor: function(media) {
			return astoria.conf.imgPrefix + media
		},
		prodImageFor: function(media) {
			return astoria.conf.prodImgPrefix + media
		},
		printPrice: function(sku) {
			var format = this.currencyFormatted,
				curr = $("#currency").attr("data-currency"),
				Sku = astoria.Sku;
			var salePrice, info = Sku.skuInfo(sku),
				price;
			if (Sku.isSale(sku)) {
				return curr + format(sku.sale_price)
			} else {
				return info.isBirthday ? "Birthday Gift" : info.isFree ? "FREE" : info.biReward ? sku.bi_type : curr + format(sku.list_price)
			}
		},
		generateProductLink: function(sku) {
			return astoria.conf.productDetailUrl + "?skuId=" + (sku.isSample() ? sku.full_size_sku_number : sku.sku_number) + (sku.isSample() ? "" : "&productId=" + sku.primary_product.id)
		},
		formatPhone: function(phone) {
			return phone.slice(0, 3) + "-" + phone.slice(3, 6) + "-" + phone.slice(6)
		},
		currencyFormatted: function(amount) {
			var i = parseFloat(amount),
				s;
			if (isNaN(i)) {
				i = 0
			}
			var minus = "";
			if (i < 0) {
				minus = "-"
			}
			i = Math.abs(i);
			i = parseInt((i + 0.005) * 100);
			i = i / 100;
			s = new String(i);
			if (s.indexOf(".") < 0) {
				s += ".00"
			}
			if (s.indexOf(".") == (s.length - 2)) {
				s += "0"
			}
			s = minus + s;
			return s
		},
		maskIt: function(value) {
			if (!value) {
				return ""
			}
			var show = 4;
			if (value.length > show) {
				return "xxxx-xxxx-xxxx-" + value.substring(value.length - show)
			} else {
				return value
			}
		}
	};
	$.extend(astoria, {
		US: "us",
		CA: "ca",
		INSTOCK_EMAIL: "e-mail when in-stock",
		COMING_SOON_EMAIL: "e-mail when available",
		JCPENNEY: "jcpenney",
		app: {
			initializer: function(fn) {
				if (!$.isFunction(fn)) {
					throw "Initializer must be a function."
				}(this.initializers = this.initializers || []).push(fn)
			},
			runInitializers: function() {
				var init;
				while (init = this.initializers.shift()) {
					init()
				}
			}
		},
		template: {
			render: function(compiled, context) {
				if ($.isArray(context)) {
					return context.reduce(function(prev, ctx) {
						try {
							return prev + compiled(ctx)
						} catch (e) {
							astoria.logger.log(e);
							return prev
						}
					}, "")
				} else {
					return compiled(context).trim()
				}
			}
		},
		dimensions: {
			scrollY: function() {
				var posY = window.scrollY;
				if (!posY) {
					var iebody = (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
					posY = document.all ? iebody.scrollTop : pageYOffset
				}
				return posY
			},
			setContainerHeight: function(selector) {
				var mydivholder = $(selector);
				var maxheight = 0;
				var i = 0;
				while (i < mydivholder.length) {
					var myheight = parseFloat($(mydivholder[i]).css("height"));
					if (maxheight < myheight) {
						maxheight = myheight
					}
					i++
				}
				$(selector).height(maxheight)
			},
			equalizeChildrenHeight: function(selector) {
				var holders = $(selector);
				holders.each(function() {
					var maxheight = 0;
					$(this).children().each(function() {
						if (maxheight < $(this).height()) {
							maxheight = $(this).height()
						}
					});
					if (maxheight != 0) {
						$(this).children().height(maxheight)
					}
				})
			},
			setAllHeights: function() {
				astoria.dimensions.setContainerHeight(".height-reg");
				astoria.dimensions.equalizeChildrenHeight(".equalize-child-height")
			},
			setContainerWidth: function(selector) {
				var mydivholder = $(selector);
				var maxwidth = 0;
				var i = 0;
				while (i < mydivholder.length) {
					var mywidth = parseFloat($(mydivholder[i]).css("width"));
					if (maxwidth < mywidth) {
						maxwidth = mywidth
					}
					i++
				}
				$(selector).width(maxwidth)
			},
			setAllWidths: function(selector) {
				selector = selector || ".text-selector:visible";
				$(selector).each(function() {
					astoria.dimensions.setContainerWidth($(".sku-width-reg", $(this)))
				})
			}
		},
		instantiateControllers: function() {
			var inst, part, parts, clazz;
			$("[data-seph-controller]").each(function(_, node) {
				node = $(node);
				parts = node.attr("data-seph-controller").split(".");
				part = parts.shift();
				clazz = window[part];
				while (part = parts.shift()) {
					clazz = clazz[part]
				}
				try {
					var i = 0,
						opts = {}, attrs = node[0].attributes,
						l = attrs.length;
					for (; i < l; i++) {
						if (attrs[i].name.indexOf("data-") === 0) {
							opts[attrs[i].name.slice(5).trim()] = node.attr(attrs[i].name)
						}
					}
					opts.scope = node;
					opts.views = astoria.template.views2;
					inst = new clazz(opts);
					inst.views = opts.views;
					inst.scope = node;
					"click".split(" ").forEach(function(event) {
						var evtAttr = "data-seph-" + event;
						node.find("[" + evtAttr + "]").each(function(_, evented) {
							$(evented).on(event, inst[$(evented).attr(evtAttr)])
						})
					})
				} catch (e) {}
			})
		},
		compileHBTemplates: function() {
			if (!("Handlebars" in window)) {
				return
			}
			var views = astoria.template.views2 || (astoria.template.views2 = {});
			astoria.logger.log("#compileHBTemplates about to compile", astoria.logger.diff());
			$("html").find("script[type='text/x-handlebars-template']").each(function(_, e) {
				e = $(e);
				try {
					views[e.attr("id")] = (function() {
						var text = e.html().trim(),
							fn = Handlebars.compile(text);
						if (e.attr("data-included") === "true") {
							Handlebars.registerPartial(e.attr("id"), text)
						}
						return function(data) {
							try {
								return fn(data).trim()
							} catch (ex) {
								astoria.logger.log("rendering error in ", e.attr("id"))
							}
						}
					})()
				} catch (e) {
					astoria.logger.log(e)
				}
			});
			Handlebars.registerHelper("groupsOfx", function(context, mod, options) {
				var fn = options.fn;
				var ret = [],
					ar, c = [].concat(context);
				while ((ar = c.splice(0, mod)).length > 0) {
					ret.push('<div class="' + context.divClazz + '">');
					for (var i = 0, j = ar.length; i < j; i++) {
						ret.push(fn(ar[i]))
					}
					ret.push("</div>")
				}
				return ret.join("")
			});
			astoria.logger.log("#compileHBTemplates done compiling", astoria.logger.diff())
		},
		init: function() {
			astoria.logger.init();
			astoria.logger.log("astoria.init");
			astoria.compileHBTemplates();
			astoria.instantiateControllers();
			$("input, textarea").placeholder();
			$("span.stars").stars();
			$("[data-toggle=popover]").popover().click(function(e) {
				e.preventDefault();
				if (Modernizr.touch && $(this).data("trigger") == "hover") {
					$(this).popover("toggle")
				}
			});
			if (("analytics" in astoria) && ("analyze" in astoria.analytics)) {
				analyze = astoria.analyze = astoria.analytics.analyze
			}
			astoria.ui.signonController = new astoria.ui.controller.SignonController;
			astoria.util.PopupUtils.decorate(astoria.ui.signonController, "loadSignonDialog");
			this.genericInfoPopups();
			if ($(".payment-methods, .order-history-summary, .my-subscriptions").length > 0 && !astoria.util.CookieUtils.isSignedInUser()) {
				astoria.ui.signonController.show()
			}
			astoria.util.initDefaultView();
			this.checkLocaleMismatch();
			$("button[data-ajaxbutton]").prop("disabled", false).removeClass("disabled");
			this.showCAOverlay();
			this.showIntlOverlay();
			var pageHasSocial = $("[data-social]");
			if (pageHasSocial.length > 0) {
				astoria.util.Social.init()
			}
			$(window).trigger("mainContentLoaded")
		},
		firePopstateIfNecessary: function() {
			if (Modernizr.history && (/firefox|msie|\.net/i).test(navigator.userAgent)) {
				$(window).load(function() {
					$(window).trigger("popstate")
				})
			}
		},
		genericInfoPopups: function() {
			if (/https/.test(location.protocol)) {
				astoria.util.PopupUtils.showInfoPopups("div.pop-register .help-link, div.pop-register .icon-help, #bi-reg-pop .icon-help, #bi-reg-pop .help-link, #reset-password-pop .icon-help, .reservations-link .pop-info", {
					closeOthers: false
				})
			} else {
				astoria.util.PopupUtils.showInfoPopups(".reservations-link .pop-info", {
					closeOthers: false
				})
			}
			astoria.util.PopupUtils.showInfoPopups('div[data-modal="true"], a[data-modal="true"]')
		},
		showCAOverlay: function() {
			var caPop = $("#modal-canada-welcome2");
			var caPopCookie = astoria.util.CookieUtils.read_cookie("showCAOverlay");
			if (caPop.length > 0 && caPopCookie !== "false") {
				astoria.util.PopupUtils.displayCenteredPopup(caPop, {
					closeSelectors: [".icon-close", ".exit", ".exit2", ".exit3"],
					backdrop: true,
					backdropDark: true,
					onClose: function(e) {
						var href = $(e.target).attr("href");
						if (href && href.split("?").length > 1) {
							var pUtils = astoria.util.ParameterUtils,
								pObj = pUtils.toObject(location.search),
								linkLang = pUtils.extractParam("lang", href.split("?")[1]);
							if (linkLang != pObj.lang) {
								pObj.lang = linkLang;
								location.search = pUtils.toSend(pObj)
							}
						}
					}
				});
				astoria.util.CookieUtils.write_cookie("showCAOverlay", false, 3650, true)
			}
		},
		showIntlOverlay: function() {
			var intlPop = $("#modal-intl-welcome");
			var cCode = intlPop.data("country");
			var intlPopCookie = astoria.util.CookieUtils.read_cookie("showIntlModal" + cCode);
			if (intlPop.length > 0 && intlPopCookie !== "false") {
				astoria.util.PopupUtils.displayCenteredPopup(intlPop, {
					closeSelectors: [".icon-close", ".exit"],
					backdrop: true,
					backdropDark: true
				});
				astoria.util.CookieUtils.write_cookie("showIntlModal" + cCode, false, 3650, true)
			}
		},
		checkLocaleMismatch: function() {
			var cookieUtils = astoria.util.CookieUtils,
				popups = astoria.util.PopupUtils,
				popup = $("#pop-country-na");
			if (popup.length > 0) {
				popups.displayCenteredPopup(popup, {
					backdrop: true,
					backdropDark: true
				});
				popup.find("button.btn-continue").click(function() {
					var requestedLocale = popup.find("input[name='new_country']").val();
					cookieUtils.currentCountry(requestedLocale);
					astoria.util.Redirecting.redirectTo(popup.find("input[name='orig_url']").val())
				}).end().find("button.btn-cancel").click(function() {
					popups.closePopups()
				})
			}
		},
		analytics: {
			analyze: $.noop
		},
		tagManager: function() {
			var tagjs = document.createElement("script");
			var s = document.getElementsByTagName("script")[0];
			tagjs.text = "{'site':'2qd6aPY'}";
			tagjs.async = true;
			tagjs.src = "//s.btstatic.com/tag.js";
			s.parentNode.insertBefore(tagjs, s)
		},
		validation: {
			Validator: function(options) {
				var stopOnError = (options || {}).stopOnError;
				this.validators = [];
				options = options || {};
				this.add = function(vtors) {
					if (vtors) {
						if ($.isArray(vtors)) {
							this.validators = this.validators.concat(vtors)
						} else {
							this.validators.push(vtors)
						}
					}
				};
				this.validate = function(env) {
					var i = 0,
						cnt = this.validators.length,
						errors = {};
					if (cnt === 0) {
						throw {
							message: "Must have at least 1 Validator."
						}
					}
					for (; i < cnt; i++) {
						this.validators[i].validate(errors, $.extend({}, options, env));
						if (stopOnError && !$.isEmptyObject(errors)) {
							break
						}
					}
					return errors
				}
			},
			ConfirmValidator: function(original, confirm, options) {
				if ($.isEmptyObject(options)) {
					throw "i18n must be specified."
				}
				options.top = options.top || "body";
				options.ifFn || (options.ifFn = function() {
					return true
				});
				this.validate = function(errors, env) {
					var i18n = astoria.i18n.t(options.i18n || env.i18n),
						field = $(original || env.original, options.top),
						other = $(confirm || env.confirm, options.top),
						name;
					if (options.ifFn() && field.val().trim() !== other.val().trim()) {
						name = other.attr("name");
						errors.field = errors.field || {};
						(errors.field[name] = errors.field[name] || []).push(i18n)
					}
				}
			},
			GenericValidator: function(fld, validFn, opts) {
				opts = opts || {};
				if (!$.isArray(fld)) {
					fld = [fld]
				}
				opts.ifFn = opts.ifFn || function() {
					return true
				};
				var msg = astoria.i18n.t(opts.i18n);
				this.validate = function(errors) {
					var fields = fld.reduce(function(ret, e) {
						return ret.add($(e, opts.scope || opts.top || "body"))
					}, $(""));
					$.each(fields, function(_, field) {
						field = $(field);
						var val = field.val(),
							name;
						if (opts.ifFn() === true && validFn(val) === false) {
							name = field.attr("name");
							errors.field = errors.field || {};
							(errors.field[name] = errors.field[name] || []).push(msg)
						}
					})
				}
			},
			MustBePresentValidator: function(fld, opts) {
				return new astoria.validation.GenericValidator(fld, function(val) {
					if (typeof val === "undefined") {
						return false
					}
					val = $.trim(val);
					var isMinCheck = "minLength" in opts,
						isMaxCheck = "maxLength" in opts,
						ret;
					if (isMinCheck || isMaxCheck) {
						ret = !isMinCheck ? true : (val.length >= opts.minLength);
						return ret && (!isMaxCheck ? true : (val.length <= opts.maxLength))
					} else {
						return val.length > 0
					}
				}, opts)
			},
			DateValidator: function(fld, opts) {
				return new astoria.validation.GenericValidator(fld, function(val) {
					var regDate = "^(0[1-9]{1}|1[0-2]{1}){1}/(0[1-9]|(1|2)[0-9]|3[0-1]){1}/([0-9]{2})$";
					var regExp = new RegExp(regDate);
					return regExp.test(val)
				}, opts)
			},
			FourDigitsValidator: function(fld, opts) {
				return new astoria.validation.GenericValidator(fld, function(val) {
					var regDate = "^([0-9]{4})$";
					var regExp = new RegExp(regDate);
					return regExp.test(val)
				}, opts)
			},
			EmailValidator: function(fld, opts) {
				opts || (opts = {});
				var i18n = astoria.i18n.t(opts.i18nKey || "email.invalid"),
					ifFn = opts.ifFn || function() {
						return true
					};
				this.validate = function(errors, options) {
					if (ifFn() !== true) {
						return
					}
					var field = $(fld, $(options.top || "body")),
						val = field.val(),
						name;
					if (!astoria.util.HtmlUtils.validateEmailAddress(val)) {
						name = field.attr("name");
						errors.field = errors.field || {};
						(errors.field[name] = errors.field[name] || []).push(i18n)
					}
				}
			}
		}
	});
	astoria.module("util");
	astoria.util.initDefaultView = function() {
		astoria.dimensions.setAllHeights();
		astoria.dimensions.setAllWidths();
		$("body").on("click", ".sidebar .sign-in a, .js-sign-in", function(e) {
			closePopups();
			astoria.ui.signonController.show({
				trigger: this
			});
			e.preventDefault()
		});
		$("a#forgotPasswordLink").click(function(e) {
			e.preventDefault();
			var userName = $("#email_returning").val();
			astoria.ui.signonController.show({
				exec: ["initiatePasswordReset", userName]
			})
		});
		$(".sign-in .register, .js-register").click("click", function(e) {
			closePopups();
			astoria.ui.registrationController.show({
				trigger: this
			});
			e.preventDefault()
		});
		if ($(".recent-order").length > 0) {
			new astoria.ui.controller.MyAccountController({
				top: ".recent-order"
			})
		}
		astoria.app.runInitializers();
		if ($("#fb-root").length > 0) {
			astoria.util.initFacebook()
		}
		astoria.util.initComponents();
		$("button[data-ajaxbutton]").prop("disabled", false).removeClass("disabled")
	};
	astoria.util.initProductItem = function(content) {
		var prods = content.find(".product-item");
		if (prods.length > 0) {
			prods.find("span.stars").stars()
		}
	};
	astoria.util.Social = {
		components: [],
		init: function() {
			var type, domain, url, link, image, message, tracking, nextSku;
			var socialTypes = $("div.social-enabled");
			$(socialTypes).each(function() {
				type = $(this).attr("data-socialtype");
				domain = $(this).attr("data-socialurlprefix");
				url = $(this).attr("data-socialurl");
				link = $(this).children("a");
				image = encodeURIComponent(location.protocol + "//www.astoria.com" + $(".hero-main-image img").attr("src"));
				message = $(this).attr("data-socialmessage");
				tracking = $(this).attr("data-socialtracking");
				astoria.util.Social.components.push(type);
				var self = $(this);
				if (typeof(url) === "undefined") {
					url = window.location.href
				}
				if ($("div.content.brand").length > 0) {
					message = $(".content").attr("data-brand") + " #astoria"
				}

				function setSocialLink() {
					if ($("div.content.astoriatv").length > 0 && (type === "pinterest" || type === "twitter")) {
						return
					}
					var socialLink = domain + encodeURIComponent(url);
					if (encodeURIComponent(tracking) !== "" && typeof(tracking) !== "undefined") {
						socialLink += encodeURIComponent(tracking)
					}
					socialLink += "&description=" + encodeURIComponent(message) + "&text=" + encodeURIComponent(message);
					if (image !== "" && typeof(image) !== "undefined") {
						socialLink += "&media=" + image
					}
					$(link).attr("href", "").attr("href", socialLink)
				}

				function updateSocialLink(eleLink) {
					nextSku = $(self).attr("data-socialnextskuid");
					tracking = $(self).attr("data-socialtracking");
					domain = $(self).attr("data-socialurlprefix");
					var updatedUrl = location.protocol + "//" + location.host + location.pathname + "?skuId=" + nextSku;
					var socialSkuColor = "";
					var newMessage = $(self).attr("data-socialmessage");
					image = encodeURIComponent(location.protocol + "//www.astoria.com" + $(".hero-main-image div img").attr("src"));
					if ($("#primarySkuInfoArea .color .value").text() !== "") {
						socialSkuColor = "in " + $("#primarySkuInfoArea .color .value").text()
					}
					if (typeof(newMessage) !== "undefined" && $("#primarySkuInfoArea .color .value").text() !== "") {
						newMessage = newMessage.substring(0, newMessage.indexOf(" in ")) + socialSkuColor;
						newMessage += "  #astoria"
					}
					return (domain + updatedUrl + tracking + "&description=" + encodeURIComponent(newMessage) + "&media=" + image)
				}
				$(link).on("click", function(e) {
					e.preventDefault();
					if ($("body#product").length > 0 && astoria.util.Social.getComponentType(self) === "pinterest") {
						var newLink = updateSocialLink($(this));
						window.open(newLink, "", "width=650,height=350,top=" + ((screen.height / 2) - 175) + ",left=" + ((screen.width / 2) - 325));
						return
					}
					window.open(astoria.util.Social.getComponentLink($(this)), "", "width=650,height=350,top=" + ((screen.height / 2) - 175) + ",left=" + ((screen.width / 2) - 325))
				});
				setSocialLink()
			})
		},
		getComponentLink: function(componentLink) {
			var link = $(componentLink).attr("href");
			return (link)
		},
		getComponentType: function(componentParent) {
			var type = $(componentParent).attr("data-socialtype");
			return (type)
		},
		getComponents: function() {
			return (astoria.util.Social.components)
		},
		getComponentCount: function() {
			return (astoria.util.Social.components.length)
		}
	};
	astoria.util.initComponents = function(context) {
		context = context ? $(context) : $("body");
		astoria.util.initMarquee(context);
		astoria.util.initImageMapTooltip(context);
		$(".carousel", context).carousel();
		$(".videoPopComponent img", context).modalVideo();
		$(".videoComponent .dynamicInfo", context).dynamicOoyala();
		$(".videoComponent img", context).video();
		$(".store-search-component", context).storeSearchComponent();
		new astoria.ui.controller.ShoppingCartController().addToBasket(".copy-component .btn", function(source) {
			return $(source).closest("form").serializeArray()
		});
		if (astoria.ui.controller.BirewardsController && $("#checkout, #basket").length === 0) {
			new astoria.ui.controller.BirewardsController({
				top: $(".bi-rewards-component", context)
			});
			$("button[data-ajaxbutton]").prop("disabled", false).removeClass("disabled")
		}
		astoria.util.initProductItem($(".authored-content", context));
		new astoria.ui.controller.SimpleLovesController({
			scope: "body",
			selector: ".copy-component .icon-love, .sku-grid-component .icon-love"
		});
		astoria.jail.lazyImages($(".modal img[data-src]", context), {
			event: "lazyModal",
			loadHiddenImages: true
		});
		astoria.jail.lazyImages($("[data-lazyload] img[data-src]", context))
	};
	astoria.util.initFacebook = function() {
		var host;
		if (!("fbAsyncInit" in window)) {
			astoria.logger.log("initFacebook invoked");
			host = window.location.host;
			window.fbAsyncInit = function() {
				FB.init({
					appId: "124755580884988",
					channelUrl: "//" + host + "/channel.jsp",
					status: true,
					cookie: true,
					xfbml: true
				})
			};
			(function(d) {
				var js = d.createElement("script");
				js.async = true;
				js.src = "//connect.facebook.net/en_US/all.js";
				(d.head || d.getElementsByTagName("head")[0]).appendChild(js)
			})(document)
		}
	};
	astoria.util.initPinterest = function() {
		astoria.logger.log("initPinterest invoked");
		(function(w, d, load) {
			var script, first = d.getElementsByTagName("body")[0],
				n = load.length,
				i = 0,
				go = function() {
					for (i = 0; i < n; i = i + 1) {
						script = d.createElement("SCRIPT");
						script.type = "text/javascript";
						script.async = true;
						script.src = load[i];
						first.appendChild(script)
					}
				};
			if (w.addEventListener) {
				w.addEventListener("load", go, false)
			} else {
				go()
			}
		}(window, document, ["//assets.pinterest.com/js/pinit.js"]))
	};
	astoria.util.initTwitter = function() {
		! function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (!d.getElementById(id)) {
				js = d.createElement(s);
				js.id = id;
				js.src = "//platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js, fjs)
			}
		}(document, "script", "twitter-wjs")
	};
	astoria.util.initMarquee = function(context) {
		function rotateToNextSection(list, sections, scrollDistance) {
			var clone = $(sections[sections.length - 1]).clone();
			list.prepend(clone);
			list.css("top", "-" + scrollDistance + "px");
			list.animate({
				top: 0
			}, 1000);
			$(sections[sections.length - 1]).remove();
			setTimeout(function() {
				rotateToNextSection(list, list.find(".section"), scrollDistance)
			}, 5000)
		}
		$(".marquee-component", context).each(function() {
			var sections = $(this).find(".section");
			var me = this;
			if (sections.length > 1) {
				setTimeout(function() {
					rotateToNextSection($(me).find(".top-list"), sections, $(me).height())
				}, 5000)
			}
		})
	};
	astoria.util.initImageMapTooltip = function(context) {
		var changeTooltipPosition = function(event) {
			var tooltipX = event.pageX - 8;
			var tooltipY = event.pageY + 8;
			$("div.toolTipBox").css({
				top: tooltipY,
				left: tooltipX
			})
		};
		var showTooltip = function(event) {
			$("div.toolTipBox").remove();
			var tooltip = $(this).attr("tooltip");
			$('<div class="toolTipBox">' + $(this).attr("tooltip") + "</div>").appendTo("body");
			changeTooltipPosition(event)
		};
		var hideTooltip = function() {
			$("div.toolTipBox").remove()
		};
		$("area[tooltip]", context).bind({
			mousemove: changeTooltipPosition,
			mouseenter: showTooltip,
			mouseleave: hideTooltip
		})
	};
	astoria.util.Validates = {
		initValidates: function(options) {
			options = options || {};
			options.top = options.top || "body";
			var self = this,
				errorBuilder = this.errorBuilder || new astoria.ui.error.ErrorBuilder(options);
			$.extend(this, {
				addValidator: function(validator) {
					if (typeof this.validator === "undefined") {
						this.validator = new astoria.validation.Validator(options)
					}
					for (var i = 0, cnt = arguments.length; i < cnt; i++) {
						this.validator.add(arguments[i])
					}
				},
				errorBuilder: errorBuilder,
				ifValid: function(success, env) {
					errorBuilder.show();
					var errors = self.validator.validate(env);
					if (env && ("error" in env)) {
						success = {
							success: success,
							error: env.error
						}
					}
					errorBuilder.withNoErrors(errors, success)
				}
			});
			return self
		}
	};
	astoria.util.Redirecting = {
		redirectTo: function(to) {
			window.location.href = to
		},
		refreshSimple: function(delay) {
			delay ? setTimeout(function() {
				window.location.reload(true)
			}, delay) : window.location.reload(true)
		},
		fatalAjaxError: function() {
			if ($("body#my-account").length) {
				this.redirectToAccountLanding()
			} else {
				if ($("body#checkout").length) {
					this.redirectToBasket()
				} else {
					this.refreshSimple()
				}
			}
		},
		redirectToHome: function() {
			if ($("#secureBody").length === 1) {
				if ("postMessage" in window.parent) {
					window.parent.postMessage(JSON.stringify({
						name: "redirectToHome"
					}), "*")
				}
			} else {
				window.location.href = astoria.conf.basketUrl + "?redirect=homepage"
			}
		},
		redirectToAccountLanding: function() {
			window.location.href = astoria.conf.myAccountLandingUrl
		},
		redirectToBasket: function() {
			window.location.href = astoria.conf.basketUrl
		}
	};
	astoria.util.HtmlUtils = {
		toSecureIFrame: function(uri, fn) {
			var loc = window.location;
			var calcPort = function() {
				return (loc.port.length === 4 ? loc.port.slice(3) : "") + "443"
			};
			return this.createFrame({
				props: {
					src: "https://" + loc.hostname + ":" + calcPort() + uri + (uri.indexOf("?") === -1 ? "?" : "&") + "ts=" + (new Date()).getTime(),
					name: "login",
					"data-location": loc.href
				},
				onLoad: fn
			})
		},
		createFrame: function(config) {
			var IFRAME_PREFIX = "astoria_";
			var HAS_NAME_PROPERTY_BUG;

			function undef(v) {
				return typeof v === "undefined"
			}

			function testForNamePropertyBug() {
				var form = document.body.appendChild(document.createElement("form")),
					input = form.appendChild(document.createElement("input"));
				HAS_NAME_PROPERTY_BUG = input !== form.elements[input.name];
				document.body.removeChild(form)
			}
			if (undef(HAS_NAME_PROPERTY_BUG)) {
				testForNamePropertyBug()
			}
			var frame;
			if (HAS_NAME_PROPERTY_BUG) {
				frame = $('<iframe name="__placeholder__"/>')[0]
			} else {
				frame = document.createElement("IFRAME");
				frame.name = config.props.name
			}
			frame.name = frame.id = config.props.name;
			delete config.props.name;
			if (typeof config.container == "string") {
				config.container = document.getElementById(config.container)
			}
			if (!config.container) {
				config.container = document.body
			}
			var src = config.props.src;
			config.props.src = "javascript:false";
			$.extend(frame, config.props);
			frame.border = frame.frameBorder = 0;
			frame.allowTransparency = true;
			if (config.onLoad) {
				on(frame, "load", config.onLoad)
			}
			frame.src = src;
			$(frame).addClass("secure-frame");
			return $(frame)
		},
		jsonOrHtml: function(data, jsonFn, htmlFn) {
			var json;
			try {
				if ($.isPlainObject(data)) {
					json = data
				} else {
					json = $.parseJSON(data)
				}
			} catch (e) {
				htmlFn.call(this, data)
			}
			if (json) {
				jsonFn(json)
			}
		},
		currentProductId: function() {
			return $(".product-information").attr("id").replace(/product-?/, "")
		},
		productIdFrom: function(e) {
			if ($(e).attr("id") !== undefined) {
				return astoria.util.HtmlUtils.productIdFromId($(e).attr("id"))
			}
		},
		productIdFromId: function(id) {
			return id.replace(/product(-|_)?/, "")
		},
		skuIdFrom: function(e) {
			return astoria.util.HtmlUtils.skuIdFromId($(e).attr("id"))
		},
		paymentInfoIdFrom: function(e) {
			if ($(e).attr("id") !== undefined) {
				return $(e).attr("id").replace(/payment-info-/, "")
			}
			return ""
		},
		skuIdFromId: function(id) {
			return id === undefined ? undefined : id.replace(/sku(-|_)?/, "").replace(/(-|_)(.*)?/, "")
		},
		sampleIdFromId: function(id) {
			return id.replace(/sample-/, "")
		},
		addressIdFrom: function(e) {
			return this.addressIdFromId($(e).attr("id"))
		},
		addressIdFromId: function(id) {
			return id.replace(/address-/, "")
		},
		skuNumberFromId: function(id) {
			return (id.split("-")[1])
		},
		skuTypeFromId: function(id) {
			var regExp = astoria.Sku.EGC_REGEXP || (astoria.Sku.EGC_REGEXP = new RegExp(astoria.Sku.EGC));
			if (regExp.test(id.toLowerCase())) {
				return astoria.Sku.EGC
			} else {
				id = id.replace(/(.*-)/, "");
				return id.toLowerCase()
			}
		},
		controllerFrom: function() {
			var module = $(this).attr("id").replace(/-/g, "");
			return module.substr(0, 1).toUpperCase() + module.substr(1) + "Controller"
		},
		findCheckedSkuIds: function(checkboxes) {
			var skuIds = [];
			checkboxes.each(function() {
				var match = $(this).attr("name").match(/sku-(.*)-product-(.*)/);
				if (match) {
					skuIds.push({
						sku_id: (match[1]),
						product_id: (match[2]),
						quantity: 1
					})
				} else {
					skuIds.push({
						sku_id: (astoria.util.HtmlUtils.skuNumberFromId($(this).attr("id"))),
						quantity: 1
					})
				}
			});
			return skuIds
		},
		enableImageLabelForIE: function(containers) {
			if (astoria.util.HtmlUtils.isBrowserIE()) {
				containers.each(function() {
					var container = $(this);
					container.find("label img").click(function() {
						var checkbox = container.find("input");
						checkbox[0].checked = !checkbox[0].checked
					})
				})
			}
		},
		isBrowserIE: function() {
			return navigator.userAgent.indexOf("MSIE") > 0
		},
		validateEmailAddress: function(address) {
			var filter = /^(?!\.)([a-zA-Z0-9_.+-]%?)+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+(?!\.)$/,
				dots = /\.\./;
			return filter.test(address) && !dots.test(address)
		},
		fillWithValueOrHide: function(topElement, typeSelector, value) {
			if (value) {
				topElement.find(typeSelector + ".value").text(value)
			} else {
				topElement.find(typeSelector).hide()
			}
		},
		getFormDataFromSelector: function(selector) {
			var params = {};
			$(selector).each(function() {
				if ($(this).attr("name")) {
					var type = $(this).attr("type");
					if ("checkbox" == type) {
						if ($(this).attr("checked") === true) {
							params[$(this).attr("name")] = $(this).val()
						}
					} else {
						if ("radio" == type) {
							if ($(this).attr("checked") === true) {
								params[$(this).attr("name")] = $(this).val()
							}
						} else {
							params[$(this).attr("name")] = $(this).val()
						}
					}
				}
			});
			return params
		},
		getFormData: getFormData
	};
	astoria.util.ShoppingCartUtils = {
		initCartUtils: function(success) {
			var self = this;
			self.currency = $("#currency").attr("data-currency");
			self.jst = astoria.template.views2;
			if (success) {
				success()
			}
			$.extend(this, {
				updateItemCount: function(cart) {
					$(".my-astoria .item-count").tap(function(c) {
						if (c.length === 1 && ("item_count" in cart)) {
							c.text(cart.item_count)
						}
					})
				},
				noDisplayVariationTypes: [astoria.util.Product.SC, astoria.util.Product.SCF],
				makeBasketItemFor: function(lineItem, xtra) {
					var sku = lineItem.sku;
					var data = $.extend({}, sku),
						isCheckoutSignin = $(".checkout-module").length === 0;
					data.lineItem = lineItem;
					data.quantity = lineItem.quantity;
					data.isRemove = sku.isBiReward() && !isCheckoutSignin;
					data.currency = this.currency;
					top = astoria.template.views2.miniLineItem(data);
					var top = $(top);
					if ($.isFunction(xtra)) {
						xtra(top)
					}
					return top
				},
				updateFreeShipping: function(cart) {
					var isDiscount = "discount_amount" in cart && cart.discount_amount > 0,
						isMerchTotal = "merchandise_total" in cart && cart.merchandise_total > 0,
						isCheckout = !! $(".calculation").length,
						top = isCheckout ? $(".calculation") : $(".basket-totals"),
						egiftAmt = "egiftcard_amount" in cart ? cart.egiftcard_amount : 0,
						giftAmt = "giftcard_amount" in cart ? cart.giftcard_amount : 0,
						creditAmt = "store_credit_amount" in cart ? cart.store_credit_amount : 0,
						showSub;
					top.find(".merch-total").text(isMerchTotal ? self.currencyFormatted(cart.merchandise_total) : "").end().find(".discount .cost").text(isDiscount ? self.currencyFormatted(cart.discount_amount) : "").end().find(".sub").text(self.currencyFormatted(cart.subtotal));
					if (isCheckout) {
						showSub = ("giftcard_amount" in cart) || ("egiftcard_amount" in cart) || ("store_credit_amount" in cart);
						top.find(".shipping").text(cart.shipping_amount === 0 ? "FREE" : self.currencyFormatted(cart.shipping_amount)).end().find(".tax").text(self.currencyFormatted(cart.tax_amount)).end().find(".cost.total").text(self.currencyFormatted(cart.credit_card_amount)).end().find(".credit.store .cost").text(self.currencyFormatted(creditAmt)).end().find(".credit.gift .cost").text(self.currencyFormatted(giftAmt)).end().find(".credit.egift .cost").text(self.currencyFormatted(egiftAmt)).end().find("li.discount")[isDiscount ? "slideDown" : "slideUp"]().end().find("li:has(span.merch)")[isMerchTotal ? "slideDown" : "slideUp"]();
						$(".currency + .shipping").prev()[cart.shipping_amount === 0 ? "hide" : "show"]();
						top.find(".credit.store")[creditAmt ? "slideDown" : "slideUp"]();
						top.find(".credit.gift")[giftAmt ? "slideDown" : "slideUp"]();
						top.find(".credit.egift")[egiftAmt ? "slideDown" : "slideUp"]();
						top.find("li:has(span.sub)")[showSub ? "slideDown" : "slideUp"]()
					} else {
						top.find(".discount")[isDiscount ? "slideDown" : "slideUp"]().end().find(".total-row:has(span.merch-total)")[isMerchTotal ? "slideDown" : "slideUp"]();
						var promos = $(".top-promo-msg"),
							k, fsmap = {
								FLASH_FREE_SHIP: "free-flash-shipping",
								FLASH_HAZMAT_FREE_SHIP: "free-flash-hazmat-shipping",
								TRESHOLD_FREE_SHIP: "free-shipping",
								ROUGE_FREE_SHIP: "rouge-free-ship",
								ROUGE_HAZMAT_FREE_SHIP: "free-rouge-hazmat-shipping"
							};
						if ("promo_display_type" in cart) {
							k = fsmap[cart.promo_display_type]
						} else {
							if (cart.amount_to_free_shipping <= 0) {
								k = "free-shipping"
							} else {
								k = "free-shipping-with-amount";
								promos.find(".free-shipping-amount").text(this.currencyFormatted(cart.amount_to_free_shipping))
							}
						}
						promos.children().each(function(_, m) {
							m = $(m);
							m[m.hasClass(k) ? "show" : "hide"]()
						})
					}
				}
			})
		}
	};
	astoria.util.MapUtils = {
		makeGeoCoder: function(success) {
			function mapLoaded() {
				success(new google.maps.Geocoder())
			}
			var hasGoogle = typeof(google) !== "undefined";
			if (hasGoogle && ("maps" in google)) {
				success(new google.maps.Geocoder())
			} else {
				if (hasGoogle) {
					google.load("maps", "3", {
						other_params: "sensor=false",
						callback: mapLoaded
					})
				} else {
					$.getScript("//www.google.com/jsapi", function() {
						google.load("maps", "3", {
							other_params: "sensor=false",
							callback: mapLoaded
						})
					})
				}
			}
		},
		longLatFrom: function(address, success, noresults) {
			try {
				this.makeGeoCoder(function(geoCoder) {
					geoCoder.geocode({
						address: address
					}, function(results, status) {
						if (status === "OK" && results.length > 0) {
							var lat = parseFloat(results[0].geometry.location.lat()),
								lg = parseFloat(results[0].geometry.location.lng());
							success(new google.maps.LatLng(lat, lg))
						} else {
							if ($.isFunction(noresults)) {
								noresults(status)
							}
						}
					})
				})
			} catch (e) {
				if ($.isFunction(noresults)) {
					noresults("Bad address >>>>>>> " + e + address)
				}
			}
		},
		mapAddress: function(address, success, noresults) {
			try {
				this.makeGeoCoder(function(geoCoder) {
					geoCoder.geocode({
						address: address
					}, function(results, status) {
						if (status === "OK") {
							success(results)
						} else {
							if ($.isFunction(noresults)) {
								noresults(status)
							}
						}
					})
				})
			} catch (e) {
				if ($.isFunction(noresults)) {
					noresults("Bad address >>>>>>> " + e + address)
				}
			}
		}
	};
	astoria.util.MyList = (function() {
		var myList, inited = false;
		return {
			all: function(opts) {
				inited = true;
				return $.ajax("/rest/user/favorites", {
					data: opts,
					complete: function() {},
					cache: false
				}).done(function(resp) {
					(resp.favorites || []).forEach(function(p) {
						astoria.Sku.mixInEntity(p)
					})
				})
			},
			mixInEntity: function(resp) {
				if ($.isPlainObject(resp)) {
					(resp.favorites || []).forEach(function(p) {
						astoria.Sku.mixInEntity(p)
					})
				}
			},
			isInited: function() {
				return inited
			},
			initFavorites: function(results) {
				var MyList = this,
					CookieUtils = astoria.util.CookieUtils;
				if (!MyList.isInited()) {
					if (CookieUtils.isSignedInOrRecognizedUser()) {
						if (results && ("favorites" in results)) {
							MyList.setMyLoves(results.favorites.map(function(e) {
								return {
									sku_number: e
								}
							}))
						} else {
							MyList.initAll()
						}
					} else {
						MyList.setMyLoves([])
					}
				}
			},
			setMyLoves: function(list) {
				myList = list;
				inited = true
			},
			initAll: (function() {
				return function() {
					if (!inited) {
						inited = true;
						this.ids(function(resp) {
							myList = resp ? resp.map(function(e) {
								return {
									sku_number: e
								}
							}) : []
						}, function() {
							myList = []
						})
					}
				}
			})(),
			ids: function(success, error) {
				$.ajax("/rest/user/favorites/ids", {
					complete: function() {},
					cache: false
				}).done(success).fail(error)
			},
			create: function(skuId, success, error) {
				var url = "/rest/user/favorites/" + skuId;
				var fn = this.memoized(url, "post", function() {
					return astoria.ajax.makeAjaxPost(url, {
						complete: function() {}
					})
				});
				fn(success, error)
			},
			resolveSharedUrl: function(success) {
				$.ajax("/rest/user/favorites/sharelink", {
					dataType: "text",
					cache: false
				}).done(success)
			},
			retrieveSharedFavorites: function(userId, opts) {
				return $.ajax("/rest/users/" + userId + "/favorites", {
					cache: false,
					data: opts
				}).done(this.mixInEntity)
			},
			memoized: (function() {
				var fns = {};
				return function(url, verb, builder) {
					fns[verb] || (fns[verb] = {});
					if (fns[verb][url]) {
						return fns[verb][url]
					} else {
						return (fns[verb][url] = builder())
					}
				}
			})(),
			destroy: function(skuId, success, error) {
				var url = "/rest/user/favorites/" + skuId;
				var fn = this.memoized(url, "delete", function() {
					return astoria.ajax.makeAjaxDelete(url, {
						complete: function() {}
					})
				});
				fn(success, error)
			},
			createAll: function(prodId, success, error) {
				var url = "/rest/user/favorites/products/" + prodId;
				var fn = this.memoized(url, "post", function() {
					return astoria.ajax.makeAjaxPost(url, {
						complete: function() {}
					})
				});
				fn(success, error)
			},
			destroyAll: function(prodId, success, error) {
				var url = "/rest/user/favorites/products/" + prodId;
				var fn = this.memoized(url, "delete", function() {
					return astoria.ajax.makeAjaxDelete(url, {
						complete: function() {}
					})
				});
				fn(success, error)
			},
			isOnMyList: function(skuId, success) {
				function checkMyList() {
					var isOnList = myList.some(function(e) {
						return e.sku_number === skuId
					});
					success(isOnList)
				}
				if (typeof myList === "undefined") {
					var c = this;
					c.ids(function(fromServer) {
						c.normalize(fromServer);
						checkMyList()
					})
				} else {
					checkMyList()
				}
			},
			setCollection: function(loves) {
				myList = loves;
				this.collection = loves
			},
			setModel: function(m) {
				this.model = m
			},
			normalize: function(fromServer) {
				if (!fromServer) {
					return
				}
				if (!$.isArray(fromServer)) {
					fromServer = fromServer.favorites
				}
				myList = fromServer.map(function(e) {
					return {
						sku_number: e
					}
				})
			},
			addListItems: function(myListIn, isAdded, success) {
				if (!$.isArray(myListIn)) {
					myListIn = [myListIn]
				}
				var controller = this,
					add = function() {
						myListIn.forEach(function(newItem) {
							if (!myList.some(function(listItem) {
								return listItem.sku_number === newItem.sku_number
							})) {
								myList.unshift(newItem)
							}
						})
					}, remove = function() {
						var idx, items = myList,
							skuId = myListIn[0].sku_number;
						items.forEach(function(item, i) {
							if (item.sku_number === skuId) {
								idx = i
							}
						});
						if (idx > -1) {
							items.splice(idx, 1)
						}
					};
				if (typeof myList === "undefined") {
					this.ids(function(fromServer) {
						if (!fromServer) {
							return
						}
						if (!$.isArray(fromServer)) {
							fromServer = fromServer.favorites
						}
						myList = fromServer.map(function(e) {
							return {
								sku_number: e
							}
						});
						if (isAdded === true) {
							add()
						} else {
							if (isAdded === false) {
								remove()
							}
						} if ($.isFunction(success)) {
							success.call(controller, myList)
						}
					}, function() {
						myList = [];
						if ($.isFunction(success)) {
							success.call(controller, myList)
						}
					})
				} else {
					if (isAdded === true) {
						add();
						if ($.isFunction(success)) {
							success.call(controller, myList)
						}
					} else {
						if (isAdded === false) {
							remove();
							if ($.isFunction(success)) {
								success.call(controller, myList)
							}
						} else {
							if ($.isFunction(success)) {
								success.call(controller, myList)
							}
						}
					}
				}
			}
		}
	})();
	astoria.MyList = astoria.util.MyList;
	astoria.util.Login = {
		REG_MODAL_IDS: "#register-pop, #bi-reg-pop, #registration",
		loadSignonDialog: function(options, success) {
			if ($("#signin-pop").length === 0) {
				var self = this;
				self.doLoadSignonDialog(function(response) {
					success(response)
				})
			} else {
				success($("#signin-pop"))
			}
		},
		forgotPasswordDialog: function(userName, success) {
			$.get(astoria.conf.resetPasswordResponseUrl, {
				emailAddress: userName
			}).done(success)
		},
		doLoadSignonDialog: function(success) {
			$.get(astoria.conf.loginPopupUrl, success)
		},
		logInUser: astoria.ajax.makeAjaxPost(astoria.conf.loginValidationMsgUrl),
		registration: function(doFn, data, success) {
			if ($.isFunction(data)) {
				success = data;
				data = {}
			}
			doFn(data, function(resp) {
				var isJSON = false;
				try {
					isJSON = $.parseJSON(resp)
				} catch (e) {}
				if ($.isFunction(success)) {
					success(isJSON ? isJSON : resp)
				}
			})
		},
		loadRegistrationDialog: function(data, success) {
			var self = this;
			if ($(this.REG_MODAL_IDS).length > 0) {
				$(this.REG_MODAL_IDS).remove()
			}
			self.registration(self.doRegisterDialog, data, function(dialog) {
				success(dialog)
			})
		},
		doRegisterDialog: function(data, success) {
			$.ajax({
				url: astoria.conf.registrationOrBIDialog,
				type: "POST",
				dataType: "html",
				data: data,
				success: success
			})
		},
		logOut: function(success) {
			$.ajax({
				url: astoria.conf.logoutResponseUrl,
				type: "POST",
				data: astoria.util.HtmlUtils.getFormData("myAccountBoxForm"),
				success: success
			})
		},
		resetPassword: function(data, success, options) {
			$.ajax({
				type: "post",
				url: options.url,
				data: data,
				dataType: "json",
				success: success,
				error: function(xhr, status, errorThrown) {
					if (console && console.log) {
						console.log("error")
					}
				}
			})
		},
		resetPasswordOffline: function(data, success, options) {
			$.ajax({
				type: "post",
				url: options.url,
				data: data,
				dataType: "json",
				success: success,
				error: function(xhr, status, errorThrown) {
					if (console && console.log) {
						console.log("error")
					}
				}
			})
		},
		validateSecurityAnswer: function(data, success, options) {
			$.ajax({
				type: "post",
				url: options.url,
				data: data,
				success: success,
				dataType: "json",
				error: function(xhr, status, errorThrown) {
					if (console && console.log) {
						console.log("error")
					}
				}
			})
		}
	};
	astoria.util.PasswordReset = {
		initPasswordReset: function(options) {
			if ("showScreen" in this) {
				return
			}
			astoria.logger.log("initPasswordReset entering for ", this.instance);
			options = options || {};
			var self = this,
				before = options.before,
				currentScreen, after = options.after,
				loginDialogErrors = options.errorShower || new astoria.ui.error.ErrorBuilder({
					topElement: ".signin-returning"
				}),
				errorBuilder = new astoria.ui.error.ErrorBuilder({
					topElement: "#reset-password-pop",
					globalInsertAfter: ".screen:visible h4"
				}),
				USER_NAME = options.emailField || "label[for='user_name'] + input";
			if (!("displayCenteredPopup" in self)) {
				$.extend(self, astoria.util.PopupUtils)
			}
			$.extend(self, astoria.util.Login, astoria.util.Redirecting, {
				showScreen: function(i) {
					var ret = this.pop.find(".screen").addClass("hidden").end().find(".screen" + i).removeClass("hidden");
					if ("sizeChanged" in self) {
						self.sizeChanged()
					}
					currentScreen = i;
					return ret
				},
				onClose: function() {
					astoria.logger.log("PasswordReset#onClose entering");
					if (currentScreen === 6) {
						self.reloadPage()
					}
				},
				loadDialog: function(userEmail, success) {
					$("body").trigger("layover", [{
						title: "reset password",
						type: "pop-info"
					}]);
					$("#reset-password-pop").remove();
					this.forgotPasswordDialog(userEmail, function(dialog) {
						if ($("body#checkout").length === 1) {
							self.pop = $(dialog);
							self.pop.addClass("modal").append('<span class="icon icon-close"></span>');
							$("body").append(self.pop)
						} else {
							if ($("body#secureBody").length === 0) {
								self.pop = $(dialog);
								self.pop.addClass("modal");
								if (self.pop.find("> .icon-close").length === 0) {
									self.pop.append($('<span class="icon icon-close"></span>'))
								}
								$("body").append(self.pop)
							} else {
								$("body").append(dialog);
								self.pop = $("#reset-password-pop")
							}
						}
						var verifyForm = $("#verifySecAnswer");
						verifyForm.submit(function() {
							var url = $(this).attr("action");
							astoria.util.Login.validateSecurityAnswer(verifyForm.serializeArray(), function(resp) {
								errorBuilder.withNoErrors(resp.errors, {
									success: function() {
										self.showScreen(2);
										$("#resetPasswordForm").find("[name='new_password'], [name='confirm_password']").val("")
									},
									error: function() {
										self.sizeChanged()
									}
								})
							}, {
								url: url
							});
							return false
						});
						$(".forgot-sec-answer").click(function(e) {
							self.showScreen(4);
							e.preventDefault()
						});
						$("#resetPasswordForm").submit(function() {
							var url = $(this).attr("action");
							astoria.util.Login.resetPassword($(this).serializeArray(), function(resp) {
								errorBuilder.withNoErrors(resp.errors, {
									success: function() {
										self.showScreen(6)
									},
									error: function() {
										self.sizeChanged()
									}
								})
							}, {
								url: url
							});
							return false
						});
						$("#resetPasswordOffline, #resetPasswordSecQuestion").submit(function() {
							var url = $(this).attr("action"),
								userName = $(this).find("[name='user_name']").val();
							astoria.util.Login.resetPasswordOffline($(this).serializeArray(), function(resp) {
								errorBuilder.withNoErrors(resp.errors, function() {
									self.showScreen(5).find("span.user-name").text(userName);
									self.sizeChanged()
								})
							}, {
								url: url
							});
							return false
						});
						success()
					})
				},
				initiatePasswordReset: function(name) {
					self.showPasswordReset(name)
				},
				showPasswordReset: function(userName) {
					self.loadDialog(userName, function() {
						if ($.isFunction(before)) {
							before()
						}
						var userNameField = self.pop.find(USER_NAME),
							verifiedUser;
						errorBuilder.show();
						userNameField.val(userName);
						self.pop.find("input[name='security_answer']").val("");

						function showVerifiedUser() {
							self.pop.find("input[name='user_name']").val(verifiedUser.user_name);
							if (verifiedUser.has_security_answer) {
								self.pop.find(".sec-question").text(verifiedUser.security_question)
							} else {
								self.showScreen(3)
							}
						}

						function showWhenReady(s) {
							astoria.util.User.find({
								user_name: userName
							}, function(user) {
								if (!$.isEmptyObject(user)) {
									verifiedUser = user;
									showVerifiedUser();
									s({})
								} else {
									s({
										errors: {
											global: astoria.i18n.t("validation.forgotpassword.user.not.exist")
										}
									})
								}
							})
						}
						showWhenReady(function(resp) {
							if (loginDialogErrors) {
								loginDialogErrors.withNoErrors(resp.errors, {
									success: function() {
										self.displaySecurePopup(self.pop, {
											beforeShow: function() {
												self.showScreen(1)
											},
											onClose: self.onClose
										});
										if ($.isFunction(after)) {
											after()
										}
									},
									error: function() {
										if ("sizeChanged" in self) {
											self.sizeChanged()
										}
									}
								})
							} else {
								astoria.util.PopupUtils.displaySecurePopup(self.pop)
							}
						})
					})
				},
				onResetPassword: function(e) {
					e.preventDefault();
					var loginForm = $(this).closest("form"),
						userName = loginForm.find(USER_NAME).val();
					self.ifValid(function() {
						self.showPasswordReset(userName)
					});
					e.preventDefault()
				}
			});
			on(window, "message", function(event) {
				try {
					var msg = JSON.parse(event.data);
					switch (msg.name) {
						case "close":
							astoria.logger.log("PasswordReset close message received");
							self.onClose();
							break
					}
				} catch (e) {}
			});
			$.extend(this, astoria.util.Validates);
			this.initValidates({
				top: self.scope
			}).addValidator(new astoria.validation.EmailValidator(USER_NAME, {
				i18nKey: "email.invalid"
			}))
		}
	};
	astoria.util.CountrySupport = {
		verifyCountrySwitch: function(country, success) {
			var url = contextPath + "/popups/modalCountrySwitch.jsp?country=" + country;
			$.get(url, success)
		}
	};
	astoria.util.User = {
		create: function(data, success, error) {
			$.ajax(astoria.conf.registrationResponse, {
				type: "POST",
				data: data
			}).done(success).fail(error)
		},
		current: function() {
			return {
				bi_status: $("meta[name='bi_status']").attr("content")
			}
		},
		retrieve: function(success, error) {
			$.ajax(astoria.conf.userUrl, {
				dataType: "json",
				cache: false
			}).done(success).fail(error)
		},
		find: function(data, success) {
			$.ajax(astoria.conf.findUserUrl, {
				data: data,
				dataType: "json",
				cache: false
			}).done(success)
		}
	};
	astoria.Sku = {
		mixInEntity: function(sku) {
			$.extend(sku, astoria.Sku);
			"is_in_stock is_show_product_link is_love_eligible is_gift_wrappable".split(" ").forEach(function(p) {
				if (!(p in sku)) {
					sku[p] = true
				}
			});
			if (!("max_quantity" in sku)) {
				sku.max_quantity = 10
			}
			if (!("sku_type" in sku)) {
				sku.sku_type = "Standard"
			}
		},
		retrieve: function(idsIn, success, extra) {
			var ids, url, extraParam = $.param(extra || {});
			if (!$.isArray(idsIn)) {
				ids = [idsIn]
			} else {
				ids = idsIn
			} if ((ids.length > 1 && astoria.conf.skuIndexUrl) || (astoria.conf.skuIndexUrl && extraParam)) {
				url = astoria.conf.skuIndexUrl + ids.join(",")
			} else {
				url = astoria.conf.skuUrl.replace(/:id/, ids.join(","))
			} if (extraParam) {
				url = url + "&" + extraParam
			}
			$.ajax(url, {
				dataType: "json"
			}).done(success)
		},
		STANDARD: "standard",
		SAMPLE: "sample",
		GWP: "gwp",
		EGC: "e-certificate",
		GC: "gift card",
		BIRTHDAY_GIFT: "birthday gift",
		WELCOME_KIT: "welcome kit",
		COLLECTION: "collection",
		VERTICAL: "vertical",
		URL_BASE: window.location.protocol + "//" + window.location.host,
		isSingle: function() {
			return [astoria.Sku.VERTICAL, astoria.Sku.COLLECTION].indexOf(this.primary_product.product_type) === -1
		},
		isBILevelBI: function() {
			return this.bi_exclusivity_level === "BI"
		},
		isBILevelVIB: function() {
			return this.bi_exclusivity_level === "VIB"
		},
		isBILevelRouge: function() {
			return this.bi_exclusivity_level === "Rouge"
		},
		isBiExclusive: function() {
			return this.bi_exclusivity_level !== "none"
		},
		getBILevelDisplay: (function() {
			var vibMsgShort = astoria.i18n.t("vibrt.vib.short_title");
			var vibMsg = astoria.i18n.t("vibrt.vib.title");
			return function(isLong) {
				if (this.isBILevelRouge()) {
					return "VIB Rouge"
				}
				var ret;
				switch (this.bi_exclusivity_level) {
					case "BI":
						ret = "Beauty Insider";
						break;
					case "VIB":
						ret = isLong ? vibMsg : vibMsgShort;
						break;
					default:
						ret = this.bi_exclusivity_level
				}
				return ret
			}
		})(),
		isUserBILevelQualified: function(userLevel) {
			var userToSkuMatrix = {
				bi_down: {
					none: true,
					bi: false,
					vib: false,
					rouge: false
				},
				non_bi: {
					none: true,
					bi: false,
					vib: false,
					rouge: false
				},
				bi: {
					none: true,
					bi: true,
					vib: false,
					rouge: false
				},
				vib: {
					none: true,
					bi: true,
					vib: true,
					rouge: false
				},
				rouge: {
					none: true,
					bi: true,
					vib: true,
					rouge: true
				}
			};
			return userToSkuMatrix[userLevel][this.bi_exclusivity_level.toLowerCase()]
		},
		isCollOrVertical: function() {
			return [this.VERTICAL, this.COLLECTION].contains(this.primary_product.product_type)
		},
		isShowAddToBasket: function() {
			return this.full_size_sku_number !== undefined || (this.is_in_stock && this.is_show_add_to_basket)
		},
		makeMediaPath: function() {
			var prodId = this.primary_product.id,
				skuNumber = this.sku_number;
			if ("Standard" === this.sku_type && this.bi_type === "None") {
				if (["vertical", "collection"].contains(this.primary_product.product_type)) {
					return this.URL_BASE + "/productimages/product/" + prodId.toLowerCase() + "-main-hero.jpg"
				} else {
					return this.URL_BASE + "/images/sku/s" + skuNumber + "-main-hero.jpg"
				}
			} else {
				return ""
			}
		},
		prodImageFor: astoria.ui.ViewHelper.prodImageFor,
		prodImageForInlineImage: function() {
			return this.prodImageFor(this.inline_image)
		},
		imageFor: function(format) {
			var imgs = this[format].split(" ");
			if (["vertical", "collection"].contains(this.primary_product.product_type)) {
				return imgs.filter(function(img) {
					return img.indexOf("sku/s" + this.sku_number) > -1
				}, this)[0]
			} else {
				return astoria.ui.ViewHelper.prodImageFor(imgs[0])
			}
		},
		isShowProductLink: function() {
			return this.is_show_product_link
		},
		enablePin: function() {
			this.bi_type || (this.bi_type = "None");
			this.piEnabled = this.isShowProductLink() && "Standard" === this.sku_type && this.bi_type === "None";
			if ("Standard" === this.sku_type && this.bi_type === "None") {
				this.fullProdLink = encodeURIComponent((this.fullProductLink()).substring(0, (this.fullProductLink()).length) + "&om_mmc=oth-pinterest-dotcompinbuttons-2012");
				this.mediaPath = this.makeMediaPath();
				this.pinDescription = this.makePinDesc()
			}
		},
		fullProductLink: function() {
			return this.URL_BASE + astoria.ui.ViewHelper.generateProductLink(this)
		},
		productLink: function() {
			var r, prodDetailUrl = astoria.conf.productDetailUrl;
			if (("primary_product" in this) && ("product_url" in this.primary_product)) {
				r = this.primary_product.product_url + "?skuId=" + ("full_size_sku_number" in this ? this.full_size_sku_number : this.sku_number)
			} else {
				if ("full_size_sku_number" in this) {
					r = prodDetailUrl + "?skuId=" + this.full_size_sku_number + (this.isSample() ? "" : "&productId=" + sku.primary_product.id)
				} else {
					r = prodDetailUrl + "?skuId=" + this.sku_number + "&productId=" + this.primary_product.id
				}
			}
			return r
		},
		makePinDesc: function() {
			return encodeURIComponent(this.primary_product.brand_name + " - " + this.primary_product.display_name + "  #astoria")
		},
		isBirthdayGift: function(sku) {
			sku || (sku = this);
			return sku.bi_type && sku.bi_type.toLowerCase() === astoria.Sku.BIRTHDAY_GIFT
		},
		isWelcomeKit: function(sku) {
			sku || (sku = this);
			return sku.bi_type && (new RegExp(astoria.Sku.WELCOME_KIT)).test(sku.bi_type.toLowerCase())
		},
		isSale: function(sku) {
			sku || (sku = this);
			return "sale_price" in sku && sku.sale_price !== 0 && sku.sale_price !== sku.list_price
		},
		isAdditionalSkuDesc: function() {
			return this.bi_type != "None" && this.full_size_sku_number == undefined
		},
		currCountry: undefined,
		isCountryRestricted: function() {
			var currCountry = astoria.util.CookieUtils.currentCountry();
			this.currCountry || (this.currCountry = astoria.Country.displayName(currCountry));
			return this.is_restricted_country || ("restricted_countries" in this && this.restricted_countries.contains(currCountry.toUpperCase()))
		},
		skuInfo: function(sku) {
			return this.skuInfo2(sku.sku_type, sku.is_beauty_insider, sku.bi_type)
		},
		biPointsAsNum: function(sku) {
			return parseInt(/^\d*/.exec(sku.bi_type)[0])
		},
		calcBiPoints: function(sku, qty) {
			var points;
			if (sku.bi_type && !astoria.Sku.isBirthdayGift(sku) && !astoria.Sku.isWelcomeKit(sku) && (points = astoria.Sku.biPointsAsNum(sku))) {
				return (points * qty) + " Points"
			}
		},
		isSample: function() {
			return this.SAMPLE === this.sku_type.toLowerCase()
		},
		isBiReward: function() {
			return this.is_beauty_insider && this.bi_type && this.bi_type.toLowerCase() !== "none"
		},
		isMiniQuickLook: function() {
			return this.isBiReward() || this.isSample() || this.isGwp()
		},
		isBiRewardOrSample: function() {
			return this.isBiReward() || this.isSample()
		},
		isGwp: function() {
			return astoria.Sku.GWP === this.sku_type.toLowerCase()
		},
		isFree: function() {
			return this.isSample() || this.isGwp()
		},
		isNeitherSampleNorGwp: function() {
			return !this.isSample() && !this.isGwp()
		},
		isNotBiRewardNotGwpNotSample: function() {
			return !this.isBiReward() && !this.isGwp() && !this.isSample()
		},
		isChangeableQty: function() {
			return !this.isSample() && !this.isBiReward() && !this.isGwp()
		},
		purchasableQuantities: function() {
			if (!this.isChangeableQty()) {
				return [1]
			}
			var ret = [],
				i = 1;
			if ("max_quantity" in this) {
				while (i <= this.max_quantity) {
					ret.push(i++)
				}
			}
			return ret
		},
		isBirthDay: function() {
			return this.bi_type && this.bi_type.toLowerCase() === astoria.Sku.BIRTHDAY_GIFT
		},
		printSalePrice: function(quantity) {
			quantity || (quantity = this.quantity) || (quantity = 1);
			return this.isSale() ? astoria.ui.ViewHelper.currencyFormatted(quantity * this.sale_price) : 0
		},
		printPrice: function(quantity) {
			quantity || (quantity = this.quantity) || (quantity = 1);
			return this.isSample() || this.isGwp() || this.isWelcomeKit() ? "FREE" : (this.isBiReward() && !this.isBirthDay()) ? this.calcBiPoints(this, quantity) : this.isBirthDay() ? "Birthday Gift" : astoria.ui.ViewHelper.currencyFormatted(quantity * this.list_price)
		},
		inlineProdImage: function() {
			return astoria.conf.prodImgPrefix + this.inline_image
		},
		variationType: function() {
			return this.primary_product.variation_type ? this.primary_product.variation_type.toLowerCase() : undefined
		},
		hasVariationType: function() {
			return this.variationType() != "none"
		},
		varLabel: function() {
			var variationType = this.primary_product.variation_type ? this.primary_product.variation_type.toLowerCase() : "";
			return variationType && !["none", astoria.util.Product.SC, astoria.util.Product.SCF].contains(variationType) ? this.primary_product.variation_type : ""
		},
		skuInfo2: function(skuType, isBi, biType) {
			var Sku = astoria.Sku,
				ret = {
					isSample: Sku.SAMPLE === skuType.toLowerCase(),
					isBirthday: biType && biType.toLowerCase() === astoria.Sku.BIRTHDAY_GIFT,
					isWelcomeKit: biType && biType.toLowerCase() === astoria.Sku.WELCOME_KIT,
					biReward: isBi && biType && biType.toLowerCase() !== "none",
					isGwp: Sku.GWP === skuType.toLowerCase(),
					isEgc: Sku.EGC === skuType.toLowerCase(),
					isGc: Sku.GC === skuType.toLowerCase()
				};
			return $.extend(ret, {
				isFree: ret.isSample || ret.isGwp || ret.isBirthday
			})
		},
		emailWhenInStock: astoria.ajax.makeAjaxPost(astoria.conf.registerBackInStockEmailUrl + "registerEmailWhenInStock.jsp"),
		removeEmailWhenInStock: astoria.ajax.makeAjaxPost(astoria.conf.registerBackInStockEmailUrl + "unRegisterEmailWhenInStock.jsp")
	};
	astoria.util.Product = {
		url: "/rest/products",
		SIZE: "size",
		SC: "sc",
		SCF: "scf",
		VARIATION_NONE: "none",
		productUrl: function() {
			return this.product_url + "?skuId=" + this.derived_sku.sku_number
		},
		isMoreThan1Color: function() {
			return this.more_colors > 1
		},
		organicSealPath: function() {
			return astoria.template.meta.image_path + "/seal-organic-grid.png"
		},
		naturalSealPath: function() {
			return astoria.template.meta.image_path + "/seal-naturals-grid.png"
		},
		bestOfastoriaPath: function() {
			return astoria.template.meta.image_path + "/seal-best-of-astoria-grid.png"
		},
		exclusiveOnlineLimited: function() {
			var ar = [];
			if (this.is_astoria_exclusive && this.is_limited_edition && this.is_online_only) {
				return "exclusive Â· limited edition"
			}
			if (this.derived_sku.is_online_only) {
				ar.push("online only")
			}
			if (this.derived_sku.is_astoria_exclusive) {
				ar.push((ar.length > 0 ? " Â· " : "") + "exclusive")
			}
			if (this.derived_sku.is_limited_edition) {
				ar.push((ar.length > 0 ? " Â· " : "") + "limited edition")
			}
			return ar.join("")
		},
		retrieve: function(prodId, success) {alert(123);
			var url = "/rest/products/" + prodId;

			// BaoNam 13 02 2014

			// $.ajax(url).done(function(p) {
			// 	$.extend(p, astoria.Product)
			// }).done(success)
		},
		find: function(crit, success) {
			astoria.logger.log("Product.find entering", crit);
			var toSend = astoria.util.ParameterUtils.toSend;
			var url = ((typeof(crit.id) !== "undefined") ? (crit.id) : "") + "?";
			delete crit.id;
			url += toSend(crit);

			// BaoNam 13 02 2014

			// $.ajax(url).done(function(p) {
			// 	if (1==1 || "display_name" in p) {
			// 		$.extend(p, astoria.Product)
			// 	}
			// }).done(function(resp) {
			// 	if (1==1 || "products" in resp) {
			// 		resp.products.forEach(function(p) {
			// 			$.extend(p, astoria.Product);
			// 			astoria.Sku.mixInEntity(p.derived_sku)
			// 		})
			// 	}
			// 	astoria.Category.mixInEntity(resp.categories)
			// }).done(success)
		}
	};
	astoria.util.storage = {
		ProductStorage: function() {
			var productData = {};
			var self = this;
			self.productFor = function(productId, fn, options) {
				var data = {};
				$.extend(data, options || {});
				if (!fn || $.isFunction(fn)) {
					var product = productData[productId];
					if (!product) {
						astoria.util.Product.retrieve(productId, data, function(product) {
							self.productFor(productId, product);
							if ($.isFunction(fn)) {
								fn.call(product)
							}
						})
					} else {
						if ($.isFunction(fn)) {
							fn.call(product)
						}
						return product
					}
				} else {
					var product = fn;
					productData[productId] = product
				}
			}
		},
		loadProductQuickView: function(data, success) {
			var skuId = data.skuId;
			delete data.skuId;
			$.get(astoria.conf.quickLookUrl.replace(/:id/, skuId), data, success)
		},
		loadSkuQuickView: function(id, success) {
			$.get(astoria.conf.miniQuickLookUrl.replace(/:id/, id), success)
		},
		SKUStorage: function() {
			var self = this,
				queue = [],
				skuData = astoria.util.storage.SKUStorage.skuData || (astoria.util.storage.SKUStorage.skuData = {}),
				fns = [];

			function storeCallable(ids, fn) {
				if ($.isFunction(fn)) {
					fns.push(function() {
						processResults(ids, fn)
					})
				}
			}

			function fireCallbacks() {
				if (queue.length === 0) {
					var toFire = [].concat(fns);
					fns = [];
					while (toFire.length > 0) {
						toFire.shift()()
					}
				}
			}

			function ifNotInQueue(ids, success) {
				var found;
				$.each(ids, function(n, id) {
					found = queue.findFirst(function(_, item) {
						return item === id
					});
					if (!found) {
						success(id)
					}
				})
			}

			function cleanQueueAndStore(skus) {
				skus.forEach(function(sku) {
					queue.splice(queue.indexOf(sku.sku_number), 1);
					astoria.Sku.mixInEntity(sku);
					self.skuFor(sku.sku_number, sku)
				})
			}

			function doSet(id, sku) {
				skuData[(id)] = sku
			}

			function checkAndNormalize(id) {
				if ($.isArray(id) && id.length === 0) {
					$.error("checkAndNormalize must be called with a non-empty Array or a string id.")
				}
				return $.isArray(id) ? id : [id]
			}

			function processResults(ids, success) {
				var skus = [];
				$.each(ids, function(n, id) {
					skus.push(skuData[id])
				});
				success.call(skus.length === 1 ? skus[0] : skus)
			}
			$.extend(self, {
				skuFor: function(id, fn, extra) {
					var isSet = $.isPlainObject(fn);
					if (isSet) {
						doSet(id, fn)
					} else {
						var ids = checkAndNormalize(id);
						storeCallable(ids, fn);
						var toSubmit = [];
						ifNotInQueue(ids, function(id) {
							if (!skuData[id]) {
								toSubmit.push(id);
								queue.push(id)
							}
						});
						if (toSubmit.length === 0) {
							fireCallbacks()
						} else {
							astoria.Sku.retrieve(toSubmit, function(skus) {
								cleanQueueAndStore($.isArray(skus) ? skus : [skus]);
								fireCallbacks()
							}, $.extend(extra || {}, self.extras || {}))
						}
					}
				},
				flush: function() {
					skuData = {}
				}
			})
		}
	};
	astoria.app.initializer(astoria.i18n.init);
	astoria.Product = astoria.util.Product;
	astoria.User = astoria.util.User;
	Function.prototype.createAuthenticatingInterceptor = function createInterceptor(ctx, opts) {
		var theFn = this;
		return function() {
			var args = Array.prototype.slice.call(arguments, 0),
				success, fail, cnt = 0,
				isRedirect = (opts || {}).redirect !== false,
				that = ctx || window,
				beforeSend = function(jqXHR) {
					jqXHR.complete(function() {
						astoria.util.Redirecting.refreshSimple()
					})
				}, failOverride = function(xhr) {
					if (xhr.status === 401 && (++cnt === 1)) {
						var opts = /https/.test(window.location.protocol) ? {
							refresh: false,
							success: function() {
								isRedirect && (args[3] ? args[3].beforeSend = beforeSend : args.push({
									beforeSend: beforeSend
								}));
								theFn.apply(that, args)
							}
						} : {
							marshalled: function() {
								theFn.apply(that, args);
								isRedirect ? $(document).one("ajaxComplete", function() {
									astoria.util.Redirecting.refreshSimple()
								}) : astoria.ui.signonController.hide()
							}
						};
						astoria.ui.signonController.show(opts)
					} else {
						if ($.isFunction(fail)) {
							fail(xhr)
						}
					}
				};
			if ($.isFunction(args.slice(-1)[0]) && $.isFunction(args.slice(-2, -1)[0])) {
				success = args.slice(-2, -1)[0];
				fail = args.slice(-1)[0]
			} else {
				success = args.slice(-1)[0]
			} if (typeof(success) === "undefined" || !$.isFunction(success)) {
				throw "Unable to detect success function"
			}
			var newArgs = [].concat(args);
			if ($.isFunction(fail)) {
				newArgs.pop()
			}
			newArgs.push(failOverride);
			theFn.apply(that, newArgs)
		}
	};
	astoria.util.MyList.create = astoria.util.MyList.create.createAuthenticatingInterceptor(astoria.util.MyList);
	astoria.util.MyList.createAll = astoria.util.MyList.createAll.createAuthenticatingInterceptor(astoria.util.MyList);
	astoria.util.MyList.destroy = astoria.util.MyList.destroy.createAuthenticatingInterceptor(astoria.util.MyList);
	astoria.util.MyList.destroyAll = astoria.util.MyList.destroyAll.createAuthenticatingInterceptor(astoria.util.MyList);
	astoria.util.PopupUtils = {};
	astoria.util.PopupUtils.decorate = function(obj, method) {
		var self = this,
			loc = window.location,
			isIFrame = !/https/.test(loc.protocol),
			iframe, fn = obj[method],
			dialogId, enc = encodeURIComponent;
		obj[method] = function() {
			var args = Array.prototype.slice.call(arguments, 0),
				success = args.pop(),
				onearg, options = "";
			var popup;
			if (!isIFrame) {
				args.push(function(dialog) {
					popup = $(dialog).first();
					if ($("body#secureBody").length === 0) {
						popup.addClass("modal")
					}
					popup.appendTo("body");
					if (success) {
						success(popup)
					}
				});
				fn.apply(obj, args)
			} else {
				dialogId = /load(.*)Dialog/.exec(method)[1];
				$("#securePopup").remove();
				popup = $('<div id="securePopup" class="modal"></div>');
				popup.appendTo("body").append($('<span class="icon icon-close"></span>'));
				if (onearg = $.extend({}, args.shift())) {
					for (var name in onearg) {
						if (onearg.hasOwnProperty(name) && $.isFunction(onearg[name])) {
							delete onearg[name]
						}
					}
					options = $.param(onearg)
				}
				popup.prepend($(iframe = astoria.util.HtmlUtils.toSecureIFrame(astoria.conf.securePopup + "?dialog=" + dialogId + "&redirectTo=" + enc(window.location.href) + "&" + options)));
				iframe.height(0);
				if (!Modernizr.postmessage) {
					popup.find(".icon-close:first").unbind("click").bind("click", function() {
						$(".modal:visible").fadeOut()
					})
				}
				var msg = "message";
				var listener = function(event) {
					try {
						var msg = JSON.parse(event.data);
						astoria.logger.log("received postMessage ", msg.name);
						switch (msg.name) {
							case "height":
								iframe.height(msg.height);
								self.displaySecurePopup(popup);
								break;
							case "fadeOut":
								$("#securePopup").fadeOut();
								break;
							case "signin":
								if ((("success" in obj) ? !obj.success() : true) && ("finalize" in obj)) {
									obj.finalize()
								}
						}
					} catch (e) {}
				};
				off(window, msg, listener);
				on(window, msg, listener)
			}
		}
	};
	astoria.util.PopupUtils.fadeOut = function(dialog) {
		dialog = $(dialog);
		if (dialog.hasClass("modal")) {
			dialog.fadeOut()
		} else {
			if ("postMessage" in parent) {
				parent.postMessage(JSON.stringify({
					name: "fadeOut"
				}), "*")
			}
		}
	};
	astoria.util.PopupUtils.sendToSecurePopup = Modernizr.postmessage ? function(msg) {
		if ($("iframe#login")[0]) {
			astoria.logger.log("sending ", msg, " to iframe.");
			$("iframe#login")[0].contentWindow.postMessage(JSON.stringify({
				name: msg
			}), "*")
		}
	} : $.noop;
	astoria.app.initializer(function() {
		$("body").on(Modernizr.touch ? "touchstart" : "click", function() {
			astoria.util.PopupUtils.sendToSecurePopup("hideInfoPopup")
		})
	});
	astoria.util.PopupUtils.displaySecurePopup = function(show, opts) {
		show = $(show);
		opts || (opts = {});
		opts = $.extend({
			closeOthers: true,
			noReattach: true
		}, opts);
		var self = this,
			process = function() {
				if (show.hasClass("modal")) {
					if (show.position().top < 0 || !show.is("#securePopup")) {
						if (Modernizr.postmessage && $("iframe#login")[0]) {
							opts.onClose = function() {
								$("iframe#login")[0].contentWindow.postMessage(JSON.stringify({
									name: "close"
								}), "*")
							}
						}
						self.displayCenteredPopup(show, opts)
					}
				} else {
					show.siblings().hide();
					if (opts.beforeShow) {
						opts.beforeShow()
					}
					self.sizeChanged()
				}
			};
		process()
	};
	astoria.util.PopupUtils.sizeChanged = function() {
		if (window.parent !== window && "postMessage" in parent) {
			var h = $("body").height();
			parent.postMessage(JSON.stringify({
				name: "height",
				height: h
			}), "*")
		}
	};
	astoria.util.PopupUtils.reloadPage = function() {
		if (window.parent !== window) {
			if ("postMessage" in parent) {
				parent.postMessage(JSON.stringify({
					name: "reloadPage"
				}), "*")
			}
		} else {
			astoria.util.Redirecting.refreshSimple()
		}
	};
	var onPostMessage = function(fn) {
		var msg = "message";
		var listener = function(event) {
			try {
				var msg = JSON.parse(event.data);
				astoria.logger.log("received postMessage ", msg.name);
				fn(msg.name)
			} catch (e) {}
		};
		off(window, msg, listener);
		on(window, msg, listener)
	};
	astoria.util.PopupUtils.displayCenteredPopup = function(popup, opts) {
		opts = opts || {};
		astoria.logger.log("PopupUtils.displayCenteredPopup");
		popup = $(popup);
		if (opts.noReattach !== true) {
			$("body").append(popup.detach())
		}
		$.isFunction(opts.closeOthers) ? $(opts.closeOthers.call(popup)).find(".icon-close").click() : opts.closeOthers ? closePopups() : 1;
		if (opts.beforeShow) {
			opts.beforeShow()
		}
		var posY = astoria.dimensions.scrollY(),
			Z = "z-index";
		var topPlacement = ($(window).height() / 2) + posY - (popup.outerHeight(true) / 2);
		if (topPlacement < 0) {
			topPlacement = 0
		}
		popup.css({
			top: topPlacement + "px",
			marginLeft: -(popup.outerWidth() / 2) + "px"
		});
		if (popup.hasClass("modal-fixed")) {
			popup.css({
				marginTop: -(popup.outerHeight() / 2) + "px"
			})
		}
		var afterDisplay = opts && $.isFunction(opts.afterDisplay) ? opts.afterDisplay : function() {};
		popup.fadeIn(afterDisplay);

		function close(e) {
			astoria.logger.log("PopupUtils.displayCenteredPopup close");
			$(".modal-backdrop").remove();
			var onClose = opts && $.isFunction(opts.onClose) ? opts.onClose : function() {};
			popup.fadeOut(function() {
				onClose(e)
			})
		}
		onPostMessage(function(name) {
			if (name === "hideInfoPopup") {
				close()
			} else {
				if (name === "reloadPage") {
					astoria.util.Redirecting.refreshSimple()
				}
			}
		});
		if (opts.backdrop) {
			if ($(".modal-backdrop").length === 0) {
				$("body").append($("<div class='modal-backdrop'></div>"))
			}
			if (opts.backdropDark) {
				$(".modal-backdrop").addClass("modal-backdrop-dark")
			}
			if (popup.css(Z) !== "auto") {
				var newZ = parseInt(popup.css(Z)) - 1;
				$(".modal:visible").each(function(_, div) {
					if (div !== popup[0] && $(div).css(Z) !== "auto" && parseInt($(div).css(Z)) >= newZ) {
						$(div).css(Z, newZ - 1)
					}
				});
				$(".modal-backdrop").css(Z, newZ)
			}
			if (!Modernizr.touch) {
				$(".modal-backdrop").unbind("click").click(close)
			}
		}
		var closeSelectors = [".icon-close"];
		if ($.isArray(opts.closeSelectors)) {
			closeSelectors = opts.closeSelectors
		}
		popup.find(closeSelectors.join(",")).off("click").on("click", function(e) {
			e.preventDefault();
			close(e)
		});
		$("img[data-src]", popup).trigger("lazyModal.jail");
		return popup
	};
	astoria.util.PopupUtils.showInfoPopups = function(selector, opts) {
		opts = $.extend({
			closeOthers: true,
			backdrop: true
		}, opts || {});
		if ($("body").length === 0) {
			throw {
				message: "You can only call this after DOM-ready."
			}
		}
		$("body").on("click", selector, function(e) {
			e.preventDefault();
			var t = $(this),
				popId = $(e.currentTarget).attr("rel"),
				popup = $(popId),
				invoke = t.attr("data-beforeshow");
			if (popup.length === 1) {
				if (typeof invoke !== "undefined") {
					var p, parts = invoke.split(".");
					opts.beforeShow = window[parts.shift()];
					while (p = parts.shift()) {
						opts.beforeShow = opts.beforeShow[p]
					}
				}
				astoria.util.PopupUtils.displayCenteredPopup(popup, opts);
				$("body").trigger("layover", [{
					title: popup.find("h2"),
					type: "pop-info",
					ict: popup.data("ict")
				}])
			}
		})
	};
	astoria.util.PopupUtils.closePopups = closePopups;
	astoria.module("ui.controller");
	astoria.ui.controller.ShoppingCartController = function(options) {
		options || (options = {});
		var self, scope = options.scope || options.top,
			inlineBasket, successFn, errorBuilder, biStatus = $("meta[name='bi_status']").attr("content").toLowerCase();
		this.biTierMatrix = {
			none: 0,
			bi: 1,
			vib: 2,
			rouge: 3
		};
		this.addToBasket = function(selector, resolveFn) {
			self = this;
			$.extend(self, astoria.ShoppingCart, astoria.util.MyList);
			if ($(".checkout-module").length === 0) {
				errorBuilder = inlineBasket = new astoria.ui.controller.InlineBasketController(options)
			}
			$.extend(self, astoria.util.ShoppingCartUtils, astoria.ui.ViewHelper);
			self.initCartUtils();
			$(scope || this.scope || "body").on("click", selector, self, self.cartEvent);
			$(selector, scope || this.scope || "body").filter("[data-is_in_stock!='false']").not("[data-custom-enable]").prop("disabled", false);
			if ($.isFunction(resolveFn)) {
				self.resolveItems = resolveFn
			}
			return this
		};
		this.onSuccessfulAdd = function(success) {
			successFn = success;
			return this
		};
		this.showErrorsWith = function(builder) {
			errorBuilder = builder;
			return this
		};
		this.updateBeautyBankPoints = function(cart) {
			$(".pts-available").text(cart.available_bipoints);
			$(".pts-earned .pts-amount").text("+ " + cart.subtotal);
			$(".pts-redeemed .pts-amount").text("- " + cart.redeemed_bipoints)
		};
		this.updateBIButtons = function(availablePoints, context, biStatus) {
			$(".bi-rewards-component .product-item[points!=0]:not(.spacer), .beauty-insider-rewards .product-item[points!=0]:not(.spacer)", context).each(function(_, prod) {
				prod = $(prod);
				var rewardPoints = parseInt(prod.attr("points")),
					button = prod.find(".btn-add-to-basket"),
					tier = prod.data("tier").toLowerCase();
				button.disableIt(rewardPoints > availablePoints || self.biTierMatrix[tier] > self.biTierMatrix[biStatus])
			})
		};
		this.insertBasketItemAt = astoria.ui.controller.BasketMixin(".my-basket .products", ".product-row").insertBasketItemAt;
		this.cartEvent = function(e) {
			e.preventDefault();
			e.stopPropagation();
			self.source = $(this);
			var items = [],
				source = self.source,
				isCollection = self.isCollection();
			self.isAddAllToMyList = source.hasClass("btn-love-all");
			items = self.resolveItems(source, items);
			if ($.isEmptyObject(items)) {
				return
			}
			var process = function() {
				self.source.disableUntilAjaxComplete();
				var fn = $.isArray(items) ? {
					ctx: self,
					m: "add"
				} : {
					ctx: astoria.ShoppingCart,
					m: "addItems"
				};
				fn.ctx[fn.m](items, function(shoppingCart) {
					self.updateBeautyBankPoints(shoppingCart);
					self.updateBIButtons(shoppingCart.available_bipoints, "body", biStatus);
					var successPath = function() {
						var lineItems = shoppingCart.line_items,
							item, points;
						astoria.analytics.analyze("addToBasket", [shoppingCart.line_items, shoppingCart.spend_amount_for_next_segment, shoppingCart.vib_status]);
						if (inlineBasket) {
							inlineBasket.start($.extend({}, shoppingCart, {
								line_items: shoppingCart.line_items
							}))
						} else {
							lineItems.forEach(function(lineItem, i) {
								$("#sku" + lineItem.sku.sku_number + ".product-row").tap(function(t) {
									if (t.length) {
										item = t;
										points = astoria.Sku.calcBiPoints(lineItem.sku, lineItem.quantity);
										if (points) {
											var qty = item.find(".qty .value");
											item.find(".list-price .price").text(points);
											qty.addClass("updated");
											setTimeout(function() {
												qty.removeClass("updated")
											}, 2000)
										}
									} else {
										self.makeBasketItemFor(lineItem, function(nitem) {
											self.insertBasketItemAt(nitem);
											item = nitem;
											nitem.slideDown(function() {
												$(".my-basket .product-list").animate({
													scrollTop: nitem.position().top
												}, 1000, "easeOutCubic")
											})
										})
									}
									item.find(".qty .value").text(lineItem.quantity)
								})
							})
						}
						self.updateItemCount(shoppingCart)
					};
					if (errorBuilder) {
						errorBuilder.withNoErrors(shoppingCart.errors, successPath, shoppingCart);
						if ("errors" in shoppingCart) {
							self.updateItemCount(shoppingCart)
						}
					} else {
						if (!shoppingCart.errors) {
							successPath()
						}
					} if ($.isFunction(successFn)) {
						successFn(source, shoppingCart, false, isCollection)
					}
				}, {
					include_skus: true,
					e: e
				})
			};
			if (self.validator) {
				if (!errorBuilder) {
					throw "If you use a validator, you must also use an ErrorBuilder."
				}
				errorBuilder.show();
				var errors = self.validator.validate({
					isCollection: isCollection
				});
				errorBuilder.withNoErrors(errors, process)
			} else {
				process()
			}
		};
		this.removeLineitems = function(lineitems, success, options) {
			var items = $.isArray(lineitems) ? lineitems : [lineitems],
				skuId = items.findFirst(function() {
					return this.name.match(/sku_id/)
				}).value;
			astoria.ShoppingCart.remove(lineitems, function(cart) {
				function process() {
					$("#sku" + skuId).slideUp(function() {
						$(this).remove()
					});
					self.updateBeautyBankPoints(cart);
					self.updateItemCount(cart);
					success(cart)
				}
				if (errorBuilder) {
					errorBuilder.withNoErrors(cart.errors, process)
				} else {
					process()
				}
			}, options)
		};
		this.isCollection = function() {
			return $(this.source).hasClass("btn-buy-the-collection")
		}
	};
	astoria.ui.controller.EmailWhenBackInStockController = function() {
		var overlay = $("#email-stock");
		var OOS_EMAIL_TEXT = "data-oos-email-text";
		var DIALOG_TITLE = "e-mail me when available";
		var DIALOG_TITLE_INSTOCK = "e-mail me when in-stock";
		var CHANGE = "change in stock e-mail";
		var controller;
		var self, scope, origin, emailSku;
		var errorBuilder = new astoria.ui.error.ErrorBuilder({
			topElement: "#email-stock",
			globalInsertBefore: ".messages p:first"
		});
		var emailText = function(sku, text) {
			return text ? text : sku.is_coming_soon ? astoria.COMING_SOON_EMAIL : astoria.INSTOCK_EMAIL
		}, onEmailWhenBackInStockNotification = function(_, isRemove) {
				var sku = emailSku,
					text;
				if (isRemove && $.isFunction(self.swatchFor)) {
					self.swatchFor(sku.sku_number).removeAttr(OOS_EMAIL_TEXT)
				}
				text = isRemove ? (self.emailLinkText ? self.emailLinkText(sku) : emailText(sku)) : sku.is_coming_soon ? "change 'coming soon' e-mail" : CHANGE;
				origin.html(text);
				if ($.isFunction(self.storeButtonText)) {
					self.storeButtonText(sku.sku_number, text)
				}
			};
		var onEmailMe = function(e) {
			var email = $(this).closest("form").find("input[name='email']").val(),
				isRemove = $(this).hasClass("btn-remove-email-reminder"),
				method = isRemove ? "removeEmailWhenInStock" : "emailWhenInStock",
				skuId = astoria.util.HtmlUtils.skuIdFrom(this);
			var data = {
				skuId: skuId
			}, success = function(response) {
					errorBuilder.withNoErrors(response.errors, function() {
						$("#email-stock").find(".messages").slideUp(function() {
							$(this).addClass("notice").html(isRemove ? "<p>You are no longer scheduled to receive the email when in stock notification.</p>" : "<p>Your in stock notification email will be sent                                                     to " + email + ".</p>").slideDown()
						}).end().find(".container, .email-me").slideUp();
						$("body").trigger("backInStockEmailNotification", [isRemove])
					})
				};
			if (!isRemove) {
				data.email = email
			}
			astoria.Sku[method](data, success);
			return false
		};
		return {
			registerEmailStock: function(selector) {
				self = controller = this;
				scope = this.scope || this.top || this.infoSettings.top;
				selector || (selector = this.infoSettings.notifyEmail);
				if (("infoSettings" in this) && "swatches" in this.infoSettings) {
					$.extend(this, {
						emailLinkText: function(sku) {
							var text = this.swatchFor(sku.sku_number).attr(OOS_EMAIL_TEXT);
							return emailText(sku, text)
						},
						updateEmailWhenBackInStock: function(sku) {
							var link = $(this.infoSettings.notifyEmail, scope).withoutAncillary(),
								isShow = !sku.is_in_stock && sku.is_available_for_email;
							link.text(this.emailLinkText(sku)).add(link.prev())[isShow ? "show" : "hide"]()
						},
						swatchFor: function(skuNumber) {
							return $(this.infoSettings.swatches, scope).filter("#sku-" + skuNumber)
						},
						storeButtonText: function(skuNumber, text) {
							this.swatchFor(skuNumber).attr(OOS_EMAIL_TEXT, text)
						}
					})
				}
				if (!("currentSku" in self)) {
					self.currentSku = function(origin) {
						return {
							sku_number: $(origin).attr("data-sku_number")
						}
					}
				}
				if (!("skuForEmail" in self)) {
					self.skuForEmail = self.currentSku
				}
				return $(scope).on("click", selector, self, function(e) {
					e.preventDefault();
					$("body").one("backInStockEmailNotification", self, onEmailWhenBackInStockNotification);
					origin = $(this);
					emailSku = controller.skuForEmail(origin);
					overlay.find(".modal-body").loadEmailStockContent(emailSku.sku_number, function() {
						astoria.util.PopupUtils.displayCenteredPopup(overlay, {
							closeOthers: true
						});
						$(".btn-email-me, .btn-add-reminder, .btn-change-address, .btn-remove-email-reminder", overlay).on("click", onEmailMe);
						if (("is_in_stock" in emailSku) || ("is_coming_soon" in emailSku)) {
							$("#email-stock .modal-title").text(emailSku.is_coming_soon ? DIALOG_TITLE : DIALOG_TITLE_INSTOCK)
						} else {
							controller.currentSku(origin, function(sku) {
								$("#email-stock .modal-title").text(sku.is_coming_soon ? DIALOG_TITLE : DIALOG_TITLE_INSTOCK)
							})
						}
						$("body").trigger("layover", [{
							title: "email when in stock"
						}])
					})
				})
			}
		}
	};
	astoria.ui.controller.BasketMixin = function(scope, row) {
		return {
			insertBasketItemAt: function(nitem) {
				nitem = $(nitem);
				nitem.hide();
				var type, inserted = false,
					orank, i = 0,
					items = $(row, scope).toArray(),
					item, length = items.length,
					rank = function(skuType, isBi, biType) {
						var info = astoria.Sku.skuInfo2(skuType, isBi, biType),
							points = astoria.Sku.biPointsAsNum({
								bi_type: biType
							});
						return /flash/i.test(skuType) ? 210 : info.isSample ? 0 : info.isGwp ? 1 : info.isEgc ? 2 : info.isGc ? 3 : info.isBirthday ? 4 : (info.biReward && !info.isWelcomeKit) ? (5000 / points) : (info.biReward && info.isWelcomeKit) ? 220 : 200
					}, nrank;
				if (length === 0) {
					$(scope).prepend(nitem);
					astoria.logger.log("inserted before")
				} else {
					nrank = rank(nitem.attr("data-skutype"), nitem.attr("data-isbi") === "true", nitem.attr("data-bitype"));
					do {
						item = $(items[i]);
						orank = rank(item.attr("data-skutype"), item.attr("data-isbi") === "true", item.attr("data-bitype"));
						astoria.logger.log("BasketMixin old rank ", orank, "new rank ", nrank);
						if (nrank > orank) {
							nitem.insertBefore(item);
							astoria.logger.log("inserted at " + i);
							inserted = true;
							break
						}
					} while (++i < length);
					if (!inserted) {
						astoria.logger.log("inserted after ");
						$(scope).append(nitem)
					}
				}
				return i
			}
		}
	};
	astoria.ui.controller.BiFormHelper = {
		initBiFormHelper: function(scope) {
			var selects = $("[name='birth_year'], [name='birth_range']", scope || this.scope || this.top);
			selects.change(function() {
				var clicked = $(this),
					other;
				if (clicked.val() !== "") {
					other = selects.filter(function() {
						return $(this)[0] !== clicked[0]
					});
					other.find("option:first").attr("selected", true);
					other.find("option:first").attr("selected", true)
				}
			})
		}
	};
	astoria.ui.controller.RegistrationController = function() {
		if (astoria.ui.registrationController) {
			return astoria.ui.registrationController
		}
		var self = this,
			jsonResult, submittedForm, registerDialog, registerErrorBuilder, options, onSuccess;
		self.top = astoria.util.Login.REG_MODAL_IDS;

		function ifVerifiedUser(cont) {
			if (self.isPosMode()) {
				cont()
			} else {
				var userName = registerDialog.find("[name='user_name']").val();
				astoria.util.User.find({
					user_name: userName
				}, function(user) {
					if (!$.isEmptyObject(user) && user.is_pos_member) {
						self.changeToPos(user.user_name);
						user.is_subscribed_to_email = true;
						registerDialog.find("form").populate(user)
					} else {
						cont()
					}
				})
			}
		}
		$.extend(self, astoria.util.HtmlUtils, astoria.util.Validates, astoria.util.Redirecting, astoria.ui.controller.BiFormHelper, astoria.util.PopupUtils, {
			show: function(settings) {
				var data, info = {
						type: "up"
					};
				if ($.isFunction(settings)) {
					onSuccess = settings;
					options = {
						refresh: true,
						success: onSuccess
					}
				} else {
					options = $.extend({
						refresh: true
					}, settings || {});
					data = options.data;
					onSuccess = options.success
				} if (options.trigger) {
					info.spot = $(settings.trigger).data("analytics")
				}
				astoria.analytics.analyze("layover", [{
					title: "register",
					type: info
				}]);
				self.loadRegistrationDialog(data, function() {
					self.init();
					self.showDialog(data)
				})
			},
			loadRegistrationDialog: function(data, fn) {
				astoria.util.Login.loadRegistrationDialog(data, fn)
			},
			finalize: function(user) {
				if (options.refresh) {
					self.refreshSimple()
				}
			},
			changeToPos: function(userName) {
				if (userName) {
					registerDialog.find(".email-section").hide().end().find(".subscribe-section").hide().end().find(".tc-agree").attr("disabled", true).end().find(".subscribe-to-email").val("true").end().find(".subscribe-to-email-checked").prop("checked", true).end().find(".pos").removeClass("hidden").end().find("span.user-name").text(userName);
					$("input.tc-agree", registerDialog).attr("checked", true);
					$("#is_pos_member").val(true)
				} else {
					registerDialog.find(".email-section").show().end().find(".subscribe-section").show().end().find(".tc-agree").removeAttr("disabled").end().find(".subscribe-to-email").val("false").end().find(".subscribe-to-email-checked").prop("checked", false).end().find(".pos").addClass("hidden").end().find("span.user-name").text("").end().find("form").clearForm();
					$("input.tc-agree", registerDialog).attr("checked", false);
					$("#is_pos_member").val(false)
				}
			},
			isPosMode: function() {
				return !registerDialog.find(".email-section").is(":visible")
			},
			isBIMode: function() {
				return registerDialog.find("button.btn-join-beauty-insider").length > 0
			},
			success: function(user) {
				if ($.isFunction(onSuccess)) {
					onSuccess(user)
				}
				return $.isFunction(onSuccess)
			},
			processRegistration: function() {
				ifVerifiedUser(function() {
					var data = submittedForm.serializeArray(),
						biReg = submittedForm.parents("#bi-reg-pop").length > 0,
						posReg = $("#is_pos_member").val() === "true";
					if (posReg || registerDialog.find("[name='tc_agree']:checked").prop("disabled")) {
						data.push({
							name: "tc_agree",
							value: true
						})
					}
					if (registerDialog.find("[name='is_subscribed_to_email_check']:checked").prop("disabled")) {
						data.push({
							name: "is_subscribed_to_email",
							value: true
						})
					}
					if (biReg) {
						data.push({
							name: "is_store_bi_member",
							value: true
						})
					}
					var onAjax = function(user, status, xhr) {
						if ([200, 201].contains(xhr.status)) {
							user = (user && typeof user === "string") ? JSON.parse(user) : !user ? {} : user;
							registerErrorBuilder.withNoErrors(user.errors, {
								success: function() {
									self.fadeOut(registerDialog);
									self.success(user);
									astoria.analytics.analyze("register", [user], true);
									if ("finalize" in self) {
										self.finalize(user)
									}
								},
								error: function() {
									self.sizeChanged()
								}
							})
						} else {
							self.sizeChanged()
						}
					};
					astoria.User.create(data, onAjax, onAjax)
				})
			},
			onFormSubmit: function() {
				submittedForm = $(this);
				registerErrorBuilder.show();
				if (self.isBIMode()) {
					self.processRegistration()
				} else {
					self.ifValid(self.processRegistration, {
						error: function() {
							self.sizeChanged()
						}
					})
				}
				return false
			},
			showDialog: function(data) {
				registerDialog = $(self.top);
				this.initBiFormHelper();
				var form = registerDialog.find("form");
				if (!data) {
					registerErrorBuilder.show();
					form.clearForm()
				} else {
					if (data.is_bi_signup) {
						data.tc_agree = true;
						registerDialog.find("div.sign-up").addClass("bi-first").prepend(registerDialog.find("div.join-bi")).end().find("[name='tc_agree'], [name='is_subscribed_to_email_check']").prop("disabled", true)
					}
					if (data.is_pos_member) {
						self.changeToPos(data.user_name)
					}
					form.populate(data)
				}
				$(".subscribe-to-email-checked").prop("checked", true);
				$(".subscribe-to-email").val("true");
				self.displaySecurePopup(self.top, {
					onClose: function() {
						astoria.util.PopupUtils.closePopups()
					}
				})
			},
			init: function() {
				if ($(self.top).length !== 1) {
					throw {
						message: "RegistrationController called with no scope"
					}
				}
				$("#register-pop").on("click", "#subscribe-to-catalog", function() {
					if (this.checked) {
						$("#user-reg-step3").slideDown()
					} else {
						$("#user-reg-step3").slideUp()
					}
				});
				$(".tc-agree").on("change", function() {
					if ($(this).prop("checked")) {
						$(".subscribe-to-email-checked").prop("checked", true);
						$(".subscribe-to-email").val(true)
					}
					$(".subscribe-to-email-checked").prop("disabled", $(this).prop("checked"))
				});
				$(".subscribe-to-email-checked").change(function() {
					$(".subscribe-to-email").val($(this).prop("checked"))
				});
				$("#register-pop, #bi-reg-pop").on("click", ".clear-pos", function(e) {
					self.changeToPos();
					e.preventDefault()
				});
				$("#register-pop").on("click", ".btn-cancel", function() {
					$("#register-pop").fadeOut()
				});
				$("#register-pop form, #bi-reg-pop form").submit(self.onFormSubmit);
				self.initValidates({
					top: self.top,
					globalInsertBefore: $(".form-start", self.scope).length ? ".form-start" : "form"
				}).addValidator(new astoria.validation.EmailValidator("input[name='user_name']", {
					i18nKey: "email.invalid"
				}), new astoria.validation.MustBePresentValidator(["input[name='first_name']"], {
					scope: self.top,
					i18n: "user.firstName.missing"
				}), new astoria.validation.MustBePresentValidator(["input[name='last_name']"], {
					scope: self.top,
					i18n: "user.lastName.missing"
				}), new astoria.validation.FourDigitsValidator("input[name='security_answer']", {
					top: self.top,
					i18n: "social.security.last4digits.invalid",
					ifFn: function() {
						return $("option:selected[value*='4 digits']", self.top).length > 0
					}
				}), new astoria.validation.DateValidator("input[name='security_answer']", {
					top: self.top,
					i18n: "date.invalid",
					ifFn: function() {
						return $("option:selected[value*='birthday']", self.top).length > 0
					}
				}), new astoria.validation.MustBePresentValidator(["input[name='password']"], {
					top: self.top,
					minLength: 6,
					maxLength: 12,
					i18n: "user.password.format.error"
				}), new astoria.validation.ConfirmValidator("[name='password']", "[name='password_confirmation']", {
					i18n: "user.password.mismatch"
				}), new astoria.validation.MustBePresentValidator(["input[name='security_answer']"], {
					top: self.top,
					i18n: "errors.general"
				}));
				registerErrorBuilder = self.errorBuilder
			}
		});
		astoria.app.initializer(function() {
			if ($(self.top).is("#registration")) {
				self.init()
			}
		})
	};
	astoria.ui.controller.SignonController = function() {
		if (astoria.ui.signonController) {
			return astoria.ui.signonController
		}
		var tryExecute = function(methodAndArgs) {
			if (!$.isArray(methodAndArgs)) {
				return
			}
			var method = methodAndArgs[0];
			$.isFunction(self[method]) && self[method].apply(self, methodAndArgs.slice(1))
		};
		var self = this,
			jsonResult, errorBuilder, newUserErrorBuilder, dialog, options, onSuccess, userNameSel = "#signin-pop form:first input:text:visible";
		this.instance = "SignonController";
		astoria.ui.signonController = this;
		astoria.logger.log("creating SignonController");
		self.scope = "#signin-pop";
		self.errorBuilder = new astoria.ui.error.ErrorBuilder({
			topElement: self.scope,
			globalInsertBefore: "form:first div:first"
		});
		$.extend(self, astoria.util.CookieUtils, astoria.util.ParameterUtils, astoria.util.PasswordReset, astoria.i18n, astoria.util.PopupUtils, {
			doResolveLoginData: function(form) {
				var map = {};
				var data = astoria.util.HtmlUtils.getFormData(form);
				$.each(data, function(key, value) {
					if (key != "ForgotPasswordLink" && key != "_D:ForgotPasswordLink") {
						map[key] = value
					}
				});
				return map
			},
			hide: function() {
				dialog ? self.fadeOut(dialog) : self.closePopups()
			},
			show: function(settings) {
				settings || (settings = {});
				var info = {
					type: "in"
				};
				if (settings.trigger) {
					info.spot = $(settings.trigger).data("analytics");
					delete settings.trigger
				}
				astoria.analytics.analyze("layover", [{
					title: "sign in",
					type: info
				}]);
				if (settings.exec) {
					return tryExecute(settings.exec)
				}
				if ($.isFunction(settings)) {
					onSuccess = settings;
					options = {
						refresh: true,
						success: onSuccess
					}
				} else {
					options = $.extend({
						refresh: true
					}, settings || {});
					onSuccess = options.success
				} if (dialog) {
					self.displaySecurePopup(dialog);
					errorBuilder.show();
					if (self.isRecognizedUser()) {
						astoria.util.User.retrieve(function(user) {
							$("#signin-pop #user_name").val(user.user_name)
						})
					} else {
						$("form", dialog).clearForm()
					}
					newUserErrorBuilder.show()
				} else {
					initDialog()
				}
			},
			loadSignonDialog: function(fn) {
				astoria.util.Login.loadSignonDialog(options, fn)
			},
			success: function(user) {
				if ($.isFunction(onSuccess)) {
					onSuccess()
				}
				if ("marshalled" in options) {
					astoria.logger.log("#success sending 'marshalled' to iframe");
					options.marshalled();
					delete options.marshalled;
					return true
				} else {
					return $.isFunction(onSuccess)
				}
			},
			finalize: function() {
				if (options.refresh) {
					astoria.util.Redirecting.refreshSimple()
				}
			}
		}, astoria.util.HtmlUtils);
		self.initPasswordReset({
			after: function() {
				dialog ? dialog.fadeOut() : 0
			}
		});

		function initDialog() {
			self.loadSignonDialog(options, function() {
				dialog = $("#signin-pop");
				$(".reset-password", self.scope).click(self.onResetPassword);
				errorBuilder = self.errorBuilder;
				newUserErrorBuilder = new astoria.ui.error.ErrorBuilder({
					topElement: $(".signin-new", dialog),
					globalInsertBefore: "form:last div:first"
				});
				dialog.find("form:first").submit(function() {
					$("input:visible", $(this)).trim();
					$("#signin-pop [data-ajaxlink], .my-astoria [data-ajaxlink]").disableUntilAjaxComplete();
					astoria.util.Login.logInUser(self.doResolveLoginData($(this)), function(user) {
						errorBuilder.withNoErrors((user || {}).errors, {
							success: function() {
								if (user.is_store_bi_member) {
									astoria.ui.registrationController.show($.extend(options, {
										data: user,
										success: onSuccess
									}))
								} else {
									self.fadeOut(dialog);
									astoria.analytics.analyze("signIn", [user], true);
									if ($.isFunction(onSuccess)) {
										onSuccess()
									}
									if ("finalize" in self) {
										self.finalize()
									}
								}
							},
							error: function() {
								self.sizeChanged()
							}
						})
					});
					return false
				});
				dialog.find("form:last").submit(function(e) {
					$("input:visible", $(this)).trim();
					var userName = $("[name='user_name']", $(this)).val();
					$("#signin-pop [data-ajaxlink], .my-astoria [data-ajaxlink]").disableUntilAjaxComplete();
					astoria.util.User.find({
						user_name: userName
					}, function(found) {
						newUserErrorBuilder.withNoErrors((found || {}).errors, {
							success: function() {
								if (!found || $.isEmptyObject(found)) {
									dialog.fadeOut();
									astoria.ui.registrationController.show($.extend(options, {
										data: {
											user_name: userName
										}
									}))
								} else {
									if (found.is_store_bi_member || found.is_pos_member) {
										dialog.fadeOut();
										astoria.ui.registrationController.show($.extend(options, {
											data: found
										}))
									} else {
										newUserErrorBuilder.withNoErrors({
											global: self.t("user.registration.emailAlreadyExists", userName)
										})
									}
								}
							},
							error: function() {
								self.sizeChanged()
							}
						})
					});
					return false
				});
				astoria.util.PopupUtils.decorate(self, "forgotPasswordDialog");
				self.displaySecurePopup(dialog);
				tryExecute(options.exec)
			})
		}
		$(".my-astoria a.sign-out, .my-astoria a.recognized-prompt").click(function(e) {
			astoria.util.Login.logOut(function() {
				$("body").trigger("pageCall", ["sign out"]);
				$("body").trigger("logout");
				if ($(".user").length > 0) {
					window.location.href = astoria.conf.myAccountLandingUrl
				} else {
					if ($(".checkout-module").length > 0) {
						window.location.href = astoria.conf.contextPath ? astoria.conf.contextPath : "/"
					} else {
						astoria.util.Redirecting.refreshSimple()
					}
				}
			});
			e.preventDefault()
		})
	};
	astoria.ui.controller.InlineBasketController = function(conf) {
		conf = conf || {};
		var self = this,
			uid, myastoriaHeight = parseFloat($(".my-astoria").outerHeight(true)),
			basket = $(".inline-basket"),
			basketItems = $(".inline-basket .basket-line-items"),
			msgClone = astoria.template.views2.inlineBasketMsg,
			currency = $("#currency").attr("data-currency");
		this.instance = "InlineBasketController";
		this.clone = $(".product-row-to-clone").clone().removeClass("product-row-to-clone hidden");
		$.extend(self, astoria.util.HtmlUtils, astoria.util.ShoppingCartUtils, astoria.ui.ViewHelper, {
			addToErrors: function(errors) {
				var i, fields, texts = [],
					newMsg;
				if (errors.global) {
					fields = $.isArray(errors.global) ? errors.global : [errors.global];
					for (i = 0; i < fields.length; i++) {
						texts.push(fields[i])
					}
				} else {
					if (errors.field) {
						fields = $.isArray(errors.field) ? errors.field : [errors.field];
						for (i = 0; i < fields.length; i++) {
							$.each(fields[i], function(k, v) {
								texts.push(v)
							})
						}
					}
				} if (texts.length) {
					newMsg = $(msgClone({}));
					newMsg.append(astoria.ui.error.makeError(texts.join("<br>")));
					$(".inline-basket-msg", basket).replaceWith(newMsg)
				}
				if (("renderFieldError" in this) && errors.field) {
					this.renderFieldError()
				}
			},
			show: function() {
				$(".inline-basket-msg", basket).find("p.error-message").remove()
			}
		}, conf);
		self.initCartUtils();

		function closeAll() {
			$(document).unbind("click." + uid);
			if (self.timeoutId) {
				clearTimeout(self.timeoutId)
			}
			$(".inline-basket,.inline-basket-msg").stop(true);
			$(".inline-basket-msg", basket).slideUp(1000, function() {
				basket.animate({
					top: -(parseFloat(basket.css("height")) - myastoriaHeight)
				}, 1500)
			})
		}

		function animateInlineBasket(isError) {
			$(".inline-basket, .inline-basket-msg").stop(true);
			basket.css({
				top: -(parseFloat(basket.css("height")) - myastoriaHeight),
				right: ($(window).width() - parseFloat($(".wrapper").css("width"))) / 2,
				visibility: "visible"
			});
			if (isError) {
				$(".inline-basket-msg", basket).slideDown(1500, function() {
					self.timeoutId = setTimeout(closeAll, 3000)
				})
			} else {
				basket.animate({
					top: myastoriaHeight + "px"
				}, 2000).delay(500).queue(function() {
					$(".inline-basket-msg", basket).slideDown(1500, function() {
						self.timeoutId = setTimeout(closeAll, 3000)
					})
				})
			}
		}

		function addToBasket(cart) {
			var message, newMsg;
			$("h2", basket).html("just added to basket");
			$(".inline-basket .product-row").remove();
			basket.find("h2").removeClass("list").end().find(".variants").show().end().find(".actions .btn-my-list").hide().end().find(".actions .btn-checkout").show();
			basketItems.empty();
			cart.line_items.forEach(function(lineItem) {
				basketItems.append(self.makeBasketItemFor(lineItem, function(scope) {
					scope.find(".qty .value").text(lineItem.quantity)
				}).show());
				basket.css({
					top: -(parseFloat(basket.css("height")) + 34)
				})
			});
			if ("errors" in cart) {
				return
			}
			if (message = cart.statusMessage("vib_status")) {
				newMsg = $(msgClone({
					clazz: "msg-vib-state" + (cart.isRouge() ? " msg-rouge" : ""),
					message: message
				}))
			} else {
				if (message = cart.statusMessage("promo_display_type")) {
					newMsg = $(msgClone({
						header: "free shipping!",
						message: message
					}))
				} else {
					newMsg = $(msgClone({}));
					newMsg.find("h4").text("free shipping!").end().find("p").append(cart.amount_to_free_shipping > 0 ? "Add <b>" + currency + '<span class="amount-to-free-shipping">' + self.currencyFormatted(cart.amount_to_free_shipping) + "</span></b> to your order to qualify." : "You now qualify for FREE STANDARD SHIPPING!")
				}
			}
			$(".inline-basket-msg", basket).replaceWith(newMsg)
		}

		function prepareInline(afterPrepare) {
			uid = Math.random().toString().slice(10);
			$(document).bind("click." + uid, function(ev) {
				if (!$(ev.target).parents(".inline-basket").length) {
					closeAll()
				}
			});
			$(".inline-basket-msg", basket).find("p.error-message").remove();
			$(".inline-basket-msg", basket).hide().children().hide();
			afterPrepare()
		}
		this.start = function(cart) {
			prepareInline(function() {
				addToBasket(cart);
				animateInlineBasket(("errors" in cart) && !("line_items" in (cart || {})))
			})
		};
		this.withNoErrors = function(errors, success, cart) {
			analyze("anyError", errors);
			if ($.isEmptyObject(errors)) {
				success()
			} else {
				prepareInline(function() {
					var isItems = cart && "line_items" in cart;
					self.addToErrors(errors);
					if (isItems) {
						addToBasket(cart)
					}
					animateInlineBasket(!("line_items" in (cart || {})))
				})
			}
		};
		$(".icon-close", basket).on("click", closeAll);
		$("a.btn-continue-shopping", basket).on("click", function(e) {
			if ($(this).attr("href") === "#") {
				e.preventDefault();
				closeAll()
			}
		})
	};
	astoria.ui.controller.StoreSearchController = function(options) {
		var scope = options.scope ? $(options.scope) : $("body");
		var forms = $("form", scope);
		var prevVal = astoria.util.CookieUtils.read_cookie("storeSearch");
		if (prevVal) {
			forms.find("input[name='location']").val(prevVal)
		}
		forms.submit(function(e) {
			e.preventDefault();
			var form = $(this);
			var val = form.find("input[name='location']").val();
			astoria.analytics.analyze("sendData", ["stores", "search", {
				criteria: val,
				location: options.analytics
			}], true);
			astoria.util.MapUtils.mapAddress(val, function(results) {
				astoria.util.CookieUtils.write_cookie("storeSearch", val);
				if (results.length == 1) {
					var data = {
						lng: results[0].geometry.location.lng(),
						lat: results[0].geometry.location.lat(),
						loc: results[0].address_components[0].long_name,
						radius: form.find("[name='radius']").val(),
						unit: form.find("[name='unit']").val()
					};
					location.href = form.attr("action") + "?" + astoria.util.ParameterUtils.toSend(data)
				} else {
					if (sessionStorage) {
						var list = [];
						$(results).each(function() {
							var info = {
								lng: this.geometry.location.lng(),
								lat: this.geometry.location.lat(),
								loc: this.address_components[0].long_name,
								radius: form.find("[name='radius']").val(),
								unit: form.find("[name='unit']").val()
							};
							list.push({
								href: "?" + astoria.util.ParameterUtils.toSend(info),
								name: this.formatted_address
							})
						});
						sessionStorage.cityInfos = JSON.stringify(list);
						location.href = form.attr("action") + "?cityInfosSize=" + results.length
					} else {
						location.href = form.attr("action") + "?noSessionStorage=1&cityInfosSize=" + results.length
					}
				}
			}, options.error)
		})
	};
	astoria.ui.widgets = {};
	astoria.ui.controller.resolveItems = resolveItems;
	astoria.ui.controller.addToBasket = function(source) {
		return astoria.ShoppingCart.makeItems(source.attr("data-sku_number"))
	};
	astoria.ui.product = {};
	astoria.ui.error = $.extend(astoria.ui.error || {}, {
		makeError: function(msg, opts) {
			opts || (opts = {});
			return '<p class="error-message' + ("clazz" in opts ? (" " + opts.clazz) : "") + '">' + msg + "</p>"
		},
		ErrorBuilder: function(options) {
			options.topElement = options.topElement || options.top;
			var settings = $.extend({
				topElement: "body",
				globalInsertBefore: "form div:first",
				entityName: ""
			}, options || {});
			var self = this;
			self.scope = settings.topElement;
			var globalInsertBefore = settings.globalInsertBefore;
			var addGlobalError = function(error) {
				if (!$.trim(error)) {
					return
				}
				var msg = typeof error === "string" ? $(astoria.ui.error.makeError(error)) : error;
				if (typeof settings.insertGlobalInto !== "undefined") {
					$(settings.insertGlobalInto, $(settings.topElement)).append(msg)
				} else {
					if (typeof settings.globalInsertAfter !== "undefined") {
						msg.insertAfter($(settings.globalInsertAfter, $(settings.topElement)))
					} else {
						msg.insertBefore($(globalInsertBefore, $(settings.topElement)))
					}
				}
			};

			function addFieldError(error, key) {
				return self.renderFieldError(error, key, settings)
			}
			this.renderFieldError = settings.renderFieldError || function(error, key) {
				var whereTo = $("[name='" + key + "']", this.scope),
					toAppend = $('<p class="field-error">' + error + "</p>");
				if (!whereTo.length) {
					whereTo = $("#" + key, this.scope)
				}
				if (whereTo.length) {
					toAppend.insertBefore(whereTo);
					whereTo.addClass("error")
				} else {
					whereTo = $("div[id='sku-" + key + "']", this.scope).find(".item-error");
					if (whereTo.length) {
						whereTo.append($('<p class="error-message">' + error + "</p>"))
					} else {
						addGlobalError(toAppend)
					}
				}
			};

			function addError(msg, key, errorType) {
				if (errorType === "section") {
					return
				}
				var noglobal = false;
				if (errorType === "field") {
					noglobal = addFieldError(msg, key)
				}
				if (noglobal !== true) {
					addGlobalError(msg)
				}
			}
			this.withNoErrors = function(errors, noErrorsFn) {
				$(".error-message, .field-error", $(settings.topElement)).remove();
				$(".error", $(settings.topElement)).removeClass("error");
				var successFn, errorFn;
				if ($.isFunction(noErrorsFn)) {
					successFn = noErrorsFn
				} else {
					if ($.isPlainObject(noErrorsFn)) {
						successFn = noErrorsFn.success;
						errorFn = noErrorsFn.error
					}
				} if (!$.isEmptyObject(errors)) {
					$.each(errors, function(group, value) {
						if ($.isPlainObject(value)) {
							$.each(value, function(fieldId, msg) {
								if ($.isArray(msg)) {
									msg.forEach(function(err) {
										addError(err, fieldId, group)
									})
								} else {
									addError(msg, fieldId, group)
								}
							})
						} else {
							if ($.isArray(value)) {
								$.each(value, function(i, error) {
									addError(error)
								})
							} else {
								addError(value)
							}
						}
					});
					$(".error-message", $(settings.topElement)).show();
					if (typeof errorFn !== "undefined") {
						errorFn()
					}
					$("body").trigger("errorMessages", errors)
				} else {
					if (typeof successFn !== "undefined") {
						successFn()
					}
				}
			};
			this.show = this.clear = function() {
				$(settings.topElement).clearError();
				$(".field-error", settings.topElement).remove();
				$(".error-message", settings.topElement).remove()
			}
		}
	});
	if (typeof astoria.ui.error.productImage === "undefined") {
		$.extend(astoria.ui.error, {
			productImage: function(img, src) {
				if ($(img).data("src") == undefined) {
					if (img.tried === undefined) {
						img.tried = true;
						img.src = src
					}
				}
			}
		})
	}
	astoria.ui.registrationController = new astoria.ui.controller.RegistrationController;
	astoria.util.PopupUtils.decorate(astoria.ui.registrationController, "loadRegistrationDialog");
	on(window, "message", function(event) {
		try {
			var msg = JSON.parse(event.data);
			if (msg.name === "redirectToHome") {
				astoria.util.Redirecting.redirectToHome()
			}
		} catch (e) {}
	});
	astoria.ui.controller.SimpleLovesController = astoria.ui.controller.CarouselController = function(options) {
		this.scope = $(options.scope);
		var scope = this.scope,
			sel = options.selector || "a.icon-love, button.btn-love";
		$.extend(this, astoria.ui.controller.SupportsLoves);
		this.isCarousel = options.isCarousel || false;
		if (this.isCarousel) {
			this.updateLoveButton = function(sku, button, isLoved) {
				var buttons = this.scope.find(sel).filter("[data-sku_number='" + sku.sku_number + "']");
				buttons[isLoved ? "addClass" : "removeClass"]("loved")
			}
		}
		this.love(sel).enableTooltips();
		this.instance = this.isCarousel ? "CarouselController" : "SimpleLovesController"
	};
	$(function() {
		astoria.init()
	})
})(jQuery);
(function($) {
	var quickLookController, miniQLookController;
	$.fn.stars = function() {
		$(this).each(function() {
			var val = parseFloat($(this).html());
			val = val > 5 ? 5 : (val < 0 ? 0 : val);
			var size = 11 * val;
			var stars = $('<span class="stars"><span></span></span>');
			stars.find("span").width(size);
			$(this).replaceWith(stars)
		})
	};
	$.fn.loadEmailStockContent = function(skuId, callback) {
		this.load(astoria.conf.productEmailStockUrl.replace(/:id/, skuId), callback);
		return this
	};
	$.extend(astoria.ui.product, {
		initQuickLook: function() {
			astoria.ui.controller.QuickLookController.usesQL.call({
				scope: "body"
			});
			if ($("#quick-look").length) {
				quickLookController = new astoria.ui.controller.QuickLookController("#quick-look");
				quickLookController.detectPrepareData()
			}
			if ($("#mini-quick-look").length) {
				miniQLookController = new astoria.ui.controller.QuickLookController("#mini-quick-look");
				miniQLookController.registerHandlers()
			}
		},
		ProductView: function(settings, skuStorage) {
			var self = this,
				prodId = $("#current-product").attr("data-id"),
				timer, selectedSku, previousSku, pinUrlDefault;
			previousSku = $("#primarySkuInfoArea .value.OneLinkNoTx").text();
			pinUrlDefault = $(".share-btn.pin.social-enabled");
			if (typeof prodId !== "undefined") {
				skuStorage.extras = {
					product_id: prodId
				}
			}
			$.extend(self, astoria.ui.ViewHelper, astoria.util.HtmlUtils, {
				skuEvent: function(skuId, skuIndex, event) {
					var controller = this;
					if (event.type === "mouseenter" || event.type === "mouseover") {
						if (timer) {
							clearTimeout(timer);
							timer = undefined
						}
						this.nearbySkuIds(skuIndex, function(ids) {
							skuStorage.skuFor(ids)
						})
					} else {
						if (event.type === "click") {
							if (self.selectedSku !== undefined) {
								previousSku = self.selectedSku
							}
							skuStorage.skuFor(skuId, function() {
								controller.updateAll(this);
								controller.rememberSku(this);
								self.selectedSku = skuId
							});
							if (pinUrlDefault.length > 0) {
								$(pinUrlDefault).attr("data-socialnextskuid", skuId);
								$(pinUrlDefault).attr("data-socialpreviousskuid", previousSku)
							}
						}
					}
				},
				nearbySkuIds: function(idx, fn) {
					var IN_ROW = 10,
						skuId, swatches = $(this.infoSettings.swatches, this.infoSettings.top),
						cnt = swatches.length,
						ids = [];
					for (var index = idx - IN_ROW; index <= idx + IN_ROW; index = index + IN_ROW) {
						var indexInRow = index % IN_ROW,
							first = Math.max(indexInRow - 2, 0),
							last = Math.min(indexInRow + 2, IN_ROW - 1),
							start = index + first - indexInRow,
							end = Math.min(index + last - indexInRow, cnt - 1);
						for (var i = start; i <= end && i > -1; i++) {
							skuId = this.skuIdFrom(swatches[i]);
							ids.push(skuId)
						}
					}
					if (ids.length && $.isFunction(fn)) {
						fn.call(this, ids)
					}
				},
				updateOnAdd: function(cart, _, isCollection) {
					if (!isCollection) {
						if (cart.line_items) {
							this.updateInStock(cart.line_items[0].sku);
							skuStorage.skuFor(cart.line_items[0].sku.id, cart.line_items[0].sku)
						}
					}
				},
				updateInStock: function(sku) {
					var settings = this.infoSettings;
					var newFn = function(sku) {
						var outDiv = $(settings.top + " .out-of-stock").withoutAncillary();
						if (outDiv.length !== 1) {
							outDiv = $("[data-sku-id='" + sku.sku_number + "'] div.out-of-stock", settings.top)
						}
						outDiv[sku.is_in_stock ? "hide" : "show"]();
						$(settings.top + " " + settings.addToCart).attr("disabled", !sku.is_show_add_to_basket);
						outDiv.find("b").text(sku.is_coming_soon ? "coming soon" : "not in stock").end();
						if (!sku.is_show_add_to_basket) {
							astoria.analytics.analyze("outofstock", [sku])
						}
					};
					newFn.call(this, sku);
					this.updateInStock = newFn
				},
				updateActionArea: (function() {
					var signedOrRecog = astoria.util.CookieUtils.isSignedInOrRecognizedUser(),
						signedIn = astoria.util.CookieUtils.isSignedInUser();
					return function(sku) {
						var settings = this.infoSettings;
						var biExcl = $(settings.biExclusive);
						$("span", biExcl).text(sku.getBILevelDisplay());
						biExcl[sku.is_beauty_insider ? "show" : "hide"]();
						var biQualify = $(settings.biInsiderQualify);
						var userLevel = astoria.User.current().bi_status.toLowerCase();
						if (!sku.isUserBILevelQualified(userLevel)) {
							$(".bi-level", biQualify)[sku.isBILevelRouge() ? "addClass" : "removeClass"]("rouge").text(sku.getBILevelDisplay(true));
							biExcl[sku.isBILevelRouge() ? "addClass" : "removeClass"]("rouge");
							$(".sign-in", biQualify)[!signedOrRecog ? "show" : "hide"]();
							$(".sign-up", biQualify)[(!signedIn || userLevel == "non_bi") && sku.isBILevelBI() ? "show" : "hide"]();
							$(".learn-vib", biQualify)[sku.isBILevelVIB() && userLevel != "vib" && userLevel != "rouge" ? "show" : "hide"]();
							$(".learn-rouge", biQualify)[sku.isBILevelRouge() && userLevel != "rouge" ? "show" : "hide"]();
							biQualify.show()
						} else {
							biQualify.hide()
						}
					}
				})(),
				loadDefaultSkuFromPage: function() {
					var sku;
					try {
						sku = astoria.Sku.defaultSku = JSON.parse($("#defaultSku").html().trim());
						astoria.Sku.mixInEntity(sku);
						skuStorage.skuFor(sku.sku_number, sku)
					} catch (e) {}
					return sku
				},
				saveInitial: function() {
					var controller = this,
						infoSettings = controller.infoSettings,
						sel = infoSettings.top + " " + infoSettings.variationPlace,
						variationPlace, itemNumberPlace = $(infoSettings.top + " " + infoSettings.itemNumber),
						ingredHead = "ingredientVariationVal" in infoSettings ? $(infoSettings.ingredientVariationVal) : undefined,
						ingredients = ingredHead ? ingredHead.next("p.sku-ingredients") : undefined,
						skuPricePlace = $(infoSettings.top + " " + infoSettings.price),
						changeQuantity = $(infoSettings.top + " " + infoSettings.quantitySelect + "," + infoSettings.quantitySelectPopup),
						salePricePlace = $(".sku-price .sale-price", infoSettings.top).withoutAncillary(),
						skuSizePlace = $(infoSettings.top + " " + infoSettings.skuSize),
						freeShippingPlace = $(infoSettings.freeShipping, infoSettings.top),
						valPricePlace = $(infoSettings.top + " .sku-price .value-price"),
						biLogo = $("div.bi-logos"),
						biMsg = $("p.bi-exclusive"),
						fsmap = {
							FLASH_FREE_SHIP: "sku-free-flash-shipping",
							FLASH_HAZMAT_FREE_SHIP: "sku-free-flash-hazmat-shipping",
							TRESHOLD_FREE_SHIP: "sku-free-shipping",
							ROUGE_FREE_SHIP: "sku-free-rouge-shipping",
							ROUGE_HAZMAT_FREE_SHIP: "sku-free-rouge-hazmat-shipping"
						};
					salePricePlace.length || (salePricePlace = $(".sku-price .sale-price", "#primarySkuInfoArea"));
					var VARIATION_NONE = astoria.util.Product.VARIATION_NONE;
					this.freeShippingAmt = parseFloat($("#free-shipping").attr("data-free-shipping-amt"));
					if ($(this.infoSettings.heroImage).parent().is("a")) {
						var updatePicture = this.updateHeroImage,
							prodPageLink = $(this.infoSettings.heroImage).parent(),
							preFix = prodPageLink.attr("href").match(/.*=/);
						if (preFix) {
							this.updateHeroImage = function(sku, heroIndex) {
								prodPageLink.attr("href", preFix + sku.sku_number);
								updatePicture.call(this, sku, heroIndex)
							}
						}
					}
					if (!Modernizr.touch) {
						$(infoSettings.swatches, infoSettings.top).tooltipDestroy().tooltip({
							html: true,
							container: "body"
						})
					}
					var sku = controller.sku = this.loadDefaultSkuFromPage();
					if (sku && !sku.isCollOrVertical() && sku.is_in_stock === false) {
						$(".btn-add-to-basket", this.scope).attr("data-is_in_stock", false)
					}
					var skuId = sku ? sku.sku_number : this.currentSkuId();
					if (skuId) {
						skuStorage.skuFor(skuId, function() {
							var primary_product = this.primary_product,
								variationType = primary_product.variation_type ? primary_product.variation_type.toLowerCase() : "",
								noSizeVariation = variationType !== "size",
								variationPlace = $(noSizeVariation ? sel : ""),
								swatchType = primary_product.swatch_type ? primary_product.swatch_type.toLowerCase() : "";
							controller.primary_product = primary_product;
							$.extend(controller, {
								updateSkuInfoView: function(sku) {
									var format = this.currencyFormatted,
										additional = ( !! sku.additional_sku_desc ? " - " + sku.additional_sku_desc : "");
									itemNumberPlace.text(sku.sku_number);
									skuPricePlace.find(".list-price .price").text(format(sku.list_price));
									var newQuantityforDropdown = sku.max_quantity;
									changeQuantity.empty();
									if (sku.is_in_stock && sku.max_quantity > 1) {
										changeQuantity.prop("disabled", false);
										for (var ourQuantity = 1; ourQuantity <= newQuantityforDropdown; ourQuantity++) {
											changeQuantity.append('<option value="' + ourQuantity + '">' + ourQuantity + "</option>")
										}
										changeQuantity.siblings(".quantity").val(1)
									} else {
										if (sku.is_in_stock) {
											changeQuantity.prop("disabled", true);
											changeQuantity.append("<option>1</option>");
											changeQuantity.siblings(".quantity").val(1)
										} else {
											changeQuantity.prop("disabled", true);
											changeQuantity.append("<option>--</option>");
											changeQuantity.siblings(".quantity").val(0)
										}
									} if (("sale_price" in sku) && sku.sale_price && sku.sale_price !== sku.list_price) {
										salePricePlace.show().find(".price").text(format(sku.sale_price));
										skuPricePlace.addClass("sale")
									} else {
										salePricePlace.hide();
										skuPricePlace.removeClass("sale")
									} if (sku.sku_size) {
										skuSizePlace.text(sku.sku_size).prev().show()
									} else {
										skuSizePlace.empty().prev().hide()
									}
									this.displayPromo(sku);
									this.displayBILogo(sku);
									if (("value_price" in sku) && sku.value_price) {
										valPricePlace.find(".price").text(sku.value_price).end().show()
									} else {
										valPricePlace.hide()
									} if (variationType !== VARIATION_NONE && sku.variation_value) {
										variationPlace.html('<span class="OneLinkNoTx">' + sku.variation_value + "</span>" + additional)
									}
									controller.updateNewForProductPage(sku, variationPlace, skuSizePlace);
									if (ingredHead) {
										ingredHead.html(sku.variation_value);
										ingredients.html(sku.ingredients)
									}
								},
								displayBILogo: function(sku) {
									if ("bi_exclusivity_level" in sku) {
										biLogo.find("img.logo-" + sku.bi_exclusivity_level.toLowerCase()).tap(function(e) {
											if (e.length === 0 || e.is(":not(:visible)")) {
												biLogo.find("img").hide();
												e.show()
											}
										})
									} else {
										biLogo.find("img").hide()
									}
								},
								displayPromo: function(sku) {
									var k, info = astoria.Sku.skuInfo(sku);
									if ("promo_display_type" in sku) {
										k = fsmap[sku.promo_display_type]
									} else {
										k = ((sku.sale_price ? sku.sale_price : sku.list_price) >= this.freeShippingAmt) && !info.isEgc ? "sku-free-shipping" : ""
									}
									freeShippingPlace.each(function(_, p) {
										$(p)[(k && $(p).hasClass(k)) ? "show" : "hide"]()
									})
								},
								updateNewForProductPage: function(sku, variationPlace, skuSizePlace) {
									if (this.isProductPage()) {
										var newPlace = variationPlace.length > 0 ? variationPlace.parent() : skuSizePlace.closest(".product-description");
										newPlace.children(".flag-new, .separator")[sku.is_new ? "show" : "hide"]()
									}
								},
								isProductPage: function() {
									return $("body#product").length > 0
								}
							})
						}, {
							include_product: true
						})
					}
					return this
				}
			});
			self.infoSettings = $.extend({
				top: ".product-information",
				biExclusive: ".bi-exclusive",
				biInsiderQualify: ".qualify",
				buyCollection: ".btn-buy-the-collection",
				freeShipping: "#primarySkuInfoArea .msg-free-ship",
				skuSelector: ".sku-selector",
				itemNumber: ".info-row:first .sku .value",
				quantitySelect: ".product-detail #primarySkuInfoArea .quantitySelector",
				quantitySelectAncillary: ".ancillary-products .quantitySelector",
				quantitySelectCollections: ".ancillary-products .quantitySelector",
				quantitySelectPopup: ".modal-body .quantitySelector",
				skuSize: "#primarySkuInfoArea .size .value",
				colorDesc: "#primarySkuInfo_color",
				heroImage: ".hero-main-image img",
				heroMessages: ".messages",
				largeHeroImage: ".product-image img",
				productActions: ".product-actions",
				currentlyOnList: ".btn-love.loved",
				hazmat: "#shippingInfo_hazmat",
				giftWrappable: "#shippingInfo_isGiftWrappable",
				price: "#primarySkuInfo_price .sku-price:has(.list-price .price)",
				addToCart: ".btn-add-to-basket",
				notifyEmail: ".btn-email-when-in-stock, .btn-change-in-stock-email",
				imageSelector: "#product-content .image-selector",
				thumbnails: ".product-thumb",
				beautyInsider: ".bi-logos",
				seal: ".seal",
				variationPlace: ".info-row.variation:first .value",
				swatches: ".sku-selector .product-thumb"
			}, settings || {});
			this.instance = "ProductView";
			self.init = function() {
				var controller = this,
					scope = this.infoSettings.top;
				$(this.infoSettings.swatches, scope).off("mouseenter mouseleave click").on("mouseenter mouseleave click", function(event) {
					var skuId = astoria.util.HtmlUtils.skuIdFromId($(this).attr("id")),
						skuIndex = $(controller.infoSettings.swatches).index($(this));
					controller.skuEvent(skuId, skuIndex, event);
					astoria.util.MyList.initFavorites();
					event.preventDefault()
				});
				if ($.isFunction(this.registerBiSignUp)) {
					this.registerBiSignUp()
				}
				var allSkuIds = this.allSkuIds = [];
				$(this.infoSettings.swatches).each(function(swatch) {
					allSkuIds.push(astoria.util.HtmlUtils.skuIdFrom($(this)))
				});
				$(this.infoSettings.top + " " + this.infoSettings.quantitySelect + ", " + this.infoSettings.quantitySelectAncillary + ", " + this.infoSettings.quantitySelectCollections + "," + this.infoSettings.quantitySelectPopup).change(function() {
					$(this).siblings(".quantity").val($(this).val())
				});
				if ("thumbScope" in this) {
					$(this.thumbScope).on("mouseenter", this.infoSettings.thumbnails, function(e) {
						controller.updateHeroImageFor(this)
					}).on("mouseleave", this.infoSettings.thumbnails, function(e) {
						controller.restoreHeroImage()
					}).on("click", this.infoSettings.thumbnails, function(e) {
						controller.rememberProductImage(this);
						e.preventDefault()
					})
				}
			};
			self.currentSkuId = function() {
				var settings = this.infoSettings;
				if ("sku" in this) {
					return this.sku.sku_number
				} else {
					if ($(settings.skuId).length > 0) {
						return $(settings.skuId).val()
					} else {
						if ($(settings.swatches, settings.top).length > 0) {
							var current = $(settings.swatches, settings.top).filter(".current");
							if (current.length === 0) {
								current = $($(settings.swatches)[0]);
								current.addClass("current")
							}
							return astoria.util.HtmlUtils.skuIdFromId(current.attr("id"))
						} else {
							if ($(settings.top + " " + settings.itemNumber).length) {
								return $(settings.top + " " + settings.itemNumber).text()
							}
						}
					}
				}
			};
			self.currentThumbnailIndex = function() {
				if ("thumbScope" in this) {
					return $(this.infoSettings.thumbnails, this.thumbScope).length > 0 ? $(this.infoSettings.thumbnails, this.thumbScope).index($(this.infoSettings.thumbnails + ".current", this.thumbScope)) : -1
				} else {
					return -1
				}
			};
			var skuIdAndHeroIndex = function(elem) {
				return $(elem).attr("id").match(/sku-(\d*)-(\d*)/).slice(1)
			};
			self.thumbnailUpdate = function(sku, activeIndex, fn) {
				var thumb_images = this.resolveImages(sku.thumb_images);
				var noShow = updateThumbnails.call(this, thumb_images, sku.id, activeIndex);
				if (fn) {
					fn(noShow)
				}
			};

			function updateThumbnails(images, skuId, activeIndex) {
				var self = this,
					imageSelector = $(this.infoSettings.imageSelector),
					thumbClone = self.infoSettings.thumbClone,
					thumb, isCurrent, oldSkuId = this.skuIdFrom(imageSelector),
					noShow = !images || images.length < 2;
				if (noShow) {
					return !!imageSelector.hide()
				}
				if (oldSkuId !== skuId) {
					imageSelector.empty().show();
					images.forEach(function(img, i) {
						isCurrent = i === activeIndex ? "current" : "";
						thumb = thumbClone.clone();
						thumb.attr("id", "sku-" + skuId + "-" + i)[isCurrent ? "addClass" : "removeClass"]("current").find("img").attr("src", astoria.ui.ViewHelper.prodImageFor(img)).end().appendTo(imageSelector)
					});
					imageSelector.attr("id", "sku" + skuId + "-" + self.instance.replace(/Controller/, "")).find("a").addClass("product-thumb")
				} else {
					if (imageSelector.find("a.current").index() !== activeIndex) {
						imageSelector.find("a").removeClass("current").eq(activeIndex).addClass("current")
					}
				}
				return false
			}
			self.resolveHeroImages = function(sku) {
				return this.resolveImages(sku.hero_images)
			};
			self.updateHeroImageFor = function(elem) {
				var skuId = this.currentSku().id;
				var heroIndex = $(this.infoSettings.thumbnails, this.thumbScope).index(elem);
				var controller = this;
				skuStorage.skuFor(skuId, function() {
					controller.updateHeroImage(this, heroIndex)
				})
			};
			self.heroImg = function(sku, idx) {
				var img;
				var heroImages = this.resolveHeroImages(sku);
				if (heroImages.length) {
					img = astoria.ui.ViewHelper.prodImageFor(idx > -1 ? heroImages[idx] : heroImages.slice(-1)[0])
				}
				return img
			};
			self.updateHeroImage = function(sku, heroIndex) {
				var heroImg = this.infoSettings.heroImage;
				var newImg = this.heroImg(sku, heroIndex);
				if (newImg) {
					$(heroImg).each(function() {
						this.tried = undefined
					});
					$(heroImg).attr("src", newImg)
				}
			};
			self.resolveImages = function(images) {
				if ($.isArray(images)) {
					return images
				} else {
					if (images) {
						return images.split(" ")
					} else {
						return
					}
				}
			};
			self.restoreHeroImage = function() {
				var index = this.currentThumbnailIndex();
				this.updateHeroImage(this.sku, index)
			};
			self.updateSeals = function(sku) {
				var sealImg = $(this.infoSettings.seal + " img");
				if (sku.is_organic) {
					sealImg.attr("src", astoria.ui.ViewHelper.imageFor("seal-organic.png")).attr("alt", "").show()
				} else {
					if (sku.is_natural) {
						sealImg.attr("src", astoria.ui.ViewHelper.imageFor("seal-naturals.png")).attr("alt", "naturally astoria").show()
					} else {
						sealImg.hide()
					}
				}
			};
			self.updateHeroMessages = function(sku) {
				var fn = sku.is_new ? "show" : "hide";
				$(this.infoSettings.heroMessages + " .flag-new")[fn]();
				var ar = [];
				if (sku.is_astoria_exclusive) {
					ar.push("exclusive")
				}
				if (sku.is_limited_edition) {
					ar.push("limited edition")
				}
				if (sku.is_online_only) {
					ar.push("online only")
				}
				var text = ar.join(" &middot; ");
				$(this.infoSettings.heroMessages + " .flags").html(text)
			};
			self.updateAll = function(options) {
				var sku = options.id ? options : options.sku;
				this.updateAllExceptAction(options);
				this.updateSeals(sku);
				if ($.isFunction(this.updateActionArea)) {
					this.updateActionArea(sku)
				}
				if ($.isFunction(this.updateLoveButton)) {
					this.updateLoveButton(sku)
				}
				if ($.isFunction(this.updateReminderButton)) {
					this.updateReminderButton(sku)
				}
				$(this.infoSettings.hazmat)[sku.is_hazmat ? "show" : "hide"]();
				$(this.infoSettings.giftWrappable)[sku.is_gift_wrappable ? "hide" : "show"]()
			};
			self.updateAllExceptAction = function(options) {
				if (!$.isFunction(this.updateSkuInfoView)) {
					return
				}
				var scope = this.infoSettings.top,
					sku = "sku_number" in options ? options : options.sku,
					skuSelector = $("div.sku-selector", scope),
					oldSkuId, viewSwitch = "thumbIndex" in options;
				this.updateSkuInfoView(sku);
				this.updateInStock(sku);
				this.updateHeroMessages(sku);
				if ($.isFunction(this.updateEmailWhenBackInStock)) {
					this.updateEmailWhenBackInStock(sku)
				}
				var index = "sku_number" in options ? this.currentThumbnailIndex() : options.thumbIndex;
				if ($.isFunction(this.updateThumbnailsView)) {
					this.updateThumbnailsView(sku, index)
				}
				if (viewSwitch) {
					oldSkuId = this.currentSkuId();
					if (oldSkuId !== this.sku.sku_number) {
						skuSelector.find("a").removeClass("current").filter("#sku-" + this.sku.sku_number).addClass("current")
					}
					if ($.isFunction(this.initScroll)) {
						this.initScroll()
					}
				}
				this.updateHeroImage(sku, index);
				if ($.isFunction(this.updateReminderStoreLocator)) {
					this.updateReminderStoreLocator(sku)
				}
			};
			self.updateForCollection = function(options) {
				var sku = "sku_number" in options ? options : options.sku,
					index = "sku_number" in options ? this.currentThumbnailIndex() : options.thumbIndex;
				if ($.isFunction(this.updateThumbnailsView)) {
					this.updateThumbnailsView(sku, index)
				}
				this.updateHeroImage(sku, index)
			};
			self.rememberSku = function(sku) {
				this.sku = sku;
				var skuUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + "?skuId=" + this.sku.id;
				if ($("#fb-button").length > 0) {
					$("#fb-button").attr("href", skuUrl);
					if (typeof FB !== "undefined") {
						FB.XFBML.parse(document.getElementById("fb-button"))
					}
				}
				if ($("#fb-root")) {
					astoria.util.initFacebook()
				}
				$(this.infoSettings.swatches).removeClass("current");
				this.swatchById(sku.id, $(this.infoSettings.swatches)).addClass("current");
				if ($.isFunction(this.updateReminderStoreLocator)) {
					this.updateReminderStoreLocator(sku)
				}
			};
			self.currentSku = function(sku) {
				return this.sku
			}, self.restore = function() {
				var action = $.isFunction(this.updateActionArea),
					myList = $.isFunction(this.updateLoveButton);
				var newFn = function() {
					this.updateAllExceptAction(this.sku);
					if (action) {
						this.updateActionArea(this.sku)
					}
					if (myList) {
						this.updateLoveButton(this.sku)
					}
				};
				newFn.call(this);
				this.restore = newFn
			};
			self.rememberProductImage = function(image) {
				$(this.infoSettings.thumbnails + ".current", this.thumbScope).removeClass("current");
				$(image).addClass("current")
			};
			self.swatchById = function(skuId, fnIn) {
				return ($.isFunction(fnIn) ? fnIn() : fnIn).filter("[id='sku-" + skuId + "']:first")
			}
		}
	});
	astoria.app.initializer(function() {
		astoria.ui.product.initQuickLook()
	});
	$.extend(astoria.ui.controller, {
		SupportsLoves: {
			CURRENTLY_ON_YOUR_LIST: ".btn-love.loved",
			love: function(selector, storeLovedIds) {
				var self = this,
					Sku = astoria.Sku,
					MyList = astoria.util.MyList,
					collVert = ("defaultSku" in Sku) && Sku.defaultSku.isCollOrVertical();
				if (!("isSignedInOrRecognizedUser" in self)) {
					$.extend(self, astoria.util.CookieUtils)
				}
				this.onLove = function(e) {
					e.preventDefault();
					var MyList = astoria.util.MyList,
						self = e.data,
						button = $(this),
						prodId, skuId, sku, fn, isLove = !button.hasClass("loved");
					button.removeClass("hover-enable");
					if (collVert) {
						prodId = button.attr("data-product_id");
						fn = isLove ? MyList.createAll : MyList.destroyAll
					} else {
						fn = isLove ? MyList.create : MyList.destroy
					}
					sku = self.currentSku(button);
					skuId = sku.sku_number;
					button.disableUntilAjaxComplete();
					if (self.isSignedInOrRecognizedUser()) {
						self.updateLoveButton(sku, button, isLove)
					}
					fn((prodId ? prodId : sku.sku_number), function() {
						if ("onSuccessfulLove" in self) {
							self.onSuccessfulLove(sku.sku_number)
						}
						astoria.event.publish(isLove ? "itemLoved" : "itemUnloved", [prodId ? self.currentSkuId() : skuId, isLove, self, sku])
					}, function() {
						self.updateLoveButton(sku, button, !isLove)
					});
					astoria.analytics.analyze("love", [prodId ? prodId : button.attr("data-product_id"), isLove])
				};
				if (!("onItemLovedOrUnloved" in self)) {
					this.onItemLovedOrUnloved = function(e, skuId, loved, publisher) {
						if (publisher === self) {
							return
						}
						var button = $(selector, self.scope).filter("[data-sku_number='" + skuId + "']");
						if (button.length) {
							self.updateLoveButton({
								sku_number: skuId
							}, button, loved)
						}
					}
				}
				astoria.event.resubscribe("itemLoved itemUnloved", self.onItemLovedOrUnloved);
				this.enableTooltips = (function() {
					var loves;
					return function() {
						if (!Modernizr.touch) {
							if (loves && this.isCarousel) {
								loves.each(function(_, e) {
									$(e).removeData("seph.tooltip")
								})
							}
							loves = $(selector, this.scope);
							loves.tooltip({
								container: "body"
							}).click(function(e) {
								e.preventDefault();
								$(this).removeClass("hover-enable").tooltip("hide")
							})
						}
						return this
					}
				})();
				if (!("updateLoveButton" in this)) {
					this.updateLoveButton = function(sku, button, isLoved) {
						if (isLoved) {
							button && button.addClass("loved").find(".btn-inner .btn-text").text(collVert ? "loved all" : "loved")
						} else {
							button && button.removeClass("loved").find(".btn-inner .btn-text").text(collVert ? "add all to loves" : "add to loves")
						}
					}
				}
				if (storeLovedIds && !collVert) {
					var updater = this.updateLoveButton;
					this.updateLoveButton = function(sku, button, isLoved) {
						button || (button = $(selector, this.scope));
						if (typeof isLoved === "undefined") {
							this.isOnMyList(sku.sku_number, function(isLoved) {
								astoria.logger.log("SupportsLoves#updateLoveButton on my loves: ", isLoved);
								updater(sku, button, isLoved)
							})
						} else {
							MyList.addListItems(sku, isLoved, function() {
								MyList.isOnMyList(sku.sku_number, function(isLoved) {
									astoria.logger.log("SupportsLoves#updateLoveButton after a click on my loves: ", isLoved);
									updater(sku, button, isLoved)
								})
							})
						}
					}
				}
				if (!("currentSku" in this)) {
					this.currentSku = function(button) {
						return {
							sku_number: button.attr("data-sku_number")
						}
					}
				}
				$(this.scope).off("click mouseenter mouseleave", selector).on("click", selector, this, this.onLove).on("mouseenter", selector, function() {
					if ($(this).hasClass("loved")) {
						$(this).find(".btn-inner .btn-text").text(collVert ? "unlove all" : "unlove")
					}
				}).on("mouseleave", selector, function() {
					$(this).addClass("hover-enable");
					if ($(this).hasClass("loved")) {
						$(this).find(".btn-inner .btn-text").text(collVert ? "loved all" : "loved");
						$(this).attr("data-original-title", collVert ? "UNLOVE ALL" : "UNLOVE")
					} else {
						$(this).attr("data-original-title", collVert ? "LOVE ALL" : "LOVE IT")
					}
				});
				return this
			}
		},
		BiExclusiveController: function() {
			var self;
			this.registerBiSignUp = function() {
				self = this;
				$(".qualify a:first").off("click").on("click", function(e) {
					astoria.ui.signonController.show();
					e.preventDefault()
				});
				$(".qualify a:last").off("click").on("click", function(e) {
					astoria.ui.registrationController.show({
						data: {
							is_bi_signup: true
						}
					});
					e.preventDefault()
				})
			}
		},
		ThumbnailController: {
			updateThumbnailsView: function(sku, activeIndex) {
				this.thumbnailUpdate(sku, activeIndex)
			},
			initScroll: function() {
				var scope = this.infoSettings.top,
					scrollable = $("div.scrollable", scope),
					swatches = scrollable.find(".product-thumb"),
					offsetTop = parseInt(scrollable.find(".sku-selector-content").css("padding-top")) + parseInt(swatches.css("border-width"));
				if (swatches.filter(".current").length > 0) {
					setTimeout(function() {
						scrollable.scrollTo(swatches.filter(".current"), 500, {
							offset: -offsetTop
						})
					}, 0)
				}
			}
		},
		QuickLookController: function(scope) {
			var self = this,
				quickLook, skuOrProductId, isExcludeBIProdLinks = $("body#checkout").length === 1,
				noRatings = !! $(".beauty-insider-rewards").length,
				regularCache = {}, settings = {
					top: scope,
					productActions: scope,
					biInsiderQualify: scope + " .qualify",
					freeShipping: ".msg-free-ship",
					swatches: ".product-thumb",
					heroImage: scope + " .hero-image",
					heroMessages: scope + " .messages",
					skuSize: ".size .value",
					price: ".sku-price:has(.list-price .price)",
					seal: scope + " .seal",
					skuId: scope + " [name='sku_id']",
					thumbnails: ".product-thumb",
					colorDesc: scope + "#quickLookSkuInfo_color",
					notifyEmail: ".btn-email-when-in-stock, .btn-change-in-stock-email",
					currentlyOnList: ".btn-love.loved"
				};
			self.scope = scope;
			var skuStorage = new astoria.util.storage.SKUStorage(),
				popUpcontent, extractParam = astoria.util.ParameterUtils.extractParam;
			$.extend(self, new astoria.ui.product.ProductView(settings, skuStorage), astoria.util.HtmlUtils, new astoria.ui.controller.BiExclusiveController, astoria.ui.controller.SupportsLoves, astoria.util.CookieUtils, new astoria.ui.controller.EmailWhenBackInStockController, astoria.dimensions);
			this.instance = "QuickLookController";
			var baseRegisterHandlers = self.init;
			$.extend(self, {
				registerHandlers: function() {
					baseRegisterHandlers.call(this)
				},
				detectPrepareData: function() {
					var L = "data-bi_level",
						level, shade, isSaleOnly = $("meta[name='searchtype']").attr("content") === "sale";
					shade = extractParam("shade_code");
					self.prepareData = function(data, trigger) {
						(level = trigger.parents("[" + L + "]").attr(L)) && (data.bi_level = level);
						shade && (data.shade_code = extractParam("shade_code"));
						isSaleOnly && (data.showOnSaleOnly = true)
					}
				},
				loadDefaultSkuFromPage: function() {
					var sku;
					try {
						sku = astoria.Sku.defaultSku = JSON.parse($("#defaultSku", this.scope).html().trim());
						astoria.Sku.mixInEntity(sku);
						skuStorage.skuFor(sku.sku_number, sku)
					} catch (e) {}
					return sku
				},
				updateSeals: function(sku) {
					var fn = sku.is_beauty_insider ? "show" : "hide";
					$(this.infoSettings.beautyInsider)[fn]()
				},
				showLoading: function() {
					$(quickLook).addClass("loading")
				},
				showMini: (function() {
					var miniCache = {};
					return function(trigger, success) {
						var skuId = self.skuIdFrom(trigger),
							content = miniCache[skuId];
						self.sku = {
							sku_number: skuId
						};
						if (content) {
							self.updateContent(content, success, false);
							astoria.analytics.analyze("quickLook", [quickLook, trigger])
						} else {
							self.showLoading();
							astoria.util.storage.loadSkuQuickView(skuId, function(response) {
								content = miniCache[skuId] = $(response);
								content.find(isExcludeBIProdLinks ? "a" : noRatings ? ".product-rating" : "").remove();
								self.updateContent(content, function() {
									$(settings.top + " span.stars").stars();
									success()
								}, true);
								astoria.analytics.analyze("quickLook", [quickLook, trigger])
							})
						}
					}
				})(),
				updateContent: function(qlContent, success, animate) {
					var container = $(quickLook).find(".container:first"),
						process = function() {
							if (animate) {
								popUpcontent.fadeIn();
								container.css({
									height: "auto"
								})
							} else {
								popUpcontent.show()
							}
							success()
						};
					popUpcontent.append(qlContent);
					if (animate) {
						$("body").append($('<div id="ql-size-test" class="popup quick-look" style="display: block;"/>').append(popUpcontent.clone().show()));
						var posY = self.scrollY(),
							testHeight = parseFloat($("#ql-size-test").css("height")),
							qlHeight = parseFloat($(quickLook).css("height")),
							contHeight = parseFloat($("#ql-size-test .container:first").css("height")),
							top = ($(window).height() / 2) - (testHeight / 2) + posY;
						$("#ql-size-test").remove();
						$(quickLook).animate({
							top: top
						}, 800);
						container.animate({
							height: contHeight
						}, 800, function() {
							$(quickLook).removeClass("loading");
							process()
						})
					} else {
						process()
					}
				},
				showRegular: function(trigger, success) {
					function init() {
						skuStorage.flush();
						skuStorage.extras = {
							product_id: $(settings.top).find("[name='prod_id']").val()
						};
						self.registerHandlers();
						self.saveInitial().love(".btn-love", true);
						success()
					}
					skuOrProductId = self.productIdFrom(trigger);
					var skuId = self.skuIdFromId(trigger.attr("name")),
						key = skuOrProductId + "," + skuId;
					if (regularCache[key]) {
						self.updateContent(regularCache[key], init, false);
						astoria.analytics.analyze("quickLook", [quickLook, trigger])
					} else {
						self.showLoading();
						var data = {
							productId: skuOrProductId,
							skuId: skuId
						};
						if ($.isFunction(self.prepareData)) {
							self.prepareData(data, trigger)
						}
						astoria.util.storage.loadProductQuickView(data, function(response) {
							regularCache[key] = $(response);
							self.updateContent(regularCache[key], init, true);
							$(scope + " span.stars").stars();
							astoria.analytics.analyze("quickLook", [quickLook, trigger])
						})
					}
				},
				loadQuicklook: function(trigger, success) {
					trigger = $(trigger);
					var fn, rel = trigger.attr("rel"),
						MyList = astoria.util.MyList;
					quickLook = $(rel);
					if (/mini-quick-look/.test(rel)) {
						fn = this.showMini
					} else {
						fn = this.showRegular;
						MyList.initFavorites()
					}
					popUpcontent = quickLook.find(".modal-body");
					popUpcontent.children().detach().end().hide();
					astoria.logger.log("QuickLookController#loadQuicklook calling displayCenteredPopup");
					astoria.util.PopupUtils.displayCenteredPopup(quickLook, {
						backdrop: true
					});
					fn(trigger, function() {
						astoria.dimensions.setAllWidths();
						if ($.isFunction(success)) {
							success()
						}
					})
				},
				resolveItems: function() {
					var quant = $("select.quantitySelector", this.scope),
						quantity = quant.length ? parseInt(quant.val()) : 1,
						skuNumber = this.currentSkuId();
					return astoria.ShoppingCart.makeItems(skuNumber, quantity)
				}
			});
			$.extend(self, new astoria.ui.controller.ShoppingCartController({
				scope: scope
			})).addToBasket(".btn-add-to-basket, .btn-buy-the-collection").onSuccessfulAdd(function(source, cart) {
				quickLook.find(".icon-close").triggerHandler("click");
				self.updateOnAdd(cart)
			});
			if ("#mini-quick-look" === scope) {
				return
			}
			self.onItemLovedOrUnloved = function(e, skuId, isLove, publisher) {
				if (publisher === self) {
					return
				}
				astoria.logger.log("QuickLookController#onItemLovedOrUnloved entering with ", isLove);
				astoria.util.MyList.addListItems({
					sku_number: skuId
				}, isLove);
				regularCache = {}
			};
			self.registerEmailStock()
		}
	});
	astoria.ui.controller.QuickLookController.usesQL = function() {
		var self = this;
		var showQL = function(e) {
			e.preventDefault();
			if (Modernizr.touch && $(this).parents(".view-grid").length > 0) {
				return
			}
			astoria.logger.log("showQL entering", this);
			if ($(this).is("[rel='#quick-look']")) {
				quickLookController.loadQuicklook(this)
			} else {
				if ($(this).is("[rel='#mini-quick-look']")) {
					miniQLookController.loadQuicklook(this)
				}
			}
		};
		if (Modernizr.touch) {
			var touchDeviceDblClick = function(onDblClick, onTap, delay) {
				var timer, wasMoved = false;
				delay || (delay = 400);
				onTap || (onTap = $.noop);
				$(self.scope).on("click", ".product-image", function(e) {
					if ($(this).find(".qlook").length > 0) {
						e.preventDefault()
					}
				}).on("touchend", ".product-image", function(e) {
					if (e.type === "click") {
						return
					}
					var target = $(this),
						now = new Date().getTime(),
						lastTouch = target.data("lastTouch") || (now + 1),
						delta = now - lastTouch;
					clearTimeout(timer);
					if (delta < delay && delta > 0) {
						e.preventDefault();
						onDblClick.call(target, e)
					} else {
						if (!wasMoved) {
							timer = setTimeout(function(evt) {
								astoria.logger.log("timed out calling onTap");
								clearTimeout(timer);
								onTap.call(evt.currentTarget, evt)
							}, delay, e)
						} else {
							wasMoved = false
						}
					}
					target.data("lastTouch", now)
				}).on("touchmove", function(e) {
					$(this).removeData("lastTouch");
					wasMoved = true
				})
			};
			touchDeviceDblClick(function() {
				$(this).find("a[href!='#']").tap(function(a) {
					if (a.length) {
						window.location.href = a.attr("href")
					}
				})
			}, function(e) {
				var a = $(this).find(".qlook");
				showQL.call(a, e)
			})
		} else {
			$(this.scope).on("click", ".qlook", showQL)
		}
	};
	astoria.ShoppingCart = {
		url: "/rest/user/cart",
		add: function(cartContents, format, successFn, errorFn, extra) {
			if (!format) {
				format = "json"
			}
			if ($.isFunction(format)) {
				extra = errorFn;
				errorFn = successFn;
				successFn = format;
				format = "json"
			}
			if ($.isPlainObject(errorFn)) {
				extra = errorFn;
				errorFn = undefined
			}
			this.doAdd(cartContents, successFn, errorFn)
		},
		doAdd: astoria.ajax.makeAjaxPost(astoria.conf.basketAddUrl, {
			success: function(cart) {
				$.extend(cart, astoria.ShoppingCart);
				if ("line_items" in cart) {
					cart.line_items.forEach(function(li) {
						astoria.Sku.mixInEntity(li.sku)
					})
				}
			}
		}),
		statusMessage: function(type, cart) {
			switch (type) {
				case "vib_status":
					return this.vibStatusMessage(cart);
				case "promo_display_type":
					return this.flashStatusMessage(cart);
				default:
					return ""
			}
		},
		FLASH_FREE_SHIP: "FLASH_FREE_SHIP",
		FLASH_HAZMAT_FREE_SHIP: "FLASH_HAZMAT_FREE_SHIP",
		TRESHOLD_FREE_SHIP: "TRESHOLD_FREE_SHIP",
		ROUGE_FREE_SHIP: "ROUGE_FREE_SHIP",
		ROUGE_HAZMAT_FREE_SHIP: "ROUGE_HAZMAT_FREE_SHIP",
		isRouge: function() {
			return /VIB_ROUGE/.test(this.vib_status)
		},
		vibStatusMessage: function(cart, isBasket) {
			cart || (cart = this);
			var message, i18n = astoria.i18n,
				curr = astoria.util.CookieUtils.currentCountry() === "us" ? "$" : "C$",
				amount = function() {
					return '<b class="to-vib">' + curr + cart.spend_amount_for_next_segment + "</b>"
				}, cookieUtils = astoria.util.CookieUtils,
				isUS = cookieUtils.currentCountry() === astoria.US;
			var VIB_ROUGE = '<b class="label-rouge">VIB Rouge</b>';
			switch (cart.vib_status) {
				case "NOT_VIB_CLOSE":
					if (cart.spend_amount_for_next_segment && isUS) {
						message = i18n.t("vibrt.spend_to_become", curr, cart.spend_amount_for_next_segment)
					}
					break;
				case "NOT_VIB_QUALIFIES":
					message = i18n.t("vibrt.qualifies_for_vib");
					break;
				case "VIB_SPEND_CLOSE":
					if (cart.spend_amount_for_next_segment && cart.vib_end_year && isUS) {
						message = i18n.t("vibrt.spend_to_extend", curr, cart.spend_amount_for_next_segment, cart.vib_end_year + 1)
					}
					break;
				case "VIB_REQUALIFIES":
					if (cart.vib_end_year) {
						message = i18n.t("vibrt.extend_vib", cart.vib_end_year + 1)
					}
					break;
				case "NOT_VIB_ROUGE_CLOSE":
				case "VIB_ROUGE_CLOSE":
					if (cart.spend_amount_for_next_segment && isUS) {
						message = i18n.t("vibrt.spend_to_unlock_rouge", amount(), VIB_ROUGE)
					}
					break;
				case "NOT_VIB_ROUGE_QUALIFIES":
				case "VIB_ROUGE_QUALIFIES":
					message = i18n.t("vibrt.qualifies_for_vib_rouge", VIB_ROUGE);
					break;
				case "VIB_ROUGE_SPEND_CLOSE":
					if (cart.spend_amount_for_next_segment && isUS) {
						message = isBasket ? i18n.t("vibrt.spend_to_become_rouge_basket", amount(), VIB_ROUGE, cart.vib_end_year + 1) : i18n.t("vibrt.spend_to_become_rouge", amount(), VIB_ROUGE, cart.vib_end_year + 1)
					}
					break;
				case "VIB_ROUGE_REQUALIFIES":
					if (isBasket) {
						message = i18n.t("vibrt.requalifies_for_vib_rouge", VIB_ROUGE, cart.vib_end_year + 1)
					} else {
						message = i18n.t("vibrt.vib_rouge_requalifies", VIB_ROUGE, cart.vib_end_year + 1)
					}
					break
			}
			return message
		}
	};
	astoria.ShoppingCart.makeItems = function(skuNumber, quantity) {
		quantity || (quantity = 1);
		var items = {};
		items["/atg/commerce/order/purchase/CartModifierFormHandler.itemsMap." + skuNumber] = quantity;
		return items
	};
	astoria.ShoppingCart.flashStatusMessage = (function() {
		var m = {}, c = astoria.ShoppingCart;
		m[c.FLASH_FREE_SHIP] = "basket.freeShipping.flash";
		m[c.FLASH_HAZMAT_FREE_SHIP] = "basket.freeShipping.flash.hazmat";
		m[c.TRESHOLD_FREE_SHIP] = "basket.freeShipping.treshold";
		m[c.ROUGE_FREE_SHIP] = "basket.freeShipping.rouge";
		m[c.ROUGE_HAZMAT_FREE_SHIP] = "basket.freeShipping.rouge.hazmat";
		var isRouge = function(cart) {
			return [c.ROUGE_FREE_SHIP, c.ROUGE_HAZMAT_FREE_SHIP].contains(cart.promo_display_type)
		}, VIB_ROUGE = '<b class="label-rouge">VIB Rouge</b>';
		return function(cart) {
			cart || (cart = this);
			if (!("promo_display_type" in cart)) {
				return ""
			}
			var key = m[cart.promo_display_type];
			if (isRouge(cart)) {
				return astoria.i18n.t(key, VIB_ROUGE, '<span class="free">', "</span>")
			} else {
				return key ? astoria.i18n.t(key, '<span class="free">', "</span>") : ""
			}
		}
	})();
	astoria.ShoppingCart.addItems = astoria.ajax.makeAjaxPost(astoria.ShoppingCart.url, {
		success: function(cart) {
			$.extend(cart, astoria.ShoppingCart);
			if ("line_items" in cart) {
				cart.line_items.forEach(function(li) {
					astoria.Sku.mixInEntity(li.sku)
				})
			}
		}
	})
})(jQuery);
(function($) {
	var contextPath = astoria.conf.contextPath;
	$.extend(astoria.conf, {
		applyGiftCardUrl: contextPath + "/checkout/json/astoriaGiftCard.jsp",
		beautyInsiderUrl: contextPath + "/profile/MyAccount/MyInformation/json/updatePersonalized.jsp",
		colorIQUri: "/IQ/color/",
		eGiftCardUrl: contextPath + "/global/json/cartEgiftSave.jsp",
		paymentInfoUrl: contextPath + "/profile/MyAccount/include/requestResult_json.jsp",
		getPaymentInfoUrl: contextPath + "/profile/MyAccount/PaymentMethods/json/getCreditCardInfoByJson.jsp",
		promotionUrl: "/rest/user/cart/promo",
		retrieveGiftCard: contextPath + "/profile/MyAccount/PaymentMethods/json/eGiftCardBalance.jsp",
		shippingInfoUrl: contextPath + "/profile/MyAccount/MyInformation/json/getAddressInfomationByJson.jsp",
		shippingRequestUrl: contextPath + "/profile/MyAccount/MyInformation/json/addOrUpdateAddressResult.jsp",
		removeShippingUrl: contextPath + "/profile/MyAccount/MyInformation/json/addOrUpdateAddressResult.jsp",
		storesUrl: contextPath + "/popups/include/findInStoreSearchStore.jsp",
		CAREG: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
		US_ZIP_REG: /^\d{5}([\-]\d{4})?$/
	});
	$.fn.disableIt = function(toDisable) {
		var disable = typeof toDisable === "undefined" ? true : toDisable;
		this.each(function() {
			$(this).attr("disabled", disable)
		});
		$(this)[disable ? "addClass" : "removeClass"]("disabled");
		return this
	};
	astoria.Category = {};
	astoria.Category.mixInEntity = function(cat) {
		if (typeof cat === "undefined") {
			return
		}
		if ($.isPlainObject(cat)) {
			cat = [cat]
		}
		if (!$.isArray(cat)) {
			return
		}
		cat.forEach(function(c) {
			$.extend(c, astoria.Category);
			if ("sub_categories" in c) {
				astoria.Category.mixInEntity(c.sub_categories)
			}
		})
	};
	astoria.Country = {
		url: "/rest/countries/",
		COUNTRY_SWITCH: "country_switch",
		find: function(ctry, success) {
			if (typeof ctry !== "string") {
				throw {
					message: "Country.find Only string id supported for now."
				}
			}
			var call = astoria.ajax.makeAjaxGet(this.url + ctry.toLowerCase() + "/states");
			call(success)
		},
		displayName: function(ctr) {
			var ret = ctr;
			switch (ctr) {
				case astoria.US:
					ret = "the United States";
					break;
				case astoria.CA:
					ret = "Canada";
					break;
				default:
					ret = ctr
			}
			return ret
		}
	};
	astoria.Page = {
		buildLinks: function(current, total) {
			var links = [],
				p = function(p) {
					links.push(p === current ? {
						selected: p
					} : {
						page: p
					})
				}, interval = function() {
					links.push({
						inter: "i"
					})
				}, prev = function(p) {
					links.push({
						prev: p
					})
				}, next = function(p) {
					links.push({
						next: p
					})
				};
			if (current > 1) {
				prev(current - 1)
			}
			if (current - 2 > 1) {
				p(1);
				interval();
				p(current - 1);
				p(current)
			} else {
				p(1);
				if (current - 1 > 1) {
					p(current - 1)
				}
				if (current > 1) {
					p(current)
				}
			} if (current + 2 < total) {
				p(current + 1);
				interval();
				p(total)
			} else {
				if (current + 1 < total) {
					p(current + 1)
				}
				if (current < total) {
					p(total)
				}
			} if (current < total) {
				next(current + 1)
			}
			return links
		}
	};
	astoria.sessionStorage = {};
	astoria.sessionStorage.setItem = Modernizr.sessionstorage ? function(key, val) {
		return sessionStorage.setItem(key, val)
	} : $.noop;
	astoria.sessionStorage.getItem = Modernizr.sessionstorage ? function(key) {
		return sessionStorage.getItem(key)
	} : $.noop;
	astoria.sessionStorage.removeItem = Modernizr.sessionstorage ? function(key) {
		return sessionStorage.removeItem(key)
	} : $.noop;
	astoria.localStorage = {};
	astoria.localStorage.setItem = Modernizr.localstorage ? function(key, val) {
		return localStorage.setItem(key, val)
	} : $.noop;
	astoria.localStorage.getItem = Modernizr.localstorage ? function(key) {
		return localStorage.getItem(key)
	} : $.noop;
	if (!$.isFunction(Array.prototype.find_all)) {
		Array.prototype.find_all = function(fn, thisObj) {
			var ret = [];
			for (var i = 0, j = this.length; i < j; ++i) {
				if (fn.call(this[i], this[i], i, this)) {
					ret.push(this[i])
				}
			}
			return ret
		}
	}
	var Scroller = astoria.ui.Scroller = {
		LOC: "searchScrollLocation",
		POS: "searchScrollPos"
	};
	Scroller.enable = function(event) {
		var p = astoria.util.ParameterUtils,
			params = p.toObject(window.location.search);
		if (!(astoria.Country.COUNTRY_SWITCH in params) && Scroller.retrieve(Scroller.LOC) !== undefined) {
			astoria.event.subscribe(event, function() {
				var pos = parseInt(Scroller.retrieve(Scroller.POS));
				if (pos) {
					window.scrollTo(0, pos)
				}
			}, true)
		}
		$(window).unload(function() {
			var pos = $(window).scrollTop();
			Scroller.store(pos, window.location.href)
		})
	};
	Scroller.retrieve = function(what) {
		return astoria.sessionStorage.getItem(what)
	};
	Scroller.store = function(pos, href) {
		astoria.sessionStorage.setItem(Scroller.POS, pos);
		astoria.sessionStorage.setItem(Scroller.LOC, href)
	};
	Scroller.scrollToTop = function() {
		$("html, body").animate({
			scrollTop: 0
		}, 500)
	};
	if (!$.isFunction(Array.prototype.map)) {
		Array.prototype.map = function(fn, ctx) {
			var ret = [],
				self = this;
			this.forEach(function(e, i) {
				ret.push(fn.call(ctx || window, e, i, self))
			});
			return ret
		}
	}
	astoria.remote = {
		updateOrCreatePaymentInfo: function(paymentInfo, success, error, extra) {
			var data, button, form;
			if ($.isPlainObject(paymentInfo)) {
				data = paymentInfo
			} else {
				if ($(paymentInfo).is("form")) {
					form = $(paymentInfo);
					data = this.doResolvePaymentInfo(form)
				} else {
					if ($(paymentInfo).is("button")) {
						button = $(paymentInfo);
						data = button.closest("form").serializeArray()
					}
				}
			}
			this.doUpdateOrCreatePaymentInfo(data, success, error, extra)
		},
		doUpdateOrCreatePaymentInfo: astoria.ajax.makeAjaxPost(astoria.conf.paymentInfoUrl, {
			complete: function() {}
		}),
		retrievePaymentInfo: function(id, success) {
			this.doRetrievePaymentInfo(id, success)
		},
		findPaymentInfo: function(criteria, success) {
			$.getJSON(astoria.conf.paymentInfoUrl, criteria, success)
		},
		deletePaymentInfo: function(id, success) {
			var url = astoria.conf.paymentInfoUrl;
			$("#deleteCreditCardId").val(id);
			var data = $("#deleteCreditCardForm").serializeArray();
			$.ajax({
				url: url,
				type: "post",
				data: data,
				success: success
			})
		},
		doRetrievePaymentInfo: function(id, success) {
			$.getJSON(astoria.conf.getPaymentInfoUrl + "?creditCardId=" + id, success)
		},
		doResolvePaymentInfo: function(form) {
			var paymentInfo = $(form).serializeArray();
			if ($.trim($(form).find("[name='update_credit_card_id']").val()) !== "") {
				paymentInfo.push({
					name: "addOrUpdatePayment",
					value: "update"
				})
			} else {
				paymentInfo.push({
					name: "addOrUpdatePayment",
					value: "add"
				})
			}
			return paymentInfo
		}
	};
	$.extend(astoria.util, {
		IntervalProxy: function(interval, fn) {
			var self = this;
			this.proxy = function() {
				if (self.lastCall === undefined || ((new Date()).getTime() - self.lastCall >= interval)) {
					fn.apply(this, arguments);
					self.lastCall = new Date()
				}
			}
		},
		PaymentUtils: {
			initExpiration: function() {
				var self = this;

				function show(show) {
					var expArea = $(".cc-expiration, .expiration-date, .sec-code", self.top);
					if (show) {
						if (!expArea.is(":visible")) {
							expArea.fadeIn()
						}
					} else {
						if (expArea.is(":visible")) {
							expArea.fadeOut()
						}
					}
				}
				$("#cc_type", self.top).change(function() {
					show($(this).val().toLowerCase() !== astoria.JCPENNEY)
				});
				self.showExpiration = function() {
					show($("#cc_type").val().toLowerCase() !== astoria.JCPENNEY)
				}
			}
		},
		ShippingInfo: {
			retrieve: function(id, success) {
				var addressId = $("#address-" + id).find("[name='update']").val();
				$("#updateAddressId").val(addressId);
				$.getJSON(astoria.conf.shippingInfoUrl + "?addressId=" + addressId, success)
			},
			retrieveAll: function(success) {
				$.getJSON(astoria.conf.shippingInfoUrl, success)
			},
			retrieve2: function(id, success) {
				$.getJSON("json/editShippingJSon.jsp", {
					addressId: id
				}, success)
			},
			retrieve3: function(id, success) {
				$.getJSON("json/editGiftCartShippingJSon.jsp", {
					addressId: id
				}, success)
			},
			save: function(shippingInfo, success) {
				shippingInfo.push({
					name: "addOrUpdateAddress",
					value: "add"
				});
				$.ajax({
					url: astoria.conf.shippingRequestUrl,
					data: shippingInfo,
					type: "post",
					dataType: "json",
					success: success
				})
			},
			update: function(id, shippingInfo, success) {
				if ($.isFunction(shippingInfo)) {
					success = shippingInfo;
					shippingInfo = $("#form_" + id.id).serializeArray()
				}
				if (!("is_default" in shippingInfo)) {
					shippingInfo.push({
						name: "addOrUpdateAddress",
						value: "update"
					})
				}
				$.ajax({
					url: astoria.conf.shippingRequestUrl,
					data: shippingInfo,
					type: "post",
					dataType: "json",
					success: success
				})
			},
			destroy: function(id, success) {
				var addressId = $("#address-" + id).find("[name='delete']").val();
				$("#deleteAddressId").val(addressId);
				var data = $("#deleteAddressForm").serializeArray();
				$.ajax({
					url: astoria.conf.removeShippingUrl,
					data: data,
					type: "POST",
					success: success
				})
			},
			destroy2: function(id, success) {
				$("#deleteAddressForm #deleteId").val(id);
				var data = $("#deleteAddressForm").serializeArray();
				$.ajax({
					url: astoria.conf.removeShippingUrl,
					data: data,
					type: "POST",
					success: success
				})
			}
		},
		BeautyInsiderProfile: {
			update: function(data, success) {
				$.ajax({
					url: astoria.conf.beautyInsiderUrl,
					type: "post",
					data: data,
					success: success,
					dataType: "json"
				})
			},
			personalizedRecommendation: function(success) {
				$.ajax(astoria.conf.personalizedPicks, {
					dataType: "json"
				}).done(success)
			}
		},
		Promotion: {
			PROMOTION_APPLIED: "promotion_applied"
		}
	});
	astoria.ShoppingCart.mixInEntity = function(cart) {
		if ($.isPlainObject(cart)) {
			$.extend(cart, astoria.ShoppingCart);
			if ("line_items" in cart) {
				cart.line_items.forEach(function(li) {
					astoria.Sku.mixInEntity(li.sku)
				})
			}
		}
	};
	astoria.util.MyList.moveToMyList = astoria.ajax.makeAjaxPost(astoria.conf.myListMoveToMyListUrl, {
		success: astoria.ShoppingCart.mixInEntity,
		complete: function() {}
	});
	$.extend(astoria.util.CountrySupport, {
		stateMap: {},
		waitingCallers: {},
		callStatus: {},
		initCountrySupport: function(options) {
			var self = this,
				settings = $.extend({
					controller: self,
					billingState: "#state",
					stateLabel: "state",
					zipLabel: "zip",
					countrySelect: $("[name='country']", this.top)
				}, options || {}),
				freeState, dropDownStates;
			var displayNames = {
				us: "United States",
				ca: "Canada",
				ru: "Russia",
				nz: "New Zealand",
				br: "Brazil",
				au: "Australia",
				uk: "UK",
				cn: "China",
				dk: "Denmark"
			};
			var stateMap = this.stateMap;
			var callers = this.waitingCallers;
			var callStatus = this.callStatus;
			var mapStates = function(states) {
				if (typeof states === "undefined") {
					return ""
				}
				return states.map(function(state) {
					return '<option value="' + state.code + '">' + state.title + "</option>"
				}).join("")
			};
			var initStates = function(ctry, done) {
				if (typeof stateMap[ctry] !== "undefined") {
					return done ? done() : 0
				}
				callers[ctry] || (callers[ctry] = []);
				if (done) {
					callers[ctry].push(done)
				}
				if (ctry in callStatus) {
					return
				}
				callStatus[ctry] = "inprogress";
				astoria.Country.find(ctry, function(c) {
					stateMap[ctry] = {
						states: mapStates(c.states),
						statesLabel: c.states_label,
						zipLabel: c.zip_label
					};
					while (callers[ctry].length) {
						callers[ctry].shift().call()
					}
				})
			};

			function displayName(countryCode) {
				countryCode = countryCode.toLowerCase();
				return displayNames[countryCode]
			}(function() {
				dropDownStates = $(settings.billingState, self.top);
				var name = dropDownStates.attr("name");
				var id = dropDownStates.attr("id");
				freeState = $('<input id="' + id + '" name="' + name + '" maxlength="20" type="text" class="form-control" />')
			})();
			initStates("us");
			$.extend(self, {
				localize: function(fn) {
					self.handler.call($(settings.countrySelect, self.top), fn)
				},
				countryStaticText: function(isStatic) {
					var toggle = isStatic ? "hide" : "show";
					var sel = $(settings.countrySelect, this.top),
						id = sel.attr("id");
					sel[toggle]();
					$("label[for='" + id + "']", this.top).siblings(".country-static").html(isStatic ? "<span>" + displayName(sel.val()) + "</span>" : "")[isStatic ? "show" : "hide"]()
				},
				setCountryToCurrent: function() {
					var country = astoria.util.CookieUtils.read_cookie(astoria.conf.localeCookie) || "us",
						val = country.toUpperCase().slice(0, 2);
					$(settings.countrySelect, self.top).val(val);
					return this
				},
				handler: function(fn) {
					var ctry = ($.isPlainObject(this) ? this : $(this)).val().toLowerCase();
					initStates(ctry, function() {
						var country = stateMap[ctry],
							states = country.states,
							isFreeState = !states,
							origLength = dropDownStates.find("option").length;
						$("[for='" + settings.stateLabel + "'] span:last").text(country.statesLabel);
						$("[for='" + settings.zipLabel + "'] span:last").text(country.zipLabel);
						self.updatePaymentMethods(ctry);
						if (dropDownStates.find("option:last").text() !== $($(states).get(-1)).text()) {
							$(states).appendTo(dropDownStates);
							if (origLength > 0) {
								dropDownStates.find("option:lt(" + origLength + ")").remove()
							}
							dropDownStates.find("option:first").attr("selected", "selected")
						}(function(one, other) {
							if (one.parents().length === 0) {
								one.insertAfter(other)
							}
							other.remove()
						}).apply(this, isFreeState ? [freeState, dropDownStates] : [dropDownStates, freeState]);
						$(settings.countrySelect).show();
						$("body").trigger("countryChange.astoria", [ctry]);
						if ($.isFunction(fn)) {
							fn()
						}
					})
				},
				updatePaymentMethods: (function() {
					var usOnly;
					return function(newVal) {
						usOnly = usOnly || $(this.cc_type + " option", this.top).filter(function() {
							return [astoria.JCPENNEY, "discover"].contains($(this).text().toLowerCase())
						});
						usOnly.prop("disabled", newVal !== astoria.US)
					}
				})()
			});
			$(settings.countrySelect, self.top).change(self.handler)
		},
		getCurrentCountry: function() {
			return astoria.util.ParameterUtils.extractParam("country") || astoria.util.CookieUtils.currentCountry() || astoria.US
		}
	});
	$.extend(astoria.util.Login, {
		hardLogOut: function(success) {
			$.ajax({
				url: "/logins/logout",
				type: "GET",
				dataType: "json",
				data: {
					hard: true
				},
				success: success
			})
		}
	});
	astoria.ShoppingCart.retrieve = function(success) {
		$.ajax("/global/json/ajaxResponse.jsp", {
			cache: false,
			dataType: "json",
			data: {
				buildAllItems: true
			}
		}).done(astoria.ShoppingCart.mixInEntity).done(success)
	};
	astoria.ShoppingCart.remove = function(items, successFn, errorFn, options) {
		if ($.isPlainObject(errorFn)) {
			options = errorFn;
			errorFn = undefined
		}
		this.doRemove(items, successFn, errorFn, options)
	};
	astoria.ShoppingCart.doRemove = astoria.ajax.makeAjaxPost(astoria.conf.cartRemoveItemUrl, {
		success: function(cart) {
			$.extend(cart, astoria.ShoppingCart)
		}
	});
	astoria.ShoppingCart.update_quantities = function(items, successFn, errorFn) {
		this.doUpdateQuantities(items, successFn, errorFn)
	};
	astoria.ShoppingCart.doUpdateQuantities = astoria.ajax.makeAjaxPost(astoria.conf.cartUpdateQuantUrl, {
		success: function(cart) {
			$.extend(cart, astoria.ShoppingCart)
		}
	});
	astoria.ShoppingCart.addSamples = function(cartContents, success, error, extra) {
		this.doAddSamples(cartContents, success, error, extra)
	};
	astoria.ShoppingCart.doAddSamples = function(items, success, error, extra) {
		$.ajax({
			url: astoria.conf.cartAddSamplesUrl,
			type: "post",
			dataType: "json",
			data: $.extend(items, extra || {}),
			error: error
		}).done(astoria.ShoppingCart.mixInEntity).done(success)
	};
	astoria.ShoppingCart.applyGiftCard = function(card, success) {
		$.ajax(this.giftCardUrl(), {
			type: "post",
			data: card
		}).done(astoria.ShoppingCart.mixInEntity).done(success)
	};
	astoria.ShoppingCart.removeGiftCard = function(card, success, format) {
		format = format || "json";
		card.apply = false;
		$.post(this.giftCardUrl(), card, success, format)
	};
	astoria.ShoppingCart.giftCardUrl = function() {
		return astoria.conf.applyGiftCardUrl
	};
	astoria.ShoppingCart.retrieveGiftCard = function(card, success, format) {
		format = format || "json";
		return $.ajax(astoria.conf.retrieveGiftCard, {
			data: card,
			success: success,
			dataType: format,
			cache: false
		})
	};
	astoria.ShoppingCart.applyPromo = astoria.ajax.makeAjaxPost(astoria.conf.promotionUrl, {
		dataType: "",
		success: astoria.ShoppingCart.mixInEntity
	});
	astoria.ShoppingCart.removePromo = function(promo) {
		var ajax = astoria.ajax.makeAjaxDelete(astoria.conf.promotionUrl + "/" + encodeURIComponent(promo.promo_code));
		return ajax()
	};
	astoria.ShoppingCart.removePromoVerbose = function(promo) {
		var ajax = astoria.ajax.makeAjaxDelete("/rest/user/cart/checkoutpromo/" + encodeURIComponent(promo.promo_code));
		return ajax()
	};
	astoria.ShoppingCart.removeAllSamples = function(success, error) {
		var params = astoria.util.HtmlUtils.getFormData("noSampleFormId");
		$.ajax({
			url: astoria.conf.cartRemoveAllSamplesUrl,
			type: "post",
			data: params,
			success: success,
			error: error
		})
	};
	$.extend(astoria.util.MapUtils, {
		repositionMap: (function() {
			var theMap;
			return function(myLatlng, radMiles) {
				if (typeof theMap === "undefined") {
					var myOptions = {
						scrollwheel: false,
						zoom: this.guessZoom(radMiles),
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					theMap = new google.maps.Map(document.getElementById("map-canvas"), myOptions)
				} else {
					theMap.setCenter(myLatlng)
				}
				return theMap
			}
		})(),
		guessZoom: function(options) {
			var radMiles = options.miles;
			if ((/mile/i).test(options.unit)) {
				return radMiles === 0 ? 14 : radMiles === 5 ? 12 : radMiles === 10 ? 11 : radMiles === 25 ? 9 : radMiles === 50 ? 8 : 7
			} else {
				return radMiles < 7 ? 13 : radMiles < 16 ? 11 : radMiles < 32 ? 10 : radMiles < 63 ? 9 : radMiles < 94 ? 8 : 7
			}
		},
		storeNameFrom: function(store) {
			var top = $(store).closest(".storehq-result").length > 0 ? $(store).closest(".storehq-result") : $(store).closest(".storehq-detail").length > 0 ? $(store).closest(".storehq-detail") : $(store);
			return top.find(".store-name span").length > 0 ? top.find(".store-name span:first").text() : top.find(".store-name").text()
		}
	});
	astoria.Purchase = {
		itemCount: function() {
			return (this.purchase_items || []).length
		},
		dateFromIsoDate: function() {
			var iso = this.purchase_date;
			if (!iso) {
				return ""
			}

			function pad(n) {
				return n < 10 ? "0" + n : n
			}
			var d, parsed = Date.parse(iso);
			if (isNaN(parsed)) {
				var mo = parseInt(iso.slice(5, 7)) - 1;
				var d = parseInt(iso.slice(8, 10));
				var y = parseInt(iso.slice(0, 4));
				d = new Date(y, mo, d)
			} else {
				d = new Date(parsed)
			}
			return pad(d.getMonth() + 1) + "/" + pad(d.getDate()) + "/" + d.getFullYear()
		}
	};
	$.extend(astoria.User, {
		NON_BI: "NON_BI",
		BI: "BI",
		VIB: "VIB",
		ROUGE: "ROUGE",
		BI_DOWN: "BI_DOWN",
		addBiSkinTone: function(shade, success, fail) {
			$.ajax("/rest/user/bi/skintones", {
				type: "post",
				complete: function() {},
				data: {
					shade_code: shade
				}
			}).done(success).fail(fail)
		},
		update: astoria.ajax.makeAjaxPost(astoria.conf.updateUserUrl),
		purchases: function(opts) {
			return $.ajax("/rest/user/purchases", {
				data: opts,
				complete: function() {},
				cache: false
			}).done(function(resp) {
				if ("purchase_items" in resp) {
					var pp = resp.purchase_items;
					delete resp.purchase_items;
					resp.purchases = [{
						purchase_items: pp
					}]
				}(resp.purchases || []).forEach(function(p) {
					$.extend(p, astoria.Purchase);
					(p.purchase_items || []).forEach(function(i) {
						astoria.Sku.mixInEntity(i)
					})
				})
			})
		},
		recentOrders: function(success) {
			$.get(astoria.conf.userUrl + "/recent_orders", success)
		},
		emailListToFriend: function(data, success) {
			var params = astoria.util.HtmlUtils.getFormData("sendMyList");
			$.post(astoria.conf.myListEmailToFriendUrl, params, success)
		}
	});
	astoria.User.addBiSkinTone = astoria.User.addBiSkinTone.createAuthenticatingInterceptor(astoria.User, {
		redirect: false
	});
	astoria.ui.RenderLoves = {
		noShipMsg: astoria.i18n.t("sku.notAvailable"),
		renderNoItems: function() {
			var html = astoria.template.views2[astoria.util.CookieUtils.isSignedInOrRecognizedUser() ? "noLoveItemsSignedIn" : "noLoveItems"]();
			$(this.listParent, this.scope).replaceWith(html)
		},
		normalRender: function(listParent, success, opts) {
			var scope = this.scope,
				self = this,
				CookieUtils = astoria.util.CookieUtils,
				isSignedIn = CookieUtils.isSignedInOrRecognizedUser();
			var views = astoria.template.views2;
			if (isSignedIn) {
				astoria.MyList.all(opts).done(function(resp) {
					resp || (resp = {
						favorites: []
					});
					astoria.logger.log(resp);
					var myLoves, toRender;
					if (resp && (myLoves = resp.favorites) && myLoves.length) {
						if ("setModel" in self) {
							self.setModel(resp)
						}
						if ("setCollection" in self) {
							self.setCollection(myLoves)
						}
						$(".pane-head", scope).removeClass("hidden");
						toRender = myLoves.slice(0, Math.min(self.SHOWPERPAGE, myLoves.length) || myLoves.length);
						toRender.forEach(self.populateListItem, self);
						$(listParent, scope).empty().append(views[self.template](toRender));
						astoria.jail.lazyImages(".product-list img[data-src]")
					} else {
						self.renderNoItems()
					} if ($.isFunction(success)) {
						success(resp)
					}
				})
			} else {
				self.renderNoItems();
				if ($.isFunction(success)) {
					success()
				}
			}
		},
		simpleRender: function(listParent, success, opts) {
			var scope = this.scope,
				myLoves, self = this,
				views = astoria.template.views2;
			var id = astoria.util.ParameterUtils.extractParam("token") || (location.pathname).split("/").pop();
			astoria.MyList.retrieveSharedFavorites(id, opts).done(function(resp) {
				astoria.logger.log("got shared favorites");
				resp || (resp = {
					favorites: []
				});
				astoria.logger.log(resp);
				if (resp && (myLoves = resp.favorites) && myLoves.length) {
					myLoves.forEach(self.populateListItem, self);
					$(listParent, scope).empty().append(views[self.template](myLoves));
					astoria.jail.lazyImages(".product-list img[data-src]")
				} else {
					self.renderNoItems()
				} if ($.isFunction(success)) {
					success(resp)
				}
			})
		},
		populateListItem: function(sku, i) {
			var self = this,
				varType = sku.primary_product.variation_type,
				hasVarType = varType != "None",
				price = (sku.sale_price ? sku.sale_price : sku.list_price),
				prodId = sku.primary_product.id;
			sku.image = sku.imageFor(self.images);
			sku.productName = sku.primary_product.brand_name + " " + sku.primary_product.display_name;
			sku.showSize = sku.sku_size && varType != "Size";
			sku.hasVariation = sku.variation_value && hasVarType;
			sku.productId = prodId;
			sku.noShipMsg = this.noShipMsg;
			if (astoria.meta.is_pinterest) {
				sku.enablePin()
			}
			sku.price = this.printPrice(sku);
			if (sku.hasVariation) {
				sku.varClazz = varType.toLocaleString().toLowerCase();
				sku.varLabel = varType
			}
			sku.is_loved = true;
			sku.forceLoad = i < 12
		},
		display: function(listParent, success, opts) {
			opts = $.extend({}, opts, ("meta" in astoria ? {} : {
				meta: true
			}));
			this.doRender(listParent, success, opts)
		}
	};
	$.extend(astoria.ui.controller, {
		PromotionController: {
			initPromos: function(options) {
				var promoInput = $("#promo_code", this.top),
					isCheckout = !! $(".checkout-module").length;
				options = options || {};
				if (typeof options.errorsBefore === "undefined") {
					options.errorsAfter = options.errorsAfter || "label[for='promo_code']"
				}
				options.scope || (options.scope = promoInput.closest(".container"));
				var self = this,
					ajaxIndicator = $(".current, .maincontent")[0],
					promoErrorBuilder = new astoria.ui.error.ErrorBuilder({
						topElement: options.scope,
						globalInsertBefore: options.errorsBefore,
						globalInsertAfter: options.errorsAfter
					}),
					msErrorBuilder = new astoria.ui.error.ErrorBuilder({
						topElement: "#sku-promo",
						globalInsertAfter: ".modal-title",
						renderFieldError: function() {
							return true
						}
					}),
					button = $(".basket-summary .promotion-box .code-entry .btn-apply, button#apply-code"),
					buttonPopupPromo = $(".promo-component button");
				this.promoErrorBuilder = promoErrorBuilder;
				$("body").on("click", "#sku-promo .btn-cancel", function() {
					$("#sku-promo").find(".icon-close").click()
				});
				var bindMSPForm = function() {
					$("#sku-promo form").submit(function() {
						self.submitMsp($(this));
						return false
					})
				};
				button.prev("input:text").keydown(function(e) {
					if (e.keyCode === 13) {
						e.preventDefault();
						$(button).disableUntilAjaxComplete();
						self.applyPromo()
					}
				});
				button.click(function(e) {
					e.preventDefault();
					$(button).disableUntilAjaxComplete();
					self.applyPromo()
				});
				buttonPopupPromo.click(function(e) {
					e.preventDefault();
					astoria.util.PopupUtils.closePopups();
					$("#promo_code").val($(this).attr("promocode"));
					$(buttonPopupPromo).disableUntilAjaxComplete();
					self.applyPromo();
					$("#promo_code").val("")
				});
				$.extend(self, astoria.util.HtmlUtils, astoria.util.PopupUtils, {
					applyPromo: function() {
						promoErrorBuilder.clear();
						if (options.basketErrors) {
							options.basketErrors.clear()
						}
						var data = {
							promo_code: $("#promo_code").val()
						};
						if (isCheckout) {
							data.is_from_checkout = true
						}
						astoria.ShoppingCart.applyPromo(data).done(self.updateForPromo)
					},
					submitMsp: function(form) {
						msErrorBuilder.show();
						if (form.find("input:checked").length === 0) {
							return
						}
						var items = form.serializeArray();
						items.push({
							name: "is_from_checkout",
							value: !! $(".checkout-module").length
						});
						astoria.ShoppingCart.add(items, function(cart) {
							msErrorBuilder.withNoErrors(cart.errors, function() {
								$("#sku-promo").fadeOut(function() {
									$(this).remove()
								});
								promoInput.val("");
								self.updatePromoMessage(cart, cart.promo_message);
								self.refreshBasketArea(cart, {
									isFromPromo: true
								})
							})
						}, {
							include_skus: true
						})
					},
					doRemovePromotion: function(data) {
						data.is_from_checkout = isCheckout;
						promoErrorBuilder.clear();
						if (options.basketErrors) {
							options.basketErrors.clear()
						}
						astoria.ShoppingCart[isCheckout ? "removePromoVerbose" : "removePromo"](data).done(function(cart) {
							promoErrorBuilder.withNoErrors(cart.errors);
							if (options.basketErrors && "errors" in cart && "section" in cart.errors) {
								options.basketErrors.withNoErrors({
									global: cart.errors.section.basket
								})
							}
							self.refreshBasketArea(cart, {
								isFromPromo: true
							});
							self.removePromoMessage();
							$("body").trigger(astoria.util.Promotion.PROMOTION_APPLIED, [cart])
						})
					},
					updateForPromo: function(resp, _, xhr) {
						var input = promoInput,
							code = input.val();
						astoria.ajax.stopWait(ajaxIndicator);
						self.jsonOrHtml(resp, function json(cart) {
							if (cart.errors !== undefined && cart.errors.global !== undefined && cart.errors.global.length > 0) {
								$("body").trigger("promoCode", [{
									promocode: input.val(),
									promocode_status: cart.errors.global[0],
									promocode_location: input.attr("class").split(" ")[0]
								}])
							}
							promoErrorBuilder.withNoErrors(cart.errors);
							if (options.basketErrors && "errors" in cart && "section" in cart.errors) {
								options.basketErrors.withNoErrors({
									global: cart.errors.section.basket
								})
							}
							if (xhr.status === 200) {
								$("body").trigger("promoCode", [{
									promocode: input.val(),
									promocode_status: "OK",
									promocode_location: input.attr("class").split(" ")[0]
								}]);
								input.val("");
								self.updatePromoMessage(cart, code);
								self.refreshBasketArea(cart, {
									isFromPromo: true,
									code: code
								});
								$("body").trigger(astoria.util.Promotion.PROMOTION_APPLIED, [cart, {
									code: code
								}])
							}
						}, function() {
							$("body").trigger("promoCode", [{
								promocode: input.val(),
								promocode_status: "OK",
								promocode_location: input.attr("class").split(" ")[0]
							}]);
							self.displayCenteredPopup(resp, {
								onClose: function() {
									$("#sku-promo").remove()
								}
							});
							bindMSPForm()
						})
					}
				})
			}
		},
		BirewardsController: function(options) {
			var self = this,
				MAX_QUANTITY = 5,
				top = options.top,
				isBirthDay, biStatus = $("meta[name='bi_status']").attr("content").toLowerCase(),
				errorBuilder, Sku = astoria.Sku;
			if ($("#bierrors").length) {
				errorBuilder = new astoria.ui.error.ErrorBuilder({
					topElement: top,
					globalInsertAfter: "#bierrors",
					renderFieldError: function(error) {
						return false
					}
				})
			}
			self.scope = top;
			self.instance = "BirewardsController";
			$("body").bind("skuRemoved2.basket", function(_, skuId, cart) {
				self.updateBIButtons(cart.available_bipoints, top, biStatus);
				if (parseInt($("#add-bi-reward" + skuId).closest(".product-item").attr("points")) === 0) {
					self.updateBirthday(skuId, true, biStatus)
				}
				if ($("form#remove-bi-reward" + skuId).parents("div.vib-kit").length) {
					$("form#remove-bi-reward" + skuId).parent().hide();
					$("form#add-bi-reward" + skuId).parent().show()
				}
				self.updateBeautyBankPoints(cart)
			});
			$.extend(self, new astoria.ui.controller.ShoppingCartController({
				scope: top
			}), astoria.util.HtmlUtils, {
				resolveItems: function(source) {
					isBirthDay = parseInt($(source).closest(".product-item").attr("points")) === 0;
					return $(source).closest("form").serializeArray()
				},
				updateBirthday: function(skuId, isRemove, biStatus) {
					var adds, removes;
					if (isRemove) {
						removes = $("div[points='0'] div button.btn-remove");
						adds = $("div[points='0'] div:hidden button.btn-add-to-basket");
						$("div[points='0'] button.btn-add-to-basket").each(function() {
							if (self.biTierMatrix[$(this).closest(".product-item").data("tier").toLowerCase()] <= self.biTierMatrix[biStatus]) {
								$(this).prop("disabled", false)
							}
						});
						adds.closest("div").show();
						removes.closest("div").hide()
					} else {
						$(".add-bi-reward" + skuId).parent().hide();
						$(".remove-bi-reward" + skuId).parent().show();
						$("div[points='0'] button.btn-add-to-basket").prop("disabled", true)
					}
				},
				onAdd: function(source, cart) {
					var sku;
					if (!("errors" in cart)) {
						if (isBirthDay) {
							self.updateBirthday(self.skuIdFrom(source), false, biStatus)
						} else {
							self.updateBIButtons(cart.available_bipoints, top, biStatus);
							if (cart.line_items[0].quantity >= MAX_QUANTITY) {
								$(source).prop("disabled", true)
							} else {
								if (Sku.isWelcomeKit(sku = cart.line_items[0].sku)) {
									$("form#remove-bi-reward" + sku.sku_number).parent().show();
									$("form#add-bi-reward" + sku.sku_number).parent().hide()
								}
							}
						}
					}
				}
			}).addToBasket(".btn-add-to-basket").onSuccessfulAdd(self.onAdd);
			if (errorBuilder) {
				self.showErrorsWith(errorBuilder)
			}
			$(top).on("click", ".btn-remove", function(e) {
				var source = $(this),
					lineitem = self.resolveItems($(source));
				self.removeLineitems(lineitem, function(cart) {
					var skuId = self.skuIdFrom(source);
					self.updateBirthday(skuId, true, biStatus);
					if (source.parents("div.vib-kit").length) {
						$("form#remove-bi-reward" + skuId).parent().hide();
						$("form#add-bi-reward" + skuId).parent().show()
					}
					astoria.analytics.analyze("removeFromBasket", [cart.line_items])
				}, {
					include_skus: true
				});
				e.preventDefault()
			});
			$(".bi-points-box", this.scope).click(function() {
				var b = $(this);
				b.hasClass("open") ? b.removeClass("open").find("h4 span").text("+") : b.addClass("open").find("h4 span").html("&ndash;")
			});
			$(".bi-rewards-component [data-productid], .mod-recommend [data-productid]", self.scope).click(function(e) {
				astoria.analytics.analyze("sendData", ["pfm", "user profile"])
			})
		}
	});
	$.extend(astoria.ui, {
		Store: {
			findInStore: astoria.ajax.makeAjaxGet(astoria.conf.storesUrl, {
				dataType: "html"
			})
		}
	});
	$.extend(astoria.ui.product, {
		initTabs: function(defaultIndex) {
			if (defaultIndex !== undefined) {
				$("div.panes").each(function() {
					$($(this).children()[defaultIndex]).show()
				});
				$("ul.tabs").each(function() {
					$($(this).children()[defaultIndex]).find("a").addClass("current")
				})
			}
			$("ul.tabs li a").click(function(e) {
				e.preventDefault();
				var index = 0;
				var tab = $(this).parent();
				tab.parent().find("a").removeClass("current");
				$(this).addClass("current");
				while (tab.prev().length > 0) {
					index++;
					tab = tab.prev()
				}
				var panes = tab.closest(".tabs").next();
				panes.children().hide();
				$(panes.children()[index]).show()
			})
		}
	});
	$.extend(astoria.validation, {
		DynamicZipValidator: function(opts) {
			return {
				validate: function(errors) {
					var name, msg, zip = $(opts.field, opts.scope).val().trim(),
						cntry = $(opts.country, opts.scope).val().toLowerCase(),
						ok = true;
					switch (cntry) {
						case "us":
							ok = astoria.conf.US_ZIP_REG.test(zip);
							break;
						case "ca":
							ok = astoria.conf.CAREG.test(zip);
							break
					}
					if (!ok) {
						name = $(opts.field).attr("name");
						msg = astoria.i18n.t("validation.zip.invalid");
						errors.field = errors.field || {};
						(errors.field[name] || (errors.field[name] = [])).push(msg)
					}
				}
			}
		},
		ZipCodeValidator: function(zipField, settings) {
			var re, USREG = astoria.conf.US_ZIP_REG,
				CAREG = astoria.conf.CAREG,
				options = $.extend({
					i18nKey: "validation.zip.invalid",
					anyCountry: false
				}, settings || {}),
				validationMsg = astoria.i18n.t(options.i18nKey);
			$.extend(this, astoria.util.CookieUtils, {
				validate: function(errors, env) {
					env = env || {};
					var field = $(env.zipField || zipField, env.top || "body"),
						name = field.attr("name"),
						zip = field.val().toUpperCase();
					if ((options.anyCountry && !USREG.test(zip) && !CAREG.test(zip)) || (!options.anyCountry && !re.test(zip))) {
						errors.field = errors.field || {};
						(errors.field[name] = errors.field[name] || []).push(validationMsg)
					}
				}
			});
			re = this.currentCountry() === "us" ? USREG : CAREG
		},
		ItemQuantityValidator: function(options) {
			options = options || {};
			if (!("global" in options)) {
				options.global = true
			}
			$.extend(this, {
				validate: function(errors, env) {
					var QUANT_MSG = astoria.i18n.t("basket.sku.quantity.invalid");
					env = env || {};
					if (env.isCollection || env.isMyList) {
						return
					}
					var totalQuantity = 0,
						quant;
					this.quantityFields(env, function(name, quantity) {
						try {
							totalQuantity += /^([0-9]{1,2})$/.test(quantity) ? parseInt(quantity) : addError(QUANT_MSG)
						} catch (e) {
							addError(QUANT_MSG)
						}

						function addError(msg) {
							errors.field = errors.field || {};
							errors.field[name] = errors.field[name] || [];
							errors.field[name].push(msg);
							return 0
						}
					});
					if (totalQuantity === 0 && options.global) {
						errors.global = errors.global || [];
						errors.global.push(QUANT_MSG)
					}
				},
				quantityFields: function(env, fn) {
					if ("quantity" in options) {
						$(options.quantity).each(function() {
							fn($(this).attr("name"), $(this).val())
						})
					} else {
						fn(env.name, env.quantity)
					}
				}
			})
		}
	});
	astoria.ui.controller.MAX_LOVES_PER_PAGE = 60;
	astoria.ui.controller.PaginationController = function(opts) {
		if ("instance" in astoria.ui.controller.PaginationController) {
			return
		}
		this.scope = $("div.page-numbers");
		var scope = this.scope,
			self = this,
			PERPAGE = astoria.ui.controller.MAX_LOVES_PER_PAGE,
			Events = astoria.event;
		astoria.ui.controller.PaginationController.instance = this;
		var buildLinks = astoria.Page.buildLinks;
		self.render = function(data) {
			var views = astoria.template.views2,
				total = data.total;
			scope.empty();
			if (total <= PERPAGE) {
				return
			}
			var links = buildLinks(data.page, parseInt(total / PERPAGE) + (total % PERPAGE ? 1 : 0));
			scope.append(views.pagination(links))
		};
		$(scope).on("click", "li a", function(e) {
			e.preventDefault();
			Events.publish("pagination", [parseInt($(this).attr("data-page"))]);
			$("html, body").animate({
				scrollTop: 0
			}, 500)
		})
	}
})(jQuery);