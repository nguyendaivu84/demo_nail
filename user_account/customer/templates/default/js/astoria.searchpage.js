//     astoria Search
//     (c) 2012  S E P H O R A
(function($) {
  "use strict"

  var controller, categories, resultsController = astoria.search.prodResultsController,
    criteria = astoria.search.getCriteria();
  var chanelPTypes = ['ALLURE HOMME SPORT EAU EXTRÊME','ALLURE HOMME SPORT','ALLURE HOMME','ALLURE','BLEU DE CHANEL','CHANCE EAU TENDRE','CHANCE EAU FRAÎCHE','CHANCE','COCO MADEMOISELLE','COCO','N°5 EAU PREMIÈRE','EAU PREMIÈRE','N°5','PLATINUM EGOISTE'];

  astoria.event.subscribe("chanelProductsReady", function(_, products) {
    products.forEach(function(prod) {
      for(var index=0; index < chanelPTypes.length; index++) {
        if(prod.display_name.indexOf(chanelPTypes[index]) > -1){
          prod.display_name = prod.display_name.substring(0, chanelPTypes[index].length)+'<br>'+prod.display_name.substring(chanelPTypes[index].length);
          break;
        }
      }
    });
  });

  astoria.app.initializer(function() {
    astoria.search.searchController = controller = new astoria.ui.controller.SearchController(criteria);
    categories = astoria.search.categories = new astoria.search.Categories(controller);

    controller.onAppInit();
    resultsController.init();
    categories.init();
  });

})(jQuery);