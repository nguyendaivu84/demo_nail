//     astoria Search
//     (c) 2010-2013  S E P H O R A
(function($) {
	"use strict"
	var controller,
		breadCrumbs,
		currBrandId,
		brandCnt,
		pagination,
		refinements,
		criteria,
		loc = window.location.href,
		yourChoices,
	resultsController,
		views,
		hbviews,
		meta,
		ADDREF = "addRef",
		REMOVEREF = "removeRef",
		REMOVECAT = "removeCat",
		REMOVEALL = 'removeall',
		BRANDID = "brandId",
		BRANDNAME = "brandName",
	hiddenRefs = [],// move to SkinIQ
		logger,
		userAgent = window.navigator.userAgent,
		isSafari = /Safari/.test(userAgent),
		isUserAction = false,
		CookieUtils = astoria.util.CookieUtils,
		Events = astoria.event,
		MyList = astoria.util.MyList,
	Scroller = astoria.ui.Scroller;

	astoria.util.CookieUtils.writeCookieMillis("refinements", window.location.href);
	astoria.module("search");

	var nameFromPath = function() {
		return window.location.pathname.replace(/\//g, "");
	};

  // Subscribe the scroller for views with a product grid
  if ($('.product-grid').length > 0) {
	Scroller.enable("productsReady");
  }

  astoria.util.ParameterUtils.sanitizeUrl(); // call this after Scroller, probably this should be done on most pages

	$.fn.hideRemove = function(fn) {
		!$.isFunction(fn) ? fn = $.noop : 0;
		var s = $(this);
		s.length === 0 ? fn() : s.fadeOut(function() {
			s.remove();
			fn();
		});
		return s;
	};

  $.fn.fadeInIfNotTouch = Modernizr.touch? function(d, fn) {
	  fn( $(this).show() );
	  return this;
	} : $.fn.fadeIn;

  $.fn.fadeOutIfNotTouch = Modernizr.touch? function(fn) {
	  fn( $(this).hide() );
	  return this;
	} : $.fn.fadeOut;

  // ENTITY
	astoria.Category.isCurrent = function() {
	return this.is_selected;
	};

  astoria.Category.isClickable = function() {
	return ("is_clickable" in this)? this.is_clickable : true;
  };

  astoria.Category.nameIfLevel2 = function() {
	return this.isLevel2()? this.name : undefined;
  }

  astoria.Category.isLevel2 = function() {
	return this.level===2;
  }

  astoria.Category.hasCategories = function() {
	return "sub_categories" in this;
  }
  // END ENTITY

	astoria.search.diffs = function(id, first, second) {
		var c, added = [],
			removed = [];
		if(Array.prototype.slice.call(arguments, 0).length === 2) {
			second = first;
			first = id;
			c = function(one, other) {
				return one === other;
			}
		} else {
			c = function(one, other) {
				return one[id] === other[id];
			}
		}

		second.forEach(function(other) {
			if(!first.some(function(one) {
				return c(one, other);
			})) {
				added.push(other);
			}
		});
		first.forEach(function(one) {
			if(!second.some(function(other) {
				return c(one, other);
			})) {
				removed.push(one);
			}
		});
		return {
			added: added,
			removed: removed
		};
	};

  astoria.search.diffWithPageLoad = function(first, second) {
	var ret = astoria.search.diffs(first, second);
	if(ret.added.length===0 && ret.removed.length===0) {
	  ret.added = first;//choose any, this is page load
	}
	return ret;
  }

	criteria = (function() {
	var defHints = {
			}, hints = {},
	  searchType;

			$.extend(hints, defHints);

	return {
	  ref: [],
		numberParams: (function() {
	  var ret = "brandId currentPage node pageSize pl ph".split(" ");
	  return function() { return ret };
	})(),
		populate: function(init) {
			var data, prop, hash = window.location.hash.replace(/^#!?/, '');
			for(prop in init) {
				if(prop && !$.isFunction(init[prop])) {// criteria has helper functions, do not remove
					delete init[prop];
				}
			}
			data = astoria.util.ParameterUtils.toObject(hash ? hash : window.location.search);
			var data2 = astoria.util.ParameterUtils.toObject(window.location.search);
			if("MobileOptOut" in data2) {
				data.MobileOptOut = data2.MobileOptOut;
			}
			this.normalize(init, data);
		},

	searchType: function() {
	  return searchType||(searchType=$("meta[name='searchtype']").attr("content"));
	},

	isChanel: function() {
	  return this.searchType()==="brand" && this.nameFromPath()==="chanel";
	},

	pathnameFromName: function() {
	  var ret="";
	  switch(this.searchType()) {
		case "category":
		  if(this.categoryName) { ret = "/"+this.categoryName; }
		  break;
		case "brand":
		  if(this.brandName) { ret = "/"+this.brandName; }
		  break;
	  }
	  return ret;
	},

	toSearchParams: function() {
	  var p = $.extend({}, this);
	  delete p.categoryName;// these are in the pathname
	  delete p.brandName;
	  return astoria.util.ParameterUtils.toBookmark(p);
	},

	normalize: function(init, data) {
			var numberParams = this.numberParams(),
				prop;

	  astoria.util.ParameterUtils.sanitizeData(data);
			if(typeof (data["ref"]) === "string") {
				data["ref"] = [parseInt(data["ref"])];
			} else if($.isArray(data.ref)) { //only tolerate integers
				data.ref = data.ref.map(function(e) {
					return parseInt(e);
				});
			}
			for(prop in data) {
				if(prop && numberParams.indexOf(prop) > -1) {
					data[prop] = parseInt(data[prop]);
				}
			}
			$.extend(init, data);
			init["ref"] || (init["ref"] = []);
			switch($("meta[name='searchtype']").attr("content")) { // use searchType()
				case "category":
					if(!("categoryName" in init) && !("categoryId" in init) && !("node" in init)) {
						init.categoryName = this.nameFromPath();
					}
					break;
				case "brand":
					if(!("brandId" in init) && !("brandName" in init)) {
						init.brandName = this.nameFromPath();
					}
					break;
				case "sale":
				case "gc":
					init[$("meta[name='searchtype']").attr("content")] = true;
					break;
				case "contentGroup":
					init.contentGroup = $("meta[name='contentGroup']").attr("content");
			}
		},

		initialize: function(state) {
	  var init = state||this;
			this.populate(init);
		},

		isUserRefined: function() {
	  var l = ("ref" in this) ? this.ref.length : 0;
	  return (("ref" in this) && l!==0 && !(l === 1 && this.ref[0] === currBrandId)) || ("pl" in this) || ("ph" in this);
	},
	removeRealRefs: function() {
	  var c = this,
		ar = c.ref,
		i = ar.length - 1,
		removed = [];
	  for(; i >= 0; i--) {
		if(!this.isNonRemovable(ar[i])) {
		  removed.push(ar[i]);
		  ar.splice(i, 1);
		}
	  }
	  if("ph" in c) {
		removed.push([c.pl, c.ph]);
		delete c.ph;
		delete c.pl;
	  }
	  return removed;
	},

	isNonRemovable: function(val) {
	  return val === currBrandId; //refactor this so that SkinIQ can override
	},

	addRef: function(val) {
	  if(this.ref.indexOf(val) === -1) {
		this.ref.push(val);
	  }
	},

	addNode: function(val) {
	  this["node"] = val;
	},
	removeRef: function(val) {
	  if(!$.isArray(val)) {
		val = [val];
	  }
	  var before = this["ref"].length;
	  this["ref"] = this["ref"].filter(function(ref) {
		return val.indexOf(ref) === -1
	  });
	  if(this["ref"].length !== before) {
		return true;
	  }
	  if((("pl" in this) || ("ph" in this)) && [this.pl, this.ph].join(",") === val.join(",")) {
		delete this.pl;
		delete this.ph;
		return true;
	  }
	  return false;
	},
	addHint: function(obj) {
	  $.extend(hints, obj);
	},
	mergeHints: function(t) {
	  $.extend(t, hints);
	  hints = $.extend({}, defHints);
	},
	//used to detect brand pages without drilling down into refinements
	//"brand" name="searchtype" could be used, too
	isBrandTop: function() {
	  return(("brandName" in this) || ("brandId" in this)) && (this["ref"].length===0 && !("node" in this));
	},
	hasPrimaryConcern: function(){return "primaryConcern" in this},
	val: function(key, val) {
	  var args = [].slice.call(arguments, 0);
	  if(args.length === 1) {
		return this[key];
	  } else {
		this[key] = val;
	  }
	},
	nameFromPath: nameFromPath,
	ifKeywordSearch: function(fn) {
	  var content, st = $("meta[name='searchtype']");
	  if(st.length) {
		content = st.attr("content");
		if(["keyword", "contentGroup", "gc", "sale"].indexOf(content) > -1) {
		  fn();
		}
	  } else {
		fn();
	  }
	},
	isBrandSeeAll: function() {
	  return(BRANDID in this) && ("ref" in this) && this.ref[0];
	},

	withCorrectParams: function(fn, opts) {// rename to generateQueryParams
	  var params = $.extend({}, this, opts||{});
			if(!params.ref.contains(currBrandId)
				 && "brandName" in params && currBrandId>0
				 && params.ref.length>0 ) {
				//delete params.brandName; temp fix until we rework search api
				params.ref.push(currBrandId);
			}
	  if(!("meta" in astoria.template) || !("image_path" in astoria.template.meta)) {
				params.meta = true;
				params._ = Math.floor(Math.random()*100000000);
			}
	  if(!("include_categories" in params)) { params.include_categories = true; }
	  if(!("include_refinements" in params)) { params.include_refinements = true; }
	  if(!MyList.isInited()) { params.include_favorites = true; }
	  fn(params, params.node || params.categoryName);
	}
		};
	})();

	astoria.Product.search = function(success) {
		criteria.withCorrectParams(function(data) {
			astoria.Product.find(data, [astoria.ajax.processMeta, success]);
		});
	};

  var ifAuthored = function(cat, brand, fn) {
	var curr;
	if(brand && ("maincontent" in brand)) {
	  fn(brand.maincontent);
	} else if(cat && astoria.search.Categories.currentNode) {
	  if($.isPlainObject(cat)) {
		if(cat.isCurrent() && ("maincontent" in cat || "banner" in cat)) {
		  fn(cat.maincontent, cat);
		} else {
		  ifAuthored(cat.sub_categories, brand, fn);
		}
	  } else {

		curr = cat.filter(function(onecat) {
		  return onecat.isCurrent();
		})[0];

		if(curr && ("maincontent" in curr || "banner" in curr)) {
		  fn(curr.maincontent, curr);
		}
	  }
	}
  };

  astoria.search.getCriteria = function() {
	return criteria;
  }

	astoria.search.withCurrentPage = function(current_page, total_pages, opts) {
		if(total_pages === 1) {
			return;
		}
		if(current_page - 2 > 1) {
			opts.onepage(1);
			opts.interval();
			opts.onepage(current_page - 1);
			opts.onepage(current_page);
		} else {
			opts.onepage(1);
			if(current_page - 1 > 1) {
				opts.onepage(current_page - 1);
			}
			if(current_page > 1) {
				opts.onepage(current_page);
			}
		}
		if(current_page + 2 < total_pages) {
			opts.onepage(current_page + 1);
			opts.interval();
			opts.onepage(total_pages);
		} else {
			if(current_page + 1 < total_pages) {
				opts.onepage(current_page + 1);
			}
			if(current_page < total_pages) {
				opts.onepage(total_pages);
			}
		}
	};

	astoria.search.BreadCrumbs = function(criteria) {
		var scope = "ul.breadcrumb", terms = criteria;

		return {
			init: function() {
				// if($("div.wrapper ul.breadcrumb").length===0) {
				// $('<ul class="breadcrumb"></ul>').insertBefore("div.wrapper div.content");
				// }
				// $(scope).on("click", "li a[href]", astoria.search.BreadCrumbs.onClick);
			},
			render: function(results) {
				// var d = $.extend({}, results, {
				// 	criteria: criteria,
				// 	searchtype: $("meta[name='searchtype']").attr("content")
				// });
				// var data = astoria.search.BreadCrumbs.ifBreadCrumb(d);
				// var crumbs = astoria.template.views2.breadCrumbs(data).trim(),
				// old = $("div.wrapper ul.breadcrumb:not(.no-replace)");
				// old.empty().append(crumbs);
			}
		};

	};

  astoria.search.BreadCrumbs.onClick = function(e){
	e.preventDefault();
	var anchor = $(this),
		node = parseInt(anchor.attr("data-node")),
		brandId = parseInt(anchor.attr("data-brandid")),
		brandName = anchor.attr("data-brandname");
	delete criteria.icid2;
	if(node) {
	  criteria.node = node;
	  delete criteria.categoryName;
	} else if(brandId) {
	  delete criteria.node;
	  delete criteria[BRANDNAME];
	  criteria.ref = [];
	  criteria.brandId = brandId;
	} else if(brandName) {
	  delete criteria.node;
	  delete criteria.brandId;
	  criteria.ref = [];
	  criteria[BRANDNAME] = brandName;
	} else {
	  criteria.categoryName = anchor.attr("href").replace(/\//, "");
	}
	controller.reSearch();
  };

	// rename ifBreadCrumb to 'prepare' and make it configurable for items to show, skin iq, color iq, etc.)
  // clean up this function!!!
	astoria.search.BreadCrumbs.ifBreadCrumb = function(data) {
		var ret = [], cat, hasBrand = "brand" in data,
	  curr, i;

		criteria.withCorrectParams(function(params, isCategory) {
			if(hasBrand) {
				ret.push($.extend({ seo_path: criteria.nameFromPath(),
				refinedBrand: isCategory || (BRANDID in params && "ref" in params)},
		data.brand));
			}
			//todo investigate if data.searchType is used
	  //todo refactor the isBrandPage so it is not in 3 spots. used for ading H1 to correct place
			if(isCategory && data.searchtype !== "keyword" && ("categories" in data)) {
				cat = data.categories;
				while(cat) {
		  if($.isArray(cat)) {
			if(cat.length>1) {
			  var c = cat.filter(function(onecat) {
				return onecat.isCurrent();
			  })[0];
			  if(hasBrand){ c.isBrandPage = true; }
			  if(c) { ret.push(c); }// the if is not necessary, one category should have #is_current set
			  break;//drill only down to current
			} else {
			  if(hasBrand){ cat[0].isBrandPage = true; }
						ret.push(cat[0]);
			  if(cat[0].isCurrent()) { break; } //drill only down to current
						cat = cat[0].sub_categories;
			}
					} else {
			if(hasBrand){ cat.isBrandPage = true; }
						curr = cat.isCurrent();
						ret.push(cat);
						if(curr) {
							break;
						}
			cat = cat.sub_categories;
					}
				}
			}
		});
	return ret;
	};

	astoria.search.Pagination = function() {
		var scope = ".pagination",
			sortBys,
			perPages,
			pageNumbers,
			totalItems,
			hasProducts;

		return {
			render: function(results) {
				hasProducts = ("total_products" in results) && results.total_products > 0;
				pageNumbers.empty();
		if(hasProducts) {
		  var buildLinks = astoria.Page.buildLinks;
					if (results.total_pages>1) {
			pageNumbers.append($(astoria.template.views2.pagination(buildLinks(results.current_page, results.total_pages))).children());
		  }
					totalItems.text(results.total_products + " item" + (results.total_products > 1 ? "s" : ""));
				}
				totalItems.parent()[hasProducts ? "removeClass" : "addClass"]("hidden");
				$(scope)[hasProducts ? "removeClass" : "addClass"]("hidden");
		$('.pagination, .total-items')[hasProducts? "removeClass" : "addClass"]('invisible');
			},

			init: function(crit) {

				if($(scope).length) {
					sortBys = $('.sort-by select', scope);
					perPages = $('.view select', scope);
					pageNumbers = $("div.page-numbers ul", scope);
					totalItems = $("div.total-items span");

					// init sort by, items per page
					sortBys.change(function() {
						var val = $(this).val();
						sortBys[sortBys[0] === this ? "last" : "first"]().val(val);

						$("body").trigger(jQuery.Event("refinement.yourchoices"), ["reshow", $(this).attr("name"), val]);
					});
					perPages.change(function() {
						var val = $(this).val();
						perPages[perPages[0] === this ? "last" : "first"]().val(val);

						$("body").trigger(jQuery.Event("refinement.yourchoices"), ["reshow", $(this).attr("name"), parseInt(val)]);
					});
					$(scope).on("click", ".page-numbers a", function(e) {
						e.preventDefault();
						Scroller.scrollToTop();
						Events.publish("refinement.yourchoices", ["pagination", parseInt($(this).attr("data-page"))]);
					});

					!("sortBy" in crit) && (sortBys.length > 0) && (crit.defaultSortBy = crit.sortBy = sortBys.first().find("option:first").val());
				}
			},

			pageSize: function(fn) {
		var crit = criteria;
		//!("pageSize" in crit) && (perPages.length > 0) && (crit.defaultPageSize = crit.pageSize = parseInt(perPages.first().find("option:first").val()));
				if(perPages && perPages.length > 0) {
					fn(criteria.pageSize||parseInt(perPages.first().find("option:first").val()));
				}
			}
		};
	};
	astoria.search.YourChoicesView = function() {
		var scope = "div.filters-bar",
			filter,
			list,
			view = {
				render: function(res) {
					if("refinements" in res) {
			// (!("ref" in c) || l === 0 || (l === 1 && c.ref[0] === currBrandId)) && !("pl" in c) && !("ph" in c)
						var	noBar = !criteria.isUserRefined();
						$(scope).removeClass('hidden').find('.inner')[noBar ? "hide" : "show"]();
					} else {
						$(scope).addClass("hidden");
					}
				},
				refinementChange: function(e, method, attrib, val, label, combinedPrice) {
					list = $(scope).find("ul");
					var views = astoria.template.views2,
						old,
						currency = astoria.template.meta.currency_symbol,
						removePrice = function(range) {
							if(!range || !$.isArray(val) || range.length < 2 || range.length % 2 !== 0) {
								return false;
							}
							var cnt = 0,
								ar = [].concat(range);
							while(ar.length) {
								cnt += $("a.remove-filter[data-value='" + JSON.stringify([ar.shift(), ar.shift()]) + "']", scope).parent("li").remove().length;
							}
							return cnt > 0;
						},
						removeValue = function(v) {
							return $.isArray(v) ? removePrice(v) : $("a.remove-filter[data-value='" + v + "']", scope).parent("li").remove();
						};
					switch(method) {
						case "addRef":
							filter = views.userFilter({
								attribute: attrib,
								label: label,
								value: val,
				isBrand: attrib.toLowerCase() === "brand"
							});
							list.append(filter);
							break;
						case "sliderremove":
							combinedPrice ? removePrice(val) : $("a.remove-filter[data-attrib='price range']", scope).parent().remove();
							break;
						case REMOVEREF:
							if(removePrice(val) === false) {
								val = $.isArray(val) ? val : [val];
								val.forEach(function(v) {
									removeValue(v);
								});
							}
							astoria.logger.log("YourChoicesView#removeRef ", val);
							break;
						case "sliderchange":
							old = $(scope).find("li:has(a[data-attrib='" + attrib + "'])");
							filter = views.userFilter({
								attribute: attrib,
								label: attrib + " " + (currency + val[0] + "-" + val[1]),
								value: JSON.stringify(val),
				isBrand: attrib.toLowerCase() === "brand"
							});
							old.length === 0 ? list.append(filter) : old.replaceWith(filter);
							break;
						case "sliderremove":
							$(scope).find("li:has(a[data-attrib='" + attrib + "'])").remove();
							break;
						default:
							// handle all clear
							$("li", scope).remove();
					}
					$(scope).removeClass("hidden");
					astoria.logger.log("YourChoicesView ", method, "processed");
				},
				choiceRemoved: function(e) {
					list = $(scope).find("ul.container");
					e.preventDefault();
					var choice = $(this),
						attrib = choice.attr("data-attrib"),
						value = $.parseJSON(choice.attr("data-value"));
					choice.parent("li").remove();
					$("body").trigger(jQuery.Event("refinement.yourchoices"), [ /*$.isArray(value) ? "sliderremove" :*/ REMOVEREF, attrib, value]);
				},

		clear: function() {
		  $(scope).find("ul li").remove();
		}
			};
		$(scope).on("click", "a.remove-filter", view.choiceRemoved);
		$(scope).on("click", '.clear-link a', function(e) {
			e.preventDefault();
			view.clear();
			$("body").trigger(jQuery.Event("refinement.yourchoices"), ["removeall"]);
		});
		return view;
	};

	$.fn.isAnySelected = function() {
		var start = $(this);
		return(start.is(".refinements") ? start : start.parents(".refinements")).find("a.swatch-thumb.active").length > 0;
	};
	astoria.search.RefinementMixin = {
		// returning false will break the clearall flow
		removeChoice: function(method, attrib, val) {
			var c = this,
				process = function(fn) {
					var shouldCont = fn();
					if(method === REMOVEALL) {
						$(".clear-link", c.scope).hide();
					} else {
						var group = $(c.scope).refinedGroupByName(attrib);
						if(group.length > 0) {
							group.clearLink()[c.isAnySelected(group) ? "show" : "hide"]();
						}
					}
					return shouldCont;
				};
			return process(function() {
				var match;
				if(method === REMOVEALL) {
					return c.deselectAll();
				} else {
					//if($.isArray(val)) {
					match = c.elementByValue(val);
					//} else {
					//	match = val.reduce(function(prev, v) {
					//		return prev.add(c.elementByValue(v));//optimize,multiselect can return number
					//	}, $(""));
					return c.deselectAll(match).length === 0;
					//}
				}
			});
		},
		render: function(ctx) {
			return $(astoria.template.render(this.template, ctx));
		}
	};

  astoria.search.Color = {
	isActive: function() {
	  return this.status===2;
	},
	uri: function() {
	  return astoria.template.meta.image_path + "/color-" + this.display_name.toLowerCase() + ".png";
	}
  };

	astoria.search.Colors = function() {
		var scope = "#refinements .swatch-color";
		var colorClicked = function(e) {
			e.preventDefault();
			var color = $(this),
				attrib = color.refineAttrib(),
				val = parseInt(color.attr('data-value'));
			color.toggleClass("active");
			var method = color.hasClass("active") ? "addRef" : "removeRef";
			$("body").trigger(jQuery.Event("refinement.leftbar"), [method, attrib, val, color.find("img").attr("alt")]);
			color.clearLink()[color.isAnySelected() ? "show" : "hide"]();
		};

		var ret = {
			scope: scope,

			init: function() {
				$("#refinements").on("click", ".swatch-color .clear-link", function(e) {
					e.preventDefault();
					var clear = $(this),
						checked = clear.parents(".refinements").find("a.swatch-thumb.active"),
						val = checked.toArray().map(function(e) {
							return parseInt($(e).attr("data-value"));
						});
					checked.removeClass("active");
					$("body").trigger(jQuery.Event("refinement.leftbar"), ["removeRef", clear.refineAttrib(), val]);
					clear.hide();
				});
			},

			restoreUserFilters: function(prevState) { //update based on current refs
				var diffs = astoria.search.diffWithPageLoad(prevState.ref, criteria.ref),
					renderer = refinements.renderers.colors;
				diffs.added.forEach(function(checked) {
					var elem = renderer.elementByValue(checked),
						attrib, label;
					if(elem.length) {
						attrib = elem.refineAttrib();
						label = elem.find("img").attr("alt");
						//elem.toggleClass("active");//should be in renderDeltas
						yourChoices.refinementChange(undefined, "addRef", attrib, checked, label);
					}
				});
				diffs.removed.forEach(function(checked) {
					var elem = renderer.elementByValue(checked),
						attrib, label;
					if(elem.length) {
						attrib = elem.refineAttrib();
						label = elem.find("img").attr("alt");
						//elem.toggleClass("active");//should be in renderDeltas
						yourChoices.refinementChange(undefined, "removeRef", attrib, checked, label);
					}
				});
			},
			renderDeltas: function(ctx, success) {
		ctx.values.forEach(function(item) {
		  $.extend(item, astoria.search.Color);
		  item.group = ctx.display_name;
		});
		ctx.anyChecked = ctx.values.some(function(c) { return c.isActive(); });
				var views = astoria.template.views2;
				var old = $(scope).filter(function() {
					return $(this).find("h4").text() === ctx.display_name;
				});
				if(old.length === 0) {

					var refs = $(this.render(ctx));
					refs.find('a.refinement').bind("click", colorClicked);
					success(refs);
				} else {
					var list = old.find(".swatch-selector-content"),
						diffs = astoria.search.diffs("value", $.map(old.find("a.swatch-thumb"), function(el) {
							return {
								value: parseInt($(el).attr("data-value")),
								display_name: $(el).find("img").attr("alt")
							};
						}), ctx.values);
					list.append(diffs.added.reduce(function(prev, add) {
						return prev.add($(views.color(add)));
					}, $("")));
					diffs.removed.reduce(function(prev, remove) {
						return prev.add(list.find("a[data-value='" + remove.value + "']"));
					}, $("")).remove();
					$('a.refinement', scope).unbind("click").bind("click", colorClicked);
					old.find(".clear-link")[ctx.values.filter(function(c) {
						list.find("a[data-value='" + c.value + "']")[c.status === 2 ? "addClass" : "removeClass"]("active");
						return c.status === 2;
					}).length ? "show" : "hide"]();
				}
			},
			isAnySelected: function(group) {
				return group.isAnySelected();
			},
			deselectAll: function(match) {
				match || (match = $(scope).find("a.active"));
				return match.removeClass("active");
			},
			elementByValue: function(v) {
				return $(scope).find("a[data-value='" + v + "']");
			},

	  render: function(data) {
		return astoria.template.views2.colors(data);
	  },

	  removeChoice: astoria.search.RefinementMixin.removeChoice
		};

		return ret;
	};

	astoria.search.Checkbox = {
	isDisabled: function() {
	  var item = this;
		!("is_disabled" in item) ? item.is_disabled = (item.status & 4) === 4 : 0;
	  return this.is_disabled;
	}, isChecked: function() {
	  var item = this;
		  !("is_checked" in item) ? item.is_checked = (item.status & 6) > 0 : 0;
	  return this.is_checked;
	},
	isBrand: function() { return this.group && (this.group.toLowerCase()==='brand'); },

	name: function() { return this.group+"__"; }
	};

	$.fn.refineAttrib = function() {
		var start = $(this);
		return(start.is(".refinements") ? start : start.parents(".refinements")).find("h4").text();
	};
	$.fn.clearLink = function() {
		var start = $(this);
		return(start.is(".refinements") ? start : start.parents(".refinements")).find(".clear-link");
	};
	$.fn.isAnyChecked = function() {
		var start = $(this);
		return(start.is(".refinements") ? start : start.parents(".refinements")).find("input:checked").length > 0;
	};
	$.fn.refinedGroupByName = function(name) {
		name = $.trim(name);
		return name ? $(this).filter(function() {
			return $(this).find("h4").text() === name;
		}) : this;
	};

	if(Modernizr.touch) {
		astoria.search.Priceboxes = function(criteria) {
			var scope = ".priceboxes",
				attr, //this never changes
				curr,
				currency,
				selRanges = function() {
					return _.flatten($("input:checked", scope).toArray().reduce(function(prev, input) {
						return prev.concat(JSON.parse($(input).val()));
					}, []));
				},
				checkMode = function(isBoxes) {
					if(isBoxes) {
						$('div.checkbox-list label', scope).removeClass("hidden");
					} else {
						$('div.checkbox-list label:has(input:not(:checked))', scope).addClass("hidden");
					}
					$('div.price-inputs', scope)[isBoxes ? "addClass" : "removeClass"]("hidden");
					if(isBoxes) {
						$("input:checkbox", scope).prop("checked", false);
					}
				};

			return {
				scope: scope,

				init: function() {
					$("#refinements").on("click", ".priceboxes div.price-inputs button", function() { //optimize
						currency || (currency = astoria.template.meta.currency_symbol);
						var selRange = [Number($("#low-price").val()), Number($("#high-price").val())],
							attrib = $(this).refineAttrib(), //optimize
							label = currency + selRange[0] + " to " + currency + selRange[1];
						$("body")
							.trigger(jQuery.Event("refinement.leftbar"), ["sliderchange", attrib, selRange, label]); //"sliderremove" or "sliderchange" for filter bar
					});
					$("#refinements").on("change", '.priceboxes input:checkbox', function(e) { //optimize
						e.preventDefault();
						var range = $(this),
							isRemove = !range.is(":checked"),
							selRange = JSON.parse(range.val()),
							attrib = range.refineAttrib(), //optimize
							checked = $("input:checked", scope),
							method = isRemove ? "sliderremove" : "sliderchange",
							label = range.siblings("span").text();
						$("body")
							.trigger(jQuery.Event("refinement.leftbar"), [method, attrib, selRange, label]); //"sliderremove" or "sliderchange" for filter bar
						range.clearLink()[checked.length ? "show" : "hide"]();
						// hide all checkboxes
						checkMode(isRemove);
						$("#low-price").val(selRange[0]);
						$("#high-price").val(selRange[1]);
					});
					// handle clear link
					$("#refinements").on("click", ".priceboxes .clear-link", function(e) {
						e.preventDefault();
						var clear = $(this),
							removedRanges = selRanges();
						// sliderremove, sliderchange
						$("body").trigger(jQuery.Event("refinement.leftbar"), ["sliderremove", clear.refineAttrib(), removedRanges]);
						$("input:checked", scope).prop("checked", false);
						checkMode(true);
						clear.hide();
					});
				},
				// check all boxes in the range
				restoreUserFilters: function(prevState) {
					var crit = criteria,
						label;
					curr || (curr = astoria.template.meta.currency_symbol);
					if(attr && ("pl" in crit)) {
						label = curr + crit.pl + " to " + curr + crit.ph;
						yourChoices.refinementChange(undefined, "sliderchange", attr, [Number(crit.pl), Number(crit.ph)], label);
					}
				},
				renderDeltas: function(ctx, success) {
					attr = ctx.display_name;
					var old = $(scope).refinedGroupByName(attr),
						data = $.extend({}, ctx, {
							isTextMode: "pl" in criteria
						});
					data.curr = astoria.template.meta.currency_symbol;
					astoria.search.Priceboxes.calcRanges(data, ctx.values[0], ctx.values[1], criteria);
					data.low = criteria.pl;
					data.high = criteria.ph;
					old.remove();
					success(this.render(data));
					//old.length===0 ? success(this.template(data)) : success();
				},
				elementByValue: function(val) {
					return $("input", scope).filter(function() {
						return JSON.stringify(val) === $(this).val();
					})[0];
				},
				deselectAll: function(match) {
					return match ? $(match).prop("checked", false) : $("input:checked", scope).prop("checked", false);
				},
				isAnySelected: function(group) {
					return !!$("input:checked", scope).length;
			},

		render: function(data) {
		  data.ranges.forEach(function(range, i) { range.uniqueId = ("range_"+i); range.isTextMode = data.isTextMode; });
		  return astoria.template.views2.pricebox(data);
		},

			removeChoice: function(method, attrib, val) {
				if(attr === attrib) {
					checkMode(true);
					return false; //stop searching for refinements to remove
				}
			}
			};
		};
		astoria.search.Priceboxes.calcRanges = function(data, pl, ph, crit) { //if max is under 200, keep below design, otherwise increment by 50
			var incr = 25,
				start = 1,
				curr = astoria.template.meta.currency_symbol,
				high, hidden;
			data.ranges = [];
			while(true) {
				if(start === 101) {
					incr = 50;
				}
				if((pl <= start) && (start <= ph) || (pl < start + incr) && (start + incr < ph)) {
					high = start + incr - 1;
					hidden = ("pl" in crit) && (crit.pl !== start || crit.ph !== high);
					data.ranges.push({
						low: start,
						high: high,
						label: "" + (curr + start) + " to " + (start + incr - 1),
						hidden: hidden
					});
				} else if(start > ph) {
					break;
				}
				start += incr;
			}
		};

		astoria.search.Multiselect = function(scope, terms) {
			var BRAND = "#brandSelect",
				snapshot;

			return {
				scope: scope,

				init: function() {
					$("#refinements").on("focus", BRAND, function(e) {
						snapshot = $(this).val() || [];
						astoria.logger.log(e.type, "snapshot ", snapshot);
					}).on("change", BRAND, function(e) {
						try {
							astoria.logger.log(e.type);
							var sel = $(this),
								newVal = sel.val() || [],
								attrib = sel.refineAttrib(),
								val, text,
								diffs = astoria.search.diffs(snapshot, newVal),
								method = diffs.added.length ? "addRef" : "removeRef";
							val = parseInt("addRef" === method ? diffs.added[0] : diffs.removed[0]);
							text = sel.find("option[value='" + val + "']").text();
							astoria.logger.log(method, attrib, val, text);
							snapshot = newVal;
							$("body").trigger(jQuery.Event("refinement.leftbar"), [method, attrib, val, text]);
							sel.clearLink()[snapshot.length ? "show" : "hide"]();
						} catch(ex) {
							astoria.logger.log("exception:", ex);
						}
					});

					$("#refinements").on("click", ".clear-link[data-multi='true']", function(e) {
						e.preventDefault();
						var clear = $(this),
							val = $(BRAND).val();
						$(BRAND).val(null);
						$("body").trigger(jQuery.Event("refinement.leftbar"), ["removeRef", clear.refineAttrib(), val]);
						clear.hide();
					});
				},

				renderDeltas: function(ctx, success) {
		  ctx.values.forEach(function(item) {
			$.extend(item, astoria.search.Checkbox);
			item.group = ctx.display_name;
		  });
					var newVal;
					var old = $(scope).filter(function() {
						return $(this).find("h4").text() === ctx.display_name;
					});
					if(old.length === 0) {
						success(this.render(ctx));
					} else {
						newVal = ctx.values.filter(function(v) {
							return v.status===2||v.status ===4;
						}).map(function(v) {
							return v.value;
						});
						$(BRAND).val(newVal);
					}
				},
				restoreUserFilters: function(prevState) {
					astoria.logger.log("Multiselect#restoreUserFilters entering", location.hash);
					var diffs = astoria.search.diffWithPageLoad(prevState.ref, criteria.ref),
						renderer = this;
					"added removed".split(" ").forEach(function(theset) {
						diffs[theset].forEach(function(checked) {
							var elem = renderer.elementByValue(checked),
								attrib, label;
							if(elem.length) {
								attrib = elem.refineAttrib();
								label = elem.text();
								yourChoices.refinementChange(undefined, theset === "added" ? "addRef" : "removeRef", attrib, checked, label);
							}
						});
					});
				},
				isAnySelected: function(group) {
					snapshot = $(BRAND).val();
					return snapshot !== null;
				},
				// uncheck all if undefined is passed in
				deselectAll: function(match) {
					if(match && match.length) {
						var toRemove = $.map(match, function(e) {
							return parseInt($(e).val());
						});
						snapshot = $(BRAND).val();
						snapshot = snapshot.filter(function(v) {
							return toRemove.indexOf(parseInt(v)) === -1;
						});
						$(BRAND).val(snapshot);
						astoria.logger.log("Multiselect setting ", snapshot.join(","))
						return match;
					} else {
						$(BRAND).val(null);
						return {
							length: 0
						};
					}
				},
				elementByValue: function(v) {
					return $(BRAND).find("option[value='" + v + "']");
				},

		render: function(data) {
		  data.anyChecked = data.values.some(function(c) { return c.isChecked(); });
		  data.is_hidden=false;
		  data.extraClass="";
		  return astoria.template.views2.brandSelector(data);
		},

		removeChoice: astoria.search.RefinementMixin.removeChoice
			};
		}; // astoria.search.Multiselect
	}

	astoria.search.Checkboxes = function(scope) {
		var checkboxChanged = function() {
			var box = $(this),
				val = parseInt(box.val()),
				method = box.is(":checked") ? "addRef" : "removeRef",
				attrib = box.refineAttrib(),
		terms = criteria;
			$("body").trigger(jQuery.Event("refinement.leftbar"), [method, attrib, val, box.next().text()]);
			box.clearLink()[box.isAnyChecked() ? "show" : "hide"]();
		};

		astoria.search.Checkboxes.isChecked = function(checkbox) {
			return(checkbox.status & 6) > 0;
		};
	astoria.search.Checkboxes.isEnabled = function(box) {
	  return (box.status&4)!==4;
	}
		return {
			scope: scope,

			init: function() {
				$("#refinements").on("change", '.checkboxes .refinement:checkbox:enabled', checkboxChanged)
				.on("click", ".checkboxes .clear-link", function(e) {
					e.preventDefault();
					var clear = $(this),
						checked = clear.parents(".refinements").find("input:checked"),
						val = checked.toArray().map(function(e) {
							return parseInt($(e).val());
						});
					checked.prop("checked", false);
					$("body").trigger(jQuery.Event("refinement.leftbar"), ["removeRef", clear.refineAttrib(), val]);
					clear.hide();
				});
			},

			restoreUserFilters: function(prevState) { //update based on current refs
				astoria.logger.log("Checkboxes#restoreUserFilters entering", location.hash);
				var diffs = astoria.search.diffWithPageLoad(prevState.ref, criteria.ref),
					renderer = this;
				"added removed".split(" ").forEach(function(theset) {
					diffs[theset].forEach(function(checked) {
						var elem = renderer.elementByValue(checked),
							attrib, label;
						if(elem.length) {
							attrib = elem.refineAttrib();
							label = elem.next().text();
							yourChoices.refinementChange(undefined, theset === "added" ? "addRef" : "removeRef", attrib, checked, label);
						}
					});
				});
			},

			renderDeltas: function(ctx, success) {
		ctx.values.forEach(function(item) {
		  $.extend(item, astoria.search.Checkbox);
		  item.group = ctx.display_name;
		});
				var views = astoria.template.views2,
					anyChecked = false,
					isChecked = astoria.search.Checkboxes.isChecked;
				var old = $(scope).filter(function() {
					return $(this).find("h4").text() === ctx.display_name;
				});
				if(old.length === 0) {
					ctx.values.forEach(function(box) {
						if(isChecked(box) && astoria.search.Checkboxes.isEnabled(box)) {
							criteria.addRef(box.value);
							anyChecked || (anyChecked = box.status === 2);
						}
			((box.status&4)===4)&&(ctx.display_name==="Brand") && (currBrandId=box.value);//detect undeselectable current brand
					});
					ctx.anyChecked = anyChecked;
					success(this.render(ctx));
				} else {
					var list = old.find("div.checkbox-list");
					list.empty();//recreate to ensure alphabetical sorting

					list.append(ctx.values.reduce(function(prev, add) {
						add.group = ctx.display_name;
						return prev.add($(views.checkbox(add)));
					}, $("")));
					old.find(".clear-link")[ctx.values.filter(function(c) {
						return c.status === 2;
					}).length ? "show" : "hide"]();
					ctx.values.length > 6 ? list.addClass('custom-scroll') : list.scrollTop(0).removeClass('custom-scroll');
				}
			},
			isAnySelected: function(group) {
				return group.isAnyChecked();
			},
			deselectAll: function(match) {
				match || (match = $(scope).find("input:checked:enabled"));
				return match.prop("checked", false);
			},
			elementByValue: function(v) {
				return $(scope).find("input:enabled[value='" + v + "']");
			},

	  render: function(data) {
		data.isBox = data.values.length > 6 && !data.no_scroll;
		var isBrand = data.display_name === 'Brand';
		data.multi = isBrand && Modernizr.touch;
		return astoria.template.views2.checkboxes(data);
	  },

	  removeChoice: astoria.search.RefinementMixin.removeChoice
		};
	}; // astoria.search.Checkboxes
	if(!Modernizr.touch) {
		astoria.search.Slider = function(criteria) {
			var scope = "#refinements .slider-bar",
				processPriceChange = true,
				keepApartInProgress = false,
				currency,
				updateVal = false;

			function displaySelRange(_, ui) {
				currency || (currency = astoria.template.meta.currency_symbol);
				var min = ui ? ui.values[0] : $(this).slider("values", 0),
					max = ui ? ui.values[1] : $(this).slider("values", 1);
				$(this).siblings(".slider-output").find(".slider-amount").val(currency + min + ' to ' + currency + max);
			}
			var updateRange = function(min, max) {
				$("div.orig-range div.price-min").text(astoria.template.meta.currency_symbol + min);
				$("div.orig-range div.price-max").text(astoria.template.meta.currency_symbol + max);
			}

			var whileIgnoreChange = function(fn) {
				processPriceChange = false;
				try {
					fn();
				} finally {
					processPriceChange = true;
				}
			};
			$.fn.resetToInitial = function() {
				this.each(function(_, slider) {
					whileIgnoreChange(function() {
						slider = $(slider);
						var origValues = [slider.slider("option", "min"), slider.slider("option", "max")];
						if(typeof (origValues) === "undefined") {
							$.error("Not a slider, missing original value.");
						}
						var newValues = [].concat(origValues); // avoid reference problem
						slider.slider("option", "values", newValues);
						displaySelRange.call(slider);
					});
				});
				return this;
			};

			function keepApart(e, extra) {
				if(!keepApartInProgress) {
					keepApartInProgress = true;
					var values = $(e.target).slider("values");
					var newValues = extra.leftHandle ? [extra.value - 5, values[1]] : [values[0], extra.value + 5];
					$(e.target).slider("values", newValues);
					displaySelRange.call($(e.target));
					keepApartInProgress = false;
				} else {
					keepApartInProgress = false;
				}
			}
			var isPriceChanged = function(slider) {
				var values = slider.slider("values"),
					orig = [slider.slider("option", "min"), slider.slider("option", "max")];
				return orig.join(",") !== values.join(",");
			};
			var priceChanged = function() {
				if(!processPriceChange) {
					return
				};
				var slider = $(this),
					attribute = slider.refineAttrib(),
					values = slider.slider("values");
				if(values[0] === values[1]) {
					var leftHandle = $(ui.handle).next('a.ui-slider-handle').length === 1;
					$(this).triggerHandler('keepapart', {
						leftHandle: leftHandle,
						value: ui.value
					});
					return; // keep handles 5 units apart
				}
				criteria.addHint({
					isRedrawPrice: false,
					isUpdateSliderVal: false
				});
				$("body").trigger(jQuery.Event("refinement.leftbar"), [isPriceChanged(slider) ? "sliderchange" : "sliderremove", attribute, values]);
				slider.clearLink()[isPriceChanged(slider) ? "show" : "hide"]();
			};
			return $.extend({}, astoria.search.RefinementMixin, {
				scope: scope,
				template: undefined /*astoria.template.views.slider*/,

				init: function() {
					$("#refinements").on("click", ".slider-bar .clear-link", function(e) {
						e.preventDefault();
						var clear = $(this),
							slider = clear.parents(".refinements").find(".slider-range"),
							val = slider.slider("values");
						slider.resetToInitial();
						$("body").trigger(jQuery.Event("refinement.leftbar"), ["removeRef", clear.refineAttrib(), val]);
						clear.hide();
					});
				},

				restoreUserFilters: function(prev) { //we should store the range not only the values
					if(("pl" in criteria) || ("ph" in criteria)) {
						var s = $(".slider-range", scope).slider(),
							attrib = s.refineAttrib(),
							val = ("pl" in criteria) || ("ph" in criteria) ? [criteria.pl, criteria.ph] : false;
						yourChoices.refinementChange(undefined, "sliderchange", attrib, val, attrib);
					}
				},
				renderDeltas: function(ctx, success, res) {
					var slider, min = ctx.values[0],
						max = ctx.values[1],
						attr = ctx.display_name,
						old = $(scope).refinedGroupByName(attr);
					if(old.length === 0 || res.isRedrawPrice !== false) {
						old.remove();
						success(this.render(ctx));
					} else {
						slider = old.find(".slider-range").slider();
						currency || (currency = astoria.template.meta.currency_symbol);
						astoria.logger.log("Slider#render redraw ", res.isRedrawPrice, " updateval ", res.isUpdateSliderVal);
						if(res.isUpdateSliderVal !== false) {
							whileIgnoreChange(function() {
								slider.slider("values", [criteria.pl || min, criteria.ph || max]);
								displaySelRange.call(slider);
							});
						}
					}
				},
				isAnySelected: function(group) {
					var slider = group.find(".slider-range");
					return isPriceChanged(slider);
				},
				deselectAll: function(match) {
					match || (match = $(scope).find(".slider-range"));
					return match.resetToInitial();
				},
				elementByValue: function(v) {
					return $(scope).find(".slider-range").filter(function(_, slider) {
						return $.isArray(v) && v.join(",") === $(slider).slider("values").join(",");
					});
				},

				render: function(ctx) {
					var currency = astoria.template.meta.currency_symbol;
		  ctx.currency = currency;
					var rendered = $(astoria.template.views2.slider(ctx));
					var slider = rendered.find('.slider-range');
					slider.slider({
						range: true,
						values: ctx.values,
						step: 5,
						min: ctx.values[0],
						max: ctx.values[1],
						change: priceChanged,
						slide: displaySelRange
					});
					displaySelRange.call(slider);
					slider.bind('keepapart', keepApart); // keep handles from overlapping
					return rendered;
				}
			});
		};
	}

  // inserts authored content into left nav such as "more ways to shop"
  astoria.search.LeftAuthored = {

	removeNonSearch: function(fn) {
	  var content = $("div.sidebar");
	  content.find("div#left").tap(function(t) {
		if(t.length === 0) {
		  fn(content);
		}
	  }).slideUp(function() {
		$(this).remove();
		fn(content);
	  });
	},

	render: function() {
	  astoria.logger.log("calling removeNonSearch");
	  var left = this.leftBcc;
	  this.removeNonSearch(function() {
		if(left && left.length) {
		  var content = $("div.sidebar");
		  left.hide();
		  content.find("div.social-links").tap(function(s) {
			s.length === 0 ? content.append(left.slideDown()) : left.slideDown().insertBefore(s);
		  });
		}
	  });
	  this.leftBcc = undefined;
	}
  };

	astoria.search.Categories = function(controller, opts) {
		var scope = "div.sidebar",
			c = criteria,
	  terms = criteria,
			left;
		opts||(opts={});
		return {
			init: function() {
				// category change
				$(scope+ " div.search-navigation").on("click", "li:not(.more-cats) a[href], .view-all-cat", function(e) {
					e.preventDefault();
					var anchor = $(this);
		  astoria.analytics.analyze("leftNav", [anchor, $(".breadcrumb li")], true);
					delete criteria.buttonMediaId;
		  delete criteria.icid2;
					if(anchor.parents("li").hasClass("see-all")) {
						var ref = parseInt(anchor.attr("data-ref")),
							brandId = parseInt(anchor.attr("data-brandid"));
						delete criteria.brandName;
						delete criteria.node;
						terms.addRef(ref);
						criteria.brandId = brandId;
		  }else if (anchor.hasClass("view-all-cat")) {
			delete criteria.brandName;
						delete criteria.node;
						criteria.removeRealRefs();
			astoria.search.yourChoices.clear();
					} else {
						var catName = anchor.attr("href").replace(/\//, ""),
			  node;
						if( !["#", ""].contains(catName) ) {
			  delete criteria.node;
			  criteria.categoryName = catName;
			} else if( node=parseInt(anchor.attr("data-node")) ) {
							criteria.node = node;
							delete criteria.categoryName;
						}
					}
					controller.reSearch();
				});

				$(scope + " div.search-navigation").on("click", "li.more-cats a", function(e) {
					e.preventDefault();
					$(this).parent().hide().siblings().children("ul").children().show();
				});
			},

	  prepare: function(data, level, isRealCat) {
		var totalProducts = data.total_products, selectedCat;

		var prep = function(data, level) {
		  var cats, recCnt, cnt, currentNodeId = astoria.search.Categories.currentNode;
		  if ( $.isPlainObject(data.sub_categories) ) {
			data.sub_categories = [data.sub_categories]; //design flaw, without this, the template can not handle
		  }
		  cats = data.categories||data.sub_categories;
		  if ( $.isPlainObject(cats) ) {
			cats = [ cats ];
		  }

		  cats.forEach(function(cat) {

			cat.refined = (currentNodeId===cat.node) || ("sub_categories" in cat && !(currentNodeId===cat.node));

			if(cat.isCurrent()){
			  selectedCat = cat;
			}
			if ((("id" in cat)||("node" in cat)) && cat.isCurrent() ) {
			  cnt=cat.record_count||totalProducts, recCnt = cnt!==undefined ? " ("+cnt+")" : "";
			} else {
			  cnt=cat.record_count, recCnt = cnt!==undefined ? " ("+cnt+")" : "";
			}

			cat.topCat = data.topCat;//what does this do?
			if(isRealCat) {
			  cat.level = level;//only for real category pages
			}

			cat.recCnt = recCnt;

			if ("sub_categories" in cat) {
			  prep(cat, level+1);
			}
		  });
		}
		prep(data, level);
		return selectedCat;
	  },

	  showCategoriesFrom: opts.showCategoriesFrom || function(data, topCat) {
		return topCat? topCat.sub_categories : data.categories;
	  },

			render: function(ctx) {
				var self = this,
					data, rendered="", hasBrand = "brand" in ctx,
					terms = criteria;

				if("categories" in ctx) {
		  var isRealCat, topCat;
		  if( $.isPlainObject(ctx.categories) ) {
			isRealCat = "seo_path" in ctx.categories;
			topCat = ctx.categories;
		  }
					terms.withCorrectParams(function(params, catId) {
						astoria.search.Categories.currentNode = catId ? catId : undefined;
			var isCategory = $.type(catId) == "string";
						data = ctx;
			var selectedCat = self.prepare(data, 1, isRealCat);
						$.extend(data.brand, hasBrand ? {
							count: brandCnt || (brandCnt = ctx.brand.all_products_qty)
						} : {});
						data.isTopLevel = !$.isPlainObject(data.categories);
			data.isColorIQ = "coloriq" in terms;

			var category = self.showCategoriesFrom(data, topCat);
						data.header = self.resolveHeader(data, hasBrand, topCat);

			data.topCat = topCat; // topCat only occurs on real category pages
						data.sub_categories = (hasBrand && topCat) ? ($.isPlainObject(topCat) ? [topCat] : topCat) : $.isPlainObject(category) ? [category] : category;
						data.hasRefinements = ("refinements" in data) && !! (data.refinements.length);
						data.showMore = isCategory && !hasBrand && data.hasRefinements && (selectedCat ? selectedCat.level == 3: false);
			if(data.showMore) { data.showMoreName = category[0].name; }
						if ( "categories" in data) {
			  rendered = astoria.template.views2.categories(data).trim();
			}

						$(".sidebar .sidenav").empty().append(rendered);

					});
				} else if("bcc_sidenav" in ctx) {
					$(".sidebar .sidenav").remove();
					$(".sidebar").append(ctx.bcc_sidenav);
				} else if(hasBrand && !("categories" in ctx)) {
					//brandName.text(ctx.brand.brand_name);
				}
			},

			resolveHeader: opts.resolveHeader || function(ctx, hasBrand, topCat) {
		return hasBrand ? ctx.brand.brand_name : criteria.keyword? 'CATEGORY' : topCat? topCat.nameIfLevel2() : "";
	  }
		}
	};

	astoria.search.Refinements = function() {
		var scope,
			renderers = {},
	  terms = criteria,
			resolveController = function(type, display) {
				if(Modernizr.touch && display.toLowerCase() === "brand") {
					return renderers.multi;
				};
				return renderers[Modernizr.touch ? (type === "slider" ? "pricebox" : type) : type];
			},

	  removeHiddenRefinments = function(results){
		if(hiddenRefs.length > 0){
		  var idxToRemove = [];
		  $(results.refinements).each(function(idx){
			if(hiddenRefs.indexOf(this.display_name.toLowerCase()) > -1){
			  idxToRemove.push(idx);
			}
		  });
		  for(var i=idxToRemove.length; i>0;i--){
			results.refinements.splice(idxToRemove[i-1], 1);
		  }
		}
		return results.refinements || [];

	  },
			forEachView = function(fn) {
				for(var name in renderers) {
					if(renderers.hasOwnProperty(name)) {
						var view = renderers[name];
						fn(view);
					}
				}
			};
		var ret = {
			init: function() {
				scope = $("#refinements");
				$("div.clear-all-filters input").on("click", function() {
					clearChoices(REMOVEALL);
					$("body").trigger(jQuery.Event("refinement.leftbar"), ["removeall"]);
				});
				$("body").on("refinement.yourchoices", function(_, method, attrib, val) {
					clearChoices(method, attrib, val);
				});

				forEachView(function(view) {
					if("init" in view) {
						view.init();
					}
				});
			},
			render: function(results) {
				var newRefs, diffs, exist = scope.find(".refinements"),
					hasRefinement = ("refinements" in results) && results.refinements.length,
					refinements = removeHiddenRefinments(results);

				diffs = astoria.search.diffs("display_name", $.map(exist, function(el) {
					return {
						display_name: $(el).refineAttrib()
					};
				}), refinements);
				diffs.removed.reduce(function(prev, ref) {
					return prev = prev.add(exist.refinedGroupByName(ref.display_name));
				}, $("")).remove();
				newRefs = refinements.reduce(function(prev, refinement, ix) {
					// colors, checkboxes, slider
					resolveController(refinement.type, refinement.display_name).renderDeltas(refinement, function(rendered) {
						prev = (rendered ? prev.add(rendered) : prev);
					}, results);
					return prev;
				}, $(""));
				$("#refinements").append(newRefs);
				$("div.clear-all-filters")[hasRefinement ? "fadeIn" : "fadeOut"]()[hasRefinement ? "removeClass" : "addClass"]("hidden");
				refinements.forEach(function(refinement) {
					var c = resolveController(refinement.type, refinement.display_name)
					if("customRender" in c) {	c.customRender(refinement); }
				});
			},
			restoreUserFilters: function(prevState) {
				forEachView(function(view) {
					if("restoreUserFilters" in view) {
						view.restoreUserFilters(prevState);
					}
				});
			},
			renderers: renderers
		};
		renderers.colors = new astoria.search.Colors();
		renderers.checkboxes = new astoria.search.Checkboxes("#refinements .checkboxes");
		if(Modernizr.touch) {
			renderers.pricebox = astoria.search.Priceboxes(criteria);
			renderers.multi = new astoria.search.Multiselect("#refinements .brand-refinement", criteria);
		} else {
			renderers.slider = new astoria.search.Slider(criteria);
		}
		var clearChoices = function(method, attrib, val) {
			for(var name in renderers) {
				if(renderers.hasOwnProperty(name)) {
					var view = renderers[name];
					if("removeChoice" in view) {
						if(view.removeChoice(method, attrib, val) === false) {
							break;
						}
					}
				}
			}
		};

		return ret;
	};
	astoria.search.initBrandButtons = function(controller, criteria) {
		$("div.main-wrap").on("click", "div.brand-right-nav a", function(e) {
			e.preventDefault();
			var a = $(this),
				paramUtils = astoria.util.ParameterUtils,
				brandId = paramUtils.paramExists("brandId");
			criteria.buttonMediaId = paramUtils.paramExists("buttonMediaId", a.attr("href"));
			brandId ? (criteria.brandId = brandId) : (criteria.brandName = nameFromPath());
	  if(paramUtils.paramExists("icid2", a.attr("href"))){criteria.icid2 = paramUtils.paramExists("icid2", a.attr("href"));}
			controller.reSearch();
		});
	};

  astoria.ui.controller.ProductResultsController = function(opts) {
	var self = this,
	  scope = opts.scope;
	this.scope = scope;

	$.extend(self, astoria.ui.controller.SupportsLoves, {

	  init: function() {
		this.love("a.icon-love");
	  },

	  initProducts: function(content) {
		astoria.util.initProductItem(content);
		astoria.jail.lazyImages(".product-grid img[data-src]");
		$(".product-item .product-image img", content).error(function(){astoria.ui.error.productImage(this,"/images/image-not-available-135.png");});
	  },

	  shownProductIds: function() {
		return $("a.qlook", scope).map(function(_, e) {
		  return astoria.util.HtmlUtils.productIdFrom($(e));
		});
	  },

			markLoves: function(prod) {
		MyList.isOnMyList(prod.derived_sku.sku_number, function(isOnList) {
		  prod.derived_sku.is_loved = isOnList;
		});
	  },

	  render: function(results) {

				self.template||(self.template="products");
		self.itemsPerRow||(self.itemsPerRow=4);

		self.handleLoves||(self.handleLoves=(("meta" in results) && results.meta.favorites===false) || self.noLoves ?
			function(prod) { prod.derived_sku.hideLove = true; } :
			self.markLoves);

		self.prepare||(self.prepare=self.prepProduct? function(prod) {
			self.prepProduct(prod);
			self.handleLoves(prod);
		  } : function(prod) {
			self.handleLoves(prod);
		  });

		var productGrid = function() {
		  var pg = $(".product-grid");
		  if(pg.length === 0) {
			$(".search-results").append('<div class="product-grid"></div>');// move .product-grid into template
			pg = $(".product-grid");
		  }
		  return pg;
		},

		populate = function() {
					var view = astoria.template.views2[self.template];
		  grid.empty();
		  if("products" in results) {
			var prods = [].concat(results.products);
			for(var i=0, n=prods.length; i<n; i++) {
			  self.prepare(prods[i]);
			}

			astoria.jail.markForceLoad(function(cnt) {
			  return prods.slice(0, cnt);
			});

			var group, all=[];
			while(prods.length) {
			  group = prods.splice(0, self.itemsPerRow);
			  all.push(view(group));
			}

			grid.append(all.join("")).fadeInIfNotTouch(300, function() {

			  astoria.event.publish("productsReady");

			  self.initProducts(grid);
			  if(self.enableTooltips){self.enableTooltips();}
			});
		  }
		},
				grid = productGrid();

		grid.children().length>0 ? grid.fadeOutIfNotTouch(function() {
		  populate();
		}) : populate();
		astoria.logger.log("ProductResultsController#render finished products products", astoria.logger.diff());
	  }
	});
  }

	$.extend(astoria.ui.controller, {

		SearchController: function(criteria, options) {
	  options||(options={});
			var self = this,
				scope = options.scope||"div#main",
				preAuth,
				brandBox,
				processors = [],
		terms = criteria;

			self.scope = scope;

	  var startRender = function(results) {
		self.results = results;
		return self;
	  }

			$.extend(self, {

		doRender: function(comp) {
		  var data = $.extend({}, self.results); // make copy here to sandbox data
		  comp.render(data);
		  return this;
		},

				listenToHashChange: function() {// rename
		  if(Modernizr.history) {
			window.onpopstate = self.onPopState;
		  } else if(Modernizr.hashchange) {
						astoria.logger.log("SearchController using onhashchange");
						window.onhashchange = self.onStateChange;
					}
				},

		show: function() {console.log(123); $(self.scope).show(); },

		hide: function() { $(self.scope).hide(); },

		onPopState: function(e, _) {
		  astoria.logger.log("#onPopState entering", e);
		  self.onStateChange();
		},

				onStateChange: function() {
					astoria.logger.log("onStateChange entering", location.search, location.hash);

		  if(self.isRecognizedRoute()) { return; }//some other controller handles this

					var t = {}, prevState;
					if(!isUserAction) {
						terms.mergeHints(t);
						prevState = t.firstTime ? {
							ref: []
						} : $.extend({}, criteria); //backbutton
						var newState = {};
			criteria.initialize(newState);
						criteria.addHint({
							isRedrawPrice: prevState.pl === newState.pl && prevState.ph === newState.ph
						}); // if price didn't change
						$("body").one("research", function() {
							self.updateNonRefinements(prevState);
						});

					}
					isUserAction = false;
					criteria.initialize();

					self.exec();
				},

				init: function() { // initial load
					astoria.logger.log("init entering", location.hash);

					preAuth = $("div.authored-content", "div#main");
					brandBox = $("div.brand-box", "div#main");

					brandBox.tap(function(t) {
						this.length === 0 ? (brandBox = $('<div class="brand-box flush no-links container"></div>').insertBefore(preAuth)) : 0;
					});

					astoria.search.yourChoices = yourChoices = new astoria.search.YourChoicesView();

					$("body").on("refinement.leftbar", yourChoices.refinementChange)
					.on("refinement.leftbar refinement.yourchoices", self.refinementChange)
					.on("pricerange", self.priceRangeChange);

					this.afterInit();
				},

				reSearch: function() { // call to request a new search (criteria changed)
					isUserAction = true;
		  astoria.util.ParameterUtils.updateUrl(criteria);
		  if(Modernizr.history) {
			setTimeout(function() {
			  self.onStateChange();
			}, 0);
		  }
				},

				updateNonRefinements: function(prevState) {
					prevState || (prevState = {
						ref: []
					});
					astoria.logger.log("updateNonRefinements entering", location.hash);
					refinements.restoreUserFilters(prevState);
					if("pageSize" in criteria && [60,20].contains(criteria.pageSize)) {//the allowed limits may change in the future
						$("select[name='pageSize']").val(criteria.pageSize);
					} else {
			$("select[name='pageSize']").find("option:first").prop("selected", true);
		  }
					if( ("sortBy" in criteria) && criteria.sortBy) {
						$("select[name='sortBy']").val(criteria.sortBy);
					} else {
			$("select[name='sortBy']").find("option:first").prop("selected", true);
		  }
				},

				refinementChange: function(e, method, attrib, val) {
					astoria.logger.log("SearchController#refinementChange entering");
					var noSearch = false;
		  delete criteria.icid2;
					switch(method) {
						case ADDREF:
						case REMOVEREF:
							if($.isArray(val)) {
								noSearch = !terms[method](val);
							} else {
								terms[method](val);
							}
							break;
						case "sliderchange":
							criteria.pl = val[0];
							criteria.ph = val[1]; // for price change
							break;
						case "sliderremove":
							if(val === false) {
								return;
							}
							delete criteria.pl;
							delete criteria.ph;
							break;
						case "reshow":
							// sort by or items per page
							criteria[attrib] = val;
							terms.addHint({
								isRedrawPrice: false,
								isUpdateSliderVal: false
							});
							break;
						case "pagination":
							val = attrib;
							criteria.currentPage = val;
							break;
						case "removeall":
							terms.removeRealRefs();
							delete criteria.pl;
							delete criteria.ph;
					}
					if(noSearch !== true) {
						self.reSearch(); // use criteria to submit the request for a search
					}
				},

				priceRangeChange: function(e, val) {
					criteria.pl = val[0];
					criteria.ph = val[1];
					self.reSearch();
				},

				searchHeader: function(results) {
					var head = [],
						total = results.total_products,
						pushCategory = function(cat, pre) {
							pre || (pre = "> ");
							head.push(pre);
							head.push(cat.name);
							head.push(" ");
							if("sub_categories" in cat && !$.isArray(cat.sub_categories)) {
								pushCategory(cat.sub_categories);
							}
						};
					if("keyword" in criteria) {
						head.push('"' + criteria.keyword + '": ');
					}
					if("categories" in results) {
						if(!$.isArray(results.categories)) {
							pushCategory(results.categories, "in ");
						}
					}
					if($("meta[name='noResults']").length > 0){
			head.push("0 results");
		  } else {
			head.push("  " + total + " ");
			head.push("result" + (total > 1 || total === 0 ? "s" : ""));
		  }

					$(".search-header").empty().append('<h1>'+Handlebars.Utils.escapeExpression(head.join(""))+'</h1>');
				},

				nthLevelLogo: function(results) {
					terms.withCorrectParams(function(params, cat) {
						var logoSel = "div#main .brand-header",
							nthLevelLogo,
							ifNeedLogo = function(fn) {
								if(("brand" in results) && ("logo_path" in results.brand)
				  && "products" in results
				  && (cat || (BRANDID in params && "ref" in params))) {
									nthLevelLogo = $(logoSel);
									if(nthLevelLogo.length === 0) {
										nthLevelLogo = $(astoria.template.views2.nthLevelLogo($.extend({}, results.brand,
					  { href: (params.brandName ||("brandId" in params))? ("/"+criteria.nameFromPath()) : window.location.pathname + window.location.search})));
									}

									$("div#main").prepend(nthLevelLogo);
								} else {
									$(logoSel).remove();
								}
							};
						ifNeedLogo();
					});
				},

				handleContent: function(maincontent, cat, callback) {
					var authored, brandb, mainContent = $(maincontent);
					mainContent.find("div.authored-content").tap(function(t) {
						authored = t;
					}).end()
						.filter("div#left").tap(function(t) {
						astoria.search.LeftAuthored.leftBcc = t;
					}).end()
						.find("div.brand-box").tap(function(t) {
						brandb = t;
					}).end();
					if(authored.length === 0) {
						mainContent.find("div#main").tap(function(t) {
							authored = t;
						}); //remove
						authored.find("div.brand-box").remove();
					}
					if(authored.length === 0) {
						mainContent.filter("div#content, div.authored-content").tap(function(t) {
							authored = t;
						});
					}
					preAuth.empty().append(authored.contents());
					if(brandb.length > 0) {
						brandBox.replaceWith(brandb);
			brandBox = brandb;
						brandBox.on("click", "a.desc-toggle", function(e) {
							e.preventDefault();
							$(".desc-more", brandBox).toggle();
						});
					} else {
						brandBox.empty();
					}
					if(cat && $.isPlainObject(cat) && "banner" in cat) {
						var cont = $("div#banner");
						cont.length === 0 ? $("div#main").prepend((cont = $('<div id="banner"></div>'))) : cont.empty();
						var bannerContent = $(cat.banner);
						cont.append(bannerContent.children());
						$("head").append(bannerContent.filter("script"));
					}

					$("head").append(mainContent.filter("script"));
					astoria.util.initComponents();

					if(typeof callback == 'function') callback();
				},
		// if an other controller passes in this option, this controller will not process the current route
		isRecognizedRoute: options.isRecognizedRoute || function() { return false; },

				processSocial: function(results) {
					var self = this,
						initSocial = function(results, fn) {
							if("brand" in results) {
								var brand = results.brand,
									enc = encodeURIComponent;
								if(brand.is_fb_enabled) {
									astoria.util.initFacebook();
									$('#fb-root').removeClass("hidden");
								}
								if(brand.is_pin_enabled) {
									var loc = window.location,
										root = $("#pinterest-root"),
										ar = [];
									ar.push("media=" + enc(loc.protocol + "//" + loc.host + brand.hero_image));
									ar.push("description=" + enc(brand.brand_name + " #astoria"));
									ar.push("ref=" + enc(loc.href));
									ar.push("layout=none");
									var query = "url=" + enc(loc.href + (loc.href.indexOf('?') === -1 ? "?" : "&")) + "om_mmc" + enc("=") + "oth-pinterest-dotcompinbuttons-2012";
									query += "&" + ar.join("&");
									var href = "//pinterest.com/pin/create/button/?" + query;
									root.removeClass("hidden");
									$("#pinterest-root a[data-pin-config]").attr("href", href);
									astoria.util.initPinterest();
								}
								fn.call(self, results);
								self.processSocial = fn;
							} else {
								self.processSocial = $.noop;
							}
						},
						process = function(results) {
							terms.withCorrectParams(function(params, isCategory) {
								$("div.social-links")[isCategory || terms.isBrandSeeAll() ? "addClass" : "removeClass"]("hidden");
							});
						};
					initSocial(results, process);
				},

				exec: function() {
					// var loader = $("#fs-loader");
					// astoria.logger.log("exec Changing flame height from ", $(loader).height(), " to ", $(window).height()," top  to ", $(window).scrollTop());
					// loader.height( $(window).height() ).offset({top: $(window).scrollTop()});
					// loader.show();
					// astoria.logger.log("#exec requesting search", astoria.logger.diff());
					// astoria.Product.search(function(results) {
					// 	astoria.ajax.processMeta(results);//for some reason this would be called 2nd otherwise
					// 	MyList.initFavorites(results);
					// 	loader.fadeOut(300);
					// 	self.render(results);
					// 	astoria.analytics.analyze("search", [$.extend(self.analyticsInfo(), criteria, results)]);
					// });
				},

		render: function(results){
		  if(!results) {
							return astoria.logger.log("no results!");
						}
						astoria.logger.log("#exec processing results", astoria.logger.diff());

						terms.mergeHints(results);
						currBrandId = parseInt("brand" in results ? results.brand.ref.split(" ")[0] : 0);
						var keyword = criteria.keyword,
							totalProducts = results.total_products,
							searchHeader = self.searchHeader;
						terms.ifKeywordSearch(function() {
							searchHeader(results);
						});
						processors.forEach(function(proc) {
							self[proc](results);
						});
						pagination.pageSize(function(perPage) {// move this to astoria.search.Pagination#render
							if(("total_products" in results) && results.total_products > 0) {
								perPage = (totalProducts > 300 && perPage === -1) ? 300 : perPage === -1 ? totalProducts : perPage;
								results.total_pages = Math.floor(totalProducts / perPage) + (totalProducts % perPage > 0 ? 1 : 0);
								results.current_page = criteria.currentPage || 1;
							}
							pagination.render(results);
						});
						results.keyword = keyword;

						//
						yourChoices.render(results);
						$("#main #banner").remove();
						astoria.logger.log("#exec about to render products", astoria.logger.diff());
			resultsController.render(results);

			refinements.render(results);

			criteria.isBrandTop() ? brandBox.show() : brandBox.hide();
			if("products" in results) {
			  preAuth.empty();
			}
						terms.withCorrectParams(function(params, isCategory) {
			  astoria.search.Categories.currentNode = isCategory ? isCategory : undefined; // refactor
			  ifAuthored(results.categories, results.brand, function(mainContent, cat) {
				self.handleContent(mainContent, cat, function() {
				  astoria.logger.log('@ifAuthored brandMainContentReady');
				  $(document.body).trigger('brandMainContentReady');
				});
			  });
			});

			startRender(results)
			  .doRender(astoria.search.categories)
			  .doRender(astoria.search.LeftAuthored);

						self.nthLevelLogo(results);
						breadCrumbs.render(results);
						$('.sidebar .sidenav').removeClass('hide');
						astoria.event.trigger("research", [criteria, results]);
						astoria.logger.log("removed flame");
						delete criteria.currentPage;
		}, //render

				onAppInit: function() {
					logger = astoria.logger;
					(astoria.template.meta || (astoria.template.meta = {})).isLazyLoad = $("body").attr("data-lazyload") !== "false";
					hbviews = astoria.template.views2;
					controller = this;
		  var isBrand = criteria.isBrandTop(), isSkinIQ = criteria.hasPrimaryConcern();
					if($("div.wrapper div.content").length === 0) {// gift, etc. pages
						$('<div class="content"></div>').insertAfter("div.wrapper ul.breadcrumb");
					}
					if($("div#main div.authored-content").length === 0) {
						$('<div class="authored-content"></div>').insertBefore("div#main div.search-results");
					}

					if($("div.content").children().length === 0) {
						$("div.content").append(astoria.template.views2.sidebarandmain( (function() {
							var pos=[true, true, true]; // this is so that the template evaluates correctly for top and bottom pagination
							return {
								isTop: function() {
									return !!pos.shift();
								},
				isBrand: isBrand,
				isSkinIQ: isSkinIQ,
								position: function() {
				  return (!!pos.shift())?"top":"bottom";
				}
								};
							})()));
					}
					refinements.init();

					pagination.init(criteria);

					views = astoria.template.views2;

					breadCrumbs.init();

					astoria.logger.log("about to make first call", location.hash);
					controller.init();

				}, //onAppInit

		//todo improve this idea and expand usage to other controller
		tryOverRideMethod: function(name, options ,func) {
		  if(options[name] === undefined){
			return func;
		  } else {
			this["super" + name] = func;
			return options[name];
		  }
		}
			});

	  self.afterInit = self.tryOverRideMethod("afterInit", options, function() {
		self.listenToHashChange();
		if(!Modernizr.history) {
		  self.exec(); // old browsers
		}
	  });

	  self.processMarketingBanner = self.tryOverRideMethod("processMarketingBanner", options, function(results) {
		var banner = $("div.search-banner");
		$(".marketingContent", banner).remove();
		if("marketing_banner" in results) {
		  banner.append(results.marketing_banner);
		}
	  });

	  self.processWarning = self.tryOverRideMethod("processWarning", options, function(results) {
		$(".search-header .no-results-message").hideRemove(function() {
		  if("warning_message" in results) {
			$(".search-header").append('<p class="no-results-message">' + decodeURIComponent(results.warning_message) + '</p>');
			$("div.search-banner").show();
		  }
		});
	  });

	  self.analyticsInfo = self.tryOverRideMethod("analyticsInfo", options, function() {
		return {};
	  });

	  if(criteria.isChanel()) {//move this out to searchpage.js since it is not for Skin IQ/Color IQ
		self.preprocessChanel = function(results) {
		  if( $.isArray(results.products) ) {
			astoria.event.publish("chanelProductsReady", [ results.products ] );
		  }
		};
		processors.push("preprocessChanel");
	  }

			for(var name in self) {
				if(self.hasOwnProperty(name) && /^process/.test(name)) {
					processors.push(name);
				}
			}

	  astoria.search.initBrandButtons(self, criteria);

		} //SearchController
	});

  criteria.initialize();
  astoria.search.prodResultsController = resultsController = new astoria.ui.controller.ProductResultsController({scope: "div.product-grid"});

	pagination = new astoria.search.Pagination();

  astoria.search.criteria = criteria;
	astoria.search.hiddenRefs = hiddenRefs;
	breadCrumbs = astoria.search.BreadCrumbs(criteria);
	refinements = astoria.search.refinements = new astoria.search.Refinements();

  astoria.firePopstateIfNecessary();

})(jQuery);
