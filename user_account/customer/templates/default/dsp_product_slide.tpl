<li data-slide-index="1">
	<div  class="product-item" data-index="[@DATA_INDEX]">
		<div class="product-image">
			<a href="/[@SLUGGER]" data-productid="P299207">
				<img height="97" width="97" alt="bareMinerals Spectrum SPF 15" src="[@PRODUCT_IMAGE]"/>
			</a>
			<!--a data-product-name="[@SLUGGER]" id="product-P299207" name="sku-1551449" href="javascript:void(0)" class="qlook" rel="click_to_quick_look">QUICK LOOK</a-->
		</div>
		<div class="new-love"></div>
		<a href="/[@SLUGGER]" data-productid="P299207" style=" font-family: fontsDosis;">
			<span class="product-info">
				<span class="name OneLinkNoTx">
					<span class="brand" >[@BRAND_NAME]</span>
					[@PRODUCT_NAME2]
				</span>
				<span class="sku-price">
					<span class="list-price">
						<span class="currency">C</span>
						<span class="price" style=" font-family: fontsDosis;">[@PRODUCT_PRICE2]</span>
					</span>
				</span>
				<span class="flags"></span>
			</span>
		</a>
		<div class="out-of-stock" style="display: none;"> <b>not in stock</b>
			<span>:</span>
			<a href="#" data-sku_number="1551449" class="btn-email-when-in-stock"></a>
		</div>
	</div>
</li>