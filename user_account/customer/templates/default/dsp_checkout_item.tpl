<div class="basket-item container data_count" id="sku_[@PRODUCT_ID]" data-skutype="[@PRODUCT_TYPE]" data-isbi="" data-bitype=""  data-productid=[@PRODUCT_ID] >
	<div class="product-image">
	  <a href="/[@PRODUCT_SLUGGER]">
		<img src="[@PRODUCT_IMAGE]" width="42" height="42" style=""></a>
	</div>
	<div class="product-description container" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
	  <h2 class="OneLinkNoTx">
		<a href="/[@PRODUCT_SLUGGER]">
		  <span class="brand" >[@BRAND_NAME]</span>
		  <span class="product-name">[@PRODUCT_NAME]</span>
		</a>
	  </h2>
	  <div class="info-row">
		<span class="sku">
		 <!--   <span class="label">Item #</span> -->
		  <span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
		</span>
	  </div>
		<div class="info-row variation">[@ITEM_INFO]</div>
	</div>
	<div class="item-purchase-info container" style="width:467px;">
	  <div class="product-quantity" style="margin-left:50px;">
		<label class="inline" for="1539543" style="font-size:15px" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">QTY</label>
          <input id="product_quantity" onkeypress="return keyNumber(event);" onkeyup="product_quantity_keyup();" onchange="product_quantity_keyup();change_item_quantity_on_basket('[@PRODUCT_ID]',this.value,'[@PRODUCT_IN]','[@URL]checkout')" class="form-control OneLinkNoTx quantitySelector" value = '[@QUANTITY_OPTION]' type="text" maxlength="3" style="width:30px; font-size: 15px" [@STYLE] />
		<!--select id="1539543" onchange="change_item_quantity_on_basket('[@PRODUCT_ID]',this.value,'[@PRODUCT_IN]','[@URL]checkout')" class="form-control OneLinkNoTx quantitySelector"   >
		  [@QUANTITY_OPTION]
		</select-->
    </div>
	  <div class="product-price">
        <span class="list-price" style="">[@ITEM_PRICE]</span>
        <span style="">[@ITEM_MOVE_LOVE_REMOVE]</span>
	  </div>
	</div>
  </div>