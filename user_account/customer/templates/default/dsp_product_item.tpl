<div class="product-item">
    <div class="product-image">
        <a href="/[@PRODUCT_SLUGGER]">
            <img src="[@PRODUCT_IMAGE]" style="max-width:135px;height: 120px;" alt="[@PRODUCT_NAME]">
        </a>
        <!--a name="sku" href="javascript:void(0)" data-product-name="[@PRODUCT_SLUGGER]" class="qlook" rel="click_to_quick_look"">QUICK LOOK</a-->
    </div>
    <div class="new-love" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">
        [@PRODUCT_NEW]
    </div>
    <a href="/[@PRODUCT_SLUGGER]">
    	<span class="product-info">
			<div class="name OneLinkNoTx" style="min-height: 50px;font-family: fontsDosis; font-weight: 400; font-style: normal;">
				<span class="brand OneLinkNoTx">[@PRODUCT_NAME]</span>
				<span>[@PRODUCT_DES]</span>
			</div>
			<div class="product-price ">
				<span class="list-price">[@PRODUCT_PRICE]</span>
				<span class="value-price">
					(<span class="price">[@PRODUCT_PRICE_DISCOUNT]</span><span style=" font-family: fontsDosis; font-weight: 400; font-style: normal;"> value</span>)
				</span>
			</div>
			[@PRODUCT_NOTE]
			[@PRODUCT_MORE_COLOR]
		</span>
    </a>
</div>