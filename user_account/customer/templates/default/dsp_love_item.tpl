<div id="div_[@PRODUCT_SLUGGER]" class="list-item container count_item_data">
    <div class="image">
        <div class="product-image">
            <a href="/[@PRODUCT_SLUGGER]">
                <img width="135" height="135" alt="" src="[@PRODUCT_IMAGE]"></a>
            <a class="qlook" href="#" data-product-name="[@PRODUCT_SLUGGER]" name="sku-1441278" rel="click_to_quick_look">QUICK LOOK</a>
        </div>
        <div class="pin">
            <a href="//www.pinterest.com/pin/create/button/?url=[@FULL_PRODUCT_LINK]&media=[@FULL_PRODUCT_DESCRIPTION]" class="PIN_1394160765685_pin_it_button_20 PIN_1394160765685_pin_it_button_en_20_gray PIN_1394160765685_pin_it_button_inline_20 PIN_1394160765685_pin_it_beside_20" target="_blank" data-pin-log="button_pinit" data-pin-config="beside">
                <span class="PIN_1394160765685_hidden" id="PIN_1394160765685_pin_count_0"> <i></i>
                </span>
            </a>
        </div>
    </div>
    <div class="info-wrap container">
        <div class="description">
            <p class="item-name OneLinkNoTx">
                <a href="/[@PRODUCT_SLUGGER]">
                    <span class="brand">[@BRAND_NAME]</span>
                    <span class="product-name">[@PRODUCT_NAME]</span>
                </a>
            </p>
            <div class="info-row">
                <span class="sku">
                    <span class="label">Item #</span>
                    <span class="value OneLinkNoTx">[@PRODUCT_CODE]</span>
                </span>
            </div>
            <div class="info-row variation">
                <span class="color">
                    <span class="label">Color</span>
                    <span class="value OneLinkNoTx">Sand</span>
                </span>
            </div>
        </div>
        <div class="price">
            <p>C[@PRODUCT_PRICE](C[@PRODUCT_PRICE_DISCOUNT] Value)</p>
        </div>
        <div class="actions" id="sku1441278">
            <button type="button" class="btn btn-default btn-add-to-basket" onclick="add_to_basket('[@PRODUCT_ID]',1,'[@FULL_PRODUCT_LINK]',0); ">add to basket</button>
            <div class="love-action">
                <a href="javascript:void(0)" class="icon icon-love hover-enable align-right loved" onclick='add_item_to_love_list("[@PRODUCT_SLUGGER]","delete_love");remove_love_item("[@PRODUCT_SLUGGER]")' title="" data-sku_number="1441278" data-product_id="P376196" data-original-title="UNLOVE"></a>
            </div>
        </div>
    </div>
</div>