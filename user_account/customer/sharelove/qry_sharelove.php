<?php if(!isset($v_sval)) die;
    $v_key = isset($_GET['txt_key']) ? $_GET['txt_key'] : '';
    $v_count = $cls_tb_favorite->count(array("user_id"=>$v_key));
    if($v_count<=0) redir("/");
    $arr_select = $cls_tb_favorite->select(array("user_id"=>$v_key));
    $v_dsp = '';
    $v_last_name = '';
    foreach($arr_select as $arr){
        $v_product_id = $arr['product_id'];
        $v_select_one = $cls_tb_contact->select_one(array("_id"=>new MongoId($v_key)));
        $v_last_name = $cls_tb_contact->get_last_name();
        if($v_select_one!=1) continue;
        $v_select_one = $cls_tb_product->select_one(array("_id"=>new MongoId($v_product_id)));
        $v_product_image = URL.$cls_tb_product->get_products_upload();
        if(!file_exists($v_product_image)) $v_product_image = JT_URL.$cls_tb_product->get_products_upload();
        if($v_select_one!=1) continue;
        $v_dsp .='<div class="product-list container">
            <div class="list-item container" data-sku_number="'.(string)$cls_tb_product->get_mongo_id().'">
                <div class="image">
                    <div class="product-image">
                        <a href="/'.$cls_tb_product->get_slugger().'">
                            <img width="135" height="135" src="'.$v_product_image.'" alt="">
                        </a>
                        <a id="product-P385588" data-product-name="'.$cls_tb_product->get_slugger().'" class="qlook" rel="click_to_quick_look" name="sku-1577055" href="#">QUICK LOOK</a>
                    </div>
                    <div class="pin">
                        <a class="pin-it-button" count-layout="horizontal" target="_blank" href="//pinterest.com/pin/create/button/?url='.URL.$cls_tb_product->get_slugger().'&media='.$v_product_image.'">
                            <img border="0" title="Pin It" src="//assets.pinterest.com/images/PinExt.png">
                        </a>
                    </div>
                </div>
                <div class="info-wrap container">
                    <div class="description">
                        <p class="item-name OneLinkNoTx">
                            <a href="/'.$cls_tb_product->get_slugger().'">
                                <span class="brand">'.$cls_tb_banner->select_scalar("banner_name",array("_id"=> new MongoId($cls_tb_product->get_brand_id()))).'</span>
                                <span class="product-name">'.$cls_tb_product->get_sku().'</span>
                            </a>
                        </p>
                        <div class="info-row">
                            <span class="sku">
                                <span class="label">Item #</span>
                                <span class="value OneLinkNoTx">'.$cls_tb_product->get_code().'</span>
                            </span>
                        </div>
                    </div>
                    <div class="price">
                        <p>C$18.00</p>
                    </div>
                    <div id="sku1577055" class="actions">
                        <button class="btn btn-default btn-add-to-basket" onclick=add_to_basket("'.(string)$cls_tb_product->get_mongo_id().'",1,"'.URL.$cls_tb_product->get_slugger().'",1); type="submit" value="1577055" name="sku_number">add to basket</button>
                        <div class="love-action">
                            <a class="icon icon-love hover-enable align-right loved" data-product_id="P385588" data-sku_number="1577055" title="UNLOVE" href="#"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }
    $dsp_share_love = new Template("dsp_share_love.tpl",$v_dir_templates);
    $dsp_share_love->set('URL_TEMPLATE',URL.$v_dir_templates);
    $dsp_share_love->set('URL',URL);
    $dsp_share_love->set('LIST_FAVORITE_LIST',$v_dsp);
    $dsp_share_love->set('CUSTOMER_NAME',$v_last_name);
    echo $dsp_share_love->output();
?>