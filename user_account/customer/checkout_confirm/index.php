<?php if(!isset($v_sval)) die();
add_class("cls_tb_province");
add_class("cls_tb_country");
add_class("cls_settings");
$cls_settings = new cls_settings($db);
$cls_provinces = new cls_tb_province($db);
$cls_country = new cls_tb_country($db);
$v_ajax = isset($_POST['ajax']) ? $_POST['ajax'] : '';
if($v_ajax!=''){
    //update_shipping_address
    include('qry_ajax_update_shipping_information.php');
}else{
    include $v_head.'checkout_login/header.php';
    include $v_head.'checkout_confirm/qry_checkout_confirm.php';
    include $v_head.'checkout_login/footer.php';
}

