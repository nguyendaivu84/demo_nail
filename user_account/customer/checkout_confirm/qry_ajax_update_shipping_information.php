<?php if(!isset($v_sval)) die;
    $v_items = isset($_POST['txt_items']) ? $_POST['txt_items'] : '';
    $v_type = isset($_POST['txt_type']) ? $_POST['txt_type'] : 0;
    settype($v_type,"int");
    if($v_items!=''){
        if($v_type==1){
            $arr_items = json_decode($v_items,true);
            $arr_ss_shipping_information = isset($_SESSION['ss_address_shipping']) ? unserialize($_SESSION['ss_address_shipping']) : array();
            $arr_ss_shipping_information['first_name'] = isset($arr_items['first_name']) ? $arr_items['first_name'] : '';
            $arr_ss_shipping_information['last_name'] = isset($arr_items['first_name']) ? $arr_items['last_name'] : '';
            $arr_ss_shipping_information['address_line_1'] = isset($arr_items['address1']) ? $arr_items['address1'] : '';
            $arr_ss_shipping_information['address_line_2'] = isset($arr_items['address1']) ? $arr_items['address2'] : '';
            $arr_ss_shipping_information['address_city'] = isset($arr_items['city']) ? $arr_items['city'] : '';
            $arr_ss_shipping_information['address_province'] = isset($arr_items['state']) ? $arr_items['state'] : '';
            $arr_ss_shipping_information['zip'] = isset($arr_items['postal_code']) ? $arr_items['postal_code'] : '';
            $arr_ss_shipping_information['phone_1'] = isset($arr_items['phone_1']) ? $arr_items['phone_1'] : '';
            $arr_ss_shipping_information['phone_2'] = isset($arr_items['phone_2']) ? $arr_items['phone_2'] : '';
            $arr_ss_shipping_information['phone_3'] = isset($arr_items['phone_3']) ? $arr_items['phone_3'] : '';
            $arr_ss_shipping_information['phone_ext'] = isset($arr_items['phone_ext']) ? $arr_items['phone_ext'] : '';
            $arr_ss_shipping_information['default_address'] = isset($arr_items['is_default']) ? $arr_items['is_default'] : '';
            $arr_ss_shipping_information['shipping_method'] = isset($arr_items['shipping_method']) ? $arr_items['shipping_method'] : '';
            $arr_ss_shipping_information['gift_check'] = isset($arr_items['gift_check']) ? $arr_items['gift_check'] : '';
            $_SESSION['ss_address_shipping'] = serialize($arr_ss_shipping_information);
        }else if($v_type==2){
            $arr_items = json_decode($v_items,true);
            $arr_ss_cart = array();
            $arr_ss_cart = isset($_SESSION['ss_cart']) ? unserialize($_SESSION['ss_cart']) : array();
            $arr_ss_cart['cc_first_name'] = isset($arr_items['cc_first_name']) ? $arr_items['cc_first_name'] :'';
            $arr_ss_cart['cc_last_name'] = isset($arr_items['cc_last_name']) ? $arr_items['cc_last_name'] :'';
            $arr_ss_cart['cc_number'] = isset($arr_items['cc_number']) ? $arr_items['cc_number'] :'';
            $arr_ss_cart['cc_expiration_month'] = isset($arr_items['cc_expiration_month']) ? $arr_items['cc_expiration_month'] :'';
            $arr_ss_cart['cc_expiration_year'] = isset($arr_items['cc_expiration_year']) ? $arr_items['cc_expiration_year'] :'';
            $arr_ss_cart['cc_code'] = isset($arr_items['cc_code']) ? $arr_items['cc_code'] :'';
            $arr_ss_cart['save_cart'] = isset($arr_items['save_cart']) ? $arr_items['save_cart'] :0;
            $arr_ss_cart['default_cart'] = isset($arr_items['default_cart']) ? $arr_items['default_cart'] :0;
            $_SESSION['ss_cart'] = serialize($arr_ss_cart);
            $arr_ss_cart = array();
            $arr_ss_cart['billing_address_same'] = isset($arr_items['billing_address_same']) ? $arr_items['billing_address_same'] :1;
            $arr_ss_cart['billing_address_1'] = isset($arr_items['billing_address_1']) ? $arr_items['billing_address_1'] :'';
            $arr_ss_cart['billing_address_2'] = isset($arr_items['billing_address_2']) ? $arr_items['billing_address_2'] :'';
            $arr_ss_cart['billing_city'] = isset($arr_items['billing_city']) ? $arr_items['billing_city'] :'';
            $arr_ss_cart['billing_state'] = isset($arr_items['billing_state']) ? $arr_items['billing_state'] :'';
            $arr_ss_cart['billing_zip'] = isset($arr_items['billing_zip']) ? $arr_items['billing_zip'] :'';
            $arr_ss_cart['billing_country'] = isset($arr_items['billing_country']) ? $arr_items['billing_country'] :'';
            $arr_ss_cart['billing_phone_1'] = isset($arr_items['billing_phone_1']) ? $arr_items['billing_phone_1'] :'';
            $arr_ss_cart['billing_phone_2'] = isset($arr_items['billing_phone_2']) ? $arr_items['billing_phone_2'] :'';
            $arr_ss_cart['billing_phone_3'] = isset($arr_items['billing_phone_3']) ? $arr_items['billing_phone_3'] :'';
            $arr_ss_cart['billing_phone_ext'] = isset($arr_items['billing_phone_ext']) ? $arr_items['billing_phone_ext'] :'';
            $_SESSION['ss_billing_address'] = serialize($arr_ss_cart);
        }
    }