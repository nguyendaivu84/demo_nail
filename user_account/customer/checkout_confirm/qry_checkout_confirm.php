<?php if (!isset($v_sval)) die();
if(!isset($_SESSION['ss_customer']) || !isset($_SESSION['ss_basket_content']) ) redir("/checkout");
if(!isset($_SESSION['ss_address_shipping'])) redir("/checkout_accordion");
if(isset($_POST['continue'])){
    $arr_ss_customer = unserialize($_SESSION['ss_customer']);
    $arr_ss_basket_content = unserialize($_SESSION['ss_basket_content']);
    $arr_ss_cart = isset($_SESSION['ss_cart']) ? unserialize($_SESSION['ss_cart']) : array();
    $arr_ss_address_shipping = unserialize($_SESSION['ss_address_shipping']);
    $arr_ss_address_billing = isset($_SESSION['ss_billing_address']) ? unserialize($_SESSION['ss_billing_address']) : array();
    if(empty($arr_ss_address_billing)) $arr_ss_address_billing = $arr_ss_address_shipping;
    $v_tax_val = 0;
    $v_total_val = 0;
    $arr_products = array();
    add_class("cls_tb_saleorder");
    add_class("cls_tb_product");
    add_class("cls_tb_contact");
    $cls_user = new cls_tb_contact($db);
    if(isset($arr_ss_cart['save_cart']) && $arr_ss_cart['save_cart'] == 1){
        $v_default_cart = 0;
        $arr_cart = $cls_user->select_scalar("cart",array("_id"=>new MongoId((string)$arr_ss_customer['_id'])));
        if(!empty($arr_cart)) $v_default_cart = count($arr_cart);
        $arr_cart[] = $arr_ss_cart;
        if($arr_ss_cart['default_cart']==1)
            $cls_user->update_fields(array("cart","default_cart"),array($arr_cart,$v_default_cart),array("_id"=>new MongoId((string)$arr_ss_customer['_id'])));
        else $cls_user->update_fields(array("cart"),array($arr_cart),array("_id"=>new MongoId((string)$arr_ss_customer['_id'])));
    }
//    if($arr_ss_address_shipping['default_address']=='true' || $arr_ss_address_shipping['default_address']==1){
    if($arr_ss_address_shipping['is_default']==true || $arr_ss_address_shipping['is_default']==1){
        $v_default_address = 0;
        $arr_add = $cls_user->select_scalar("addresses",array("_id"=>$arr_ss_customer['_id']));
        if(!empty($arr_add)) $v_default_address = count($arr_add);
        $arr_add[] = array(
            'name'=>''
            ,'deleted'=>false
            ,'default'=>true
            ,'country'=>'Canada'
            ,'province_state'=>$arr_ss_address_shipping['address_province']
            ,"province_state_id" => ""
            ,"address_1"=>$arr_ss_address_shipping['address_line_1']
            ,"address_2"=>$arr_ss_address_shipping['address_line_2']
            ,"address_3"=>isset($arr_ss_address_shipping['address_line_3']) ? $arr_ss_address_shipping['address_line_3'] : ''
            ,"town_city"=>$arr_ss_address_shipping['address_city']
            ,"zip_postcode"=>$arr_ss_address_shipping['postal_code']
            ,"phone"=>$arr_ss_address_shipping['phone_1']."-".$arr_ss_address_shipping['phone_2']."-".$arr_ss_address_shipping['phone_3'].$arr_ss_address_shipping['phone_ext']
        );

        $cls_user->update_fields(array("addresses","addresses_default_key"),array($arr_add,$v_default_address),array("_id"=>$arr_ss_customer['_id']));
    }
    $cls_product = new cls_tb_product($db);
    $v_tax_val = 0;
    $taxProvince = 'AB';
    for($i=0;$i<count($arr_ss_basket_content);$i++){
        $taxProvince = isset($arr_ss_basket_content[$i]['taxProvince']) ? $arr_ss_basket_content[$i]['taxProvince'] : $taxProvince;;
        $v_total_val += $arr_ss_basket_content[$i]['product_quantity']*$arr_ss_basket_content[$i]['product_price'];
        $v_tax_index = (float) isset($arr_ss_basket_content[$i]['tax']) ? $arr_ss_basket_content[$i]['tax'] : 0;
        $v_tax_val += ($v_tax_index*$arr_ss_basket_content[$i]['product_quantity']*$arr_ss_basket_content[$i]['product_price'])/(float)100.0;
        $arr_temp = array();
        $v_product_id = $arr_ss_basket_content[$i]['product_id'];
        $cls_product->select_one(array("_id"=>new MongoId($v_product_id)));
        $arr_temp['deleted'] = $cls_product->get_deleted();
        $arr_temp['products_name'] = $cls_product->get_name();
        $arr_temp['products_costing_name'] = "";
        $arr_temp['products_id'] = $v_product_id;
        $arr_temp['option'] = $cls_product->get_options();
        $arr_temp['sizew'] = $cls_product->get_sizew();
        $arr_temp['sizew_unit'] = $cls_product->get_sizew_unit();
        $arr_temp['sizeh'] = $cls_product->get_sizeh();
        $arr_temp['sizeh_unit'] = $cls_product->get_sizeh_unit();
        $arr_temp['receipts'] = '';
        $arr_temp['sell_by'] = '';
        $arr_temp['oum'] = $cls_product->get_oum();
        $arr_temp['unit_price'] = $cls_product->get_unit_price();
        $arr_temp['quantity'] = $arr_ss_basket_content[$i]['product_quantity'];
        $arr_temp['adh_qty'] = 0;
        $arr_temp['sub_total'] = '';
        $arr_temp['taxper'] = ($v_tax_index*$arr_ss_basket_content[$i]['product_quantity']*$arr_ss_basket_content[$i]['product_price'])/(float)100.0;// ($arr_ss_basket_content[$i]['product_quantity']*$arr_ss_basket_content[$i]['product_price'])/(float)10;
        $arr_temp['amount'] = $arr_ss_basket_content[$i]['product_quantity']*$arr_ss_basket_content[$i]['product_price'];
        $arr_temp['sku'] = $arr_ss_basket_content[$i]['product_sku'];
        $arr_products [] = $arr_temp;
    }
//    $v_tax_val = $v_total_val / (float) 10;
    $cls_tb_saleorder = new cls_tb_saleorder($db);
    $arr_insert_data['deleted'] = false;
    $arr_insert_data['code'] = $cls_tb_saleorder->select_next("code");
    $arr_insert_data['sales_order_type'] = "Sale order";
    $arr_insert_data['created_by'] = $arr_ss_customer['_id'];
    $arr_insert_data['description'] = '';
    $arr_insert_data['company_name'] = 'Home';
    $arr_insert_data['company_id'] = '';
    $arr_insert_data['contact_name'] = $arr_ss_customer['full_name'];
    $arr_insert_data['contact_id'] = $arr_ss_customer['_id'];
    $arr_insert_data['phone'] = $arr_ss_address_shipping['phone_1']."-".$arr_ss_address_shipping['phone_2']."-".$arr_ss_address_shipping['phone_3'].$arr_ss_address_shipping['phone_ext'];
    $arr_insert_data['email'] =$arr_ss_customer['email'];
    $arr_insert_data['salesorder_date'] = new MongoDate(strtotime(date('Y-m-d h:m:s'))) ;// date("y-m-d h:m:s");
    $arr_insert_data['payment_due_date'] = new MongoDate(strtotime(date('Y-m-d h:m:s')));// date("y-m-d h:m:s");
    $arr_insert_data['our_csr'] = "";
    $arr_insert_data['our_csr_id'] = "";
    $arr_insert_data['our_rep'] = "";
    $arr_insert_data['our_rep_id'] = "";
    $arr_insert_data['invoice_address'] = array();
    $arr_insert_data['shipping_address'] = array($arr_ss_address_shipping);
    $arr_insert_data['billing_address'] = $arr_ss_address_billing;
    $arr_insert_data['status'] = "New";
    $arr_insert_data['status_id'] = "New";
    $arr_insert_data['payment_terms'] = 0;
    $arr_insert_data['tax'] = $taxProvince;
    $arr_insert_data['taxval'] = $v_tax_val;
    $arr_insert_data['sum_tax'] = $v_tax_val;
    $arr_insert_data['sum_sub_total'] = $v_total_val;
    $arr_insert_data['sum_amount'] = $v_total_val+$v_tax_val;
    $arr_insert_data['customer_po_no'] = '';
    $arr_insert_data['name'] = $arr_ss_customer['full_name'];
    $arr_insert_data['job_name'] = '';
    $arr_insert_data['job_number'] = '';
    $arr_insert_data['job_id'] = '';
    $arr_insert_data['quotation_name'] = '';
    $arr_insert_data['quotation_number'] = '';
    $arr_insert_data['quotation_id'] = '';
    $arr_insert_data['delivery_method'] = '';
    $arr_insert_data['shipper'] = '';
    $arr_insert_data['total_order'] = $v_total_val;
    $arr_insert_data['products'] = $arr_products;
    $cls_tb_saleorder->insert($arr_insert_data);
    $tpl_content = new Template('dsp_checkout_success.tpl',$v_dir_templates);
    echo $tpl_content->output();
}else{
    $arr_ss_address_shipping = unserialize($_SESSION['ss_address_shipping']);
    $tpl_content = new Template('dsp_checkout_confirm.tpl',$v_dir_templates);
    $v_shipping_information = '';
    $v_shipping_phone = '';
    $v_first_name = '';
    $v_last_name = '';
    $v_address_1 ='';
    $v_address_2 ='';
    $v_gift_check ='';
    $v_address_city ='';
    $v_province ='';
    $dsp_state_option = '';
    $arr_state_option = array();
    $v_postal_code = '';
    $v_phone_1 = '';
    $v_phone_2 = '';
    $v_phone_3 = '';
    $v_phone_ext = '';
    $v_gift_message = '';
    $v_shipping_method = 'standard';
    $v_month_option = '';
    $v_yes = '';
    $v_no = '';
    $v_month_option = '';
    $v_year_option = '';
    $dsp_billing_state_option = '';
    $arr_billing_state_option = array();
    $dsp_billing_country_option = '';
    $arr_billing_country_option = array();
    $dsp_option_method = '';
    $arr_option_method = array();
    $dsp_list_basket_item = '';
    $arr_list_basket_item = array();
    $v_basket_price = 0;
    $v_tax_price = 0;
    $v_total_price = 0;
    $v_country_id = 'CA';
    $arr_ss_shipping_information = isset($_SESSION['ss_address_shipping']) ? unserialize($_SESSION['ss_address_shipping']) : array();
    $v_province = $arr_ss_shipping_information['address_province'];
    $arr_qry_province = $cls_provinces->select(array("country_id"=>$v_country_id,"deleted"=>false),array("name"=>1));
    foreach($arr_qry_province as $arr_province_temp){
        $tpl_province_option = new Template("dsp_provinces_option.tpl",$v_dir_templates);
        $tpl_province_option->set('PROVINCE_NAME',$arr_province_temp['name']);
        $tpl_province_option->set('PROVINCE_KEY',$arr_province_temp['key']);
        $v_check = '';
        if($v_province == $arr_province_temp['key']) $v_check = "selected";
        $tpl_province_option->set('OPTION_CHECK',$v_check);
        $tpl_province_option->set('PROVINCE_ID',(string)$arr_province_temp['_id']);
        $arr_billing_state_option [] = $tpl_province_option;
        $arr_state_option [] = $tpl_province_option;
    }
    $arr_qry_country = $cls_country->select(array("deleted"=>false),array("name"=>1));
    foreach($arr_qry_country as $arr_province_temp){
        $tpl_province_option = new Template("dsp_provinces_option.tpl",$v_dir_templates);
        $tpl_province_option->set('PROVINCE_NAME',$arr_province_temp['name']);
        $tpl_province_option->set('PROVINCE_KEY',$arr_province_temp['value']);
        $tpl_province_option->set('PROVINCE_ID',(string)$arr_province_temp['_id']);
        $arr_billing_country_option [] = $tpl_province_option;
    }
    $arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
    $v_tax_price = 0;
    for($i=0;$i<count($arr_ss_basket_content);$i++){
        $v_type = 'STANDARD';
        $v_product_image = URL.$arr_ss_basket_content[$i]['product_image'];
        if(!checkRemoteFile($v_product_image)) $v_product_image = JT_URL.$arr_ss_basket_content[$i]['product_image'];
        if($arr_ss_basket_content[$i]['product_type']==1) $v_type = 'FREE';
        $tpl_basket_item = new Template("dsp_checkout_accordion_basket_item.tpl",$v_dir_templates);
        $tpl_basket_item->set('PRODUCT_ID',$arr_ss_basket_content[$i]['product_id']);
        $tpl_basket_item->set('PRODUCT_TYPE','');
        $tpl_basket_item->set('PRODUCT_IMAGE',$v_product_image);
        $tpl_basket_item->set('PRODUCT_PRICE',format_currency($arr_ss_basket_content[$i]['product_price']));
        $tpl_basket_item->set('BRAND_NAME',$arr_ss_basket_content[$i]['brand_name']);
        $tpl_basket_item->set('PRODUCT_NAME',$arr_ss_basket_content[$i]['product_name']);
        $tpl_basket_item->set('PRODUCT_CODE',$arr_ss_basket_content[$i]['product_code']);
        $tpl_basket_item->set('PRODUCT_QUANTITY',$arr_ss_basket_content[$i]['product_quantity']);
        $v_basket_price += (float)number_format($arr_ss_basket_content[$i]['product_price']*$arr_ss_basket_content[$i]['product_quantity'],2);
        $v_tax_index = isset($arr_ss_basket_content[$i]['tax']) ? $arr_ss_basket_content[$i]['tax'] : 0;
        $v_tax_price += ($v_tax_index * $arr_ss_basket_content[$i]['product_price']*$arr_ss_basket_content[$i]['product_quantity']) / (float)100.0;
        $arr_list_basket_item [] = $tpl_basket_item;
    }
    $v_shipping_method = $arr_ss_shipping_information['shipping_method'];
    $v_gift_check = $arr_ss_shipping_information['gift_check'];
    if($v_gift_check=='false') $v_no = "checked";
    else $v_yes = "checked";
    $arr_qry_option_shipping = $cls_settings->get_option_by_name("shipping_method");
    foreach($arr_qry_option_shipping as $arr_shipping){
        $dsp_option_shipping = new Template("dsp_shipping_method.tpl",$v_dir_templates);
        $dsp_option_shipping->set('OPTION_KEY',$arr_shipping['key']);
        $dsp_option_shipping->set('OPTION_NAME',$arr_shipping['name']);
        $v_selected = '';
        if($v_shipping_method == $arr_shipping['key']) $v_selected = "checked";
        $dsp_option_shipping->set('SELECTED',$v_selected);
        $arr_option_method [] = $dsp_option_shipping;
    }
    for($i=1;$i<=12;$i++){
        $v_month_option .= '<option value='.$i.'>'.$i.'</option>';
    }
    for($i=2000;$i<=2025;$i++){
        $v_year_option .= '<option value='.$i.'>'.$i.'</option>';
    }
    $dsp_option_method = Template::merge($arr_option_method);
    $v_first_name = $arr_ss_shipping_information['first_name'];
    $v_last_name = $arr_ss_shipping_information['last_name'];
    $v_address_1 = $arr_ss_shipping_information['address_line_1'];
    $v_address_2 = $arr_ss_shipping_information['address_line_2'];
    $v_address_city = $arr_ss_shipping_information['address_city'];
    $v_postal_code = $arr_ss_shipping_information['postal_code'];
    $v_phone_1 = $arr_ss_shipping_information['phone_1'];
    $v_phone_2 = $arr_ss_shipping_information['phone_2'];
    $v_phone_3 = $arr_ss_shipping_information['phone_3'];
    $v_phone_ext = $arr_ss_shipping_information['phone_ext'];
    $v_shipping_information = $arr_ss_shipping_information['first_name']. "  ". $arr_ss_shipping_information['last_name'];
    $v_shipping_information .= "<br /> ".$arr_ss_shipping_information['address_line_1'];
    $v_shipping_information .= "<br /> ".$arr_ss_shipping_information['address_line_2'];
    $v_shipping_information .= "<br /> ".$arr_ss_shipping_information['address_city'];
    $v_shipping_information .= " ".$arr_ss_shipping_information['address_province'];
    $v_shipping_information .= " ".$arr_ss_shipping_information['postal_code'];
    $v_shipping_information .= " <br /> Canada";
    $v_shipping_information .= "<br /> ".$arr_ss_shipping_information['phone_1']."-".$arr_ss_shipping_information['phone_2']."-".$arr_ss_shipping_information['phone_3']."-".$arr_ss_shipping_information['phone_ext'];
//    $v_tax_price = (float)$v_basket_price/(float) 10 ;
    if($v_shipping_method=='') $v_shipping_method = 'Standard';
    $v_shipping_method = ucwords($v_shipping_method);
    $v_total_price = (float)number_format($v_tax_price,2) + (float)number_format($v_basket_price,2);
    $dsp_state_option = Template::merge($arr_state_option);
    $dsp_list_basket_item = Template::merge($arr_list_basket_item);
    $dsp_billing_country_option = Template::merge($arr_billing_country_option);
    $dsp_billing_state_option = Template::merge($arr_billing_state_option);
    $tpl_content->set('SHIPPING_INFO',$v_shipping_information);
    $tpl_content->set('SHIPPING_PHONE',$v_shipping_phone);
    $tpl_content->set('FIRST_NAME',$v_first_name);
    $tpl_content->set('LAST_NAME',$v_last_name);
    $tpl_content->set('ADDRESS_1',$v_address_1);
    $tpl_content->set('ADDRESS_2',$v_address_2);
    $tpl_content->set('ADDRESS_CITY',$v_address_city);
    $tpl_content->set('OPTION_STATE',$dsp_state_option);
    $tpl_content->set('POSTAL_CODE',$v_postal_code);
    $tpl_content->set('PHONE_1',$v_phone_1);
    $tpl_content->set('PHONE_2',$v_phone_2);
    $tpl_content->set('PHONE_3',$v_phone_3);
    $tpl_content->set('PHONE_EXT',$v_phone_ext);
    $tpl_content->set('GIFT_MESSAGE',$v_gift_message);
    $tpl_content->set('MONTH_OPTION',$v_month_option);
    $tpl_content->set('YEAR_OPTION',$v_year_option);
    $tpl_content->set('STATE_OPTION',$dsp_billing_state_option);
    $tpl_content->set('COUNTRY_OPTION',$dsp_billing_country_option);
    $tpl_content->set('LIST_BASKET_ITEM',$dsp_list_basket_item);
    $tpl_content->set('BASKET_PRICE',number_format($v_basket_price,2));
    $tpl_content->set('TAX_PRICE',number_format($v_tax_price,2));
    $tpl_content->set('TOTAL_PRICE',number_format($v_total_price,2));
    $tpl_content->set('OPTION_METHOD_SHIPPING',$dsp_option_method);
    $tpl_content->set('YES',$v_yes);
    $tpl_content->set('NO',$v_no);
    $tpl_content->set('URL_LINK',URL."checkout_confirm");
    $tpl_content->set('SHIPPING_METHOD',$v_shipping_method);
    echo $tpl_content->output();
}