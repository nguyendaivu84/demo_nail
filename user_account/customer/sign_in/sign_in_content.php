<?php if(!isset($v_sval)) die;
    $v_email = '';
    $v_password = '';
    $v_error = '';
    $v_login = 0;
    $v_display = 'display:none';
    if(isset($_POST['sign_in'])){
        // basic login
        $v_email = isset($_POST['user_name']) ? $_POST['user_name'] : '';
        $v_email = trim($v_email);
        $v_password = isset($_POST['password']) ? $_POST['password'] : '';
        $v_select_one = $cls_tb_user->select_one(array("email"=>$v_email));
        if($v_select_one){
            $v_select_one = $cls_tb_user->select_one(array("email"=>$v_email,"user_password"=>md5($v_password)));
            if($v_select_one){
                $arr_customer['_id'] = $cls_tb_user->get_mongo_id();
                $arr_customer['contact_id'] = $cls_tb_user->get_contact_id();
                $arr_customer['first_name'] = $cls_tb_user->get_first_name();
                $arr_customer['middle_name'] = $cls_tb_user->get_middle_name();
                $arr_customer['last_name'] = $cls_tb_user->get_last_name();
                $arr_customer['full_name'] = $cls_tb_user->get_full_name();
                $arr_customer['addresses'] = $cls_tb_user->get_addresses();
                $arr_customer['address_default'] = $cls_tb_user->get_addresses_default_key();
                $arr_customer['company'] = $cls_tb_user->get_company();
                $arr_customer['deleted'] = $cls_tb_user->get_deleted();
                $arr_customer['direct_dia'] = $cls_tb_user->get_direct_dial();
                $arr_customer['email'] = $cls_tb_user->get_email();
                $arr_customer['extension_no'] = $cls_tb_user->get_extension_no();
                $arr_customer['fax'] = $cls_tb_user->get_fax();
                $arr_customer['is_customer'] = $cls_tb_user->get_is_customer();
                $arr_customer['is_employee'] = $cls_tb_user->get_is_employee();
                $arr_customer['mobile'] = $cls_tb_user->get_mobile();
                $arr_customer['no'] = $cls_tb_user->get_no();
                $arr_customer['position'] = $cls_tb_user->get_position();
                $arr_customer['title'] = $cls_tb_user->get_title();
                $arr_customer['user_password'] = $cls_tb_user->get_user_password();
                $arr_customer['user_name'] = $cls_tb_user->get_user_name();
                $arr_customer['user_status'] = $cls_tb_user->get_user_status();
                $arr_customer['user_type'] = $cls_tb_user->get_user_type();
                $_SESSION['ss_customer'] = serialize($arr_customer);
                $v_login = 1;
                $v_count = 0;
                $arr_ss_basket = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
                $arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
                $arr_ss_basket_content = get_data_table_cart($arr_ss_basket,$arr_ss_basket_content,$db,$cls_tb_user->get_mongo_id(),$v_count);
                for($i=0;$i<count($arr_ss_basket_content);$i++){
                    if(!in_array($arr_ss_basket_content[$i]['product_id'],$arr_ss_basket)) $arr_ss_basket [] = $arr_ss_basket_content[$i]['product_id'];
                }
                $_SESSION['ss_basket_item'] = $arr_ss_basket;
                $_SESSION['ss_basket_content'] = serialize($arr_ss_basket_content);
                $arr_ss_love_content = isset($_SESSION['ss_love_item']) ? unserialize($_SESSION['ss_love_item']) : array();
                $arr_ss_love = isset($_SESSION['ss_love']) ? unserialize($_SESSION['ss_love']) : array();
                get_data_favorite_list_after_login($db,(string)$cls_tb_user->get_mongo_id(),$arr_ss_love,$arr_ss_love_content);
                $_SESSION['ss_love_item'] = serialize($arr_ss_love_content);
                $_SESSION['ss_love'] = serialize($arr_ss_love);
            }else{
                $v_error = ' ';
            }
        }else{
            $v_error = '';
        }
    }
    if($v_error!='') $v_display = '';
    $tpl_sign_in_content = new Template("dsp_popup_sign_in_content.tpl",$v_dir_templates);
    $tpl_sign_in_content->set('URL_TEMP',URL."user_account/customer/templates/default/");
    $tpl_sign_in_content->set('URL',URL);
    $tpl_sign_in_content->set('PASSWORD',$v_password);
    $tpl_sign_in_content->set('EMAIL',$v_email);
    $tpl_sign_in_content->set('ERROR',$v_error);
    $tpl_sign_in_content->set('DISPLAY',$v_error);

    $tpl_sign_in_content->set('IS_LOGIN',$v_login);
    echo $tpl_sign_in_content->output();
?>