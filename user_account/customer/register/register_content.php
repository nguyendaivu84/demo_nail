<?php if(!isset($v_sval)) die;
$v_login = 0;
$v_exsted_stype = 'hidden';
$v_first_name = '';
$v_last_name = '';
$v_user_name = '';
$v_security_answer = '';
if(isset($_POST['register_bnt'])){
    // basic register
    $v_first_name = isset($_POST['first_name']) ? $_POST['first_name'] : '';
    $v_last_name = isset($_POST['last_name']) ? $_POST['last_name'] : '';
    $v_user_name = isset($_POST['user_name']) ? $_POST['user_name'] : '';
    $v_password = isset($_POST['password']) ? $_POST['password'] : '';
    $v_security_answer = isset($_POST['security_answer']) ? $_POST['security_answer'] : '';
    $v_birth_month = isset($_POST['birth_month']) ? $_POST['birth_month'] : '';
    $v_birth_day = isset($_POST['birth_day']) ? $_POST['birth_day'] : '';
    $v_birth_year = isset($_POST['birth_year']) ? $_POST['birth_year'] : '';
    if($cls_tb_contact->count(array("user_email"=>$v_user_name)) == 1) $v_exsted_stype = '';
    else{
        $v_result = create_new_user($db,$v_first_name,$v_last_name,$v_user_name,$v_password,$v_contact_id);
        if($v_result){
            $arr_customer = create_user_session($v_result,$v_contact_id,$v_first_name,$v_last_name,$v_user_name,$v_password);
            $_SESSION['ss_customer'] = serialize($arr_customer);
            $v_login = 1;
        }
    }
}
$tpl_popup_register = new Template("dsp_popup_register_content.tpl",$v_dir_templates);
$tpl_popup_register->set('URL_TEMP',URL."user_account/customer/templates/default/");
$tpl_popup_register->set('IS_LOGIN',$v_login);
$tpl_popup_register->set('URL',URL);
$tpl_popup_register->set('FIRST_NAME',$v_first_name);
$tpl_popup_register->set('LAST_NAME',$v_last_name);
$tpl_popup_register->set('EMAIL',$v_user_name);
$tpl_popup_register->set('SRC_ANS',$v_security_answer);
$tpl_popup_register->set('ERROR_EXISTED_STYLE',$v_exsted_stype);
echo $tpl_popup_register->output();
?>