<?php if (!isset($v_sval)) die();
//    if(!isset($_SESSION['ss_basket_content']) || !isset($_SESSION['ss_basket_item']) || !isset($_SESSION['ss_customer'])) redir("/checkout");
    if(!isset($_SESSION['ss_basket_content']) || !isset($_SESSION['ss_basket_item']) ) redir("/checkout");
    if(isset($_POST['checkout-signout-hidden'])){
        unset($_SESSION['ss_basket_content']);
        unset($_SESSION['ss_basket_item']);
        if(isset($_SESSION['ss_customer'])) unset($_SESSION['ss_customer']);
        redir("/checkout");
    }
    $tpl_content = new Template('dsp_checkout_accordion.tpl',$v_dir_templates);
    $v_first_name = '';
    $v_last_name = '';
    $v_email = '';
    $v_address_line_1 = '';
    $v_address_line_2 = '';
    $v_address_city = '';
    $dsp_state_option = '';
    $v_shipping_method = "standard";
    $arr_state_option = array();
    $dsp_option_method = '';
    $arr_option_method = array();
    $v_city = '';
    $v_error_message = '';
    if(isset($_SESSION['checkout_accordion_message'])) $v_error_message = ($_SESSION['checkout_accordion_message']);
    $v_phone_1 = '';
    $v_phone_2 = '';
    $v_phone_3 = '';
    $v_postal_code = '';
    $v_phone_ext = '';
    $dsp_basket_list_item = '';
    $arr_basket_list_item = array();
    $v_basket_total_price = 0;
    $v_basket_tax = 0;
    $v_order_total = 0;
    $v_user_name = '';
    $v_display = 'display:none';
    $v_style = '';
    $v_country_id = 'CA';
    $v_province_id = '';
    $v_country = '';
    $v_default_address = -1 ;
    if(isset($_POST['continue'])){
        $v_user_name = isset($_POST['user_email']) ? $_POST['user_email'] : '';
        $v_first_name = isset($_POST['first_name']) ? $_POST['first_name'] : '';
        $v_last_name = isset($_POST['last_name']) ? $_POST['last_name'] : '';
        $v_address_line_1 = isset($_POST['address1']) ? $_POST['address1'] : '';
        $v_address_line_2 = isset($_POST['address2']) ? $_POST['address2'] : '';
        $v_address_city = isset($_POST['city']) ? $_POST['city'] : '';
        $v_province_id = isset($_POST['state']) ? $_POST['state'] : '';
        $v_postal_code = isset($_POST['zip']) ? $_POST['zip'] : '';
        $v_check_default = isset($_POST['is_default']) ? 1 : 0;
        $v_phone_1 = isset($_POST['phone_1']) ? $_POST['phone_1'] : '';
        $v_phone_2 = isset($_POST['phone_2']) ? $_POST['phone_2'] : '';
        $v_phone_3 = isset($_POST['phone_3']) ? $_POST['phone_3'] : '';
        $v_phone_ext = isset($_POST['phone_ext']) ? $_POST['phone_ext'] : '';
        $v_check_default_address = isset($_POST['default_address_index']) ? $_POST['default_address_index'] : 0;
        $v_shipping_method = isset($_POST['shippingMethod']) ? $_POST['shippingMethod'] : 1;
        $v_gift_check = isset($_POST['gift_check']) ? $_POST['gift_check'] : false;

        $arr_ss_address = array(
            'first_name'=>$v_first_name
            ,'last_name'=>$v_last_name
            ,'address_line_1'=>$v_address_line_1
            ,'address_line_2'=>$v_address_line_2
            ,'address_city'=>$v_address_city
            ,'address_province'=>$v_province_id
            ,'postal_code'=>$v_postal_code
            ,'phone_1'=>$v_phone_1
            ,'phone_2'=>$v_phone_2
            ,'is_default'=>$v_check_default
            ,'phone_3'=>$v_phone_3
            ,'phone_ext'=>$v_phone_ext
            ,'default_address'=>$v_check_default_address
            ,'shipping_method'=>$v_shipping_method
            ,'gift_check'=>$v_gift_check
            ,'deleted'=>false
        );

        if(!isset($_SESSION['ss_customer'])){
            $v_error_message = '';
            if($v_first_name==''){
                $v_error_message .='First name cannot empty';
            }
            if($v_last_name==''){
                $v_error_message .='<br />Last name cannot empty';
            }
            if($v_user_name==''){
                $v_error_message .='<br />Email cannot empty';
            }
            $v_check = $cls_tb_contact->count(array('email'=>$v_user_name));
            if($v_check) $v_error_message .='<br />Email is not avaible.Please use different email!';
            if($v_error_message!=''){
                $_SESSION['checkout_accordion_message'] = $v_error_message;
                redir("/checkout_accordion");
            }else{
                if(isset($_SESSION['checkout_accordion_message'])) unset($_SESSION['checkout_accordion_message']);
                $v_password = $v_first_name.'-'.$v_last_name;
                $v_result = create_new_user($db,$v_first_name,$v_last_name,$v_user_name,$v_password,$v_contact_id,1);
                $arr_customer = create_user_session($v_result,$v_contact_id,$v_first_name,$v_last_name,$v_user_name,$v_password);
                $_SESSION['ss_customer'] = serialize($arr_customer);
                $v_mail_subject = 'Confirm email';
                add_class('PHPMailer','class.phpmailer.php');
                $cls_mail = new PHPMailer (true);
                $v_main_site_title = $cls_settings->get_option_name_by_key('website_attribute', 'website_main_title','Astoria nail supply');
                $v_support_email = $cls_settings->get_option_name_by_key('email','support_email', 'info@anvydigital.com');
                $v_support_email = trim($v_support_email);
                $v_mail_dir_templates = 'mail/';
                $arr_user_order = array('name'=>'', 'contact'=>$v_contact_id, 'full'=>$v_first_name.' '.$v_last_name, 'email'=>$v_user_name);
                $tpl_mail = new Template('tpl_active_email.tpl', $v_mail_dir_templates);
                $tpl_mail->set('FULL_NAME', $v_first_name.' '.$v_last_name);
                $tpl_mail->set('URL', URL);
                $tpl_mail->set('PASSWORD', $v_password);
                $tpl_mail->set('SITE_NAME', 'Astoria Nails Supply');
                $tpl_mail->set('SUPPORT_EMAIL', $v_support_email);
                $tpl_mail->set('CONFIRM_URL', (URL."account/TOKEN=".$v_result."&date=".md5(date('d-M-y'))));
                $v_mail_body = $tpl_mail->output();
                $check = send_email_el($cls_mail,$v_main_site_title, $v_support_email, $arr_user_order,$v_mail_subject, $v_mail_body);
                if($check==true){
                    $_SESSION['ss_address_shipping'] = serialize($arr_ss_address);
                    redir("/checkout_confirm");
                }else{
                    var_dump($check);
                }
            }
        }else{
            $_SESSION['ss_address_shipping'] = serialize($arr_ss_address);
            redir("/checkout_confirm");
        }
    }

    if(isset($_SESSION['ss_address_shipping'])){
        $arr_ss_address = unserialize($_SESSION['ss_address_shipping']);
        $v_user_name = $arr_ss_address['last_name'];
        $v_first_name = $arr_ss_address['first_name'];
        $v_last_name = $arr_ss_address['last_name'];
        $v_default_address = $arr_ss_address['default_address'];
        $v_address_line_1 = $arr_ss_address['address_line_1'];
        $v_address_line_2 = $arr_ss_address['address_line_2'];
        $v_province_id = $arr_ss_address['address_province'];
        $v_postal_code = $arr_ss_address['postal_code'];
        $v_phone_1 = $arr_ss_address['phone_1'];
        $v_phone_2 = $arr_ss_address['phone_2'];
        $v_phone_3 = $arr_ss_address['phone_3'];
        $v_phone_ext = $arr_ss_address['phone_ext'];
        $v_address_city = $arr_ss_address['address_city'];
        $v_shipping_method = $arr_ss_address['shipping_method'];
        $v_country_id = 'CA';
    }else if(isset($_SESSION['ss_customer'])){
        $v_display = '';
        $arr_ss_customer = unserialize($_SESSION['ss_customer']);
        $v_user_name = $arr_ss_customer['last_name'];
        $v_first_name = $arr_ss_customer['first_name'];
        $v_last_name = $arr_ss_customer['last_name'];
        $v_default_address = $arr_ss_customer['address_default'];
        $arr_addresses = $arr_ss_customer['addresses'];
        $v_email = $arr_ss_customer['email'];
        if(isset($arr_addresses[$v_default_address])){
            $v_address_line_1 = $arr_addresses[$v_default_address]['address_1'];
            $v_address_line_2 = $arr_addresses[$v_default_address]['address_2'];
            $v_address_line_3 = $arr_addresses[$v_default_address]['address_3'];
            $v_country = $arr_addresses[$v_default_address]['country'];
            $v_province_id = $arr_addresses[$v_default_address]['province_state'];
            $v_country_id = isset($arr_addresses[$v_default_address]['country_id'])?$arr_addresses[$v_default_address]['country_id']:'CA';
            $v_postal_code = $arr_addresses[$v_default_address]['zip_postcode'];
            $v_full_phone = $arr_addresses[$v_default_address]['phone'];
            $v_address_city = $arr_addresses[$v_default_address]['town_city'];
            $arr_full_phone = explode("-",$v_full_phone);
            $v_phone_1 = isset($arr_full_phone[0]) ? $arr_full_phone[0] : '';
            $v_phone_2 = isset($arr_full_phone[1]) ? $arr_full_phone[1] : '';
            $v_phone_3 = isset($arr_full_phone[2]) ? $arr_full_phone[2] : '';
            $v_phone_ext = isset($arr_full_phone[4]) ? $arr_full_phone[4] : '';
        }
    }
    // lay danh sach item trong basket
    $arr_ss_basket_content = isset($_SESSION['ss_basket_content']) ? unserialize($_SESSION['ss_basket_content']) : array();
    $arr_ss_basket_item_id = isset($_SESSION['ss_basket_item']) ? $_SESSION['ss_basket_item'] : array();
    $v_count = count($arr_ss_basket_item_id);
    $v_basket_tax = 0;
    for($i=0;$i<count($arr_ss_basket_content);$i++){
        $v_style = '';
        $dsp_item_item = new Template("dsp_checkout_accordion_basket_item.tpl",$v_dir_templates);
        $v_product_type = $arr_ss_basket_content[$i]['product_type'];
        $v_item_price = 'FREE';
        if($v_product_type !=1){
            $v_province_id = isset($arr_ss_basket_content[$i]['taxProvince']) ? $arr_ss_basket_content[$i]['taxProvince'] : 'AB';
            $v_item_price = $arr_ss_basket_content[$i]['product_price']*$arr_ss_basket_content[$i]['product_quantity'];
            $v_basket_total_price += (float)number_format((float)$v_item_price,2);
        }
        $v_basket_index_tax = isset($arr_ss_basket_content[$i]['tax']) ? $arr_ss_basket_content[$i]['tax'] : 0;
        $v_basket_tax +=((float)$v_basket_index_tax *$v_item_price)/(float)100.0 ;
        $v_product_quantity = $arr_ss_basket_content[$i]['product_quantity'];
        $v_product_image = URL.$arr_ss_basket_content[$i]['product_image'];
        if(!checkRemoteFile($v_product_image)) $v_product_image = JT_URL.$arr_ss_basket_content[$i]['product_image'];
        $v_product_name = $arr_ss_basket_content[$i]['product_name'];
        $v_product_brand_name = $arr_ss_basket_content[$i]['brand_name'];
        $dsp_item_item->set('STYLE',$v_style);
        $dsp_item_item->set('PRODUCT_QUANTITY',$v_product_quantity);
        $dsp_item_item->set('PRODUCT_CODE',$v_product_name);
        $dsp_item_item->set('PRODUCT_NAME',$v_product_name);
        $dsp_item_item->set('BRAND_NAME',$v_product_brand_name);
        $dsp_item_item->set('PRODUCT_IMAGE',$v_product_image);
        $dsp_item_item->set('PRODUCT_PRICE',format_currency($v_item_price));
        $dsp_item_item->set('PRODUCT_ID',$arr_ss_basket_content[$i]['product_id']);
        $arr_basket_list_item[] = $dsp_item_item;
    }
    $arr_select_list_province = $cls_province->select(array("country_id"=>$v_country_id,"deleted"=>false),array("name"=>1));
    foreach($arr_select_list_province as $arr_province_template){
        $v_check_option = '';
        if($v_province_id == $arr_province_template['key']) $v_check_option = "selected='selected'";
        $tpl_province_option = new Template('dsp_provinces_option.tpl',$v_dir_templates);
        $tpl_province_option->set('PROVINCE_KEY',$arr_province_template['key']);
        $tpl_province_option->set('PROVINCE_NAME',$arr_province_template['name']);
        $tpl_province_option->set('PROVINCE_ID',(string)$arr_province_template['_id']);
        $tpl_province_option->set('OPTION_CHECK',$v_check_option);
        $arr_state_option [] = $tpl_province_option;
    }
    $arr_qry_option_shipping = $cls_settings->get_option_by_name("shipping_method");
    foreach($arr_qry_option_shipping as $arr_shipping){
        $dsp_option_shipping = new Template("dsp_shipping_method.tpl",$v_dir_templates);
        $dsp_option_shipping->set('OPTION_KEY',$arr_shipping['key']);
        $dsp_option_shipping->set('OPTION_NAME',$arr_shipping['name']);
        $v_selected = '';
        if($v_shipping_method == $arr_shipping['key']) $v_selected = "checked";
        $dsp_option_shipping->set('SELECTED',$v_selected);
        $arr_option_method [] = $dsp_option_shipping;
    }
    if(isset($_SESSION['ss_customer'])){
        if($v_email=='' ){
            $arr_ss_customer = unserialize($_SESSION['ss_customer']);
            $v_email = $arr_ss_customer['email'];
        }
        $v_display = '';
    }
    $dsp_option_method = Template::merge($arr_option_method);
    $v_order_total = (float)number_format($v_basket_tax,2) + (float)number_format($v_basket_total_price,2);
    $dsp_basket_list_item = count($arr_basket_list_item) >0 ? Template::merge($arr_basket_list_item) : '';
    $dsp_state_option = count($arr_state_option) >0 ? Template::merge($arr_state_option) : '';
    $tpl_content->set('USER_EMAIL',$v_email);
    $tpl_content->set('FIRST_NAME',$v_first_name);
    $tpl_content->set('LAST_NAME',$v_last_name);
    $tpl_content->set('ADDRESS_LINE_1',$v_address_line_1);
    $tpl_content->set('ADDRESS_LINE_2',$v_address_line_2);
    $tpl_content->set('ADDRESS_CITY',$v_address_city);
    $tpl_content->set('STATE_OPTION',$dsp_state_option);
    $tpl_content->set('PHONE_1',$v_phone_2);
    $tpl_content->set('PHONE_2',$v_phone_2);
    $tpl_content->set('PHONE_3',$v_phone_3);
    $tpl_content->set('PHONE_EXT',$v_phone_ext);
    $tpl_content->set('BASKET_LIST_ITEM',$dsp_basket_list_item);
    $tpl_content->set('PRODUCT_ALL_PRICE',number_format($v_basket_total_price,2));
    $tpl_content->set('TAX_PRICE',number_format($v_basket_tax,2));
    $tpl_content->set('POSTAL_CODE',$v_postal_code);
    $tpl_content->set('TOTAL_PRICE',number_format($v_order_total,2));
    $tpl_content->set('USER_NAME',$v_user_name);
    $tpl_content->set('DISPLAY',$v_display);
    $tpl_content->set('OPTION_METHOD',$dsp_option_method);
    $tpl_content->set('DEFAULT_ADDRESS',$v_default_address);
    $tpl_content->set('URL_TEMPLATE',URL.$v_url_template);
    $tpl_content->set('URL',URL);
    $tpl_content->set('ERROR_MESSAGE',$v_error_message);
    echo $tpl_content->output();