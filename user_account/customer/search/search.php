<?php if (!isset($v_sval)) die();
$arr_language_switch = unserialize($_SESSION['ss_language_value']);
add_class("cls_tb_nail_category");
$cls_tb_nail_category = new cls_tb_nail_category($db);
add_class("cls_tb_banner");
$cls_tb_banner = new cls_tb_banner($db);
add_class("cls_tb_product");
$arr_sort = array('name'=>1);
$cls_tb_product = new cls_tb_product($db);
$v_search_key = isset($_GET['search_key']) ? $_GET['search_key'] : '';
$v_url_page_size = $v_url_page_sort = URL."search/".$v_search_key;
//$v_url_page_size = $v_url_page_sort = isset($_GET['page']) ? URL."search/".$v_search_key."?page=".$_GET['page'] : URL."search/".$v_search_key;
$v_url_page_size = isset($_GET['page']) ? URL."search/".$v_search_key."?page=".$_GET['page'] : URL."search/".$v_search_key;
$v_page_link = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
$v_page_link = isset($_GET['num_row']) ?URL."search/".$v_search_key."&num_row=".$_GET['num_row'] : $v_url_page_sort;
$v_page_link = isset($_GET['sort']) ?$v_page_link."&sort=".$_GET['sort'] : $v_page_link;
$v_page_link_cat = isset($_GET['page']) ? $v_page_link."&page=".$_GET['page'] : $v_page_link;
$v_url_page_sort = isset($_GET['num_row']) ?$v_url_page_sort."&num_row=".$_GET['num_row'] : $v_url_page_sort;
$v_url_page_sort = isset($_GET['page']) ? $v_url_page_sort."?page=".$_GET['page'] : $v_url_page_sort;
$v_url_page_size = isset($_GET['sort']) ?$v_url_page_size."&sort=".$_GET['sort'] : $v_url_page_size;
$arr_where_clause['deleted'] = false;
if(isset($_GET['cat'])){
    $arr_where_clause['category_id'] = (string)$cls_tb_nail_category->select_scalar("_id",array("slugger"=>rtrim($_GET['cat'],"/")));
    $v_url_page_sort .="&cat=".rtrim($_GET['cat'],"/");
    $v_url_page_size .="&cat=".rtrim($_GET['cat'],"/");
}
$tpl_content = new Template('dsp_cg_search.tpl', $v_dir_templates);
$v_sort_type = '';
if(isset($_GET['sort'])){
    $v_sort_type = $_GET['sort'];
    if($v_sort_type=='new') $arr_sort['_id'] = -1;
    else if($v_sort_type=='price_height_low') $arr_sort['sell_price'] = -1;
    else if($v_sort_type=='price_low_height') $arr_sort['sell_price'] = 1;
}
$tpl_cg_search_content = new Template('dsp_cg_search_content.tpl', $v_dir_templates);
$arr_filter_item = array();
// ---------------------------------- product list ----------------------------------
$v_item_drop = 4;
$v_item_start = 1;
$v_page = isset($_GET['page'])?$_GET['page']:1;
settype($v_page,"int");
$v_page = ($v_page<=0)?1:$v_page;
$v_num_row = isset($_GET['num_row'])?$_GET['num_row']:20;
$v_number_of_record = $cls_settings->draw_option_name_by_key("number_of_record",0,$v_num_row);
settype($v_num_row,"int");
$v_num_row = ($v_num_row<=0)?100:$v_num_row;
$v_product_sort_option = $cls_settings->draw_option_name_by_key("product_sort_option",0,$v_sort_type);
settype($v_num_row,"int");
$v_num_row = ($v_num_row<=0)?100:$v_num_row;
$arr_where_clause['$or'] = array(array("name"=>new MongoRegex('/'.$v_search_key.'/i')),array("sku"=>new MongoRegex('/'.$v_search_key.'/i')));
$v_total_row = $cls_tb_product->count($arr_where_clause);
$v_total_page = ceil($v_total_row /$v_num_row);
if($v_total_page <= 0) $v_total_page = 1;
if($v_total_page<$v_page) $v_page = $v_total_page;
$v_offset = ($v_page - 1)*$v_num_row;
$v_pagination = '';
// t?o du?ng d?n cho ph?n trang
$page_link = isset($_SERVER['REDIRECT_URL'])?$_SERVER['REDIRECT_URL']:'';if($v_total_page > 1)
	for($i=1;$i<=$v_total_page;$i++){
		if($v_page == $i) $v_pagination .= '<li class="selected OneLinkNoTx">'.$i.'</li>';
		else $v_pagination .= '<li class="OneLinkNoTx"> <a class="click_a" href="'.$v_page_link.'?page='.$i.'">'.$i.'</a> </li>';
	}
$v_product_pagination = pagination2($v_total_page, $v_page, $v_page_link.'?page=');
$v_sort_footer = new Template('dsp_product_item_sort.tpl', $v_dir_templates);
$v_sort_footer->set('PAGING',$v_product_pagination);
$v_sort_footer->set('URL_PAGESIZE',$v_url_page_size);
$v_sort_footer->set('URL_SORT',$v_url_page_sort);
$v_sort_footer->set('PRODUCT_SORT_OPTION',$v_product_sort_option);
$v_sort_footer->set('NUMBER_ROW_OPTION',$v_number_of_record);
$v_sort_footer->set('INDEX',0);
$tpl_cg_search_content->set('PRODUCT_ITEM_HEADER',$v_sort_footer->output());
$v_sort_footer->set('INDEX',1);
$tpl_cg_search_content->set('PRODUCT_ITEM_FOOTER',$v_sort_footer->output());
// BaoNam
$arr_category_product = array();
$arr_category_product_count = array();
$arr_where_clause2 = $arr_where_clause;
$arr_select = $cls_tb_product->select($arr_where_clause);
foreach($arr_select as $arr){
    if(!in_array($arr['category_id'],$arr_category_product)){
        $arr_category_product [] = $arr['category_id'];
        $arr_where_clause2['category_id'] = $arr['category_id'];
        $arr_category_product_count[$arr['category_id']] = $cls_tb_product->count($arr_where_clause2);
    }
}
$arr_select = $cls_tb_product->select_limit($v_offset,$v_num_row,$arr_where_clause,$arr_sort);
$arr_product_content = array();
$v_dsp_product_list = '';
$arr_brand_list = array();


foreach($arr_select as $arr){
    $v_image_url = URL.$arr['products_upload'];
    if(!checkRemoteFile($v_image_url)){
        $v_image_url = JT_URL.$arr['products_upload'];
    }else{
        $v_image_url_temp =  isset($arr['image_url_origin']) ? $arr['saved_dir'].PRODUCT_IMAGE_THUMB_FRONT."_".$arr['image_url_origin'] : '' ;
        if(file_exists($v_image_url_temp)){
            $v_image_url = $v_image_url_temp;
        }
    }
	if($v_dsp_product_list=='' || $v_item_start == 4)
		$v_dsp_product_list .= '<div class="product-items container">';
	$v_product_row_content_item = new Template('dsp_product_item.tpl', $v_dir_templates);
	$v_product_row_content_item->set("PRODUCT_SLUGGER",$arr['slugger']);
	$v_product_row_content_item->set("PRODUCT_IMAGE",$v_image_url);//[count($arr['products_upload']) - 1]['path']);
	$v_product_row_content_item->set("PRODUCT_PRICE",$arr['sell_price']);
	$v_product_row_content_item->set("PRODUCT_PRICE",format_currency($arr['sell_price']));
	$v_product_row_content_item->set("PRODUCT_PRICE_DISCOUNT",format_currency($arr['discount']));
	$v_product_row_content_item->set("PRODUCT_NAME",$arr['name']);
	$v_product_row_content_item->set("PRODUCT_DES",$arr['description']);
    $new_product = isset($arr['new_product']) ? $arr['new_product'] : 0;
	$v_product_new_html = '';
	if ($v_product_new_html){
		$v_product_new_html = '<span class="flag-new">NEW</span>';
	}
	$v_product_row_content_item->set("PRODUCT_NEW",$v_product_new_html);
	// exclusive, online only, limited edition
	$items = array('', 'exclusive', 'online only', 'limited edition');
    $exclusive = isset($arr['exclusive']) ? $arr['exclusive'] : 0;
    $online_only = isset($arr['online_only']) ? $arr['online_only'] : 0;
    $limited_edition = isset($arr['limited_edition']) ? $arr['limited_edition'] : 0;
	$v_product_note_1 = $items[array_rand($items)];
	$v_product_note_2 = $items[array_rand($items)];
	$v_product_note_html = '';
	if ($exclusive || $online_only || $limited_edition){
		$v_product_note_html = '<span class="flags-market">';
		if ($exclusive)
			$v_product_note_html .= '<span class="flag flag-ex">exclusive</span>';
		if ($online_only)
			$v_product_note_html .= ' <span class="flag flag-ex">online only</span>';
        if ($limited_edition)
            $v_product_note_html .= ' <span class="flag flag-ex">limited_edition</span>';
		$v_product_note_html .= '</span>';
	}
	$v_product_row_content_item->set("PRODUCT_NOTE",$v_product_note_html);
	$v_product_row_content_item->set("PRODUCT_MORE_COLOR",'');

	$v_dsp_product_list .=$v_product_row_content_item->output();
	if($v_item_start == 4 || $v_dsp_product_list =='') {
		$v_dsp_product_list .= '</div>';
		$v_item_start = 1;
	}
	else $v_item_start ++;
//	if(!in_array($arr['brand_id'],$arr_brand_list) && $arr['brand_id'] !='') $arr_brand_list [] = $arr['brand_id'];
}
// ------- end ----------------- product list ----------------------------------
if(isset($_GET['cat'])) $v_category_image = $v_category_image = '"'.$v_search_key.'": in '.$_GET['cat']." ".$v_total_row.' '.$arr_language_switch[$v_language]['SEARCH_FOUND'];
else $v_category_image = $v_category_image = '"'.$v_search_key.'": '.$v_total_row.' results';
$v_category_image = '<h3 style="padding-bottom:25px;padding-left:25px;" href="javascript:void(0)" class="imageComponent  OneLinkHide" data-lazyload="true" target="" title="Face Wash &amp; Cleansers"> '.$v_category_image.'</h3>';
//if(!$v_select_one) redir("/");
$v_category_ul = '';
$arr_cat_language = $cls_tb_nail_category->get_language_val();
$v_name_cat = isset($arr_cat_language[$v_language]) ? $arr_cat_language[$v_language]  :$cls_tb_nail_category->get_name();
$v_category_ul = '<li class="current"> <h1>'.$v_name_cat.'</h1> </li>';
$arr_category_sidebar = array();
$dsp_category_sidebar = '';
$arr_category_right_item = array();
$arr_category_right_dsp = array();
$arr_right_temp = array();
$dsp_category_right_item = '';
$dsp_filter = '';
$tpl_search_sidebar_category = new Template('dsp_search_sidebar_category.tpl', $v_dir_templates);
$v_headline_name = '<div class="nav-title non-click" data-name="">Category</div>';
$tpl_search_sidebar_category->set('HEADLINE_NAME',$v_headline_name);
$arr_child_item_temp = array();
$dsp_child_item = '';
for($j=0;$j<count($arr_category_product);$j++){
    $v_id_tem = $arr_category_product[$j];
    try{
        $arr_select_field_child = $cls_tb_nail_category->select_limit_fields(0,1,array("name","name_language","slugger","child_item"),array("_id"=> new MongoId($v_id_tem)));
        $arr_select_field_child = iterator_to_array($arr_select_field_child);
        if(count($arr_select_field_child)<=0){
            continue;
        }
    }catch(Exception $ex){
        continue;
    }
    $tpl_search_sidebar_category_item = new Template('dsp_search_sidebar_category_item.tpl', $v_dir_templates);
    if(isset($arr_select_field_child[$v_id_tem]['name_language']) && count($arr_select_field_child[$v_id_tem]['name_language'])){
        $arr_name_temp = $arr_select_field_child[$v_id_tem]['name_language'];
    }
    else{
        if(isset($arr_select_field_child[$v_id_tem]['name'])){
            $arr_name_temp[$arr_select_field_child[$v_id_tem]['name']] = $arr_select_field_child[$v_id_tem]['name'];
        }
        else{
            $arr_name_temp = array();
        }
    }
    $v_name_temp = (isset($arr_name_temp[$v_language]) && count($arr_name_temp[$v_language])) ? $arr_name_temp[$v_language] : $arr_select_field_child[$v_id_tem]['name'];
    $v_temp_slugger = isset($arr_select_field_child[$v_id_tem]['slugger']) ? $arr_select_field_child[$v_id_tem]['slugger'] : '';


    $v_child_name_link = '<a href="'.$v_page_link_cat."&cat=".$v_temp_slugger.'" data-name="'.$v_name_temp.'">'.$v_name_temp.' <span> ('.$arr_category_product_count[$v_id_tem].')</span></a>';
    $tpl_search_sidebar_category_item->set('CHILD_NAME',$v_child_name_link);
    $arr_child_item_temp [] = $tpl_search_sidebar_category_item;
}
$dsp_child_item = count($arr_child_item_temp) > 0 ? Template::merge($arr_child_item_temp) : '';
$tpl_search_sidebar_category->set('CATEGORY_CHILD',$dsp_child_item);
// FILTER THEO NH?NG ?I?U KI?N KH?C NHAU
$tpl_search_sidebar_category->set('CATEGORY_FILTER',$dsp_child_item);
$dsp_category_right_item = "";
$v_dsp_right_item = $tpl_search_sidebar_category->output();
$tpl_search_sidebar_right_content = new Template("dsp_search_category_right_content.tpl",$v_dir_templates);
$tpl_search_sidebar_right_content->set('CATEGORY_SEARCH_RIGHT_ITEM',$arr_right_temp);
$dsp_category_sidebar = $tpl_search_sidebar_category->output();
$v_total_product = 100;
$v_dsp_product_items = '';
$tpl_content->set('v_category_ul',"");
if(count($arr_category_product)) $tpl_content->set('SEARCH_CONTENT',$tpl_cg_search_content->output());
else $tpl_content->set('SEARCH_CONTENT','<h1>No result found</h1>');
$tpl_content->set('CATEGORY_IMAGE',$v_category_image);
$tpl_cg_search_sidebar = new Template('dsp_cg_search_sidebar.tpl', $v_dir_templates);
$tpl_cg_search_sidebar->set('DSP_CG_SEARCH_SIDEBAR_CATEGORY',$dsp_category_sidebar);
$tpl_cg_search_sidebar->set('CATEGORY_RIGHT_ITEM',"");
// filter content
if(count($arr_brand_list)){
	for($i=0;$i<count($arr_brand_list);$i++){
		$cls_tb_banner->select_one(array("_id"=> new MongoId($arr_brand_list[$i])));
		$tpl_cg_filter_item = new Template('dsp_cg_filter_brand_item.tpl', $v_dir_templates);
		$tpl_cg_filter_item->set('SLUGGER', $cls_tb_banner->get_banner_name()); // $cls_tb_banner->get_banner_name()
		$tpl_cg_filter_item->set('ITEM_NAME', $cls_tb_banner->get_banner_slugger()); // $cls_tb_banner->get_banner_name()
		$arr_filter_item [] = $tpl_cg_filter_item;
	}
}
$dsp_filter = Template::merge($arr_filter_item);
$tpl_cg_search_sidebar->set('DSP_CG_SEARCH_SIDEBAR_BRAND_ITEM',$dsp_filter);
// end
$tpl_content->set('SEARCH_SIDEBAR',$tpl_cg_search_sidebar->output());
$tpl_content->set('PRODUCT_ITEMS',$v_dsp_product_list);
$tpl_content->set('TOTAL_ITEM',$v_total_row);
$tpl_content->set('CATEGORY_UL',$v_category_ul);
$tpl_content->set('LANGUAGE_SORT_BY',$arr_language_switch[$v_language]['SEARCH_SORT']);
$tpl_content->set('LANGUAGE_VIEW',$arr_language_switch[$v_language]['SEARCH_VIEW']);
echo $tpl_content->output();
