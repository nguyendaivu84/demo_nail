<?php if(!isset($v_sval)) die;
    if(isset($_POST['txt_product_id'])){
        $v_product_slugger = $_POST['txt_product_id'];
        add_class("cls_tb_nail_favorite_list");
        $cls_love = new cls_tb_nail_favorite_list($db);
        add_class("cls_tb_product");
        $cls_tb_product = new cls_tb_product($db);
        $v_check = $cls_tb_product->select_one(array("slugger"=>$v_product_slugger));
        if($v_check==1){
            $v_user_id = '';
            if(isset($_SESSION['ss_customer'])){
                $arr_user = unserialize($_SESSION['ss_customer']);
                $v_user_id = (string)$arr_user['_id'];
            }
            $arr_ss_love = isset($_SESSION['ss_love']) ? unserialize($_SESSION['ss_love']) : array();
            $arr_ss_love_content = isset($_SESSION['ss_love_item']) ? unserialize($_SESSION['ss_love_item']) : array();
            $v_count = $cls_love->count(array("user_id"=>$v_user_id,"product_id"=>(string)$cls_tb_product->get_mongo_id()));
            if($v_count==0){
                add_product_to_favorite_list($v_user_id,$arr_ss_love,$arr_ss_love_content,(string)$cls_tb_product->get_mongo_id(),$db);
            }
            $_SESSION['ss_love'] = serialize($arr_ss_love);
            $_SESSION['ss_love_item'] = serialize($arr_ss_love_content);
        }
    }
?>