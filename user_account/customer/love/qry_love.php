<?php if(!isset($v_sval)) die();
	$dsp_static_content = new Template("dsp_love.tpl",$v_dir_templates);
    $v_dsp_love_item = '';
    $v_check_login = '-1';
    if(isset($_SESSION['ss_customer'])){
        $arr_user = unserialize($_SESSION['ss_customer']);
        $v_check_login = (string)$arr_user['_id'];
    }
    $arr_item = array();
    $v_check_no_existed = '';
    $v_check_existed = 'display:none';
    $arr_love_item = isset($_SESSION['ss_love_item']) ? unserialize($_SESSION['ss_love_item']) : array();
    if(!empty($arr_love_item)){
        $v_check_no_existed = 'display:none';
        $v_check_existed = '';
        for($i=0;$i<count($arr_love_item);$i++){
            $v_check_product = $cls_tb_product->select_one(array("_id"=>new MongoId($arr_love_item[$i]['product_id'])));
            if($v_check_product){
                $v_product_image = URL.$cls_tb_product->get_products_upload();
                if(!checkRemoteFile($v_product_image)) $v_product_image = JT_URL.$cls_tb_product->get_products_upload();
                $dsp_love_item = new Template("dsp_love_item.tpl",$v_dir_templates);
                $dsp_love_item->set('PRODUCT_SLUGGER',$cls_tb_product->get_slugger());
                $dsp_love_item->set('PRODUCT_IMAGE',$v_product_image);
                $dsp_love_item->set('FULL_PRODUCT_LINK',URL.$cls_tb_product->get_slugger());
                $dsp_love_item->set('FULL_PRODUCT_DESCRIPTION',$v_product_image);
                $dsp_love_item->set('PRODUCT_NAME',$cls_tb_product->get_sku());
                $dsp_love_item->set('PRODUCT_CODE',$cls_tb_product->get_code());
                $dsp_love_item->set('PRODUCT_PRICE',format_currency($cls_tb_product->get_sell_price()));
                $dsp_love_item->set('PRODUCT_PRICE_DISCOUNT',format_currency($cls_tb_product->get_product_sell_discount()));
                $dsp_love_item->set('BRAND_NAME',$cls_tb_banner->select_scalar("banner_name",array("_id"=> new MongoId($cls_tb_product->get_brand_id()))));
                $dsp_love_item->set('PRODUCT_ID',(string)$cls_tb_product->get_mongo_id());
                $arr_item [] = $dsp_love_item;
            }
        }
    }
    $v_dsp_love_item = !empty($arr_item) ? Template::merge($arr_item) : '';
    $dsp_static_content->set('URL_TEMP',$v_dir_templates);
    $dsp_static_content->set('NO_EXISTED',$v_check_no_existed);
    $dsp_static_content->set('EXISTED',$v_check_existed);
    $dsp_static_content->set('LOVED_ITEM_LIST',$v_dsp_love_item);
    $dsp_static_content->set('URL',URL);
    $dsp_static_content->set('USER_ID',$v_check_login);
    $dsp_static_content->set('IS_LOGIN',$v_check_login);
	echo $dsp_static_content->output();
?>