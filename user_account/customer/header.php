<?php if(!isset($v_sval)) die();
//$pathCache = open_cache(PRODUCT_IMAGE_DIR.'cache-php/','home-header');
$arr_language_switch = unserialize($_SESSION['ss_language_value']);
$x_header = new Template('header.tpl',$v_dir_templates);
add_class("cls_tb_nail_static_content");
$cls_tb_nail_static_content = new cls_tb_nail_static_content($db);
add_class("cls_tb_product");
$cls_tb_product = new cls_tb_product($db);
$v_shipping_content = $cls_tb_nail_static_content->select_scalar("content_content",array("content_key"=>"header_free_shipping_".$v_language));
// $v_header_login = '<li class="sign-in">
// 					<a id="sign_in" href="javascript:void(0)" class="js-sign-in" data-ajaxlink="" >sign in</a>
// 					<span>or</span>
// 					<a id="register" class="register" href="javascript:void(0)" data-ajaxlink="" >register</a>
// 				</li>
// 				<li>
// 					<a href="#">ACCOUNT</a>
// 				</li>';

$v_header_login='<span class="span-control"><a id="sign_in" href="javascript:void(0)" class="js-sign-in" data-ajaxlink="" >Sign in</a></span>
                          <span class="span-control"><a id="register" class="register" href="javascript:void(0)" data-ajaxlink="" >Register</a></span>';
if(isset($_SESSION['ss_customer'])){
    $arr_customer = unserialize($_SESSION['ss_customer']);
    $v_last_name = $arr_customer['last_name'];
    $v_header_login = '
    <span class="span-control">
        <a href="/profile/my-information">
            <span class="account-name">
                <span class="first-name OneLinkNoTx">'.$v_last_name.'</span>
                ’s
                </span>
            ACCOUNT
        </a>
    </span>
    <span class="span-control">
        <a class="sign-out" href="javascript::void(0)">sign out</a>
    </span>
    ';
}
add_class("cls_tb_nail_category");
$v_cat_by_character = '';
$v_dsp_cat_character_item = '';
$cls_tb_nail_category = new cls_tb_nail_category($db);
$v_run = 0;
$arr_temp = array();
foreach($arr_character as $key =>$val){
    $v_run ++;
    $v_count = $cls_tb_nail_category->count(array('$or'=>array(array('name'=>new MongoRegex("/^".strtolower($val)."/i")),array('name'=>new MongoRegex("/^".strtoupper($val)."/i"))),'status'=>1));
    if($v_count){
        $v_count_item = $cls_tb_nail_category->count(array('$or'=>array(array('name'=>new MongoRegex("/^".strtolower($val)."/i")),array('name'=>new MongoRegex("/^".strtoupper($val)."/i"))),'status'=>1));
        $arr_category_parent_list = $cls_tb_nail_category->select(array('$or'=>array(array('name'=>new MongoRegex("/^".strtolower($val)."/i")),array('name'=>new MongoRegex("/^".strtoupper($val)."/i"))),'status'=>1));
        if($v_count_item){
            $v_dsp_cat_character_item = '';
            if($val=='a'){
                $v_cat_by_character .="<li class='active'> <a href='#brands-".$val."'> ".strtoupper($val)."</a> </li>";
                $v_dsp_cat_character_item .='<div id="brands-'.$val.'" class="" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">';
            }
            else{
                $v_cat_by_character .="<li class=''> <a href='#brands-".$val."'> ".strtoupper($val)."</a> </li>";
                $v_dsp_cat_character_item .='<div id="brands-'.$val.'" class="hide" style=" font-family: fontsDosis; font-weight: 400; font-style: normal;">';
            }
            $v_j_count = 1;
            foreach($arr_category_parent_list as $arr){
                if($v_j_count%15==0 || $v_j_count==1 ){
                    $v_dsp_cat_character_item .='<div class="col"><ul class="nav">';
                }
                $v_dsp_cat_character_item .='
                            <li>
                                <a href="/'.$arr['slugger'].'/">'.$arr['name'].'</a>
                            </li>';
                if((($v_j_count%15==14 || $v_j_count == $v_count_item) && $v_j_count!=1) || $v_count_item==1  ){
                    $v_dsp_cat_character_item .='</ul></div>';
                }
                $v_j_count++;
            }
            $v_dsp_cat_character_item .='</div>';
            $arr_temp [$val] = $v_dsp_cat_character_item;
        }else{
            $arr_temp [$val] ='<div class="col"><ul class="nav"></ul></div>';
        }
    }else{
        $v_cat_by_character .="<li class='disabled'> <a href='#brands-".$val."'> ".strtoupper($val)."</a> </li>";
    }
}
foreach($arr_temp as $key =>$val){
    $temp = substr($val,strlen($val)-6);
    $v_dsp_cat_character_item .= $val;
}
$arr_category_parent_list = $cls_tb_nail_category->select(array("is_parent"=>1,"is_group"=>0,'show_on_menu'=>1),array("name"=>1,"order_no"=>1));
$arr_category_list = array();
$v_category_list = '';
$v_index = 1;
foreach($arr_category_parent_list as $arr_parent){
    $arr_language = isset($arr_parent['name_language'])?$arr_parent['name_language']:array();
    $v_parent_name = isset($arr_language[$v_language]) ? $arr_language[$v_language] : $arr_parent['name'];
    $v_parent_slugger = $arr_parent['slugger'];
    $v_parent_id = (string)$arr_parent['_id'];
    $v_parent_image = isset($arr_parent['image_link'])? $arr_parent['image_link'] : '';
    $v_image_path = "resources/category/".$v_parent_id."/".$v_parent_image;
    $v_image_link = '';
    if(file_exists($v_image_path) && $v_parent_image!='') $v_image_link = '<img src="'.URL.$v_image_path.'" width="154" height="120"></a>';
    $v_category_left_item = '';
    $v_category_right_item = '';
    $arr_category_left = array();
    $dsp_category = new Template('dsp_category_item.tpl',$v_dir_templates);
    $dsp_category->set('INDEX',$v_index++);
    $v_headline_index = 1;
    $arr_header_line_column1 = array();
    $arr_header_line_column2 = array();
    $arr_header_line_column3 = array();
    $arr_header_line_column4 = array();
    $dsp_category_left_item = new Template('dsp_category_left_item.tpl',$v_dir_templates);
    $arr_column_item = array();
    for($i=0;$i<count($arr_parent['child_item']);$i++){
        $v_header_id = $arr_parent['child_item'][$i];
        $v_select_one = $cls_tb_nail_category->select_one(array("_id"=>new MongoId($v_header_id),"status"=>1));
        if(!$v_select_one) continue;
        $v_headline_name = '';
        $arr_language = $cls_tb_nail_category->get_language_val();
        $v_headline_name = isset($arr_language[$v_language]) ? $arr_language[$v_language] : $cls_tb_nail_category->get_name();
        $v_headline_slugger = $cls_tb_nail_category->get_slugger();
        $arr_headline_child_item = array();
        // child item
        $cls_tb_nail_category_item = new cls_tb_nail_category($db);
        $arr_child_item = $cls_tb_nail_category_item->select(array("parent_id"=> $v_header_id,"status"=>1));
        foreach($arr_child_item as $arr_child_temp){
            $dsp_category_left_child_item = new Template('dsp_category_left_child_item.tpl',$v_dir_templates);
            $arr_language = isset($arr_child_temp['name_language'])?$arr_child_temp['name_language']:array();
            $v_name = isset($arr_language[$v_language]) ? $arr_language[$v_language] : $arr_child_temp['name'];
            $v_name = ucwords($v_name);
            $v_child_slugger = isset($arr_child_temp['slugger']) ? $arr_child_temp['slugger'] : '';
            $dsp_category_left_child_item->set('CHILD_NAME',$v_name);
            $dsp_category_left_child_item->set('CHILD_SLUGGER',$v_child_slugger);
            $arr_headline_child_item [] = $dsp_category_left_child_item;
        }
        // end child item
        if($v_headline_index>4) $v_headline_index = 1;
        $dsp_category_headline_item = new Template('dsp_colum_item.tpl',$v_dir_templates);
        $dsp_headline_child = count($arr_headline_child_item)>0 ? Template::merge($arr_headline_child_item) : '';
        //====
        $v_count = $cls_tb_product->count(array("category_id"=>(string)$v_header_id));
        /* v2 kiem tra xem category co product hay ko neu co thi` then linl*/
        $v_headline_name = ucwords($v_headline_name);
        if(!$v_count) $v_headline_name .='<span class="arrow arrow-right"></span>';
        else $v_headline_name = '<a href="/'.$v_headline_slugger.'/">'.$v_headline_name.'</a>';
        //====
        $dsp_category_headline_item->set('HEADLINE_NAME',$v_headline_name);
        $dsp_category_headline_item->set('CHILD_ITEM',$dsp_headline_child);
        $arr_column_item [] = $dsp_category_headline_item;
        if($v_headline_index==1) $arr_header_line_column1 [] = $dsp_category_headline_item;
        if($v_headline_index==2) $arr_header_line_column2 [] = $dsp_category_headline_item;
        if($v_headline_index==3) $arr_header_line_column3 [] = $dsp_category_headline_item;
        if($v_headline_index==4) $arr_header_line_column4 [] = $dsp_category_headline_item;
        $v_headline_index++;
//        $dsp_category_left_item
    }
    $v_dsp_column_data1 = is_array($arr_header_line_column1) ? Template::merge($arr_header_line_column1) : '';
    $v_dsp_column_data2 = is_array($arr_header_line_column2) ? Template::merge($arr_header_line_column2) : '';
    $v_dsp_column_data3 = is_array($arr_header_line_column3) ? Template::merge($arr_header_line_column3) : '';
    $v_dsp_column_data4 = is_array($arr_header_line_column4) ? Template::merge($arr_header_line_column4) : '';
    $dsp_category_left_item->set('LIST_COLUMN1',$v_dsp_column_data1);
    $dsp_category_left_item->set('LIST_COLUMN2',$v_dsp_column_data2);
    $dsp_category_left_item->set('LIST_COLUMN3',$v_dsp_column_data3);
    $dsp_category_left_item->set('LIST_COLUMN4',$v_dsp_column_data4);
    $arr_category_left [] = $dsp_category_left_item;
    $v_category_left_item = is_array($arr_category_left) ? Template::merge($arr_category_left) : '';
    $v_mtui = '';
    if(count($arr_parent['child_item'])) $v_mtui = '<span class="arrow arrow-right"></span>';
    $dsp_category->set('PARENT_NAME',$v_parent_name);
    $dsp_category->set('ARROW',$v_mtui);
    $dsp_category->set('LEFT_ITEM',$v_category_left_item);
    $dsp_category->set('PARENT_SLUGGER',$v_parent_slugger);
    $arr_category_list [] = $dsp_category;
}
$v_basket_count = isset($_SESSION['ss_basket_item']) ? count($_SESSION['ss_basket_item']) : 0 ;
$v_category_list = is_array($arr_category_list) ? Template::merge($arr_category_list) : '';
// =============================================== end category =========================================
$v_website_title = $cls_settings->get_option_name_by_key("website_attribute","webbsite_main_title");
$v_shipping_text = $cls_settings->get_option_name_by_key("shipping_price_limit","free_shipping_price");
$x_header->set('HEADER_TITLE',$v_website_title);
$x_header->set('CATEGORY_LIST',$v_category_list);
$x_header->set('HEADER_FREE_SHIPPING',$v_shipping_content);
$x_header->set('URL_CUSTOMER',$v_dir_templates);
$x_header->set('URL',URL);
$x_header->set('HEADER_LOGIN',$v_header_login);
$arr_ss_language = array();
if(isset($_SESSION['ss_language_value'])) $arr_ss_language = unserialize($_SESSION['ss_language_value']);
$v_search_placeholder = isset($arr_ss_language[$v_language]['SEARCH_FORM']) ? $arr_ss_language[$v_language]['SEARCH_FORM'] : '';
$x_header->set('SEARCH_PLACEHOLDER',$v_search_placeholder);
$x_header->set('CAT_START_BY_CHARACTER',$v_cat_by_character);
$x_header->set('CAT_START_BY_CHARACTER_ITEM',$v_dsp_cat_character_item);
$x_header->set('BASKET_COUNT',$v_basket_count);
$x_header->set('FREE_SHIPPING_TEXT',$v_shipping_text);
$x_header->set('IMAGE_LOGO',URL."images/logo.png");
$x_header->set('IMAGE_NAME',URL."images/name.png");
$x_header->set('LANGUAGE_STORES',strtoupper($arr_language_switch[$v_language]['STORES']));
$x_header->set('LANGUAGE_BASKET',strtoupper($arr_language_switch[$v_language]['HEADER_BASKET']));
echo $x_header->output();
//create_cache($pathCache);
?>