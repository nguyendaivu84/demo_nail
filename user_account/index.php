<?php if(!isset($v_sval)) die();
$v_act = isset($_REQUEST['a'])?$_REQUEST['a']:'';
if(isset($arr_user) && $arr_user['user_type'] <3 && $arr_user['user_name']!='') $v_act = 'ADMIN-ACT';
if(isset($_SESSION['ss_language'])){
    $v_language = $_SESSION['ss_language'];
}else{
    $v_language = $_SESSION['ss_language'] = 'EN';
}
$v_url_template = "user_account/customer/templates/default/";
translate_language($db,$v_language);
switch($v_act){
    case 'ADMIN-ACT':
    case 'LO':
        if(!isset($arr_user) || $arr_user['user_type'] >=3 || $arr_user['user_name']=='' ){
            include 'user_login/index.php';
        }else {
            $v_head  = 'user_account/admin/';
            $v_dir  = 'user_account/admin/';
            include 'user_account/admin/index.php';
        }
        break;
    default :
        $v_head  = 'user_account/customer/';
        $v_dir  = 'user_account/customer/';
        include 'user_account/customer/index.php';
}
?>