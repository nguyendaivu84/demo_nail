<?php
App::uses('AppController', 'Controller');
class SettingsController extends AppController {

	public $helpers = array();

	// public $helpers = array('Cache');
	// public $cacheAction = "1 hour";

	public function beforeFilter( ){
		// goi den before filter cha
		// parent::beforeFilter();
	}

	public function index(){
		$this->redirect("/settings/stock_manager");
	}

	public function stock_manager(){
		$cond = array();
		if(!empty($_POST)){
			$cond = array();
		}
		$this->selectModel('Product');
		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'arr_field' => array('name','products_upload','qty_in_stock','in_stock'),
			'limit'     => 20
		));
		$data = iterator_to_array($products);

		$this->set('products', $products);

		$this->selectModel('Category');
		$this->Category->has_field_deleted = false;
		$arr_tmp = $this->Category->select_all(array(
			'arr_where' => array('status' => 1),
			'arr_order' => array('name' => 1)
		));

		$product_category = array();
		foreach ($arr_tmp as $key => $value) {
			$product_category[(string)$value['_id']] = $value['name'];
		}
		$this->set('product_category', $product_category);

		$this->selectModel('Location');
		$location = $this->Location->select_all(array('arr_where' => array('deleted' => false, 'stockuse' => 1), 'arr_field' => array('name')));
		$this->set('location', $location);

		$arr_category_level_1 = $this->category_child();
		$this->set('arr_category_level_1',$arr_category_level_1);
	}

	function choose_unit($product_id){
		$this->selectModel('Unit');
		$arr_unit = $this->Unit->select_one(array('product_id' => new MongoID($product_id),'type' => 'group'),array('barcode_no','type','quantity'));
		echo json_encode($arr_unit);
		die;
	}

	public function category_child($parent_id='',$function=0){  // tim cha cả lớn nhất
        if(isset($_POST['parent_id']))
            $parent_id = $_POST['parent_id'];

        if($parent_id=='')
            $arr_where = array('is_parent'=>1);
        else
            $arr_where = array('parent_id'=>$parent_id);

        $this->selectModel('Category');
        $this->Category->has_field_deleted = false;
        $arr_category = $this->Category->select_all(array(
                                                    'arr_where' => $arr_where,
                                                    'arr_field' => array('name'),
                                                    ));
        $data = iterator_to_array($arr_category);
        $menu = array();
        foreach($data as $key => $value){
            $menu[$key] = $value['name'];
            //echo $menu[$key] ;
        }
        if($function==1){
        	 return $menu;
        }
        if($this->request->is('ajax')){
        	echo json_encode($menu);
        	die;
        }
        if(isset($_POST['parent_id'])){
            echo json_encode($menu);
            die;
        }else
            return $menu;
	}

	function get_category_product($category_id){
		$category_id_list = array();
		$category_child_2 = $this->category_child($category_id,1);
			if(count($category_child_2)>0){
				foreach($category_child_2 as $keys=>$values){
					$category_child_3 = $this->category_child($keys,1);
					if(count($category_child_3)>0){
							foreach($category_child_3 as $keys3=>$values3)
								$category_id_list[] = $keys3;
					}else{
						$category_id_list[] = $keys;
					}

				}
			}else
				$category_id_list[] = $category_id;
		return $category_id_list;

	}


	function settings_get_product_from_category(){
		$category_id = $_POST['category_id'];
		$product_name = $_POST['product_name'];

		$this->selectModel('Product');
		$cond = $category_id_list = array();
		if($category_id != 'all' && $category_id != ''){
			$category_id_list = $this->get_category_product($category_id);
			$cond = array('category_id' => array('$in' => $category_id_list));
		}
		if($product_name!='')
			$cond['name'] = new MongoRegex('/'.$product_name.'/i');

		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => 20
		));
		$this->set('products', $products);
		$this->render('settings_get_product_from_category');
	}



	 function import_barcode(){
		if(isset($_POST['barcode'])){
			$this->selectModel('Unit');
/*			$unit = $this->Unit->select_one(array('product_id'=> new MongoID($_POST['products_id']) ) , array('_id'));
			if(isset($unit['_id'])){
				echo 'Unit is existed!';
				die;
			}*/
			$unit_qty = count($_POST['barcode']);
			$total_qty = 0;
			for($i = 0; $i < $unit_qty; $i++){
				$save['barcode_no'] = $_POST['barcode'][$i];
				$save['quantity'] = $_POST['qty'][$i];
				$save['type'] = $_POST['type'][$i];
				$save['code'] = $this->Unit->get_auto_code('code');
				$save['product_id'] = new MongoID($_POST['products_id']);
				$save['product_name'] = $_POST['products_name'];
				$save['created_by'] = "";
				$save['modified_by'] = "";
				$location_id = explode('_', $_POST['location_id']);
				$save['current_location_id'] = new MongoId($location_id['0']);
				$save['current_location_name'] = $location_id['1'];
				$this->Unit->save($save);
				$total_qty = $total_qty + (int)$save['quantity'];
			}
			$this->selectModel('Product');
			$product = $this->Product->select_one(array('_id' => new MongoID($_POST['products_id'])));
			$is_have = 10000;
			$product['qty_in_stock'] += $total_qty;
			if(!empty($product['locations'])){
				foreach($products['locations'] as $key_location => $value_location){
					if($value_location['location_id'] == new MongoId($location_id[0]) && $value_location['deleted'] == false){
						$is_have = $key_location;
					}
				}
			}
			if($is_have != 10000){
				if(!isset($product['locations'][$is_have]['min_stock']))
					$product['locations'][$is_have]['min_stock'] = 0;
				$product['locations'][$is_have]['total_stock'] += $total_qty;
				$product['locations'][$is_have]['min_stock'] += $total_qty;
			}
			else{
				$new_location = array();
				$new_location['location_id'] = new MongoId($location_id['0']);
				$new_location['location_name'] = $location_id['1'];
				$new_location['deleted'] = false;
				$new_location['total_stock'] = $total_qty;
				$new_location['min_stock'] = $total_qty;
				$new_location['location_type'] = '';
				$new_location['stock_usage'] = '';
				$new_location['avalible'] = '';
				$product['locations'][] = $new_location;
			}

			$this->Product->save($product);

			echo 'ok';
		}
		die;
	}

	public function get_product(){
		$category_id = $_POST['category_id'];
		$page = $_POST['page'];
		$product_name = $_POST['product_name'];

		$this->selectModel('Product');
		$cond = $category_id_list = array();
		if($category_id != 'all' && $category_id != ''){
			$category_id_list = $this->get_category_product($category_id);
			$cond = array('category_id' => array('$in' => $category_id_list));
		}
		$limit = 20;
		$skip = $limit * ($page - 1);
		if($product_name!='')
			$cond['name'] = new MongoRegex('/'.$product_name.'/i');

		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => $limit,
			'skip' => $skip
		));
		$html = '';
		if($products->count()){
			foreach($products as $key=>$value){
				$html .= '<li>
				 			<label id="qty_in_stock" style="color: red"> '. $value['qty_in_stock'].'</label>
						  <a href="javascript:void(0)">
							<div class="img_products">
							   <img src="/jt/app/webroot/'.str_replace('\\', '/', ( isset($value['products_upload']) ? $value['products_upload'] : '')) .'" alt="POS" style="max-width:90px">
							</div>
							 <div class="check_span">
					        	<input type="checkbox">
					        </div>
					        <span class="product_name">'. $value['name'].'</span>
					        <input class="product_id" type="hidden" value="'. $key .'" />
						  </a>
						</li>';
			}
		}
		echo $html;
		die;
	}

	function check_barcode(){
		if(isset($_POST['barcode_no'])){
			$this->selectModel('Unit');
			$arr_unit = $this->Unit->select_one(array('barcode_no' => (string)$_POST['barcode_no']), array('barcode_no'));
			echo json_encode($arr_unit);
			die;
		}
	}

	function system(){

	}

	function get_product_barcode(){
		if(isset($_POST['barcode'])){
			$this->selectModel('Unit');
			$this->selectModel('Product');
			$cond = array('type' => 'group');
			if(strlen($_POST['barcode']) > 0 ){
				$cond = array('barcode_no' => trim($_POST['barcode']));
				$unit = $this->Unit->select_one($cond,array('barcode_no','barcode_type','quantity','product_id'));
				$product = array();
				if(isset($unit['product_id']) && $unit['product_id'] != "")
					$product = $this->Product->select_one(array('_id' => $unit['product_id']),array('name','products_upload','qty_in_stock'));
				if(count($product) > 0){
					$product['_id'] = (string)$product['_id'];
					unset($unit['_id']);
					$arr_merge = array_merge($product, $unit);
					echo  json_encode($arr_merge);
				}
				else
					echo 'Null';
			}
		}
		die;
	}


}