<?php
App::uses('AppController', 'Controller');
class HomesController extends AppController {

	public $helpers = array();

	// public $helpers = array('Cache');
	// public $cacheAction = "1 hour";

	public function beforeFilter( ){
		// goi den before filter cha
		 parent::beforeFilter();
	}

	public function convert(){
		$this->selectModel('Product');
		$this->Product->has_field_deleted = false;
		$products = $this->Product->select_all();
		foreach ($products as $key => $value) {
			$value['deleted'] = false;
			$value['name'] = $value['description'];
			if (!$this->Product->save($value)){
				echo 'error';
			}
		}
		echo '<br>xong';
		die;
	}

	public function get_product($category_id,$page){
		$this->selectModel('Product');
		$cond = array();
		if($category_id != 'all' && $category_id != '')
			$cond = array('category_id' => $category_id);
		$limit = 20;
		$skip = $limit * ($page - 1);
		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => $limit,
			'skip' => $skip
		));
		$html = '';
		if($products->count()){
			foreach($products as $key=>$value){
				$html .= '<li onclick="show_number(\''.$key.'\');">
						  <a href="javascript:void(0)">
							<div class="img_products">
							   <img src="/jt/app/webroot/'.str_replace('\\', '/', ( isset($value['products_upload']) ? $value['products_upload'] : '')) .'" alt="POS" style="max-width:90px">
							</div>
							<div class="check_span"><input type="checkbox"></div>
							'.$value['name'].'
						  </a>
						</li>';
			}
		}
		echo $html;
		die;
	}

	public function index(){
		$cond = array();
		if(!empty($_POST)){
			$cond = array();
		}
		$this->selectModel('Product');
		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => 20
		));
		$this->set('products', $products);

		// lấy cái order last sau cùng ra
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('status'=> 'new'), array('_id', 'name', 'products'), array('_id' => -1));
		$this->set('order', $order);

		$this->selectModel('Category');
		$this->Category->has_field_deleted = false;
		$arr_tmp = $this->Category->select_all(array(
			'arr_where' => array('status' => 1),
			'arr_order' => array('name' => 1)
		));
		$product_category = array();
		foreach ($arr_tmp as $key => $value) {
			$product_category[(string)$value['_id']] = $value['name'];
		}
		$this->set('product_category', $product_category);


		//English
		$arr_language =  array(
		                  'new_order'=>'New Order',
		                  'no'=>'No.',
		                  'language' => 'Language',
		                  'products' => 'Products',
		                  'qty' => 'Quantity',
		                  'price' => 'Price',
		                  'Total' => 'Total',
		                  'Total_Price' => 'Total Price:',
		                  'Tax_Breakdown' => 'Tax Breakdown',
		                  'setup_price' => 'Setting',
		                  'view_list' => 'View List',
		                  'print' => 'Print',
		                  'check_out' => 'Check Out',
		                  'close' => 'Close',
		                  'cash' => 'Cash',
		                  'cashier' => 'Cashier',
		                  'code_staff' => 'Code Staff',
		                		);
		//Tieng viet
		if($this->Session->check('language') && $this->Session->read('language') == 'vn')
			$arr_language =  array(
		                  'new_order'=>'Tạo đơn hàng',
		                  'no'=>'STT',
		                  'language' => 'Ngôn ngữ',
		                  'products' => 'Tên sản phẩm',
		                  'qty' => 'Số lượng',
		                  'price' => 'Giá',
		                  'Total' => 'Tổng',
		                  'Total_Price' => 'Tổng tiền:',
		                  'Tax_Breakdown' => 'Thuế',
		                  'setup_price' => 'Thiết lập',
		                  'view_list' => 'Xem danh sách',
		                  'print' => 'In',
		                  'check_out' => 'Thanh toán',
		                  'search_barcode' => 'Tìm kiếm',
		                  'close' => 'Đóng',
		                  'cash' => 'Tiền mặt',
		                  'cashier' => 'Quầy thu ngân',
		                  'code_staff' => 'Mã nhân viên',
		                		);
		$this->set('arr_language',$arr_language);

	}

	function change_language(){
		if(isset($_POST['language'])){
			if($_POST['language'] == 'en')
				$this->Session->write('language','en');
			else
				$this->Session->write('language','vn');
			echo 'ok';
		}
		die;
	}

	function get_product_from_category($category_id){
		$this->selectModel('Product');
		$cond = array();
		if($category_id != 'all' && $category_id != '')
			$cond = array('category_id' => $category_id);
		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => 20
		));
		$this->set('products', $products);
		$this->render('get_product_from_category');
	}
/*
	function get_product_name($get_product_name=''){
		$this->selectModel('Product');
		$cond = array();
		if( strlen($get_product_name) > 0 ){
			$cond = array('barcode' => trim($get_product_name));
			$products = $this->Product->select_one($cond);
			if(count($products) > 0){
				$this->set('products', $products);
			}else

		$this->render('get_product_from_category');
		}else{
			echo '123';die;
		}
	}*/

	function get_product_name($get_product_name=''){
		$this->selectModel('Unit');
		$cond = array();
		if( strlen($get_product_name) > 0 ){
			$cond = array('barcode_no' => trim($get_product_name));
			$unit = $this->Unit->select_one($cond);

		$this->selectModel('Product');
		if(isset($unit['product_id'])){
			$product = $this->Product->select_all(array('arr_where' => array('_id' => new MongoId($unit['product_id']))));
		}
		else{
			echo 'not found product!';die;
		}
		$this->set('products', $product);
		$this->render('get_product_from_category');
		}else{
			echo '123';die;
		}
	}

	function update_product_order(){
		$product_id = $_GET['product_id'];
		$field      = $_GET['field'];
		$order_id   = $_GET['order_id'];
		$new_value  = $_GET['new_value'];

		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));

		$checkkey = -1;
		foreach ($order['products'] as $key => $value) {
			if((string)$value['product_id'] == (string)$product_id){
				$checkkey = $key;
				break;
			}
		}

		if($checkkey > -1){
			$order['products'][$checkkey][$field] = $new_value;
			$this->selectModel('Order');
			if($this->Order->save($order))
				echo 'ok';
			else
				echo 'error'.$this->Order->arr_errors_save[1];
		}
		die;
	}

	function reload_product($order_id){
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->set('order', $order);
		$this->render('add_product');
	}

	function add_product($product_id, $qty, $order_id){
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->selectModel('Product');
		$product = $this->Product->select_one(array('_id' => new MongoId($product_id)));
		if(isset($order['_id']) && isset($product['_id'])){
			$new = true;
			foreach ($order['products'] as $key => $value) {
				if((string)$value['product_id'] == (string)$product_id){
					$order['products'][$key]['qty'] += $qty;
					$new = false;
				}
			}
			$order['status']= 'new';//An, dung cho check out.
			if($new)
				$order['products'][] = array(
					'product_id' => new MongoId($product_id),
					'name'       => $product['name'],
					'price'      => (isset($product['sell_price'])?$product['sell_price']:0),
					'qty'        => $qty,
					'deleted'    => false
				);
			if(!$this->Order->save($order)){
				echo 'Error in save new product to order, please contact developer. Thanks!'; die;
			}
			$this->set('order', $order);
		}else{
			echo 'Order or product does not exist!'; die;
		}
	}

	function edit_order($order_name, $order_id = ''){
		if(!$this->request->is('ajax'))die('Request is not valid.');
		if($order_name != ''){
			$save['name'] = $order_name;

			if($order_id != '')
				$save['_id'] = $order_id;
			else
				$save['products'] = array();

			$this->selectModel('Order');
			if($this->Order->save($save)){
				if($order_id != '')
					echo 'ok';
				else
					echo $this->Order->mongo_id_after_save;
			}else{
				echo 'error'.$this->Order->arr_errors_save[1]; die;
			}
		}
		die;
	}

	function view_list(){
		$this->selectModel('Order');
		$list = $this->Order->select_all(array('arr_where'=>array('status'=>'new')));
		$this->set('list',$list);
		$check_out = $this->Order->select_all(array('arr_where'=> array('status'=>'complete')));
		$this->set('check_out',$check_out);
	}

	function view_list_detail($order_id){
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->set('list',$order);
	}

	function check_out($order_id){
		$this->selectModel('Order');
		$check_out = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->set('check_out_data',$check_out);

		$arr_save['status'] = 'complete';
		$arr_save['_id'] = new MongoId($order_id);
		$this->Order->save($arr_save);
	}

	function settings(){

	}

}