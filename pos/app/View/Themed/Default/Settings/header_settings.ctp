<script type="text/javascript" src="<?php echo URL ?>/theme/default/js/settings/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/theme/default/js/settings/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/theme/default/js/settings/jquery.idTabs.min.js"></script>

<script type="text/javascript">
    $(window).load(function() {
        $('.container_same_category').mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            advanced:{
	         autoScrollOnFocus: false,
	     	}
        });
    });
</script>
<style>
	.k-window-titlebar.k-header {
	    height: 32px;
	}
	.jt_confirms_window_ok:hover, .jt_confirms_window_cancel:hover{
		text-decoration: none;
	}
</style>
<div class="wrapper">
		<div class="content">
			<div class="content_left">
                <ul class="list_products big_size settings">
					<li class="active"><a href="<?php echo URL;?>/settings/stock_manager">Stock Manager</a></li>
					<li><a href="<?php echo URL;?>/settings/system">System</a></li>
					<li><a href="<?php echo URL;?>"><< Go Back</a></li>
				</ul>
            </div>