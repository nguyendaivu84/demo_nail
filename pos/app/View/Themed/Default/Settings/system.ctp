<?php echo $this->element('../Settings/header_settings');?>
 <div class="content_right bg_right">
    <ul class="idTabs clearfix">
        <li><a class="selected" href="#language">Language</a></li>
        <label id="warming_id" style="color:red;margin-top: 1.5%;margin-left: 51%;font-size: 1.5em;text-transform:uppercase "></label>
    </ul>
<!--     <div class="content_idt container_same_category">
        <div id="language" style="color:red; background: red">
			<ul>
				<li class="language" >
				    <a href="javascript:void(0)" title="">language
				    <i class="fa fa-caret-down icon_indent"></i>
				    </a>
				    <ul class="dropdown_list">
				        <li><a id="vn" href="javascript:void(0)" value="Vietnamese">Vietnamese</a></li>
				        <li><a id="en" href="javascript:void(0)" value="English">English</a></li>
				    </ul>
				</li>
			</ul>
        </div>
    </div> -->
    <label style="color:#0E9CDC">Choose language:</label>
    <select id="language" class="language_change">
		<option id="en" href="javascript:void(0)" value="English">English</option>
		<option id="vn" href="javascript:void(0)" value="Vietnamese">Vietnamese</option>
	</select>
</div>

<script type="text/javascript">
$(function(){
	$("#language").kendoComboBox();

	$(".language_change").change(function(){
	    $.ajax({
	        url : "<?php echo URL.'/homes/change_language' ?>",
	        type: "POST",
	        data: {language:$(this).attr("id")},
	        success: function(result){
	            if(result=="ok")
	            	console.log(result);
	        }

	    })
	})
});
</script>

 <?php echo $this->element('../Settings/footer_settings');?>