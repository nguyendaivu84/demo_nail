<ul class="list_products products_settings" id="get_products">
	<?php foreach($products as $key => $value){?>
    <li>
        <label id="qty_in_stock" style="color: red"> <?php echo $value['qty_in_stock']; ?></label>
      <a href="javascript:void(0)">
        <div class="img_products">
        	<img src="/jt/app/webroot/<?php echo str_replace('\\','/', isset($value['products_upload']))?$value['products_upload']:''; ?>"alt="POS">
        </div>
        <div class="check_span">
        	<input type="checkbox">
        </div>
        <span class="product_name"><?php echo isset($value['name'])?$value['name']:'';?></span>
        <input class="product_id" type="hidden" value="<?php echo $key; ?>" />
      </a>
    </li>
    <?php } ?>
 </ul>
<script type="text/javascript">
$(function(){
    $("#get_products").on("click","li",function(){
        var tmp = $("#qty_in_stock",this).html();
        $("#in_stock").html(tmp);
        $("li").removeClass('active2').addClass('active');
        $(this).removeClass('active').addClass('active2');
        var names = $(".product_name",this).html();
        var product_id = $(".product_id",this).val();
        var old_products_id = $("#products_id").val();
        var img = $(".img_products",this).html();
        var total_id = $("#input_total_id").val();
        $("#input_total_id").val(0);
        $("#input_barcode_name").focus();


        if(product_id == old_products_id) return false;

        /*popup OK/CANCEL*/
        if(old_products_id != "" && old_products_id != 'undefined' && parseInt(total_id) > 0){
            confirms("Message","Do you have save barcode ?"
                     ,function(){
                        i=j=0;
                        $("#products_id").val(product_id);
                        $("#input_barcode_name").val("");
                        $("#input_products_name").val(names);
                        $("#products_name_label").html(names);
                        $(".img_block").empty().append(img);
                        var data = $("input","#table_barcode").serialize();
                        data += "&products_name="+$("#input_products_name").val();
                        data += "&location_id="+$("#select_location").val();
                        $("#input_products_name").val("");
                        $("#htdocs").html("");
                        $("#products_id").val("");
                        $("#input_total_id").val('0');
                        if($("#htdocs") != ""){
                            alert('Fill barcode not blank!');
                             return false;
                        }
                        $("#radio_1").removeAttr("disabled");
                        $("#radio_2").removeAttr("disabled");
                        $("#input_barcode_name").prop('disabled',false);
                        $.ajax({
                            url: "<?php echo URL.'/settings/import_barcode'; ?>",
                            type: "POST",
                            data: data,
                            success: function(result){
                                if(result!="ok")
                                    alert(result);
                            }
                        })
                     },function(){
                        i=j=0;
                        $("#products_id").val(product_id);
                        $("#input_barcode_name").val("");
                        $("#input_products_name").val(names);
                        $("#products_name_label").html(names);
                        $(".img_block").empty().append(img);
                        $("#htdocs").html("");
                        $("#input_total_id").val('0');
                        $("#input_barcode_name").focus();
                        $("#radio_1").removeAttr("disabled");
                        $("#radio_2").removeAttr("disabled");
                        $("#input_barcode_name").prop('disabled',false);
                     }) ;
        }else{
             /*khi barcode là group và có trong db thì lấy ra*/
            $.ajax({
                url:"<?php echo URL;?>/settings/choose_unit/" + product_id,
                success: function (result){
                    if(result != "" && result != 'null'){
                        result = $.parseJSON(result);
                        $("#input_barcode_name").val(result.barcode_no);
                        $("#table_barcode").append( "<tr class=record id=group1><td>1</td><td><input type='text' name=barcode[] value='" + $("#input_barcode_name").val() + "' readonly style='color:red;width:100%; border-radius: 3px;'/></td><td> <input type='text' class='qty_group' name='qty[]' value='"+result.quantity+"' style='color:red;width:100%; border-radius: 3px;' /><input type='hidden' name=type[] value='group'/></td>" + "</tr>" );
                        var value_total = parseInt($("#input_total_id").val()) + 1;
                        $("#input_total_id").val(value_total);
                        $("#input_barcode_name").prop('disabled',true);
                        $("#radio_1").attr("disabled", true);
                    }
                }
            });
            $("#htdocs").html("");
            $("#products_id").val(product_id);
            $("#input_barcode_name").val("");
            $("#products_name_label").html(names);
            $("#input_products_name").val(names);
            $(".img_block").empty().append(img);
        }
    });
    $('#get_products','#product_list_data').scroll(function(){
        var current_height = $(this).scrollTop() + $(this).height();
        var height = $(document).height();

        if(current_height >= height) {
            load_more_product();
        }
    });
})
var page = 1;
function load_more_product(){
    page++;
    $.ajax({
        url: '<?php echo URL;?>/settings/get_product/',
        type: 'POST',
        data: {category_id:$("#choose_category").val(),page:page,product_name:$("#search_product_input").val()},
        success: function(html){
            if(html!="")
                $("ul#get_products").append(html);
        }
    });
}
</script>

