 <?php echo $this->element('../Settings/header_settings');?>
<div class="content_right bg_right">
    <ul class="idTabs clearfix">
        <li><a class="selected" href="#contacts">Import</a></li>
        <li><a href="#comms">Export</a></li>
        <label id="warming_id" style="color:red;margin-top: 1.5%;margin-left: 51%;font-size: 1.5em;text-transform:uppercase "></label>
    </ul>
    <div class="content_idt container_same_category">
        <div id="contacts">
            <div class="col-xs-8">
                 <div class="search">
			        <form class="forms-controls" accept-charset="utf-8"  action="" onsubmit='choose_category($("#choose_category").val(),$("#search_product_input").val());return false'>
			          	<input style="width: 96%" id="search_product_input" type="text" placeholder="Search products" class="form-control">
			         </form>

			        <div class="category_group">
			        	<input type="hidden" value="all" id="choose_category"  />
			        	<!--  cateogry level 1 -->
			       		<div class="category_level_1_container form-control">
					          <select onchange="choose_category_level($(this).val());">
				                 <option value="all">All</option>
				                <?php foreach($arr_category_level_1 as $id => $value)
				                	echo '<option value="'.$id.'">'.$value.'</option>';
				                ?>
				              </select>
			          	</div>
		                <!--  cateogry level 2 -->
		                <div class="category_level_2_container form-control">
		                		<select></select>
		              	</div>
		                <!--  cateogry level 3 -->
		               	<div class="category_level_3_container form-control">
		               			<select></select>
		          	   	</div>
		          	</div>
			      </div>
			      <div id="product_list_data">
			      	<?php echo $this->element('../Settings/settings_get_product_from_category'); ?>
			      </div>
            </div>
            <div class="col-xs-4">
            	<form action="javascript:void(0)" class="settings_f" id="barcode_form" method="post">
                	<div class="top_block">
                		<div class="img_block col-xs-4">
                			<img src="/jt/app/webroot/upload/2014_04/big_Berkeley_3mm_Folded_Eva_Foam_Foot_Slipper_Pack_of_12_pairs_A.jpg" alt="POS">
                		</div>
                		<div class="form-group col-xs-8" style="color: #555;">
                			<label id="products_name_label">Products name</label>
                			<label >In stock</label>
                			<label id="in_stock" style="color: red"></label>
							<label for="" class="localtion_lbl">Location</label>
							<select name="location_id"  id="select_location" class="form-control category_select">
				                <?php
				                foreach($location as $id => $value)
				                	echo '<option value="'.$id.'_'.$value['name'].'">'.$value['name'].'</option>';
				                ?>
				              </select>
						</div>
                	</div>

                	<div>
                		<div class="form-group">
							<input  type="hidden" name="products_name" id="input_products_name" class="form-control" readonly>
				  		</div>
						<div class="form-group form_barcode">
							<label for="">Barcode</label>
							<input type="text" name="barcode" id="input_barcode_name" class="form-control">
						</div>

						<div class="form-group" id="case_check"> <!-- &nbsp&nbspSimple &nbsp&nbsp&nbsp -->
					        <input type="radio" id="radio_1" name="case" value="simple"  > <label for="radio_1">Simple</label>&nbsp&nbsp&nbsp
					        <input type="radio" id="radio_2" name="case" value="group"  checked="checked"> <label for="radio_2">Group</label>
					    </div>
                	</div>
                	<table class="table table-bordered no-bottom">
				      <thead>
				        <tr>
				          <th style="width: 45px; text-align:center">No.</th>
				          <th>Barcode</th>
				          <th>Quantity</th>
				        </tr>
				      </thead>
				    </table>
                	<div id="scroll_div" class="product_more">
	                	<table class="table table-bordered tablehover" id="table_barcode">
	                		<input type="hidden" name="products_id" id="products_id" value="">
					      <tbody style="color: #555;" id="htdocs">

					      </tbody>
					    </table>
				    </div>
				    <div class="control_f">
			       		<div class="col-xs-6 form-edit">
			       			<label for="">Total:</label>
				      		<input type="text" style="color: #555; border:0px" id="input_total_id" name="input_total_name" value="" readonly="true"></input>
			       		</div>
			       		<div class="col-xs-6 right_col">
			       			<button type="button" class="btn btn-default" id="save_barccode">Save</button>
				      		<button type="button" class="btn btn-default" id="cacel_barcode">Cancel</button>
			       		</div>
				    </div>
				</form>
            </div>
        </div>

        <div id="comms">
        	<!-- Export -->
        </div>
    </div>
</div>

 <?php echo $this->element('../Settings/footer_settings');?>