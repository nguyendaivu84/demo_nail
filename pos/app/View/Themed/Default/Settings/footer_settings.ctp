</div>
		<div id="footer" style="clear: both;"></div>
	</div>

<span id="popupNotification"></span>
<script src="http://cdn.kendostatic.com/2014.1.416/js/kendo.all.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#scroll_div').slimScroll({
          alwaysVisible: true
      	});

		var i=0;
		var j=0;
		$("#input_total_id").val('0');

		$("#input_barcode_name").keypress(function(e){
			var v_barcode_name = $(this).val();
			if(e.which == 13 && $("#input_barcode_name").val() != '' && $("#input_products_name").val() != ""){
				var flag = 0;
				$(".barcode_no_class","#table_barcode").each(function(){
					if($(this).val() == v_barcode_name){
						flag = 1;
						message('barcode existed!');
						return false;
					}
				})
				var check = $("input[name=case]:checked", "#case_check").val();
				var n=$(".record").size()+1;
				if(check == 'simple' && flag == 0){
					$("#table_barcode").prepend("<tr class=record id=barcode" + n + ">" + "<td style='width: 45px; text-align:center;'>"+n+"</td>"  + "<td><input class='barcode_no_class' id='simple_id_"+n+"' type='text' name=barcode[] value='" + $("#input_barcode_name").val() + "' readonly style='width:100% ; border-radius: 3px;'/></td>" + "<td><input type='text' name='qty[]' value='1' readonly style='width:100% ; border-radius: 3px;' /><td onclick='delete_barcode_line("+n+");' class='delete_icon'>X</td><input type='hidden' name=type[] value='simple'></td>" + "</tr>" );
						var value_total = parseInt($("#input_total_id").val()) + 1;
						$("#input_total_id").val(value_total);
						$("#input_barcode_name").prop('disabled',false);
						$("#radio_2").attr("disabled", true);

				}else if(check == 'group' && flag == 0){
					$("#table_barcode").prepend( "<tr class=record id=barcode" + n + ">"+"<td style='width: 45px; text-align:center'>"+n+"</td>"+ "<td><input id='group_id_"+n+"' type='text' name=barcode[] value='" + $("#input_barcode_name").val() + "' readonly style='color:red;width:100%; border-radius: 3px;'/></td>" + "<td> <input type='text' class='qty_group' name='qty[]' value='1' style='color:red;width:100%; border-radius: 3px;' /><input type='hidden' name=type[] value='group'/></td>" + "</tr>" );
						$(".qty_group").change(function(){
							var value_total = parseInt($(".qty_group").val());
							$("#input_total_id").val(value_total);
						});

					$(".qty_group").focus();
					$("#input_barcode_name").prop('disabled',true);
					$("#radio_1").attr("disabled", true);
				}



				$("#input_barcode_name").val("");
				$("#input_barcode_name").focus();


				//check barcode
				if(v_barcode_name!='' && v_barcode_name!=undefined){
					$.ajax({
						url: "<?php echo URL;?>/settings/check_barcode",
						type: "POST",
						data:{barcode_no: v_barcode_name},
						success: function(result){
							if(result != "" && result != 'null'){
								message('barcode existed!');
								$("#barcode"+n).remove();
								$("#radio_1").removeAttr("disabled");
		                        $("#radio_2").removeAttr("disabled");
		                        $("#input_barcode_name").prop('disabled',false);
							}
						}
					});
				}



			}else if(e.which == 13 && $("#input_barcode_anme").val() != '' && $("#input_products_name").val() == ""){
				search_product_form_barcode($("#input_barcode_name").val());
			}
		});


		var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
		$("#save_barccode").click(function(){
			var data = $("input,select","#table_barcode").serialize();
			data += "&products_name="+$("#input_products_name").val();
			data += "&location_id="+$("#select_location").val();
			$("#input_products_name").val("");
			$("#htdocs").html("");
			$("#products_id").val("");
			$("#input_total_id").val("");
			$("#radio_1").removeAttr("disabled");
			$("#radio_2").removeAttr("disabled");
			$("#input_barcode_name").prop('disabled',false);
			$.ajax({
				url: "<?php echo URL.'/settings/import_barcode'; ?>",
				type: "POST",
				data: data,
				success: function(result){
					if(result=="ok"){
	                    popupNotification.show('Saved!', "success");
	                    choose_category($("#choose_category").val(),$("#search_product_input").val());
	                }
				}
			})
		});

		$("#cacel_barcode").click(function(){
			$("#input_products_name").val("");
			$("#htdocs").html("");
			$("#products_id").val("");
			$("#input_barcode_name").val("");
			$("#input_total_id").val("");
			$("#radio_1").removeAttr("disabled");
			$("#radio_2").removeAttr("disabled");
			$("#input_barcode_name").prop('disabled',false);
		});

		$("#select_location").change(function(){
			 $("#input_barcode_name").focus();
		});
		$("#radio_1").change(function(){
			$("#input_barcode_name").focus();
		});
		$("#radio_2").change(function(){
			$("#input_barcode_name").focus();
		});
	});

	function save_barcode(){
		$.ajax({
			url: "<?php echo URL.'/settings/import_barcode'; ?>",
			type: "POST",
			data: $("input","#table_barcode").serialize(),
			success: function(result){
				if(result!="ok")
					message(result);
			}
		})
	}
	function unit_save(){
		$.ajax({
			url: "<?php echo URL.'/settings/import_barcode'; ?>",
			type: "POST",
			data: $("input","#barcode_form").serialize(),
			success: function(result){
				if(result!="ok")
					message(result);
			}
		})
	}
   function choose_category(category_id,product_name){
        $("#choose_category").val(category_id);
        $.ajax({
            url: '<?php echo URL;?>/settings/settings_get_product_from_category',
            timeout: 15000,
            type: 'POST',
        	data: {category_id:$("#choose_category").val(),product_name:$("#search_product_input").val()},
            success: function(html){
                $("#product_list_data").html(html);
            }
        });
    }

    function choose_category_level(category_id){
    	 $.ajax({
            url: '<?php echo URL;?>/settings/category_child/'+category_id,
            timeout: 15000,
            success: function(result){
            	result = $.parseJSON(result);
            	$(".category_level_2_container").html("");

            	var html = '<select class="category_select" id="category_level_2" onchange="choose_category_level_3($(this).val());">';
            	html += '<option value=""></option>';
					for(var i in result){
						html += '<option value="'+i+'">'+result[i]+'</option>';
					}
					html += '</select>';
				$(".category_level_2_container").html(html);
				$(".category_level_3_container").html('<select></select>');
				choose_category(category_id,$("#search_product_input").val());
            }
        });
    }

     function choose_category_level_3(category_id){
    	 $.ajax({
            url: '<?php echo URL;?>/settings/category_child/'+category_id,
            timeout: 15000,
            success: function(result){
            	result = $.parseJSON(result);
            	$(".category_level_3_container").html("");

            	var html = '<select class="category_select" id="category_level_3" onchange="choose_category($(this).val(),$("#search_product_input").val());">';
            	html += '<option value=""></option>';
					for(var i in result){
						html += '<option value="'+i+'">'+result[i]+'</option>';
					}
					html += '</select>';
				$(".category_level_3_container").html(html);
				choose_category(category_id,$("#search_product_input").val());
            }
        });
    }

    function delete_barcode_line(n){
		$("#barcode"+n).remove();
		var total = $("#input_total_id").val() - 1;
		$("#input_total_id").val(total);
		return false;
    }

    function message(message){
    	$("#warming_id").html(message).show();
    	setTimeout(function(){
    		$("#warming_id").hide();
    	},6000)
    }

    function search_product_form_barcode(barcode_no){
    	$.ajax({
            url: '<?php echo URL;?>/settings/get_product_barcode',
            timeout: 15000,
            type:'POST',
            data: {barcode: barcode_no},
            success: function(result){
            	if(result != 'Null'){
            		result = $.parseJSON(result);
	            	$("#products_name_label").html(result.name);
	            	$("#in_stock").html(result.qty_in_stock);
	            	$("img",".img_block ").attr("src",result.products_upload);
	            	$("#input_barcode_name").val(result.barcode_no);
                    $("#table_barcode").append( "<tr class=record id=group1><td>1</td><td><input type='text' name=barcode[] value='" + $("#input_barcode_name").val() + "' readonly style='color:red;width:100%; border-radius: 3px;'/></td><td> <input type='text' class='qty_group' name='qty[]' value='1' style='color:red;width:100%; border-radius: 3px;' /><input type='hidden' name=type[] value='group'/></td>" + "</tr>" );
                    $(".qty_group").focus();
                    $("#products_id").val(result._id);
                    $("#radio_1").removeAttr("disabled");
                    $("#radio_2").removeAttr("disabled");
                    $("#input_barcode_name").prop('disabled',false);

            	}else{
            		message("You must choose product!");
            	}
            }
        });
    }


</script>