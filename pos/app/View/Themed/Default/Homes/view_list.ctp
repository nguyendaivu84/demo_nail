<nav class="top_header">
<ul>
    <li><a id="now_orders" onclick="display_order('old');" href="javascript:void(0)">Now orders</a></li>
    <li><a id="old_orders" onclick="display_order('now');" href="javascript:void(0)">OLD ORDERS</a></li>
    <li class="icon-logo"></li>
  </ul>
</nav>

<table class="table">
	<thead class="order_detailed reset_width_table">
	  <tr>
	    <th>STT</th>
	    <th>Name</th>
	    <th></th>
	    <th></th>
	    <th>Price</th>
	  </tr>
	</thead>
</table>

 <div id="scroll_div" class="block_dent now_order_detail" style="display: block">
  	<table class="table reset_table">
        <tbody id="list_product_of_order">
        	<?php
        		$STT = 1;
        		$sum = 0;
        		if(isset($list))
        			foreach($list as $key => $value){
        				foreach($value['products'] as $k => $v){
        					$sum +=$v['price']*$v['qty'];
        				}
        	 ?>
          <tr onclick="view_list_detail('<?php echo $value['_id']; ?>', '<?php echo $value['name']; ?>')">
          	<td>
	          	<?php
	          		if(strlen($STT)===1){
	          			echo '00';
	          		}if(strlen($STT)===2){
	          			echo '0';
	          		}
	          		echo $STT++;
	          	?>
          	</td>
          	<td>
          		<input type="text" value="<?php echo $value['name']; ?>" readonly>
          	</td>
            <td>
            	<input type="text" value="<?php echo number_format($sum); ?>" readonly>
            </td>
          </tr>
          <?php  } ?>
        </tbody>
    </table>
</div>

 <div id="scroll_div" class="block_dent old_order_detail" style="display: none">
  	<table class="table reset_table">
        <tbody id="list_product_of_order">
        	<?php
        		$STT = 1;
        		$sum = 0;
        		if(isset($check_out))
        			foreach($check_out as $key => $value){
        				foreach($value['products'] as $k => $v){
        					$sum +=$v['price']*$v['qty'];
        				}
        	 ?>
          <tr onclick="view_list_detail('<?php echo $value['_id']; ?>', '<?php echo $value['name']; ?>')">
          	<td>
	          	<?php
	          		if(strlen($STT)===1){
	          			echo '00';
	          		}if(strlen($STT)===2){
	          			echo '0';
	          		}
	          		echo $STT++;
	          	?>
          	</td>
          	<td>
          		<input type="text" value="<?php echo $value['name']; ?>" readonly>
          	</td>
            <td>
            	<input type="text" value="<?php echo number_format($sum); ?>" readonly>
            </td>
          </tr>
          <?php  } ?>
        </tbody>
    </table>
</div>