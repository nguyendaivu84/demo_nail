	<div id="element_to_pop_up">
		<a class="b-close"> x </a>
		<table width="100%" border="1" class="table_popup">
			<thead>
				<tr>
					<th><?php //if(isset($arr_language['products'])) echo $arr_language['products'];?> Products</th>
					<th><?php //if(isset($arr_language['qty'])) echo $arr_language['qty'];?> Qty</th>
					<th><?php //if(isset($arr_language['price'])) echo $arr_language['price'];?>Price</th>
					<th><?php //if(isset($arr_language['Total_Price'])) echo $arr_language['Total_Price'];?>Total price</th>
				</tr>
			</thead>
			<tbody style="overflow-y: scroll; overflow-x: hidden; height: 100px;">
				<?php
					$sum_price = 0;
					if(isset($check_out_data['products']))
						foreach($check_out_data['products'] as $key => $value){
							$sum_price += $value['price']*$value['qty'];
				 ?>
				<tr>
					<td><?php echo $value['name'];?></td>
					<td><?php echo $value['qty'];?></td>
					<td><?php echo $value['price'];?></td>
					<td><?php echo number_format($value['price']*$value['qty'])?></td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2"><?php if(isset($arr_language['Total_Price'])) echo $arr_language['Total_Price'];?></td>
					<td colspan="2"><?php echo number_format($sum_price); ?>$</td>
				</tr>
				<tr>
					<td colspan="2"><?php //if(isset($arr_language['cash'])) echo $arr_language['cash'];?> Cash: 123</td>
					<td colspan="2">0 $</td>
				</tr>
				<tr></tr>
				<tr class="end_deatails">
					<td><?php //if(isset($arr_language['cashier'])) echo $arr_language['cashier'];?> Cashier: 110052692</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><?php if(isset($arr_language['code_staff'])) echo $arr_language['code_staff'];?> Code staff: 123456463</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tfoot>
		</table>
		<ul class="list_products big_size pop_list">
			<li><a href="" class="print"><?php //if(isset($arr_language['print'])) echo $arr_language['print'];?> Print</a></li>
			<li><a href="" class="Setting"><?php //if(isset($arr_language['close'])) echo $arr_language['close'];?> Close</a></li>
		</ul>
	</div>
