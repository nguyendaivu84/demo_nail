<div id="scroll_div">
  <div class="block_list list_products">
	<table class="table">
	  <tbody>
		<?php foreach($products as $key => $value){?>
		<tr rel="<?php echo $value['_id']; ?>">
			  <td><img src="/jt/app/webroot/<?php echo str_replace('\\', '/', isset($value['products_upload']))?$value['products_upload']:''; ?>" alt="POS" style="max-width:90px"></td>
			  <td><?php echo $value['name'];?></td>
			  <td><?php echo $value['sell_price'];?>$</td>
		</tr>
		<?php } ?>
	  </tbody>
	</table>
  </div>
<!-- list products thumbs-->
<div class="block_thumbs">
	<table class="table">
	  <ul class="list_products">
		 <?php foreach($products as $key => $value){ ?>
		<li onclick="show_number('<?php echo $key; ?>')">
		  <a href="javascript:void(0)">
			<div class="img_products">
			  <img src="/jt/app/webroot/<?php echo str_replace('\\', '/', isset($value['products_upload']))?$value['products_upload']:''; ?>" alt="POS" style="max-width:90px">
			</div>
			<div class="check_span"><input type="checkbox"></div>
			<?php echo $value['name'];?>
		  </a>
		</li>
		<?php } ?>
		<div class="block_content_hd">
			<ul class="sub_list_quantity">
			  <li>
				<p class="title_q">Quantity</p>
				<div class="sub_ico_in clearfix">
				  <span class="ico-sub">sub_ico</span>
				  <span>
					<input id="number_holder" value="" type="text">
				  </span>
				  <span class="ico-plus">plus_ico</span>
				</div>
				<div class="clearfix btn_popup">
				  <a href="javascript:void(0)" class="btn-ok close_popup" >OK</a>
				  <a href="javascript:void(0)" class="btn-cancel close_popup">Cancel</a>
				</div>
			  </li>
			</ul>
			<div class="keyboard">
			  <ul class="cf" id="numbers">
				<li><a href="javascript:void(0);" class="key c49">1</a></li>
				<li><a href="javascript:void(0);" class="key c50">2</a></li>
				<li><a href="javascript:void(0);" class="key c51">3</a></li>
				<li><a href="javascript:void(0);" class="key c52">4</a></li>
				<li><a href="javascript:void(0);" class="key c53">5</a></li>
				<li><a href="javascript:void(0);" class="key c54">6</a></li>
				<li><a href="javascript:void(0);" class="key c55">7</a></li>
				<li><a href="javascript:void(0);" class="key c56">8</a></li>
				<li><a href="javascript:void(0);" class="key c57">9</a></li>
				<li><a href="javascript:void(0);" class="key c48">0</a></li>
				<li><a href="javascript:void(0);" class="key c48 clear_button">Clear</a></li>
			  </ul>
			</div>
		  </div>
		  <a class="infinite-more-link" href="<?php echo URL.'/homes/get_product_from_category'; ?>" ></a>
	  </ul>
	</table>
</div>
</div>
<script type="text/javascript">
	$(function(){
		$(".block_thumbs").css("display","none");
        $(".thumbs_clk").click(function(){
          $(".list_clk").css("color", "#CBCBCB");
          $(this).css("color", "#0E9CDC");
         $(".block_list").css("display", "none");
         $(".block_thumbs").css("display", "block");
        });
        $(".list_clk").click(function(){
          $(".thumbs_clk").css("color", "#CBCBCB");
          $(this).css("color", "#0E9CDC");
          $(".block_thumbs").css("display", "none");
          $(".block_list").css("display", "block");
        });
         var content = $(".block_content_hd");
        $(".close_popup").click(function(){
          content.fadeOut();
        });
		$(".fa-th").click();
		$("a",".keyboard").click(function(){
				if($(this).hasClass("clear_button")){
					$("#number_holder").val('');
					return false;
				}
				var value = $("#number_holder").val();
				value += $(this).text();
				$("#number_holder").val(value);
		  });

		$('#scroll_div','#list_product').scroll(function(){
			var current_height = $(this).scrollTop() + $(this).height();
			var height = $(document).height();

		   	if(current_height >= height) {
		   		load_more_product();
		   	}
		});
	})
	var page = 1;
	function show_number(product_id){
		 $(".block_content_hd").css("display", "block");
         if(product_id==undefined)
           return false;
         $("#number_holder").val("");
         $(".btn-ok",".sub_list_quantity").attr("onclick","add_new_line('"+product_id+"','number_holder');");
	}
    function load_more_product(){
    	page++;
        $.ajax({
            url: '<?php echo URL;?>/homes/get_product/'+$("#choose_category").val()+'/'+page,
            timeout: 15000,
            success: function(html){
            	if(html!="")
                	$("ul.list_products").append(html);
            }
        });
    }
</script>