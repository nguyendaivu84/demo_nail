<?php
require_once APP.'Model'.DS.'AppModel.php';
class Order extends AppModel {
	public function __construct($db) {
		if(is_object($db)){
			$this->collection = $db->selectCollection('tb_order');
			$this->db = $db;
		}
	}
}