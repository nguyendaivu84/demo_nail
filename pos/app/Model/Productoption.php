<?php
require_once APP.'Model'.DS.'AppModel.php';
class Productoption extends AppModel {
	public $arr_settings = array(
								'module_name' => 'Productoption'
							);
	public $arr_temp = array();
	public $temp = '';
	public $arr_key_nosave = array('setup','none','none1','none2','mongo_id');
	public $arr_type_nosave = array('autocomplete','id');

	function __construct($db) {
		$this->_setting();
		if(is_object($db)){
			$this->collection = $db->selectCollection($this->arr_settings['colection']);
			$this->collection->ensureIndex(array('deleted'=>1), array('name'=>'deleted_id_key'));
		}
	}

	
	// Thêm record, gán các giá trị mặc định và gán field $field = $values,
	public function add($field='',$values=''){
		$this->arr_temp = array();
		if($this->arrfield()){
			//BEGIN special
			$nextcode = $this->get_auto_code('code');
			if(isset($nextcode))
				$this->arr_temp['code'] = $nextcode;
			//END special
			$this->arr_temp[$field] = $values;
			//set default

			$this->save($this->arr_temp);
			$this->arr_temp = array();
			return $this->mongo_id_after_save.'||'.$values; //xuất ra chuỗi để js hiển thị ra html
		}else
			return '';
	}


	//update field $field  = $values của record có _id là $ids
	public function update($ids='',$field='',$values=''){
			$this->arr_temp = array(); $this->temp ='';
			$this->arr_temp['_id'] = $ids;
			if(preg_match("/_id$/",$field) && strlen($values)==24)
				$this->arr_temp[$field] = new MongoId($values); //neu la id
			else
				$this->arr_temp[$field] = $values;

			if($this->save($this->arr_temp))
				return $ids.'||'.$this->arr_temp[$field];
	}


	//update option của module. $arr_vl = Array(moduleids,moduleop,field,value,opwhere)
	public function update_value_option_of_module($arr_vl=array()){
		if($arr_vl['moduleids']!=''){
			$this->arr_temp = $arr_temp = array();
			$booleans = true;
			$this->arr_temp = $this->select_one(array('_id' => new MongoId($arr_vl['moduleids'])),array($arr_vl['moduleop']));
			$arr_temp = (array)$this->arr_temp[$arr_vl['moduleop']];

			foreach($arr_temp as $kr => $vr){
				//kiem tra dieu kien
				$booleans = true;
				foreach($arr_vl['opwhere'] as $ok => $ov){
					if($booleans && $vr[$ok]==$ov)
						$booleans = true;
					else
						$booleans = false;
				}

				//danh cho current cua product-company, neu current = 1
				if($arr_vl['field'] == 'cost_price' && $kr=='current' && $vr['current']=='1'){
					$arr_temp[$kr][$arr_vl['field']] = $arr_vl['value'];
					$this->cal_price($arr_vl['moduleids'],'cost_price',$vr['current']==$arr_vl['value']);

				//gan gia tri neu thoa opwhere
				}else if($booleans)
					$arr_temp[$kr][$arr_vl['field']] = $arr_vl['value'];
			}

			//gan gia tri cho option va luu record
			$this->arr_temp[$arr_vl['moduleop']] = $arr_temp;
			if($this->save($this->arr_temp))
				return true;
			else
				return false;
		}
	}


	//update option của module. $arr_vl = Array(moduleids,moduleop,field,value,opwhere)
	public function update_current_default($arr_vl=array()){
		if($arr_vl['moduleids']!=''){
			$this->arr_temp = $arr_temp = array();
			$booleans = true; $str = '';$fl = 0;
			$this->arr_temp = $this->select_one(array('_id' => new MongoId($arr_vl['moduleids'])),array($arr_vl['moduleop']));
			$arr_temp = (array)$this->arr_temp[$arr_vl['moduleop']];

			foreach($arr_temp as $kr => $vr){

				if((int)$arr_vl['value'] == 1 && $arr_temp[$kr]['id'] == $arr_vl['ids']){
					$arr_temp[$kr][$arr_vl['field']] = 1;
					$this->cal_price($arr_vl['moduleids'],'cost_price',$vr['cost_price']);


				}else if((int)$arr_vl['value'] == 1){
					$arr_temp[$kr][$arr_vl['field']] = 0;

				}else if((int)$arr_vl['value'] == 0 && $arr_temp[$kr]['id'] == $arr_vl['ids']){
					$arr_temp[$kr][$arr_vl['field']] = 0;

				}else if($fl == 0){
					$arr_temp[$kr][$arr_vl['field']] = 1;
					$fl = 1;
				}else{
					$arr_temp[$kr][$arr_vl['field']] = $vr['current'];
				}
			}
			//return serialize($arr_temp);
			//gan gia tri cho option va luu record
			$this->arr_temp[$arr_vl['moduleop']] = $arr_temp;
			if($this->save($this->arr_temp))
				return true;
			else
				return false;
		}
	}

	
	// list ra cac option image
	public function get_option($ids='',$opt='image'){
		if($ids!=''){
			$this->arr_temp = $this->select_one(array('_id' => new MongoId($ids)),array($opt));
			if(isset($this->arr_temp[$opt]))
				return (array)$this->arr_temp[$opt];
			else
				return array();
		}else
			return array();
	}


}