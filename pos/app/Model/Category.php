<?php
require_once APP.'Model'.DS.'AppModel.php';
class Category extends AppModel {
	public $arr_settings = array(
		'module_name' => 'Category'
	);
	public function __construct($db) {
		if(is_object($db)){
			$this->collection = $db->selectCollection('tb_nail_category');
			$this->db = $db;
		}
	}
}