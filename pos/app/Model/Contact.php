<?php
require_once APP.'Model'.DS.'AppModel.php';
class Contact extends AppModel {
	public $arr_settings = array(
		'module_name' => 'Contact'
	);
	public function __construct($db) {
		if(is_object($db)){
			$this->collection = $db->selectCollection('tb_contact');
			$this->collection->ensureIndex(array('deleted'=>1), array('name'=>'deleted_id_key'));
			$this->db = $db;
		}
	}

	public $arr_default_before_save = array();
	public function add() {
		$arr_tmp = $this->select_one(array(), array(), array('no' => -1));
		$arr_save = array();
		$arr_save['no'] = 1;
		if (isset($arr_tmp['no']))
			$arr_save['no'] = $arr_tmp['no'] + 1;
		$arr_save['is_customer'] = 0;
		$arr_save['is_employee'] = 0;
		$arr_save['inactive'] = 0;
		$arr_save['user_id'] = 0;
		$arr_save['addresses'] = array();
		$arr_save['title'] = '';
		$arr_save['first_name'] = '';
		$arr_save['last_name'] = '';
		$arr_save['full_name'] = '';
		$arr_save['direct_dial'] = '';
		$arr_save['email'] = '';
		$arr_save['inactive'] = 0;
		$arr_save['company_id'] = '';
		$arr_save['position'] = '';
		$arr_save['extension_no'] = '';
		$arr_save['mobile'] = '';
		$arr_save['addresses'] = array(
			array(
				'name' => '',
				'deleted' => false,
				'default' => true,
				'country' => 'Canada',
				'country_id' => "CA",
				'province_state' => '',
				'province_state_id' => '',
				'address_1' => '',
				'address_2' => '',
				'address_3' => '',
				'town_city' => '',
				'zip_postcode' => ''
			)
		);
		$arr_save['addresses_default_key'] = 0;

		$arr_save = array_merge($arr_save, $this->arr_default_before_save);
		if ($this->save($arr_save)) {
			return $this->mongo_id_after_save;
		} else {
			echo 'Error: ' . $this->arr_errors_save[1];die;
		}

	}

	public function update_account($contact_id, $new_balance = 0, $old_balance = 0, $option = array()){

		$arr_save = $this->select_one(array('_id' => new MongoId($contact_id)));

		if( !isset($arr_save['_id']) ){
			echo 'Contact id does not exist in db. Please check again.'; die;
		}

		if( !isset($arr_save['account']) ){
			$arr_tmp = $this->select_one(array(), array(), array('account.no' => -1));
			$arr_save['account']['no'] = 1;
			if (isset($arr_tmp['account']['no']))
				$arr_save['account']['no'] = $arr_tmp['account']['no'] + 1;

			$arr_save['account']['status'] = 'Current';
			$arr_save['account']['status_id'] = 'Current';
			$arr_save['account']['balance'] = $new_balance - $old_balance;
			if( isset($option['update_invoices_credits']) ){
				$arr_save['account']['invoices_credits'] = $new_balance - $old_balance;
			}
			if( isset($option['update_receipts']) ){
				$arr_save['account']['receipts'] = $new_balance - $old_balance;
			}
			$arr_save['account']['credit_limit'] = '';
			$arr_save['account']['receipts'] = '';
			$arr_save['account']['invoices_credits'] = '';
			$arr_save['account']['quotation_limit'] = 0;
			$arr_save['account']['difference'] = '';
			$arr_save['account']['payment_terms'] = $arr_save['account']['payment_terms_id'] = '';
			$arr_save['account']['tax_code'] = $arr_save['account']['tax_code_id'] = '';
			$arr_save['account']['nominal_code'] = $arr_save['account']['nominal_code_id'] = '';
			$arr_save['account']['tax_no'] = '';
		}else{
			$arr_save['account']['balance'] = $arr_save['account']['balance'] - $old_balance + $new_balance;
			if( isset($option['update_invoices_credits']) ){
				$arr_save['account']['invoices_credits'] = $arr_save['account']['invoices_credits'] - $old_balance + $new_balance;
			}
			if( isset($option['update_receipts']) ){

				if( $option['update_receipts'] === 1 )
					$arr_save['account']['receipts'] = $arr_save['account']['receipts'] - $old_balance + $new_balance;
				elseif( $option['update_receipts'] === -1 )// TH xóa receipt
					$arr_save['account']['receipts'] = $arr_save['account']['receipts'] + $old_balance - $new_balance;
			}
		}

		if (!$this->save($arr_save)) {
			echo 'Error: ' . $this->arr_errors_save[1]; die;
		}
		return $arr_save;
	}
}