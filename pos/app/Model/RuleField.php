<?php

/* file định nghĩa field list và hiển thị
	-Cấu trúc chung:
		Array gồm các thông tin : tên module, list field, field hiện lên tiêu đề, các sub tab
		Các field được nhóm lại thành 1 khối gọi là panel, trong panel có 1 array thông tin setup,các field
		title_field: là mảng các field hiện trên tiêu đề khi xem detail


	-Cấu trúc của 1 field gồm:
		name:			Tên hiển thị/label
		type:			Loại field
		moreclass:		Class gắn thêm cho box (box=lable+input)
		element_input:	Các element của input
		moreinline:		Nội dung (html) phía sau input
		css:			Css của input. Nếu của setup thì là css của panel
		lablewith:		Độ rộng của label (%)
		fieldwith:		Độ rộng của field (%)
		with:			With của span bao input. Ghi kèm đơn vị px hoặc %
		after:			Nội dung sau span bao input
		default:		Giá trị mặc định
		droplist:		Tên setting nếu là loại sellect
		label: 			Label nếu là loại checkbox
		id:				Id của bảng quan hệ, dùng cho loại relationship
		table:			Bảng quan hệ với module, dùng cho loại relationship
		listview:		Giá trị là 1 array, cho phép hiện trong List View, bao gồm thông tin:
							+ order : Thứ tự cột
							+ with : độ dày cột
							+ align: canh lề text
		format: 		Danh cho format tiền tệ, theo dạng format của kedo (http://demos.kendoui.com/web/numerictextbox/index.html)



	-Các loại field (type) gồm:
		text: 			input loại text
		hidden: 		input loại hidden (ẩn)
		select: 		loại select sổ chọn
		price: 			loại giá (số float, canh text phải)
		price_notchange: Loại giá nhưng ko cho phép chỉnh, chỉ dùng hiển thị
		checkbox: 		loại checkbox
		relationship:	Loại chọn popup
		not_in_data:	Ô trắng để cho bảng được cân đối, quy định key loại này là none
		fieldsave: 		không hiển thị ra giao diện, nhưng list ra trong database field. Dùng trong trường hợp đưa thêm input và moreinline.
		id:				Không có trong database thường dùng cho id của item đang xem



*/

$RuleField = array(
	'module_name' 		=> __('Rule'),
	'module_label' 	=> __('Rule'),
	'colection' => 'tb_rule',
	'field' => array(

				'panel_1' => array(
					'setup'	=> array(
							'css'	=> 'width:100%;',
							'lablewith' => '18',
							'blockcss' => 'width:29%;float:right;',
							),

					'code'	=>array(
							'name' 		=> __('Code'),
							'type' 		=> 'text',
							'moreclass' => 'fixbor',
							'listview'	=>	array(
												'order'	=>	'1',
												'with'	=>	'100',
												'align'	=>	'center',
											),
							),
					'name'	=>array(
							'name' 		=>  __('Name'),
							'type' 		=> 'text',
							'listview'	=>	array(
											'order'	=>	'1',
											'with'	=>	'100',
											'align'	=>	'left',
										),
							),
					'mongo_id'	=>array(
							'type' 		=> 'id',
							'element_input' => ' class="jthidden"',
							),
					'date_modified'=>array(
							'type' 		=> 'hidden',
							),
					'created_by'=>array(
							'type' 		=> 'hidden',
							),
					'modified_by'=>array(
							'type' 		=> 'hidden',
							),
					'description'=>array(
							'name' 		=>  __('Description'),
							'type' 		=> 'text',
							'listview'	=>	array(
											'order'	=>	'1',
											'with'	=>	'100',
										),
							),
					'status'	=>array(
							'name' 		=>  __('Status'),
							'type' 		=> 'select',
							'droplist'	=> 'product_statuses',
							'default'	=> '1',
							'moreclass' => 'fixbor2',
							'listview'	=>	array(
											'order'	=>	'1',
											'with'	=>	'100',
											'align'	=>	'left',
										),
							),

						),
					// end panel 1


				'panel_2' => array(
					'setup'	=> array(
							'css'	=> 'width:100%;',
							'lablewith' => '8',
							'blockcss' => 'width:70%;float:left;',
							),
					'tools'	=>array(
							'name' 		=>  __('Tools'),
							'type' 		=> 'not_in_data',
							'moreinline'=> '

	<div class="formula_tools">
		<input type="text" value="0" style="width:20px;" name="tools_num" id="tools_num" />
		<input type="button" value=" Add Number " name="tools_numbt" id="tools_numbt" />
		<input type="button" value=" Fields " name="tools_fields" id="tools_fields" />
		<input type="button" value=" + " name="tools_addition" id="tools_addition" />
		<input type="button" value=" - " name="tools_subtraction" id="tools_subtraction" />
		<input type="button" value=" * " name="tools_multiplication" id="tools_multiplication" />
		<input type="button" value=" / " name="tools_division" id="tools_division" />
		<input type="button" value=" AND " name="tools_and" id="tools_and" />
		<input type="button" value=" OR " name="tools_or"  id="tools_or" />
		<input type="button" value=" F " name="tools_function"  id="tools_function" />
		<input type="checkbox" value="1" name="tools_clear" id="tools_clear" /> <label for="tools_clear">Remove</label>
    	<input type="checkbox" value="1" name="tools_edit" id="tools_edit" /> <label for="tools_edit">Edit</label>
		<input type="button" value=" Save " name="tools_save"  id="tools_save" />
		<input type="button" value=" Clear all " name="tools_clear_all"  id="tools_clear_all" />
	</div>',
							'moreclass' => 'fixbor',
							),
					'formula'	=>array(
							'name' 		=>  __('Formula'),
							'type' 		=> 'mapfied',
							'default'	=> '',
							'css'		=> 'font-size:14px; height:60px;',
							),
					'map_formula'=>array(
							'type' 		=> 'fieldsave',
							'listview'	=>	array(
											'order'	=>	'1',
											'align'	=>	'left',
										),
							),
					'none'	=>array(
							'type' 		=> 'not_in_data',
							),
					'none1'	=>array(
							'type' 		=> 'not_in_data',
							'moreclass' => 'fixbor2',
							),
					), // end panel 2

			), // end field

		'title_field' => array('code','name','status','description'),



		'relationship'=> array(

				'general'=> array(
					'name' 		=>  __('General'),
					'block' =>array(
						'other_formula'=>array(
							'title'	=>__('Other Formula'),
							'css'	=>'width:100%;',
							'height' => '300',
							'searchselect'=>'1',
							'type'=>'listview_box',
							'link'	=> array('w'=>'1', 'cls'=>'rules'),
							'deleted' =>'1',
							'field'=> array(
										'code' => array(
											'name' 	=>  __('Code'),
											'width'=>'5',
											'align' => 'center',
										),
										'name' => array(
											'name' 	=>  __('Name'),
											'width'=>'10',

										),
										'description' => array(
											'name' 	=>  __('Description'),
											'width'=>'30',
										),
										'map_formula' => array(
											'name' 	=>  __('Formula'),
											'width'=>'40',
										),
										'status' => array(
											'name' 	=>  __('Status'),
											'width'=>'5',
										),
									),
						),

					),
				),

/*				'documents'=> array(
					'name' 		=>  __('Documents'),
				),
				'others'=> array(
					'name' 		=>  __('Others'),
				),
*/
			),
);