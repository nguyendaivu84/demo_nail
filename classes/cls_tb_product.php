<?php
class cls_tb_product{
    private $v_product_id = 0;
    private $v_approved = 0;
    private $v_assemply_item = 0;
    private $v_brand_id = '';
    private $v_saved_dir = '';
    private $v_update_price_date = '';
    private $v_category = '';
    private $v_category_id = '';
    private $v_check_stock_stracking = 0;
    private $v_code = 0;
    private $v_company_id = '';
    private $v_company_name = '';
    private $v_cost_price = '';
    private $v_created_by = '';
    private $v_date_modified = '0000-00-00 00:00:00';
    private $v_deleted = 0;
    private $v_description = '';
    private $v_group_type = '';
    private $v_gst_tax = '';
    private $v_in_stock = '';
    private $v_is_custom_size = 0;
    private $v_is_free_sample = 0;
    private $arr_locations = array();
    private $v_makeup = '';
    private $v_modified_by = '';
    private $v_name = '';
    private $v_none02 = '';
    private $v_product_description = '';
    private $v_product_indgridient = '';
    private $arr_product_slide_image = array();
    private $arr_product_grab = array();
    private $v_big_image = array();
    private $v_none11 = '';
    private $v_none12 = '';
    private $v_none31 = '';
    private $v_on_po = '';
    private $v_on_so = '';
    private $arr_options = array();
    private $v_oum = '';
    private $v_oum_depend = '';
    private $v_parent_product_code = '';
    private $v_parent_product_id = '';
    private $v_parent_product_name = '';
    private $arr_pricebreaks = array();
    private $v_products_upload = '';
    private $v_profit = '';
    private $v_pst_tax = '';
    private $v_qty_in_stock = 0;
    private $v_sell_by = '';
    private $v_sell_price = 0;
    private $v_sell_discount = 0;
    private $v_sell_price_cb = 0;
    private $arr_sellprices = array();
    private $v_serial = '';
    private $v_sizeh = 0;
    private $v_sizeh_unit = '';
    private $v_sizew = 0;
    private $v_sizew_unit = '';
    private $v_sku = '';
    private $v_slugger = '';
    private $v_special_order = '';
    private $v_status = '';
    private $arr_stocktakes = array();
    private $v_thickness = 0;
    private $v_thickness_unit = '';
    private $v_url_image_link_origin = '';
    private $v_under_over = '';
    private $v_unit_price = 0;
    private $v_exclusive = 0;
    private $v_online_only = 0;
    private $v_limited_edition = 0;
    private $v_update_price_by = '';
    private $v_update_price_by_id = '';
    private $collection = NULL;
    private $v_mongo_id = NULL;
    private $v_error_code = 0;
    private $v_error_message = '';
    private $v_is_log = false;
    private $v_dir = '';

    /**
     *  constructor function
     *  @param $db: instance of Mongo
     *  @param $p_log_dir string: directory contains its log file
     */
    public function __construct(MongoDB $db, $p_log_dir = ""){
        $this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
        if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
        $this->collection = $db->selectCollection('tb_product');
        $this->v_date_modified = new MongoDate(time());
        $this->collection->ensureIndex(array("product_id"=>1), array('name'=>"product_id_key", "unique"=>1, "dropDups" => 1));
    }

    /**
     *  function get current MongoDB collection
     *  @return Object: current MongoDB collection
     */
    public function get_collection(){
        return $this->collection;
    }

    /**
     *  function write log
     */
    private function my_error(){
        if(! $this->v_is_log) return;
        global $_SERVER;
        $v_filename = 'tb_product';
        $v_ext = '.log';
        $v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
        $v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
        $v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
        $v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
        $v_log_str .= "\r\n----------------End Log-----------------";
        $v_log_str .= "\r\n";
        $v_new_file = false;
        if(file_exists($this->v_dir.$v_filename.$v_ext)){
            if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
                rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
                $v_new_file = true;
                @unlink($this->v_dir.$v_filename.$v_ext);
            }
        }
        $fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
        if($fp){
            fwrite($fp, $v_log_str, strlen($v_log_str));
            fflush($fp);
            fclose($fp);
        }
    }

    /**
     * function return properties "product_id" value
     * @return int value
     */
    public function get_product_id(){
        return (int) $this->v_product_id;
    }


    /**
     * function allow change properties "product_id" value
     * @param $p_product_id: int value
     */
    public function set_product_id($p_product_id){
        $this->v_product_id = (int) $p_product_id;
    }

    /**
     * function return properties "discount" value
     * @return float value
     */
    public function get_product_sell_discount(){
        return $this->v_sell_discount;
    }


    /**
     * function allow change properties "discount" value
     * @param $p_discount: float value
     */
    public function set_product_sell_discount($p_discount){
        $this->v_sell_discount = $p_discount;
    }


    /**
     * function return properties "approved" value
     * @return int value
     */
    public function get_approved(){
        return (int) $this->v_approved;
    }


    /**
     * function allow change properties "approved" value
     * @param $p_approved: int value
     */
    public function set_approved($p_approved){
        $this->v_approved = (int) $p_approved;
    }


    /**
     * function return properties "assemply_item" value
     * @return int value
     */
    public function get_assemply_item(){
        return (int) $this->v_assemply_item;
    }


    /**
     * function allow change properties "assemply_item" value
     * @param $p_assemply_item: int value
     */
    public function set_assemply_item($p_assemply_item){
        $this->v_assemply_item = (int) $p_assemply_item;
    }


    /**
     * function return properties "brand_id" value
     * @return string value
     */
    public function get_brand_id(){
        return $this->v_brand_id;
    }


    /**
     * function allow change properties "brand_id" value
     * @param $p_brand_id: string value
     */
    public function set_brand_id($p_brand_id){
        $this->v_brand_id = $p_brand_id;
    }


    /**
     * function return properties "brand_id" value
     * @return string value
     */
    public function get_saved_dir(){
        return $this->v_saved_dir;
    }


    /**
     * function allow change properties "brand_id" value
     * @param $p_brand_id: string value
     */
    public function set_saved_dir($p_brand_id){
        $this->v_saved_dir = $p_brand_id;
    }


    /**
     * function return properties "category" value
     * @return string value
     */
    public function get_category(){
        return $this->v_category;
    }


    /**
     * function allow change properties "category" value
     * @param $p_category: string value
     */
    public function set_category($p_category){
        $this->v_category = $p_category;
    }


    /**
     * function return properties "category_id" value
     * @return string value
     */
    public function get_category_id(){
        return $this->v_category_id;
    }


    /**
     * function allow change properties "category_id" value
     * @param $p_category_id: string value
     */
    public function set_category_id($p_category_id){
        $this->v_category_id = $p_category_id;
    }


    /**
     * function return properties "check_stock_stracking" value
     * @return int value
     */
    public function get_check_stock_stracking(){
        return (int) $this->v_check_stock_stracking;
    }


    /**
     * function allow change properties "check_stock_stracking" value
     * @param $p_check_stock_stracking: int value
     */
    public function set_check_stock_stracking($p_check_stock_stracking){
        $this->v_check_stock_stracking = (int) $p_check_stock_stracking;
    }


    /**
     * function return properties "code" value
     * @return int value
     */
    public function get_code(){
        return (int) $this->v_code;
    }


    /**
     * function allow change properties "code" value
     * @param $p_code: int value
     */
    public function set_code($p_code){
        $this->v_code = (int) $p_code;
    }


    /**
     * function return properties "company_id" value
     * @return string value
     */
    public function get_company_id(){
        return $this->v_company_id;
    }


    /**
     * function allow change properties "company_id" value
     * @param $p_company_id: string value
     */
    public function set_company_id($p_company_id){
        $this->v_company_id = $p_company_id;
    }


    /**
     * function return properties "company_name" value
     * @return string value
     */
    public function get_company_name(){
        return $this->v_company_name;
    }


    /**
     * function allow change properties "company_name" value
     * @param $p_company_name: string value
     */
    public function set_company_name($p_company_name){
        $this->v_company_name = $p_company_name;
    }


    /**
     * function return properties "cost_price" value
     * @return string value
     */
    public function get_cost_price(){
        return $this->v_cost_price;
    }


    /**
     * function allow change properties "cost_price" value
     * @param $p_cost_price: string value
     */
    public function set_cost_price($p_cost_price){
        $this->v_cost_price = $p_cost_price;
    }


    /**
     * function return properties "created_by" value
     * @return string value
     */
    public function get_created_by(){
        return $this->v_created_by;
    }


    /**
     * function allow change properties "created_by" value
     * @param $p_created_by: string value
     */
    public function set_created_by($p_created_by){
        $this->v_created_by = $p_created_by;
    }


    /**
     * function return properties "date_modified" value
     * @return int value indicates amount of seconds
     */
    public function get_date_modified(){
        return  $this->v_date_modified->sec;
    }


    /**
     * function allow change properties "date_modified" value
     * @param $p_date_modified: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_date_modified($p_date_modified){
        if($p_date_modified=='') $p_date_modified = NULL;
        if(!is_null($p_date_modified)){
            try{
                $this->v_date_modified = new MongoDate(strtotime($p_date_modified));
            }catch(MongoException $me){
                $this->v_date_modified = NULL;
            }
        }else{
            $this->v_date_modified = NULL;
        }
    }


    /**
     * function return properties "deleted" value
     * @return int value
     */
    public function get_deleted(){
        return $this->v_deleted;
    }


    /**
     * function allow change properties "deleted" value
     * @param $p_deleted: int value
     */
    public function set_deleted($p_deleted){
        $this->v_deleted = $p_deleted;
    }


    /**
     * function return properties "description" value
     * @return string value
     */
    public function get_description(){
        return $this->v_description;
    }


    /**
     * function allow change properties "description" value
     * @param $p_description: string value
     */
    public function set_description($p_description){
        $this->v_description = $p_description;
    }


    /**
     * function return properties "group_type" value
     * @return string value
     */
    public function get_group_type(){
        return $this->v_group_type;
    }


    /**
     * function allow change properties "group_type" value
     * @param $p_group_type: string value
     */
    public function set_group_type($p_group_type){
        $this->v_group_type = $p_group_type;
    }


    /**
     * function return properties "gst_tax" value
     * @return string value
     */
    public function get_gst_tax(){
        return $this->v_gst_tax;
    }


    /**
     * function allow change properties "gst_tax" value
     * @param $p_gst_tax: string value
     */
    public function set_gst_tax($p_gst_tax){
        $this->v_gst_tax = $p_gst_tax;
    }


    /**
     * function return properties "in_stock" value
     * @return string value
     */
    public function get_in_stock(){
        return $this->v_in_stock;
    }


    /**
     * function allow change properties "in_stock" value
     * @param $p_in_stock: string value
     */
    public function set_in_stock($p_in_stock){
        $this->v_in_stock = $p_in_stock;
    }


    /**
     * function return properties "is_custom_size" value
     * @return int value
     */
    public function get_is_custom_size(){
        return (int) $this->v_is_custom_size;
    }


    /**
     * function allow change properties "is_custom_size" value
     * @param $p_is_custom_size: int value
     */
    public function set_is_custom_size($p_is_custom_size){
        $this->v_is_custom_size = (int) $p_is_custom_size;
    }


    /**
     * function return properties "is_free_sample" value
     * @return int value
     */
    public function get_is_free_sample(){
        return (int) $this->v_is_free_sample;
    }


    /**
     * function allow change properties "is_free_sample" value
     * @param $p_is_free_sample: int value
     */
    public function set_is_free_sample($p_is_free_sample){
        $this->v_is_free_sample = (int) $p_is_free_sample;
    }


    /**
     * function return properties "locations" value
     * @return array value
     */
    public function get_locations(){
        return  $this->arr_locations;
    }


    /**
     * function allow change properties "locations" value
     * @param $arr_locations array
     */
    public function set_locations(array $arr_locations = array()){
        $this->arr_locations = $arr_locations;
    }


    /**
     * function return properties "makeup" value
     * @return string value
     */
    public function get_makeup(){
        return $this->v_makeup;
    }


    /**
     * function allow change properties "makeup" value
     * @param $p_makeup: string value
     */
    public function set_makeup($p_makeup){
        $this->v_makeup = $p_makeup;
    }


    /**
     * function return properties "modified_by" value
     * @return string value
     */
    public function get_modified_by(){
        return $this->v_modified_by;
    }


    /**
     * function allow change properties "modified_by" value
     * @param $p_modified_by: string value
     */
    public function set_modified_by($p_modified_by){
        $this->v_modified_by = $p_modified_by;
    }


    /**
     * function return properties "name" value
     * @return string value
     */
    public function get_name(){
        return $this->v_name;
    }


    /**
     * function allow change properties "name" value
     * @param $p_name: string value
     */
    public function set_name($p_name){
        $this->v_name = $p_name;
    }


    /**
     * function return properties "none02" value
     * @return string value
     */
    public function get_none02(){
        return $this->v_none02;
    }


    /**
     * function allow change properties "none02" value
     * @param $p_none02: string value
     */
    public function set_none02($p_none02){
        $this->v_none02 = $p_none02;
    }
    public function get_product_grab(){
        return $this->arr_product_grab;
    }
    public function set_product_grab($p_none02 =array()){
        $this->arr_product_grab = $p_none02;
    }

    public function get_product_slide_image(){
        return $this->arr_product_slide_image;
    }
    public function set_product_slide_image($p_none02 =array()){
        $this->arr_product_slide_image = $p_none02;
    }

    public function get_product_big_image(){
        return $this->v_big_image;
    }
    public function set_product_big_image($p_none02){
        $this->v_big_image = $p_none02;
    }

    public function get_product_indgridient(){
        return $this->v_product_indgridient;
    }
    public function set_product_indgridient($p_none02){
        $this->v_product_indgridient = $p_none02;
    }

    public function get_product_decription(){
        return $this->v_product_description;
    }
    public function set_product_decription($p_none02){
        $this->v_product_description = $p_none02;
    }

    public function get_update_price_date(){
        return $this->v_update_price_date;
    }
    public function set_update_price_date($p_none02){
        $this->v_update_price_date = $p_none02;
    }


    /**
     * function return properties "none11" value
     * @return string value
     */
    public function get_none11(){
        return $this->v_none11;
    }


    /**
     * function allow change properties "none11" value
     * @param $p_none11: string value
     */
    public function set_none11($p_none11){
        $this->v_none11 = $p_none11;
    }


    /**
     * function return properties "none12" value
     * @return string value
     */
    public function get_none12(){
        return $this->v_none12;
    }


    /**
     * function allow change properties "none12" value
     * @param $p_none12: string value
     */
    public function set_none12($p_none12){
        $this->v_none12 = $p_none12;
    }


    /**
     * function return properties "none31" value
     * @return string value
     */
    public function get_none31(){
        return $this->v_none31;
    }


    /**
     * function allow change properties "none31" value
     * @param $p_none31: string value
     */
    public function set_none31($p_none31){
        $this->v_none31 = $p_none31;
    }


    /**
     * function return properties "on_po" value
     * @return string value
     */
    public function get_on_po(){
        return $this->v_on_po;
    }


    /**
     * function allow change properties "on_po" value
     * @param $p_on_po: string value
     */
    public function set_on_po($p_on_po){
        $this->v_on_po = $p_on_po;
    }


    /**
     * function return properties "on_so" value
     * @return string value
     */
    public function get_on_so(){
        return $this->v_on_so;
    }


    /**
     * function allow change properties "on_so" value
     * @param $p_on_so: string value
     */
    public function set_on_so($p_on_so){
        $this->v_on_so = $p_on_so;
    }


    /**
     * function return properties "options" value
     * @return array value
     */
    public function get_options(){
        return  $this->arr_options;
    }


    /**
     * function allow change properties "options" value
     * @param $arr_options array
     */
    public function set_options(array $arr_options = array()){
        $this->arr_options = $arr_options;
    }


    /**
     * function return properties "oum" value
     * @return string value
     */
    public function get_oum(){
        return $this->v_oum;
    }


    /**
     * function allow change properties "oum" value
     * @param $p_oum: string value
     */
    public function set_oum($p_oum){
        $this->v_oum = $p_oum;
    }


    /**
     * function return properties "oum_depend" value
     * @return string value
     */
    public function get_oum_depend(){
        return $this->v_oum_depend;
    }


    /**
     * function allow change properties "oum_depend" value
     * @param $p_oum_depend: string value
     */
    public function set_oum_depend($p_oum_depend){
        $this->v_oum_depend = $p_oum_depend;
    }


    /**
     * function return properties "parent_product_code" value
     * @return string value
     */
    public function get_parent_product_code(){
        return $this->v_parent_product_code;
    }


    /**
     * function allow change properties "parent_product_code" value
     * @param $p_parent_product_code: string value
     */
    public function set_parent_product_code($p_parent_product_code){
        $this->v_parent_product_code = $p_parent_product_code;
    }


    /**
     * function return properties "parent_product_id" value
     * @return string value
     */
    public function get_parent_product_id(){
        return $this->v_parent_product_id;
    }


    /**
     * function allow change properties "parent_product_id" value
     * @param $p_parent_product_id: string value
     */
    public function set_parent_product_id($p_parent_product_id){
        $this->v_parent_product_id = $p_parent_product_id;
    }


    /**
     * function return properties "parent_product_name" value
     * @return string value
     */
    public function get_parent_product_name(){
        return $this->v_parent_product_name;
    }


    /**
     * function allow change properties "parent_product_name" value
     * @param $p_parent_product_name: string value
     */
    public function set_parent_product_name($p_parent_product_name){
        $this->v_parent_product_name = $p_parent_product_name;
    }


    /**
     * function return properties "pricebreaks" value
     * @return array value
     */
    public function get_pricebreaks(){
        return  $this->arr_pricebreaks;
    }


    /**
     * function allow change properties "pricebreaks" value
     * @param $arr_pricebreaks array
     */
    public function set_pricebreaks(array $arr_pricebreaks = array()){
        $this->arr_pricebreaks = $arr_pricebreaks;
    }


    /**
     * function return properties "products_upload" value
     * @return string value
     */
    public function get_products_upload(){
        return $this->v_products_upload;
    }


    /**
     * function allow change properties "products_upload" value
     * @param $p_products_upload: string value
     */
    public function set_products_upload($p_products_upload){
        $this->v_products_upload = $p_products_upload;
    }


    /**
     * function return properties "profit" value
     * @return string value
     */
    public function get_profit(){
        return $this->v_profit;
    }


    /**
     * function allow change properties "profit" value
     * @param $p_profit: string value
     */
    public function set_profit($p_profit){
        $this->v_profit = $p_profit;
    }


    /**
     * function return properties "pst_tax" value
     * @return string value
     */
    public function get_pst_tax(){
        return $this->v_pst_tax;
    }


    /**
     * function allow change properties "pst_tax" value
     * @param $p_pst_tax: string value
     */
    public function set_pst_tax($p_pst_tax){
        $this->v_pst_tax = $p_pst_tax;
    }


    /**
     * function return properties "qty_in_stock" value
     * @return int value
     */
    public function get_qty_in_stock(){
        return (int) $this->v_qty_in_stock;
    }


    /**
     * function allow change properties "qty_in_stock" value
     * @param $p_qty_in_stock: int value
     */
    public function set_qty_in_stock($p_qty_in_stock){
        $this->v_qty_in_stock = (int) $p_qty_in_stock;
    }


    /**
     * function return properties "sell_by" value
     * @return string value
     */
    public function get_sell_by(){
        return $this->v_sell_by;
    }


    /**
     * function allow change properties "sell_by" value
     * @param $p_sell_by: string value
     */
    public function set_sell_by($p_sell_by){
        $this->v_sell_by = $p_sell_by;
    }


    /**
     * function return properties "sell_price" value
     * @return float value
     */
    public function get_sell_price(){
        return (float) $this->v_sell_price;
    }


    /**
     * function allow change properties "sell_price" value
     * @param $p_sell_price: float value
     */
    public function set_sell_price($p_sell_price){
        $this->v_sell_price = (float) $p_sell_price;
    }


    /**
     * function return properties "sell_price_cb" value
     * @return float value
     */
    public function get_sell_price_cb(){
        return (float) $this->v_sell_price_cb;
    }


    /**
     * function allow change properties "sell_price_cb" value
     * @param $p_sell_price_cb: float value
     */
    public function set_sell_price_cb($p_sell_price_cb){
        $this->v_sell_price_cb = (float) $p_sell_price_cb;
    }


    /**
     * function return properties "sellprices" value
     * @return array value
     */
    public function get_sellprices(){
        return  $this->arr_sellprices;
    }


    /**
     * function allow change properties "sellprices" value
     * @param $arr_sellprices array
     */
    public function set_sellprices(array $arr_sellprices = array()){
        $this->arr_sellprices = $arr_sellprices;
    }


    /**
     * function return properties "serial" value
     * @return string value
     */
    public function get_serial(){
        return $this->v_serial;
    }


    /**
     * function allow change properties "serial" value
     * @param $p_serial: string value
     */
    public function set_serial($p_serial){
        $this->v_serial = $p_serial;
    }


    /**
     * function return properties "sizeh" value
     * @return int value
     */
    public function get_sizeh(){
        return (int) $this->v_sizeh;
    }


    /**
     * function allow change properties "sizeh" value
     * @param $p_sizeh: int value
     */
    public function set_sizeh($p_sizeh){
        $this->v_sizeh = (int) $p_sizeh;
    }


    /**
     * function return properties "sizeh_unit" value
     * @return string value
     */
    public function get_sizeh_unit(){
        return $this->v_sizeh_unit;
    }


    /**
     * function allow change properties "sizeh_unit" value
     * @param $p_sizeh_unit: string value
     */
    public function set_sizeh_unit($p_sizeh_unit){
        $this->v_sizeh_unit = $p_sizeh_unit;
    }


    /**
     * function return properties "sizew" value
     * @return int value
     */
    public function get_sizew(){
        return (int) $this->v_sizew;
    }


    /**
     * function allow change properties "sizew" value
     * @param $p_sizew: int value
     */
    public function set_sizew($p_sizew){
        $this->v_sizew = (int) $p_sizew;
    }


    /**
     * function return properties "sizew_unit" value
     * @return string value
     */
    public function get_sizew_unit(){
        return $this->v_sizew_unit;
    }


    /**
     * function allow change properties "sizew_unit" value
     * @param $p_sizew_unit: string value
     */
    public function set_sizew_unit($p_sizew_unit){
        $this->v_sizew_unit = $p_sizew_unit;
    }


    /**
     * function return properties "sku" value
     * @return string value
     */
    public function get_sku(){
        return $this->v_sku;
    }


    /**
     * function allow change properties "sku" value
     * @param $p_sku: string value
     */
    public function set_sku($p_sku){
        $this->v_sku = $p_sku;
    }


    /**
     * function return properties "slugger" value
     * @return string value
     */
    public function get_slugger(){
        return $this->v_slugger;
    }


    /**
     * function allow change properties "slugger" value
     * @param $p_slugger: string value
     */
    public function set_slugger($p_slugger){
        $this->v_slugger = $p_slugger;
    }


    /**
     * function return properties "special_order" value
     * @return string value
     */
    public function get_special_order(){
        return $this->v_special_order;
    }


    /**
     * function allow change properties "special_order" value
     * @param $p_special_order: string value
     */
    public function set_special_order($p_special_order){
        $this->v_special_order = $p_special_order;
    }


    /**
     * function return properties "status" value
     * @return string value
     */
    public function get_status(){
        return $this->v_status;
    }


    /**
     * function allow change properties "status" value
     * @param $p_status: string value
     */
    public function set_status($p_status){
        $this->v_status = $p_status;
    }


    /**
     * function return properties "stocktakes" value
     * @return array value
     */
    public function get_stocktakes(){
        return  $this->arr_stocktakes;
    }


    /**
     * function allow change properties "stocktakes" value
     * @param $arr_stocktakes array
     */
    public function set_stocktakes(array $arr_stocktakes = array()){
        $this->arr_stocktakes = $arr_stocktakes;
    }


    /**
     * function return properties "thickness" value
     * @return int value
     */
    public function get_thickness(){
        return (int) $this->v_thickness;
    }


    /**
     * function allow change properties "thickness" value
     * @param $p_thickness: int value
     */
    public function set_thickness($p_thickness){
        $this->v_thickness = (int) $p_thickness;
    }


    /**
     * function return properties "thickness_unit" value
     * @return string value
     */
    public function get_thickness_unit(){
        return $this->v_thickness_unit;
    }


    /**
     * function allow change properties "thickness_unit" value
     * @param $p_thickness_unit: string value
     */
    public function set_thickness_unit($p_thickness_unit){
        $this->v_thickness_unit = $p_thickness_unit;
    }


    /**
     * function return properties "thickness_unit" value
     * @return string value
     */
    public function get_url_image_link_origin(){
        return $this->v_url_image_link_origin;
    }


    /**
     * function allow change properties "thickness_unit" value
     * @param $p_thickness_unit: string value
     */
    public function set_url_image_link_origin($p_thickness_unit){
        $this->v_url_image_link_origin = $p_thickness_unit;
    }


    /**
     * function return properties "under_over" value
     * @return string value
     */
    public function get_under_over(){
        return $this->v_under_over;
    }


    /**
     * function allow change properties "under_over" value
     * @param $p_under_over: string value
     */
    public function set_under_over($p_under_over){
        $this->v_under_over = $p_under_over;
    }


    /**
     * function return properties "unit_price" value
     * @return int value
     */
    public function get_unit_price(){
        return (int) $this->v_unit_price;
    }


    /**
     * function allow change properties "unit_price" value
     * @param $p_unit_price: int value
     */
    public function set_unit_price($p_unit_price){
        $this->v_unit_price = (int) $p_unit_price;
    }
    /**
     * function return properties "limited_edition" value
     * @return int value
     */
    public function get_limited_edition(){
        return (int) $this->v_online_only;
    }


    /**
     * function allow change properties "limited_edition" value
     * @param $p_unit_price: int value
     */
    public function set_limited_edition($p_unit_price){
        $this->v_online_only = (int) $p_unit_price;
    }
    /**
     * function return properties "online_only" value
     * @return int value
     */
    public function get_online_only(){
        return (int) $this->v_online_only;
    }


    /**
     * function allow change properties "online_only" value
     * @param $p_unit_price: int value
     */
    public function set_online_only($p_unit_price){
        $this->v_online_only = (int) $p_unit_price;
    }
    /**
     * function return properties "exclusive" value
     * @return int value
     */
    public function get_exclusive(){
        return (int) $this->v_exclusive;
    }


    /**
     * function allow change properties "exclusive" value
     * @param $p_unit_price: int value
     */
    public function set_exclusive($p_unit_price){
        $this->v_exclusive = (int) $p_unit_price;
    }


    /**
     * function return properties "update_price_by" value
     * @return string value
     */
    public function get_update_price_by(){
        return $this->v_update_price_by;
    }


    /**
     * function allow change properties "update_price_by" value
     * @param $p_update_price_by: string value
     */
    public function set_update_price_by($p_update_price_by){
        $this->v_update_price_by = $p_update_price_by;
    }


    /**
     * function return properties "update_price_by_id" value
     * @return string value
     */
    public function get_update_price_by_id(){
        return $this->v_update_price_by_id;
    }


    /**
     * function allow change properties "update_price_by_id" value
     * @param $p_update_price_by_id: string value
     */
    public function set_update_price_by_id($p_update_price_by_id){
        $this->v_update_price_by_id = $p_update_price_by_id;
    }


    /**
     * function return properties "update_price_date" value
    }


    /**
     * function allow change properties "update_price_date" value
    }


    /**
     * function return MongoID value after inserting new record
     * @return ObjectId: MongoId
     */
    public function get_mongo_id(){
        return $this->v_mongo_id;
    }


    /**
     * function set MongoID to properties
     */
    public function set_mongo_id($p_mongo_id){
        $this->v_mongo_id = $p_mongo_id;
    }


    /**
     *  function allow insert one record
     *  @return MongoID
     */
    public function insert(){
        $arr = array('product_id' => $this->v_product_id
        ,'approved' => $this->v_approved
        ,'discount' => $this->v_sell_discount
        ,'assemply_item' => $this->v_assemply_item
        ,'brand_id' => $this->v_brand_id
        ,'saved_dir' => $this->v_saved_dir
        ,'category' => $this->v_category
        ,'category_id' => $this->v_category_id
        ,'check_stock_stracking' => $this->v_check_stock_stracking
        ,'code' => $this->v_code
        ,'company_id' => $this->v_company_id
        ,'company_name' => $this->v_company_name
        ,'cost_price' => $this->v_cost_price
        ,'created_by' => $this->v_created_by
        ,'date_modified' => $this->v_date_modified
        ,'deleted' => $this->v_deleted
        ,'description' => $this->v_description
        ,'group_type' => $this->v_group_type
        ,'gst_tax' => $this->v_gst_tax
        ,'in_stock' => $this->v_in_stock
        ,'is_custom_size' => $this->v_is_custom_size
        ,'is_free_sample' => $this->v_is_free_sample
        ,'locations' => $this->arr_locations
        ,'makeup' => $this->v_makeup
        ,'modified_by' => $this->v_modified_by
        ,'name' => $this->v_name
        ,'none02' => $this->v_none02
        ,'product_description' => $this->v_product_description
        ,'product_ingredients' => $this->v_product_indgridient
        ,'product_slide_image' => $this->arr_product_slide_image
        ,'product_grab' => $this->arr_product_grab
        ,'big_image' => $this->v_big_image
        ,'none11' => $this->v_none11
        ,'none12' => $this->v_none12
        ,'none31' => $this->v_none31
        ,'on_po' => $this->v_on_po
        ,'on_so' => $this->v_on_so
        ,'options' => $this->arr_options
        ,'oum' => $this->v_oum
        ,'oum_depend' => $this->v_oum_depend
        ,'parent_product_code' => $this->v_parent_product_code
        ,'parent_product_id' => $this->v_parent_product_id
        ,'parent_product_name' => $this->v_parent_product_name
        ,'pricebreaks' => $this->arr_pricebreaks
        ,'products_upload' => $this->v_products_upload
        ,'profit' => $this->v_profit
        ,'pst_tax' => $this->v_pst_tax
        ,'qty_in_stock' => $this->v_qty_in_stock
        ,'sell_by' => $this->v_sell_by
        ,'sell_price' => $this->v_sell_price
        ,'sell_price_cb' => $this->v_sell_price_cb
        ,'sellprices' => $this->arr_sellprices
        ,'serial' => $this->v_serial
        ,'sizeh' => $this->v_sizeh
        ,'sizeh_unit' => $this->v_sizeh_unit
        ,'sizew' => $this->v_sizew
        ,'sizew_unit' => $this->v_sizew_unit
        ,'sku' => $this->v_sku
        ,'slugger' => $this->v_slugger
        ,'special_order' => $this->v_special_order
        ,'status' => $this->v_status
        ,'stocktakes' => $this->arr_stocktakes
        ,'thickness' => $this->v_thickness
        ,'thickness_unit' => $this->v_thickness_unit
        ,'image_url_origin' => $this->v_url_image_link_origin
        ,'under_over' => $this->v_under_over
        ,'unit_price' => $this->v_unit_price
        ,'exclusive' => $this->v_exclusive
        ,'online_only' => $this->v_online_only
        ,'limited_edition' => $this->v_limited_edition
        ,'update_price_by' => $this->v_update_price_by
        ,'update_price_by_id' => $this->v_update_price_by_id
        ,'update_price_date' => $this->v_update_price_date);
        try{
            $this->collection->insert($arr, array('safe'=>true));
            $this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            var_dump($e);
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
        die;
    }


    /**
     *  function allow insert array with parameter
     *  @param array $arr_fields_and_values
     *  @return MongoID
     */
    public function insert_array(array $arr_fields_and_values){
        try{
            $this->collection->insert($arr_fields_and_values, array('safe'=>true));
            $this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     * function select_one_record
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: all values will assign to this instance properties
     * @example:
     * <code>
     *       SELECT * FROM `tb_product` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return int
     */
    public function select_one(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_count = 0;
        foreach($rss as $arr){
            $this->v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
            $this->v_sell_discount = isset($arr['discount'])?$arr['discount']:0;
            $this->v_approved = isset($arr['approved'])?$arr['approved']:0;
            $this->v_assemply_item = isset($arr['assemply_item'])?$arr['assemply_item']:0;
            $this->v_brand_id = isset($arr['brand_id'])?$arr['brand_id']:'';
            $this->v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
            $this->v_category = isset($arr['category'])?$arr['category']:'';
            $this->v_category_id = isset($arr['category_id'])?$arr['category_id']:'';
            $this->v_check_stock_stracking = isset($arr['check_stock_stracking'])?$arr['check_stock_stracking']:0;
            $this->v_code = isset($arr['code'])?$arr['code']:0;
            $this->v_company_id = isset($arr['company_id'])?$arr['company_id']:'';
            $this->v_company_name = isset($arr['company_name'])?$arr['company_name']:'';
            $this->v_cost_price = isset($arr['cost_price'])?$arr['cost_price']:'';
            $this->v_created_by = isset($arr['created_by'])?$arr['created_by']:'';
            $this->v_date_modified = isset($arr['date_modified'])?$arr['date_modified']:(new MongoDate(time()));
            $this->v_deleted = isset($arr['deleted'])?$arr['deleted']:0;
            $this->v_description = isset($arr['description'])?$arr['description']:'';
            $this->v_group_type = isset($arr['group_type'])?$arr['group_type']:'';
            $this->v_gst_tax = isset($arr['gst_tax'])?$arr['gst_tax']:'';
            $this->v_in_stock = isset($arr['in_stock'])?$arr['in_stock']:'';
            $this->v_is_custom_size = isset($arr['is_custom_size'])?$arr['is_custom_size']:0;
            $this->v_is_free_sample = isset($arr['is_free_sample'])?$arr['is_free_sample']:0;
            $this->arr_locations = isset($arr['locations'])?$arr['locations']:array();
            $this->v_makeup = isset($arr['makeup'])?$arr['makeup']:'';
            $this->v_modified_by = isset($arr['modified_by'])?$arr['modified_by']:'';
            $this->v_name = isset($arr['name'])?$arr['name']:'';
            $this->v_none02 = isset($arr['none02'])?$arr['none02']:'';

            $this->v_product_description = isset($arr['product_description'])?$arr['product_description']:'';
            $this->v_product_indgridient = isset($arr['product_ingredients'])?$arr['product_ingredients']:'';
            $this->arr_product_slide_image = isset($arr['product_slide_image'])?$arr['product_slide_image']:array();
            $this->arr_product_grab = isset($arr['product_grab'])?$arr['product_grab']:array();
            $this->v_big_image = isset($arr['big_image'])?$arr['big_image']:'';
            $this->v_none11 = isset($arr['none11'])?$arr['none11']:'';
            $this->v_none12 = isset($arr['none12'])?$arr['none12']:'';
            $this->v_none31 = isset($arr['none31'])?$arr['none31']:'';
            $this->v_on_po = isset($arr['on_po'])?$arr['on_po']:'';
            $this->v_on_so = isset($arr['on_so'])?$arr['on_so']:'';
            $this->arr_options = isset($arr['options'])?$arr['options']:array();
            $this->v_oum = isset($arr['oum'])?$arr['oum']:'';
            $this->v_oum_depend = isset($arr['oum_depend'])?$arr['oum_depend']:'';
            $this->v_parent_product_code = isset($arr['parent_product_code'])?$arr['parent_product_code']:'';
            $this->v_parent_product_id = isset($arr['parent_product_id'])?$arr['parent_product_id']:'';
            $this->v_parent_product_name = isset($arr['parent_product_name'])?$arr['parent_product_name']:'';
            $this->arr_pricebreaks = isset($arr['pricebreaks'])?$arr['pricebreaks']:array();
            $this->v_products_upload = isset($arr['products_upload'])?$arr['products_upload']:'';
            $this->v_profit = isset($arr['profit'])?$arr['profit']:'';
            $this->v_pst_tax = isset($arr['pst_tax'])?$arr['pst_tax']:'';
            $this->v_qty_in_stock = isset($arr['qty_in_stock'])?$arr['qty_in_stock']:0;
            $this->v_sell_by = isset($arr['sell_by'])?$arr['sell_by']:'';
            $this->v_sell_price = isset($arr['sell_price'])?$arr['sell_price']:0;
            $this->v_sell_price_cb = isset($arr['sell_price_cb'])?$arr['sell_price_cb']:0;
            $this->arr_sellprices = isset($arr['sellprices'])?$arr['sellprices']:array();
            $this->v_serial = isset($arr['serial'])?$arr['serial']:'';
            $this->v_sizeh = isset($arr['sizeh'])?$arr['sizeh']:0;
            $this->v_sizeh_unit = isset($arr['sizeh_unit'])?$arr['sizeh_unit']:'';
            $this->v_sizew = isset($arr['sizew'])?$arr['sizew']:0;
            $this->v_sizew_unit = isset($arr['sizew_unit'])?$arr['sizew_unit']:'';
            $this->v_sku = isset($arr['sku'])?$arr['sku']:'';
            $this->v_slugger = isset($arr['slugger'])?$arr['slugger']:'';
            $this->v_special_order = isset($arr['special_order'])?$arr['special_order']:'';
            $this->v_status = isset($arr['status'])?$arr['status']:'';
            $this->arr_stocktakes = isset($arr['stocktakes'])?$arr['stocktakes']:array();
            $this->v_thickness = isset($arr['thickness'])?$arr['thickness']:0;
            $this->v_thickness_unit = isset($arr['thickness_unit'])?$arr['thickness_unit']:'';
            $this->v_url_image_link_origin = isset($arr['image_url_origin'])?$arr['image_url_origin']:'';
            $this->v_under_over = isset($arr['under_over'])?$arr['under_over']:'';
            $this->v_unit_price = isset($arr['unit_price'])?$arr['unit_price']:0;
            $this->v_exclusive = isset($arr['exclusive'])?$arr['exclusive']:0;
            $this->v_online_only = isset($arr['online_only'])?$arr['online_only']:0;
            $this->v_limited_edition = isset($arr['limited_edition'])?$arr['limited_edition']:0;
            $this->v_update_price_by = isset($arr['update_price_by'])?$arr['update_price_by']:'';
            $this->v_update_price_by_id = isset($arr['update_price_by_id'])?$arr['update_price_by_id']:'';
            $this->v_update_price_date = isset($arr['update_price_date'])?$arr['update_price_date']:'';
            $this->v_mongo_id = $arr['_id'];
            $v_count++;
        }
        return $v_count;
    }
    /**
     * function select scalar value
     * @param $p_field_name string, name of field
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: assign to properties
     * @example:
     * <code>
     * SELECT `update_price_date` FROM `tb_product` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select_scalar('update_price_date',array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return mixed
     */
    public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = NULL;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return $v_ret;
    }

    /**
     * function get next int value for key
     * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @result: assign to properties
     * @example:
     * <code>
     *   SELECT `update_price_date` FROM `tb_product` WHERE `user_id`=2 ORDER BY `update_price_date` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select_next('update_price_date',array('user_id'=>2), array('update_price_date'=>-1))
     * </code>
     * @return int
     */
    public function select_next($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => -1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = 0;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return ((int) $v_ret)+1;
    }

    /**
     * function get missing value
     * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function select_missing($p_field_name, array $arr_where = array()){
        $arr_order = array(''.$p_field_name.'' => 1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_start = 1;
        $v_ret = 1;
        foreach($rss as $arr){
            if($arr[''.$p_field_name.'']!=$v_start){
                $v_ret = $v_start;
                break;
            }
            $v_start++;
        }
        return ((int) $v_ret);
    }

    /**
     * function select limit records
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr;
    }

    /**
     * function select records
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order);
        return $arr;
    }

    /**
     * function select distinct
     * @param $p_field_name string, name of selected field
     * @example:
     * <code>
     *         SELECT DISTINCT `name` FROM `tbl_users`
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select_distinct('nam')
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_distinct($p_field_name){
        return $this->collection->command(array("distinct"=>"tb_product", "key"=>$p_field_name));
    }

    /**
     * function select limit fields
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_fields array, array of fields will be selected
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_product($db)
     * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr_field = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr_field[$arr_fields[$i]] = 1;
        if($p_row <= 0)
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
        else
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr_return;
    }

    /**
     *  function update one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(isset($v_has_mongo_id) && $v_has_mongo_id)
            $arr = array('$set' => array(
                'product_id' => $this->v_product_id,'approved' => $this->v_approved,'assemply_item' => $this->v_assemply_item,'brand_id' => $this->v_brand_id,'category' => $this->v_category,'category_id' => $this->v_category_id,'check_stock_stracking' => $this->v_check_stock_stracking,'code' => $this->v_code,'company_id' => $this->v_company_id,'company_name' => $this->v_company_name,'cost_price' => $this->v_cost_price,'created_by' => $this->v_created_by,'date_modified' => $this->v_date_modified,'deleted' => $this->v_deleted,'description' => $this->v_description,'group_type' => $this->v_group_type,'gst_tax' => $this->v_gst_tax,'in_stock' => $this->v_in_stock,'is_custom_size' => $this->v_is_custom_size,'is_free_sample' => $this->v_is_free_sample,'locations' => $this->arr_locations    ,'product_description' => $this->v_product_description
                ,'product_ingredients' => $this->v_product_indgridient
                ,'discount' => $this->v_sell_discount
                ,'product_slide_image' => $this->arr_product_slide_image
                ,'product_grab' => $this->arr_product_grab
                ,'saved_dir' => $this->v_saved_dir
                ,'big_image' => $this->v_big_image,'makeup' => $this->v_makeup,'modified_by' => $this->v_modified_by,'name' => $this->v_name,'none02' => $this->v_none02,'none11' => $this->v_none11,'none12' => $this->v_none12,'none31' => $this->v_none31,'on_po' => $this->v_on_po,'on_so' => $this->v_on_so,'options' => $this->arr_options,'oum' => $this->v_oum,'oum_depend' => $this->v_oum_depend,'parent_product_code' => $this->v_parent_product_code,'parent_product_id' => $this->v_parent_product_id,'parent_product_name' => $this->v_parent_product_name,'pricebreaks' => $this->arr_pricebreaks,'products_upload' => $this->v_products_upload,'profit' => $this->v_profit,'pst_tax' => $this->v_pst_tax,'qty_in_stock' => $this->v_qty_in_stock,'sell_by' => $this->v_sell_by,'sell_price' => $this->v_sell_price,'sell_price_cb' => $this->v_sell_price_cb,'sellprices' => $this->arr_sellprices,'serial' => $this->v_serial,'sizeh' => $this->v_sizeh,'sizeh_unit' => $this->v_sizeh_unit,'sizew' => $this->v_sizew,'sizew_unit' => $this->v_sizew_unit,'sku' => $this->v_sku,'slugger' => $this->v_slugger,'special_order' => $this->v_special_order,'status' => $this->v_status,'stocktakes' => $this->arr_stocktakes,'image_url_origin' => $this->v_url_image_link_origin,'thickness' => $this->v_thickness,'thickness_unit' => $this->v_thickness_unit,'under_over' => $this->v_under_over    ,'exclusive' => $this->v_exclusive
            ,'online_only' => $this->v_online_only
            ,'limited_edition' => $this->v_limited_edition,'unit_price' => $this->v_unit_price,'update_price_by' => $this->v_update_price_by,'update_price_by_id' => $this->v_update_price_by_id,'update_price_date' => $this->v_update_price_date));
        else
            $arr =  array('$set' => array(
                'approved' => $this->v_approved,'assemply_item' => $this->v_assemply_item
                ,'exclusive' => $this->v_exclusive
                ,'online_only' => $this->v_online_only
                ,'limited_edition' => $this->v_limited_edition,'brand_id' => $this->v_brand_id,'category' => $this->v_category,'category_id' => $this->v_category_id,'check_stock_stracking' => $this->v_check_stock_stracking,'code' => $this->v_code,'company_id' => $this->v_company_id,'company_name' => $this->v_company_name,'cost_price' => $this->v_cost_price,'created_by' => $this->v_created_by,'date_modified' => $this->v_date_modified,'deleted' => $this->v_deleted,'description' => $this->v_description,'group_type' => $this->v_group_type,'gst_tax' => $this->v_gst_tax,'in_stock' => $this->v_in_stock,'is_custom_size' => $this->v_is_custom_size,'is_free_sample' => $this->v_is_free_sample,'locations' => $this->arr_locations    ,'product_description' => $this->v_product_description
                ,'product_ingredients' => $this->v_product_indgridient
                ,'product_slide_image' => $this->arr_product_slide_image
                ,'discount' => $this->v_sell_discount
                ,'product_grab' => $this->arr_product_grab
                ,'saved_dir' => $this->v_saved_dir
                ,'big_image' => $this->v_big_image,'makeup' => $this->v_makeup,'modified_by' => $this->v_modified_by,'name' => $this->v_name,'none02' => $this->v_none02,'none11' => $this->v_none11,'none12' => $this->v_none12,'none31' => $this->v_none31,'on_po' => $this->v_on_po,'on_so' => $this->v_on_so,'options' => $this->arr_options,'oum' => $this->v_oum,'oum_depend' => $this->v_oum_depend,'parent_product_code' => $this->v_parent_product_code,'parent_product_id' => $this->v_parent_product_id,'parent_product_name' => $this->v_parent_product_name,'pricebreaks' => $this->arr_pricebreaks,'products_upload' => $this->v_products_upload,'profit' => $this->v_profit,'pst_tax' => $this->v_pst_tax,'qty_in_stock' => $this->v_qty_in_stock,'sell_by' => $this->v_sell_by,'sell_price' => $this->v_sell_price,'sell_price_cb' => $this->v_sell_price_cb,'sellprices' => $this->arr_sellprices,'serial' => $this->v_serial,'sizeh' => $this->v_sizeh,'sizeh_unit' => $this->v_sizeh_unit,'sizew' => $this->v_sizew,'sizew_unit' => $this->v_sizew_unit,'sku' => $this->v_sku,'slugger' => $this->v_slugger,'special_order' => $this->v_special_order,'status' => $this->v_status,'stocktakes' => $this->arr_stocktakes,'image_url_origin' => $this->v_url_image_link_origin,'thickness' => $this->v_thickness,'thickness_unit' => $this->v_thickness_unit,'under_over' => $this->v_under_over,'unit_price' => $this->v_unit_price,'update_price_by' => $this->v_update_price_by,'update_price_by_id' => $this->v_update_price_by_id,'update_price_date' => $this->v_update_price_date));
        try{
            $this->collection->update($arr_where, $arr, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function delete one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function delete(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->remove($arr_where, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_field($p_field, $p_value, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields array, array of selected fields go to updated
     * @param $arr_values array, array of selected values go to assigned
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function increase one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields = array(), array of selected fields go to updated
     * @param $arr_values = array(), array of selected values go to increase
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function draw option tag
     * @param $p_field_value string: name of field will be value option tag
     * @param $p_field_display string: name of field will be display text option tag
     * @param $p_selected_value mixed: value of field will be display text option tag
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_exclude array: array list value of exclude
     * @return string
     */
    public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
        $arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
        $v_dsp_option = '';
        foreach($arr as $a){
            if(!in_array($a[$p_field_value],$arr_exclude)){
                if($a[$p_field_value] == $p_selected_value)
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
                else
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
            }
        }
        return $v_dsp_option;
    }

    /**
     * function count all records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count(array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->count();
        else
            return $this->collection->find($arr_where)->count();
    }

    /**
     * function count all records
     * @param $p_field string: in field to count
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count_field($p_field, array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->find(array($p_field => array('$exists' => true)))->count();
        else
            return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
    }
}
?>