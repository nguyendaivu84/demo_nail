<?php
class cls_tb_design_template{

	private $v_template_id = 0;
	private $v_original_id = 0;
	private $v_template_name = '';
	private $v_template_width = 0;
	private $v_template_height = 0;
	private $v_folding_type = 0;
	private $v_folding_direction = 0;
	private $v_die_cut_type = 0;
	private $v_stock_cost = 0;
	private $v_markup_cost = 0;
	private $v_print_cost = 0;
	private $v_template_type = 0;
	private $v_template_status = 0;
	private $v_template_dpi = 0;
	private $v_template_bleed = 0;
	private $v_created_time = '0000-00-00 00:00:00';
	private $v_user_id = 0;
	private $v_user_name = '';
	private $v_is_already = 0;
	private $arr_product_list = array();
	private $v_location_id = 0;
	private $v_company_id = 0;
	private $v_template_desc = '';
	private $arr_template_tag = array();
	private $v_template_order = 0;
	private $v_template_group = 0;
	private $v_template_data = '';
	private $v_template_image = '';
	private $arr_template_color = array();
	private $v_saved_dir = '';
	private $v_sample_image = '';
	private $v_category_id = 0;
	private $v_section_id = 0;
	private $v_style_id = 0;
	private $v_industry_id = 0;
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
	
	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection('tb_design_template');
		$this->v_created_time = new MongoDate(time());
		$this->collection->ensureIndex(array("template_id"=>1), array('name'=>"template_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = 'tb_design_template';
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "template_id" value
	 * @return int value
	 */
	public function get_template_id(){
		return (int) $this->v_template_id;
	}

	
	/**
	 * function allow change properties "template_id" value
	 * @param $p_template_id: int value
	 */
	public function set_template_id($p_template_id){
		$this->v_template_id = (int) $p_template_id;
	}

    /**
     * function return properties "original_id" value
     * @return int value
     */
    public function get_original_id(){
        return (int) $this->v_original_id;
    }


    /**
     * function allow change properties "original_id" value
     * @param $p_original_id: int value
     */
    public function set_original_id($p_original_id){
        $this->v_original_id = (int) $p_original_id;
    }


    /**
	 * function return properties "template_name" value
	 * @return string value
	 */
	public function get_template_name(){
		return $this->v_template_name;
	}

	
	/**
	 * function allow change properties "template_name" value
	 * @param $p_template_name: string value
	 */
	public function set_template_name($p_template_name){
		$this->v_template_name = $p_template_name;
	}

	
	/**
	 * function return properties "template_width" value
	 * @return float value
	 */
	public function get_template_width(){
		return (float) $this->v_template_width;
	}

	
	/**
	 * function allow change properties "template_width" value
	 * @param $p_template_width: float value
	 */
	public function set_template_width($p_template_width){
		$this->v_template_width = (float) $p_template_width;
	}

	
	/**
	 * function return properties "template_height" value
	 * @return float value
	 */
	public function get_template_height(){
		return (float) $this->v_template_height;
	}

	
	/**
	 * function allow change properties "template_height" value
	 * @param $p_template_height: float value
	 */
	public function set_template_height($p_template_height){
		$this->v_template_height = (float) $p_template_height;
	}

	
	/**
	 * function return properties "folding_type" value
	 * @return int value
	 */
	public function get_folding_type(){
		return (int) $this->v_folding_type;
	}

	
	/**
	 * function allow change properties "folding_type" value
	 * @param $p_folding_type: int value
	 */
	public function set_folding_type($p_folding_type){
		$this->v_folding_type = (int) $p_folding_type;
	}

	
	/**
	 * function return properties "folding_direction" value
	 * @return int value
	 */
	public function get_folding_direction(){
		return (int) $this->v_folding_direction;
	}

	
	/**
	 * function allow change properties "folding_direction" value
	 * @param $p_folding_direction: int value
	 */
	public function set_folding_direction($p_folding_direction){
		$this->v_folding_direction = (int) $p_folding_direction;
	}

	
	/**
	 * function return properties "die_cut_type" value
	 * @return int value
	 */
	public function get_die_cut_type(){
		return (int) $this->v_die_cut_type;
	}

	
	/**
	 * function allow change properties "die_cut_type" value
	 * @param $p_die_cut_type: int value
	 */
	public function set_die_cut_type($p_die_cut_type){
		$this->v_die_cut_type = (int) $p_die_cut_type;
	}

	
	/**
	 * function return properties "stock_cost" value
	 * @return float value
	 */
	public function get_stock_cost(){
		return (float) $this->v_stock_cost;
	}

	
	/**
	 * function allow change properties "stock_cost" value
	 * @param $p_stock_cost: float value
	 */
	public function set_stock_cost($p_stock_cost){
		$this->v_stock_cost = (float) $p_stock_cost;
	}

	
	/**
	 * function return properties "markup_cost" value
	 * @return float value
	 */
	public function get_markup_cost(){
		return (float) $this->v_markup_cost;
	}

	
	/**
	 * function allow change properties "markup_cost" value
	 * @param $p_markup_cost: float value
	 */
	public function set_markup_cost($p_markup_cost){
		$this->v_markup_cost = (float) $p_markup_cost;
	}

	
	/**
	 * function return properties "print_cost" value
	 * @return float value
	 */
	public function get_print_cost(){
		return (float) $this->v_print_cost;
	}

	
	/**
	 * function allow change properties "print_cost" value
	 * @param $p_print_cost: float value
	 */
	public function set_print_cost($p_print_cost){
		$this->v_print_cost = (float) $p_print_cost;
	}

	
	/**
	 * function return properties "template_type" value
	 * @return int value
	 */
	public function get_template_type(){
		return (int) $this->v_template_type;
	}

	
	/**
	 * function allow change properties "template_type" value
	 * @param $p_template_type: int value
	 */
	public function set_template_type($p_template_type){
		$this->v_template_type = (int) $p_template_type;
	}

    /**
     * function return properties "template_dpi" value
     * @return int value
     */
    public function get_template_dpi(){
        return (int) $this->v_template_dpi;
    }


    /**
     * function allow change properties "template_dpi" value
     * @param $p_template_dpi: int value
     */
    public function set_template_dpi($p_template_dpi){
        $this->v_template_dpi = (int) $p_template_dpi;
    }


    /**
	 * function return properties "template_status" value
	 * @return int value
	 */
	public function get_template_status(){
		return (int) $this->v_template_status;
	}

	
	/**
	 * function allow change properties "template_status" value
	 * @param $p_template_status: int value
	 */
	public function set_template_status($p_template_status){
		$this->v_template_status = (int) $p_template_status;
	}

	
	/**
	 * function return properties "template_bleed" value
	 * @return float value
	 */
	public function get_template_bleed(){
		return (float) $this->v_template_bleed;
	}

	
	/**
	 * function allow change properties "template_bleed" value
	 * @param $p_template_bleed: float value
	 */
	public function set_template_bleed($p_template_bleed){
		$this->v_template_bleed = (float) $p_template_bleed;
	}

	
	/**
	 * function return properties "created_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_created_time(){
		return  $this->v_created_time->sec;
	}

	
	/**
	 * function allow change properties "created_time" value
	 * @param $p_created_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_created_time($p_created_time){
		if($p_created_time=='') $p_created_time = NULL;
		if(!is_null($p_created_time)){
			try{
				$this->v_created_time = new MongoDate(strtotime($p_created_time));
			}catch(MongoException $me){
				$this->v_created_time = NULL;
			}
		}else{
			$this->v_created_time = NULL;
		}
	}

	
	/**
	 * function return properties "user_id" value
	 * @return int value
	 */
	public function get_user_id(){
		return (int) $this->v_user_id;
	}

	
	/**
	 * function allow change properties "user_id" value
	 * @param $p_user_id: int value
	 */
	public function set_user_id($p_user_id){
		$this->v_user_id = (int) $p_user_id;
	}

	
	/**
	 * function return properties "user_name" value
	 * @return string value
	 */
	public function get_user_name(){
		return $this->v_user_name;
	}

	
	/**
	 * function allow change properties "user_name" value
	 * @param $p_user_name: string value
	 */
	public function set_user_name($p_user_name){
		$this->v_user_name = $p_user_name;
	}

	
	/**
	 * function return properties "is_already" value
	 * @return int value
	 */
	public function get_is_already(){
		return (int) $this->v_is_already;
	}

	
	/**
	 * function allow change properties "is_already" value
	 * @param $p_is_already: int value
	 */
	public function set_is_already($p_is_already){
		$this->v_is_already = (int) $p_is_already;
	}

	
	/**
	 * function return properties "product_list" value
	 * @return array value
	 */
	public function get_product_list(){
		return  $this->arr_product_list;
	}

	
	/**
	 * function allow change properties "product_list" value
	 * @param $arr_product_list array
	 */
	public function set_product_list(array $arr_product_list = array()){
		$this->arr_product_list = $arr_product_list;
	}

	
	/**
	 * function return properties "location_id" value
	 * @return int value
	 */
	public function get_location_id(){
		return (int) $this->v_location_id;
	}

	
	/**
	 * function allow change properties "location_id" value
	 * @param $p_location_id: int value
	 */
	public function set_location_id($p_location_id){
		$this->v_location_id = (int) $p_location_id;
	}

	
	/**
	 * function return properties "company_id" value
	 * @return int value
	 */
	public function get_company_id(){
		return (int) $this->v_company_id;
	}

	
	/**
	 * function allow change properties "company_id" value
	 * @param $p_company_id: int value
	 */
	public function set_company_id($p_company_id){
		$this->v_company_id = (int) $p_company_id;
	}

	
	/**
	 * function return properties "template_desc" value
	 * @return string value
	 */
	public function get_template_desc(){
		return $this->v_template_desc;
	}

	
	/**
	 * function allow change properties "template_desc" value
	 * @param $p_template_desc: string value
	 */
	public function set_template_desc($p_template_desc){
		$this->v_template_desc = $p_template_desc;
	}

	
	/**
	 * function return properties "template_tag" value
	 * @return array value
	 */
	public function get_template_tag(){
		return  $this->arr_template_tag;
	}

	
	/**
	 * function allow change properties "template_tag" value
	 * @param $arr_template_tag array
	 */
	public function set_template_tag(array $arr_template_tag = array()){
		$this->arr_template_tag = $arr_template_tag;
	}

	
	/**
	 * function return properties "template_order" value
	 * @return int value
	 */
	public function get_template_order(){
		return (int) $this->v_template_order;
	}

	
	/**
	 * function allow change properties "template_order" value
	 * @param $p_template_order: int value
	 */
	public function set_template_order($p_template_order){
		$this->v_template_order = (int) $p_template_order;
	}

	
	/**
	 * function return properties "template_group" value
	 * @return int value
	 */
	public function get_template_group(){
		return (int) $this->v_template_group;
	}

	
	/**
	 * function allow change properties "template_group" value
	 * @param $p_template_group: int value
	 */
	public function set_template_group($p_template_group){
		$this->v_template_group = (int) $p_template_group;
	}

	
	/**
	 * function return properties "template_data" value
	 * @return string value
	 */
	public function get_template_data(){
		return $this->v_template_data;
	}

	
	/**
	 * function allow change properties "template_data" value
	 * @param $p_template_data: string value
	 */
	public function set_template_data($p_template_data){
		$this->v_template_data = $p_template_data;
	}

	
	/**
	 * function return properties "template_image" value
	 * @return string value
	 */
	public function get_template_image(){
		return $this->v_template_image;
	}

	
	/**
	 * function allow change properties "template_image" value
	 * @param $p_template_image: string value
	 */
	public function set_template_image($p_template_image){
		$this->v_template_image = $p_template_image;
	}

	
	/**
	 * function return properties "template_color" value
	 * @return array value
	 */
	public function get_template_color(){
		return  $this->arr_template_color;
	}

	
	/**
	 * function allow change properties "template_color" value
	 * @param $arr_template_color array
	 */
	public function set_template_color(array $arr_template_color = array()){
		$this->arr_template_color = $arr_template_color;
	}

	
	/**
	 * function return properties "saved_dir" value
	 * @return string value
	 */
	public function get_saved_dir(){
		return $this->v_saved_dir;
	}

	
	/**
	 * function allow change properties "saved_dir" value
	 * @param $p_saved_dir: string value
	 */
	public function set_saved_dir($p_saved_dir){
		$this->v_saved_dir = $p_saved_dir;
	}

	
	/**
	 * function return properties "sample_image" value
	 * @return string value
	 */
	public function get_sample_image(){
		return $this->v_sample_image;
	}

	
	/**
	 * function allow change properties "sample_image" value
	 * @param $p_sample_image: string value
	 */
	public function set_sample_image($p_sample_image){
		$this->v_sample_image = $p_sample_image;
	}

	
	/**
	 * function return properties "category_id" value
	 * @return int value
	 */
	public function get_category_id(){
		return (int) $this->v_category_id;
	}

	
	/**
	 * function allow change properties "category_id" value
	 * @param $p_category_id: int value
	 */
	public function set_category_id($p_category_id){
		$this->v_category_id = (int) $p_category_id;
	}

	
	/**
	 * function return properties "section_id" value
	 * @return int value
	 */
	public function get_section_id(){
		return (int) $this->v_section_id;
	}

	
	/**
	 * function allow change properties "section_id" value
	 * @param $p_section_id: int value
	 */
	public function set_section_id($p_section_id){
		$this->v_section_id = (int) $p_section_id;
	}

	
	/**
	 * function return properties "style_id" value
	 * @return int value
	 */
	public function get_style_id(){
		return (int) $this->v_style_id;
	}

	
	/**
	 * function allow change properties "style_id" value
	 * @param $p_style_id: int value
	 */
	public function set_style_id($p_style_id){
		$this->v_style_id = (int) $p_style_id;
	}

	
	/**
	 * function return properties "industry_id" value
	 * @return int value
	 */
	public function get_industry_id(){
		return (int) $this->v_industry_id;
	}

	
	/**
	 * function allow change properties "industry_id" value
	 * @param $p_industry_id: int value
	 */
	public function set_industry_id($p_industry_id){
		$this->v_industry_id = (int) $p_industry_id;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
		$arr = array('template_id' => $this->v_template_id
					,'template_name' => $this->v_template_name
					,'template_width' => $this->v_template_width
					,'template_height' => $this->v_template_height
					,'folding_type' => $this->v_folding_type
					,'folding_direction' => $this->v_folding_direction
					,'die_cut_type' => $this->v_die_cut_type
					,'stock_cost' => $this->v_stock_cost
					,'markup_cost' => $this->v_markup_cost
					,'print_cost' => $this->v_print_cost
					,'template_type' => $this->v_template_type
					,'template_status' => $this->v_template_status
					,'template_dpi' => $this->v_template_dpi
					,'template_bleed' => $this->v_template_bleed
					,'created_time' => $this->v_created_time
					,'user_id' => $this->v_user_id
					,'user_name' => $this->v_user_name
					,'is_already' => $this->v_is_already
					,'product_list' => $this->arr_product_list
					,'location_id' => $this->v_location_id
					,'company_id' => $this->v_company_id
					,'template_desc' => $this->v_template_desc
					,'template_tag' => $this->arr_template_tag
					,'template_order' => $this->v_template_order
					,'template_group' => $this->v_template_group
					,'template_data' => $this->v_template_data
					,'template_image' => $this->v_template_image
					,'template_color' => $this->arr_template_color
					,'saved_dir' => $this->v_saved_dir
					,'sample_image' => $this->v_sample_image
					,'category_id' => $this->v_category_id
					,'section_id' => $this->v_section_id
					,'style_id' => $this->v_style_id
					,'original_id' => $this->v_original_id
					,'industry_id' => $this->v_industry_id);
		try{
			$this->collection->insert($arr, array('safe'=>true));
			$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_design_template` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
			$this->v_template_name = isset($arr['template_name'])?$arr['template_name']:'';
			$this->v_template_width = isset($arr['template_width'])?$arr['template_width']:0;
			$this->v_template_height = isset($arr['template_height'])?$arr['template_height']:0;
			$this->v_folding_type = isset($arr['folding_type'])?$arr['folding_type']:0;
			$this->v_folding_direction = isset($arr['folding_direction'])?$arr['folding_direction']:0;
			$this->v_die_cut_type = isset($arr['die_cut_type'])?$arr['die_cut_type']:0;
			$this->v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
			$this->v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
			$this->v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
			$this->v_template_type = isset($arr['template_type'])?$arr['template_type']:0;
			$this->v_template_status = isset($arr['template_status'])?$arr['template_status']:0;
			$this->v_template_dpi = isset($arr['template_dpi'])?$arr['template_dpi']:72;
			$this->v_template_bleed = isset($arr['template_bleed'])?$arr['template_bleed']:0;
			$this->v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
			$this->v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
			$this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
			$this->v_is_already = isset($arr['is_already'])?$arr['is_already']:0;
			$this->arr_product_list = isset($arr['product_list'])?$arr['product_list']:array();
			$this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
			$this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
			$this->v_template_desc = isset($arr['template_desc'])?$arr['template_desc']:'';
			$this->arr_template_tag = isset($arr['template_tag'])?$arr['template_tag']:array();
			$this->v_template_order = isset($arr['template_order'])?$arr['template_order']:0;
			$this->v_template_group = isset($arr['template_group'])?$arr['template_group']:0;
			$this->v_template_data = isset($arr['template_data'])?$arr['template_data']:'';
			$this->v_template_image = isset($arr['template_image'])?$arr['template_image']:'';
			$this->arr_template_color = isset($arr['template_color'])?$arr['template_color']:array();
			$this->v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
			$this->v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
			$this->v_category_id = isset($arr['category_id'])?$arr['category_id']:0;
			$this->v_section_id = isset($arr['section_id'])?$arr['section_id']:0;
			$this->v_style_id = isset($arr['style_id'])?$arr['style_id']:0;
			$this->v_original_id = isset($arr['original_id'])?$arr['original_id']:0;
			$this->v_industry_id = isset($arr['industry_id'])?$arr['industry_id']:0;
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `template_id` FROM `tb_design_template` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select_scalar('template_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `template_id` FROM `tb_design_template` WHERE `user_id`=2 ORDER BY `template_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select_next('template_id',array('user_id'=>2), array('template_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return ((int) $v_ret)+1;
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
	
	/**
	 * function select distinct
	 * @param $p_field_name string, name of selected field
	 * @example: 
	 * <code>
	 *         SELECT DISTINCT `name` FROM `tbl_users`
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select_distinct('nam')
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_distinct($p_field_name){
		return $this->collection->command(array("distinct"=>"tb_design_template", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_template($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('template_id' => $this->v_template_id,'template_name' => $this->v_template_name,'template_width' => $this->v_template_width,'template_height' => $this->v_template_height,'folding_type' => $this->v_folding_type,'folding_direction' => $this->v_folding_direction,'die_cut_type' => $this->v_die_cut_type,'stock_cost' => $this->v_stock_cost,'markup_cost' => $this->v_markup_cost,'print_cost' => $this->v_print_cost,'template_type' => $this->v_template_type,'template_status' => $this->v_template_status,'template_bleed' => $this->v_template_bleed,'created_time' => $this->v_created_time,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'is_already' => $this->v_is_already,'product_list' => $this->arr_product_list,'location_id' => $this->v_location_id,'company_id' => $this->v_company_id,'template_desc' => $this->v_template_desc,'template_tag' => $this->arr_template_tag,'template_order' => $this->v_template_order,'template_group' => $this->v_template_group,'template_data' => $this->v_template_data,'template_image' => $this->v_template_image,'template_color' => $this->arr_template_color,'saved_dir' => $this->v_saved_dir,'sample_image' => $this->v_sample_image,'category_id' => $this->v_category_id,'section_id' => $this->v_section_id,'style_id' => $this->v_style_id,'industry_id' => $this->v_industry_id,'original_id' => $this->v_original_id, 'template_dpi'=>$this->v_template_dpi));
		 else 
			$arr = array('$set' => array('template_name' => $this->v_template_name,'template_width' => $this->v_template_width,'template_height' => $this->v_template_height,'folding_type' => $this->v_folding_type,'folding_direction' => $this->v_folding_direction,'die_cut_type' => $this->v_die_cut_type,'stock_cost' => $this->v_stock_cost,'markup_cost' => $this->v_markup_cost,'print_cost' => $this->v_print_cost,'template_type' => $this->v_template_type,'template_status' => $this->v_template_status,'template_bleed' => $this->v_template_bleed,'created_time' => $this->v_created_time,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'is_already' => $this->v_is_already,'product_list' => $this->arr_product_list,'location_id' => $this->v_location_id,'company_id' => $this->v_company_id,'template_desc' => $this->v_template_desc,'template_tag' => $this->arr_template_tag,'template_order' => $this->v_template_order,'template_group' => $this->v_template_group,'template_data' => $this->v_template_data,'template_image' => $this->v_template_image,'template_color' => $this->arr_template_color,'saved_dir' => $this->v_saved_dir,'sample_image' => $this->v_sample_image,'category_id' => $this->v_category_id,'section_id' => $this->v_section_id,'style_id' => $this->v_style_id,'industry_id' => $this->v_industry_id,'original_id' => $this->v_original_id, 'template_dpi'=>$this->v_template_dpi));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>