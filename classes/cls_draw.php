<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ThanhHai
 * Date: 14/06/2013
 * Time: 00:05
 * To change this template use File | Settings | File Templates.
 */
class cls_draw{
    private $v_width = 200;
    private $v_height = 200;
    private $v_x_pos = 0;
    private $v_y_pos = 0;
    private $v_fill_color;
    private $v_stroke_color;
    private $v_stroke_style = 'solid';
    private $v_stroke_width = 0.0;
    private $v_rotation = 0.0;
    private $v_transparent;
    private $v_output_format = 'png';

    public function __construct(){
        $this->v_transparent = new ImagickPixel('transparent');
        $this->v_fill_color = new ImagickPixel('transparent');
        $this->v_stroke_color = new ImagickPixel('black');
    }
    /**
     * @param float $p_width
     * set width image
     */
    public function set_width($p_width){
        $this->v_width = $p_width;
    }

    /**
     * @param $p_x_pos
     */
    public function set_x_pos($p_x_pos){
        $this->v_x_pos = $p_x_pos;
    }

    /**
     * @param $p_y_pos
     */
    public function set_y_pos($p_y_pos){
        $this->v_y_pos = $p_y_pos;
    }

    /**
     * @param float $p_height
     * set height image
     */
    public function set_height($p_height){
        $this->v_height = $p_height;
    }

    /**
     * @param string $p_fill_color
     * set fill color
     */
    public function set_fill_color($p_fill_color = 'white'){
        $this->v_fill_color = new ImagickPixel($p_fill_color);
    }

    /**
     * @param string $p_stroke_color
     * set stroke color
     */
    public function set_stroke_color($p_stroke_color = 'black'){
        $this->v_stroke_color = new ImagickPixel($p_stroke_color);
    }

    /**
     * @param string $p_stroke_style
     * set style of stroke
     */
    public function set_stroke_style($p_stroke_style = 'solid'){
        $this->v_stroke_style = $p_stroke_style;
    }

    /**
     * @param float $p_stroke_width
     * set width of stroke
     */
    public function set_stroke_width($p_stroke_width = 0.0){
        $this->v_stroke_width = $p_stroke_width;
    }

    /**
     * @param float $p_rotation
     * set rotation in degree
     */
    public function set_rotation($p_rotation = 0.0){
        $this->v_rotation = $p_rotation;
    }

    /**
     * @param string $p_output_format
     * set output format
     */
    public function set_output_format($p_output_format='png'){
        $this->v_output_format = $p_output_format;
    }

    /**
     * @param ImagickDraw $draw
     * @param $p_border_stroke
     * @param $p_border_width
     * @return array
     */
    function create_line_style(ImagickDraw &$draw, $p_border_stroke, $p_border_width){
        $arr_style = array();
        if($p_border_stroke=='solid'){
        }else if($p_border_stroke=='round_dot'){
            $draw->setstrokelinecap(Imagick::LINECAP_ROUND);
            if($p_border_width*2==20) $p_border_width+=1;
            $draw->setstrokedasharray(array($p_border_width/20, $p_border_width*2));
        }else if($p_border_stroke=='square_dot'){
            $draw->setstrokedasharray(array($p_border_width , $p_border_width));
        }else if($p_border_stroke=='dash'){
            $draw->setstrokedasharray(array($p_border_width*6 , $p_border_width*2));
        }else if($p_border_stroke=='dash_dot'){
            $draw->setstrokedasharray(array($p_border_width*6 , $p_border_width*2, $p_border_width*2, $p_border_width*2));
        }else if($p_border_stroke=='long_dash'){
            $draw->setstrokedasharray(array($p_border_width*8 , $p_border_width*2));
        }else if($p_border_stroke=='long_dash_dot'){
            $draw->setstrokedasharray(array($p_border_width*8 , $p_border_width*2, $p_border_width*4, $p_border_width*2));
        }
        return $arr_style;
    }
    /**
     * @param Imagick $image
     * @param ImagickDraw $draw
     * @param $p_text
     * @param $p_max_width
     * @return array
     */
    public function word_wrap_annotation(Imagick &$image,ImagickDraw &$draw, $p_text, $p_max_width)
    {
        $p_text .=' ';
        $arr_words = explode(" ", $p_text);
        $arr_lines = array();
        $i = 0;
        $v_line_height = 0;
        while($i < count($arr_words) )
        {
            $v_current_line = $arr_words[$i];
            if($i+1 >= count($arr_words))
            {
                $arr_lines[] = $v_current_line;
                break;
            }
            $arr_metrics = $image->queryfontmetrics($draw, $v_current_line . ' ' . $arr_words[$i+1]);

            while($arr_metrics['textWidth'] <= $p_max_width)
            {
                $v_current_line .= ' ' . $arr_words[++$i];
                if($i+1 >= count($arr_words))
                    break;
                $arr_metrics = $image->queryfontmetrics($draw, $v_current_line . ' ' . $arr_words[$i+1]);
            }
            $arr_lines[] = $v_current_line;
            $i++;
            if($arr_metrics['textHeight'] > $v_line_height)
                $v_line_height = $arr_metrics['textHeight'];
        }
        return array($arr_lines, $v_line_height);
    }

    /**
     * @param Imagick $image
     * @param string $p_text
     * @param string $p_gravity
     * @param string $p_font
     * @param float $p_font_point
     * @param string $p_bullet_style
     */
    public function text(Imagick & $image, $p_text = '', $p_gravity = 'west', $p_font = 'Arial', $p_font_point = 15.0, $p_bullet_style = 'undefined'){
        $arr_bullet_style = array(
            'undefined'=>''
            ,'black_circle'=>"&#9679; "
            ,'white_circle'=>"&#9675; "
            ,'white_square'=>"&#9633; "
            ,'black_square'=>"&#9632; "
            ,'dash'=>"&ndash; "
            ,'number_dot'=>". "
            ,'number_dash'=>"- "
            ,'number_parenth'=>") "
        );
        if(!isset($arr_bullet_style[$p_bullet_style])) $p_bullet_style = 'undefined';
        $arr_gravity = array(
            'west'=>Imagick::ALIGN_LEFT
            ,'center'=>Imagick::ALIGN_CENTER
            ,'east'=>Imagick::ALIGN_RIGHT
        );
        if(!isset($arr_gravity[$p_gravity])) $p_gravity = 'west';

        $draw = new ImagickDraw();
        $draw->setfillcolor($this->v_fill_color);
        $draw->setfontsize($p_font_point);
        $draw->setfont($p_font);
        //$draw->settextencoding('unicode');
        $draw->setgravity($arr_gravity[$p_gravity]);

        $p_text = html_entity_decode($p_text);
        $p_text = urldecode($p_text);

        $arr_body = explode("\n", $p_text);
        $v_tmp_line_height = 0;
        $arr_lines = array();

        for($i=0; $i<count($arr_body);$i++){
            if($arr_bullet_style[$p_bullet_style]!=''){
                if(strpos($p_bullet_style,'number')===false){
                    $arr_body[$i] = $arr_bullet_style[$p_bullet_style].$arr_body[$i];
                }else{
                    $arr_body[$i] = ($i+1).$arr_bullet_style[$p_bullet_style].$arr_body[$i];
                }
            }
            //$arr_body[$i] = html_entity_decode($arr_body[$i]);
            //$arr_body[$i] = urldecode($arr_body[$i]);
            list($arr_tmp_lines, $v_line_height) = $this->word_wrap_annotation($image, $draw, $arr_body[$i], $this->v_width);
            $v_tmp_line_height = $v_line_height;
            for($j=0;$j<count($arr_tmp_lines);$j++)
                $arr_lines[]=$arr_tmp_lines[$j];
        }

        $v_height = $v_tmp_line_height * count($arr_lines);
        $image->newimage($this->v_width, $v_height, $this->v_transparent);
        $v_x_pos = 0;

        if($this->v_width>0 && $v_height>0){
            for($i = 0; $i < count($arr_lines); $i++){
                $v_y_pos = $i*$v_tmp_line_height;
                $v_line = trim($arr_lines[$i]);
                if($v_line!='') $image->annotateimage($draw, $v_x_pos, $v_y_pos, 0, $v_line);
            }
        }
        if($this->v_rotation!=0.0)
            $image->rotateimage($this->v_transparent, $this->v_rotation);

        $draw->clear();
        $draw->destroy();
    }

    /**
     * @param Imagick $image
     * @param string $p_text
     * @param string $p_gravity
     * @param string $p_font
     * @param float $p_font_point
     * @param string $p_bullet_style
     */
    public function add_text(Imagick & $image, $p_text = '', $p_gravity = 'west', $p_font = 'Arial', $p_font_point = 15.0, $p_bullet_style = 'undefined'){
        $tmp_image = new Imagick();
        $this->text($tmp_image, $p_text, $p_gravity, $p_font, $p_font_point, $p_bullet_style);
        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }

    /**
     * @param Imagick $image
     * @param string $p_shape_type
     */
    public function add_shape(Imagick & $image, $p_shape_type = 'hline'){
        $tmp_image = new Imagick();
        $tmp_image->newimage($this->v_width, $this->v_height, $this->v_transparent);
        if(in_array($p_shape_type, array('hline', 'vline'))){
            $this->line($tmp_image, $p_shape_type);
        }else if($p_shape_type=='triangle')
            $this->triangle($tmp_image);
        else if($p_shape_type=='circle')
            $this->ellipse($tmp_image);
        else
            $this->rectangle($tmp_image);

        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }

    /**
     * @param Imagick $image
     * @param $p_full_path
     * @param float $p_crop_left
     * @param float $p_crop_top
     * @param float $p_crop_width
     * @param float $p_crop_height
     */
    public function add_image(Imagick & $image, $p_full_path, $p_crop_left = 0.0, $p_crop_top = 0.0, $p_crop_width=0.0, $p_crop_height=0.0){
        $tmp_image = new Imagick();
        $tmp_image->readimage($p_full_path);
        if($this->v_rotation) $tmp_image->rotateimage($this->v_transparent, $this->v_rotation);
        $v_width = $tmp_image->getimagewidth();
        $v_height = $tmp_image->getimageheight();
        $v_new_width = ceil($p_crop_width*$v_width);
        $v_new_height = ceil($p_crop_height*$v_height);
        $v_crop_left = ceil($p_crop_left*$v_width);
        $v_crop_top = ceil($p_crop_top*$v_height);
        $tmp_image->cropimage($v_new_width, $v_new_height, $v_crop_left, $v_crop_top);
        $tmp_image->resizeimage($this->v_width, $this->v_height, Imagick::FILTER_LANCZOS, 1);

        if($this->v_stroke_width>0){
            $draw = new ImagickDraw();
            $draw->setstrokecolor($this->v_stroke_color);
            $draw->setstrokewidth($this->v_stroke_width);
            $draw->setfillcolor($this->v_transparent);
            $draw->rectangle($this->v_stroke_width/2,$this->v_stroke_width/2,$this->v_width - $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);
            $tmp_image->drawimage($draw);
            $draw->clear();
            $draw->destroy();
        }
        //Fix image profile
        $arr_profiles = $tmp_image->getimageprofiles('*', false);
        $v_has_icc = (array_search('icc', $arr_profiles) !== false);
        if($v_has_icc){
            //$icc_cmyk = file_get_contents(ROOT_DIR.DS.'tools'.DS.'data'.'/USWebUncoated.icc');
            //$image->profileImage('icc', $icc_cmyk);
            //unset($icc_cmyk);
            $icc_rgb = file_get_contents(DESIGN_DATA_DIR.DS.'sRGB_v4_ICC_preference.icc');
            $tmp_image->profileimage('icc', $icc_rgb);
            unset($icc_rgb);
        }

        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }

    /**
     * @param Imagick $image
     * @param string $p_direction
     */
    public function line(Imagick & $image, $p_direction='vline'){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);

        $v_x1 = $p_direction=='vline'?$this->v_width/2:0;
        $v_x2 = $p_direction=='vline'?$this->v_width/2:$this->v_width;
        $v_y1 = $p_direction=='vline'?0:$this->v_height/2;
        $v_y2 = $p_direction=='vline'?$this->v_height:$this->v_height/2;
        $draw->setfillcolor($this->v_transparent);
        $draw->line($v_x1, $v_y1, $v_x2, $v_y2);
        $image->drawimage($draw);
        $draw->clear();
        $draw->destroy();
    }

    /**
     * @param Imagick $image
     */
    public function triangle(Imagick & $image){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);

        $draw->setfillcolor($this->v_fill_color);
        $arr_coords = array(
            array('x'=>$this->v_width/2 - $this->v_stroke_width/2, 'y'=>$this->v_stroke_width/2),
            array('x'=>$this->v_stroke_width/2, 'y'=>$this->v_height - $this->v_stroke_width/2),
            array('x'=>$this->v_width - $this->v_stroke_width/2, 'y'=>$this->v_height - $this->v_stroke_width/2)
        );
        $draw->polygon($arr_coords);

        $image->drawimage($draw);
    }
    /**
     * @param Imagick $image
     */
    public function ellipse(Imagick & $image){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);

        $draw1 = new ImagickDraw();

        if($this->v_stroke_style=='round_dot'){
            $draw1->setfillcolor($this->v_fill_color);
            $draw1->ellipse($this->v_width/2, $this->v_height/2, $this->v_width/2 - $this->v_stroke_width/2, $this->v_height/2-$this->v_stroke_width/2, 0, 360);
            $draw->setfillcolor($this->v_transparent);
            $draw->ellipse($this->v_width/2, $this->v_height/2, $this->v_width/2 - $this->v_stroke_width/2, $this->v_height/2-$this->v_stroke_width/2, 130, 310);
        }else{
            $draw->setfillcolor($this->v_fill_color);
            $draw->ellipse($this->v_width/2, $this->v_height/2, $this->v_width/2 - $this->v_stroke_width/2, $this->v_height/2-$this->v_stroke_width/2, 0, 360);
        }

        if($this->v_stroke_style=='round_dot'){
            $image->drawimage($draw1);
        }
        $image->drawimage($draw);
        if($this->v_stroke_style=='round_dot'){
            $image->rotateimage($this->v_transparent, 180);
            $image->drawimage($draw);
        }
        $draw1->clear();
        $draw1->destroy();
        $draw->clear();
        $draw->destroy();
    }

    public function create_die_cut(Imagick & $image, $p_distance, $p_die_type = 'circle', $p_page = 0){
        $v_width = $this->v_width;
        $v_height = $this->v_height;
        //$p_distance = 0;
        $v_stroke_color = $this->v_stroke_color;
        $draw = new ImagickDraw();
        $draw->setfillcolor($this->v_transparent);
        $draw->setstrokecolor($v_stroke_color);
        $v_line = 30;

        switch($p_die_type){
            case 'circle':
                $draw->ellipse($v_width/2 + $p_distance , $v_height/2 +$p_distance , $v_width/2 , $v_height/2 , 0, 360);
                break;
            case 'roundedCorners':
                $draw->roundrectangle($p_distance, $p_distance, $v_width +$p_distance, $v_height+$p_distance , 5, 5);
                break;
            case 'halfCircleSide':

                $v_draw_height = $v_height;// - 2*$p_distance;
                $v_radius = $v_draw_height/2;
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_radius, $p_distance);
                $draw->line($p_distance, $v_height + $p_distance, $v_width + $p_distance - $v_radius, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance - $v_radius, $p_distance + $v_radius , $v_radius, $v_radius, -90, 90);

                /*
                $v_draw_height = $v_height - 2*$p_distance;
                $v_radius = $v_draw_height/2;
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_radius, $p_distance);
                $draw->line($p_distance, $v_height + $p_distance, $v_width + $p_distance - $v_radius, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance - $v_radius, $p_distance + $v_radius , $v_radius, $v_radius, -90, 90);
                */
                break;
            case 'leaf':
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance - $v_line);
                $draw->line($p_distance + $v_line, $v_height + $p_distance, $v_width + $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_line, $p_distance);
                $draw->line($v_width + $p_distance, $p_distance + $v_line, $v_width + $p_distance, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance-$v_line, $p_distance+$v_line, $v_line, $v_line, -90, 0);
                $draw->ellipse($p_distance + $v_line, $v_height + $p_distance - $v_line, $v_line, $v_line, 90, 180);

                break;
            case 'roundedSingleCorner':
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $v_height + $p_distance, $v_width + $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_line, $p_distance);
                $draw->line($v_width + $p_distance, $p_distance + $v_line, $v_width + $p_distance, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance-$v_line, $p_distance+$v_line, $v_line, $v_line, -90, 0);

                break;
            default:
                $draw->rectangle($p_distance, $p_distance, $v_width + $p_distance, $v_height+$p_distance);
                break;
        }
        $image->drawimage($draw);
        if($p_page==1) $image->rotateimage($this->v_transparent, 180);

        $v_stroke_color->clear();
        $v_stroke_color->destroy();
        $draw->clear();
        $draw->destroy();
    }

    public function rectangle(Imagick & $image){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);
        $draw1 = new ImagickDraw();
        if($this->v_stroke_style=='round_dot'){
            $draw1->setfillcolor($this->v_fill_color);
            $draw1->rectangle($this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width- $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);
            $draw->setfillcolor($this->v_transparent);
            $draw->line($this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width - $this->v_stroke_width/2, $this->v_stroke_width/2);//line 1
            $draw->line($this->v_width - $this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width - $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);//line 2
            $draw->line($this->v_width - $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2, $this->v_stroke_width/2 , $this->v_height - $this->v_stroke_width/2);//line 3
            $draw->line($this->v_stroke_width/2 , $this->v_height - $this->v_stroke_width/2, $this->v_stroke_width/2 , $this->v_stroke_width/2);//line 4
        }else{
            $draw->setfillcolor($this->v_fill_color);
            $draw->rectangle($this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width- $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);
        }
        if($this->v_stroke_style=='round_dot'){
            $image->drawimage($draw1);
        }


        $image->drawimage($draw);
        if($this->v_stroke_style=='round_dot'){
            $image->rotateimage($this->v_transparent, 180);
            $image->drawimage($draw);
        }
        $draw1->clear();
        $draw1->destroy();
        $draw->clear();
        $draw->destroy();

    }

    /**
     * @param $p_filename
     * @param $p_rotation
     * @param float $p_crop_left
     * @param float $p_crop_top
     * @param float $p_crop_right
     * @param float $p_crop_bottom
     * @return Imagick
     */
    public function image($p_filename, $p_rotation, $p_crop_left = 0.0 ,$p_crop_top = 0.0, $p_crop_right = 0.0, $p_crop_bottom = 0.0){
        $image = new Imagick($p_filename);
        $v_width = $this->v_width;
        $v_height = $this->v_height;
        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
        $image->resizeimage($v_width, $v_height, Imagick::FILTER_LANCZOS, 1, true);
        if($p_crop_bottom!=0 || $p_crop_left!=0 || $p_crop_right!=0 || $p_crop_top!=0){
            $v_width = $this->v_width - $p_crop_left - $p_crop_right;
            $v_height = $this->v_height - $p_crop_top - $p_crop_bottom;
            $image->cropimage($v_width, $v_height, $p_crop_left, $p_crop_top);
        }
        if($this->v_stroke_width>0){
            $draw = new ImagickDraw();
            $draw->setfillcolor($this->v_transparent);
            $draw->setstrokecolor($this->v_stroke_color);
            $draw->rectangle($this->v_stroke_width/2, $this->v_stroke_width/2, $v_width- $this->v_stroke_width/2, $v_height - $this->v_stroke_width/2);
            $image->drawimage($draw);
            $draw->clear();
            $draw->destroy();
        }
        return $image;
    }

    public function create_thumb(Imagick &$image, $p_file, $p_thumb_width=200){
        $image->readimage($p_file);
        $image->setcompression(Imagick::COMPRESSION_JPEG);
        $image->setcompressionquality(75);
        $image->resizeimage($p_thumb_width, 0, Imagick::FILTER_LANCZOS,1);
    }

    public function create_thumb_gd($p_file, $p_thumb_width=200){
        list($width, $height, $type) = @getimagesize($p_file);
        $v_percent = $p_thumb_width / $width;
        $v_thumb_height = floor($height * $v_percent);
        switch($type){
            case 1;//Gif
                $im_source = @imagecreatefromgif($p_file);
                break;
            case 2;//Jpg
                $im_source = @imagecreatefromjpeg($p_file);
                break;
            case 3;//Png
                $im_source = @imagecreatefrompng($p_file);
                break;
            default ;
                $im_source = @imagecreatefromjpeg($p_file);
                break;
        }
        $tmp_img = imagecreatetruecolor( $p_thumb_width, $v_thumb_height );
        imagecopyresized($tmp_img, $im_source, 0, 0, 0, 0, $p_thumb_width, $v_thumb_height, $width, $height );
        return $tmp_img;
    }
    /**
     * @param array $arr_array
     * @param $p_field
     * @param bool $p_reverse
     * @return array
     */
    public function record_sort(array $arr_array, $p_field, $p_reverse=false){
        $arr_hash = array();
        foreach($arr_array as $arr){
            $arr_hash[$arr[$p_field]] = $arr;
        }
        ($p_reverse)? krsort($arr_hash) : ksort($arr_hash);
        $arr_return = array();
        foreach($arr_hash as $arr){
            $arr_return []= $arr;
        }
        return $arr_return;
    }

    public function gradient(Imagick & $image, $p_gradient_string = 'gradient:white-lightgray', $p_rotation = 90){
        $tmp_image = new Imagick();
        $tmp_image->newpseudoimage($this->v_width, $this->v_height, $p_gradient_string);
        $tmp_image->rotateimage($this->v_transparent, $p_rotation);

        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);

        $tmp_image->clear();
        $tmp_image->destroy();
    }

    public function __destroy(){
        if($this->v_transparent){
            $this->v_transparent->clear();
            $this->v_transparent->destroy();
        }
        if($this->v_fill_color){
            $this->v_fill_color->clear();
            $this->v_fill_color->destroy();
        }
        if($this->v_stroke_color){
            $this->v_stroke_color->clear();
            $this->v_stroke_color->destroy();
        }
    }

    public function shadow_image_file($p_image_file, $p_width = 0){
        $im = new Imagick($p_image_file);
        return $this->shadow_image($im, $p_width);
    }

    public function shadow_image(Imagick $image, $p_width = 0){
        $v_image_width = $image->getimagewidth();
        if($p_width<$v_image_width && $p_width > 20)
            $image->thumbnailimage($p_width, null);
        $shadow = $image->getimage();
        $shadow->setimagebackgroundcolor(new ImagickPixel('black'));
        $shadow->shadowimage(80, 3, 5, 5);
        $shadow->compositeimage($image, Imagick::COMPOSITE_OVER, 0, 0);
        $image->clear();
        $image->destroy();
        return $shadow;
    }

    public function create_sample(Imagick $image1,Imagick $image2, $p_size = 200){
        $im = new Imagick();
        $im->newimage($p_size, $p_size, new ImagickPixel('transparent'));
        $v_image_width = $image1->getimagewidth();
        $v_image_height = $image1->getimageheight();

        if($v_image_width>$v_image_height){
            $v_new_height = .6*$p_size;
            $v_new_width = ($v_new_height/$v_image_height)*$v_image_width;
            $shadow1 = $this->shadow_image($image1, $v_new_width);
            $shadow2 = $this->shadow_image($image2, $v_new_width);
            $shadow2->rotateimage($this->v_transparent, 15);
            $im->compositeimage($shadow2, Imagick::COMPOSITE_OVER,0, $v_new_height - .4*$p_size);
            $im->compositeimage($shadow1, Imagick::COMPOSITE_OVER,0, 0);
        }else{
            $v_new_width = .6*$p_size;
            $shadow1 = $this->shadow_image($image1, $v_new_width);
            $shadow2 = $this->shadow_image($image2, $v_new_width);
            $shadow2->rotateimage($this->v_transparent, 15);
            $im->compositeimage($shadow2, Imagick::COMPOSITE_OVER,.4*$p_size, 0);
            $im->compositeimage($shadow1, Imagick::COMPOSITE_OVER,0, 0);
        }
        $shadow1->clear();
        $shadow1->destroy();
        $shadow2->clear();
        $shadow2->destroy();
        return $im;
    }

    public function create_sample_file($p_summary_file, $p_image1, $p_image2, $p_size = 200, $p_delete_source = true){
        $image1 = new Imagick($p_image1);
        $image2 = new Imagick($p_image2);
        $image = $this->create_sample($image1, $image2, $p_size);
        if($p_delete_source){
            @unlink($p_image1);
            @unlink($p_image2);
        }
        $image->setimageformat('png');
        $image->writeimage($p_summary_file);
        $image->clear();
        $image->destroy();
    }

    public function create_preview(Imagick &$image, cls_tb_design_image $cls_image, array $arr_json, $p_dpi, $p_page = 0, $p_is_admin=true){
        $arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        $arr_design = isset($arr_canvas[$p_page])?$arr_canvas[$p_page]:array();

        $v_folding_type = isset($arr_json['folding'])?$arr_json['folding']:'none';
        $v_die_cut_type = isset($arr_json['dieCutType'])?$arr_json['dieCutType']:'none';
        if($v_die_cut_type=='undefined' || $v_die_cut_type=='') $v_die_cut_type = 'none';
        $v_folding_direction = isset($arr_json['foldingDirection'])?$arr_json['foldingDirection']:'none';
        $v_bleed = isset($arr_design['bleed'])?$arr_design['bleed']:'0';
        settype($v_bleed, 'float');
        $v_page_count = 1;
        if($v_bleed>0){
            if($v_folding_type!='none' && $v_die_cut_type=='none'){
                if(in_array($v_folding_type, array('zFold', 'triFold','letterFold'))){
                    $v_page_count = 3;
                }else if(in_array($v_folding_type, array('accordionFold','rollFold'))){
                    $v_page_count = 4;
                }
            }
        }
        $v_width = isset($arr_design['width'])?$arr_design['width']:0;
        $v_height = isset($arr_design['height'])?$arr_design['height']:0;
        $v_bg_color = isset($arr_design['bg_color'])?$arr_design['bg_color']:'transparent';
        if(strlen($v_bg_color)!=6)
            $v_bg_color = 'transparent';
        else
            $v_bg_color = '#'.$v_bg_color;

        $v_width = ceil($v_width * $p_dpi);
        $v_height = ceil($v_height * $p_dpi);
        if($v_width<=0) $v_width = 50;
        if($v_height<=0) $v_height = 50;


        $pixel = new ImagickPixel($v_bg_color);
        $image->newimage($v_width, $v_height, $pixel);


//Gradient if exists
        if($v_page_count>1){
            $j = 0;
            if($v_folding_direction=='vertical'){
                $v_distance = ceil($v_width/$v_page_count);
                for($i=$v_page_count; $i>=0; $i--){
                    $v_tmp_height = ceil($v_distance/6);
                    $v_tmp_width = $v_height;
                    $v_gradient_string = $j%2==0?'gradient:white-lightgray':'gradient:lightgray-white';
                    $v_tmp_x_pos = $j%2==0? $v_distance * ($i-1):$v_distance * ($i-1) - $v_tmp_height;
                    $v_tmp_y_pos = 0;
                    $v_rotation = 90;
                    $this->set_x_pos($v_tmp_x_pos);
                    $this->set_y_pos($v_tmp_y_pos);
                    $this->set_width($v_tmp_width);
                    $this->set_height($v_tmp_height);
                    $this->gradient($image, $v_gradient_string, $v_rotation);

                    $j++;
                }
            }else{
                $v_distance = ceil($v_height/$v_page_count);
                for($i=$v_page_count; $i>=0; $i--){
                    $v_tmp_height = ceil($v_distance/6);
                    $v_tmp_width = $v_width;
                    $v_gradient_string = $j%2==0?'gradient:lightgray-white':'gradient:white-lightgray';
                    $v_tmp_y_pos = $j%2==0? $v_distance * ($i-1):$v_distance * ($i-1) - $v_tmp_height;
                    $v_tmp_x_pos = 0;
                    $v_rotation = 0;
                    $this->set_x_pos($v_tmp_x_pos);
                    $this->set_y_pos($v_tmp_y_pos);
                    $this->set_width($v_tmp_width);
                    $this->set_height($v_tmp_height);
                    $this->gradient($image, $v_gradient_string, $v_rotation);

                    $j++;
                }
            }
        }


//Images
        $arr_images = isset($arr_design['images'])?$arr_design['images']:array();
        if(count($arr_images)>1) $arr_images = $this-> record_sort($arr_images, 'zindex');
//$v_str = '';



        $v_root_dir = ROOT_DIR.DS;
        $v_font_dir = DESIGN_FONT_DIR.DS;


        for($i=0; $i<count($arr_images);$i++){
            $v_image_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:0;
            $v_width = isset($arr_images[$i]['width'])?$arr_images[$i]['width']:0;
            $v_height = isset($arr_images[$i]['height'])?$arr_images[$i]['height']:0;
            $v_rotation = isset($arr_images[$i]['rotation'])?$arr_images[$i]['rotation']:0;
            $v_left = isset($arr_images[$i]['left'])?$arr_images[$i]['left']:0;
            $v_top = isset($arr_images[$i]['top'])?$arr_images[$i]['top']:0;
            $v_crop = isset($arr_images[$i]['crop'])?$arr_images[$i]['crop']:false;
            $v_noprint = isset($arr_images[$i]['noprint']);
            if($v_noprint) $v_noprint = $arr_images[$i]['noprint'];
            $v_cropTop = isset($arr_images[$i]['cropTop'])?$arr_images[$i]['cropTop']:0;
            $v_cropLeft = isset($arr_images[$i]['cropLeft'])?$arr_images[$i]['cropLeft']:0;
            $v_cropWidth = isset($arr_images[$i]['cropWidth'])?$arr_images[$i]['cropWidth']:0;
            $v_cropHeight = isset($arr_images[$i]['cropHeight'])?$arr_images[$i]['cropHeight']:0;
            $v_border_width = isset($arr_images[$i]['border_width'])?$arr_images[$i]['border_width']:0;
            $v_border_color = isset($arr_images[$i]['border_color'])?$arr_images[$i]['border_color']:'transparent';

            if(strlen($v_border_color)<6)
                $v_border_color = 'transparent';
            else{
                $v_border_color = $v_border_color == 'transparent'?$v_border_color:'#'.substr($v_border_color,0,6);
            }
            $v_border_width = ceil($v_border_width);
            settype($v_border_width, 'float');
            settype($v_cropLeft, 'float');
            settype($v_cropTop, 'float');
            settype($v_cropWidth, 'float');
            settype($v_cropHeight, 'float');

            $v_top *= $p_dpi;
            $v_left *= $p_dpi;
            $v_width *= $p_dpi;
            $v_height *= $p_dpi;
            $v_width = ceil($v_width);
            $v_height = ceil($v_height);
            $v_top = ceil($v_top);
            $v_left = ceil($v_left);
            $v_noprint = $v_noprint && !$p_is_admin;
            if(!$v_noprint){
                if($v_image_id>0){
                    $v_row = $cls_image->select_one(array('image_id'=>$v_image_id));

                    if($v_row==1){
                        $v_directory = $cls_image->get_saved_dir();
                        $v_directory = $v_root_dir.$v_directory;
                        $v_image = $cls_image->get_image_file();
                        $v_image = $v_directory.$v_image;
                        if(file_exists($v_image)){
                            if($v_border_width>0){
                                $v_border_width = ceil($v_border_width*$p_dpi/(72*1.2));
                            }
                            $this->set_stroke_color($v_border_color);
                            $this->set_stroke_width($v_border_width);
                            $this->set_x_pos($v_left);
                            $this->set_y_pos($v_top);
                            $this->set_width($v_width);
                            $this->set_rotation($v_rotation);
                            $this->set_height($v_height);
                            $this->add_image($image, $v_image, $v_cropLeft, $v_cropTop, $v_cropWidth, $v_cropHeight);
                        }
                    }

                }else{
                    $v_shape_type = isset($arr_images[$i]['shape_type'])?$arr_images[$i]['shape_type']:'hline';
                    $v_border_stroke = isset($arr_images[$i]['border_stroke'])?$arr_images[$i]['border_stroke']:'solid';
                    $v_shape_fill_color = isset($arr_images[$i]['fill_color'])?$arr_images[$i]['fill_color']:'transparent';

                    $v_shape_fill_color = trim($v_shape_fill_color);

                    if(strlen($v_shape_fill_color)<6)
                        $v_shape_fill_color = 'transparent';
                    else{
                        $v_shape_fill_color = $v_shape_fill_color == 'transparent'?$v_shape_fill_color: '#'.substr($v_shape_fill_color, 0, 6);
                    }

                    $this->set_x_pos($v_left);
                    $this->set_y_pos($v_top);
                    $this->set_width($v_width);
                    $this->set_height($v_height);
                    $this->set_fill_color($v_shape_fill_color);
                    $this->set_stroke_color($v_border_color);
                    $this->set_stroke_width($v_border_width);
                    $this->set_stroke_style($v_border_stroke);
                    $this->set_rotation(0);
                    $this->add_shape($image, $v_shape_type);

                }
            }

        }

        $arr_texts = isset($arr_design['texts'])?$arr_design['texts']:array();

        for($i=0; $i<count($arr_texts);$i++){

            $v_color = isset($arr_texts[$i]['color'])?$arr_texts[$i]['color']:'black';
            if(strlen($v_color)<6)
                $v_color = 'black';
            else{
                $v_color = '#'.substr($v_color,0,6);
            }
            $v_top = (float) $arr_texts[$i]['top'];
            $v_left = (float) $arr_texts[$i]['left'];
            $v_width = (float) $arr_texts[$i]['width'];
            $v_height = (float) $arr_texts[$i]['height'];
            $v_body = isset($arr_texts[$i]['body'])?$arr_texts[$i]['body']:'';
            $v_bold = isset($arr_texts[$i]['bold'])?$arr_texts[$i]['bold']:false;
            $v_italic = isset($arr_texts[$i]['italic'])?$arr_texts[$i]['italic']:false;
            $v_gravity = isset($arr_texts[$i]['gravity'])?$arr_texts[$i]['gravity']:'center';
            $v_rotation = isset($arr_texts[$i]['rotation'])?$arr_texts[$i]['rotation']:0;
            $v_point_size = isset($arr_texts[$i]['pointsize'])?$arr_texts[$i]['pointsize']:0;

            $v_bullet_style = isset($arr_texts[$i]['bullet_style'])?$arr_texts[$i]['bullet_style']:'undefined';


            if($v_body==''){
                if($p_is_admin){
                    $v_body = isset($arr_texts[$i]['preset_text'])?$arr_texts[$i]['preset_text']:'';
                }
            }
            $v_point = $v_point_size;
            $v_point_size = ceil($v_point_size * $p_dpi/72);
            $v_font_name = $v_font_key = '';
            $v_font_name =  isset($arr_texts[$i]['font'])?$arr_texts[$i]['font']:'Arial';
            $v_font_key = seo_friendly_url($v_font_name);
            //$v_row = $cls_font->select_one(' AND `font_key`='."'{$v_font_key}' AND `font_regular`=1");
            $v_default_font = $v_font_dir.'arial'.DS.'regular.ttf';
            //if($v_row==1){
            //$v_bold = $cls_font->get_font_bold();
            //$v_italic = $cls_font->get_font_italic();
            //$v_bold_italic = $cls_font->get_font_bold_italic();
            if($v_bold && $v_italic){
                $v_font = $v_font_dir.$v_font_key.DS.'both.ttf';
            }else if($v_bold && !$v_italic)
                $v_font = $v_font_dir.$v_font_key.DS.'bold.ttf';
            else if(!$v_bold && $v_italic)
                $v_font = $v_font_dir.$v_font_key.DS.'italic.ttf';
            else
                $v_font = $v_font_dir.$v_font_key.DS.'regular.ttf';
            if(!file_exists($v_font)) $v_font = $v_default_font;
            //}else
            //    $v_font = $v_default_font;

            //$v_body = html_entity_decode($v_body);
            //$v_body = urldecode($v_body);

            $v_top *= $p_dpi;
            $v_left *= $p_dpi;
            $v_width *= $p_dpi;
            $v_height *= $p_dpi;

            $v_top = ceil($v_top);
            $v_left = ceil($v_left);
            $v_width = ceil($v_width);
            $v_height = ceil($v_height);

            if($v_rotation==90 || $v_rotation==270){
                $v_tmp = $v_width;
                $v_width = $v_height;
                $v_height = $v_tmp;
            }
            //$this->set_fill_color($v_color);
            //$this->set_height($v_height);
            //$this->set_width($v_width);
            //$this->set_rotation($v_rotation);
            //$this->set_x_pos($v_left);
            //$this->set_y_pos($v_top);

            //$this->add_text($image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style );

            $this->set_fill_color($v_color);
            $this->set_height($v_height);
            $this->set_width($v_width);
            $this->set_rotation($v_rotation);
            $this->set_x_pos($v_left);
            $this->set_y_pos($v_top);

            $tmp_image = new Imagick();

            $this->text($tmp_image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style);

            $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $v_left, $v_top);
            $tmp_image->clear();
            $tmp_image->destroy();
        }
    }
    public function remove_temp_file($p_dir, $p_calc_time, $p_after_hours = 1){
        if(!file_exists($p_dir) || !is_dir($p_dir)) return -1;
        $v_return = 0;
        $v_ds = DIRECTORY_SEPARATOR;
        $handle = opendir($p_dir);
        if($handle){
            while(($file=readdir($handle))!==false){
                if($file!='.' && $file!='..'){
                    $v_dir = $p_dir.$v_ds.$file;
                    if(is_file($v_dir)){
                        $v_time = filectime($v_dir);
                        $v_distance = $p_calc_time - $v_time;
                        if($v_distance >= 3600 * $p_after_hours){
                            if(@unlink($v_dir)) $v_return++;
                        }
                    }else if(is_dir($v_dir)){
                        $v_return += $this->remove_temp_file($v_dir, $p_calc_time, $p_after_hours);
                    }
                }
            }
            closedir($handle);
        }
        return $v_return;
    }
}