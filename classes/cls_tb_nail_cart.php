<?php
class cls_tb_nail_cart{

	private $v_cart_id = 0;
	private $v_product_id = 0;
	private $v_product_type = 0; // 0- normal , 1-free sample, 2- bonus
	private $v_product_quantity = 0;
	private $v_user_id = '';
	private $v_brand_id = '';
	private $v_tax = '';
	private $v_brand_name = '';
	private $v_category_id = '';
	private $v_category_name = '';
	private $v_product_image = '';
	private $v_product_slugger = '';
	private $v_product_name = '';
	private $v_product_sku = '';
	private $v_product_code = '';
	private $v_product_price = 0;
	private $v_product_price_discount = 0;
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
	
	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection('tb_nail_cart');
		$this->collection->ensureIndex(array("cart_id"=>1), array('name'=>"cart_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = 'tb_nail_cart';
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "cart_id" value
	 * @return int value
	 */
	public function get_tax(){
		return $this->v_tax;
	}

	
	/**
	 * function allow change properties "tax" value
	 * @param $p_cart_id: int value
	 */
	public function set_tax($p_cart_id){
		$this->v_tax = $p_cart_id;
	}

    /**
	 * function return properties "cart_id" value
	 * @return int value
	 */
	public function get_cart_id(){
		return (int) $this->v_cart_id;
	}


	/**
	 * function allow change properties "cart_id" value
	 * @param $p_cart_id: int value
	 */
	public function set_cart_id($p_cart_id){
		$this->v_cart_id = (int) $p_cart_id;
	}

	
	/**
	 * function return properties "product_id" value
	 * @return int value
	 */
	public function get_product_id(){
		return $this->v_product_id;
	}

	
	/**
	 * function allow change properties "product_id" value
	 * @param $p_product_id: int value
	 */
	public function set_product_id($p_product_id){
		$this->v_product_id = $p_product_id;
	}

	
	/**
	 * function return properties "product_quantity" value
	 * @return int value
	 */
	public function get_product_quantity(){
		return (int) $this->v_product_quantity;
	}

	
	/**
	 * function allow change properties "product_quantity" value
	 * @param $p_product_quantity: int value
	 */
	public function set_product_quantity($p_product_quantity){
		$this->v_product_quantity = (int) $p_product_quantity;
	}

	/**
	 * function return properties "brand_id" value
	 * @return string value
	 */
	public function get_brand_id(){
		return $this->v_brand_id;
	}


	/**
	 * function allow change properties "brand_id" value
	 * @param $p_brand_id: int value
	 */
	public function set_brand_id($p_brand_id){
		$this->v_brand_id = $p_brand_id;
	}

	/**
	 * function return properties "brand_name" value
	 * @return string value
	 */
	public function get_brand_name(){
		return $this->v_brand_name;
	}


	/**
	 * function allow change properties "brand_name" value
	 * @param $p_brand_name: int value
	 */
	public function set_brand_name($p_brand_name){
		$this->v_brand_name = $p_brand_name;
	}


    /**
     * function return properties "category_id" value
     * @return string value
     */
    public function get_category_id(){
        return $this->v_category_id;
    }


    /**
     * function allow change properties "category_id" value
     * @param $p_cat_id: int value
     */
    public function set_category_id($p_cat_id){
        $this->v_category_id = $p_cat_id;
    }



    /**
     * function return properties "category_name" value
     * @return string value
     */
    public function get_category_name(){
        return $this->v_category_name;
    }


    /**
     * function allow change properties "category_name" value
     * @param $p_cat_name: int value
     */
    public function set_category_name($p_cat_name){
        $this->v_category_name = $p_cat_name;
    }


	/**
	 * function return properties "product_type" value
	 * @return int value
	 */
	public function get_product_type(){
		return (int) $this->v_product_type;
	}


	/**
	 * function allow change properties "product_type" value
	 * @param $p_product_type: int value
	 */
	public function set_product_type($p_product_type){
		$this->v_product_type = (int) $p_product_type;
	}

	
	/**
	 * function return properties "product_image" value
	 * @return string value
	 */
	public function get_product_image(){
		return $this->v_product_image;
	}

	
	/**
	 * function allow change properties "product_image" value
	 * @param $p_product_image: string value
	 */
	public function set_product_image($p_product_image){
		$this->v_product_image = $p_product_image;
	}
    /**
	 * function return properties "product_slugger" value
	 * @return string value
	 */
	public function get_product_slugger(){
		return $this->v_product_slugger;
	}


	/**
	 * function allow change properties "product_slugger" value
	 * @param $p_slugger: string value
	 */
	public function set_product_slugger($p_slugger){
		$this->v_product_slugger = $p_slugger;
	}

	/**
	 * function return properties "user_id" value
	 * @return string value
	 */
	public function get_user_id(){
		return $this->v_user_id;
	}


	/**
	 * function allow change properties "user_id" value
	 * @param $p_user_id: string value
	 */
	public function set_user_id($p_user_id){
		$this->v_user_id = $p_user_id;
	}

	
	/**
	 * function return properties "product_name" value
	 * @return string value
	 */
	public function get_product_name(){
		return $this->v_product_name;
	}

	
	/**
	 * function allow change properties "product_name" value
	 * @param $p_product_name: string value
	 */
	public function set_product_name($p_product_name){
		$this->v_product_name = $p_product_name;
	}

	
	/**
	 * function return properties "product_sku" value
	 * @return string value
	 */
	public function get_product_sku(){
		return $this->v_product_sku;
	}

	
	/**
	 * function allow change properties "product_sku" value
	 * @param $p_product_sku: string value
	 */
	public function set_product_sku($p_product_sku){
		$this->v_product_sku = $p_product_sku;
	}

	/**
	 * function return properties "product_code" value
	 * @return string value
	 */
	public function get_product_code(){
		return $this->v_product_code;
	}


	/**
	 * function allow change properties "product_code" value
	 * @param $p_product_code: string value
	 */
	public function set_product_code($p_product_code){
		$this->v_product_code = $p_product_code;
	}

	
	/**
	 * function return properties "product_price" value
	 * @return float value
	 */
	public function get_product_price(){
		return (float) $this->v_product_price;
	}

	
	/**
	 * function allow change properties "discount" value
	 * @param $p_product_price: float value
	 */
	public function set_product_price($p_product_price){
		$this->v_product_price = (float) $p_product_price;
	}

	/**
	 * function return properties "discount" value
	 * @return float value
	 */
	public function get_product_sell_discount(){
		return (float) $this->v_product_price_discount;
	}


	/**
	 * function allow change properties "product_price" value
	 * @param $p_product_price: float value
	 */
	public function set_product_sell_discount($p_product_price){
		$this->v_product_price_discount = (float) $p_product_price;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
		$arr = array('cart_id' => $this->v_cart_id
					,'product_id' => $this->v_product_id
					,'tax' => $this->v_tax
					,'product_quantity' => $this->v_product_quantity
					,'product_type' => $this->v_product_type
					,'brand_id' => $this->v_brand_id
					,'brand_name' => $this->v_brand_name
					,'category_id' => $this->v_category_id
					,'category_name' => $this->v_category_name
					,'product_image' => $this->v_product_image
					,'product_slugger' => $this->v_product_slugger
					,'user_id' => $this->v_user_id
					,'product_name' => $this->v_product_name
					,'product_sku' => $this->v_product_sku
					,'product_code' => $this->v_product_code
					,'discount' => $this->v_product_price_discount
					,'product_price' => $this->v_product_price);
		try{
			$this->collection->insert($arr, array('safe'=>true));
			$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_nail_cart` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_cart($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_cart_id = isset($arr['cart_id'])?$arr['cart_id']:0;
			$this->v_tax = isset($arr['tax'])?$arr['tax']:0;
			$this->v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
			$this->v_product_quantity = isset($arr['product_quantity'])?$arr['product_quantity']:0;
			$this->v_brand_id = isset($arr['brand_id'])?$arr['brand_id']:'';
			$this->v_brand_name = isset($arr['brand_name'])?$arr['brand_name']:'';
			$this->v_category_id = isset($arr['category_id'])?$arr['category_id']:'';
			$this->v_category_name = isset($arr['category_name'])?$arr['category_name']:'';
			$this->v_product_type = isset($arr['product_type'])?$arr['product_type']:0;
			$this->v_product_image = isset($arr['product_image'])?$arr['product_image']:'';
			$this->v_product_slugger = isset($arr['product_slugger'])?$arr['product_slugger']:'';
			$this->v_user_id = isset($arr['user_id'])?$arr['user_id']:'';
			$this->v_product_name = isset($arr['product_name'])?$arr['product_name']:'';
			$this->v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
			$this->v_product_code = isset($arr['product_code'])?$arr['product_code']:'';
			$this->v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
			$this->v_product_price_discount = isset($arr['discount'])?$arr['discount']:0;
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `cart_id` FROM `tb_nail_cart` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_cart($db)
	 * 		 $cls->select_scalar('cart_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `cart_id` FROM `tb_nail_cart` WHERE `user_id`=2 ORDER BY `cart_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_cart($db)
	 * 		 $cls->select_next('cart_id',array('user_id'=>2), array('cart_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return ((int) $v_ret)+1;
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_nail_cart($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_nail_cart($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
    public function select_distinct($p_field_name,MongoDB $db,$arr_where = array()){
        return $this->command(array("distinct"=>"tb_nail_cart" ,"key"=>$p_field_name,"query"=>$arr_where ), $db);
    }
    public function command($data, MongoDB $db) {
        return $db->selectCollection('$cmd')->findOne($data);
    }
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_nail_cart($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('cart_id' => $this->v_cart_id
            ,'brand_id' => $this->v_brand_id
            ,'tax' => $this->v_tax
            ,'brand_name' => $this->v_brand_name
            ,'category_id' => $this->v_category_id
            ,'category_name' => $this->v_category_name
            ,'product_type' => $this->v_product_type,'product_id' => $this->v_product_id,'product_quantity' => $this->v_product_quantity,'product_slugger' => $this->v_product_slugger,'user_id' => $this->v_user_id,'product_image' => $this->v_product_image,'product_name' => $this->v_product_name,'product_code' => $this->v_product_code,'discount' => $this->v_product_price_discount,'product_sku' => $this->v_product_sku,'product_price' => $this->v_product_price));
		 else 
			$arr = array('$set' => array('product_id' => $this->v_product_id
            ,'brand_id' => $this->v_brand_id
            ,'tax' => $this->v_tax
            ,'brand_name' => $this->v_brand_name
            ,'category_id' => $this->v_category_id
            ,'category_name' => $this->v_category_name
            ,'product_type' => $this->v_product_type,'product_quantity' => $this->v_product_quantity,'product_slugger' => $this->v_product_slugger,'product_image' => $this->v_product_image,'user_id' => $this->v_user_id,'product_name' => $this->v_product_name,'product_code' => $this->v_product_code,'product_sku' => $this->v_product_sku,'discount' => $this->v_product_price_discount,'product_price' => $this->v_product_price));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>