<?php
class cls_tb_contact{

    private $v_contact_id = 0;
    private $arr_addresses = array();
    private $v_addresses_default_key = 0;
    private $v_company = '';
    private $v_company_id = '0';
    private $v_created_by = '';
    private $v_date_modified = '0000-00-00 00:00:00';
    private $v_user_lastlog = '0000-00-00 00:00:00';
    private $v_deleted = 0;
    private $v_default_cart = 0;
    private $arr_cart = array();
    private $v_direct_dial = '';
    private $v_email = '';
    private $v_extension_no = '';
    private $v_fax = '';
    private $v_first_name = '';
    private $v_last_name = '';
    private $v_middle_name = '';
    private $v_mobile = '';
    private $v_modified_by = '';
    private $v_full_name = '';
    private $v_position = '';
    private $v_user_email = '';
    private $v_user_name = '';
    private $v_user_password = '';
    private $v_user_rule = '';
    private $v_title = '';
    private $v_inactive = 0;
    private $v_is_customer = 0;
    private $v_is_employee = 0;
    private $v_user_status = 0;
    private $v_is_subcription = 0;
    private $v_user_type = 0;
    private $v_no = 0;
    private $collection = NULL;
    private $v_mongo_id = NULL;
    private $v_error_code = 0;
    private $v_error_message = '';
    private $v_is_log = false;
    private $v_dir = '';

    /**
     *  constructor function
     *  @param $db: instance of Mongo
     *  @param $p_log_dir string: directory contains its log file
     */
    public function __construct(MongoDB $db, $p_log_dir = ""){
        $this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
        if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
        $this->collection = $db->selectCollection('tb_contact');
        $this->v_date_modified = new MongoDate(time());
        $this->v_user_lastlog = new MongoDate(time());
        $this->collection->ensureIndex(array("contact_id"=>1), array('name'=>"contact_id_key", "unique"=>1, "dropDups" => 1));
    }

    /**
     *  function get current MongoDB collection
     *  @return Object: current MongoDB collection
     */
    public function get_collection(){
        return $this->collection;
    }

    /**
     *  function write log
     */
    private function my_error(){
        if(! $this->v_is_log) return;
        global $_SERVER;
        $v_filename = 'tb_contact';
        $v_ext = '.log';
        $v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
        $v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
        $v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
        $v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
        $v_log_str .= "\r\n----------------End Log-----------------";
        $v_log_str .= "\r\n";
        $v_new_file = false;
        if(file_exists($this->v_dir.$v_filename.$v_ext)){
            if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
                rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
                $v_new_file = true;
                @unlink($this->v_dir.$v_filename.$v_ext);
            }
        }
        $fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
        if($fp){
            fwrite($fp, $v_log_str, strlen($v_log_str));
            fflush($fp);
            fclose($fp);
        }
    }

    /**
     * function return properties "contact_id" value
     * @return int value
     */
    public function get_contact_id(){
        return (int) $this->v_contact_id;
    }


    /**
     * function allow change properties "contact_id" value
     * @param $p_contact_id: int value
     */
    public function set_contact_id($p_contact_id){
        $this->v_contact_id = (int) $p_contact_id;
    }


    /**
     * function return properties "addresses" value
     * @return array value
     */
    public function get_addresses(){
        return  $this->arr_addresses;
    }


    /**
     * function allow change properties "addresses" value
     * @param $arr_addresses array
     */
    public function set_addresses(array $arr_addresses = array()){
        $this->arr_addresses = $arr_addresses;
    }


    /**
     * function return properties "addresses_default_key" value
     * @return int value
     */
    public function get_addresses_default_key(){
        return (int) $this->v_addresses_default_key;
    }


    /**
     * function allow change properties "addresses_default_key" value
     * @param $p_addresses_default_key: int value
     */
    public function set_addresses_default_key($p_addresses_default_key){
        $this->v_addresses_default_key = (int) $p_addresses_default_key;
    }


    /**
     * function return properties "company" value
     * @return string value
     */
    public function get_company(){
        return $this->v_company;
    }


    /**
     * function allow change properties "company" value
     * @param $p_company: string value
     */
    public function set_company($p_company){
        $this->v_company = $p_company;
    }


    /**
     * function return properties "company_id" value
     * @return string value
     */
    public function get_company_id(){
        return $this->v_company_id;
    }


    /**
     * function allow change properties "company_id" value
     * @param $p_company_id: string value
     */
    public function set_company_id($p_company_id){
        $this->v_company_id = $p_company_id;
    }


    /**
     * function return properties "created_by" value
     * @return string value
     */
    public function get_created_by(){
        return $this->v_created_by;
    }


    /**
     * function allow change properties "created_by" value
     * @param $p_created_by: string value
     */
    public function set_created_by($p_created_by){
        $this->v_created_by = $p_created_by;
    }


    /**
     * function return properties "date_modified" value
     * @return int value indicates amount of seconds
     */
    public function get_date_modified(){
        return  $this->v_date_modified->sec;
    }


    /**
     * function allow change properties "date_modified" value
     * @param $p_date_modified: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_date_modified($p_date_modified){
        if($p_date_modified=='') $p_date_modified = NULL;
        if(!is_null($p_date_modified)){
            try{
                $this->v_date_modified = new MongoDate(strtotime($p_date_modified));
            }catch(MongoException $me){
                $this->v_date_modified = NULL;
            }
        }else{
            $this->v_date_modified = NULL;
        }
    }


    /**
     * function return properties "user_lastlog" value
     * @return int value indicates amount of seconds
     */
    public function get_user_lastlog(){
        return  $this->v_user_lastlog->sec;
    }


    /**
     * function allow change properties "user_lastlog" value
     * @param $p_user_lastlog: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_user_lastlog($p_user_lastlog){
        if($p_user_lastlog=='') $p_user_lastlog = NULL;
        if(!is_null($p_user_lastlog)){
            try{
                $this->v_user_lastlog = new MongoDate(strtotime($p_user_lastlog));
            }catch(MongoException $me){
                $this->v_user_lastlog = NULL;
            }
        }else{
            $this->v_user_lastlog = NULL;
        }
    }


    /**
     * function return properties "deleted" value
     * @return int value
     */
    public function get_deleted(){
        return (int) $this->v_deleted;
    }


    /**
     * function allow change properties "deleted" value
     * @param $p_deleted: int value
     */
    public function set_deleted($p_deleted){
        $this->v_deleted = (int) $p_deleted;
    }

    /**
     * function return properties "default_cart" value
     * @return int value
     */
    public function get_default_cart_index(){
        return (int) $this->v_default_cart;
    }


    /**
     * function allow change properties "default_cart" value
     * @param $p_index: int value
     */
    public function set_default_cart_index($p_index){
        $this->v_default_cart = (int) $p_index;
    }

    /**
     * function return properties "cart" value
     * @return array value
     */
    public function get_cart(){
        return $this->arr_cart;
    }


    /**
     * function allow change properties "cart" value
     * @param $arr_cart: array value
     */
    public function set_cart($arr_cart = array()){
        $this->arr_cart = $arr_cart;
    }


    /**
     * function return properties "direct_dial" value
     * @return string value
     */
    public function get_direct_dial(){
        return $this->v_direct_dial;
    }


    /**
     * function allow change properties "direct_dial" value
     * @param $p_direct_dial: string value
     */
    public function set_direct_dial($p_direct_dial){
        $this->v_direct_dial = $p_direct_dial;
    }


    /**
     * function return properties "email" value
     * @return string value
     */
    public function get_email(){
        return $this->v_email;
    }


    /**
     * function allow change properties "email" value
     * @param $p_email: string value
     */
    public function set_email($p_email){
        $this->v_email = $p_email;
    }


    /**
     * function return properties "extension_no" value
     * @return string value
     */
    public function get_extension_no(){
        return $this->v_extension_no;
    }


    /**
     * function allow change properties "extension_no" value
     * @param $p_extension_no: string value
     */
    public function set_extension_no($p_extension_no){
        $this->v_extension_no = $p_extension_no;
    }


    /**
     * function return properties "fax" value
     * @return string value
     */
    public function get_fax(){
        return $this->v_fax;
    }


    /**
     * function allow change properties "fax" value
     * @param $p_fax: string value
     */
    public function set_fax($p_fax){
        $this->v_fax = $p_fax;
    }


    /**
     * function return properties "first_name" value
     * @return string value
     */
    public function get_first_name(){
        return $this->v_first_name;
    }


    /**
     * function allow change properties "first_name" value
     * @param $p_first_name: string value
     */
    public function set_first_name($p_first_name){
        $this->v_first_name = $p_first_name;
    }


    /**
     * function return properties "last_name" value
     * @return string value
     */
    public function get_last_name(){
        return $this->v_last_name;
    }


    /**
     * function allow change properties "last_name" value
     * @param $p_last_name: string value
     */
    public function set_last_name($p_last_name){
        $this->v_last_name = $p_last_name;
    }


    /**
     * function return properties "middle_name" value
     * @return string value
     */
    public function get_middle_name(){
        return $this->v_middle_name;
    }


    /**
     * function allow change properties "middle_name" value
     * @param $p_middle_name: string value
     */
    public function set_middle_name($p_middle_name){
        $this->v_middle_name = $p_middle_name;
    }


    /**
     * function return properties "mobile" value
     * @return string value
     */
    public function get_mobile(){
        return $this->v_mobile;
    }


    /**
     * function allow change properties "mobile" value
     * @param $p_mobile: string value
     */
    public function set_mobile($p_mobile){
        $this->v_mobile = $p_mobile;
    }


    /**
     * function return properties "modified_by" value
     * @return string value
     */
    public function get_modified_by(){
        return $this->v_modified_by;
    }


    /**
     * function allow change properties "modified_by" value
     * @param $p_modified_by: string value
     */
    public function set_modified_by($p_modified_by){
        $this->v_modified_by = $p_modified_by;
    }


    /**
     * function return properties "full_name" value
     * @return string value
     */
    public function get_full_name(){
        return $this->v_full_name;
    }


    /**
     * function allow change properties "full_name" value
     * @param $p_full_name: string value
     */
    public function set_full_name($p_full_name){
        $this->v_full_name = $p_full_name;
    }


    /**
     * function return properties "position" value
     * @return string value
     */
    public function get_position(){
        return $this->v_position;
    }


    /**
     * function allow change properties "position" value
     * @param $p_position: string value
     */
    public function set_position($p_position){
        $this->v_position = $p_position;
    }


    /**
     * function return properties "user_email" value
     * @return string value
     */
    public function get_user_email(){
        return $this->v_user_email;
    }


    /**
     * function allow change properties "user_email" value
     * @param $p_user_email: string value
     */
    public function set_user_email($p_user_email){
        $this->v_user_email = $p_user_email;
    }


    /**
     * function return properties "user_name" value
     * @return string value
     */
    public function get_user_name(){
        return $this->v_user_name;
    }


    /**
     * function allow change properties "user_name" value
     * @param $p_user_name: string value
     */
    public function set_user_name($p_user_name){
        $this->v_user_name = $p_user_name;
    }


    /**
     * function return properties "user_password" value
     * @return string value
     */
    public function get_user_password(){
        return $this->v_user_password;
    }


    /**
     * function allow change properties "user_password" value
     * @param $p_user_password: string value
     */
    public function set_user_password($p_user_password){
        $this->v_user_password = $p_user_password;
    }


    /**
     * function return properties "user_rule" value
     * @return string value
     */
    public function get_user_rule(){
        return $this->v_user_rule;
    }


    /**
     * function allow change properties "user_rule" value
     * @param $p_user_rule: string value
     */
    public function set_user_rule($p_user_rule){
        $this->v_user_rule = $p_user_rule;
    }


    /**
     * function return properties "title" value
     * @return string value
     */
    public function get_title(){
        return $this->v_title;
    }


    /**
     * function allow change properties "title" value
     * @param $p_title: string value
     */
    public function set_title($p_title){
        $this->v_title = $p_title;
    }


    /**
     * function return properties "inactive" value
     * @return int value
     */
    public function get_inactive(){
        return (int) $this->v_inactive;
    }


    /**
     * function allow change properties "inactive" value
     * @param $p_inactive: int value
     */
    public function set_inactive($p_inactive){
        $this->v_inactive = (int) $p_inactive;
    }


    /**
     * function return properties "is_customer" value
     * @return int value
     */
    public function get_is_customer(){
        return (int) $this->v_is_customer;
    }


    /**
     * function allow change properties "is_customer" value
     * @param $p_is_customer: int value
     */
    public function set_is_customer($p_is_customer){
        $this->v_is_customer = (int) $p_is_customer;
    }


    /**
     * function return properties "is_employee" value
     * @return int value
     */
    public function get_is_employee(){
        return (int) $this->v_is_employee;
    }


    /**
     * function allow change properties "is_employee" value
     * @param $p_is_employee: int value
     */
    public function set_is_employee($p_is_employee){
        $this->v_is_employee = (int) $p_is_employee;
    }


    /**
     * function return properties "user_status" value
     * @return int value
     */
    public function get_user_status(){
        return (int) $this->v_user_status;
    }


    /**
     * function allow change properties "user_status" value
     * @param $p_user_status: int value
     */
    public function set_user_status($p_user_status){
        $this->v_user_status = (int) $p_user_status;
    }


    /**
     * function return properties "is_subcription" value
     * @return int value
     */
    public function get_is_subcription(){
        return (int) $this->v_is_subcription;
    }


    /**
     * function allow change properties "is_subcription" value
     * @param $p_is_subcription: int value
     */
    public function set_is_subcription($p_is_subcription){
        $this->v_is_subcription = (int) $p_is_subcription;
    }


    /**
     * function return properties "user_type" value
     * @return int value
     */
    public function get_user_type(){
        return (int) $this->v_user_type;
    }


    /**
     * function allow change properties "user_type" value
     * @param $p_user_type: int value
     */
    public function set_user_type($p_user_type){
        $this->v_user_type = (int) $p_user_type;
    }


    /**
     * function return properties "no" value
     * @return int value
     */
    public function get_no(){
        return (int) $this->v_no;
    }


    /**
     * function allow change properties "no" value
     * @param $p_no: int value
     */
    public function set_no($p_no){
        $this->v_no = (int) $p_no;
    }


    /**
     * function return MongoID value after inserting new record
     * @return ObjectId: MongoId
     */
    public function get_mongo_id(){
        return $this->v_mongo_id;
    }


    /**
     * function set MongoID to properties
     */
    public function set_mongo_id($p_mongo_id){
        $this->v_mongo_id = $p_mongo_id;
    }


    /**
     *  function allow insert one record
     *  @return MongoID
     */
    public function insert(){
        $arr = array('contact_id' => $this->v_contact_id
        ,'addresses' => $this->arr_addresses
        ,'addresses_default_key' => $this->v_addresses_default_key
        ,'company' => $this->v_company
        ,'company_id' => $this->v_company_id
        ,'created_by' => $this->v_created_by
        ,'date_modified' => $this->v_date_modified
        ,'user_lastlog' => $this->v_user_lastlog
        ,'deleted' => $this->v_deleted
        ,'default_cart' => $this->v_default_cart
        ,'cart' => $this->arr_cart
        ,'direct_dial' => $this->v_direct_dial
        ,'email' => $this->v_email
        ,'extension_no' => $this->v_extension_no
        ,'fax' => $this->v_fax
        ,'first_name' => $this->v_first_name
        ,'last_name' => $this->v_last_name
        ,'middle_name' => $this->v_middle_name
        ,'mobile' => $this->v_mobile
        ,'modified_by' => $this->v_modified_by
        ,'full_name' => $this->v_full_name
        ,'position' => $this->v_position
        ,'user_email' => $this->v_user_email
        ,'user_name' => $this->v_user_name
        ,'user_password' => $this->v_user_password
        ,'user_rule' => $this->v_user_rule
        ,'title' => $this->v_title
        ,'inactive' => $this->v_inactive
        ,'is_customer' => $this->v_is_customer
        ,'is_employee' => $this->v_is_employee
        ,'user_status' => $this->v_user_status
        ,'is_subcription' => $this->v_is_subcription
        ,'user_type' => $this->v_user_type
        ,'no' => $this->v_no);
        try{
            $this->collection->insert($arr, array('safe'=>true));
            $this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     *  function allow insert array with parameter
     *  @param array $arr_fields_and_values
     *  @return MongoID
     */
    public function insert_array(array $arr_fields_and_values){
        try{
            $this->collection->insert($arr_fields_and_values, array('safe'=>true));
            $this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     * function select_one_record
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: all values will assign to this instance properties
     * @example:
     * <code>
     *       SELECT * FROM `tb_contact` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return int
     */
    public function select_one(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_count = 0;
        foreach($rss as $arr){
            $this->v_contact_id = isset($arr['contact_id'])?$arr['contact_id']:0;
            $this->arr_addresses = isset($arr['addresses'])?$arr['addresses']:array();
            $this->v_addresses_default_key = isset($arr['addresses_default_key'])?$arr['addresses_default_key']:0;
            $this->v_company = isset($arr['company'])?$arr['company']:'';
            $this->v_company_id = isset($arr['company_id'])?$arr['company_id']:'0';
            $this->v_created_by = isset($arr['created_by'])?$arr['created_by']:'';
            $this->v_date_modified = isset($arr['date_modified'])?$arr['date_modified']:(new MongoDate(time()));
            $this->v_user_lastlog = isset($arr['user_lastlog'])?$arr['user_lastlog']:(new MongoDate(time()));
            $this->v_deleted = isset($arr['deleted'])?$arr['deleted']:0;
            $this->v_default_cart = isset($arr['default_cart'])?$arr['default_cart']:0;
            $this->arr_cart = isset($arr['cart'])?$arr['cart']:array();
            $this->v_direct_dial = isset($arr['direct_dial'])?$arr['direct_dial']:'';
            $this->v_email = isset($arr['email'])?$arr['email']:'';
            $this->v_extension_no = isset($arr['extension_no'])?$arr['extension_no']:'';
            $this->v_fax = isset($arr['fax'])?$arr['fax']:'';
            $this->v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
            $this->v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
            $this->v_middle_name = isset($arr['middle_name'])?$arr['middle_name']:'';
            $this->v_mobile = isset($arr['mobile'])?$arr['mobile']:'';
            $this->v_modified_by = isset($arr['modified_by'])?$arr['modified_by']:'';
            $this->v_full_name = isset($arr['full_name'])?$arr['full_name']:'';
            $this->v_position = isset($arr['position'])?$arr['position']:'';
            $this->v_user_email = isset($arr['user_email'])?$arr['user_email']:'';
            $this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
            $this->v_user_password = isset($arr['user_password'])?$arr['user_password']:'';
            $this->v_user_rule = isset($arr['user_rule'])?$arr['user_rule']:'';
            $this->v_title = isset($arr['title'])?$arr['title']:'';
            $this->v_inactive = isset($arr['inactive'])?$arr['inactive']:0;
            $this->v_is_customer = isset($arr['is_customer'])?$arr['is_customer']:0;
            $this->v_is_employee = isset($arr['is_employee'])?$arr['is_employee']:0;
            $this->v_user_status = isset($arr['user_status'])?$arr['user_status']:0;
            $this->v_is_subcription = isset($arr['is_subcription'])?$arr['is_subcription']:0;
            $this->v_user_type = isset($arr['user_type'])?$arr['user_type']:0;
            $this->v_no = isset($arr['no'])?$arr['no']:0;
            $this->v_mongo_id = $arr['_id'];
            $v_count++;
        }
        return $v_count;
    }

    /**
     * function select scalar value
     * @param $p_field_name string, name of field
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: assign to properties
     * @example:
     * <code>
     * SELECT `contact_id` FROM `tb_contact` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select_scalar('contact_id',array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return mixed
     */
    public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = NULL;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return $v_ret;
    }

    /**
     * function get next int value for key
     * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @result: assign to properties
     * @example:
     * <code>
     *   SELECT `contact_id` FROM `tb_contact` WHERE `user_id`=2 ORDER BY `contact_id` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select_next('contact_id',array('user_id'=>2), array('contact_id'=>-1))
     * </code>
     * @return int
     */
    public function select_next($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => -1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = 0;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return ((int) $v_ret)+1;
    }

    /**
     * function get missing value
     * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function select_missing($p_field_name, array $arr_where = array()){
        $arr_order = array(''.$p_field_name.'' => 1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_start = 1;
        $v_ret = 1;
        foreach($rss as $arr){
            if($arr[''.$p_field_name.'']!=$v_start){
                $v_ret = $v_start;
                break;
            }
            $v_start++;
        }
        return ((int) $v_ret);
    }

    /**
     * function select limit records
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr;
    }

    /**
     * function select records
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order);
        return $arr;
    }

    /**
     * function select distinct
     * @param $p_field_name string, name of selected field
     * @example:
     * <code>
     *         SELECT DISTINCT `name` FROM `tbl_users`
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select_distinct('nam')
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_distinct($p_field_name){
        return $this->collection->command(array("distinct"=>"tb_contact", "key"=>$p_field_name));
    }

    /**
     * function select limit fields
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_fields array, array of fields will be selected
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_contact($db)
     * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr_field = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr_field[$arr_fields[$i]] = 1;
        if($p_row <= 0)
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
        else
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr_return;
    }

    /**
     *  function update one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(isset($v_has_mongo_id) && $v_has_mongo_id)
            $arr = array('$set' => array('contact_id' => $this->v_contact_id,'addresses' => $this->arr_addresses,'addresses_default_key' => $this->v_addresses_default_key,'company' => $this->v_company,'company_id' => $this->v_company_id,'created_by' => $this->v_created_by    ,'default_cart' => $this->v_default_cart
            ,'cart' => $this->arr_cart,'date_modified' => $this->v_date_modified,'user_lastlog' => $this->v_user_lastlog,'deleted' => $this->v_deleted,'direct_dial' => $this->v_direct_dial,'email' => $this->v_email,'extension_no' => $this->v_extension_no,'fax' => $this->v_fax,'first_name' => $this->v_first_name,'last_name' => $this->v_last_name,'middle_name' => $this->v_middle_name,'mobile' => $this->v_mobile,'modified_by' => $this->v_modified_by,'full_name' => $this->v_full_name,'position' => $this->v_position,'user_email' => $this->v_user_email,'user_name' => $this->v_user_name,'user_password' => $this->v_user_password,'user_rule' => $this->v_user_rule,'title' => $this->v_title,'inactive' => $this->v_inactive,'is_customer' => $this->v_is_customer,'is_employee' => $this->v_is_employee,'user_status' => $this->v_user_status,'is_subcription' => $this->v_is_subcription,'user_type' => $this->v_user_type,'no' => $this->v_no));
        else
            $arr = array('$set' => array('addresses' => $this->arr_addresses    ,'default_cart' => $this->v_default_cart
            ,'cart' => $this->arr_cart,'addresses_default_key' => $this->v_addresses_default_key,'company' => $this->v_company,'company_id' => $this->v_company_id,'created_by' => $this->v_created_by,'date_modified' => $this->v_date_modified,'user_lastlog' => $this->v_user_lastlog,'deleted' => $this->v_deleted,'direct_dial' => $this->v_direct_dial,'email' => $this->v_email,'extension_no' => $this->v_extension_no,'fax' => $this->v_fax,'first_name' => $this->v_first_name,'last_name' => $this->v_last_name,'middle_name' => $this->v_middle_name,'mobile' => $this->v_mobile,'modified_by' => $this->v_modified_by,'full_name' => $this->v_full_name,'position' => $this->v_position,'user_email' => $this->v_user_email,'user_name' => $this->v_user_name,'user_password' => $this->v_user_password,'user_rule' => $this->v_user_rule,'title' => $this->v_title,'inactive' => $this->v_inactive,'is_customer' => $this->v_is_customer,'is_employee' => $this->v_is_employee,'user_status' => $this->v_user_status,'is_subcription' => $this->v_is_subcription,'user_type' => $this->v_user_type,'no' => $this->v_no));
        try{
            $this->collection->update($arr_where, $arr, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function delete one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function delete(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->remove($arr_where, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_field($p_field, $p_value, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields array, array of selected fields go to updated
     * @param $arr_values array, array of selected values go to assigned
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function increase one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields = array(), array of selected fields go to updated
     * @param $arr_values = array(), array of selected values go to increase
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function draw option tag
     * @param $p_field_value string: name of field will be value option tag
     * @param $p_field_display string: name of field will be display text option tag
     * @param $p_selected_value mixed: value of field will be display text option tag
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_exclude array: array list value of exclude
     * @return string
     */
    public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
        $arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
        $v_dsp_option = '';
        foreach($arr as $a){
            if(!in_array($a[$p_field_value],$arr_exclude)){
                if($a[$p_field_value] == $p_selected_value)
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
                else
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
            }
        }
        return $v_dsp_option;
    }

    /**
     * function count all records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count(array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->count();
        else
            return $this->collection->find($arr_where)->count();
    }

    /**
     * function count all records
     * @param $p_field string: in field to count
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count_field($p_field, array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->find(array($p_field => array('$exists' => true)))->count();
        else
            return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
    }
}
?>