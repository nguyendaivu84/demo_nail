<?php
class cls_tb_upload_order{

	private $v_row_id = 0;
	private $v_location_id = 0;
	private $v_location_number = '';
	private $v_location_name = '';
	private $v_kitting_cost = 0;
	private $v_company_name = '';
	private $v_address_unit = '';
	private $v_address_line_1 = '';
	private $v_address_line_2 = '';
	private $v_address_line_3 = '';
	private $v_address_postal = '';
	private $v_address_city = '';
	private $v_address_province = '';
	private $v_address_country = '';
	private $v_company_id = 0;
	private $v_product_id = 0;
	private $v_product_similar_id = 0;
	private $v_product_sku = '';
	private $v_short_description = '';
	private $v_product_quantity = '0';
	private $v_product_status = 0;
	private $v_product_image = '0';
	private $arr_product_material = array();
	private $v_product_price = 0;
	private $v_excel_col = '';
	private $v_excel_file = '';
	private $v_product_accept = '';
	private $v_material_detail = '';
	private $v_user_name = '';
	private $v_user_company = 0;
	private $v_upload_time = '0000-00-00 00:00:00';
	private $v_upload_session = '';
	private $v_upload_key = '';
	private $v_order_id = 0;
	private $v_order_item_id = 0;
	private $arr_error_field = array();
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
	
	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection('tb_upload_order');
		$this->collection->ensureIndex(array("location_id"=>1));
		$this->collection->ensureIndex(array("location_number"=>1));
		$this->v_upload_time = new MongoDate(time());
		$this->collection->ensureIndex(array("row_id"=>1), array('name'=>"row_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = 'tb_upload_order';
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "row_id" value
	 * @return int value
	 */
	public function get_row_id(){
		return (int) $this->v_row_id;
	}

	
	/**
	 * function allow change properties "row_id" value
	 * @param $p_row_id: int value
	 */
	public function set_row_id($p_row_id){
		$this->v_row_id = (int) $p_row_id;
	}

	
	/**
	 * function return properties "location_id" value
	 * @return int value
	 */
	public function get_location_id(){
		return (int) $this->v_location_id;
	}

	
	/**
	 * function allow change properties "location_id" value
	 * @param $p_location_id: int value
	 */
	public function set_location_id($p_location_id){
		$this->v_location_id = (int) $p_location_id;
	}

	
	/**
	 * function return properties "location_number" value
	 * @return string value
	 */
	public function get_location_number(){
		return $this->v_location_number;
	}

	
	/**
	 * function allow change properties "location_number" value
	 * @param $p_location_number: string value
	 */
	public function set_location_number($p_location_number){
		$this->v_location_number = $p_location_number;
	}

	
	/**
	 * function return properties "location_name" value
	 * @return string value
	 */
	public function get_location_name(){
		return $this->v_location_name;
	}

	
	/**
	 * function allow change properties "location_name" value
	 * @param $p_location_name: string value
	 */
	public function set_location_name($p_location_name){
		$this->v_location_name = $p_location_name;
	}

	
	/**
	 * function return properties "kitting_cost" value
	 * @return float value
	 */
	public function get_kitting_cost(){
		return (float) $this->v_kitting_cost;
	}

	
	/**
	 * function allow change properties "kitting_cost" value
	 * @param $p_kitting_cost: float value
	 */
	public function set_kitting_cost($p_kitting_cost){
		$this->v_kitting_cost = (float) $p_kitting_cost;
	}

	
	/**
	 * function return properties "company_name" value
	 * @return string value
	 */
	public function get_company_name(){
		return $this->v_company_name;
	}

	
	/**
	 * function allow change properties "company_name" value
	 * @param $p_company_name: string value
	 */
	public function set_company_name($p_company_name){
		$this->v_company_name = $p_company_name;
	}

	
	/**
	 * function return properties "address_unit" value
	 * @return string value
	 */
	public function get_address_unit(){
		return $this->v_address_unit;
	}

	
	/**
	 * function allow change properties "address_unit" value
	 * @param $p_address_unit: string value
	 */
	public function set_address_unit($p_address_unit){
		$this->v_address_unit = $p_address_unit;
	}

	
	/**
	 * function return properties "address_line_1" value
	 * @return string value
	 */
	public function get_address_line_1(){
		return $this->v_address_line_1;
	}

	
	/**
	 * function allow change properties "address_line_1" value
	 * @param $p_address_line_1: string value
	 */
	public function set_address_line_1($p_address_line_1){
		$this->v_address_line_1 = $p_address_line_1;
	}

	
	/**
	 * function return properties "address_line_2" value
	 * @return string value
	 */
	public function get_address_line_2(){
		return $this->v_address_line_2;
	}

	
	/**
	 * function allow change properties "address_line_2" value
	 * @param $p_address_line_2: string value
	 */
	public function set_address_line_2($p_address_line_2){
		$this->v_address_line_2 = $p_address_line_2;
	}

	
	/**
	 * function return properties "address_line_3" value
	 * @return string value
	 */
	public function get_address_line_3(){
		return $this->v_address_line_3;
	}

	
	/**
	 * function allow change properties "address_line_3" value
	 * @param $p_address_line_3: string value
	 */
	public function set_address_line_3($p_address_line_3){
		$this->v_address_line_3 = $p_address_line_3;
	}

	
	/**
	 * function return properties "address_postal" value
	 * @return string value
	 */
	public function get_address_postal(){
		return $this->v_address_postal;
	}

	
	/**
	 * function allow change properties "address_postal" value
	 * @param $p_address_postal: string value
	 */
	public function set_address_postal($p_address_postal){
		$this->v_address_postal = $p_address_postal;
	}

	
	/**
	 * function return properties "address_city" value
	 * @return string value
	 */
	public function get_address_city(){
		return $this->v_address_city;
	}

	
	/**
	 * function allow change properties "address_city" value
	 * @param $p_address_city: string value
	 */
	public function set_address_city($p_address_city){
		$this->v_address_city = $p_address_city;
	}

	
	/**
	 * function return properties "address_province" value
	 * @return string value
	 */
	public function get_address_province(){
		return $this->v_address_province;
	}

	
	/**
	 * function allow change properties "address_province" value
	 * @param $p_address_province: string value
	 */
	public function set_address_province($p_address_province){
		$this->v_address_province = $p_address_province;
	}

	
	/**
	 * function return properties "address_country" value
	 * @return string value
	 */
	public function get_address_country(){
		return $this->v_address_country;
	}

	
	/**
	 * function allow change properties "address_country" value
	 * @param $p_address_country: string value
	 */
	public function set_address_country($p_address_country){
		$this->v_address_country = $p_address_country;
	}

	
	/**
	 * function return properties "company_id" value
	 * @return int value
	 */
	public function get_company_id(){
		return (int) $this->v_company_id;
	}

	
	/**
	 * function allow change properties "company_id" value
	 * @param $p_company_id: int value
	 */
	public function set_company_id($p_company_id){
		$this->v_company_id = (int) $p_company_id;
	}

	
	/**
	 * function return properties "product_id" value
	 * @return int value
	 */
	public function get_product_id(){
		return (int) $this->v_product_id;
	}

	
	/**
	 * function allow change properties "product_id" value
	 * @param $p_product_id: int value
	 */
	public function set_product_id($p_product_id){
		$this->v_product_id = (int) $p_product_id;
	}

	
	/**
	 * function return properties "product_similar_id" value
	 * @return int value
	 */
	public function get_product_similar_id(){
		return (int) $this->v_product_similar_id;
	}

	
	/**
	 * function allow change properties "product_similar_id" value
	 * @param $p_product_similar_id: int value
	 */
	public function set_product_similar_id($p_product_similar_id){
		$this->v_product_similar_id = (int) $p_product_similar_id;
	}

	
	/**
	 * function return properties "product_sku" value
	 * @return string value
	 */
	public function get_product_sku(){
		return $this->v_product_sku;
	}

	
	/**
	 * function allow change properties "product_sku" value
	 * @param $p_product_sku: string value
	 */
	public function set_product_sku($p_product_sku){
		$this->v_product_sku = $p_product_sku;
	}

	
	/**
	 * function return properties "short_description" value
	 * @return string value
	 */
	public function get_short_description(){
		return $this->v_short_description;
	}

	
	/**
	 * function allow change properties "short_description" value
	 * @param $p_short_description: string value
	 */
	public function set_short_description($p_short_description){
		$this->v_short_description = $p_short_description;
	}

	
	/**
	 * function return properties "product_quantity" value
	 * @return string value
	 */
	public function get_product_quantity(){
		return $this->v_product_quantity;
	}

	
	/**
	 * function allow change properties "product_quantity" value
	 * @param $p_product_quantity: string value
	 */
	public function set_product_quantity($p_product_quantity){
		$this->v_product_quantity = $p_product_quantity;
	}

	
	/**
	 * function return properties "product_status" value
	 * @return int value
	 */
	public function get_product_status(){
		return (int) $this->v_product_status;
	}

	
	/**
	 * function allow change properties "product_status" value
	 * @param $p_product_status: int value
	 */
	public function set_product_status($p_product_status){
		$this->v_product_status = (int) $p_product_status;
	}

	
	/**
	 * function return properties "product_image" value
	 * @return string value
	 */
	public function get_product_image(){
		return $this->v_product_image;
	}

	
	/**
	 * function allow change properties "product_image" value
	 * @param $p_product_image: string value
	 */
	public function set_product_image($p_product_image){
		$this->v_product_image = $p_product_image;
	}

	
	/**
	 * function return properties "product_material" value
	 * @return array value
	 */
	public function get_product_material(){
		return  $this->arr_product_material;
	}

	
	/**
	 * function allow change properties "product_material" value
	 * @param $arr_product_material array
	 */
	public function set_product_material(array $arr_product_material = array()){
		$this->arr_product_material = $arr_product_material;
	}

	
	/**
	 * function return properties "product_price" value
	 * @return float value
	 */
	public function get_product_price(){
		return (float) $this->v_product_price;
	}

	
	/**
	 * function allow change properties "product_price" value
	 * @param $p_product_price: float value
	 */
	public function set_product_price($p_product_price){
		$this->v_product_price = (float) $p_product_price;
	}

	
	/**
	 * function return properties "excel_col" value
	 * @return string value
	 */
	public function get_excel_col(){
		return $this->v_excel_col;
	}

	
	/**
	 * function allow change properties "excel_col" value
	 * @param $p_excel_col: string value
	 */
	public function set_excel_col($p_excel_col){
		$this->v_excel_col = $p_excel_col;
	}

	
	/**
	 * function return properties "excel_file" value
	 * @return string value
	 */
	public function get_excel_file(){
		return $this->v_excel_file;
	}

	
	/**
	 * function allow change properties "excel_file" value
	 * @param $p_excel_file: string value
	 */
	public function set_excel_file($p_excel_file){
		$this->v_excel_file = $p_excel_file;
	}

	
	/**
	 * function return properties "product_accept" value
	 * @return string value
	 */
	public function get_product_accept(){
		return $this->v_product_accept;
	}

	
	/**
	 * function allow change properties "product_accept" value
	 * @param $p_product_accept: string value
	 */
	public function set_product_accept($p_product_accept){
		$this->v_product_accept = $p_product_accept;
	}

	
	/**
	 * function return properties "material_detail" value
	 * @return string value
	 */
	public function get_material_detail(){
		return $this->v_material_detail;
	}

	
	/**
	 * function allow change properties "material_detail" value
	 * @param $p_material_detail: string value
	 */
	public function set_material_detail($p_material_detail){
		$this->v_material_detail = $p_material_detail;
	}

	
	/**
	 * function return properties "user_name" value
	 * @return string value
	 */
	public function get_user_name(){
		return $this->v_user_name;
	}

	
	/**
	 * function allow change properties "user_name" value
	 * @param $p_user_name: string value
	 */
	public function set_user_name($p_user_name){
		$this->v_user_name = $p_user_name;
	}

	
	/**
	 * function return properties "user_company" value
	 * @return int value
	 */
	public function get_user_company(){
		return (int) $this->v_user_company;
	}

	
	/**
	 * function allow change properties "user_company" value
	 * @param $p_user_company: int value
	 */
	public function set_user_company($p_user_company){
		$this->v_user_company = (int) $p_user_company;
	}

	
	/**
	 * function return properties "upload_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_upload_time(){
		return  $this->v_upload_time->sec;
	}

	
	/**
	 * function allow change properties "upload_time" value
	 * @param $p_upload_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_upload_time($p_upload_time){
		if($p_upload_time=='') $p_upload_time = NULL;
		if(!is_null($p_upload_time)){
			try{
				$this->v_upload_time = new MongoDate(strtotime($p_upload_time));
			}catch(MongoException $me){
				$this->v_upload_time = NULL;
			}
		}else{
			$this->v_upload_time = NULL;
		}
	}

	
	/**
	 * function return properties "upload_session" value
	 * @return string value
	 */
	public function get_upload_session(){
		return $this->v_upload_session;
	}

	
	/**
	 * function allow change properties "upload_session" value
	 * @param $p_upload_session: string value
	 */
	public function set_upload_session($p_upload_session){
		$this->v_upload_session = $p_upload_session;
	}

	
	/**
	 * function return properties "upload_key" value
	 * @return string value
	 */
	public function get_upload_key(){
		return $this->v_upload_key;
	}

	
	/**
	 * function allow change properties "upload_key" value
	 * @param $p_upload_key: string value
	 */
	public function set_upload_key($p_upload_key){
		$this->v_upload_key = $p_upload_key;
	}

	
	/**
	 * function return properties "order_id" value
	 * @return int value
	 */
	public function get_order_id(){
		return (int) $this->v_order_id;
	}

	
	/**
	 * function allow change properties "order_id" value
	 * @param $p_order_id: int value
	 */
	public function set_order_id($p_order_id){
		$this->v_order_id = (int) $p_order_id;
	}

	
	/**
	 * function return properties "order_item_id" value
	 * @return int value
	 */
	public function get_order_item_id(){
		return (int) $this->v_order_item_id;
	}

	
	/**
	 * function allow change properties "order_item_id" value
	 * @param $p_order_item_id: int value
	 */
	public function set_order_item_id($p_order_item_id){
		$this->v_order_item_id = (int) $p_order_item_id;
	}

	
	/**
	 * function return properties "error_field" value
	 * @return array value
	 */
	public function get_error_field(){
		return  $this->arr_error_field;
	}

	
	/**
	 * function allow change properties "error_field" value
	 * @param $arr_error_field array
	 */
	public function set_error_field(array $arr_error_field = array()){
		$this->arr_error_field = $arr_error_field;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
		$arr = array('row_id' => $this->v_row_id
					,'location_id' => $this->v_location_id
					,'location_number' => $this->v_location_number
					,'location_name' => $this->v_location_name
					,'kitting_cost' => $this->v_kitting_cost
					,'company_name' => $this->v_company_name
					,'address_unit' => $this->v_address_unit
					,'address_line_1' => $this->v_address_line_1
					,'address_line_2' => $this->v_address_line_2
					,'address_line_3' => $this->v_address_line_3
					,'address_postal' => $this->v_address_postal
					,'address_city' => $this->v_address_city
					,'address_province' => $this->v_address_province
					,'address_country' => $this->v_address_country
					,'company_id' => $this->v_company_id
					,'product_id' => $this->v_product_id
					,'product_similar_id' => $this->v_product_similar_id
					,'product_sku' => $this->v_product_sku
					,'short_description' => $this->v_short_description
					,'product_quantity' => $this->v_product_quantity
					,'product_status' => $this->v_product_status
					,'product_image' => $this->v_product_image
					,'product_material' => $this->arr_product_material
					,'product_price' => $this->v_product_price
					,'excel_col' => $this->v_excel_col
					,'excel_file' => $this->v_excel_file
					,'product_accept' => $this->v_product_accept
					,'material_detail' => $this->v_material_detail
					,'user_name' => $this->v_user_name
					,'user_company' => $this->v_user_company
					,'upload_time' => $this->v_upload_time
					,'upload_session' => $this->v_upload_session
					,'upload_key' => $this->v_upload_key
					,'order_id' => $this->v_order_id
					,'order_item_id' => $this->v_order_item_id
					,'error_field' => $this->arr_error_field);
		try{
			$this->collection->insert($arr, array('safe'=>true));
			$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_upload_order` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_row_id = isset($arr['row_id'])?$arr['row_id']:0;
			$this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
			$this->v_location_number = isset($arr['location_number'])?$arr['location_number']:'';
			$this->v_location_name = isset($arr['location_name'])?$arr['location_name']:'';
			$this->v_kitting_cost = isset($arr['kitting_cost'])?$arr['kitting_cost']:0;
			$this->v_company_name = isset($arr['company_name'])?$arr['company_name']:'';
			$this->v_address_unit = isset($arr['address_unit'])?$arr['address_unit']:'';
			$this->v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
			$this->v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
			$this->v_address_line_3 = isset($arr['address_line_3'])?$arr['address_line_3']:'';
			$this->v_address_postal = isset($arr['address_postal'])?$arr['address_postal']:'';
			$this->v_address_city = isset($arr['address_city'])?$arr['address_city']:'';
			$this->v_address_province = isset($arr['address_province'])?$arr['address_province']:'';
			$this->v_address_country = isset($arr['address_country'])?$arr['address_country']:'';
			$this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
			$this->v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
			$this->v_product_similar_id = isset($arr['product_similar_id'])?$arr['product_similar_id']:0;
			$this->v_product_sku = isset($arr['product_sku'])?$arr['product_sku']:'';
			$this->v_short_description = isset($arr['short_description'])?$arr['short_description']:'';
			$this->v_product_quantity = isset($arr['product_quantity'])?$arr['product_quantity']:'0';
			$this->v_product_status = isset($arr['product_status'])?$arr['product_status']:0;
			$this->v_product_image = isset($arr['product_image'])?$arr['product_image']:'0';
			$this->arr_product_material = isset($arr['product_material'])?$arr['product_material']:array();
			$this->v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
			$this->v_excel_col = isset($arr['excel_col'])?$arr['excel_col']:'';
			$this->v_excel_file = isset($arr['excel_file'])?$arr['excel_file']:'';
			$this->v_product_accept = isset($arr['product_accept'])?$arr['product_accept']:'';
			$this->v_material_detail = isset($arr['material_detail'])?$arr['material_detail']:'';
			$this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
			$this->v_user_company = isset($arr['user_company'])?$arr['user_company']:0;
			$this->v_upload_time = isset($arr['upload_time'])?$arr['upload_time']:(new MongoDate(time()));
			$this->v_upload_session = isset($arr['upload_session'])?$arr['upload_session']:'';
			$this->v_upload_key = isset($arr['upload_key'])?$arr['upload_key']:'';
			$this->v_order_id = isset($arr['order_id'])?$arr['order_id']:0;
			$this->v_order_item_id = isset($arr['order_item_id'])?$arr['order_item_id']:0;
			$this->arr_error_field = isset($arr['error_field'])?$arr['error_field']:array();
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `row_id` FROM `tb_upload_order` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select_scalar('row_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `row_id` FROM `tb_upload_order` WHERE `user_id`=2 ORDER BY `row_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select_next('row_id',array('user_id'=>2), array('row_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return ((int) $v_ret)+1;
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
	
	/**
	 * function select distinct
	 * @param $p_field_name string, name of selected field
	 * @example: 
	 * <code>
	 *         SELECT DISTINCT `name` FROM `tbl_users`
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select_distinct('nam')
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_distinct($p_field_name){
		return $this->collection->command(array("distinct"=>"tb_upload_order", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_upload_order($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('row_id' => $this->v_row_id,'location_id' => $this->v_location_id,'location_number' => $this->v_location_number,'location_name' => $this->v_location_name,'kitting_cost' => $this->v_kitting_cost,'company_name' => $this->v_company_name,'address_unit' => $this->v_address_unit,'address_line_1' => $this->v_address_line_1,'address_line_2' => $this->v_address_line_2,'address_line_3' => $this->v_address_line_3,'address_postal' => $this->v_address_postal,'address_city' => $this->v_address_city,'address_province' => $this->v_address_province,'address_country' => $this->v_address_country,'company_id' => $this->v_company_id,'product_id' => $this->v_product_id,'product_similar_id' => $this->v_product_similar_id,'product_sku' => $this->v_product_sku,'short_description' => $this->v_short_description,'product_quantity' => $this->v_product_quantity,'product_status' => $this->v_product_status,'product_image' => $this->v_product_image,'product_material' => $this->arr_product_material,'product_price' => $this->v_product_price,'excel_col' => $this->v_excel_col,'excel_file' => $this->v_excel_file,'product_accept' => $this->v_product_accept,'material_detail' => $this->v_material_detail,'user_name' => $this->v_user_name,'user_company' => $this->v_user_company,'upload_time' => $this->v_upload_time,'upload_session' => $this->v_upload_session,'upload_key' => $this->v_upload_key,'order_id' => $this->v_order_id,'order_item_id' => $this->v_order_item_id,'error_field' => $this->arr_error_field));
		 else 
			$arr = array('$set' => array('location_id' => $this->v_location_id,'location_number' => $this->v_location_number,'location_name' => $this->v_location_name,'kitting_cost' => $this->v_kitting_cost,'company_name' => $this->v_company_name,'address_unit' => $this->v_address_unit,'address_line_1' => $this->v_address_line_1,'address_line_2' => $this->v_address_line_2,'address_line_3' => $this->v_address_line_3,'address_postal' => $this->v_address_postal,'address_city' => $this->v_address_city,'address_province' => $this->v_address_province,'address_country' => $this->v_address_country,'company_id' => $this->v_company_id,'product_id' => $this->v_product_id,'product_similar_id' => $this->v_product_similar_id,'product_sku' => $this->v_product_sku,'short_description' => $this->v_short_description,'product_quantity' => $this->v_product_quantity,'product_status' => $this->v_product_status,'product_image' => $this->v_product_image,'product_material' => $this->arr_product_material,'product_price' => $this->v_product_price,'excel_col' => $this->v_excel_col,'excel_file' => $this->v_excel_file,'product_accept' => $this->v_product_accept,'material_detail' => $this->v_material_detail,'user_name' => $this->v_user_name,'user_company' => $this->v_user_company,'upload_time' => $this->v_upload_time,'upload_session' => $this->v_upload_session,'upload_key' => $this->v_upload_key,'order_id' => $this->v_order_id,'order_item_id' => $this->v_order_item_id,'error_field' => $this->arr_error_field));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true,'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>