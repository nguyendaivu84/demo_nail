<?php
class cls_tb_design_design{

	private $v_design_id = 0;
	private $v_design_name = '';
	private $v_design_data = '';
	private $v_design_width = 0;
	private $v_design_height = 0;
	private $v_design_folding = 0;
	private $v_design_direction = 0;
	private $v_design_die_cut = 0;
	private $v_design_image = '';
	private $v_design_status = 0;
	private $v_design_bleed = 0;
	private $v_design_dpi = 0;
	private $v_markup_cost = 0;
	private $v_print_cost = 0;
	private $v_stock_cost = 0;
	private $v_created_time = '0000-00-00 00:00:00';
	private $v_template_id = 0;
	private $v_theme_id = 0;
	private $v_product_id = 0;
	private $v_user_id = 0;
	private $v_user_name = '';
	private $v_user_ip = '';
	private $v_user_agent = '';
	private $v_sample_image = '';
	private $v_saved_dir = '';
	private $v_company_id = 0;
	private $v_location_id = 0;
	private $arr_design_share = array();
	private $v_design_key_id = 0;
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
	
	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection('tb_design_design');
		$this->v_created_time = new MongoDate(time());
		$this->collection->ensureIndex(array("design_id"=>1), array('name'=>"design_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = 'tb_design_design';
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "design_id" value
	 * @return int value
	 */
	public function get_design_id(){
		return (int) $this->v_design_id;
	}

	
	/**
	 * function allow change properties "design_id" value
	 * @param $p_design_id: int value
	 */
	public function set_design_id($p_design_id){
		$this->v_design_id = (int) $p_design_id;
	}

	
	/**
	 * function return properties "design_name" value
	 * @return string value
	 */
	public function get_design_name(){
		return $this->v_design_name;
	}

	
	/**
	 * function allow change properties "design_name" value
	 * @param $p_design_name: string value
	 */
	public function set_design_name($p_design_name){
		$this->v_design_name = $p_design_name;
	}

	
	/**
	 * function return properties "design_data" value
	 * @return string value
	 */
	public function get_design_data(){
		return $this->v_design_data;
	}

	
	/**
	 * function allow change properties "design_data" value
	 * @param $p_design_data: string value
	 */
	public function set_design_data($p_design_data){
		$this->v_design_data = $p_design_data;
	}

	
	/**
	 * function return properties "design_width" value
	 * @return float value
	 */
	public function get_design_width(){
		return (float) $this->v_design_width;
	}

	
	/**
	 * function allow change properties "design_width" value
	 * @param $p_design_width: float value
	 */
	public function set_design_width($p_design_width){
		$this->v_design_width = (float) $p_design_width;
	}

	
	/**
	 * function return properties "design_height" value
	 * @return float value
	 */
	public function get_design_height(){
		return (float) $this->v_design_height;
	}

	
	/**
	 * function allow change properties "design_height" value
	 * @param $p_design_height: float value
	 */
	public function set_design_height($p_design_height){
		$this->v_design_height = (float) $p_design_height;
	}

	
	/**
	 * function return properties "design_folding" value
	 * @return int value
	 */
	public function get_design_folding(){
		return (int) $this->v_design_folding;
	}

	
	/**
	 * function allow change properties "design_folding" value
	 * @param $p_design_folding: int value
	 */
	public function set_design_folding($p_design_folding){
		$this->v_design_folding = (int) $p_design_folding;
	}

    /**
     * function return properties "design_dpi" value
     * @return int value
     */
    public function get_design_dpi(){
        return (int) $this->v_design_dpi;
    }


    /**
     * function allow change properties "design_dpi" value
     * @param $p_design_dpi: int value
     */
    public function set_design_dpi($p_design_dpi){
        $this->v_design_dpi = (int) $p_design_dpi;
    }


    /**
	 * function return properties "design_direction" value
	 * @return int value
	 */
	public function get_design_direction(){
		return (int) $this->v_design_direction;
	}

	
	/**
	 * function allow change properties "design_direction" value
	 * @param $p_design_direction: int value
	 */
	public function set_design_direction($p_design_direction){
		$this->v_design_direction = (int) $p_design_direction;
	}

	
	/**
	 * function return properties "design_die_cut" value
	 * @return int value
	 */
	public function get_design_die_cut(){
		return (int) $this->v_design_die_cut;
	}

	
	/**
	 * function allow change properties "design_die_cut" value
	 * @param $p_design_die_cut: int value
	 */
	public function set_design_die_cut($p_design_die_cut){
		$this->v_design_die_cut = (int) $p_design_die_cut;
	}

	
	/**
	 * function return properties "design_image" value
	 * @return string value
	 */
	public function get_design_image(){
		return $this->v_design_image;
	}

	
	/**
	 * function allow change properties "design_image" value
	 * @param $p_design_image: string value
	 */
	public function set_design_image($p_design_image){
		$this->v_design_image = $p_design_image;
	}

	
	/**
	 * function return properties "design_status" value
	 * @return int value
	 */
	public function get_design_status(){
		return (int) $this->v_design_status;
	}

	
	/**
	 * function allow change properties "design_status" value
	 * @param $p_design_status: int value
	 */
	public function set_design_status($p_design_status){
		$this->v_design_status = (int) $p_design_status;
	}

	
	/**
	 * function return properties "design_bleed" value
	 * @return float value
	 */
	public function get_design_bleed(){
		return (float) $this->v_design_bleed;
	}

	
	/**
	 * function allow change properties "design_bleed" value
	 * @param $p_design_bleed: float value
	 */
	public function set_design_bleed($p_design_bleed){
		$this->v_design_bleed = (float) $p_design_bleed;
	}

	
	/**
	 * function return properties "markup_cost" value
	 * @return float value
	 */
	public function get_markup_cost(){
		return (float) $this->v_markup_cost;
	}

	
	/**
	 * function allow change properties "markup_cost" value
	 * @param $p_markup_cost: float value
	 */
	public function set_markup_cost($p_markup_cost){
		$this->v_markup_cost = (float) $p_markup_cost;
	}

	
	/**
	 * function return properties "print_cost" value
	 * @return float value
	 */
	public function get_print_cost(){
		return (float) $this->v_print_cost;
	}

	
	/**
	 * function allow change properties "print_cost" value
	 * @param $p_print_cost: float value
	 */
	public function set_print_cost($p_print_cost){
		$this->v_print_cost = (float) $p_print_cost;
	}

	
	/**
	 * function return properties "stock_cost" value
	 * @return float value
	 */
	public function get_stock_cost(){
		return (float) $this->v_stock_cost;
	}

	
	/**
	 * function allow change properties "stock_cost" value
	 * @param $p_stock_cost: float value
	 */
	public function set_stock_cost($p_stock_cost){
		$this->v_stock_cost = (float) $p_stock_cost;
	}

	
	/**
	 * function return properties "created_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_created_time(){
		return  $this->v_created_time->sec;
	}

	
	/**
	 * function allow change properties "created_time" value
	 * @param $p_created_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_created_time($p_created_time){
		if($p_created_time=='') $p_created_time = NULL;
		if(!is_null($p_created_time)){
			try{
				$this->v_created_time = new MongoDate(strtotime($p_created_time));
			}catch(MongoException $me){
				$this->v_created_time = NULL;
			}
		}else{
			$this->v_created_time = NULL;
		}
	}

	
	/**
	 * function return properties "template_id" value
	 * @return int value
	 */
	public function get_template_id(){
		return (int) $this->v_template_id;
	}

	
	/**
	 * function allow change properties "template_id" value
	 * @param $p_template_id: int value
	 */
	public function set_template_id($p_template_id){
		$this->v_template_id = (int) $p_template_id;
	}

	
	/**
	 * function return properties "theme_id" value
	 * @return int value
	 */
	public function get_theme_id(){
		return (int) $this->v_theme_id;
	}

	
	/**
	 * function allow change properties "theme_id" value
	 * @param $p_theme_id: int value
	 */
	public function set_theme_id($p_theme_id){
		$this->v_theme_id = (int) $p_theme_id;
	}

	
	/**
	 * function return properties "product_id" value
	 * @return int value
	 */
	public function get_product_id(){
		return (int) $this->v_product_id;
	}

	
	/**
	 * function allow change properties "product_id" value
	 * @param $p_product_id: int value
	 */
	public function set_product_id($p_product_id){
		$this->v_product_id = (int) $p_product_id;
	}

	
	/**
	 * function return properties "user_id" value
	 * @return int value
	 */
	public function get_user_id(){
		return (int) $this->v_user_id;
	}

	
	/**
	 * function allow change properties "user_id" value
	 * @param $p_user_id: int value
	 */
	public function set_user_id($p_user_id){
		$this->v_user_id = (int) $p_user_id;
	}

	
	/**
	 * function return properties "user_name" value
	 * @return string value
	 */
	public function get_user_name(){
		return $this->v_user_name;
	}

	
	/**
	 * function allow change properties "user_name" value
	 * @param $p_user_name: string value
	 */
	public function set_user_name($p_user_name){
		$this->v_user_name = $p_user_name;
	}

	
	/**
	 * function return properties "user_ip" value
	 * @return string value
	 */
	public function get_user_ip(){
		return $this->v_user_ip;
	}

	
	/**
	 * function allow change properties "user_ip" value
	 * @param $p_user_ip: string value
	 */
	public function set_user_ip($p_user_ip){
		$this->v_user_ip = $p_user_ip;
	}

	
	/**
	 * function return properties "user_agent" value
	 * @return string value
	 */
	public function get_user_agent(){
		return $this->v_user_agent;
	}

	
	/**
	 * function allow change properties "user_agent" value
	 * @param $p_user_agent: string value
	 */
	public function set_user_agent($p_user_agent){
		$this->v_user_agent = $p_user_agent;
	}

	
	/**
	 * function return properties "sample_image" value
	 * @return string value
	 */
	public function get_sample_image(){
		return $this->v_sample_image;
	}

	
	/**
	 * function allow change properties "sample_image" value
	 * @param $p_sample_image: string value
	 */
	public function set_sample_image($p_sample_image){
		$this->v_sample_image = $p_sample_image;
	}

	
	/**
	 * function return properties "saved_dir" value
	 * @return string value
	 */
	public function get_saved_dir(){
		return $this->v_saved_dir;
	}

	
	/**
	 * function allow change properties "saved_dir" value
	 * @param $p_saved_dir: string value
	 */
	public function set_saved_dir($p_saved_dir){
		$this->v_saved_dir = $p_saved_dir;
	}

	
	/**
	 * function return properties "company_id" value
	 * @return int value
	 */
	public function get_company_id(){
		return (int) $this->v_company_id;
	}

	
	/**
	 * function allow change properties "company_id" value
	 * @param $p_company_id: int value
	 */
	public function set_company_id($p_company_id){
		$this->v_company_id = (int) $p_company_id;
	}

	
	/**
	 * function return properties "location_id" value
	 * @return int value
	 */
	public function get_location_id(){
		return (int) $this->v_location_id;
	}

	
	/**
	 * function allow change properties "location_id" value
	 * @param $p_location_id: int value
	 */
	public function set_location_id($p_location_id){
		$this->v_location_id = (int) $p_location_id;
	}

	
	/**
	 * function return properties "design_share" value
	 * @return array value
	 */
	public function get_design_share(){
		return  $this->arr_design_share;
	}

	
	/**
	 * function allow change properties "design_share" value
	 * @param $arr_design_share array
	 */
	public function set_design_share(array $arr_design_share = array()){
		$this->arr_design_share = $arr_design_share;
	}

	
	/**
	 * function return properties "design_key_id" value
	 * @return int value
	 */
	public function get_design_key_id(){
		return (int) $this->v_design_key_id;
	}

	
	/**
	 * function allow change properties "design_key_id" value
	 * @param $p_design_key_id: int value
	 */
	public function set_design_key_id($p_design_key_id){
		$this->v_design_key_id = (int) $p_design_key_id;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
		$arr = array('design_id' => $this->v_design_id
					,'design_name' => $this->v_design_name
					,'design_data' => $this->v_design_data
					,'design_width' => $this->v_design_width
					,'design_height' => $this->v_design_height
					,'design_folding' => $this->v_design_folding
					,'design_direction' => $this->v_design_direction
					,'design_die_cut' => $this->v_design_die_cut
					,'design_image' => $this->v_design_image
					,'design_status' => $this->v_design_status
					,'design_bleed' => $this->v_design_bleed
					,'design_dpi' => $this->v_design_dpi
					,'markup_cost' => $this->v_markup_cost
					,'print_cost' => $this->v_print_cost
					,'stock_cost' => $this->v_stock_cost
					,'created_time' => $this->v_created_time
					,'template_id' => $this->v_template_id
					,'theme_id' => $this->v_theme_id
					,'product_id' => $this->v_product_id
					,'user_id' => $this->v_user_id
					,'user_name' => $this->v_user_name
					,'user_ip' => $this->v_user_ip
					,'user_agent' => $this->v_user_agent
					,'sample_image' => $this->v_sample_image
					,'saved_dir' => $this->v_saved_dir
					,'company_id' => $this->v_company_id
					,'location_id' => $this->v_location_id
					,'design_share' => $this->arr_design_share
					,'design_key_id' => $this->v_design_key_id);
		try{
			$this->collection->insert($arr, array('safe'=>true));
			$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_design_design` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
			$this->v_design_name = isset($arr['design_name'])?$arr['design_name']:'';
			$this->v_design_data = isset($arr['design_data'])?$arr['design_data']:'';
			$this->v_design_width = isset($arr['design_width'])?$arr['design_width']:0;
			$this->v_design_height = isset($arr['design_height'])?$arr['design_height']:0;
			$this->v_design_folding = isset($arr['design_folding'])?$arr['design_folding']:0;
			$this->v_design_direction = isset($arr['design_direction'])?$arr['design_direction']:0;
			$this->v_design_die_cut = isset($arr['design_die_cut'])?$arr['design_die_cut']:0;
			$this->v_design_image = isset($arr['design_image'])?$arr['design_image']:'';
			$this->v_design_status = isset($arr['design_status'])?$arr['design_status']:0;
			$this->v_design_bleed = isset($arr['design_bleed'])?$arr['design_bleed']:0;
			$this->v_design_dpi = isset($arr['design_dpi'])?$arr['design_dpi']:0;
			$this->v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
			$this->v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
			$this->v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
			$this->v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
			$this->v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
			$this->v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
			$this->v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
			$this->v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
			$this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
			$this->v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
			$this->v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
			$this->v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
			$this->v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
			$this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
			$this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
			$this->arr_design_share = isset($arr['design_share'])?$arr['design_share']:array();
			$this->v_design_key_id = isset($arr['design_key_id'])?$arr['design_key_id']:0;
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `design_id` FROM `tb_design_design` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select_scalar('design_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `design_id` FROM `tb_design_design` WHERE `user_id`=2 ORDER BY `design_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select_next('design_id',array('user_id'=>2), array('design_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return ((int) $v_ret)+1;
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
	
	/**
	 * function select distinct
	 * @param $p_field_name string, name of selected field
	 * @example: 
	 * <code>
	 *         SELECT DISTINCT `name` FROM `tbl_users`
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select_distinct('nam')
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_distinct($p_field_name){
		return $this->collection->command(array("distinct"=>"tb_design_design", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_design($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('design_id' => $this->v_design_id,'design_name' => $this->v_design_name,'design_data' => $this->v_design_data,'design_width' => $this->v_design_width,'design_height' => $this->v_design_height,'design_folding' => $this->v_design_folding,'design_direction' => $this->v_design_direction,'design_die_cut' => $this->v_design_die_cut,'design_image' => $this->v_design_image,'design_status' => $this->v_design_status,'design_bleed' => $this->v_design_bleed,'markup_cost' => $this->v_markup_cost,'print_cost' => $this->v_print_cost,'stock_cost' => $this->v_stock_cost,'created_time' => $this->v_created_time,'template_id' => $this->v_template_id,'theme_id' => $this->v_theme_id,'product_id' => $this->v_product_id,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'user_ip' => $this->v_user_ip,'user_agent' => $this->v_user_agent,'sample_image' => $this->v_sample_image,'saved_dir' => $this->v_saved_dir,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id,'design_share' => $this->arr_design_share,'design_key_id' => $this->v_design_key_id, 'design_dpi'=>$this->v_design_dpi));
		 else 
			$arr = array('$set' => array('design_name' => $this->v_design_name,'design_data' => $this->v_design_data,'design_width' => $this->v_design_width,'design_height' => $this->v_design_height,'design_folding' => $this->v_design_folding,'design_direction' => $this->v_design_direction,'design_die_cut' => $this->v_design_die_cut,'design_image' => $this->v_design_image,'design_status' => $this->v_design_status,'design_bleed' => $this->v_design_bleed,'markup_cost' => $this->v_markup_cost,'print_cost' => $this->v_print_cost,'stock_cost' => $this->v_stock_cost,'created_time' => $this->v_created_time,'template_id' => $this->v_template_id,'theme_id' => $this->v_theme_id,'product_id' => $this->v_product_id,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'user_ip' => $this->v_user_ip,'user_agent' => $this->v_user_agent,'sample_image' => $this->v_sample_image,'saved_dir' => $this->v_saved_dir,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id,'design_share' => $this->arr_design_share,'design_key_id' => $this->v_design_key_id, 'design_dpi'=>$this->v_design_dpi));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>