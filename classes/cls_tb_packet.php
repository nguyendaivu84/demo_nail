<?php
class cls_tb_packet{

    private $v_packet_id = 0;
    private $v_packet_overview = '';
    private $v_packet_specs = '';
    private $v_packet_gallery = '';
    private $v_packet_option = '';
    private $v_packet_sku = '';
    private $v_packet_short_description = '';
    private $v_packet_long_description = '';
    private $v_packet_detail = '';
    private $v_packet_total_quantity = 0;
    private $v_packet_type = 0;
    private $arr_packet_content = array();
    private $arr_tag = array();
    private $v_packet_image_file = '';
    private $v_packet_image_desc = '' ;
    private $v_packet_image_dir = '';
    private $v_packet_total_price = 0.0;
    private $v_packet_status = 0;
    private $v_packet_user_create_name = '';
    private $v_packet_user_create_type = '';
    private $v_create_time = '0000-00-00 00:00:00';
    private $collection = NULL;
    private $v_mongo_id = NULL;
    private $v_error_code = 0;
    private $v_error_message = '';
    private $v_is_log = false;
    private $v_dir = '';

    /**
     *  constructor function
     *  @param $db: instance of Mongo
     *  @param $p_log_dir string: directory contains its log file
     */
    public function __construct(MongoDB $db, $p_log_dir = ""){
        $this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
        if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
        $this->collection = $db->selectCollection('tb_packet');
        $this->v_create_time = new MongoDate(time());
        $this->collection->ensureIndex(array("packet_id"=>1), array('name'=>"packet_id_key", "unique"=>1, "dropDups" => 1));
    }

    /**
     *  function get current MongoDB collection
     *  @return Object: current MongoDB collection
     */
    public function get_collection(){
        return $this->collection;
    }

    /**
     *  function write log
     */
    private function my_error(){
        if(! $this->v_is_log) return;
        global $_SERVER;
        $v_filename = 'tb_packet';
        $v_ext = '.log';
        $v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
        $v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
        $v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
        $v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
        $v_log_str .= "\r\n----------------End Log-----------------";
        $v_log_str .= "\r\n";
        $v_new_file = false;
        if(file_exists($this->v_dir.$v_filename.$v_ext)){
            if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
                rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
                $v_new_file = true;
                @unlink($this->v_dir.$v_filename.$v_ext);
            }
        }
        $fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
        if($fp){
            fwrite($fp, $v_log_str, strlen($v_log_str));
            fflush($fp);
            fclose($fp);
        }
    }

    /**
     * function return properties "packet_id" value
     * @return int value
     */
    public function get_packet_id(){
        return (int) $this->v_packet_id;
    }


    /**
     * function allow change properties "packet_id" value
     * @param $p_packet_id: int value
     */
    public function set_packet_id($p_packet_id){
        $this->v_packet_id = (int) $p_packet_id;
    }


    /**
     * function return properties "packet_overview" value
     * @return string value
     */
    public function get_packet_overview(){
        return $this->v_packet_overview;
    }


    /**
     * function allow change properties "packet_overview" value
     * @param $p_packet_overview: string value
     */
    public function set_packet_overview($p_packet_overview){
        $this->v_packet_overview = $p_packet_overview;
    }


    /**
     * function return properties "packet_specs" value
     * @return string value
     */
    public function get_packet_specs(){
        return $this->v_packet_specs;
    }


    /**
     * function allow change properties "packet_specs" value
     * @param $p_packet_specs: string value
     */
    public function set_packet_specs($p_packet_specs){
        $this->v_packet_specs = $p_packet_specs;
    }


    /**
     * function return properties "packet_gallery" value
     * @return string value
     */
    public function get_packet_gallery(){
        return $this->v_packet_gallery;
    }


    /**
     * function allow change properties "packet_gallery" value
     * @param $p_packet_gallery: string value
     */
    public function set_packet_gallery($p_packet_gallery){
        $this->v_packet_gallery = $p_packet_gallery;
    }


    /**
     * function return properties "packet_option" value
     * @return string value
     */
    public function get_packet_option(){
        return $this->v_packet_option;
    }


    /**
     * function allow change properties "packet_option" value
     * @param $p_packet_option: string value
     */
    public function set_packet_option($p_packet_option){
        $this->v_packet_option = $p_packet_option;
    }


    /**
     * function return properties "packet_sku" value
     * @return string value
     */
    public function get_packet_sku(){
        return $this->v_packet_sku;
    }


    /**
     * function allow change properties "packet_sku" value
     * @param $p_packet_sku: string value
     */
    public function set_packet_sku($p_packet_sku){
        $this->v_packet_sku = $p_packet_sku;
    }


    /**
     * function return properties "packet_short_description" value
     * @return string value
     */
    public function get_packet_short_description(){
        return $this->v_packet_short_description;
    }


    /**
     * function allow change properties "packet_short_description" value
     * @param $p_packet_short_description: string value
     */
    public function set_packet_short_description($p_packet_short_description){
        $this->v_packet_short_description = $p_packet_short_description;
    }


    /**
     * function return properties "packet_long_description" value
     * @return string value
     */
    public function get_packet_long_description(){
        return $this->v_packet_long_description;
    }


    /**
     * function allow change properties "packet_long_description" value
     * @param $p_packet_long_description: string value
     */
    public function set_packet_long_description($p_packet_long_description){
        $this->v_packet_long_description = $p_packet_long_description;
    }


    /**
     * function return properties "packet_detail" value
     * @return string value
     */
    public function get_packet_detail(){
        return $this->v_packet_detail;
    }


    /**
     * function allow change properties "packet_detail" value
     * @param $p_packet_detail: string value
     */
    public function set_packet_detail($p_packet_detail){
        $this->v_packet_detail = $p_packet_detail;
    }


    /**
     * function return properties "packet_total_quantity" value
     * @return float value
     */
    public function get_packet_total_quantity(){
        return (float) $this->v_packet_total_quantity;
    }


    /**
     * function allow change properties "packet_total_quantity" value
     * @param $p_packet_total_quantity: float value
     */
    public function set_packet_total_quantity($p_packet_total_quantity){
        $this->v_packet_total_quantity = (float) $p_packet_total_quantity;
    }


    /**
     * function return properties "packet_type" value
     * @return int value
     */
    public function get_packet_type(){
        return (int) $this->v_packet_type;
    }


    /**
     * function allow change properties "packet_type" value
     * @param $p_packet_type: int value
     */
    public function set_packet_type($p_packet_type){
        $this->v_packet_type = (int) $p_packet_type;
    }


    /**
     * function return properties "packet_content" value
     * @return array value
     */
    public function get_packet_content(){
        return  $this->arr_packet_content;
    }


    /**
     * function allow change properties "packet_content" value
     * @param $arr_packet_content array
     */
    public function set_packet_content(array $arr_packet_content = array()){
        $this->arr_packet_content = $arr_packet_content;
    }


    /**
     * function return properties "packet_image_file" value
     * @return int value
     */
    public function get_packet_image_file(){
        return (int) $this->v_packet_image_file;
    }


    /**
     * function allow change properties "packet_image_file" value
     * @param $p_packet_image_file: int value
     */
    public function set_packet_image_file($p_packet_image_file){
        $this->v_packet_image_file = (int) $p_packet_image_file;
    }


    /**
     * function return properties "packet_image_desc" value
     * @return int value
     */
    public function get_packet_image_desc(){
        return (int) $this->v_packet_image_desc;
    }


    /**
     * function allow change properties "packet_image_desc" value
     * @param $p_packet_image_desc: int value
     */
    public function set_packet_image_desc($p_packet_image_desc){
        $this->v_packet_image_desc = (int) $p_packet_image_desc;
    }


    /**
     * function return properties "packet_image_dir" value
     * @return int value
     */
    public function get_packet_image_dir(){
        return (int) $this->v_packet_image_dir;
    }


    /**
     * function allow change properties "packet_image_dir" value
     * @param $p_packet_image_dir: int value
     */
    public function set_packet_image_dir($p_packet_image_dir){
        $this->v_packet_image_dir = (int) $p_packet_image_dir;
    }


    /**
     * function return properties "packet_total_price" value
     * @return float value
     */
    public function get_packet_total_price(){
        return (float) $this->v_packet_total_price;
    }


    /**
     * function allow change properties "packet_total_price" value
     * @param $p_packet_total_price: float value
     */
    public function set_packet_total_price($p_packet_total_price){
        $this->v_packet_total_price = (float) $p_packet_total_price;
    }


    /**
     * function return properties "packet_status" value
     * @return int value
     */
    public function get_packet_status(){
        return (int) $this->v_packet_status;
    }


    /**
     * function allow change properties "packet_status" value
     * @param $p_packet_status: int value
     */
    public function set_packet_status($p_packet_status){
        $this->v_packet_status = (int) $p_packet_status;
    }


    /**
     * function return properties "packet_user_create_name" value
     * @return string value
     */
    public function get_packet_user_create_name(){
        return $this->v_packet_user_create_name;
    }


    /**
     * function allow change properties "packet_user_create_name" value
     * @param $p_packet_user_create_name: string value
     */
    public function set_packet_user_create_name($p_packet_user_create_name){
        $this->v_packet_user_create_name = $p_packet_user_create_name;
    }


    /**
     * function return properties "packet_user_create_type" value
     * @return int value
     */
    public function get_packet_user_create_type(){
        return (int) $this->v_packet_user_create_type;
    }


    /**
     * function allow change properties "packet_user_create_type" value
     * @param $p_packet_user_create_type: int value
     */
    public function set_packet_user_create_type($p_packet_user_create_type){
        $this->v_packet_user_create_type = (int) $p_packet_user_create_type;
    }


    /**
     * function return properties "create_time" value
     * @return int value indicates amount of seconds
     */
    public function get_create_time(){
        return  $this->v_create_time->sec;
    }


    /**
     * function allow change properties "create_time" value
     * @param $p_create_time: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_create_time($p_create_time){
        $this->v_create_time = new MongoDate(strtotime($p_create_time));
    }


    /**
     * function return MongoID value after inserting new record
     * @return ObjectId: MongoId
     */
    public function get_mongo_id(){
        return $this->v_mongo_id;
    }


    /**
     * function set MongoID to properties
     */
    public function set_mongo_id($p_mongo_id){
        $this->v_mongo_id = $p_mongo_id;
    }


    /**
     *  function allow insert one record
     *  @return MongoID
     */
    public function insert(){
        $arr = array('packet_id' => $this->v_packet_id
        ,'packet_overview' => $this->v_packet_overview
        ,'packet_specs' => $this->v_packet_specs
        ,'packet_gallery' => $this->v_packet_gallery
        ,'packet_option' => $this->v_packet_option
        ,'packet_sku' => $this->v_packet_sku
        ,'packet_short_description' => $this->v_packet_short_description
        ,'packet_long_description' => $this->v_packet_long_description
        ,'packet_detail' => $this->v_packet_detail
        ,'packet_total_quantity' => $this->v_packet_total_quantity
        ,'packet_type' => $this->v_packet_type
        ,'packet_content' => $this->arr_packet_content
        ,'packet_image_file' => $this->v_packet_image_file
        ,'packet_image_desc' => $this->v_packet_image_desc
        ,'packet_image_dir' => $this->v_packet_image_dir
        ,'packet_total_price' => $this->v_packet_total_price
        ,'packet_status' => $this->v_packet_status
        ,'packet_user_create_name' => $this->v_packet_user_create_name
        ,'packet_user_create_type' => $this->v_packet_user_create_type
        ,'create_time' => $this->v_create_time);
        try{
            $this->collection->insert($arr, array('safe'=>true));
            $this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     *  function allow insert array with parameter
     *  @param array $arr_fields_and_values
     *  @return MongoID
     */
    public function insert_array(array $arr_fields_and_values){
        try{
            $this->collection->insert($arr_fields_and_values, array('safe'=>true));
            $this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     * function select_one_record
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: all values will assign to this instance properties
     * @example:
     * <code>
     *       SELECT * FROM `tb_packet` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return int
     */
    public function select_one(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_count = 0;
        foreach($rss as $arr){
            $this->v_packet_id = isset($arr['packet_id'])?$arr['packet_id']:0;
            $this->v_packet_overview = isset($arr['packet_overview'])?$arr['packet_overview']:'';
            $this->v_packet_specs = isset($arr['packet_specs'])?$arr['packet_specs']:'';
            $this->v_packet_gallery = isset($arr['packet_gallery'])?$arr['packet_gallery']:'';
            $this->v_packet_option = isset($arr['packet_option'])?$arr['packet_option']:'';
            $this->v_packet_sku = isset($arr['packet_sku'])?$arr['packet_sku']:'';
            $this->v_packet_short_description = isset($arr['packet_short_description'])?$arr['packet_short_description']:'';
            $this->v_packet_long_description = isset($arr['packet_long_description'])?$arr['packet_long_description']:'';
            $this->v_packet_detail = isset($arr['packet_detail'])?$arr['packet_detail']:'';
            $this->v_packet_total_quantity = isset($arr['packet_total_quantity'])?$arr['packet_total_quantity']:0;
            $this->v_packet_type = isset($arr['packet_type'])?$arr['packet_type']:0;
            $this->arr_packet_content = isset($arr['packet_content'])?$arr['packet_content']:array();
            $this->v_packet_image_file = isset($arr['packet_image_file'])?$arr['packet_image_file']:'';
            $this->v_packet_image_desc = isset($arr['packet_image_desc'])?$arr['packet_image_desc']:'';
            $this->v_packet_image_dir = isset($arr['packet_image_dir'])?$arr['packet_image_dir']:'';
            $this->v_packet_total_price = isset($arr['packet_total_price'])?$arr['packet_total_price']:0;
            $this->v_packet_status = isset($arr['packet_status'])?$arr['packet_status']:0;
            $this->v_packet_user_create_name = isset($arr['packet_user_create_name'])?$arr['packet_user_create_name']:'';
            $this->v_packet_user_create_type = isset($arr['packet_user_create_type'])?$arr['packet_user_create_type']:0;
            $this->v_create_time = isset($arr['create_time'])?$arr['create_time']:(new MongoDate(time()));
            $this->v_mongo_id = $arr['_id'];
            $v_count++;
        }
        return $v_count;
    }

    /**
     * function select scalar value
     * @param $p_field_name string, name of field
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: assign to properties
     * @example:
     * <code>
     * SELECT `packet_id` FROM `tb_packet` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select_scalar('packet_id',array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return mixed
     */
    public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = NULL;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return $v_ret;
    }

    /**
     * function get next int value for key
     * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @result: assign to properties
     * @example:
     * <code>
     *   SELECT `packet_id` FROM `tb_packet` WHERE `user_id`=2 ORDER BY `packet_id` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select_next('packet_id',array('user_id'=>2), array('packet_id'=>-1))
     * </code>
     * @return int
     */
    public function select_next($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => -1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = 0;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return ((int) $v_ret)+1;
    }

    /**
     * function get missing value
     * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function select_missing($p_field_name, array $arr_where = array()){
        $arr_order = array(''.$p_field_name.'' => 1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_start = 1;
        $v_ret = 1;
        foreach($rss as $arr){
            if($arr[''.$p_field_name.'']!=$v_start){
                $v_ret = $v_start;
                break;
            }
            $v_start++;
        }
        return ((int) $v_ret);
    }

    /**
     * function select limit records
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr;
    }

    /**
     * function select records
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order);
        return $arr;
    }

    /**
     * function select distinct
     * @param $p_field_name string, name of selected field
     * @example:
     * <code>
     *         SELECT DISTINCT `name` FROM `tbl_users`
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select_distinct('nam')
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_distinct($p_field_name){
        return $this->collection->command(array("distinct"=>"tb_packet", "key"=>$p_field_name));
    }

    /**
     * function select limit fields
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_fields array, array of fields will be selected
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_packet($db)
     * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr_field = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr_field[$arr_fields[$i]] = 1;
        if($p_row <= 0)
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
        else
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr_return;
    }

    /**
     *  function update one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(isset($v_has_mongo_id) && $v_has_mongo_id)
            $arr = array('$set' => array('packet_id' => $this->v_packet_id,'packet_overview' => $this->v_packet_overview,'packet_specs' => $this->v_packet_specs,'packet_gallery' => $this->v_packet_gallery,'packet_option' => $this->v_packet_option,'packet_sku' => $this->v_packet_sku,'packet_short_description' => $this->v_packet_short_description,'packet_long_description' => $this->v_packet_long_description,'packet_detail' => $this->v_packet_detail,'packet_total_quantity' => $this->v_packet_total_quantity,'packet_type' => $this->v_packet_type,'packet_content' => $this->arr_packet_content,'packet_image_file' => $this->v_packet_image_file,'packet_image_desc' => $this->v_packet_image_desc,'packet_image_dir' => $this->v_packet_image_dir,'packet_total_price' => $this->v_packet_total_price,'packet_status' => $this->v_packet_status,'packet_user_create_name' => $this->v_packet_user_create_name,'packet_user_create_type' => $this->v_packet_user_create_type,'create_time' => $this->v_create_time));
        else
            $arr = array('$set' => array('packet_overview' => $this->v_packet_overview,'packet_specs' => $this->v_packet_specs,'packet_gallery' => $this->v_packet_gallery,'packet_option' => $this->v_packet_option,'packet_sku' => $this->v_packet_sku,'packet_short_description' => $this->v_packet_short_description,'packet_long_description' => $this->v_packet_long_description,'packet_detail' => $this->v_packet_detail,'packet_total_quantity' => $this->v_packet_total_quantity,'packet_type' => $this->v_packet_type,'packet_content' => $this->arr_packet_content,'packet_image_file' => $this->v_packet_image_file,'packet_image_desc' => $this->v_packet_image_desc,'packet_image_dir' => $this->v_packet_image_dir,'packet_total_price' => $this->v_packet_total_price,'packet_status' => $this->v_packet_status,'packet_user_create_name' => $this->v_packet_user_create_name,'packet_user_create_type' => $this->v_packet_user_create_type,'create_time' => $this->v_create_time));
        try{
            $this->collection->update($arr_where, $arr, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function delete one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function delete(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->remove($arr_where, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_field($p_field, $p_value, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields array, array of selected fields go to updated
     * @param $arr_values array, array of selected values go to assigned
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function increase one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields = array(), array of selected fields go to updated
     * @param $arr_values = array(), array of selected values go to increase
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function draw option tag
     * @param $p_field_value string: name of field will be value option tag
     * @param $p_field_display string: name of field will be display text option tag
     * @param $p_selected_value mixed: value of field will be display text option tag
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_exclude array: array list value of exclude
     * @return string
     */
    public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
        $arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
        $v_dsp_option = '';
        foreach($arr as $a){
            if(!in_array($a[$p_field_value],$arr_exclude)){
                if($a[$p_field_value] == $p_selected_value)
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
                else
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
            }
        }
        return $v_dsp_option;
    }

    /**
     * function count all records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count(array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->count();
        else
            return $this->collection->find($arr_where)->count();
    }

    /**
     * function count all records
     * @param $p_field string: in field to count
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count_field($p_field, array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->find(array($p_field => array('$exists' => true)))->count();
        else
            return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
    }
}
/*
$arr_fields = array(
    array('name'=>'packet_id', 'type'=>'int', 'default'=>'0', 'pkey'=>1, 'index'=>1)//Xem như khóa chính (1 field)??
,array('name'=>'packet_overview', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_specs', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_gallery', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_option', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_sku', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_short_description', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_long_description', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_detail', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_total_quantity', 'type'=>'float', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_type', 'type'=>'int', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_content', 'type'=>'array', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_image_file', 'type'=>'int', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_image_desc', 'type'=>'int', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_image_dir', 'type'=>'int', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_total_price', 'type'=>'float', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_status', 'type'=>'int', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_user_create_name', 'type'=>'string', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'packet_user_create_type', 'type'=>'int', 'default'=>'', 'pkey'=>0, 'index'=>0)
,array('name'=>'create_time', 'type'=>'datetime', 'default'=>'', 'pkey'=>0, 'index'=>0)
);

{
  "default_price": 81.99,
  "image_desc": "Image for current product",
  "image_file": "200_RDK12050014_cropped.jpg",
  "image_option": NumberInt(0),
  "long_description": "Backlit-Tommy Guns Model-RDK12050014",
  "map_content": [

  ],
  "packet_content": [

  ],
  "packet_detail": "",
  "packet_overview": "",
  "packet_specs": "",
  "packet_gallery": "",
  "packet_option": "",
  "packet_total_quantity": 0,
  "packet_total_price": 0,
  "packet_id": NumberInt(339),
  "packet_note": "",
  "packet_sku": "blm-bkl-tg-r014",
  "packet_status": NumberInt(3),
  "saved_dir": "resources\/CHT\/products\/339\/",
  "short_description": "Backlit-Tommy Guns Model-RDK12050014",
  "sold_by": NumberInt(0),
  "tag": [
    NumberInt(63)
  ],
  "template": [
    NumberInt(139)
  ],
  "text": [
    {
      "text": "",
      "color": "#000000",
      "font-name": "Verdana",
      "font-size": "14pt",
      "font-bold": NumberInt(1),
      "font-italic": NumberInt(0),
      "left": NumberInt(0),
      "top": NumberInt(0)
    }
  ],
  "text_option": NumberInt(0),
  "user_name": "admin",
  "user_type": NumberInt(0),
  "vendor_id": NumberInt(-1)
}

 */
?>