<?php
class cls_tb_data_temp{

    private $v_auth_id = 0;
    private $v_auth_string = '';
    private $v_auth_user_name = '';
    private $v_auth_user_pwd = '';
    private $v_auth_order_id = 0;
    private $v_auth_location_id = 0;
    private $v_auth_company_id = 0;
    private $v_auth_ip = '';
    private $collection = NULL;
    private $v_mongo_id = NULL;
    private $v_error_code = 0;
    private $v_error_message = '';
    private $v_is_log = false;
    private $v_dir = '';

    public function __construct(MongoDB $db, $p_log_dir = ""){
        $this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
        if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
        $this->collection = $db->selectCollection('tb_data_temp');
        $this->collection->ensureIndex(array("auth_string"=>1));
        $this->collection->ensureIndex(array("auth_id"=>1), array('name'=>"auth_string", "unique"=>1, "dropDups" => 1));
    }

    public function get_collection(){
        return $this->collection;
    }

    public function get_auth_id(){
        return (int) $this->v_auth_id;
    }



    public function set_auth_id($v_auth_id){
        $this->v_auth_id = (int) $v_auth_id;
    }


    public function get_auth_string(){
        return $this->v_auth_string;
    }


    public function set_auth_string($v_auth_string){
        $this->v_auth_string = $v_auth_string;
    }


    public function get_auth_user_name(){
        return $this->v_auth_user_name;
    }



    public function set_auth_user_name($p_auth_user_name){
        $this->v_auth_user_name = $p_auth_user_name;
    }

    public function get_auth_user_pwd(){
        return $this->v_auth_user_pwd;
    }

    public function set_auth_user_pwd($p_auth_user_pwd){
        $this->v_auth_user_pwd = $p_auth_user_pwd;
    }


    public function get_auth_order_id(){
        return  $this->v_auth_order_id;
    }


    public function set_auth_order_id($p_auth_order_id){
        $this->v_auth_order_id =  $p_auth_order_id;
    }

    public function get_auth_location_id(){
        return  $this->v_auth_location_id;
    }


    public function set_auth_location_id($p_auth_location_id){
        $this->v_auth_location_id =  $p_auth_location_id;
    }

    public function get_auth_company_id(){
        return  $this->v_auth_company_id;
    }


    public function set_auth_company_id($p_auth_company_id){
        $this->v_auth_company_id = $p_auth_company_id;
    }

    public function get_auth_ip(){
        return $this->v_auth_ip;
    }

    public function set_auth_ip($p_auth_ip){
        $this->v_auth_ip = $p_auth_ip;
    }

    /**
     *  function write log
     */
    private function my_error(){
        if(! $this->v_is_log) return;
        global $_SERVER;
        $v_filename = 'tb_tb_data_temp';
        $v_ext = '.log';
        $v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
        $v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
        $v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
        $v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
        $v_log_str .= "\r\n----------------End Log-----------------";
        $v_log_str .= "\r\n";
        $v_new_file = false;
        if(file_exists($this->v_dir.$v_filename.$v_ext)){
            if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
                rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
                $v_new_file = true;
                @unlink($this->v_dir.$v_filename.$v_ext);
            }
        }
        $fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
        if($fp){
            fwrite($fp, $v_log_str, strlen($v_log_str));
            fflush($fp);
            fclose($fp);
        }
    }

    public function insert(){
        $arr = array('auth_id' => $this->v_auth_id
        ,'auth_string' => $this->v_auth_string
        ,'auth_user_name' => $this->v_auth_user_name
        ,'auth_password' => $this->v_auth_user_pwd
        ,'auth_ip' => $this->v_auth_ip
        ,'auth_company_id' => $this->v_auth_company_id
        ,'auth_location_id' => $this->v_auth_location_id
        ,'auth_order_id' => $this->v_auth_order_id);
        try{
            $this->collection->insert($arr, array('safe'=>true));
            $this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return NULL;
        }
    }

    public function select_one(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_count = 0;
        foreach($rss as $arr){
            $this->v_auth_id = isset($arr['auth_id'])?$arr['auth_id']:0;
            $this->v_auth_string = isset($arr['auth_string'])?$arr['auth_string']:'';
            $this->v_auth_user_name = isset($arr['auth_user_name'])?$arr['auth_user_name']:'';
            $this->v_auth_user_pwd = isset($arr['auth_password'])?$arr['auth_password']:'';
            $this->v_auth_ip = isset($arr['auth_ip'])?$arr['auth_ip']:0;
            $this->v_auth_company_id = isset($arr['auth_company_id'])?$arr['auth_company_id']:0;
            $this->v_auth_location_id = isset($arr['auth_location_id'])?$arr['auth_location_id']:0;
            $this->v_auth_order_id = isset($arr['auth_order_id'])?$arr['auth_order_id']:0;
            $this->v_mongo_id = $arr['_id'];
            $v_count++;
        }
        return $v_count;
    }


    public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = NULL;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return $v_ret;
    }

    public function select_next($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => -1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = 0;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return ((int) $v_ret)+1;
    }


    public function select_missing($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => 1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_start = 1;
        $v_ret = 1;
        foreach($rss as $arr){
            if($arr[$p_field_name]!=$v_start){
                $v_ret = $v_start;
                break;
            }
            $v_start++;
        }
        return ((int) $v_ret);
    }

    public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr;
    }

    public function select(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order);
        return $arr;
    }


    public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr_return = array();
        $arr_field = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr_field[$arr_fields[$i]] = 1;
        if($p_row <= 0)
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
        else
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr_return;
    }


    public function update(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(isset($v_has_mongo_id) && $v_has_mongo_id)
            $arr = array('$set' => array('auth_id' => $this->v_auth_id,'auth_string' => $this->v_auth_string,'auth_password' => $this->v_auth_user_pwd,'auth_user_name' => $this->v_auth_user_name,'auth_ip' => $this->v_auth_ip
            ,'auth_location_id' => $this->v_auth_location_id,'auth_company_id' => $this->v_auth_company_id,'auth_order_id'=>$this->v_auth_order_id));
        else
            $arr = array('$set' => array('auth_string' => $this->v_auth_string,'auth_password' => $this->v_auth_user_pwd,'auth_user_name' => $this->v_auth_user_name
            ,'auth_ip' => $this->v_auth_ip,'auth_location_id' => $this->v_auth_location_id,'auth_company_id' => $this->v_auth_company_id,'auth_order_id'=>$this->v_auth_order_id));
        try{
            $this->collection->update($arr_where, $arr, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    public function delete(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->remove($arr_where, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    public function update_field($p_field, $p_value, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$set' => $arr));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$inc' => array($arr)));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
        $arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
        $v_dsp_option = '';
        foreach($arr as $a){
            if($a[$p_field_value] == $p_selected_value)
                $v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
            else
                $v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
        }
        return $v_dsp_option;
    }

    public function count(array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->count();
        else
            return $this->collection->find($arr_where)->count();
    }

    public function count_field($p_field, array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->find(array($p_field => array('$exists' => true)))->count();
        else
            return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
    }
}
?>