<?php
class cls_tb_design_temp{

    private $v_design_temp_id = 0;
    private $v_package_id = 0 ;
    private $v_cart_item_id = 0 ;
    private $v_package_quantity = 1 ;
    private $v_saved_id = 0 ;
    private $v_package_name = '';
    private $v_package_image = '';
    private $v_package_dir = '';
    private $v_product_id = 0;
    private $v_user_id =  0;
    private $v_design_id = 0;
    private $v_template_id = 0 ;
    private $v_company_id = 0 ;
    private $v_location_id = 0 ;
    private $v_user_name = '';
    private $v_saved_dir = '';
    private $v_image_design = '';
    private $v_product_design_quantity =  0;
    private $v_total_product_origin_quantity =  0;
    private $v_date_design = '0000-00-00 00:00:00';
    private $v_date_modify = '0000-00-00 00:00:00';
    private $v_product_price =  0.0;
    private $v_product_kitting_cost =  0.0;
    private $v_product_markup_cost = 0.0 ;
    private $v_product_print_cost = 0.0 ;
    private $v_total_amount = 0.0;
    private $v_width = 0 ;
    private $v_length = 0 ;
    private $v_usize = '';
    private $v_design_name = '';
    private $collection = NULL;
    private $v_mongo_id = NULL;
    private $v_error_code = 0;
    private $v_error_message = '';
    private $v_is_log = false;
    private $v_dir = '';

    /**
     *  constructor function
     *  @param $db: instance of Mongo
     *  @param $p_log_dir string: directory contains its log file
     */
    public function __construct(MongoDB $db, $p_log_dir = ""){
        $this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
        if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
        $this->collection = $db->selectCollection('tb_design_temp');
        $this->v_date_design = new MongoDate(time());
        $this->v_date_modify = new MongoDate(time());
        $this->collection->ensureIndex(array("design_temp_id"=>1), array('name'=>"design_temp_id_key", "unique"=>1, "dropDups" => 1));
    }

    /**
     *  function get current MongoDB collection
     *  @return Object: current MongoDB collection
     */
    public function get_collection(){
        return $this->collection;
    }

    /**
     *  function write log
     */
    private function my_error(){
        if(! $this->v_is_log) return;
        global $_SERVER;
        $v_filename = 'tb_design_temp';
        $v_ext = '.log';
        $v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
        $v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
        $v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
        $v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
        $v_log_str .= "\r\n----------------End Log-----------------";
        $v_log_str .= "\r\n";
        $v_new_file = false;
        if(file_exists($this->v_dir.$v_filename.$v_ext)){
            if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
                rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
                $v_new_file = true;
                @unlink($this->v_dir.$v_filename.$v_ext);
            }
        }
        $fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
        if($fp){
            fwrite($fp, $v_log_str, strlen($v_log_str));
            fflush($fp);
            fclose($fp);
        }
    }
    public function get_cart_item(){
        return (int) $this->v_cart_item_id;
    }

    public function set_cart_item($v_cart_item_id){
        $this->v_cart_item_id = (int) $v_cart_item_id;
    }
    public function get_package_quantity(){
        return (int) $this->v_package_quantity;
    }

    public function set_package_quantity($p_design_temp_id){
        $this->v_package_quantity = (int) $p_design_temp_id;
    }
    public function get_saved_id(){
        return (int) $this->v_saved_id;
    }

    public function set_saved_id($p_design_temp_id){
        $this->v_saved_id = (int) $p_design_temp_id;
    }
    /**
     * function return properties "design_temp_id" value
     * @return int value
     */
    public function get_design_temp_id(){
        return (int) $this->v_design_temp_id;
    }


    /**
     * function allow change properties "design_temp_id" value
     * @param $p_design_temp_id: int value
     */
    public function set_design_temp_id($p_design_temp_id){
        $this->v_design_temp_id = (int) $p_design_temp_id;
    }


    /**
     * function return properties "package_id" value
     * @return int value
     */
    public function get_package_id(){
        return (int) $this->v_package_id;
    }


    /**
     * function allow change properties "package_id" value
     * @param $p_package_id: int value
     */
    public function set_package_id($p_package_id){
        $this->v_package_id = (int) $p_package_id;
    }


    /**
     * function return properties "product_id" value
     * @return int value
     */
    public function get_product_id(){
        return (int) $this->v_product_id;
    }


    /**
     * function allow change properties "product_id" value
     * @param $p_product_id: int value
     */
    public function set_product_id($p_product_id){
        $this->v_product_id = (int) $p_product_id;
    }


    /**
     * function return properties "user_id" value
     * @return int value
     */
    public function get_user_id(){
        return (int) $this->v_user_id;
    }


    /**
     * function allow change properties "user_id" value
     * @param $p_user_id: int value
     */
    public function set_user_id($p_user_id){
        $this->v_user_id = (int) $p_user_id;
    }


    /**
     * function return properties "design_id" value
     * @return int value
     */
    public function get_design_id(){
        return (int) $this->v_design_id;
    }


    /**
     * function allow change properties "design_id" value
     * @param $p_design_id: int value
     */
    public function set_design_id($p_design_id){
        $this->v_design_id = (int) $p_design_id;
    }


    /**
     * function return properties "template_id" value
     * @return int value
     */
    public function get_template_id(){
        return (int) $this->v_template_id;
    }


    /**
     * function allow change properties "template_id" value
     * @param $p_template_id: int value
     */
    public function set_template_id($p_template_id){
        $this->v_template_id = (int) $p_template_id;
    }


    /**
     * function return properties "company_id" value
     * @return int value
     */
    public function get_company_id(){
        return (int) $this->v_company_id;
    }


    /**
     * function allow change properties "company_id" value
     * @param $p_company_id: int value
     */
    public function set_company_id($p_company_id){
        $this->v_company_id = (int) $p_company_id;
    }


    /**
     * function return properties "location_id" value
     * @return int value
     */
    public function get_location_id(){
        return (int) $this->v_location_id;
    }


    /**
     * function allow change properties "location_id" value
     * @param $p_location_id: int value
     */
    public function set_location_id($p_location_id){
        $this->v_location_id = (int) $p_location_id;
    }


    /**
     * function return properties "user_name" value
     * @return string value
     */
    public function get_user_name(){
        return $this->v_user_name;
    }


    /**
     * function allow change properties "user_name" value
     * @param $p_user_name: string value
     */
    public function set_user_name($p_user_name){
        $this->v_user_name = $p_user_name;
    }

    public function get_package_name(){
        return $this->v_package_name;
    }

    public function set_package_name($p_package_name){
        $this->v_package_name = $p_package_name;
    }

    public function get_package_image(){
        return $this->v_package_image;
    }

    public function set_package_image($p_package_image){
        $this->v_package_image = $p_package_image;
    }


    public function get_package_dir(){
        return $this->v_package_dir;
    }

    public function set_package_dir($p_package_dir){
        $this->v_package_dir = $p_package_dir;
    }
    /**
     * function return properties "saved_dir" value
     * @return string value
     */
    public function get_saved_dir(){
        return $this->v_saved_dir;
    }


    /**
     * function allow change properties "saved_dir" value
     * @param $p_saved_dir: string value
     */
    public function set_saved_dir($p_saved_dir){
        $this->v_saved_dir = $p_saved_dir;
    }


    /**
     * function return properties "image_design" value
     * @return string value
     */
    public function get_image_design(){
        return $this->v_image_design;
    }


    /**
     * function allow change properties "image_design" value
     * @param $p_image_design: string value
     */
    public function set_image_design($p_image_design){
        $this->v_image_design = $p_image_design;
    }


    /**
     * function return properties "product_design_quantity" value
     * @return int value
     */
    public function get_product_design_quantity(){
        return (int) $this->v_product_design_quantity;
    }


    /**
     * function allow change properties "product_design_quantity" value
     * @param $p_product_design_quantity: int value
     */
    public function set_product_design_quantity($p_product_design_quantity){
        $this->v_product_design_quantity = (int) $p_product_design_quantity;
    }


    /**
     * function return properties "total_product_origin_quantity" value
     * @return int value
     */
    public function get_total_product_origin_quantity(){
        return (int) $this->v_total_product_origin_quantity;
    }


    /**
     * function allow change properties "total_product_origin_quantity" value
     * @param $p_total_product_origin_quantity: int value
     */
    public function set_total_product_origin_quantity($p_total_product_origin_quantity){
        $this->v_total_product_origin_quantity = (int) $p_total_product_origin_quantity;
    }


    /**
     * function return properties "date_design" value
     * @return int value indicates amount of seconds
     */
    public function get_date_design(){
        return  $this->v_date_design->sec;
    }


    /**
     * function allow change properties "date_design" value
     * @param $p_date_design: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_date_design($p_date_design){
        if($p_date_design=='') $p_date_design = NULL;
        if(!is_null($p_date_design)){
            try{
                $this->v_date_design = new MongoDate(strtotime($p_date_design));
            }catch(MongoException $me){
                $this->v_date_design = NULL;
            }
        }else{
            $this->v_date_design = NULL;
        }
    }


    /**
     * function return properties "date_modify" value
     * @return int value indicates amount of seconds
     */
    public function get_date_modify(){
        return  $this->v_date_modify->sec;
    }


    /**
     * function allow change properties "date_modify" value
     * @param $p_date_modify: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_date_modify($p_date_modify){
        if($p_date_modify=='') $p_date_modify = NULL;
        if(!is_null($p_date_modify)){
            try{
                $this->v_date_modify = new MongoDate(strtotime($p_date_modify));
            }catch(MongoException $me){
                $this->v_date_modify = NULL;
            }
        }else{
            $this->v_date_modify = NULL;
        }
    }


    /**
     * function return properties "product_price" value
     * @return float value
     */
    public function get_product_price(){
        return (float) $this->v_product_price;
    }


    /**
     * function allow change properties "product_price" value
     * @param $p_product_price: float value
     */
    public function set_product_price($p_product_price){
        $this->v_product_price = (float) $p_product_price;
    }


    /**
     * function return properties "product_kitting_cost" value
     * @return float value
     */
    public function get_product_kitting_cost(){
        return (float) $this->v_product_kitting_cost;
    }


    /**
     * function allow change properties "product_kitting_cost" value
     * @param $p_product_kitting_cost: float value
     */
    public function set_product_kitting_cost($p_product_kitting_cost){
        $this->v_product_kitting_cost = (float) $p_product_kitting_cost;
    }


    /**
     * function return properties "product_markup_cost" value
     * @return float value
     */
    public function get_product_markup_cost(){
        return (float) $this->v_product_markup_cost;
    }


    /**
     * function allow change properties "product_markup_cost" value
     * @param $p_product_markup_cost: float value
     */
    public function set_product_markup_cost($p_product_markup_cost){
        $this->v_product_markup_cost = (float) $p_product_markup_cost;
    }


    /**
     * function return properties "product_print_cost" value
     * @return float value
     */
    public function get_product_print_cost(){
        return (float) $this->v_product_print_cost;
    }


    /**
     * function allow change properties "product_print_cost" value
     * @param $p_product_print_cost: float value
     */
    public function set_product_print_cost($p_product_print_cost){
        $this->v_product_print_cost = (float) $p_product_print_cost;
    }


    /**
     * function return properties "total_amount" value
     * @return float value
     */
    public function get_total_amount(){
        return (float) $this->v_total_amount;
    }


    /**
     * function allow change properties "total_amount" value
     * @param $p_total_amount: float value
     */
    public function set_total_amount($p_total_amount){
        $this->v_total_amount = (float) $p_total_amount;
    }


    /**
     * function return properties "width" value
     * @return float value
     */
    public function get_width(){
        return (float) $this->v_width;
    }


    /**
     * function allow change properties "width" value
     * @param $p_width: float value
     */
    public function set_width($p_width){
        $this->v_width = (float) $p_width;
    }


    /**
     * function return properties "length" value
     * @return float value
     */
    public function get_length(){
        return (float) $this->v_length;
    }


    /**
     * function allow change properties "length" value
     * @param $p_length: float value
     */
    public function set_length($p_length){
        $this->v_length = (float) $p_length;
    }


    /**
     * function return properties "usize" value
     * @return string value
     */
    public function get_usize(){
        return $this->v_usize;
    }


    /**
     * function allow change properties "usize" value
     * @param $p_usize: string value
     */
    public function set_usize($p_usize){
        $this->v_usize = $p_usize;
    }


    /**
     * function return properties "design_name" value
     * @return string value
     */
    public function get_design_name(){
        return $this->v_design_name;
    }


    /**
     * function allow change properties "design_name" value
     * @param $p_design_name: string value
     */
    public function set_design_name($p_design_name){
        $this->v_design_name = $p_design_name;
    }


    /**
     * function return MongoID value after inserting new record
     * @return ObjectId: MongoId
     */
    public function get_mongo_id(){
        return $this->v_mongo_id;
    }


    /**
     * function set MongoID to properties
     */
    public function set_mongo_id($p_mongo_id){
        $this->v_mongo_id = $p_mongo_id;
    }


    /**
     *  function allow insert one record
     *  @return MongoID
     */
    public function insert(){
        $arr = array('design_temp_id' => $this->v_design_temp_id
        ,'package_id' => $this->v_package_id
        ,'product_id' => $this->v_product_id
        ,'user_id' => $this->v_user_id
        ,'saved_id' => $this->v_saved_id
        ,'package_quantity' => $this->v_package_quantity
        ,'cart_item_id' => $this->v_cart_item_id
        ,'design_id' => $this->v_design_id
        ,'template_id' => $this->v_template_id
        ,'company_id' => $this->v_company_id
        ,'location_id' => $this->v_location_id
        ,'user_name' => $this->v_user_name
        ,'package_dir' => $this->v_package_dir
        ,'package_image' => $this->v_package_image
        ,'package_name' => $this->v_package_name
        ,'saved_dir' => $this->v_saved_dir
        ,'image_design' => $this->v_image_design
        ,'product_design_quantity' => $this->v_product_design_quantity
        ,'total_product_origin_quantity' => $this->v_total_product_origin_quantity
        ,'date_design' => $this->v_date_design
        ,'date_modify' => $this->v_date_modify
        ,'product_price' => $this->v_product_price
        ,'product_kitting_cost' => $this->v_product_kitting_cost
        ,'product_markup_cost' => $this->v_product_markup_cost
        ,'product_print_cost' => $this->v_product_print_cost
        ,'total_amount' => $this->v_total_amount
        ,'width' => $this->v_width
        ,'length' => $this->v_length
        ,'usize' => $this->v_usize
        ,'design_name' => $this->v_design_name);
        try{
            $this->collection->insert($arr, array('safe'=>true));
            $this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     *  function allow insert array with parameter
     *  @param array $arr_fields_and_values
     *  @return MongoID
     */
    public function insert_array(array $arr_fields_and_values){
        try{
            $this->collection->insert($arr_fields_and_values, array('safe'=>true));
            $this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return NULL;
        }
    }


    /**
     * function select_one_record
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: all values will assign to this instance properties
     * @example:
     * <code>
     *       SELECT * FROM `tb_design_temp` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return int
     */
    public function select_one(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_count = 0;
        foreach($rss as $arr){
            $this->v_design_temp_id = isset($arr['design_temp_id'])?$arr['design_temp_id']:0;
            $this->v_saved_id = isset($arr['saved_id'])?$arr['saved_id']:0;
            $this->v_package_quantity = isset($arr['package_quantity'])?$arr['package_quantity']:1;
            $this->v_cart_item_id = isset($arr['cart_item_id'])?$arr['cart_item_id']:0;
            $this->v_package_id = isset($arr['package_id'])?$arr['package_id']:0;
            $this->v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
            $this->v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
            $this->v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
            $this->v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
            $this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
            $this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
            $this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';

            $this->v_package_dir = isset($arr['package_dir'])?$arr['package_dir']:'images/';
            $this->v_package_image = isset($arr['package_image'])?$arr['package_image']:'images/';
            $this->v_package_name = isset($arr['package_name'])?$arr['package_name']:'product_sample.jpg';

            $this->v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
            $this->v_image_design = isset($arr['image_design'])?$arr['image_design']:'';
            $this->v_product_design_quantity = isset($arr['product_design_quantity'])?$arr['product_design_quantity']:0;
            $this->v_total_product_origin_quantity = isset($arr['total_product_origin_quantity'])?$arr['total_product_origin_quantity']:0;
            $this->v_date_design = isset($arr['date_design'])?$arr['date_design']:(new MongoDate(time()));
            $this->v_date_modify = isset($arr['date_modify'])?$arr['date_modify']:(new MongoDate(time()));
            $this->v_product_price = isset($arr['product_price'])?$arr['product_price']:0.0;
            $this->v_product_kitting_cost = isset($arr['product_kitting_cost'])?$arr['product_kitting_cost']:0.0;
            $this->v_product_markup_cost = isset($arr['product_markup_cost'])?$arr['product_markup_cost']:0.0;
            $this->v_product_print_cost = isset($arr['product_print_cost'])?$arr['product_print_cost']:0.0;
            $this->v_total_amount = isset($arr['total_amount'])?$arr['total_amount']:0.0;
            $this->v_width = isset($arr['width'])?$arr['width']:0.0;
            $this->v_length = isset($arr['length'])?$arr['length']:0.0;
            $this->v_usize = isset($arr['usize'])?$arr['usize']:'';
            $this->v_design_name = isset($arr['design_name'])?$arr['design_name']:'0';
            $this->v_mongo_id = $arr['_id'];
            $v_count++;
        }
        return $v_count;
    }

    /**
     * function select scalar value
     * @param $p_field_name string, name of field
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @result: assign to properties
     * @example:
     * <code>
     * SELECT `design_temp_id` FROM `tb_design_temp` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select_scalar('design_temp_id',array('user_id'=>2), array('user_email'=>-1))
     * </code>
     * @return mixed
     */
    public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = NULL;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return $v_ret;
    }

    /**
     * function get next int value for key
     * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @result: assign to properties
     * @example:
     * <code>
     *   SELECT `design_temp_id` FROM `tb_design_temp` WHERE `user_id`=2 ORDER BY `design_temp_id` DESC LIMIT 0,1
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select_next('design_temp_id',array('user_id'=>2), array('design_temp_id'=>-1))
     * </code>
     * @return int
     */
    public function select_next($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => -1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = 0;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return ((int) $v_ret)+1;
    }

    /**
     * function get missing value
     * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function select_missing($p_field_name, array $arr_where = array()){
        $arr_order = array(''.$p_field_name.'' => 1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_start = 1;
        $v_ret = 1;
        foreach($rss as $arr){
            if($arr[''.$p_field_name.'']!=$v_start){
                $v_ret = $v_start;
                break;
            }
            $v_start++;
        }
        return ((int) $v_ret);
    }

    /**
     * function select limit records
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr;
    }

    /**
     * function select records
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order);
        return $arr;
    }

    /**
     * function select distinct
     * @param $p_field_name string, name of selected field
     * @example:
     * <code>
     *         SELECT DISTINCT `name` FROM `tbl_users`
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select_distinct('nam')
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_distinct($p_field_name){
        return $this->collection->command(array("distinct"=>"tb_design_temp", "key"=>$p_field_name));
    }

    /**
     * function select limit fields
     * @param $p_offset int: start record to select, first record is 0
     * @param $p_row int: amount of records to select
     * @param $arr_fields array, array of fields will be selected
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @example:
     * <code>
     * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
     * 		 $cls = new cls_tb_design_temp($db)
     * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
     * </code>
     * @return array with indexes are names of fields
     */
    public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr_field = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr_field[$arr_fields[$i]] = 1;
        if($p_row <= 0)
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
        else
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr_return;
    }

    /**
     *  function update one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(isset($v_has_mongo_id) && $v_has_mongo_id)
            $arr = array('$set' => array('design_temp_id' => $this->v_design_temp_id
            ,'package_id' => $this->v_package_id
            ,'saved_id' => $this->v_saved_id
            ,'cart_item_id' => $this->v_cart_item_id
            ,'package_quantity' => $this->v_package_quantity
            ,'product_id' => $this->v_product_id,'user_id' => $this->v_user_id,'design_id' => $this->v_design_id,'template_id' => $this->v_template_id,'company_id' => $this->v_company_id
            ,'package_dir' => $this->v_package_dir
            ,'package_image' => $this->v_package_image
            ,'package_name' => $this->v_package_name
            ,'location_id' => $this->v_location_id,'user_name' => $this->v_user_name
            ,'saved_dir' => $this->v_saved_dir,'image_design' => $this->v_image_design,'product_design_quantity' => $this->v_product_design_quantity,'total_product_origin_quantity' => $this->v_total_product_origin_quantity,'date_design' => $this->v_date_design,'date_modify' => $this->v_date_modify,'product_price' => $this->v_product_price,'product_kitting_cost' => $this->v_product_kitting_cost,'product_markup_cost' => $this->v_product_markup_cost,'product_print_cost' => $this->v_product_print_cost,'total_amount' => $this->v_total_amount,'width' => $this->v_width,'length' => $this->v_length,'usize' => $this->v_usize,'design_name' => $this->v_design_name));
        else
            $arr = array('$set' => array('package_id' => $this->v_package_id,'product_id' => $this->v_product_id
            ,'package_dir' => $this->v_package_dir
            ,'package_image' => $this->v_package_image
            ,'package_name' => $this->v_package_name
            ,'saved_id' => $this->v_saved_id
            ,'cart_item_id' => $this->v_cart_item_id
            ,'package_quantity' => $this->v_package_quantity
            ,'user_id' => $this->v_user_id,'design_id' => $this->v_design_id,'template_id' => $this->v_template_id,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id,'user_name' => $this->v_user_name,'saved_dir' => $this->v_saved_dir,'image_design' => $this->v_image_design,'product_design_quantity' => $this->v_product_design_quantity,'total_product_origin_quantity' => $this->v_total_product_origin_quantity,'date_design' => $this->v_date_design,'date_modify' => $this->v_date_modify,'product_price' => $this->v_product_price,'product_kitting_cost' => $this->v_product_kitting_cost,'product_markup_cost' => $this->v_product_markup_cost,'product_print_cost' => $this->v_product_print_cost,'total_amount' => $this->v_total_amount,'width' => $this->v_width,'length' => $this->v_length,'usize' => $this->v_usize,'design_name' => $this->v_design_name));
        try{
            $this->collection->update($arr_where, $arr, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function delete one or more records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function delete(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->remove($arr_where, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_field($p_field, $p_value, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields array, array of selected fields go to updated
     * @param $arr_values array, array of selected values go to assigned
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function increase one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, assigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $arr_fields = array(), array of selected fields go to updated
     * @param $arr_values = array(), array of selected values go to increase
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            $this->my_error();
            return false;
        }
    }

    /**
     * function draw option tag
     * @param $p_field_value string: name of field will be value option tag
     * @param $p_field_display string: name of field will be display text option tag
     * @param $p_selected_value mixed: value of field will be display text option tag
     * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
     * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
     * @param $arr_exclude array: array list value of exclude
     * @return string
     */
    public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
        $arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
        $v_dsp_option = '';
        foreach($arr as $a){
            if(!in_array($a[$p_field_value],$arr_exclude)){
                if($a[$p_field_value] == $p_selected_value)
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
                else
                    $v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
            }
        }
        return $v_dsp_option;
    }

    /**
     * function count all records
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count(array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->count();
        else
            return $this->collection->find($arr_where)->count();
    }

    /**
     * function count all records
     * @param $p_field string: in field to count
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return int
     */
    public function count_field($p_field, array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->find(array($p_field => array('$exists' => true)))->count();
        else
            return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
    }
}
?>