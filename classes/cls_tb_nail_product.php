<?php
class cls_tb_nail_product{

	private $v_product_id = 0;
	private $v_product_name = '';
	private $v_product_code_number = '';
	private $v_product_price = 0;
	private $v_product_viewed = 0;
	private $v_product_is_on_sale = 0;
	private $v_product_is_best_sale = 0;
	private $v_product_brand_id = 0;
	private $arr_product_information = array();
	private $v_product_description = '';
	private $v_product_review = '';
	private $arr_product_material_information = array();
	private $v_product_category_id = 0;
	private $v_user_create = 0;
	private $v_user_create_name = '';
	private $v_product_status = 1;
	private $v_user_edit = 0;
	private $v_user_edit_name = '';
	private $v_preview_image = '';
	private $v_preview_rated = '';
	private $arr_preview_rated_information = array();
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
	
	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection('tb_nail_product');
		$this->collection->ensureIndex(array("product_id"=>1), array('name'=>"product_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = 'tb_nail_product';
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "product_id" value
	 * @return int value
	 */
	public function get_product_id(){
		return (int) $this->v_product_id;
	}

	
	/**
	 * function allow change properties "product_id" value
	 * @param $p_product_id: int value
	 */
	public function set_product_id($p_product_id){
		$this->v_product_id = (int) $p_product_id;
	}

	
	/**
	 * function return properties "product_name" value
	 * @return string value
	 */
	public function get_product_name(){
		return $this->v_product_name;
	}

	
	/**
	 * function allow change properties "product_name" value
	 * @param $p_product_name: string value
	 */
	public function set_product_name($p_product_name){
		$this->v_product_name = $p_product_name;
	}

	
	/**
	 * function return properties "product_code_number" value
	 * @return string value
	 */
	public function get_product_code_number(){
		return $this->v_product_code_number;
	}

	
	/**
	 * function allow change properties "product_code_number" value
	 * @param $p_product_code_number: string value
	 */
	public function set_product_code_number($p_product_code_number){
		$this->v_product_code_number = $p_product_code_number;
	}

	
	/**
	 * function return properties "product_price" value
	 * @return float value
	 */
	public function get_product_price(){
		return (float) $this->v_product_price;
	}

	
	/**
	 * function allow change properties "product_price" value
	 * @param $p_product_price: float value
	 */
	public function set_product_price($p_product_price){
		$this->v_product_price = (float) $p_product_price;
	}

	
	/**
	 * function return properties "product_viewed" value
	 * @return int value
	 */
	public function get_product_viewed(){
		return (int) $this->v_product_viewed;
	}

	
	/**
	 * function allow change properties "product_viewed" value
	 * @param $p_product_viewed: int value
	 */
	public function set_product_viewed($p_product_viewed){
		$this->v_product_viewed = (int) $p_product_viewed;
	}

	
	/**
	 * function return properties "product_is_on_sale" value
	 * @return int value
	 */
	public function get_product_is_on_sale(){
		return (int) $this->v_product_is_on_sale;
	}

	
	/**
	 * function allow change properties "product_is_on_sale" value
	 * @param $p_product_is_on_sale: int value
	 */
	public function set_product_is_on_sale($p_product_is_on_sale){
		$this->v_product_is_on_sale = (int) $p_product_is_on_sale;
	}

	
	/**
	 * function return properties "product_is_best_sale" value
	 * @return int value
	 */
	public function get_product_is_best_sale(){
		return (int) $this->v_product_is_best_sale;
	}

	
	/**
	 * function allow change properties "product_is_best_sale" value
	 * @param $p_product_is_best_sale: int value
	 */
	public function set_product_is_best_sale($p_product_is_best_sale){
		$this->v_product_is_best_sale = (int) $p_product_is_best_sale;
	}

	
	/**
	 * function return properties "product_brand_id" value
	 * @return int value
	 */
	public function get_product_brand_id(){
		return (int) $this->v_product_brand_id;
	}

	
	/**
	 * function allow change properties "product_brand_id" value
	 * @param $p_product_brand_id: int value
	 */
	public function set_product_brand_id($p_product_brand_id){
		$this->v_product_brand_id = (int) $p_product_brand_id;
	}

	
	/**
	 * function return properties "product_information" value
	 * @return array value
	 */
	public function get_product_information(){
		return  $this->arr_product_information;
	}

	
	/**
	 * function allow change properties "product_information" value
	 * @param $arr_product_information array
	 */
	public function set_product_information(array $arr_product_information = array()){
		$this->arr_product_information = $arr_product_information;
	}

	
	/**
	 * function return properties "product_description" value
	 * @return string value
	 */
	public function get_product_description(){
		return $this->v_product_description;
	}

	
	/**
	 * function allow change properties "product_description" value
	 * @param $p_product_description: string value
	 */
	public function set_product_description($p_product_description){
		$this->v_product_description = $p_product_description;
	}

	
	/**
	 * function return properties "product_review" value
	 * @return string value
	 */
	public function get_product_review(){
		return $this->v_product_review;
	}

	
	/**
	 * function allow change properties "product_review" value
	 * @param $p_product_review: string value
	 */
	public function set_product_review($p_product_review){
		$this->v_product_review = $p_product_review;
	}

	
	/**
	 * function return properties "product_material_information" value
	 * @return array value
	 */
	public function get_product_material_information(){
		return  $this->arr_product_material_information;
	}

	
	/**
	 * function allow change properties "product_material_information" value
	 * @param $arr_product_material_information array
	 */
	public function set_product_material_information(array $arr_product_material_information = array()){
		$this->arr_product_material_information = $arr_product_material_information;
	}

	
	/**
	 * function return properties "product_category_id" value
	 * @return int value
	 */
	public function get_product_category_id(){
		return (int) $this->v_product_category_id;
	}

	
	/**
	 * function allow change properties "product_category_id" value
	 * @param $p_product_category_id: int value
	 */
	public function set_product_category_id($p_product_category_id){
		$this->v_product_category_id = (int) $p_product_category_id;
	}

	
	/**
	 * function return properties "user_create" value
	 * @return int value
	 */
	public function get_user_create(){
		return (int) $this->v_user_create;
	}

	
	/**
	 * function allow change properties "user_create" value
	 * @param $p_user_create: int value
	 */
	public function set_user_create($p_user_create){
		$this->v_user_create = (int) $p_user_create;
	}

	
	/**
	 * function return properties "user_create_name" value
	 * @return string value
	 */
	public function get_user_create_name(){
		return $this->v_user_create_name;
	}

	
	/**
	 * function allow change properties "user_create_name" value
	 * @param $p_user_create_name: string value
	 */
	public function set_user_create_name($p_user_create_name){
		$this->v_user_create_name = $p_user_create_name;
	}

	
	/**
	 * function return properties "product_status" value
	 * @return int value
	 */
	public function get_product_status(){
		return (int) $this->v_product_status;
	}

	
	/**
	 * function allow change properties "product_status" value
	 * @param $p_product_status: int value
	 */
	public function set_product_status($p_product_status){
		$this->v_product_status = (int) $p_product_status;
	}

	
	/**
	 * function return properties "user_edit" value
	 * @return int value
	 */
	public function get_user_edit(){
		return (int) $this->v_user_edit;
	}

	
	/**
	 * function allow change properties "user_edit" value
	 * @param $p_user_edit: int value
	 */
	public function set_user_edit($p_user_edit){
		$this->v_user_edit = (int) $p_user_edit;
	}

	
	/**
	 * function return properties "user_edit_name" value
	 * @return string value
	 */
	public function get_user_edit_name(){
		return $this->v_user_edit_name;
	}

	
	/**
	 * function allow change properties "user_edit_name" value
	 * @param $p_user_edit_name: string value
	 */
	public function set_user_edit_name($p_user_edit_name){
		$this->v_user_edit_name = $p_user_edit_name;
	}

	
	/**
	 * function return properties "preview_image" value
	 * @return string value
	 */
	public function get_preview_image(){
		return $this->v_preview_image;
	}

	
	/**
	 * function allow change properties "preview_image" value
	 * @param $p_preview_image: string value
	 */
	public function set_preview_image($p_preview_image){
		$this->v_preview_image = $p_preview_image;
	}

	
	/**
	 * function return properties "preview_rated" value
	 * @return string value
	 */
	public function get_preview_rated(){
		return $this->v_preview_rated;
	}

	
	/**
	 * function allow change properties "preview_rated" value
	 * @param $p_preview_rated: string value
	 */
	public function set_preview_rated($p_preview_rated){
		$this->v_preview_rated = $p_preview_rated;
	}

	
	/**
	 * function return properties "preview_rated_information" value
	 * @return array value
	 */
	public function get_preview_rated_information(){
		return  $this->arr_preview_rated_information;
	}

	
	/**
	 * function allow change properties "preview_rated_information" value
	 * @param $arr_preview_rated_information array
	 */
	public function set_preview_rated_information(array $arr_preview_rated_information = array()){
		$this->arr_preview_rated_information = $arr_preview_rated_information;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
		$arr = array('product_id' => $this->v_product_id
					,'product_name' => $this->v_product_name
					,'product_code_number' => $this->v_product_code_number
					,'product_price' => $this->v_product_price
					,'product_viewed' => $this->v_product_viewed
					,'product_is_on_sale' => $this->v_product_is_on_sale
					,'product_is_best_sale' => $this->v_product_is_best_sale
					,'product_brand_id' => $this->v_product_brand_id
					,'product_information' => $this->arr_product_information
					,'product_description' => $this->v_product_description
					,'product_review' => $this->v_product_review
					,'product_material_information' => $this->arr_product_material_information
					,'product_category_id' => $this->v_product_category_id
					,'user_create' => $this->v_user_create
					,'user_create_name' => $this->v_user_create_name
					,'product_status' => $this->v_product_status
					,'user_edit' => $this->v_user_edit
					,'user_edit_name' => $this->v_user_edit_name
					,'preview_image' => $this->v_preview_image
					,'preview_rated' => $this->v_preview_rated
					,'preview_rated_information' => $this->arr_preview_rated_information);
		try{
			$this->collection->insert($arr, array('safe'=>true));
			$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_nail_product` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
			$this->v_product_name = isset($arr['product_name'])?$arr['product_name']:'';
			$this->v_product_code_number = isset($arr['product_code_number'])?$arr['product_code_number']:'';
			$this->v_product_price = isset($arr['product_price'])?$arr['product_price']:0;
			$this->v_product_viewed = isset($arr['product_viewed'])?$arr['product_viewed']:0;
			$this->v_product_is_on_sale = isset($arr['product_is_on_sale'])?$arr['product_is_on_sale']:0;
			$this->v_product_is_best_sale = isset($arr['product_is_best_sale'])?$arr['product_is_best_sale']:0;
			$this->v_product_brand_id = isset($arr['product_brand_id'])?$arr['product_brand_id']:0;
			$this->arr_product_information = isset($arr['product_information'])?$arr['product_information']:array();
			$this->v_product_description = isset($arr['product_description'])?$arr['product_description']:'';
			$this->v_product_review = isset($arr['product_review'])?$arr['product_review']:'';
			$this->arr_product_material_information = isset($arr['product_material_information'])?$arr['product_material_information']:array();
			$this->v_product_category_id = isset($arr['product_category_id'])?$arr['product_category_id']:0;
			$this->v_user_create = isset($arr['user_create'])?$arr['user_create']:0;
			$this->v_user_create_name = isset($arr['user_create_name'])?$arr['user_create_name']:'';
			$this->v_product_status = isset($arr['product_status'])?$arr['product_status']:1;
			$this->v_user_edit = isset($arr['user_edit'])?$arr['user_edit']:0;
			$this->v_user_edit_name = isset($arr['user_edit_name'])?$arr['user_edit_name']:'';
			$this->v_preview_image = isset($arr['preview_image'])?$arr['preview_image']:'';
			$this->v_preview_rated = isset($arr['preview_rated'])?$arr['preview_rated']:'';
			$this->arr_preview_rated_information = isset($arr['preview_rated_information'])?$arr['preview_rated_information']:array();
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `product_id` FROM `tb_nail_product` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select_scalar('product_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `product_id` FROM `tb_nail_product` WHERE `user_id`=2 ORDER BY `product_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select_next('product_id',array('user_id'=>2), array('product_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return ((int) $v_ret)+1;
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
	
	/**
	 * function select distinct
	 * @param $p_field_name string, name of selected field
	 * @example: 
	 * <code>
	 *         SELECT DISTINCT `name` FROM `tbl_users`
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select_distinct('nam')
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_distinct($p_field_name){
		return $this->collection->command(array("distinct"=>"tb_nail_product", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_nail_product($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('product_id' => $this->v_product_id,'product_name' => $this->v_product_name,'product_code_number' => $this->v_product_code_number,'product_price' => $this->v_product_price,'product_viewed' => $this->v_product_viewed,'product_is_on_sale' => $this->v_product_is_on_sale,'product_is_best_sale' => $this->v_product_is_best_sale,'product_brand_id' => $this->v_product_brand_id,'product_information' => $this->arr_product_information,'product_description' => $this->v_product_description,'product_review' => $this->v_product_review,'product_material_information' => $this->arr_product_material_information,'product_category_id' => $this->v_product_category_id,'user_create' => $this->v_user_create,'user_create_name' => $this->v_user_create_name,'product_status' => $this->v_product_status,'user_edit' => $this->v_user_edit,'user_edit_name' => $this->v_user_edit_name,'preview_image' => $this->v_preview_image,'preview_rated' => $this->v_preview_rated,'preview_rated_information' => $this->arr_preview_rated_information));
		 else 
			$arr = array('$set' => array('product_name' => $this->v_product_name,'product_code_number' => $this->v_product_code_number,'product_price' => $this->v_product_price,'product_viewed' => $this->v_product_viewed,'product_is_on_sale' => $this->v_product_is_on_sale,'product_is_best_sale' => $this->v_product_is_best_sale,'product_brand_id' => $this->v_product_brand_id,'product_information' => $this->arr_product_information,'product_description' => $this->v_product_description,'product_review' => $this->v_product_review,'product_material_information' => $this->arr_product_material_information,'product_category_id' => $this->v_product_category_id,'user_create' => $this->v_user_create,'user_create_name' => $this->v_user_create_name,'product_status' => $this->v_product_status,'user_edit' => $this->v_user_edit,'user_edit_name' => $this->v_user_edit_name,'preview_image' => $this->v_preview_image,'preview_rated' => $this->v_preview_rated,'preview_rated_information' => $this->arr_preview_rated_information));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>