<?php
class cls_tb_activity{
    private $collection = NULL;
    private $v_mongo_id = NULL;
    private $v_error_code = 0;
    private $v_error_message = '';
    private $v_is_log = false;
    private $v_dir = '';

    /**
     *  constructor function
     *  @param $db: instance of Mongo
     *  @param $p_log_dir string: directory contains its log file
     */
    public function __construct(MongoDB $db, $p_log_dir = ""){
        $this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
        if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
        $this->collection = $db->selectCollection('tb_noactivity');
//        $this->collection->ensureIndex(array("thickness_id"=>1), array('name'=>"thickness_id_key", "unique"=>1, "dropDups" => 1));
    }

    /**
     *  function get current MongoDB collection
     *  @return Object: current MongoDB collection
     */
    public function get_collection(){
        return $this->collection;
    }
    /**
     * function return MongoID value after inserting new record
     * @return ObjectId: MongoId
     */
    public function get_mongo_id(){
        return $this->v_mongo_id;
    }
    /**
     * function set MongoID to properties
     */
    public function set_mongo_id($p_mongo_id){
        $this->v_mongo_id = $p_mongo_id;
    }
    public function insert($arr_data){
        try{
            $this->collection->insert($arr_data, array('safe'=>true));
            $this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
            return $this->v_mongo_id;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return NULL;
        }
    }
    public function select_one(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        return iterator_to_array($rss);
    }
    public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = NULL;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return $v_ret;
    }
    public function select_next($p_field_name, array $arr_where = array()){
        $arr_order = array($p_field_name => -1);//last insert show first
        $rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
        $v_ret = 0;
        foreach($rss as $arr){
            if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
        }
        return ((int) $v_ret)+1;
    }
    public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr;
    }
    public function select(array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr = $this->collection->find($arr_where)->sort($arr_order);
        return $arr;
    }

    public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_order) || count($arr_order)==0){
            $arr_order = array('_id' => -1);//last insert show first
        }
        $arr_return = array();
        $arr_field = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr_field[$arr_fields[$i]] = 1;
        if($p_row <= 0)
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
        else
            $arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
        return $arr_return;
    }
    public function update(array $arr_where = array(),$arr_data = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, $arr_data, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return false;
        }
    }
    public function delete(array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->remove($arr_where, array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return false;
        }
    }

    /**
     * function update one or more records
     * @param $p_field string, name of field
     * @param $p_value = mix value, asigned to field
     * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
     * @return boolean
     */
    public function update_field($p_field, $p_value, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return false;
        }
    }
    public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$set' => $arr));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return false;
        }
    }
    public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        try{
            $this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return false;
        }
    }
    public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        $arr = array();
        for($i=0; $i<count($arr_fields); $i++)
            $arr[$arr_fields[$i]] = $arr_values[$i];
        try{
            $this->collection->update($arr_where, array('$inc' => array($arr)));
            return true;
        }catch(MongoCursorException $e){
            $this->v_error_code = $e->getCode();
            $this->v_error_message = $e->getMessage();
            return false;
        }
    }
    public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array()){
        if(is_null($arr_where) || count($arr_where)==0){
            $v_has_mongo_id = !is_null($this->v_mongo_id);
            if($v_has_mongo_id)
                $arr_where = array('_id' => $this->v_mongo_id);
        }
        if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
        $arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
        $v_dsp_option = '';
        foreach($arr as $a){
            if($a[$p_field_value] == $p_selected_value)
                $v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
            else
                $v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
        }
        return $v_dsp_option;
    }
    public function count(array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->count();
        else
            return $this->collection->find($arr_where)->count();
    }
    public function count_field($p_field, array $arr_where = array()){
        if(is_null($arr_where) || (count($arr_where)==0))
            return $this->collection->find(array($p_field => array('$exists' => true)))->count();
        else
            return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
    }
}
?>