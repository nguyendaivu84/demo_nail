<?php
class cls_tb_nail_credit_card{

	private $v_cart_id = 0;
	private $v_cart_type = 0;
	private $v_cart_number = '';
	private $v_expiration_month = 0;
	private $v_expiration_year = 0;
	private $v_first_name = '';
	private $v_last_name = '';
	private $v_user_id = 0;
	private $v_address_line_1 = '';
	private $v_address_line_2 = '';
	private $v_city_name = '';
	private $v_zip_code = '';
	private $v_state_id = 0;
	private $v_country_id = 0;
	private $v_phone = '0000-00-00 00:00:00';
	private $v_default_cart = 0;
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
	
	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection('tb_nail_credit_card');
		$this->collection->ensureIndex(array("cart_number"=>1));
		$this->collection->ensureIndex(array("expiration_month"=>1));
		$this->collection->ensureIndex(array("expiration_year"=>1));
		$this->v_phone = new MongoDate(time());
		$this->collection->ensureIndex(array("cart_id"=>1), array('name'=>"cart_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = 'tb_nail_credit_card';
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "cart_id" value
	 * @return int value
	 */
	public function get_cart_id(){
		return (int) $this->v_cart_id;
	}

	
	/**
	 * function allow change properties "cart_id" value
	 * @param $p_cart_id: int value
	 */
	public function set_cart_id($p_cart_id){
		$this->v_cart_id = (int) $p_cart_id;
	}

	
	/**
	 * function return properties "cart_type" value
	 * @return int value
	 */
	public function get_cart_type(){
		return (int) $this->v_cart_type;
	}

	
	/**
	 * function allow change properties "cart_type" value
	 * @param $p_cart_type: int value
	 */
	public function set_cart_type($p_cart_type){
		$this->v_cart_type = (int) $p_cart_type;
	}

	
	/**
	 * function return properties "cart_number" value
	 * @return string value
	 */
	public function get_cart_number(){
		return $this->v_cart_number;
	}

	
	/**
	 * function allow change properties "cart_number" value
	 * @param $p_cart_number: string value
	 */
	public function set_cart_number($p_cart_number){
		$this->v_cart_number = $p_cart_number;
	}

	
	/**
	 * function return properties "expiration_month" value
	 * @return int value
	 */
	public function get_expiration_month(){
		return (int) $this->v_expiration_month;
	}

	
	/**
	 * function allow change properties "expiration_month" value
	 * @param $p_expiration_month: int value
	 */
	public function set_expiration_month($p_expiration_month){
		$this->v_expiration_month = (int) $p_expiration_month;
	}

	
	/**
	 * function return properties "expiration_year" value
	 * @return int value
	 */
	public function get_expiration_year(){
		return (int) $this->v_expiration_year;
	}

	
	/**
	 * function allow change properties "expiration_year" value
	 * @param $p_expiration_year: int value
	 */
	public function set_expiration_year($p_expiration_year){
		$this->v_expiration_year = (int) $p_expiration_year;
	}

	
	/**
	 * function return properties "first_name" value
	 * @return string value
	 */
	public function get_first_name(){
		return $this->v_first_name;
	}

	
	/**
	 * function allow change properties "first_name" value
	 * @param $p_first_name: string value
	 */
	public function set_first_name($p_first_name){
		$this->v_first_name = $p_first_name;
	}

	
	/**
	 * function return properties "last_name" value
	 * @return string value
	 */
	public function get_last_name(){
		return $this->v_last_name;
	}

	
	/**
	 * function allow change properties "last_name" value
	 * @param $p_last_name: string value
	 */
	public function set_last_name($p_last_name){
		$this->v_last_name = $p_last_name;
	}

	
	/**
	 * function return properties "user_id" value
	 * @return int value
	 */
	public function get_user_id(){
		return (int) $this->v_user_id;
	}

	
	/**
	 * function allow change properties "user_id" value
	 * @param $p_user_id: int value
	 */
	public function set_user_id($p_user_id){
		$this->v_user_id = (int) $p_user_id;
	}

	
	/**
	 * function return properties "address_line_1" value
	 * @return string value
	 */
	public function get_address_line_1(){
		return $this->v_address_line_1;
	}

	
	/**
	 * function allow change properties "address_line_1" value
	 * @param $p_address_line_1: string value
	 */
	public function set_address_line_1($p_address_line_1){
		$this->v_address_line_1 = $p_address_line_1;
	}

	
	/**
	 * function return properties "address_line_2" value
	 * @return string value
	 */
	public function get_address_line_2(){
		return $this->v_address_line_2;
	}

	
	/**
	 * function allow change properties "address_line_2" value
	 * @param $p_address_line_2: string value
	 */
	public function set_address_line_2($p_address_line_2){
		$this->v_address_line_2 = $p_address_line_2;
	}

	
	/**
	 * function return properties "city_name" value
	 * @return string value
	 */
	public function get_city_name(){
		return $this->v_city_name;
	}

	
	/**
	 * function allow change properties "city_name" value
	 * @param $p_city_name: string value
	 */
	public function set_city_name($p_city_name){
		$this->v_city_name = $p_city_name;
	}

	
	/**
	 * function return properties "zip_code" value
	 * @return string value
	 */
	public function get_zip_code(){
		return $this->v_zip_code;
	}

	
	/**
	 * function allow change properties "zip_code" value
	 * @param $p_zip_code: string value
	 */
	public function set_zip_code($p_zip_code){
		$this->v_zip_code = $p_zip_code;
	}

	
	/**
	 * function return properties "state_id" value
	 * @return int value
	 */
	public function get_state_id(){
		return (int) $this->v_state_id;
	}

	
	/**
	 * function allow change properties "state_id" value
	 * @param $p_state_id: int value
	 */
	public function set_state_id($p_state_id){
		$this->v_state_id = (int) $p_state_id;
	}

	
	/**
	 * function return properties "country_id" value
	 * @return int value
	 */
	public function get_country_id(){
		return (int) $this->v_country_id;
	}

	
	/**
	 * function allow change properties "country_id" value
	 * @param $p_country_id: int value
	 */
	public function set_country_id($p_country_id){
		$this->v_country_id = (int) $p_country_id;
	}

	
	/**
	 * function return properties "phone" value
	 * @return int value indicates amount of seconds
	 */
	public function get_phone(){
		return  $this->v_phone->sec;
	}

	
	/**
	 * function allow change properties "phone" value
	 * @param $p_phone: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_phone($p_phone){
		if($p_phone=='') $p_phone = NULL;
		if(!is_null($p_phone)){
			try{
				$this->v_phone = new MongoDate(strtotime($p_phone));
			}catch(MongoException $me){
				$this->v_phone = NULL;
			}
		}else{
			$this->v_phone = NULL;
		}
	}

	
	/**
	 * function return properties "default_cart" value
	 * @return int value
	 */
	public function get_default_cart(){
		return (int) $this->v_default_cart;
	}

	
	/**
	 * function allow change properties "default_cart" value
	 * @param $p_default_cart: int value
	 */
	public function set_default_cart($p_default_cart){
		$this->v_default_cart = (int) $p_default_cart;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
		$arr = array('cart_id' => $this->v_cart_id
					,'cart_type' => $this->v_cart_type
					,'cart_number' => $this->v_cart_number
					,'expiration_month' => $this->v_expiration_month
					,'expiration_year' => $this->v_expiration_year
					,'first_name' => $this->v_first_name
					,'last_name' => $this->v_last_name
					,'user_id' => $this->v_user_id
					,'address_line_1' => $this->v_address_line_1
					,'address_line_2' => $this->v_address_line_2
					,'city_name' => $this->v_city_name
					,'zip_code' => $this->v_zip_code
					,'state_id' => $this->v_state_id
					,'country_id' => $this->v_country_id
					,'phone' => $this->v_phone
					,'default_cart' => $this->v_default_cart);
		try{
			$this->collection->insert($arr, array('safe'=>true));
			$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_nail_credit_card` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_cart_id = isset($arr['cart_id'])?$arr['cart_id']:0;
			$this->v_cart_type = isset($arr['cart_type'])?$arr['cart_type']:0;
			$this->v_cart_number = isset($arr['cart_number'])?$arr['cart_number']:'';
			$this->v_expiration_month = isset($arr['expiration_month'])?$arr['expiration_month']:0;
			$this->v_expiration_year = isset($arr['expiration_year'])?$arr['expiration_year']:0;
			$this->v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
			$this->v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
			$this->v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
			$this->v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
			$this->v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
			$this->v_city_name = isset($arr['city_name'])?$arr['city_name']:'';
			$this->v_zip_code = isset($arr['zip_code'])?$arr['zip_code']:'';
			$this->v_state_id = isset($arr['state_id'])?$arr['state_id']:0;
			$this->v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
			$this->v_phone = isset($arr['phone'])?$arr['phone']:(new MongoDate(time()));
			$this->v_default_cart = isset($arr['default_cart'])?$arr['default_cart']:0;
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `cart_id` FROM `tb_nail_credit_card` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select_scalar('cart_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `cart_id` FROM `tb_nail_credit_card` WHERE `user_id`=2 ORDER BY `cart_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select_next('cart_id',array('user_id'=>2), array('cart_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return ((int) $v_ret)+1;
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
	
	/**
	 * function select distinct
	 * @param $p_field_name string, name of selected field
	 * @example: 
	 * <code>
	 *         SELECT DISTINCT `name` FROM `tbl_users`
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select_distinct('nam')
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_distinct($p_field_name){
		return $this->collection->command(array("distinct"=>"tb_nail_credit_card", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_nail_credit_card($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('cart_id' => $this->v_cart_id,'cart_type' => $this->v_cart_type,'cart_number' => $this->v_cart_number,'expiration_month' => $this->v_expiration_month,'expiration_year' => $this->v_expiration_year,'first_name' => $this->v_first_name,'last_name' => $this->v_last_name,'user_id' => $this->v_user_id,'address_line_1' => $this->v_address_line_1,'address_line_2' => $this->v_address_line_2,'city_name' => $this->v_city_name,'zip_code' => $this->v_zip_code,'state_id' => $this->v_state_id,'country_id' => $this->v_country_id,'phone' => $this->v_phone,'default_cart' => $this->v_default_cart));
		 else 
			$arr = array('$set' => array('cart_type' => $this->v_cart_type,'cart_number' => $this->v_cart_number,'expiration_month' => $this->v_expiration_month,'expiration_year' => $this->v_expiration_year,'first_name' => $this->v_first_name,'last_name' => $this->v_last_name,'user_id' => $this->v_user_id,'address_line_1' => $this->v_address_line_1,'address_line_2' => $this->v_address_line_2,'city_name' => $this->v_city_name,'zip_code' => $this->v_zip_code,'state_id' => $this->v_state_id,'country_id' => $this->v_country_id,'phone' => $this->v_phone,'default_cart' => $this->v_default_cart));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>