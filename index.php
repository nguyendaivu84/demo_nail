<?php
//session_cache_limiter('private_no_expire');
session_start();
ob_start();
error_reporting(E_ALL);
$v_sval = 1;
include 'const.php';
include 'design_constants.php';
include 'config.php';
include 'connect.php';
require 'functions/index.php';
//if(isset($_SESSION['ss_user_demo'])) unset($_SESSION['ss_user_demo']);
date_default_timezone_set($v_server_timezone);
$arr_user = create_user();

$a = isset($_GET['a'])?$_GET['a']:'';
if($a =='LO'){
    if(isset($_SESSION['ss_customer'])){
        unset($_SESSION['ss_customer']);
        unset($_SESSION['ss_basket_content']);
        unset($_SESSION['ss_basket_item']);
        unset($_SESSION['ss_basket']);
        unset($_SESSION['ss_address_shipping']);
        unset($_SESSION['ss_billing_address']);
        unset($_SESSION['ss_cart']);
    }
    else session_destroy();
    redir(URL);
}
if(isset($_GET['txt_language'])){
    $v_language_get = $_GET['txt_language'];
    if(!isset($cls_settings)){
        add_class("cls_settings");
        $cls_settings = new cls_settings($db);
        $v_check = $cls_settings->get_option_id_by_key("language_setting",$v_language_get);
        if($v_check>=0){
            $_SESSION['ss_language'] = $v_language_get;
        }
    }
}
include 'user_account/index.php';
require 'disconnect.php';