<?php
class CompaniesController extends MobileAppController {
	var $modelName = 'Company';
	var $name = 'Companies';
	function beforeFilter() {
		parent::beforeFilter();
	}
	public function popup($key = "") {
		$this->set('key', $key);

		$limit = 100; $skip = 0;
		$page_num = 1;
		if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0){

			// $limit = $_POST['pagination']['page-list'];
			$page_num = $_POST['pagination']['page-num'];
			$limit = $_POST['pagination']['page-list'];
			$skip = $limit*($page_num - 1);
		}
		$this->set('page_num', $page_num);
		$this->set('limit', $limit);

		$arr_order = array('no' => 1);
		if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
			$sort_type = 1;
			if( $_POST['sort']['type'] == 'desc' ){
				$sort_type = -1;
			}
			$arr_order = array($_POST['sort']['field'] => $sort_type);

			$this->set('sort_field', $_POST['sort']['field']);
			$this->set('sort_type', ($sort_type === 1)?'asc':'desc');
			$this->set('sort_type_change', ($sort_type === 1)?'desc':'asc');
		}

		$cond = array();
		$cond['inactive'] = 0;
		if (!empty($this->data)) {
			$arr_post = $this->data['Company'];

			if (strlen($arr_post['name']) > 0)
				$cond['name'] = new MongoRegex('/'. (string)$arr_post['name'] .'/i');

			if( isset($arr_post['inactive']) && $arr_post['inactive'] )
				$cond['inactive'] = 1;

			if ( isset($arr_post['is_customer']) &&is_numeric($arr_post['is_customer']) && $arr_post['is_customer']) {
				$cond['is_customer'] = 1;
			}

			if ( isset($arr_post['is_supplier']) &&is_numeric($arr_post['is_supplier']) && $arr_post['is_supplier']) {
				$cond['is_supplier'] = 1;
			}

			if (isset($arr_post['is_shipper'])) {
				$cond['is_shipper'] = 1;
				$this->set( 'is_shipper', 1 );
			}


		}

		if (!empty($_GET)) {

			$tmp = $this->data;

			if (isset($_GET['is_customer'])) {
				$cond['is_customer'] = 1;
				$tmp['Company']['is_customer'] = 1;
			}

			if (isset($_GET['is_supplier'])) {
				$cond['is_supplier'] = 1;
				$tmp['Company']['is_supplier'] = 1;
			}

			if (isset($_GET['name'])) {

				$cond['name'] = new MongoRegex('/'. $_GET['name'] .'/i');
				$tmp['Company']['name'] = $_GET['name'];
			}

			if (isset($_GET['is_shipper']) && is_numeric($_GET['is_shipper']) && $_GET['is_shipper']) {
				$cond['is_shipper'] = 1;
				$this->set( 'is_shipper', 1 );
			}

			$this->data = $tmp;
		}

		$this->selectModel('Company');
		$arr_companies = $this->Company->select_all(array(
			'arr_where' => $cond,
			'arr_order' => $arr_order,
			'arr_field' => array('name', 'addresses', 'addresses_default_key', 'is_customer', 'is_supplier', 'default_address_1', 'default_address_2', 'default_address_3', 'default_town_city', 'default_country_name', 'default_country', 'default_province_state_name', 'default_province_state', 'default_zip_postcode', 'phone', 'fax', 'email', 'web'),
			'limit' => 10,
			'skip' => $skip
		));
		$this->set('arr_company', $arr_companies);

		$total_page = $total_record = $total_current = 0;
		if( is_object($arr_companies) ){
			$total_current = $arr_companies->count(true);
			$total_record = $arr_companies->count();
			if( $total_record%$limit != 0 ){
				$total_page = floor($total_record/$limit) + 1;
			}else{
				$total_page = $total_record/$limit;
			}
		}
		$this->set('total_current', $total_current);
		$this->set('total_page', $total_page);
		$this->set('total_record', $total_record);

		$this->layout = 'ajax';
	}
}