<?php
class ContactsController extends MobileAppController {
	var $modelName = 'Contact';
	var $name = 'Contacts';
	function beforeFilter() {
		parent::beforeFilter();
	}
	public function popup($key = '') {
		$this->set('key', $key);

		if( $key == '_assets' ){
			$this->selectModel('Equipment');
			$arr_equipment = $this->Equipment->select_all(array('arr_order' => array('name' => 1)));
			$this->set( 'arr_equipment', $arr_equipment );
		}

		$limit = 10; $skip = 0; $cond = array();

		// Nếu là search GET
		if (!empty($_GET)) {

			$tmp = $this->data;

			if (isset($_GET['company_id'])) {
				$cond['company_id'] = new MongoId($_GET['company_id']);
				$tmp['Contact']['company'] = $_GET['company_name'];
			}

			if (isset($_GET['is_customer'])) {
				$cond['is_customer'] = 1;
				$tmp['Contact']['is_customer'] = 1;
			}

			if (isset($_GET['is_employee'])) {
				$cond['is_employee'] = 1;
				$tmp['Contact']['is_employee'] = 1;
			}

			$this->data = $tmp;
		}

		// Nếu là search theo phân trang
		$page_num = 1;
		if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0 ){

			// $limit = $_POST['pagination']['page-list'];
			$page_num = $_POST['pagination']['page-num'];
			$limit = $_POST['pagination']['page-list'];
			$skip = $limit*($page_num - 1);
		}
		$this->set('page_num', $page_num);
		$this->set('limit', $limit);

		$arr_order = array('first_name' => 1);
		if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
			$sort_type = 1;
			if( $_POST['sort']['type'] == 'desc' ){
				$sort_type = -1;
			}
			$arr_order = array($_POST['sort']['field'] => $sort_type);

			$this->set('sort_field', $_POST['sort']['field']);
			$this->set('sort_type', ($sort_type === 1)?'asc':'desc');
			$this->set('sort_type_change', ($sort_type === 1)?'desc':'asc');
		}

		// search theo submit $_POST kèm điều kiện
		$cond['inactive'] = 0;
		if (!empty($this->data) && !empty($_POST)) {
			$arr_post = $this->data['Contact'];

			if (isset($arr_post['name']) && strlen($arr_post['name']) > 0) {
				$cond['full_name'] = new MongoRegex('/' . trim($arr_post['name']) . '/i');
			}

			if (strlen($arr_post['company']) > 0) {
				$cond['company'] = new MongoRegex('/' . $arr_post['company'] . '/i');
			}

			if( $arr_post['inactive'] )
				$cond['inactive'] = 1;

			if (is_numeric($arr_post['is_customer']) && $arr_post['is_customer'])
				$cond['is_customer'] = 1;

			if (is_numeric($arr_post['is_employee']) && $arr_post['is_employee'])
				$cond['is_employee'] = 1;
			$inputHolder = (isset($this->data['inputHolder']) ? $this->data['inputHolder'] : '');
			$this->set('inputHolder',$inputHolder);
		}

		if(empty($_POST) && ( isset($cond['is_employee']) && $cond['is_employee'] ) || ( isset($_GET['company_id']) && $_GET['company_id'] == '5271dab4222aad6819000ed0' ) ){
			$limit = 100;
			$this->set('limit', $limit);
		}
		$this->selectModel('Contact');
		$arr_contact = $this->Contact->select_all(array(
			'arr_where' => $cond,
			'arr_order' => $arr_order,
			'limit' => $limit,
			'skip' => $skip
				// 'arr_field' => array('name', 'is_customer', 'is_employee', 'company_id', 'company_name')
		));
		$this->set('arr_contact', $arr_contact);

		$total_page = $total_record = $total_current = 0;
		if( is_object($arr_contact) ){
			$total_current = $arr_contact->count(true);
			$total_record = $arr_contact->count();
			if( $total_record%$limit != 0 ){
				$total_page = floor($total_record/$limit) + 1;
			}else{
				$total_page = $total_record/$limit;
			}
		}
		$this->set('total_current', $total_current);
		$this->set('total_page', $total_page);
		$this->set('total_record', $total_record);

		$this->selectModel('Company');
		$this->set('model_company', $this->Company);

		$this->layout = 'ajax';
	}
}