<?php

App::uses('AppController', 'Controller');

class MobileAppController extends AppController {
	public $viewClass = 'Theme';
	public $theme = 'mobile';
	public $layout = 'mobile';
	public $opm = '';
	function beforeFilter() {
		if ($this->request->is('ajax') && !$this->Session->check('arr_user')) {
			echo 'Your session is time out, please re-login. Thank you.<script type="text/javascript">location.reload(true);</script>';
			exit;
		}
		if ($this->request->url != 'mobile/users/login' && !$this->Session->check('arr_user')) {
			$this->Session->write('REFERRER_LOGIN', '/' . $this->request->url);
			$this->redirect('/mobile/users/login');
		}
		$this->set('controller', $this->params->params['controller']);
		$this->set('model', $this->modelName);
		$this->set('action', $this->params->params['action']);
		$this->set('arr_msg', $this->define_system_message());
		$this->set('arr_menu',$this->rebuild_header());
		$this->set('request',$this->request);
	}
	function rebuild_header(){
		$arr_menu = array(
			'crm' => array(
				'name'	=> 'CRM',
				'companies' => array(
									'name' => 'Company',
									'class' => 'company',
									'inactive' => '0',
								),
				'contacts' => array(
									'name' => 'Contact',
									'class' => 'contacts',
									'inactive'	=> '0',
								),
				'communications' => array(
									'name' => 'Comm',
									'class' => 'comms',
									'inactive'	=> '0',
								),
				'docs' => array(
									'name' => 'Doc',
									'class' => 'docs',
									'inactive'	=> '0',
								),
				'jobs' => array(
									'name' => 'Job',
									'class' => 'jobs',
									'inactive'	=> '0',
								),
				'tasks' => array(
									'name' => 'Task',
									'class' => 'tasks',
									'inactive'	=> '0',
								),
				'timelogs' => array(
				  					'name' => 'TimeLog',
				  					'class' => 'timelog',
				  					'inactive'	=> '1',
				  				),
				'stages' => array(
				 					'name' => 'Stages',
				 					'class' => 'stages',
				 					'inactive'	=> '1',
				 				),
			),
			'sales' => array(
				'name'	=> 'Sales',
				'enquiries' => array(
									'name' => 'Enquiry',
									'class' => 'enquiries',
									'inactive'	=> '0',
								),
				'quotations' => array(
									'name' => 'Quote',
									'class' => 'quotes',
									'inactive'	=> '0',
								),
				'salesorders' => array(
									'name' => 'Sales Ord',
									'class' => 'sales_ord',
									'inactive'	=> '0',
								),
				'salesaccounts' => array(
									'name' => 'Sales Acc',
									'class' => 'sales_acc',
									'inactive'	=> '0',
								),
				'salesinvoices' => array(
									'name' => 'Sales Inv',
									'class' => 'sales_inv',
									'inactive'	=> '0',
								),
				'receipts' => array(
									'name' => 'Receipt',
									'class' => 'receipts',
									'inactive'	=> '0',
								)
			),
			'inventory' => array(
				'name'	=> 'Inventory',

				'products' => array(
								'name' => 'Product',
								'class' => 'products',
								'inactive'	=> '0',
								),
				'locations' => array(
								'name' => 'Location',
								'class' => 'locations',
								'inactive'	=> '0',
								),
				'units' => array(
								'name' => 'Units',
								'class' => 'units',
								'inactive'	=> '0',
								),
				'batches' => array(
								'name' => 'Batches',
								'class' => 'batches',
								'inactive'	=> '1',
								),
				'purchaseorders' => array(
								'name' => 'Pur Order',
								'class' => 'purch_ord',
								'inactive'	=> '0',
								),
				'shippings' => array(
								'name' => 'Shipping',
								'class' => 'shipping',
								'inactive'	=> '0',
								),
			)
		);
		foreach($arr_menu as $menu=>$sub_menu){
			foreach($sub_menu as $controller=>$value){
				if(is_array($value)&&$value['inactive']!=1){
					$model = trim(ucfirst(Inflector::singularize($controller)));
					$this->selectModel($model);
					$count = $this->$model->count();
					$arr_menu[$menu][$controller]['number'] = $count;
				}
			}
		}
		return $arr_menu;
	}
	public function index() {
		if ($this->Session->check($this->name . 'ViewThemes') && $this->Session->read($this->name . 'ViewThemes') != '')
			$views = $this->Session->read($this->name . 'ViewThemes');
		else
			$views = 'entry';
		$this->redirect('/mobile/' . $this->params->params['controller'] . '/' . $views);
		die;
	}
	public function entry(){
		echo 'In Construct<br />';
		echo '<a href="'.M_URL.'/tasks/entry">Come back to Tasks</a>';
		die;
	}
	public function entry_search(){
		echo 'In Construct<br />';
		echo '<a href="'.M_URL.'/tasks/entry">Come back to Tasks</a>';
		die;
	}
	public function lists(){
		echo 'In Construct<br />';
		echo '<a href="'.M_URL.'/tasks/entry">Come back to Tasks</a>';
		die;
	}
	public function options($module_id='',$module_name=''){
		echo 'In Construct<br />';
		echo '<a href="'.M_URL.'/tasks/entry">Come back to Tasks</a>';
		die;
	}
	public function delete($ids = 0) {
		$ids = $this->get_id();
		if ($ids != '') {
			$this->delete_all_associate($ids);
			$str_return = $this->opm->update($ids, 'deleted', true);
			$actions = $this->Session->read($this->name . 'ViewThemes');
		}
		$this->Session->delete($this->name . 'ViewId');
		$this->redirect('/mobile/' . $this->params->params['controller'] . '/entry');
		die;
	}
	function entry_init($id, $num_position, $model, $controller) {
		$this->selectModel($model);
		$cond = array();
		if( $this->Session->check($controller.'_entry_search_cond') ){
			$cond = $this->Session->read($controller.'_entry_search_cond');
		}
		// TH vào khi click entry bình thường
		if ($id != '0') {
			if ($id == 'first') { // called from menu_entry.ctp
				$num_position = 1;
				$arr_tmp = $this->$model->select_one($cond, array(), array('_id' => 1));
			} elseif ($id == 'last') { // called from menu_entry.ctp
				$arr_tmp = $this->$model->select_one($cond, array(), array('_id' => -1));
			} else {
				$arr_tmp = $this->$model->select_one(array('_id' => new MongoId($id)));
				if (!isset($arr_tmp['_id'])) {
					echo 'This record is deleted or not exist, please come back or click to go to <a href="/">JobTraq - Home page</a>';die;
				}
			}
			if(!$this->request->is("ajax"))
				$this->Session->write($this->name . 'ViewId',(string) $arr_tmp['_id']);
		} else {

			if ($this->Session->check($this->name . 'ViewId')) {
				$arr_tmp = $this->$model->select_one(array('_id' => new MongoId($this->Session->read($this->name . 'ViewId'))));
				if (!isset($arr_tmp['_id'])) {
					$this->Session->delete($this->name . 'ViewId');
					$this->redirect('/' . $controller . '/entry');
					die;
				}
				$id = (string) $arr_tmp['_id'];
			}

			if (!isset($arr_tmp['_id'])) {
				$arr_tmp = $this->$model->select_one($cond, array(), array('_id' => -1));
			}
		}

		if (!isset($arr_tmp['_id'])) {
			$this->redirect('/' . $controller . '/add');
			die;
		}
		$this->set('mongoid',$arr_tmp['_id']);
		// this valriable array use in menu_entry.ctp
		$arr_prev = $this->$model->select_one(array_merge($cond, array('_id' => array('$lt' => $arr_tmp['_id']))), array('_id'), array('_id' => -1));
		$this->set('arr_prev', $arr_prev);
		$arr_next = $this->$model->select_one(array_merge($cond, array('_id' => array('$gt' => $arr_tmp['_id']))), array('_id'), array('_id' => 1));
		$this->set('arr_next', $arr_next);
		$sum = $this->$model->count($cond);
		$this->set('sum', $sum);
		if ($id == '0') {
			$num_position = $sum;
		} else {
			$num_position = $sum - $this->$model->count(array_merge($cond, array('_id' => array('$gt' => $arr_tmp['_id']))));
		}
		$this->set('num_position', $num_position); // đếm entry thứ mấy trong tất cả entry
		return $arr_tmp;
	}
	public function prevs($id='') {
		if($id!=''){
			$this->Session->write($this->name . 'ViewId',$id);
		}
		$modelName = $this->modelName;
		$this->selectModel($modelName);
		$this->opm = $this->$modelName;
		if ($this->get_id() != '') {
			$where_query = $this->arr_search_where();

			$where_query['_id'] = array('$lt' => new MongoId($this->get_id()));
			$arr_prev = $this->opm->select_one($where_query, array('_id'), array('_id' => -1));
		}
		if (isset($arr_prev['_id']))
			$prevs = $arr_prev['_id'];
		else
			$prevs = 'first';
		if($this->request->is('ajax')){
			if($prevs=='first')
				die;
			$this->entry($prevs);
			$this->set("ajax",true);
			$this->render('../'.$this->name.'/entry');
		} else
			$this->redirect('/mobile/' . $this->params->params['controller'] . '/entry/' . $prevs);
	}

	// link to next entry
	public function nexts($id='') {
		if($id!=''){
			$this->Session->write($this->name . 'ViewId',$id);
		}
		$modelName = $this->modelName;
		$this->selectModel($modelName);
		$this->opm = $this->$modelName;
		if ($this->get_id() != '') {
			$where_query = $this->arr_search_where();
			$where_query['_id'] = array('$gt' => new MongoId($this->get_id()));
			$arr_nexts = $this->opm->select_one($where_query, array('_id'), array('_id' => 1));
		}
		if (isset($arr_nexts['_id']))
			$nexts = $arr_nexts['_id'];
		else
			$nexts = 'last';
		if($this->request->is('ajax')){
			if($nexts=='last')
				die;
			$this->entry($nexts);
			$this->set("ajax",true);
			$this->render('../'.$this->name.'/entry');
		} else
			$this->redirect('/mobile/' . $this->params->params['controller'] . '/entry/' . $nexts);
	}
	public function get_id() {
		if($this->Session->check($this->name . 'ViewId')){
			$iditem = $this->Session->read($this->name . 'ViewId');
		}
		else {
			//find last id
			if ($this->opm)
				$arr_tmp = $this->opm->select_one(array(), array(), array('_id' => -1));
			else {
				$module = $this->modelName;
				$this->selectModel($module);
				$arr_tmp = $this->$module->select_one(array(), array(), array('_id' => -1));
			}
			if (isset($arr_tmp['_id']) && is_object($arr_tmp['_id'])) {
				$iditem = (array) $arr_tmp['_id'];
				$iditem = $iditem['$id'];
			} else if (isset($arr_tmp['_id']) && strlen($arr_tmp['_id']) == 24) {
				$iditem = $arr_tmp['_id'];
			}
			else
				$iditem = '';
		}
		return $iditem;
	}
	public function getSubTab($className){
		$arr_subtab = array();
		$arr_methods = get_class_methods($className);
		foreach($arr_methods as $method_name){
			if(strpos($method_name, '_sub_tab')){
				$arr_subtab[] = str_replace('_sub_tab', '', $method_name);
			}
		}
		return $arr_subtab;
	}
}
