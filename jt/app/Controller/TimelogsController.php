<?php

App::uses('AppController', 'Controller');

class TimelogsController extends AppController {

    var $modelName = 'Timelog';

    public function beforeFilter() {
        parent::beforeFilter();

        $this->set('title_entry', 'Timelogs');
    }

    function entry($id = '0', $num_position = -1) {
        $arr_tmp = $this->entry_init($id, $num_position, 'Timelog', 'Timelogs');
        $arr_tmp['date'] = (is_object($arr_tmp['date'])) ? date('m/d/Y', $arr_tmp['date']->sec) : '';

        $arr_tmp1['Timelog'] = $arr_tmp;
        $this->data = $arr_tmp1;

        $this->selectModel('Setting');
        $this->set('arr_category', $this->Setting->select_option(array('setting_value' => 'timelogs_category'), array('option')));
        $this->set('arr_employee_type', $this->Setting->select_option(array('setting_value' => 'contacts_type'), array('option')));

        $this->selectModel('Contact');
        $contact_list = $this->Contact->select_list(array(
            'arr_field' => array('_id', 'first_name', 'last_name')
        ));
        $this->set('arr_employee', $contact_list);

        $this->show_footer_info($arr_tmp);
    }

    function lists() {
        $this->selectModel('Timelog');

        $arr_timelogs = $this->Timelog->select_all(array(
            'arr_order' => array('_id' => -1)
        ));
        foreach ($arr_timelogs as $value) {
            $arr_tmp_timelogs[] = $value;
        }
        $this->set('arr_timelogs', $arr_tmp_timelogs);
    }

    function auto_save() {
        if (!empty($this->data)) {
            $arr_post_data = $this->data['Timelog'];
            $arr_save = $arr_post_data;

            $date = $this->Common->strtotime($arr_save['date'] . ' 00:00:00');
            $arr_save['date'] = new MongoDate($date);

            $this->selectModel('Timelog');
            if ($this->Timelog->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Timelog->arr_errors_save[1];
            }
        }
        die;
    }

    function entry_caltime() {
        $data = $this->data['Timelog'];
// get time data
        $start_time = explode(':', $data['start_time']);
        $finish_time = explode(':', $data['finish_time']);
        $or_entered_time = explode(':', $data['or_entered_time']);
// convert Or Entered Time to second
        $total_minutes = $or_entered_time[0] * 60 + $or_entered_time[1];
        $time1 = mktime($finish_time[0], $finish_time[1]);
        $time2 = mktime($start_time[0], $start_time[1]);
// calculator time
// $new_time = Finish time - Start time
        $diff = $time1 - $time2;
        $diff += strtotime(date('Y-m-d'));
        $h = date('H', $diff);
        $m = date('i', $diff);
        $new_time = $h . ':' . $m;
// $total_time = $new_time + $total_minutes
        $total_time = date('H:i', strtotime($new_time . " + $total_minutes minutes"));
        echo $total_time;
        die();
    }

    function _add_get_info_save() {
        $this->selectModel('Timelog');
        $arr_tmp = $this->Timelog->select_one(array(), array(), array('no' => -1));

        $arr_save = array(
            // Employee details
            'no' => 1,
            'employee_name' => '',
            'employee_type' => '',
            // Timelog details
            'date' => new MongoDate(),
            'start_time' => '00:00',
            'finish_time' => '00:00',
            'or_entered_time' => '00:00',
            'total_time' => '00:00',
            'category' => '',
            'billable' => '',
            'billed' => '',
            // job/stage/task details
            'job_no' => '',
            'job_name' => '',
            'stage_no' => '',
            'stage_name' => '',
            'task_no' => '',
            'task_name' => '',
            'customer' => '',
            'comment' => ''
        );
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }

        return $arr_save;
    }

    function add() {
        $arr_save = $this->_add_get_info_save();
        if ($this->Timelog->save($arr_save)) {
            $this->redirect('/timelogs/entry/' . $this->Timelog->mongo_id_after_save);
        } else {
            echo 'Error: ' . $this->Timelog->arr_errors_save[1];
        }
        die;
    }

    function delete($id = 0) {
        $arr_save['_id'] = $id;
        $arr_save['deleted'] = true;

        $this->selectModel('Timelog');
        if ($this->Timelog->save($arr_save)) {
            $this->redirect('/timelogs/entry');
        } else {
            echo 'Error: ' . $this->Timelog->arr_errors_save[1];
        }
    }

    function lists_delete($id = 0) {
        $arr_save['_id'] = $id;
        $arr_save['deleted'] = true;

        $this->selectModel('Timelog');
        if ($this->Timelog->save($arr_save)) {
            echo 'ok';
            die();
        } else {
            echo 'Error: ' . $this->Timelog->arr_errors_save[1];
        }
    }
}