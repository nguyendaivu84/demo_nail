<?php
App::uses('Controller', 'Controller');
class AppController extends Controller {
	var $components = array('Session', 'Common' , 'Cookie');
	var $helpers = array('Html', 'Form', 'Common');
	public $viewClass = 'Theme';
	public $theme = 'default';
	var $uses = null;
	var $modelName = '';
	var $arr_permission = null;
	var $arr_inactive_permission = null;
	function isAuthorized($user) {
		return true;
	}
	var $system_admin = false;

	function beforeFilter() {
		//Tung, auto redirect and use plugin mobile
		if ($this->request->is('mobile'))
			$this->redirect(URL.'/mobile/');
		//$this->Session->write('default_lang','vi');
		//An, 21/2/2014 Use for Mobile
		if(!$this->check_report_pdf()){ //Dùng để bypass SESSION cho phantomjs
			if ($this->request->is('ajax') && !$this->Session->check('arr_user')) {
				echo 'Your session is time out, please re-login. Thank you.<script type="text/javascript">location.reload(true);</script>';
				exit;
			}
			if ($this->request->url != 'users/login' && !$this->Session->check('arr_user')) {
				$this->Session->write('REFERRER_LOGIN', '/' . $this->request->url);
				$this->redirect('/users/login');
				die;
			}
		}
		// $this->theme = 'AnotherExample';
		$this->set('controller', $this->params->params['controller']);
		$this->set('model', $this->modelName);
		$this->set('action', $this->params->params['action']);

		// định nghĩa các thông báo từ hệ thống
		$this->set('arr_msg', $this->define_system_message());
		$arr_permission = array('current_permission'=>array(),'inactive_permission'=>array());
		$arr_inactive_permission = array();

		// BaoNam: kiem tra system admin
		if( isset($_SESSION['arr_user']) && $_SESSION['arr_user']['contact_name'] == 'System Admin' ){
			$this->system_admin = true;
			$this->set('system_admin', true);
		}

		if ($this->request->url != 'users/login' && $this->Session->check('arr_user') && CHECK_PERMISSION && !$this->system_admin) {
			// dùng cho view hoặc function của controller
			$arr_permission['current_permission'] = $this->get_all_permission();
			$arr_permission['inactive_permission'] = $this->get_all_inactive_permission();
			$this->set('arr_menu',$this->rebuild_header_menu($arr_permission));
			/*	Tạm thời ko dùng khái niệm owner
				//Nếu như không có quyền trên toàn controller, và ko có quyền xem tất cả record entry trên controller
				if(!isset($this->arr_permission['all']) &&
				   isset($this->arr_permission[strtolower($this->name).'_@_entry_@_view']) && $this->arr_permission[strtolower($this->name).'_@_entry_@_view']!='all'){
					$arr_where = array('created_by'=>new MongoId($_SESSION['arr_user']['contact_id']));
					$this->Session->write($this->name.'_where_permission',$arr_where);
				}
			*/
		}
		$this->arr_permission = $arr_permission['current_permission'];
		$this->arr_inactive_permission = $arr_permission['inactive_permission'];
		$this->set('arr_permission',$arr_permission);
		$this->get_language();
		if(isset($_SESSION['theme']))
			$this->theme = $_SESSION['theme'];
	}
	function check_report_pdf(){
		if(isset($_GET['print_pdf'])&&trim($_GET['print_pdf'])!=''){
			$token = trim($_GET['print_pdf']);
			$this->selectModel('Stuffs');
			$report_pdf_token = $this->Stuffs->select_one(array('name'=>new MongoRegex('/report_pdf_token/i')));
			if(isset($report_pdf_token['value'])&&!empty($report_pdf_token['value'])){
				foreach($report_pdf_token['value'] as $key=>$value){
					if($token!=$value) continue;
					unset($report_pdf_token['value'][$key]);
					$arr_contact['contact_id'] = new MongoId('100000000000000000000000');
					$arr_contact['contact_name'] = $arr_contact['full_name'] = 'System Admin';
					$arr_contact['first_name'] = 'System';
					$arr_contact['last_name'] = 'Admin';
					$_SESSION['arr_user'] = $arr_contact;
					$this->Stuffs->save($report_pdf_token);
					return true;
				}
			}
		}
		return false;
	}
	function rebuild_header_menu($arr_permission){
		$arr_menu = array();
		//inactive: 0-hoạt động, 1-ẩn, 2-hiện nhưng không hoạt động
		$arr_crm = array(
						'stages' 		=> array(
										'name' => __('Stages'),
										'class' => 'stages',
										'inactive'	=> 1,
								),
						'timelogs'		=> array(
										'name' => __('TimeLog'),
										'class' => 'timelog',
										'inactive'	=> 1,
									),
						'tasks'			=> array(
										'name' => __('Task'),
										'class' => 'tasks',
										'inactive'	=> 1,
									),
						'jobs'			=> array(
										'name' => __('Job'),
										'class' => 'jobs',
										'inactive'	=> 1,
									),
						'docs'			=> array(
										'name' => __('Doc'),
										'class' => 'docs',
										'inactive'	=> 1,
									),
						'communications'=> array(
										'name' => __('Comm'),
										'class' => 'comms',
										'inactive'	=> 1,
									),
						'contacts'		=> array(
										'name' => __('Contact'),
										'class' => 'contacts',
										'inactive'	=> 1,
									),
						'companies'	=>	array(
										'name' => __('Company'),
										'class' => 'company',
										'inactive' =>  1,
									),
						);
		$arr_sales = array(
						'receipts' => array(
											'name' => __('Receipt'),
											'class' => 'receipts',
											'inactive'	=> 1,
										),
						'salesinvoices' => array(
											'name' => __('Sales Inv'),
											'class' => 'sales_inv',
											'inactive'	=> 1,
										),
						'salesaccounts' => array(
											'name' => __('Sales Acc'),
											'class' => 'sales_acc',
											'inactive'	=> 1,
										),
						'salesorders' => array(
											'name' => __('Sales Ord'),
											'class' => 'sales_ord',
											'inactive'	=> 1,
										),
						'quotations' => array(
											'name' => __('Quote'),
											'class' => 'quotes',
											'inactive'	=> 1,
										),
						'enquiries' => array(
											'name' => __('Enquiry'),
											'class' => 'enquiries',
											'inactive'	=> 1,
										),

						   );
		$arr_inventory = array(
						'shippings' => array(
										'name' => __('Shipping'),
										'class' => 'shipping',
										'inactive'	=> 1,
										),
						'purchaseorders' => array(
										'name' => __('Pur Order'),
										'class' => 'purch_ord',
										'inactive'	=> 1,
										),
						'batches' => array(
										'name' => __('Batches'),
										'class' => 'batches',
										'inactive'	=> 1,
										),
						'units' => array(
										'name' => __('Units'),
										'class' => 'units',
										'inactive'	=> 1,
										),
						'locations' => array(
										'name' => __('Location'),
										'class' => 'locations',
										'inactive'	=> 1,
										),
						'products' => array(
										'name' => __('Product'),
										'class' => 'products',
										'inactive'	=> 1,
										),
							   );
		$current_permission = $arr_permission['current_permission'];
		$inactive_permission = $arr_permission['inactive_permission'];
		//Neu co quyen all, mo het tru` timelogs, stages
		if(isset($current_permission['all'])){
			foreach($arr_crm as $key=>$value){
				if($key!='stages' &&  $key!='timelogs'
				   && !isset($inactive_permission[$key.'_@_entry_@_view']))
					$arr_crm[$key]['inactive'] = 0;
			}
			$arr_crm['name'] = __('CRM');
			foreach($arr_sales as $key=>$value)
				if(!isset($inactive_permission[$key.'_@_entry_@_view']))
					$arr_sales[$key]['inactive'] = 0;
			$arr_sales['name'] = __('Sales');
			foreach($arr_inventory as $key=>$value)
				if(!isset($inactive_permission[$key.'_@_entry_@_view']))
					$arr_inventory[$key]['inactive'] = 0;
			$arr_inventory['name'] = __('Inventory');
			$arr_menu['crm'] = array_reverse($arr_crm);
			$arr_menu['sales'] = array_reverse($arr_sales);
			$arr_menu['inventory'] = array_reverse($arr_inventory);
			return $arr_menu;
		}
		//Tên gán ngược vì name sẽ đc gán sau cùng, nếu để name vào luôn thì trong foreach liên tục kiểm tra if ! liên tục (name với sub_menu cùng cấp)
		foreach($arr_crm as $key=>$value){
			if( !isset($inactive_permission[$key.'_@_entry_@_view'])
			   && isset($current_permission[$key.'_@_entry_@_view'])   ){
				$arr_crm[$key]['inactive'] = 0;
				$crm = true;
			}
		}
		foreach($arr_sales as $key=>$value){
			if(  !isset($inactive_permission[$key.'_@_entry_@_view'])
			   && isset($current_permission[$key.'_@_entry_@_view'])   ){
				$arr_sales[$key]['inactive'] = 0;
				$sales = true;
			}
		}
		foreach($arr_inventory as $key=>$value){
			if(  !isset($inactive_permission[$key.'_@_entry_@_view'])
			   && isset($current_permission[$key.'_@_entry_@_view'])   ){
				$arr_inventory[$key]['inactive'] = 0;
				$inventory = true;
			}
		}
		//Mục đích gán $sales,$crm,$inventory vì nếu menu con của nó ko cái nào active hết thì nó (CRM , Sales, Inventory) sẽ ko có trong arr_menu
		//reverse lại vì mình đã sắp theo thứ tự ngược
		//Nếu ở trên sắp thứ tự đúng, và gán name sau cùng vẫn chạy, tuy nhiên chữ CRM lại nằm sau Company,Contact,...
		if(isset($crm)){
			$arr_crm['name'] = __('CRM');
			$arr_menu['crm'] = array_reverse($arr_crm);
		}
		if(isset($sales)){
			$arr_sales['name'] = __('Sales');
			$arr_menu['sales'] = array_reverse($arr_sales);
		}
		if(isset($inventory)){
			$arr_inventory['name'] = __('Inventory');
			$arr_menu['inventory'] = array_reverse($arr_inventory);
		}
		return $arr_menu;
	}
	function beforeRender() {
		if ($this->request->is('ajax')) {
			$this->set('ajax', true);
			$this->layout = 'ajax';
		}
		if (is_object($this->db)) {
			// tạm thời không ngắt kết nối để tối ưu các request đỡ phải mất thời gian kết nối nhiều lần, vd: các ajax trên page chẳng hạn
			// if (!$this->mongo_disconnect()) {
			//     die('Can not disconnect mongodb!');
			// }
		}
	}

	protected $db = null;
	private $connectionDB = null;

	function mongo_connect() {
		if (!is_object($this->connectionDB)) {
			// http://docs.mongodb.org/manual/reference/connection-string/#connections-standard-connection-string-format
			// set Timeout và không cần close nữa
			$this->connectionDB = new Mongo(IP_CONNECT_MONGODB . ':27017?connectTimeoutMS=300000');
			$this->db = $this->connectionDB->selectDB(DB_CONNECT_MONGODB);
		}
	}

	function mongo_disconnect() {
		// $this->db->command(array("logout" => 1));
		return $this->connectionDB->close();
	}

	function selectModel($model) {
		$this->mongo_connect();
		if (is_object($this->db) && !is_object($this->$model)) {
			if (file_exists(APP . 'Model' . DS . $model . '.php')) {
				require_once APP . 'Model' . DS . $model . '.php';
				$this->$model = new $model($this->db);
			} else {
				//echo 'File ' . APP.'Model'.DS.$model.'.php' . ' does not exist';die;
			}
		}
	}

	function get_language(){
		if(!isset($_SESSION['default_lang']) || $_SESSION['default_lang'] == DEFAULT_LANG)
			return array();
		$key = $_SESSION['default_lang'];
		$arr_tmp = array();
		// if(!isset($_SESSION['arr_language_'.$key])){
			$this->selectModel('Languagedetail');
			$arr_temp = array();
			$arr_language = $this->Languagedetail->select_all(array(
				'arr_where' => array(
					'content.'.$key => array(
						'$exists'=>true,
						'$ne' => ''
					),
					'key' => array(
					   '$ne'=>''
					),
				),
				'arr_order' => array('key' => 1),
			));
			foreach($arr_language as $keys => $value){
				// $arr_tmp[$value['key']] = isset($value['content'][$key])?$value['content'][$key]:'';
				$arr_tmp[$value['content']['en']] = isset($value['content'][$key])?$value['content'][$key]:'';
			}
			$_SESSION['arr_language_'.$key] = $arr_tmp;
		// }
	}

	function define_system_message() {
		$arr_messages = array();
		$this->Session->write('arr_messages', array());
		$this->selectModel('SettingMessage');
		$arr_messages = $this->SettingMessage->select_list(array(
			'arr_field' => array('key', 'content'),
		));
		$this->Session->write('arr_messages', $arr_messages);
		return $arr_messages;
	}

	//nhận tên dựa vào id
	function get_name($module = '', $module_id = '', $fieldname = 'name') {
		//for ajax
		if (isset($_POST['module']))
			$module = $_POST['module'];
		if (isset($_POST['module_id']))
			$module_id = $_POST['module_id'];
		if (isset($_POST['fieldname']))
			$fieldname = $_POST['fieldname'];

		$names = '';
		if ($module_id == '') {
			$module_id = $this->get_id();
		}

		if($module == '')
			$module = $this->modelName;

		//process
		$names = '';

		if (!is_object($module_id))
			$module_id = new MongoId($module_id);
		$this->selectModel($module);
		$datalist = array($fieldname);
		if ($module == 'Contact'){
			$datalist = array('first_name', 'last_name');
			$query = $this->$module->collection->findOne(array('deleted' => false, '_id' => $module_id), $datalist);
			$names = $query['first_name'] . ' ' . $query['last_name'];
		}
		else
		{
			if( $module == 'Shipper' ){
				$module = 'Company';
				$this->selectModel('Company');
			}
			$query = $this->$module->collection->findOne(array('deleted' => false, '_id' => $module_id), $datalist);
			if (isset($query[$fieldname]))
				$names = $query[$fieldname];
			else
				$names='';
		}

		//output
		if (isset($_POST['module'])) {
			echo $names;
			die;
		}
		else
			return $names;
	}

	//chuyển text controller thành text module
	function ModuleName($controller = '') {
		if ($controller == '')
			$controller = $this->params->params['controller'];
		$modulename = preg_replace("/s$/i", "", $controller);
		$modulename = preg_replace("/ie$/i", "y", $modulename);
		return ucfirst($modulename);
	}

	/* ======  BASIC MODULE FUNCTION ======

	  The Basic module include these functions:

	  set_module_before_filter()
	  index()					: action index
	  add()					: action add
	  ajax_save()				: ajax save
	  delete(id)				: action delete
	  get_id()				: find this id
	  get_pages_order()		: get page for this id by oder
	  get_pages()				: get page for this id
	  first() 				: action first
	  prevs()					: action prevs
	  nexts()					: action nexts
	  lasts()					: action lasts
	  entry_search()			: Entry search
	  popup()					: popup listview
	  printpdf()				: print pdf frame
	  sub_tab($sub_tab)		: Subtab
	  general_select(array)	: general options of selects in arr_setting
	  show_footer_info(array)	: show footer info in entry
	  save_option(array)		: add/update option(value array) of modules
	  get_default_rel($opname,$flat): nhận giá trị default của field trong arr setting box
	  get_fields_rel($field_name): field chính có rel_name link đến 1 box trong subtab,dựa vào nhận danh sách field
	  deleteopt()				: delete option (fields have type is array)
	  search_list()			:
	  update_default()		:
	  province()				:
	  general_province()		:
	  country()				:
	  general_country()		:
	  ajax_general_province()	:
	  get_data_other_model($from,$setup_ass)
	  get_data_form_module()	:
	  save_muti_field()		:
	  save_data_form_to()		:
	  reload_address()		:
	  reload_cc($company_id='')
	  get_email_of_company($company_id)
	  cal_sum_by_request($opname,$amount_field)

	 */

	public $name;
	public $opm; //Option Module
	public $sub_tab_default;
	public $lists_mod;
	public $is_popup = false;

	//set_module_before_filter()
	public function set_module_before_filter($module) {

		$this->selectModel($module);
		$this->opm = $this->$module;
		$this->set('name', $this->name);
		//kiểm tra và khóa dựa vào seeting
		if ($this->get_id()!='' || $this->get_last_id()!='')
			$this->rebuild_setting();

		//kiem tra session sub tab với setting subtab de tranh bao loi k oton tai trong arr setting
		$this->check_subtab();
		// Get setting from Module
		$arr_set = $this->opm->arr_settings;
		if ($this->params->params['action'] != 'entry')
			$this->set('arr_settings', $arr_set);
		// Render Option Field into arr_options. Struction: arr_options['fieldname']= 'htmlstring';
		$datas = $this->general_select($arr_set['field']);
		$this->set('arr_options', $datas);
		//Save session of sub tab
		if (isset($this->params->params['pass'][0]) && strlen($this->params->params['pass'][0]) == 24)
			$this->set('iditem', $this->params->params['pass'][0]);
		else if ($this->Session->read($this->name . 'ViewId') != '')
			$this->set('iditem', $this->Session->read($this->name . 'ViewId'));
		else {
			//find last id
			$where_query = $this->arr_search_where();
			$arr_tmp = $this->opm->select_one($where_query, array(), array('_id' => -1));
			if (isset($arr_tmp['_id'])) {
				$direct = (array) $arr_tmp['_id'];
				$this->set('iditem', $direct['$id']);
			}
			else
				$this->set('iditem', '');
		}
		$entry_menu['page'] = $this->get_pages();
		$where_query = $this->arr_search_where();
		$entry_menu['search'] = $this->opm->count($where_query);
		$entry_menu['total'] = $this->opm->count($where_query);
		$this->set('entry_menu', $entry_menu);
	}



	//kiểm tra và khóa dựa vào setting. custom trong từng module
	public function rebuild_setting($arr_setting=array()){
		$model_name = $this->modelName;
		$this->selectModel($model_name);
		$arr_setting = $this->$model_name->arr_settings;
		if(!$this->check_permission($this->name.'_@_entry_@_edit')){
			$arr_setting = $this->$model_name->set_lock(array(),'out');
			$this->set('address_lock', '1');
		}
		if(!$this->check_permission('products_@_entry_@_view')){
			//Cac module khac
			unset($arr_setting['relationship']['line_entry']['block']['products']['link'],$arr_setting['relationship']['text_entry']['block']['products']['link']);
			//Dung cho QT
			unset($arr_setting['relationship']['line_entry']['block']['products']['field']['sku']['link_field']);
		}
		$this->$model_name->arr_settings = $arr_setting;
		$this->rebuild_line_text_entry();
		$arr_tmp = $this->$model_name->arr_field_key('cls');
		$arr_link = array();
		if(!empty($arr_tmp))
			foreach($arr_tmp as $key=>$value)
				$arr_link[$value][] = $key;
		$this->set('arr_link',$arr_link);
	}


	//check subtab off. Dựa vào arr_setting
	public function check_subtab(){
		if ($this->Session->check($this->name . '_sub_tab')) {
			$sub_tab = $this->Session->read($this->name . '_sub_tab');
			$arr_setting = $this->opm->arr_settings;
			if(isset($arr_setting['relationship']))
				if(!isset($arr_setting['relationship'][$sub_tab]) && count($arr_setting['relationship'])>0){
					foreach($arr_setting['relationship'] as $kk=>$vv){
						$this->Session->write($this->name . '_sub_tab',$kk);
						break;
					}
			}
		}
	}


	//Index action
	public function index() {
		if ($this->Session->check($this->name . 'ViewThemes') && $this->Session->read($this->name . 'ViewThemes') != '')
			$views = $this->Session->read($this->name . 'ViewThemes');
		else
			$views = 'entry';
		$this->redirect('/' . $this->params->params['controller'] . '/' . $views);
		die;
	}


	//Entry action
	public function entry() {
		if(!$this->check_permission($this->name.'_@_entry_@_view'))
			$this->error_auth();
		$this->Session->write($this->name . 'ViewThemes', 'entry');
		$this->SetModelClass();
		if (!$this->element_exit('entry'))
			$this->render('../Elements/entry');
	}



	/**
	* List view action
	* @author vu.nguyen
	* @since v1.0
	*/
	public function lists() {
		if(!$this->check_permission($this->name.'_@_entry_@_view'))
			$this->error_auth();
		// --- BaoNam sort phan trang ---
		$limit = LIST_LIMIT; $skip = 0;
		// dùng cho sort
		$sort_field = '_id';
		$sort_type = -1;
		if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
			if( $_POST['sort']['type'] == 'desc' ){
				$sort_type = -1;
			}else{
				$sort_type = 1;
			}
			$sort_field = $_POST['sort']['field'];
			$this->Session->write($this->name.'_lists_search_sort', array($sort_field, $sort_type));

		}elseif( $this->Session->check($this->name.'_lists_search_sort') ){
			$session_sort = $this->Session->read($this->name.'_lists_search_sort');
			$sort_field = $session_sort[0];
			$sort_type = $session_sort[1];
		}
		$arr_order = array($sort_field => $sort_type);
		$this->set('sort_field', $sort_field);
		$this->set('sort_type', ($sort_type === 1)?'asc':'desc');
		// BaoNam sort phan trang

		// dùng cho phân trang
		$page_num = 1;
		if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0){
			$page_num = $_POST['pagination']['page-num'];
			$limit = $_POST['pagination']['page-list'];
			$skip = $limit*($page_num - 1);
		}

		$this->set('page_num', $page_num);
		$this->set('limit', $limit);
		// --- end --- BaoNam phan trang

		$where_query = $this->arr_search_where();

//unset($where_query['company_id'], $where_query['product_type'], $where_query['status']);
//pr($where_query);
		//neu khong phai la POPUP
		if(!$this->is_popup){
			$arr_query = $this->opm->select_all(array(
				'arr_where' => $where_query,
				'arr_order' => $arr_order,
				'limit' => $limit,
				'skip' => $skip
			));


		//neu la POPUP
		}else{
			// BaoNam, dùng cho popup
			$arr_order = array('code' => 1);
			if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
				$sort_type = 1;
				if( $_POST['sort']['type'] == 'desc' ){
					$sort_type = -1;
				}
				$arr_order = array('code'=>1,$_POST['sort']['field'] => $sort_type);

				$this->set('sort_field', $_POST['sort']['field']);
				$this->set('sort_type', ($sort_type === 1)?'asc':'desc');
				$this->set('sort_type_change', ($sort_type === 1)?'desc':'asc');
			}

			// BaoNam ============== PRODUCT ================================
			$limit = 100; $skip = 0;
			if( $this->name == 'Products'){
				$limit = 100; $skip = 0;
				$page_num = 1;
				if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0 ){
					$page_num = $_POST['pagination']['page-num'];
					$limit = $_POST['pagination']['page-list'];
					$skip = $limit*($page_num - 1);
				}
				$this->set('page_num', $page_num);
				$this->set('limit', $limit);
				// BaoNam -- kiem tra search popup
				if( isset($where_query['name']) && isset($_POST['products_name']) && strlen($_POST['products_name']) > 0 ){
					$cond_tmp = $where_query['name'];
					unset($where_query['name']);
					$where_query['$or'] = array(
						array('code' => (int)$_POST['products_name']),
						array('sku' => $cond_tmp),
						array('name' => $cond_tmp)
					);
				}
				// end
				if (!empty($_GET)) {
					if (isset($_GET['company_id'])) { // $_GET['company_name']
						$arr_where['company_name']['values'] = $_GET['company_name'];
						$this->set('arr_where', $arr_where);
						$this->set('popup_company_id', $_GET['company_id']);
					}
				}
			}
			//============== END PRODUCT ================================

			// a.Vu code
			$arr_query = $this->opm->select_all(array(
				'arr_where' => $where_query,
				'arr_order' => $arr_order,// BaoNam
				'limit' => $limit, // BaoNam
				'skip' => $skip // BaoNam
			));
		}

		// --- BaoNam ---
		$total_page = $total_record = $total_current = 0;
		if( is_object($arr_query) ){
			$total_current = $arr_query->count(true);
			$total_record = $arr_query->count();
			if( $total_record%$limit != 0 ){
				$total_page = floor($total_record/$limit) + 1;
			}else{
				$total_page = $total_record/$limit;
			}
		}

		$this->set('total_current', $total_current);
		$this->set('total_page', $total_page);
		$this->set('total_record', $total_record);
		// --- BaoNam dùng cho popup và lists ---

		$this->set('arr_list', $arr_query);
		$list_field = $this->opm->list_view_field();
		$arr_set = $this->opm->arr_field_multi_key(array('name', 'type', 'cls', 'id', 'droplist'));
		$this->set('list_field', $list_field);
		$this->set('arr_set', $arr_set);

		foreach ($arr_set['type'] as $keys => $values) {
			if ($values == 'select')
				$opt_select[$keys] = $this->Setting->select_option_vl(array('setting_value' => $arr_set['droplist'][$keys]));
		}
		$this->set('opt_select', $opt_select);
		$this->SetModelClass();

		$this->Session->write($this->name . 'ViewThemes', 'lists');

		//Truong hop sort hoac load ajax
		if ($this->request->is('ajax') && !$this->is_popup) {
			$this->render('../Elements/lists_ajax');

		// Trường hợp không custom view
		}else if (!$this->is_popup && (!$this->element_exit('lists') || ($this->element_exit('lists') && $this->lists_mod != ''))){
			$this->render('../Elements/lists');

		 // Trường hợp custom view trong module
		}
	}


	// Set module class dùng cho view
	public function SetModelClass() {
		$arr_set = $this->opm->arr_field_multi_key(array('type', 'cls', 'syncname','list_syncname'));
		$syncname = $list_syncname = array();
		if(isset($arr_set['cls']))
		foreach ($arr_set['cls'] as $kk => $vv) {
			if (isset($vv)) {
				$arr_list = array();
				$modulename = $this->ModuleName($vv);
				if (!in_array($modulename, $arr_list)) {
					$arr_list = array_push($arr_list, $modulename);
					$moduleclass = $vv . '_class';
					$this->selectModel($modulename);
					$this->set($moduleclass, $this->$modulename);
					if (isset($arr_set['syncname'][$kk]))
						$syncname[$kk] = $arr_set['syncname'][$kk];
					if (isset($arr_set['list_syncname'][$kk]))
						$list_syncname[$kk] = $arr_set['list_syncname'][$kk];
					else
						$syncname[$kk] = 'name';
				}
			}
		}
		$this->set('syncname', $syncname);
		$this->set('list_syncname', $list_syncname);
	}

	// Add action
	public function add() {
		if(!$this->check_permission($this->name.'_@_entry_@_add'))
			$this->error_auth();
		$ids = $this->opm->add('name', '');
		$newid = explode("||", $ids);
		$this->Session->write($this->name . 'ViewId', $newid[0]);
		$this->redirect('/' . $this->params->params['controller'] . '/entry');
		die;
	}

	//Add or update field when this field change, use in js.ctp
	public function ajax_save() {
		if (isset($_POST['field']) && isset($_POST['value']) && isset($_POST['func']) && !in_array((string) $_POST['field'], $this->opm->arr_autocomplete())) {
			if($_POST['func']=='update'&&!$this->check_permission($this->name.'_@_entry_@_edit')){
				echo 'You do not have permission on this action.';
				die;
			}
			if($_POST['func']=='add'&&!$this->check_permission($this->name.'_@_entry_@_add')){
				echo 'You do not have permission on this action.';
				die;
			}
			$values = $_POST['value'];
			if (preg_match("/_date$/", $_POST['field']))
				$values = $this->Common->strtotime($values . '00:00:00');
//echo $_POST['func'];die;
			if ($_POST['func'] == 'add') {
				$ids = $this->opm->add($_POST['field'], $values);
				$newid = explode("||", $ids);
				$this->Session->write($this->name . 'ViewId', $newid[0]);
			} else if ($_POST['func'] == 'update' && isset($_POST['ids'])) {
				if($_POST['field']=='email')
				{
					if (strlen($values) > 0 && !filter_var($values, FILTER_VALIDATE_EMAIL)) {
						echo 'email_not_valid';
						die;
					}
				} else if(preg_match("/status$/i", $_POST['field'])
						  && ($this->name == 'Salesorders' || $this->name == 'Quotations' ) ){
					$this->auto_action($_POST['value']);
				}
				$ids = $this->opm->update($_POST['ids'], $_POST['field'], $values);
				$this->Session->write($this->name . 'ViewId', $_POST['ids']);
			}
			echo $ids;
		}
		else
			echo 'error';
		die;
	}


	/**
	* Ajax save data
	* This is new ajax save function, return array data, more sercury
	* The first, you need setup $this->arr_associated_data()
	* @use: call ajax from js file
	* @author vu.nguyen
	* @since v1.0
	*/
	public function save_data($field = '', $value = '', $ids = '', $valueid = '') {
		if (isset($_POST['field']))
			$field = $_POST['field'];
		if (isset($_POST['value']))
			$value = $_POST['value'];
		if (isset($_POST['ids']))
			$ids = $_POST['ids'];
		if (isset($_POST['valueid']))
			$valueid = $_POST['valueid'];
		$arr_save = $arr_temp = array();

		if ($field != '' && !in_array((string) $field, $this->opm->arr_nonsave())) {

			$arr_save = $this->set_mongo_data($field, $value); //check & format data
			if ($ids != '')
				$arr_save['_id'] = new MongoId($ids);
			$arr_temp = $this->arr_associated_data($field, $value, $valueid); //set more associated fields : các dữ liệu liên quan
			$arr_save = array_merge((array) $arr_save, (array) $arr_temp);
			if ($this->opm->save($arr_save)) {
				echo json_encode($this->arr_list_changed($arr_save));
			}
			else
				echo 'Can not save.';
		}
		die;
	}

	//kiem tra loai data dua vao field và value
	public function set_mongo_data($field = '', $value = '') {
		$arr_temp = array();
		if (preg_match("/_date$/", $field) && (int)$value>0)
			$arr_temp[$field] = new MongoDate($value);
		else if(preg_match("/_date$/", $field)){
			$values = $this->Common->strtotime($values . '00:00:00');
			$arr_temp[$field] = new MongoDate($value);
		}else if (preg_match("/_id$/", $field))
			$arr_temp[$field] = new MongoId($value);
		else
			$arr_temp[$field] = $value;
		return $arr_temp;
	}


	//kiem tra loai data dua vao field và value
	public function set_mongo_array($arr = array()) {
	   $arr_temp = array();
	   if(count($arr)>0){
		   foreach($arr as $field=>$value){
			   if (preg_match("/_date$/", $field))
					$arr_temp[$field] = new MongoDate($value);
			   else if (preg_match("/_id$/", $field))
					$arr_temp[$field] = new MongoId($value);
			   else
					$arr_temp[$field] = $value;

		   }
	   }
	   return $arr_temp;
	}


	//custom in ModuleModel
	public function arr_associated_data($field = '', $value = '', $valueid = '') {
		$arr_return = array();
		$arr_return[$field] = $value;
		return $arr_return;
	}

	//custom in ModuleController
	public function check_lock() {
		return true;
	}

	//liet ke cac field da check
	public function arr_list_changed($arr_save = array()) {
		$arr_ret = array();
		foreach ($arr_save as $k1 => $arr1) {
			if (preg_match("/_date$/", $k1) && is_object($arr1)){
				$arr_ret[$k1] = date("d M, Y",$arr1->sec);

			}else if (preg_match("/_id$/", $k1) && is_object($arr1)){
				$arr1 = (array)$arr1;
				if(isset($arr1['$id']))
				$arr_ret[$k1] = $arr1['$id'];

			}else if (is_array($arr1) || is_object($arr1)) {
				foreach ($arr1 as $k2 => $arr2) {
					if (is_array($arr2) || is_object($arr2)) {
						foreach ($arr2 as $k3 => $arr3) {
							if (is_array($arr3) || is_object($arr3)) {
								foreach ($arr3 as $k4 => $arr4) {
									$arr_ret[$k1 . '.' . $k3 . '.' . $k4] = $arr4;
								}
							}
							else
								$arr_ret[$k1 . '.' . $k3] = $arr3;
						}
					}
					else
						$arr_ret[$k2] = $arr2;
				}
			}
			else
				$arr_ret[$k1] = $arr1;
		}
		return $arr_ret;
	}

	// Delete 1 record
	public function delete($ids = 0) {
		if(!$this->check_permission($this->name.'_@_entry_@_delete'))
			$this->error_auth();
		$ids = $this->get_id();
		if ($ids != '') {
			$this->delete_all_associate($ids);
			$str_return = $this->opm->update($ids, 'deleted', true);
			$actions = $this->Session->read($this->name . 'ViewThemes');
			$this->Session->delete($this->name . 'ViewId');


			$this->redirect('/' . $this->params->params['controller'] . '/lists');
			//$this->redirect('/' . $this->params->params['controller'] . '/' . $actions);
		} else {
			$this->Session->delete($this->name . 'ViewId');
			$this->redirect('/' . $this->params->params['controller'] . '/lists');
		}
		die;
	}

	public function delete_all_associate($ids){
		//
	}

	// Check url or session to get this id
	public function get_id() {
		if (isset($this->params->params['pass'][0]) && strlen($this->params->params['pass'][0]) == 24)
			$iditem = $this->params->params['pass'][0];
		else if ($this->Session->check($this->name . 'ViewId'))
			$iditem = $this->Session->read($this->name . 'ViewId');
		else {
			//find last id
			if ($this->opm)
				$arr_tmp = $this->opm->select_one(array(), array(), array('_id' => -1));
			else {
				$module = $this->modelName;
				$this->selectModel($module);
				$arr_tmp = $this->$module->select_one(array(), array(), array('_id' => -1));
			}
			if (isset($arr_tmp['_id']) && is_object($arr_tmp['_id'])) {
				$iditem = (array) $arr_tmp['_id'];
				$iditem = $iditem['$id'];
			} else if (isset($arr_tmp['_id']) && strlen($arr_tmp['_id']) == 24) {
				$iditem = $arr_tmp['_id'];
			}
			else
				$iditem = '';
		}
		return $iditem;
	}

	//session search where chage to array
	public function arr_search_where() {
		$where_query = $where = array();
		if ($this->Session->check($this->name . '_where'))
			$where = $this->Session->read($this->name . '_where');

		//ghép điều kiện truyền vào từ hàm popup
		if ($this->is_popup && $this->Session->check($this->name . '_where_popup'))
			$where = $this->Session->read($this->name . '_where_popup');
		//set operater
		if (count($where) > 0) {
			foreach ($where as $keys => $arr) {
				if (isset($arr['operator']) && $arr['operator'] == 'LIKE')
					$where_query[$keys] = new MongoRegex("/" . $arr['values'] . "/i");
				else if (isset($arr['operator']) && $arr['operator'] == 'in')
					$where_query[$keys]['$in'] = $arr['values'];
				else if (isset($arr['operator']) && $arr['operator'] == 'nin')
					$where_query[$keys]['$nin'] = $arr['values'];
				else if (isset($arr['operator']) && $arr['operator'] == 'or')
					$where_query['$or']= $arr['values'];

				else if (isset($arr['operator']) && $arr['operator'] == '>')
					$where_query[$keys] = array('$gt' => $arr['values']);

				else if (isset($arr['operator']) && $arr['operator'] == '<')
					$where_query[$keys] = array('$lt' => $arr['values']);

				else if (isset($arr['operator']) && $arr['operator'] == '>=')
					$where_query[$keys] = array('$gte' => $arr['values']);

				else if (isset($arr['operator']) && $arr['operator'] == '<=')
					$where_query[$keys] = array('$lte' => $arr['values']);
				else if(isset($arr['operator']) && $arr['operator'] == '!=')
					$where_query[$keys] = array('$ne' => $arr['values']);
				else if (isset($arr['operator']) && $arr['operator'] == '=int')
					$where_query[$keys] = (int) $arr['values'];
				else if (isset($arr['operator']) && $arr['operator'] == 'exists@!=')
					$where_query[$keys] = array('$exists'=>true,'$ne'=>array($arr['values']));
				else if ($keys == 'code')
					$where_query[$keys] = (int) $arr['values'];
				else if(isset($arr['>='])||isset($arr['<=']))
				{
					if (isset($arr['>=']['operator']) && $arr['>=']['operator'] == 'day>=')
						$where_query[$keys]['$gte'] = $arr['>=']['values'];
					if (isset($arr['<=']['operator']) && $arr['<=']['operator'] == 'day<=')
						$where_query[$keys]['$lte'] = $arr['<=']['values'];

				}
				else if(isset($arr['operator']) && $arr['operator'] == 'elemMatch')
					$where_query[$keys]['$elemMatch'] =  $arr['values'];
				else
					$where_query[$keys] = isset($arr['values'])?$arr['values']:'';
			}
		}
		if(isset($_SESSION[$this->name.'_where_permission'])){
			if(is_array($where_query))
				return array_merge($where_query,$_SESSION[$this->name.'_where_permission']);
			return $_SESSION[$this->name.'_where_permission'];
		}
		return $where_query;
	}

	public function get_pages() {
		$ids = $this->get_id();
		$where_query = $this->arr_search_where();
		//set operater
		if (isset($where_query) && count($where_query) > 0) {
			$query = $this->opm->select_all(array(
				'arr_field' => array('_id'),
				'arr_where' => $where_query,
				'arr_order' => array('_id' => 1)//,
			));
			$n = 0;
			foreach ($query as $keys => $values) {
				$n++;
				if ($this->get_id() == $keys)
					return $n;
			}
			if ($n == 0)
				return '0';
			else
				return '1';
		}

		$sum = 0;
		if ($ids != '') {
			$arr_where = array('_id' => array('$lte' => new MongoId($ids)));
			$sum = $this->opm->count($arr_where);
		}
		if ($ids == '')
			return $this->opm->count();
		else if ($sum != -1)
			return $sum;
		else
			return 0;
	}

	// link to first entry
	public function first() {
		$where_query = $this->arr_search_where();
		$arr_tmp = $this->opm->select_one($where_query, array(), array('_id' => 1));
		if (isset($arr_tmp['_id']))
			$direct = $arr_tmp['_id'];
		else
			$direct = '';
		$this->redirect('/' . $this->params->params['controller'] . '/entry/' . $direct);
		die;
	}

	// link to preview entry
	public function prevs() {
		if ($this->get_id() != '') {
			$where_query = $this->arr_search_where();
			$where_query['_id'] = array('$lt' => new MongoId($this->get_id()));
			$arr_prev = $this->opm->select_one($where_query, array('_id'), array('_id' => -1));
		}
		if (isset($arr_prev['_id']))
			$prevs = $arr_prev['_id'];
		else
			$prevs = '';
		$this->redirect('/' . $this->params->params['controller'] . '/entry/' . $prevs);
		die;
	}

	// link to next entry
	public function nexts() {
		if ($this->get_id() != '') {
			$where_query = $this->arr_search_where();
			$where_query['_id'] = array('$gt' => new MongoId($this->get_id()));
			$arr_nexts = $this->opm->select_one($where_query, array('_id'), array('_id' => 1));
		}
		if (isset($arr_nexts['_id']))
			$nexts = $arr_nexts['_id'];
		else
			$nexts = '';
		$this->redirect('/' . $this->params->params['controller'] . '/entry/' . $nexts);
		die;
	}

	// link to lasts entry
	public function lasts() {
		$where_query = $this->arr_search_where();
		$arr_tmp = $this->opm->select_one($where_query, array(), array('_id' => -1));
		if (isset($arr_tmp['_id']))
			$direct = $arr_tmp['_id'];
		else
			$direct = '';
		$this->redirect('/' . $this->params->params['controller'] . '/entry/' . $direct);
		die;
	}

	// link to lasts entry
	public function get_last_id() {
		$arr_tmp = $this->opm->select_one(array(), array(), array('_id' => -1));
		if (isset($arr_tmp['_id']))
			return $arr_tmp['_id'];
		else
			return '';
		die;
	}

	// Action Options
	public function options($module_id = '', $module_name = '') {
		//Kiểm tra quyền view option
		if(!$this->check_permission($this->name.'_@_options_@_',true))
			$this->error_auth();
		//$this->set('set_sumline',25); //set in custom of ModuleController
		$ids = $this->get_id();
		if ($module_name == '' && isset($this->opm->arr_settings))
			$module_name = $this->opm->arr_settings['module_name'];

		$this->selectModel('Permission');
		$permission = $this->Permission;
		$controller = '';
		if(isset($this->params->params['controller']))
			$controller = $this->params->params['controller'];
		$query = $permission->select_one(array('controller' => $controller));
		$arr_opt = $query;

		if (isset($arr_opt['option_list'])){
			//Tung, dùng để filter option list dựa trên permission, 6/1/2013
			/*
				-Lấy tất cả permission của user login (1)
				-Tạo 1 chuỗi controller_@_options_@_ (2)
				-So sánh chuỗi (2) với (1) để lấy ra 1 mảng chứa codekey của option mà user có quyền (3)
					+Về !== false tham khảo http://www.php.net/manual/en/function.strpos.php  phần Warning và example#1
						This function may return Boolean FALSE, but may also return a non-Boolean value which evaluates to FALSE. Please read the section on Booleans for more information. Use the === operator for testing the return value of this function.
					+Bắt buộc phải dùng (2) nên nếu có nó sẽ trả về 0.
						// Note our use of ===.  Simply == would not work as expected
						// because the position of 'a' was the 0th (first) character.
				-Duyệt mảng option_list của permission, nếu code_key của mảng option tồn tại trong (3), đẩy vào mảng $arr_option
				-Cuối cùng set vào view
				-Rườm ra như vậy, mục đích để option hiển thị chính xác những codekey tồn tại trong permission, và view cũng hiển thị hàng cột chính xác như chỉ có duy nhất những codekey trong quyền tồn tại.
			*/
			$data = $this->get_all_permission();
			if(!isset($data['all'])){

				$arr_option = array();
				$search = strtolower($this->name).'_@_options_@_';
				$option = array();
				foreach($data as $key=>$value){
					if(strpos($key, $search)!==false){
						$key = explode("_@_", $key);
						$option[] = $key[2];
					}
				}
				foreach($arr_opt['option_list'] as $key_options=>$options){
					foreach($options as $key=>$value){
						foreach($value as $k=>$v){
							if(isset($v['deleted'])&&$v['deleted']) continue;
							if(in_array($v['codekey'], $option) || $this->system_admin){
								if(isset($v['permission']) ){
									$is_array = false;
									if(strpos($v['permission'],'||')!==false){
										$is_array = true;
										$type="or";
										$delimiter = '||';
									} else if (strpos($v['permission'],'&&')!==false){
										$is_array = true;
										$type="and";
										$delimiter = '&&';
									}
									if($is_array){
										$permission = explode($delimiter, $v['permission']);
										if($this->check_permission_array($permission,$arr_permission,$type)){
											$arr_option['option_list'][$key_options][$key][$k] = $v;
										}
									} else if( $this->check_permission($v['permission']) )
										$arr_option['option_list'][$key_options][$key][$k] = $v;
								}
								else if(!isset($v['permission']))
									$arr_option['option_list'][$key_options][$key][$k] = $v;
							}
						}
					}
				}
				//End filter
			} else {
				$arr_option = $arr_opt;
			}
			//pr($arr_option['option_list']);die;
			$this->set('option_list', $arr_option['option_list']);
		}
		if (1 == 1) {
			$this->render('../Elements/options');
		}
		$this->set('module_id', $module_id);
	}

	// Action Options
	public function swith_options($keys) {
		if(!$this->check_permission($this->name.'_@_options_@_'.$keys))
			die;
	}

	// Action cancel
	public function cancel() {
		$this->redirect('/' . $this->params->params['controller'] . '/entry');
		die;
	}

	// Action Options
	public function continues() {
		$this->redirect('/' . $this->params->params['controller'] . '/entry');
		die;
	}

	// Action find_all
	public function find_all() {
		$this->Session->write($this->name . '_where', array());
		$this->redirect('/' . $this->params->params['controller'] . '/lists');
		die;
	}

	// Action omit
	public function omit() {
		$this->redirect('/' . $this->params->params['controller'] . '/lists');
		die;
	}

	// Action sorts
	public function sorts() {
		$this->redirect('/' . $this->params->params['controller'] . '/lists');
		die;
	}

	// Action prints
	public function prints() {
		$this->redirect('/' . $this->params->params['controller'] . '/lists');
		die;
	}

	// Entry search
	public function entry_search() {
		if(!$this->check_permission($this->name.'_@_entry_@_view'))
			$this->error_auth();
		$arr_set = $this->opm->arr_settings;
		$this->set('search_class', 'jt_input_search');
		$this->set('search_class2', 'jt_select_search');
		$this->set('search_flat', 'placeholder="1"');
		$where = array();

		if ($this->Session->check($this->name . '_where'))
			$where = $this->Session->read($this->name . '_where');
		if (count($where) > 0) {
			foreach ($arr_set['field'] as $ks => $vls) {
				foreach ($vls as $field => $values) {
					if (isset($where[$field])) {
						$arr_set['field'][$ks][$field]['default'] = '';//$where[$field]['values'];//tạm thời không đưa các giá trị đã tìm kiếm ra giao diện tìm kiếm
					}
				}
			}
		}
		$this->set('arr_settings', $arr_set);
		//$this->sub_tab();
	}

	//Popup action
	public function popup($keys='') {
		if($keys!='')
			$this->set('ppkey',$keys);
		$this->is_popup = true;
		$this->lists();
		$this->layout = 'ajax';
		// if (isset($this->params->params['pass'][0]))
		//     $this->set('keys', $this->params->params['pass'][0]);
		// else
		//     $this->set('keys', '');
		// BaoNam
		$this->set('keys', $keys);
		$this->set('key', $keys);
		if (!$this->element_exit('popup')) // dung theme chung
			$this->render('../Elements/popup');
	}

	public function printpdf() {
		if (isset($this->params->params['pass'][0])) {
			$this->layout = 'ajax';
			$this->set('file_name', $this->params->params['pass'][0]);
		}
		else
			$this->redirect('/' . $this->params->params['controller'] . '/lists');
	}

	//kiem tra element trong Module
	public function element_exit($sub_tab) {
		if (isset($this->name) && is_file(APP . 'View' . DS . 'Themed' . DS . ucfirst($this->theme) . DS . $this->name . DS . $sub_tab . '.ctp')){
			return true;
		}else{
			return false;
		}
	}

	// Subtab
	function sub_tab($sub_tab = '', $item_id = null) {
		// Kiểm tra $_GET để khi bấm vào thẻ a href thì sẽ vào trực tiếp tab liên quan luôn mà không cần click chọn tab nữa
		if( isset($_GET['sub_tab']) ){
			$sub_tab = $_GET['sub_tab'];
		}
		if ($sub_tab == '') {
			if (!$this->Session->check($this->name . '_sub_tab')) {
				$sub_tab = $this->sub_tab_default;
				$this->Session->write($this->name . '_sub_tab', $sub_tab);
			} else {
				$sub_tab = $this->Session->read($this->name . '_sub_tab');
			}
		} else {
			$this->Session->write($this->name . '_sub_tab', $sub_tab);
		}

		$this->set('sub_tab', $sub_tab);
		if ($item_id)
			$this->$sub_tab($item_id);
		if ($this->request->is('ajax')) {
			if ($this->element_exit($sub_tab))
				$this->render($sub_tab);
			else
				$this->render('../Elements/box_type/subtab_box_default');
		}
	}

	// general options of selects in arr_setting
	function general_select($arr_field = array()) {
		if (isset($arr_field) && is_array($arr_field)) {
			$this->selectModel('Setting');
			foreach ($arr_field as $keys => $arr_value) {
				foreach ($arr_value as $k => $arr_v) {
					if (isset($arr_v['droplist']) && isset($arr_v['type']) && $arr_v['type'] == 'select')
						$datas[$k] = $this->Setting->select_option_vl(array('setting_value' => $arr_v['droplist']));
				}
			}
			if (isset($datas))
				return $datas;
		}
	}

	function get_option_status_color( $option_name ){

		$this->selectModel('Setting');
		$arr_option = $this->Setting->select_one(array('setting_value' => $option_name), array('option'));
		$arr_status = $arr_status_color = array();
		foreach ($arr_option['option'] as $key => $value) {
			if ($value['deleted'])
				continue;
			$arr_status[$value['value']] = $value['name'];
			$arr_status_color[$value['value']] = $value['color'];
			$arr_status_move[$value['value']] = $value['cal_enabled_move'];

		}
		return array( $arr_status, $arr_status_color, $arr_status_move );
	}

	function entry_init($id, $num_position, $model, $controller) {

		$this->selectModel($model);

		$cond = array();
		if( $this->Session->check($controller.'_entry_search_cond') ){
			$cond = $this->Session->read($controller.'_entry_search_cond');
		}

		// TH vào khi click entry bình thường
		if ($id != '0') {
			if ($id == 'first') { // called from menu_entry.ctp
				$num_position = 1;
				$arr_tmp = $this->$model->select_one($cond, array(), array('_id' => 1));
			} elseif ($id == 'last') { // called from menu_entry.ctp
				$arr_tmp = $this->$model->select_one($cond, array(), array('_id' => -1));
			} else {
				$arr_tmp = $this->$model->select_one(array('_id' => new MongoId($id)));
				if (!isset($arr_tmp['_id'])) {
					echo 'This record is deleted or not exist, please come back or click to go to <a href="/">Nail - Home page</a>';die;
				}
			}
			$this->Session->write($model . '_entry_id', (string) $arr_tmp['_id']);
		} else {

			if ($this->Session->check($model . '_entry_id')) {
				$arr_tmp = $this->$model->select_one(array('_id' => new MongoId($this->Session->read($model . '_entry_id'))));
				if (!isset($arr_tmp['_id'])) {
					$this->Session->delete($model . '_entry_id');
					$this->redirect('/' . $controller . '/entry');
					die;
				}
				$id = (string) $arr_tmp['_id'];
			}

			if (!isset($arr_tmp['_id'])) {
				$arr_tmp = $this->$model->select_one($cond, array(), array('_id' => -1));
			}
		}

		if (!isset($arr_tmp['_id'])) {
			$this->redirect('/' . $controller . '/add');
			die;
		}
		$this->Session->write($this->name . 'ViewId',(string) $arr_tmp['_id']);

		// -------------------- kiểm tra phân quyền -------------------------
		if( !$this->check_permission($controller.'_@_entry_@_view') ){
			$this->error_auth();
		}

		// this valriable array use in menu_entry.ctp
		$arr_prev = $this->$model->select_one(array_merge($cond, array('_id' => array('$lt' => $arr_tmp['_id']))), array('_id'), array('_id' => -1));
		$this->set('arr_prev', $arr_prev);
		$arr_next = $this->$model->select_one(array_merge($cond, array('_id' => array('$gt' => $arr_tmp['_id']))), array('_id'), array('_id' => 1));
		$this->set('arr_next', $arr_next);
		$sum = $this->$model->count($cond);
		$this->set('sum', $sum);
		if ($id == '0') {
			$num_position = $sum;
		} else {
			$num_position = $sum - $this->$model->count(array_merge($cond, array('_id' => array('$gt' => $arr_tmp['_id']))));
		}
		$this->set('num_position', $num_position); // đếm entry thứ mấy trong tất cả entry
		return $arr_tmp;
	}

	// show footer info in entry footer
	public function show_footer_info($arr_tmp = array(), $arr_contact_id = array()) {

		// BaoNam đổi 04/09/2013 09:25
		// $arr_contact_id = array();
		$arr_contact_id[] = $arr_tmp['created_by'];
		$arr_contact_id[] = $arr_tmp['modified_by'];
		$arr_contact_id = array_filter($arr_contact_id);
		$this->selectModel('Contact');
		$arr_contact = $this->Contact->select_list(array(
			'arr_where' => array(
				'_id' => array('$in' => $arr_contact_id)
			),
			'arr_field' => array('_id', 'first_name', 'last_name'),
		));
		$this->set('arr_contact', $arr_contact);

		// show info user in the footer
		$arr_info_footer = array();
		if (isset($arr_tmp['_id'])) {
			$arr_info_footer['date_created'] = date('d M, Y', $arr_tmp['_id']->getTimestamp());
			$arr_info_footer['date_created_hour'] = date('H:i', $arr_tmp['_id']->getTimestamp());
		}
		if (isset($arr_tmp['date_modified'])) {
			$arr_info_footer['date_modified'] = date('d M, Y', $arr_tmp['date_modified']->sec);
			$arr_info_footer['date_modified_hour'] = date('H:i', $arr_tmp['date_modified']->sec);
		}
		if (isset($arr_contact[(string) $arr_tmp['created_by']]))
			$arr_info_footer['created_by'] = $arr_contact[(string) $arr_tmp['created_by']];
		else
			$arr_info_footer['created_by'] = '';
		if (isset($arr_contact[(string) $arr_tmp['modified_by']]))
			$arr_info_footer['modified_by'] = $arr_contact[(string) $arr_tmp['modified_by']];
		else
			$arr_info_footer['modified_by'] = '';

		if (isset($arr_tmp['_id']))
			$this->set('arr_info_footer', $arr_info_footer);
	}




	//========== OPTION ===========
	public function save_default(array $arr_input = array()) {
		if (isset($_POST['arr'])) {
			$arr_input = (array) json_decode(stripslashes($_POST['arr']));
			$arr_input2 = (array) json_decode(stripslashes($_POST['arr']));
			reset($arr_input['value_object']);
			$first_key = key($arr_input['value_object']);
			if ($this->reset_default($arr_input['opname'], $first_key, 0))
				;
			$this->save_option($arr_input);
		}
		die;
	}

	public function reset_default($opname = '', $defaultkey = '', $vls = 0) {
		$ids = $this->get_id();
		if ($ids != '') {
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$data_insert = $options = array();
			if (isset($arr_tmp[$opname])) {
				$options = (array) $arr_tmp[$opname];
				foreach ($options as $keys => $values) {
					$options[$keys][$defaultkey] = $vls;
				}
			}
			$data_insert[$opname] = $options;
			$data_insert['_id'] = $ids;
			if ($this->opm->save($data_insert))
				return true;
			else
				return false;
		}
		else
			echo false;
		die;
	}

	// input array(1=opname,2=value_str,3=opid,0=keys)
	public function save_option(array $arr_input = array()) {
		$ids = $this->get_id();
		if(isset($_POST['mongo_id']) && $_POST['mongo_id']!='')
			$ids = $_POST['mongo_id'];
		if(isset($_POST['fieldchage']))
			$fieldchage = $_POST['fieldchage'];
		else
			$fieldchage = '';
		if ($ids != '') {
			if (isset($_POST['arr']))
				$arr_input = (array)json_decode($_POST['arr']);
				// $arr_input = (array) json_decode(stripslashes($_POST['arr'])); // BaoNam: stripslashes sẽ gây lỗi nếu có dấu hai nháy "

			//nhận giá trị record
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$options = array();

			if (isset($arr_tmp[$arr_input['opname']]))
				$options = $arr_tmp[$arr_input['opname']];

			//tạo giá trị mới
			if ($arr_input['opid'] == '' || $arr_input['keys'] == 'add') {
				$newopt = array();
				//nhận giá trị default
				$newopt = $this->get_default_rel($arr_input['opname']);

				//nhận giá trị input
				if (isset($arr_input['value_object'])) {
					$temps = array();
					$temps = (array) $arr_input['value_object'];
					foreach ($temps as $keys => $values) {

						if (preg_match("/_id$/", $keys) && strlen($values) == 24)
							$newopt[$keys] = new MongoId($values); //neu la id
						else if (preg_match("/_date$/", $keys)){
							 //neu la ngay
								$newopt[$keys] = new MongoDate($values);
						}
						else if (preg_match("/_price$/", $keys))
							$newopt[$keys] = (float) $values; //neu la gia
						else
							$newopt[$keys] = $values;
					}
				}
				$newopt = (array) $newopt;
				$options[] = $newopt;

				$data_insert = $arrass = $this->arr_associated_data($arr_input['opname'],$options,count($options)-1,$fieldchage);//set các field liên quan
				unset($arrass[$arr_input['opname']]);
				$arrass = $this->set_mongo_array($arrass);//lọc object
				$data_insert['_id'] = $ids;
				if (count($options) > 0)
					if ($this->opm->save($data_insert)){
						if(count($arrass)>0)
							echo json_encode($arrass);
						else
							echo (count($options)-1);
					}
			}else {//update giá trị cũ
				if (isset($arr_input['value_object'])) {
					$temps = array();
					$temps = (array) $arr_input['value_object'];

					foreach ($temps as $keys => $values) {
						if (preg_match("/_id$/", $keys) && strlen($values) == 24)
							$options[$arr_input['opid']][$keys] = new MongoId($values); //neu la id
						else if (preg_match("/_date$/", $keys)){
							if($values != '')
								$options[$arr_input['opid']][$keys] = new MongoDate($values); //neu la ngay
							else
								$options[$arr_input['opid']][$keys] = '';
						}
						else if (is_numeric($values) && is_float((float) $values))
							$options[$arr_input['opid']][$keys] = (float) $values; //neu la gia
						else
							$options[$arr_input['opid']][$keys] = $values;
					}

				}
				$arrass = $data_insert = array();
				$data_insert = $arrass = $this->arr_associated_data($arr_input['opname'],$options,$arr_input['opid'],$fieldchage);
				unset($arrass[$arr_input['opname']]);
				$arrass = $this->set_mongo_array($arrass);//lọc object
				$data_insert['_id'] = $ids;

				if ($this->opm->save($data_insert)){
					if(count($arrass)>0)
						echo json_encode($arrass);
					else
						echo $arr_input['opid'];
				}
			}
		}
		else
			echo '';
		die;
	}




	//nhận giá trị default của field trong box
	public function get_default_rel($opname = '', $entryfield = 0) {
		$new_opt = array();
		$new_opt['deleted'] = false;
		if ($opname != '') {
			if ($entryfield != 0)
				$field_list = $this->opm->get_fields_rel($opname);
			else
				$field_list = $this->opm->arr_field_rel($opname);
			if (count($field_list) > 0)
				foreach ($field_list as $ks => $vs) { //lập vòng field của option
					if (isset($vs['indata']) && $vs['indata'] == '0') //ko lưu field indata=0
						$m = 0;
					else if (isset($vs['default']) && isset($vs['type']) && $vs['type'] == 'price')
						$new_opt[$ks] = (float) $vs['default'];
					else if (isset($vs['default']))
						$new_opt[$ks] = $vs['default'];
					else
						$new_opt[$ks] = '';
				}
		}
		return $new_opt;
	}

	//field chính có rel_name link đến 1 box trong subtab,dựa vào nhận danh sách field
	public function get_fields_rel($field_name = '') {
		if ($field_name != '') {
			$name_field = array();
			$keys = '';
			$name_field = $this->opm->arr_field_key('rel_name');
			$keys = $name_field[$field_name]; //nhận tên box
			return $this->opm->arr_field_rel($keys);
		}
		else
			return '';
	}

	//give data of fields in field_list
	public function reload_field($field_list = '', $module_id = '') {
		if (isset($_POST['field_list']))
			$field_list = $_POST['field_list'];
		if (isset($_POST['module_id']))
			$module_id = $_POST['module_id'];

		if (strlen($module_id) != 24)
			$module_id = $this->get_id();

		$field_list = explode(",", $field_list);
		$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($module_id)), $field_list);
		$new_str = '';
		foreach ($field_list as $vals) {
			if (isset($arr_tmp[$vals]))
				$new_str .= $arr_tmp[$vals] . '@';
		}

		echo $new_str;
		die;
	}

	// delete option
	public function deleteopt() {
		// neu ton tai session id
		if ($this->Session->check($this->name . 'ViewId') && isset($this->params->params['pass'][0])) {
			$idsession = $this->Session->read($this->name . 'ViewId');
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($idsession)));
			$idopt = $this->params->params['pass'][0];
			$idopt = explode('@', $idopt);
			$opname = $idopt[1];
			$idopt = $idopt[0];
			$options = array();
			if (isset($arr_tmp[$opname]) && isset($opname))
				$options = $arr_tmp[$opname];
			$n = 0;
			foreach ($options as $keys => $arr_op) {//lap vong va set deleted=true
				if ($idopt == $n) {
					$options[$keys]['deleted'] = true;
				}
				$n++;
			}

			$arr_insert[$opname] = $options;
			$arr_insert['_id'] = $idsession;
			//thong tin lien quan


			if ($this->opm->save($arr_insert)){
				$this->delete_all_associate($idopt,$opname);
				echo $this->opm->mongo_id_after_save;
			}

			$this->redirect('/' . $this->params->params['controller'] . '/entry/' . $idsession);
		}
		else
			$this->redirect('/' . $this->params->params['controller'] . '/entry');

		die;
	}

	//search list
	public function search_list($arr_where = array()) {
		$arr_fields = $this->opm->arr_field_key('type');

		//BaoNam
		//Nhận danh sách giá trị các field từ giao diện
		$post = $_POST; //pr($post);
		//Lọc ký hiệu đặc biệt
		$post = $this->Common->strip_search($post); // replace some special characters to search as LIKE in mysql
		//Hiện tại tất cả module có address bên module Sales, đều nằm trong mảng [data][Contact], có thể do lỗi copy paste
		if(isset($post['data']['Contact']))
			$data = $post['data']['Contact'];
		if(!empty($data))
		{
			$where_invoice_address = array();
			$where_shipping_address = array();
			$tmp_array = array('contact_address_1','contact_address_2','contact_address_3','contact_town_city','contact_province_state','contact_province_state_id','contact_zip_postcode','contact_country','contact_country_id');
			foreach($tmp_array as $value)
			{
				//contact_country_id lúc nào cũng tồn tại 1 giá trị, dù người dùng ko tác động, và giá trị đó hoàn toàn ko có trong db, thường là chữ a hoặc c?
				if($value=='contact_country_id'&&strlen($data[$value])<2)continue;
				if(strlen($data[$value])>0)
				{
					//đổi thành invoice
					$tmp_value = str_replace('contact_', 'invoice_', $value);
					$where_invoice_address[$tmp_value] = new MongoRegex( '/'. $data[$value].'/i');
				}
			}
			$tmp_array = array('shipping_address_1','shipping_address_2','shipping_address_3','shipping_town_city','shipping_province_state','shipping_province_state_id','shipping_zip_postcode','shipping_country','shipping_country_id');
			foreach($tmp_array as $value)
			{
				if($value=='shipping_country_id'&&strlen($data[$value])<2)continue;
				if( strlen($data[$value])>0)
				{
					$where_shipping_address[$value] = new MongoRegex('/'. $data[$value].'/i');
				}
			}
			if(!empty($where_invoice_address))
			{
				// $where_invoice_address['deleted'] = false;
				$arr_where['invoice_address'] = array('values' => $where_invoice_address, 'operator' => 'elemMatch');
			}
			if(!empty($where_shipping_address))
			{
				// $where_shipping_address['deleted'] = false;
				$arr_where['shipping_address'] = array('values' => $where_shipping_address, 'operator' => 'elemMatch');
			}

		}
		foreach ($arr_fields as $kss => $vss) {
			if (isset($post[$kss]) && trim($post[$kss]) != '') {
				$arr_where[$kss]['values'] = trim($post[$kss]);
				//nếu là loại select
				if($vss=='select'){
					$arr_where[$kss]['values'] = $post[$kss.'_id'];
					$arr_where[$kss]['operator'] = '=';

				//nếu là loại checkbox
				}else if($vss=='checkbox'){
					$arr_where[$kss]['operator'] = '=';
					if($post[$kss]=='on')
						$arr_where[$kss]['values'] = 1;
					else
						$arr_where[$kss]['values'] = 0;

				//nếu là loại id
				}else if (preg_match("/_id$/", $kss) && strlen($arr_where[$kss]['values']) == 24) {
					$arr_where[$kss]['operator'] = '=';
					$arr_where[$kss]['values'] = new MongoId($arr_where[$kss]['values']);
					if($kss=='our_rep_id' || $kss=='our_csr_id')
						$kss2 = str_replace("_id","",$kss);
					else
						$kss2 = str_replace("_id","_name",$kss);
					if(isset($post[$kss2]))
						$arr_where[$kss2]['values'] = trim($post[$kss2]);
				//nếu là loại ngày
				}else if (preg_match("/_date$/", $kss)) {
					$begin = $this->Common->strtotime($arr_where[$kss]['values'] . '00:00:00');
					$end = $this->Common->strtotime($arr_where[$kss]['values'] . '23:59:59');
					$arr_where[$kss]['>=']['operator'] = 'day>=';
					$arr_where[$kss]['>=']['values'] = new MongoDate($begin);
					$arr_where[$kss]['<=']['operator'] = 'day<=';
					$arr_where[$kss]['<=']['values'] = new MongoDate($end);

				//các loại khác
				}else
					$arr_where[$kss]['operator'] = 'LIKE';


				//nếu giá trị là loại số
				if (is_numeric($arr_where[$kss]['values'])){
					$arr_where[$kss]['operator'] = '=';
					if ($vss=='price')
						$arr_where[$kss]['values'] = (float)$arr_where[$kss]['values'];
					else
						$arr_where[$kss]['values'] = (int)$arr_where[$kss]['values'];
				}

			}
		}

		if (is_array($arr_where) && count($arr_where) > 0) {
			$this->Session->write($this->name . '_where', $arr_where);
		}
		//check

		$where_query = $this->arr_search_where();
		$amount = $this->opm->count($where_query);

		//echo $amount;
		if ($amount > 1) {
			$arr_query = $this->opm->select_one($where_query, array('_id'), array('_id' => -1));
			if (isset($arr_query['_id'])) {
				$arr_query['_id'] = (array) $arr_query['_id'];
				$this->Session->write($this->name . 'ViewId', $arr_query['_id']['$id']);
			}
			echo 'lists';
		} else if ($amount == 1) {
			$arr_query = $this->opm->select_one($where_query, array('_id'));
			if (isset($arr_query['_id']))
				echo 'entry/' . $arr_query['_id'];
			else
				echo 'entry';
		}else {
			$this->Session->write($this->name . '_where', array());
			echo '0';
		}
		die;
	}

	// update các giá trị array được set là default
	public function update_default($opname = '', $default = 'default', $fields = '', $values = '') {
		$ids = $this->get_id();
		if (isset($_POST['opname']))
			$opname = $_POST['opname'];
		if (isset($_POST['defaultfield']))
			$default = $_POST['defaultfield'];
		if (isset($_POST['fields']))
			$fields = $_POST['fields'];
		if (isset($_POST['values']))
			$values = $_POST['values'];

		if ($opname != '' && $fields != '' && $ids != '') {
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($ids)), array($opname));
			$flat = 0;
			if (isset($arr_tmp[$opname])) {
				$arr_insert = $arr_tmp[$opname];
				foreach ($arr_tmp[$opname] as $kss => $vss) {
					$arr_insert[$kss] = $vss;
					if (!($vss['deleted']) && (int) $vss[$default] == 1) {
						$arr_insert[$kss][$fields] = $values;
						$flat = 1;
					}
				}
			}
			if ($flat == 0) {
				$arr_new['deleted'] = false;
				$arr_new[$default] = 1;
				$arr_new[$fields] = $values;
				$arr_insert[] = $arr_new;
			}

			$data_insert[$opname] = $arr_insert;
			$data_insert['_id'] = $ids;
			if ($this->opm->save($data_insert))
				echo 'Saved';
		}
		die;
	}

	public function province($country_id = '') {
		$this->selectModel('Province');
		$where = array();
		if ($country_id != '')
			$where['country_id'] = $country_id;
			$query = $this->Province->select_all(
				array(
					'arr_where' => $where,
					'arr_field' => array('key', 'name')
				)
		);

		$datas = array();
		foreach ($query as $kss => $arr) {
			$datas[$arr['key']] = $arr['name'];
		}
		asort($datas);
		return $datas;
	}


	public function province_reverse($country_id = '') {
		$this->selectModel('Province');
		$where = array();
		if ($country_id != '')
			$where['country_id'] = $country_id;
			$query = $this->Province->select_all(
				array(
					'arr_where' => $where,
					'arr_field' => array('key', 'name')
				)
		);

		$datas = array();
		foreach ($query as $kss => $arr) {
			$datas[$arr['name']] = $arr['key'];
		}
		asort($datas);
		return $datas;
	}


	public function country() { // BaoNam 08/11/2013
		$this->selectModel('Country');
		$arr_temp = $this->Country->select_all(array('arr_order' => array('name' => 1)));
		$arr_country = array();
		foreach ($arr_temp as $key => $value) {
			$arr_country[$value['value']] = $value['name'];
		}
		// $this->selectModel('Setting');
		// $arr_temp = $this->Setting->select_option(
		//         array('setting_value' => $dropname)
		// );
		return $arr_country;
	}

	public function ajax_general_province($country_id = "CA") {
		if (isset($_POST['country_id']))
			$country_id = $_POST['country_id'];
		echo json_encode($this->province($country_id));
		die;
	}

	public function get_data_other_model($module_name, $field_setup, $ids) {
		$form_field = $to_field = $arr_ret = $arr_tmp = array();
		foreach ($field_setup as $kss => $vss) {
			$form_field[] = $kss;
		}
		$this->selectModel($module_name);
		$modules = $this->$module_name;
		$arr_tmp = $modules->select_one(array('_id' => new MongoId($ids)), $form_field);
		foreach ($field_setup as $kk => $vv) {
			if (isset($arr_tmp[$kk]))
				$arr_ret[$vv] = $arr_tmp[$kk];
			else
				$arr_ret[$vv] = '';
		}
		return $arr_ret;
	}

	// ajax nhận data từ 1 record của module bất kỳ
	public function get_data_form_module($module_name = 'Company', $ids = '', $arr = array()) {
		if (isset($_POST['module_name']))
			$module_name = $_POST['module_name'];
		if (isset($_POST['ids']))
			$ids = $_POST['ids'];
		if (isset($_POST['arr']))
			$arr = json_decode($_POST['arr']);
		$arr = (array)$arr;
		$form_field = array();
		$x=0;
		foreach ($arr as $keys => $values) {
			$form_field[$x] = $keys;
			$x++;
		}
		$this->selectModel($module_name);
		$modules = $this->$module_name;
		$arr_tmp = $modules->select_one(array('_id' => new MongoId($ids)), $form_field);
		$arr_return = array();
		if (isset($arr_tmp)) {
			foreach ($arr_tmp as $kk => $vv) {
				$arr_return[$kk] = $vv;
				if (preg_match("/_id$/", $kk) && is_object($vv)) {
					$arr_return[$kk] = (array) $arr_return[$kk];
					$arr_return[$kk] = $arr_return[$kk]['$id'];
				}
			}
		}
		echo json_encode($arr_return);
		die;
	}

	public function save_muti_field($ids = '', $arr_field = array(), $arr_data = array()) {
		$arr_data = $arr_insert = $arr_return = array();
		if (isset($_POST['ids']) && $_POST['ids'] != '')
			$ids = $_POST['ids'];
		else
			$ids = $this->get_id();
		if (isset($_POST['jsondata']))
			$arr_data = json_decode($_POST['jsondata']);
		if (isset($_POST['jsonfield']))
			$arr_field = json_decode($_POST['jsonfield']);
		$arr_field = (array) $arr_field;
		$arr_data = (array) $arr_data;
		foreach ($arr_field as $keys => $values) {
			if (isset($arr_data[$keys])) {
				$arr_return[$values] = $arr_insert[$values] = $arr_data[$keys];
				//set id
				if (preg_match("/_id$/", $values) && strlen($arr_data[$keys]) == 24)
					$arr_insert[$values] = new MongoId($arr_data[$keys]);
			}
		}
		if (strlen($ids) == 24) {
			$arr_insert['_id'] = new MongoId($ids);
			$arr_return['_id'] = $ids;
		} else {
			$temps = $this->opm->add('default', false);
			$temps = explode("||", $temps);
			$arr_insert['_id'] = $temps[0];
			$this->Session->write($this->name . 'ViewId', $arr_insert['_id']);
			$arr_return['_id'] = $temps[0];
		}

		if ($this->opm->save($arr_insert))
			echo json_encode($arr_return);
		else
			echo 'error';
		die;
	}




	// ajax nhận data từ 1 record của module bất kỳ và lưu vào module đang thao tác
	public function save_data_form_to($module_from = 'Company', $ids = '', $arr = array()) {
		if (isset($_POST['module_from']))
			$module_from = $_POST['module_from'];
		if (isset($_POST['ids']))
			$ids = $_POST['ids'];
		if (isset($_POST['arr']))
			$arr = json_decode(str_replace('\"', '"', $_POST['arr']));
		$form_field = array();
		$arr = (array) $arr;
		foreach ($arr as $keys => $values) {
			$form_field[] = $keys;
		}
		//get data
		$this->selectModel($module_from);
		$modules = $this->$module_from;
		$arr_tmp = $modules->select_one(array('_id' => new MongoId($ids)), $form_field);

		//send data
		$arr_return = $arr_insert = array();
		foreach ($arr_tmp as $kss => $vals) {
			if (isset($arr[$kss])) {
				$arr_return[$arr[$kss]] = $arr_insert[$arr[$kss]] = $vals;
				if (preg_match("/_id$/", $arr[$kss])) {
					if (is_object($arr_return[$arr[$kss]])) {
						$arr_return[$arr[$kss]] = (array) $arr_return[$arr[$kss]];
						$arr_return[$arr[$kss]] = $arr_return[$arr[$kss]]['$id'];
					}
					if (is_string($arr_return[$arr[$kss]]) && isset($arr_return[$arr[$kss]]) && strlen($arr_return[$arr[$kss]]) == 24)
						$arr_insert[$arr[$kss]] = new MongoId($arr_return[$arr[$kss]]);
					else
						$arr_insert[$arr[$kss]] = '';
				}
			}
		}

		//save data
		$this_ids = $this->get_id();
		if ($this_ids != '') {
			$arr_insert['_id'] = new MongoId($this_ids);
			if ($this->opm->save($arr_insert)) {
				echo json_encode($arr_return);
			}
		}
		else
			echo 'empty id';
		die;
	}

	public function reload_address($address_key = '') {
		if (isset($_POST['address_key']))
			$address_key = $_POST['address_key'];
		$ids = $this->get_id();
		$arr_tmp = $this->opm->select_one(
			array('_id' => new MongoId($ids)),
			array($address_key . 'address')
		);

		$arr_temp = array();
		if (isset($arr_tmp[$address_key . 'address'][0]))
			foreach ($arr_tmp[$address_key . 'address'][0] as $kk => $vv) {
				$arr_temp[$kk] = $vv;
				if ($kk == $address_key . 'province_state') {
					$arr_province = $this->province();
					if (isset($arr_province[$vv]))
						$arr_temp[$kk] = $arr_province[$vv];
				}else if ($kk == $address_key . 'country') {
					$arr_country = $this->country();
					if (isset($arr_country[$vv]))
						$arr_temp[$kk] = $arr_country[$vv];
				}
			}

		echo json_encode($arr_temp);
		die;
	}

	//general html cc,bcc
	public function reload_cc($company_id = '') {
		if (isset($_POST['company_id']))
			$company_id = $_POST['company_id'];
		$cc = array();
		if (isset($company_id) && strlen($company_id) == 24)
			$cc = $this->get_email_of_company($company_id);
		if (isset($_POST['company_id'])) {
			echo json_encode($cc);
			die;
		}
		else
			return $cc;
	}

	public function get_email_of_company($company_id) {
		if (isset($company_id)) {
			//set id
			if (is_object($company_id))
				$idmongo = $company_id;
			else if (strlen($company_id) == 24)
				$idmongo = new MongoId($company_id);

			//find email list
			if (isset($idmongo)) {
				$this->selectModel('Contact');
				$query = $this->Contact->select_all(array(
					'arr_where' => array('company_id' => $idmongo),
					'arr_order' => array('_id' => 1),
					'arr_field' => array('email', 'first_name', 'last_name')
				));
				$arr_contact = $query;
				$arr = array();
				$m = 0;
				foreach ($arr_contact as $kss => $vss) {
					$x = $m;
					if (isset($vss['email'])) {
						$m = $vss['email'];
						$arr[$m] = $vss['email'];
					}
					if (isset($vss['first_name']))
						$arr[$m] .= ' ' . $vss['first_name'];
					if (isset($vss['last_name']))
						$arr[$m] .= ' ' . $vss['last_name'];
					$m = $x + 1;
				}
				return $arr;
			}
		}
		return array();
	}

	// display Log or history
	public function history($ids = '') {
		/*echo 'Under Contruct';
		die;*/
		$arr_data = array();
		if ($ids == '')
			$ids = $this->get_id();
		if ($ids != '') {
			$where_query = array();
			$where_query['item_id'] = new MongoId($ids);
			$where_query['module'] = $this->ModuleName();
			$this->selectModel('Log');
			$this->selectModel('Contact');
			$query = $this->Log->select_all(array(
				'arr_order' => array('_id' => -1),
				'arr_where' => $where_query
			));
			$arr = $query;
			foreach ($arr as $k => $v) {
				if (isset($v['change_to']) && count($v['change_to']) > 0) {
					$created_by = $this->get_name('Contact', $v['created_by']);
					$created_date = date("d M,Y H:i:s", $v['date_modified']->sec);
					foreach($v['change_to'] as $keys=>$values){
						if(!is_array($values))
						$arr_data[] = array(
										'fieldname' => $keys,
										'created_by' => $created_by,
										'created_date' => $created_date,
										'change_from' => (isset($v['change_from'][$keys]) ? $v['change_from'][$keys] : array()),
										'change_to' => $values,
									 );
						else{
							foreach($values as $morekey=>$morevalue){
								if(!is_array($morevalue)){
									if(is_array($v['change_from'][$keys][$morekey]))
										$change_from = '';
									else
										$change_from = (isset($v['change_from'][$keys][$morekey][$kks]) ? $v['change_from'][$keys][$morekey][$kks] : '');
									if($morevalue==$change_from)
										continue;
									$arr_data[] = array(
										'fieldname' => $morekey,
										'created_by' => $created_by,
										'created_date' => $created_date,
										'change_from' => $change_from,
										'change_to' => $morevalue,
									 );


								}else
									foreach($morevalue as $kks=>$vvs){
										//echo $keys.'.'.$morekey.'.'.$kks.'<br />';
										if(is_array($vvs)){
											$vvs = (count($vvs)>0)?'ARRAY':'';
											$change_from = $vvs;
										}elseif(is_object($vvs)){
											$vvs = (string)$vvs;
											if(!isset($v['change_from'][$keys][$morekey][$kks])||!is_array($v['change_from'][$keys][$morekey][$kks]))
												$change_from = (isset($v['change_from'][$keys][$morekey][$kks]) ? (string)$v['change_from'][$keys][$morekey][$kks] : '');
											else
												$change_from = '';
										}else{
											if(!isset($v['change_from'][$keys][$morekey][$kks])||!is_array($v['change_from'][$keys][$morekey][$kks]))
												$change_from = (isset($v['change_from'][$keys][$morekey][$kks]) ? $v['change_from'][$keys][$morekey][$kks] : '');
											else
												$change_from = '';
										}
										if($vvs==$change_from)
											continue;
										$arr_data[] = array(
												'fieldname' => $keys.'.'.$morekey.'.'.$kks,
												'created_by' => $created_by,
												'created_date' => $created_date,
												'change_from' => $change_from,
												'change_to' => $vvs,
											 );
									}
							}
						}
					}

				}
			}
		}
		//pr($this->Log->arr_settings);
		//pr($arr_data);die;
		$subdatas = array();
		$subdatas['history_detail'] = $arr_data;
		$lock_settings = $this->Log->arr_settings;
		$this->set('subdatas',$subdatas);
		$this->set('name',$this->name);
		$this->set('iditem',$this->get_id());
		$this->set('lock_settings',$lock_settings);
		$this->render('../Elements/history');
		//return $arr_data;
	}

	//cal sum again after delete
	public function cal_sum_by_request($opname = '', $amount_field = '') {
		if (isset($_POST['opname']))
			$opname = $_POST['opname'];
		if (isset($_POST['amount_field']))
			$amount_field = $_POST['amount_field'];
		$sum = 0;
		if ($this->get_id() != '') {
			$query = $this->opm->select_one(
					array('_id' => new MongoId($this->get_id())), array($opname)
			);
			$arr_tmp = $query;
			if (isset($arr_tmp[$opname]) && is_array($arr_tmp[$opname]) && count($arr_tmp[$opname]) > 0) {
				foreach ($arr_tmp[$opname] as $kk => $vv) {
					if (isset($vv[$amount_field]) && !$vv['deleted'])
						$sum += (float) $vv[$amount_field];
				}
			}
		}
		echo $sum;
		die;
	}


	//scan setting and get data for type Select
	public function set_select_data_list($boxtype='panel',$subtab=''){
		$listdata = array();
		$arr_set = $this->opm->arr_settings;
		$this->selectModel('Setting');
		if($boxtype=='panel' && $subtab!=''){
			return $listdata;
		}else if($boxtype=='relationship' && $subtab!=''){
			foreach($arr_set['relationship'][$subtab]['block'] as $bkey => $arr_box){
				if(isset($arr_box['field'])){
					foreach($arr_box['field'] as $fk=>$fv){
						if(isset($fv['type']) && $fv['type']=='select'){
							$listdata[$fk] = $this->Setting->select_option_vl(array('setting_value'=>$fv['droplist']));
						}
					}
				}
			}
			$this->set('option_select', $listdata);
			return $listdata;
		}else return false;
	}


	//update sub doc cua module
	public function update_all_option($opname='',$arr_value=array(),$nosave=false){
		if (isset($_POST['opname']))
			$opname = $_POST['opname'];
		if (isset($_POST['arr_value'])){
		   $arr_value = json_decode($_POST['arr_value']);
		   $arr_value = (array)$arr_value;
		}

		$new_arr = array();

		if ($this->get_id() != '') {
			$query = $this->opm->select_one(
					array('_id' => new MongoId($this->get_id())), array($opname)
			);
			$arr_tmp = $query;
			$new_arr = $arr_tmp[$opname];

			if($opname=='products')
				$this->set_cal_price();

			if(isset($new_arr) && is_array($new_arr) && count($new_arr) > 0) {
				foreach ($new_arr as $kk => $vv) {
					if(!$vv['deleted'] && $opname=='products'){
						if(isset($arr_value['taxper']) && $arr_value['taxper']!='')
							$vv['taxper'] = (float)$arr_value['taxper'];
						else
							$vv['taxper'] = 0;
						$vv['tax'] = round(((float)(isset($vv['taxper']) ? $vv['taxper'] : 0)/100)*(float)$vv['sub_total'],3);
						$vv['amount'] = round((float)$vv['sub_total']+(float)$vv['tax'],2);
						$new_arr[$kk] = $vv;

					}else if(!$vv['deleted']){
						foreach($arr_value as $keys=>$vls){
							$new_arr[$kk][$keys] = $vls;
						}

					}
				}
			}
			$arr_insert[$opname] = $new_arr;
			$arr_insert['_id'] = new MongoId($this->get_id());
			if($nosave)
				return $new_arr;
			if ($this->opm->save($arr_insert)) {
				//update sum
				$keyfield = array(
						"sub_total"		=> "sub_total",
						"tax"			=> "tax",
						"amount"		=> "amount",
						"sum_sub_total"	=> "sum_sub_total",
						"sum_tax"		=> "sum_tax",
						"sum_amount"	=> "sum_amount"
					);
				$arr_sum = $this->update_sum('products',$keyfield);

				if (isset($_POST['opname']))
					echo json_encode($new_arr);
				else
					return true;
			}
		}
		if (isset($_POST['opname']))
			die;
		else
			return false;
	}




	/**
	* Thay đổi tax entry khi province thay đổi
	*/
	public function change_tax_entry($not_js=false){
		if ($this->get_id() != '') {
			$this->selectModel('Tax');
			$arr_tax = $this->Tax->tax_select_list();
			$query = $this->opm->select_one(
					array('_id' => new MongoId($this->get_id())), array('invoice_address','shipping_address','tax')
			);

			$shipping = $invoice = array(); $key_tax = '';
			if(isset($query['shipping_address'][0]))
				$shipping = $query['shipping_address'][0];
			if(isset($query['invoice_address'][0]))
				$invoice = $query['invoice_address'][0];
			if(isset($shipping['shipping_province_state_id']) && $shipping['shipping_province_state_id']!='')
				$key_tax = $shipping['shipping_province_state_id'];
			else if(isset($invoice['invoice_province_state_id']) && $invoice['invoice_province_state_id']!='')
				$key_tax = $invoice['invoice_province_state_id'];
			if(isset($arr_tax[$key_tax])){
				$prov_key = explode("%",$arr_tax[$key_tax]);
				$arr_insert['products'] = $this->update_all_option('products',array('taxper'=>$prov_key[0]),true);
				$arr_insert['tax'] = $key_tax;
				$arr_insert['_id'] = new MongoId($this->get_id());
				if($this->opm->save($arr_insert)){
					$html_ret['keytax'] = $key_tax;
					$html_ret['texttax'] = $arr_tax[$key_tax];

					if(!$not_js)
						echo json_encode($html_ret);
					else
						return true;
				}
			}
		}
		die;
	}


	//Lấy thông tin các company của hệ thống (anvy)
	public function company_system(){
		$this->selectModel('Company');
		$arr_tmp = $this->Company->select_one(array('system' => true));
		if(count($arr_tmp)>0){
			$arr_tmp = iterator_to_array($arr_tmp);
			return $this->set_mongo_array($arr_tmp);
		}else
			return array();
	}


	/*
	* Tính total các line entry
	* Use: Dùng cho chế độ hàm tính và ajax. Trước hết định nghĩa lại các tên field từ ajax.
	* Ex: Xem cách truyền ajax từ file line_entry.ctp của Quotation
	* Construct: $keyfield = array(
		"sub_total"		: "sub_total",
		"tax"			: "tax",
		"amount"		: "amount",
		"sum_sub_total"	: "sum_sub_total",
		"sum_tax"		: "sum_tax",
		"sum_amount"	: "sum_amount"
	)
	*/
	public function update_sum($subdoc='',$keyfield=array()){
		$arr_return = array();
		//get data
		if(isset($_POST['keyfield'])){
			$keyfield = $_POST['keyfield'];
			$keyfield = json_decode($keyfield);
			$keyfield = (array)$keyfield;
		}
		if(isset($_POST['subdoc']))
			$subdoc = $_POST['subdoc'];

		$sub_total		=	$keyfield['sub_total'];
		$tax			=	$keyfield['tax'];
		$amount			=	$keyfield['amount'];
		$key_sub_total	=	$keyfield['sum_sub_total'];
		$key_tax		=	$keyfield['sum_tax'];
		$key_amount		=	$keyfield['sum_amount'];
		$count_sub_total = $count_tax = $count_amount = 0;
		//process
		$ids = $this->get_id();
		if($ids!=''){
			$qr = $this->opm->select_one(array('_id'=> new MongoId($ids)));
			$query = $qr;
			if(isset($query[$subdoc]) && is_array($query[$subdoc]) && count($query[$subdoc])>0){
				$prolist = (array)$query[$subdoc];
				foreach($prolist as $opid=>$arr_item){
					if(isset($arr_item['deleted'])&&!$arr_item['deleted']){
						if(isset($arr_item['option_for'])&&is_array($prolist[$arr_item['option_for']])
							&& (isset($prolist[$arr_item['option_for']]['sell_by'])&&$prolist[$arr_item['option_for']]['sell_by']=='combination'
								|| (isset($arr_item['same_parent'])&&$arr_item['same_parent']>0)
								)
							)
							continue;
						//cộng dồn sub_total
						if(isset($arr_item[$sub_total]))
							$count_sub_total += (float)$arr_item[$sub_total];
						//cộng dồn amount
						if(isset($arr_item[$sub_total]))
							$count_amount += (float)$arr_item[$amount];
					}
				}
				//tính lại sum tax
				$count_tax = $count_amount - $count_sub_total;

				//save sum
				$arr_insert[$key_amount] 	= $count_amount;
				$arr_insert[$key_sub_total] = $count_sub_total;
				$arr_insert[$key_tax] 		= $count_tax;
				$arr_insert['_id'] = new MongoId($ids);
				if($this->opm->save($arr_insert)){
					$arr_insert['_id'] = $ids;
					$arr_return = $arr_insert;
				}

			}
		}

		//output
		if(isset($_POST['keyfield'])){
			echo json_encode($arr_return);
			die;
		}else
			return $arr_return;
	}




	/**
	* Lấy data sub doccumment của 1 item
	* Use:
	* Ex: ví dụ tìm sub doccumment <product> của quotation
	*/
	public function get_option_data($opname='',$module=''){
		$ids = $this->get_id();
		if($ids!='' && $opname!=''){
			if($module!=''){
				$this->selectModel($module);
				$qr = $this->$module->select_one(array('_id'=> new MongoId($ids)));
			}else
				$qr = $this->opm->select_one(array('_id'=> new MongoId($ids)));
			if(isset($qr[$opname]) && is_array($qr[$opname]) && count($qr[$opname])>0)
				return $qr[$opname];
		}
		return array();
	}



	/**
	* Lưu data vào module khi đứng ở module khác
	* Use:
	* Ex: ví dụ tìm sub doccumment <product> của quotation
	*/
	public function save_to_other_module($arr=array(),$keylink=''){
		$arr_insert = array();
		if(isset($_POST['arr']))
			$arr = json_decode($_POST['arr']);
		$arr_insert = $this->set_mongo_array($arr);
		if($keylink!='')
			$arr_insert[$keylink] = $this->get_id();
		$this->opm->save($arr_insert);
		if(isset($_POST['arr']))
			echo json_encode($this->arr_list_changed($arr_insert));
		else
			return $arr_insert;
		die;
	}




	function documents($module_id) {
		$this->selectModel('DocUse');
		$arr_docuse = $this->DocUse->select_all(array(
			'arr_where' => array(
				'module' => $this->modelName,
				'module_id' => new MongoId($module_id)
			)
		));
		$arr_tmp_id = array();
		foreach ($arr_docuse as $value) {
			$arr_tmp_id[] = $value['doc_id'];
		}

		$arr_doc = array();
		if (!empty($arr_tmp_id)) {
			$this->selectModel('Doc');
			$arr_doc = $this->Doc->select_all(array(
				'arr_where' => array(
					'_id' => array('$in' => $arr_tmp_id)
				)
			));
		}
		$this->set('arr_doc', $arr_doc);
		$this->set('module_id', $module_id);
		$this->set(strtolower($this->modelName).'_id', $module_id);

		// lấy ra category
		$this->selectModel('Setting');
		$this->set('arr_docs_category', $this->Setting->select_option(array('setting_value' => 'docs_category'), array('option')));

		if ($this->request->is('ajax')) {
			echo $this->render('../Elements/documents');die;
		}
	}

	function documents_save($module_id, $doc_id, $module_no = '', $module_detail = '') {
		if(!$this->check_permission($this->name.'_@_documents_tab_@_add')
		   &&!$this->check_permission('docs_@_entry_@_add')){
			echo 'You do not have this permission on this action.';
			die;
		}
		$this->selectModel('DocUse');
		$arr_docuse = $this->DocUse->select_one(array('module' => $this->modelName, 'doc_id' => new MongoId($doc_id), 'module_id' => new MongoId($module_id)));
		if (!isset($arr_docuse['_id'])) {
			$arr_save = array();
			$arr_save['doc_id'] = new MongoId($doc_id);
			// $arr_save['module_controller'] = 'companies';
			$arr_save['module_id'] = new MongoId($module_id);
			$arr_save['controller'] = $this->params->params['controller'];
			$arr_save['module'] = $this->modelName;
			$arr_save['module_no'] = $module_no;
			$arr_save['module_detail'] = $module_detail;
			if ($this->DocUse->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->DocUse->arr_errors_save[1];
			}
		} else {
			echo 'Error: this document is selected before';
		}

		die;
	}

	function documents_delete($module_id, $doc_id, $type= '') {
		if(!$this->check_permission($this->name.'_@_documents_tab_@_delete')
		   &&!$this->check_permission('docs_@_entry_@_delete')){
			echo 'You do not have this permission on this action.';
			die;
		}
		$this->selectModel('DocUse');
		$arr_docuse = $this->DocUse->select_one(array('module' => new MongoRegex('/'.$this->modelName.'/i'), 'doc_id' => new MongoId($doc_id), 'module_id' => new MongoId($module_id)));
		if (isset($arr_docuse['_id'])) {
			$arr_save = array();
			$arr_save['_id'] = $arr_docuse['_id'];
			$arr_save['deleted'] = true;
			if ($this->DocUse->save($arr_save)) {
				if($type=='image'){
					$this->selectModel('Doc');
					$arr_doc =$this->Doc->select_one(array('_id'=>new MongoId($doc_id)));
					if(isset($arr_doc['path'])){
						unlink(ROOT.DS.APP_DIR.DS.WEBROOT_DIR.$arr_doc['path']);
						echo 'ok';
					} else
						echo 'Error: not exist record';
					die;
				} else
					echo 'ok';
			} else {
				echo 'Error: ' . $this->DocUse->arr_errors_save[1];
			}
		} else {
			echo 'Error: not exist record';
		}
		die;
	}

	// BaoNam:
	function communications($module_id) {

		$arr_communication=array();
		$this->selectModel('Communication');
		$arr_communication = $this->Communication->select_all(array(
			'arr_where' => array(
			   'module' => $this->modelName,
			   'module_id' => new MongoId($module_id)
			)
		));
		$this->set('arr_communication',$arr_communication);

		if( $this->modelName == "" ){ echo "Vui long ghi bien modelName vao moi controller. Vi du: var \$modelName = 'Company';"; die; }
		$this->set('model_name', $this->modelName);
		$this->selectModel('Setting');
		$this->set('com_type', $this->Setting->select_option_vl(array('setting_value'=>'com_type')));
		$this->set('module_id', $module_id);
		if ($this->request->is('ajax') && $this->params->params['pass'][0] == 'communications') {
			echo $this->render('../Elements/communications');die;
		}
	}

	// BaoNam:
	public function add_from_module($module_id='', $comms_type='', $option = array()){
		if( $comms_type == 'Message' ){
			$this->redirect('/communications/entry_message/'.$_GET['contact_id'].'?module=' . $this->modelName . '&module_id=' . $module_id);
		}else{

			$arr_save['comms_type'] = $comms_type;
			$arr_save['module'] = $this->modelName;
			$arr_save['module_id'] = new MongoId($module_id);
			if( $arr_save['module'] == 'Company' ){
				$this->selectModel('Company');
				$tmp = $this->Company->select_one(array('_id' => $arr_save['module_id']), array('_id', 'name', 'contact_default_id'));
				$arr_save['company_name'] = $tmp['name'];
				$arr_save['company_id'] = $tmp['_id'];

				if (isset($tmp['contact_default_id']) && is_object($tmp['contact_default_id'])) {
					$this->selectModel('Contact');
					$contact_default = $this->Contact->select_one(array('_id' => $tmp['contact_default_id']), array('_id', 'first_name', 'last_name'));
					$arr_save['contact_name'] = $contact_default['first_name'].' '.$contact_default['last_name'];
					$arr_save['contact_id'] = $contact_default['_id'];
				}
			}
			$not_redirect = false;
			if(isset($option['not_redirect'])){
				unset($option['not_redirect']);
				$not_redirect = true;
			}
			$this->selectModel('Communication');
			$arr_save = array_merge($arr_save, $option);
			$this->Communication->arr_default_before_save = $arr_save;
			if( $this->Communication->add() ){
				if(!$not_redirect){
					$this->redirect('/communications/entry/'.$this->Communication->mongo_id_after_save);
					die;
				} else
					return $this->Communication->mongo_id_after_save;
			}else{
				echo 'Error: ' . $this->Communication->arr_errors_save[1];die;
			}
		}

	}

	function noteactivity($module_id) {
		$this->selectModel('Noteactivity');
		$arr_noteactivity = $this->Noteactivity->select_all(array(
			'arr_where' => array(
				'module' => $this->modelName,
				'module_id' => new MongoId($module_id)
			),
			'arr_order' => array('_id' => 1)
		));
		$this->set('arr_noteactivity', $arr_noteactivity);
		$this->set('module_id', $module_id);
		$this->set(strtolower($this->modelName).'_id', $module_id);

		$this->selectModel('Contact');
		$this->set('model_contact', $this->Contact);

		if ($this->request->is('ajax')) {
			if( $this->params->params['action'] == 'noteactivity_add' ){
				echo $this->render('../Elements/noteactivity');die;
			}else{
				$this->set('noteactivity', $this->render('../Elements/noteactivity'));
			}
		}
	}

	function noteactivity_add($module_id) {
		$arr_save = array();
		$arr_save['type'] = 'Note';
		$arr_save['content'] = '';
		$arr_save['module'] = $this->modelName;
		$arr_save['module_id'] = new MongoId($module_id);

		$this->selectModel('Noteactivity');
		if ($this->Noteactivity->save($arr_save)) {
			$this->noteactivity($module_id);
		} else {
			echo 'Error: ' . $this->Noteactivity->arr_errors_save[1];
		}
		die;
	}

	function noteactivity_update($id) {
		$arr_save = array();
		$arr_save['_id'] = $id;
		$arr_save['content'] = $_POST['content'];
		$this->selectModel('Noteactivity');
		if ($this->Noteactivity->save($arr_save)) {
			echo 'ok';
		} else {
			echo 'Error: ' . $this->Noteactivity->arr_errors_save[1];
		}
		die;
	}

	function noteactivity_delete($id) {
		$this->selectModel('Noteactivity');
		$arr_save = array();
		$arr_save['_id'] = $id;
		$arr_save['deleted'] = true;
		if ($this->Noteactivity->save($arr_save)) {
			echo 'ok';
		} else {
			echo 'Error: ' . $this->Noteactivity->arr_errors_save[1];
		}
		die;
	}



	//change sellprice: nhận sell price moi va bang chiec khau
	function change_sell_price_company($idcompany='',$product_id=''){
		$result = array();
		//Dò thông tin trong bảng Company
		$sell_category = $this->company_pricing($idcompany,$product_id);

		//Nếu trong Company có Price Break thì lấy thông tin
		if(isset($sell_category['price_break']) && count($sell_category['price_break'])>0)
			$result['company_price_break'] = $sell_category['price_break'];

		//Lấy thông tin Discount cho Company
		if(isset($sell_category['discount']) && $sell_category['discount']!='')
			$result['discount'] = $sell_category['discount'];


		//neu company co sell_category_key
		if(!isset($sell_category['sell_category_key']))
			$sell_category['sell_category_key'] = '';

		$sell_break = $this->product_price_break($sell_category['sell_category_key'],$product_id);
		//lay gia moi
		if(isset($sell_break['sell_price']) && $sell_break['sell_price']!='')
			$result['sell_price'] = $sell_break['sell_price'];

		//gia phu them cua option
		if(isset($sell_break['sell_price_plus']) && $sell_break['sell_price_plus']!='')
			$result['sell_price_plus'] = $sell_break['sell_price_plus'];

		//lay bang chiec khau product
		if(isset($sell_break['price_break']) && count($sell_break['price_break'])>0)
			$result['product_price_break'] = $sell_break['price_break'];

		//pr($result);die;
		return $result;

		/**Kết quả cần lấy :
		*  1.Bảng chiếc khấu trong Company 							=> $result['company_price_break']
		*  2.Chiếc khấu áp dụng cho Company 						=> $result['discount']
		*  3.Giá bán cho company dựa theo key category, 			=> $result['sell_price']
		*  4.Bảng chiếc khấu trong product dựa theo key category, 	=> $result['product_price_break']
		*/
	}



	//lay data pricing_category tu Product dua vao Company
	function company_pricing($idcompany='',$product_id=''){
		$price_break = $result = array();

		if($idcompany!=''){
			if(!is_object($idcompany))
				$idcompany = new MongoId($idcompany);

			$this->selectModel('Company');

			$arr_company = $this->Company->select_one(
				array('_id'=>$idcompany),
				array('sell_category','sell_category_id','pricing','discount')
			);

			//neu co bang  pricing
			if(isset($arr_company['pricing']) && is_array($arr_company['pricing']) && count($arr_company['pricing'])>0 && $product_id!=''){
				if(is_object($product_id))
					$product_id = (string)$product_id;

				//lap va tim $price_break cho 1 san pham dang can tim
				foreach($arr_company['pricing'] as $keys => $values){
					if(isset($values['deleted']) && !($values['deleted']) && isset($values['product_id']) && (string)$values['product_id'] == $product_id ){
						if(isset($values['price_break']) && is_array($values['price_break']) && count($values['price_break'])>0 ){
							foreach($values['price_break'] as $kk=>$vv){
								if(isset($vv['deleted']) && !$vv['deleted'])
									$price_break[$kk] = $vv;
							}
						}
					}
				}

				$result['price_break'] = $price_break;
			}


			if(isset($arr_company['sell_category_id']))
				$result['sell_category_key'] = $arr_company['sell_category_id'];
			if(isset($arr_company['discount']))
				$result['discount'] = (float)$arr_company['discount'];

		}

		return $result; //ket qua tra ve la array price_break va sell_category_key

	}




	//lay data price_break tu Product dua vao Company hoac default pricing_category
	function product_price_break($sell_category_key='',$product_id=''){
		$result = array();
		$sell_price_default = '';
		$sell_category_key_df = '';

		if($product_id!=''){
			if(!is_object($product_id) && strlen($product_id)!=24)
				return '';
			else if(!is_object($product_id))
				$product_id = new MongoId($product_id);


			$this->selectModel('Product');
			$arr_product = $this->Product->select_one(
				array('_id'=>$product_id),
				array('pricebreaks','sellprices')
			);
			//tim sell_category id
			if(isset($arr_product['sellprices']) && is_array($arr_product['sellprices']) && count($arr_product['sellprices'])>0){
				$result['sell_price'] = '';
				foreach($arr_product['sellprices'] as $keys=>$values){
					if(isset($values['deleted']) && !$values['deleted'] && isset($values['sell_category'])){
						if($sell_category_key!='' && $values['sell_category'] == $sell_category_key)
							$result['sell_price'] = $values['sell_unit_price'];

						if(isset($values['sell_default']) && (int)$values['sell_default']==1){
							$sell_price_default = $values['sell_unit_price'];
							$sell_category_key_df = $values['sell_category'];
						}
					}
				}

				if($result['sell_price'] == '' && $sell_price_default!='' ){
					$result['sell_price'] = $sell_price_default;
					$sell_category_key = $sell_category_key_df;
				}

			}else if(isset($arr_product['sell_price'])){
				$result['sell_price'] = $arr_product['sell_price'];
			}

			//Cong them option
			if(isset($result['sell_price'])){
				$this->selectModel('Product');
				$result['sell_price_plus'] = $this->Product->sell_price_plus_option((string)$product_id,$result['sell_price']) - $result['sell_price'];
			}

			//tim Price breaks
			if(isset($arr_product['pricebreaks']) && is_array($arr_product['pricebreaks']) && count($arr_product['pricebreaks'])>0){

				foreach($arr_product['pricebreaks'] as $keys=>$values){
					if(isset($values['deleted']) && !$values['deleted'] && isset($values['sell_category']) && $values['sell_category'] == $sell_category_key){
						$result['price_break'][$keys] = $values;
					}
				}
			}


		}

		return $result;
	}



	//nhan gia tri tax dua vao key cua province
	function get_tax($tax_key=''){
		if($tax_key!=''){
			$this->selectModel('Tax');
			$tax_list = $this->Tax->tax_list();
			if(isset($tax_list[$tax_key]))
				return $tax_list[$tax_key];
		}
		return 0;
	}



	function get_setting_option($type){
		$this->selectModel('Setting');
		return $this->Setting->select_option(array('setting_value' => $type), array('option'));
	}
	//gửi email dựa vào template định sẵn
	function get_email_template($email){
		if(!isset($email['template_name']) || $email['template_name'] =='')
			$email['template_name'] = 'default';
		$this->selectModel("Emailtemplate");
		if(isset($email['template_id']))
			$arr_where = array('_id'=>new MongoId($email['template_id']));
		else
			$arr_where = array('name'=>new MongoRegex('/'.$email['template_name'].'/i'));
		$email_template = $this->Emailtemplate->select_one($arr_where);
		$template = $email_template['template'];
		//Lay tat ca span
		preg_match_all("!<span[^>]+>(.*?)</span>!", $template, $matches);
		$all_span = $matches[0];
		foreach($all_span as $span){
			//Lay noi dung trong rel
			preg_match_all("/<span [^>]+ rel=\"{{(.+?)}}\" [^>]+>[^>]+<\/span>/",$span,$content_matches);
			foreach($content_matches[1] as $val){
				$val = strtolower($val);
				if(strpos($val, '_date')!==false && isset($email[$val]))
				   $email[$val] = date('M d, Y',$email[$val]->sec);
				else if(strpos($val, '_address')!==false){
					$name = str_replace('_address', '', $val);
					$tmp_address = $email[$val][0];
					$email[$val] = '';
					if (isset($tmp_address[$name . '_address_1']) && $tmp_address[$name . '_address_1'] != '')
					$email[$val] .= $tmp_address[$name . '_address_1'] . ' ';
					if (isset($tmp_address[$name . '_address_2']) && $tmp_address[$name . '_address_2'] != '')
						$email[$val] .= $tmp_address[$name . '_address_2'] . ' ';
					if (isset($tmp_address[$name . '_address_3']) && $tmp_address[$name . '_address_3'] != '')
						$email[$val] .= $tmp_address[$name . '_address_3'] . '<br />';
					else
						$email[$val] .= '<br />';
					if (isset($tmp_address[$name . '_town_city']) && $tmp_address[$name . '_town_city'] != '')
						$email[$val] .= $tmp_address[$name . '_town_city'];
					if (isset($tmp_address[$name . '_province_state']))
						$email[$val] .= ' ' . $tmp_address[$name . '_province_state'] . ' ';
					else if (isset($tmp_address[$name . '_province_state_id']) && isset($tmp_address[$name . '_country_id'])) {
						$keytmp_address = $tmp_address[$name . '_province_state_id'];
						$provkey = $this->province($tmp_address[$name . '_country_id']);
						if (isset($provkey[$tmp_address]))
							$email[$val] .= ' ' . $provkey[$tmp_address] . ' ';
					}
					if (isset($tmp_address[$name . '_zip_postcode']) && $tmp_address[$name . '_zip_postcode'] != '')
						$email[$val] .= $tmp_address[$name . '_zip_postcode'];
					if (isset($tmp_address[$name . '_country']) && isset($tmp_address[$name . '_country_id']) && (int) $tmp_address[$name . '_country_id'] != "CA")
						$email[$val] .= ' ' . $tmp_address[$name . '_country'] . '<br />';
					else
						$email[$val] .= '<br />';

				}
				$template = str_replace($span, (isset($email[$val]) ? $email[$val] : ''), $template);
			}
		}
		return $template;
	}
	//Gửi email tự động
	function auto_send_email($data)
	{
		if(!empty($data)){
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail();
			$this->selectModel('Stuffs');
			$system_email = $this->Stuffs->select_one(array('value'=>"system_email"));
			if(!empty($system_email)){
				$config_set = array(
					'from'      => array($system_email['username']=>(trim($system_email['email_name'])!='' ? $system_email['email_name'] : 'Anvy Digital - Nail')) ,
					'username'  => $system_email['username'],
					'password'  => $system_email['password'],
					'host'		=> $system_email['host'],
					'port'		=> $system_email['port'],
				);
				$email->config('smtp',$config_set);
			}
			else{
				$email->config('gmail',array('nail.mail@gmail.com'=>$this->opm->user_name()));
				$system_email['username'] = 'nail.mail@gmail.com';
			}
			$email->to($data['to']);
			if(isset($data['cc'])&&!empty($data['cc']))
				$email->cc($data['cc']);
			if(isset($data['bcc'])&&!empty($data['bcc']))
				$email->bcc($data['bcc']);
			$email->subject($data['subject']);
			//Kiem tra attachment, va dua vao mail se gui
			if(!empty($data['attachments']))
				$email->attachments($data['attachments']);
			$email->emailFormat('both');
			try{
				$email->send($data['template']);
				//Save vao comms
				$option = array(
								'not_redirect'	=>	true,
								'name'			=>	$data['subject'],
								'content'		=>	$data['template'],
								'comms_status'	=> 	'Sent',
								'contact_name'	=>	$data['contact_name'],
								'contact_id'	=>	$data['contact_id'],
								'email'			=>	$data['to'],
								'contact_from'	=> 	$this->opm->user_name(),
								'identity'		=> 	'Auto Send',
								'sign_off'		=> 	'',
								);
				$this->add_from_module($this->get_id(),'Email',$option);
				$comms_id = $this->Communication->mongo_id_after_save;
				$this->Session->setFlash('<span>An email has just been sent to '.$data['to'].' by '.$system_email['username'].'.</span><a id="notifyTopsub" style="right: 35px;position: absolute;" href="'.URL.'/communications/entry/'.$comms_id.'">View</a>','default',array('class'=>'flash_message'));
			}
			catch(Exception $e){
				echo $e;
				die;
			}
		}
	}

	function auto_action($status){
		$id = $this->get_id();
		$controller_name = $this->name;
		$model_name = $this->modelName;
		//Lấy thông tin record dựa vào id
		$data = $this->opm->select_one(array('_id'=>new MongoId($id)));
		$this->selectModel('AutoProcess');
		//Lấy id của template
		$process= $this->AutoProcess->select_all(array('arr_where'=>array('controller'=> new MongoRegex('/'.$controller_name.'/i'))));
		if($process->count()){
			$content = '';//'<p>The <a href="'.URL.'/'.strtolower($controller_name).'/entry/'.$data['_id'].'">'.$model_name.' code: '.$data['code'].'</a> has been changed status to '.$status.'. Please check.</p>';
			$subject = 'Nail - '.$model_name.' code : '.$data['code'];
			$this->selectModel('Contact');
			foreach($process as $value){
				switch ($value['name']) {
					case 'Email CSR, Rep (Completed)':
						//GỬI MAIL CHO OUR REP VÀ OUR CSR KHI STATUS VỀ COMPLETE
						if($status!='Completed') continue 2;
						if(!is_object($data['our_rep_id']) || !is_object($data['our_csr_id'])){
							echo 'This record must have our rep and csr to send email.';
							die;
						}
						//Gửi OUR REP
						$our_rep = $this->Contact->select_one(array('_id'=>new MongoId($data['our_rep_id'])));
						if(!isset($our_rep['email']) || !filter_var($our_rep['email'], FILTER_VALIDATE_EMAIL)){
							echo 'The email of our responsible is not valid.';
							die;
						}
						$email = array(
								   'contact_name' 	=> 	$our_rep['full_name'],
								   'contact_id'		=>	new MongoId($our_rep['_id']),
								   'template_id'	=>	$value['email_template_id'],
								   'content'		=> 	$content,
								   );
						$email = array_merge($data,$email);
						if(isset($value['extra_email'])&&filter_var($value['extra_email'], FILTER_VALIDATE_EMAIL)){
							$email['cc'] = $value['extra_email'];
						}
						$email['to'] = $our_rep['email'];
						$email['template'] = $this->get_email_template($email);
						$email['subject'] = $subject;
						//Gửi mail cho our_rep
						$this->auto_send_email($email);
						//==================================================
						//Gửi OUR CSR
						$our_csr = $this->Contact->select_one(array('_id'=>new MongoId($data['our_csr_id'])));
						if(!isset($our_csr['email']) || !filter_var($our_csr['email'], FILTER_VALIDATE_EMAIL)){
							echo 'The email of our crs is not valid.';
							die;
						}
						if($data['our_csr_id']==$data['our_rep_id']) continue 2;
						$email = array(
								   'contact_name' 	=> 	$our_csr['full_name'],
								   'contact_id'		=>	new MongoId($our_csr['_id']),
								   'template_id'	=>	$value['email_template_id'],
								   'content'		=> 	$content,
								   );
						$email = array_merge($data,$email);
						$email['to'] = $our_csr['email'];
						$email['template'] = $this->get_email_template($email);
						$email['subject'] = $subject;
						//Gửi mail cho our_csr
						$this->auto_send_email($email);
						//=======================End=======================
						break;
					case 'Email Customer (Completed)':
						//GỬI MAIL CHO CUSTOMER KHI STATUS VỀ COMPLETE
						if($status!='Completed') continue 2;
						if(!is_object($data['company_id'])){
						echo 'This record must have customer to send email.';
							die;
						}
						$send_email = $this->get_name('Company',$data['company_id'],'email_so_completed');
						if($send_email!=1) continue 2;
						$customer = $this->Contact->select_one(array('_id'=>new MongoId($data['contact_id'])));
						if(!isset($customer['email']) || !filter_var($customer['email'], FILTER_VALIDATE_EMAIL)){
							echo 'The email of customer is not valid.';
							die;
						}
						$email = array(
										'contact_name' 	=> 	$customer['full_name'],
										'contact_id'	=>	new MongoId($customer['_id']),
										'template_id'	=>	$value['email_template_id'],
										'content'		=> 	$content,
									   );
						$email = array_merge($data,$email);
						if(isset($value['extra_email'])&&filter_var($value['extra_email'], FILTER_VALIDATE_EMAIL)){
							$email['cc'] = $value['extra_email'];
						}
						$email['to'] = $customer['email'];
						$email['template'] = $this->get_email_template($email);
						$email['subject'] = $subject;
						//Gửi mail cho our_rep
						$this->auto_send_email($email);
						//=======================End=======================
						break;
					case 'Email CSR (Submitted)':
						//GỬI MAIL CHO OUR CSR KHI STATUS VỀ SUBMIT
						if(strtolower($controller_name)!='quotations') continue 2;
						if($status!='Submitted') continue 2;
						if(!$this->check_limit()) continue 2;
						if(!isset($data['our_csr_id']) || !is_object($data['our_csr_id'])){
							echo 'This record must have our csr to send email.';
							die;
						}
						$our_csr = $this->Contact->select_one(array('_id'=>new MongoId($data['our_csr_id'])));
						if(!isset($our_csr['email']) || !filter_var($our_csr['email'], FILTER_VALIDATE_EMAIL)){
							echo 'The email of our crs is not valid.';
							die;
						}
						$email = array(
										'contact_name' 	=> 	$our_csr['full_name'],
										'contact_id'	=>	new MongoId($our_csr['_id']),
										'template_id'	=>	$value['email_template_id'],
										'content'		=> 	$content,
									   );
						$email = array_merge($data,$email);
						if(isset($value['extra_email'])&&filter_var($value['extra_email'], FILTER_VALIDATE_EMAIL)){
							$email['cc'] = $value['extra_email'];
						}
						$email['to'] = $our_csr['email'];
						$email['template'] = $this->get_email_template($email);
						$email['subject'] = $subject;
						//Gửi mail cho our_csr
						$this->auto_send_email($email);
						//=======================End=======================
						break;
					case 'Email Rep (Approved)':
						//GỬI MAIL CHO OUR REP KHI STATUS VỀ APPROVED
						if($status!='Approved') continue 2;
						$our_rep = $this->Contact->select_one(array('_id'=>new MongoId($data['our_rep_id'])));
						if(!isset($our_rep['email']) || !filter_var($our_rep['email'], FILTER_VALIDATE_EMAIL)){
							echo 'The email of our crs is not valid.';
							die;
						}
						$email = array(
									   'contact_name' 	=> 	$our_rep['full_name'],
									   'contact_id' 	=> 	new MongoId($our_rep['_id']),
									   'template_id'	=>	$value['email_template_id'],
									   'content'		=> 	$content,
									   );
						$email = array_merge($data,$email);
						if(isset($value['extra_email'])&&filter_var($value['extra_email'], FILTER_VALIDATE_EMAIL)){
							$email['cc'] = $value['extra_email'];
						}
						$email['to'] = $our_rep['email'];
						$email['template'] = $this->get_email_template($email);
						$email['subject'] = $subject;
						//Gửi mail cho our_rep
						$this->auto_send_email($email);
						//=======================End=======================
						break;
					default:
						continue 2;
						break;
				}
			}
		}
	}
	public function create_email_pdf($ajax=false){
		//Lay len ten file cua report vua dc tao ra
		$file = $this->view_pdf(true);
		if($file!=''){
			$model_name = $this->modelName;
			$this->selectModel($model_name);
			//Lay du lieu record cua controller hien tai, de dua vao comms
			$data = $this->$model_name->select_one(array('_id'=> new MongoId($this->get_id())));
			$arr_save = array();
			//Add Doc truoc
			$this->selectModel('Doc');
			$arr_save = array(
				'deleted' 			=>	false,
				'no'				=>	$this->Doc->get_auto_code('no'),
				'create_by_module'	=>	$model_name,
				'path'				=>	DS.'upload'.DS.$file,
				'name'				=>	$file,
				'ext'				=> 'pdf',
				'location'			=>	$model_name,
				'type'				=> 	'application/pdf',
				'description'		=>	'Created at: '.date("h:m a, M d, Y"),
				'create_by'			=>	new MongoId($this->opm->user_id()),
				);
			$this->Doc->save($arr_save);
			$doc_id = $this->Doc->mongo_id_after_save;
			//Tiep theo add Comms
			$this->selectModel('Communication');
			$arr_save = array(
				'deleted'			=> false,
				'code'				=> $this->Communication->get_auto_code('code'),
				'comms_type'		=> 'Email',
				'comms_date'		=> new MongoDate(),
				'comms_status'		=> 'Draft',
				'sign_off'			=> 0,
				'include_signature'	=> 0,
				'email_cc'			=> '',
				'email_bcc'			=> '',
				'identity'			=> '',
				'internal_notes'	=> '',
				'company_id'		=> (isset($data['company_id'])&&$data['company_id']!='' ? new MongoId($data['company_id']) : ''),
				'company_name'		=> (isset($data['company_name']) ? $data['company_name'] : ''),
				'module'			=> $model_name,
				'module_id'			=> new MongoId($data['_id']),
				'content'			=> '',
				'contact_id'		=> (isset($data['contact_id'])&&$data['contact_id']!='' ? new MongoId($data['contact_id']) : ''),
				'contact_name'		=> (isset($data['contact_name']) ?$data['contact_name'] : ''),
				'last_name'			=> '',
				'position'			=> (isset($data['position'])?$data['position']:''),
				'salutation'		=> '',
				'name'				=> '',
				'toother'			=> '',
				'email'				=> (isset($data['email']) ? $data['email'] : ''),
				'phone'				=> (isset($data['phone']) ? $data['phone'] : ''),
				'fax'				=> (isset($data['fax']) ? $data['fax'] : ''),
				'job_number'		=> (isset($data['job_number']) ? $data['job_number'] : ''),
				'job_name'			=> (isset($data['job_name']) ? $data['job_name'] : ''),
				'job_id'			=> (isset($data['job_id'])&&$data['job_id']!='' ? new MongoId($data['job_id']) : ''),
				);
			$this->Communication->save($arr_save);
			$comms_code = $arr_save['code'];
			$comms_id = $this->Communication->mongo_id_after_save;
			//Add docuse, gan lien ket giua comms va doc, muc dich de email tu dong get dc attachment
			$this->selectModel('DocUse');
			$arr_save = array(
				'deleted'		=> false,
				'controller'	=> 'communications',
				'module'		=> 'Communication',
				'module_no'		=> 	$comms_code,
				'doc_id'		=> new MongoId($doc_id),
				'module_id'		=> new MongoId($comms_id),
				'created_by'	=> new MongoId($this->opm->user_id()),
				);
			$this->DocUse->save($arr_save);
			if($ajax){
				echo URL.'/communications/entry/'.$comms_id;
				die;
			}
			$this->redirect('/communications/entry/'.$comms_id);
		}
	}
	function get_all_permission()
	{
		$arr_permission = array();
		if( CHECK_DB_PRIVILEGE ){
			$this->selectModel('Contact');
			$current_contact = $this->Contact->select_one(array('_id' => $_SESSION['arr_user']['contact_id']));
		}else
			$current_contact = $_SESSION['arr_user'];

		if( !isset($current_contact['roles']) )
			$current_contact['roles'] = array();
		if( !isset($current_contact['roles']['roles']) )
			$current_contact['roles']['roles'] = array();

		if(isset($current_contact['roles']) && !empty($current_contact['roles'])){
			$roles= $current_contact['roles']['roles'];
			unset($current_contact['roles']['roles']);
			$arr_permission = $current_contact['roles'];
			if(!empty($roles)){
				$this->selectModel('Role');
				$group_roles = $this->Role->select_all(array(
										'arr_where'	=>	array('_id'	=>	array('$in'	=> $roles)),
											  ));
				if($group_roles->count()>0){
					foreach($group_roles as $value){
						// BaoNam: kiểm tra đây có phải là System Admin không
						if($value['name'] == 'System Admin'){
							$this->set('system_admin', true);
						}
						//==================================================
						if(isset($value['value'])&&!empty($value['value'])){
							foreach($value['value'] as $key=>$val){
								$arr_permission[$key] = $val;
							}
						}
					}
				}
			}
		}
		return $arr_permission;
	}
	function get_all_inactive_permission()
	{
		$arr_inactive_permission = array();
		$this->selectModel('Permission');
		$permissions = $this->Permission->select_all(array('arr_where'=>array('inactive_permission'=>array('$exists'=>true)),'arr_field'=>array('inactive_permission')));
		if($permissions->count()==0)
			return array();
		else {
			foreach($permissions as $permission){
				foreach($permission['inactive_permission'] as $key=>$value)
					$arr_inactive_permission[$key] = $value;
			}
		}
		return $arr_inactive_permission;
	}
	function check_permission($permission,$options=false){
		$permission = strtolower($permission);
		if(!CHECK_PERMISSION || $this->system_admin)
			return true;
		if(isset($this->arr_inactive_permission[$permission]))
			return false;
		if(isset($this->arr_permission['all']))
			return true;
		if($options){
			foreach($this->arr_permission as $key=>$value)
				if(strpos($key, $permission)!==false)
					return true;
		} else {
			if(isset($this->arr_permission[$permission]))
				return true;
		}
		return false;
	}
	function check_permission_array(array $permission,$type="and",$options=false){
		$i = 0;
		if($options){
			$controller = strtolower($this->name);
			foreach($permission as $key=>$value)
				$permission[$key] = $controller.'_@_options_@_'.$value;
		} else {
			foreach($permission as $key=>$value)
				$permission[$key] = strtolower($value);
		}
		if(!CHECK_PERMISSION || $this->system_admin)
			return true;
		if($type=="and"){
			//AND:
			//Nếu 1 giá trị trong mảng permission tồn tại trong inactive_permission, return false
			foreach($permission as $value){
				if(isset($this->arr_inactive_permission[$value]))
					return false;
			}
		} else if($type=="or"){
			//OR:
			//unset nhứng giá trị trong mảng permission tồn tại trong inactive_permission
			foreach($permission as $key=>$value){
				if(isset($this->arr_inactive_permission[$value]))
					unset($permission[$key]);
			}
			//Nếu đã unset tất cả, return false
			if(empty($permission))
				return false;
		}
		if(isset($this->arr_permission['all']))
			return true;
		if($type=="and"){
			//AND sai 1 => sai het
			foreach($permission as $value){
				if(!isset($this->arr_permission[$value]))
					return false;
			}
			return true;
		}
		else if($type=="or"){
			//OR dung 1 => dung het
			foreach($permission as $value){
				if(isset($this->arr_permission[$value]))
					return true;
			}
		}
		return false;
	}



	function error_auth(){
		$this->redirect('/');
		echo 'You have no right in this item, please come back. Thank you!'; die;
	}




	function cal_production_time($arr_data=array()){
		//pr($arr_data);
		//$arr_data['sell_by'] = strtolower($arr_data['sell_by']); // BaoNam fix bug viet hoa chu Unit
		if($arr_data['sell_by']!=''){
			//tinh tong do may
			$speed_asset = 1; $production_time = 0;
			$this->selectModel('Equipment');
			if(isset($arr_data['tag_key'])){
				$speed_asset = $this->Equipment->speed_asset_old($arr_data['tag_key']);
			}
			if($speed_asset==0)
				$speed_asset = 1;
			//echo $speed_asset;

			$cal_price = new cal_price;
			$cal_price->arr_product_items['sizew'] = (float)$arr_data['sizew'];
			$cal_price->arr_product_items['sizeh'] = (float)$arr_data['sizeh'];
			if(isset($arr_data['sizew_unit']))
				$cal_price->arr_product_items['sizew_unit'] = $arr_data['sizew_unit'];
			if(isset($arr_data['sizeh_unit']))
				$cal_price->arr_product_items['sizeh_unit'] = $arr_data['sizeh_unit'];
			//neu la dien tich
			if($arr_data['sell_by']=='area'){
				$cal_price->cal_area();
				$production_time = $cal_price->arr_product_items['area']*$arr_data['quantity']*$arr_data['factor']/$speed_asset;
				//echo '<br />'.$arr_data['factor'];
			//neu la chieu dai
			}else if($arr_data['sell_by']=='lengths'){
				$cal_price->cal_perimeter();
				$production_time = $cal_price->arr_product_items['perimeter']*$arr_data['quantity']*$arr_data['factor']/$speed_asset;

			//neu la unit
			}else if($arr_data['sell_by']=='unit'){

				$production_time = (float)$arr_data['min_of_uom']*$arr_data['quantity']/60;
			}
			if($production_time<0.5&&$production_time>0)
				$production_time = 0.5;
			else
				$production_time = ceil($production_time*2)/2;
			return $production_time;

		}
	}


	function rebuild_line_text_entry(){
		$model_name = $this->modelName;
		$this->selectModel($model_name);
		$line_action = array();
		$text_action = array();
		//LINE_ENTRY
		if(!$this->check_permission(strtolower($this->name).'_@_entry_@_edit')){ // Baonam fix: strtolower
			$line_action['edit'] = 1;
			$line_action['delete'] = 1;
			$line_action['add'] = 1;
			$text_action['edit'] = 1;
			$text_action['delete'] = 1;
			$text_action['add'] = 1;
		}
		if(!empty($line_action))
			$this->$model_name->set_lock_option('line_entry','products',$line_action);
		if(!empty($text_action))
			$this->$model_name->set_lock_option('text_entry','products',$text_action);
	}

	function other_tab_auto_save(){
		if(!$this->check_permission($this->name.'_@_entry_@_edit')){
			echo 'You do not have permission on this action.';
			die;
		}
		if (!empty($_POST)) {
			parse_str($_POST['content'],$data);
			$arr_save['_id'] = new MongoId($_POST['id']);
			foreach($data as $key=>$value)
				$arr_save[$key] = $value;
			if ($this->opm->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->opm->arr_errors_save[1];
			}
		}
		die;
	}


	function theme(){
	}
	function render_pdf($arr_data = array()){
		if(!isset($arr_data['footer']) || $arr_data['footer'] == ''){
			$arr_data['footer'] = '
								<ul>
									<li>103, 3016 - 10th Ave. NE, Calgary, Alberta, Canada T2A 6A3 </li>
									<li><span style="font-weight:bold;">Tel:</span> 403.291.2244</li>
									<li><span style="font-weight:bold;">Fax:</span> 403.291.2246 </li>
									<li class="left"><span class="bold_text">Web:</span> <a href="#">www.anvydigital.com</a></li>
								</ul>';
		}
		$this->layout = 'ajax';
		$this->set('arr_data',$arr_data);
		$this->render('../Elements/render_pdf');
	}
	function print_pdf(){
		if($this->request->is('ajax')){
			$this->selectModel('Stuffs');
			$token = $this->Stuffs->select_one(array('name'=>new MongoRegex('/report_pdf_token/i')));
			$report_token = $token['value'][] = md5(time().rand(0,123654));
			$this->Stuffs->save($token);
			$report_size = (isset($_POST['report_size'])&&$_POST['report_size']!='' ? $_POST['report_size'] : 'A4');
			$report_file_name = (isset($_POST['report_file_name'])&&$_POST['report_file_name']!='' ? $_POST['report_file_name'] : md5(time().rand(0,1000)));
			$report_orientation = (isset($_POST['report_orientation'])&&$_POST['report_orientation']!='' ? $_POST['report_orientation'] : '');
			$report_url = (isset($_POST['report_url'])&&$_POST['report_url']!='' ? $_POST['report_url'].'?print_pdf='.$report_token : URL);
			$command = PHANTOMJS_PATH."phantomjs ".PHANTOMJS_PATH."kei.js '".$report_url."' ".PHANTOMJS_PATH.DS."..".DS."upload".DS.$report_file_name.".pdf '".$report_size."' '".$report_orientation."'";
			if(exec($command)=='ok'){
				echo URL.'/upload/'.$report_file_name.'.pdf';
			}
		}
		die;
	}
	public function support(){
		$this->selectModel('Support');
		$supports = $this->Support->select_all(array(
		                                       	'arr_where'=>array('module'=>$this->modelName)
		                                       ));
		$this->set('supports',$supports);
		$this->set('name',$this->name);
		$this->set('iditem',$this->get_id());
		$this->render('../Elements/support');
	}
	function autocomplete(){
		if(isset($_POST['data'])){
			header("Content-type: application/json");
			$arr_data = array();
			$_POST['data'] = (array)json_decode($_POST['data']);
			if($this->name=='Products'){
				$where['name'] = new MongoRegex('/'.$_POST['data']['name'].'/i');
				if($_POST['data']['type']!='')
					$where['product_type'] = $_POST['data']['type'];
				if($_POST['data']['category']!='')
					$where['category'] = $_POST['data']['category'];
				$products = $this->opm->select_all(array(
				                       'arr_where'=> $where,
				                       'arr_order'=>array('name'=>1),
				                       'arr_field'=>array('name'),
				                       'limit'=>10
				                       ));
				if($products->count()){
					$arr_data = array();
					foreach($products as $value)
						$arr_data[]['name'] = $value['name'];
				}
			} else if($this->name=='Contacts'){
				$where['full_name'] = new MongoRegex('/'.$_POST['data']['full_name'].'/i');
				unset($_POST['data']['full_name']);
				if(!empty($_POST['data'])){
					foreach($_POST['data'] as $key=>$value){
						$key = str_replace(array('data[Contact][',']'),'',$key);
						$where[$key] = $value;
					}
				}
				$this->selectModel('Contact');
				$contacts = $this->Contact->select_all(array(
				                       'arr_where'=> $where,
				                       'arr_order'=>array('full_name'=>1),
				                       'arr_field'=>array('full_name'),
				                       'limit'=>10
				                       ));
				if($contacts->count()){
					$arr_data = array();
					foreach($contacts as $value)
						$arr_data[]['full_name'] = $value['full_name'];
				}
			} else if($this->name=='Companies'){
				$where['name'] = new MongoRegex('/'.$_POST['data']['name'].'/i');
				unset($_POST['data']['name']);
				if(!empty($_POST['data'])){
					foreach($_POST['data'] as $key=>$value){
						$key = str_replace(array('data[Company][',']'),'',$key);
						$where[$key] = $value;
					}
				}
				$this->selectModel('Company');
				$companies = $this->Company->select_all(array(
				                       'arr_where'=> $where,
				                       'arr_order'=>array('name'=>1),
				                       'arr_field'=>array('name'),
				                       'limit'=>10
				                       ));
				if($companies->count()){
					$arr_data = array();
					foreach($companies as $value)
						$arr_data[]['name'] = $value['name'];
				}
			}
			echo "{\"data\":" .json_encode($arr_data)."}";
		}
		die;
	}
	function rebuild_code(){
		$arr_field = array('salesorder_date','invoice_date','quotation_date','code');
		switch (strtolower($this->modelName)) {
			case 'salesorder':
				$where = 'salesorder_date';
				break;
			case 'salesinvoice':
				$where = 'invoice_date';
				break;
			case 'quotation':
				$where = 'quotation_date';
				break;
			default:
				$where = 'quotation_date';
				break;
		}
        $i =0;
        for($y = 2013; $y <= 2014; $y++){
	        for($m = 1; $m <= 12; $m++){
	        	$day_start = date("1-$m-$y");
	        	$mongo_day_start = new MongoDate(strtotime("1-$m-$y"));
	        	$num_day_of_month = date('t',strtotime("1-$m-$y"));
	        	$day_end = date("$num_day_of_month-$m-$y");
	        	$mongo_day_end = new MongoDate(strtotime("$num_day_of_month-$m-$y") + DAY - 1);
	        	echo "$day_start to $day_end";
	        	$records = $this->opm->select_all(array('arr_where'=>array($where=>array('$gte'=>$mongo_day_start,'$lte'=>$mongo_day_end)),'arr_field'=>$arr_field,'arr_order'=>array($where=>1)),array('limit'=>999999));
        		pr($records->count().' record(s) found.');
        		$year = substr($y, 2);
        		$i = 1;
	        	foreach($records as $value){
	        		$arr_data = array();
	        		$arr_data['_id'] = new MongoId($value['_id']);
	        		$arr_data['code'] = "$year-".str_pad($m, 2, "0", STR_PAD_LEFT)."-".str_pad($i, 3, "0", STR_PAD_LEFT);
	        		pr($arr_data['code']);
	        		$this->opm->rebuild_collection($arr_data);
	        		$i++;
	        	}
	        	echo ($i-1).' record(s) updated.';
	        	echo '<hr />';

	        }
    	}
        die;
	}
	//CAL PRICE AT QT,SO,SI
	function cal_price_line($arr_post = array()){
        if(isset($_POST['data'])){
            $arr_post = $_POST;
            $arr_post['data'] = (array)json_decode($_POST['data']);
        }
        if(!empty($arr_post)){
            $data = $arr_post['data'];
            $fieldchange = $arr_post['fieldchange'];
            $id = $this->get_id();
        	$query = $this->opm->select_one(array('_id'=>new MongoId($id)),array('products','options','company_id','tax',));
        	$date = $query['_id']->getTimestamp();
            if(!isset($query['options']))
                $query['options'] = array();
            $this->selectModel('Setting');
            $uom_list = $this->Setting->uom_option_list();
            //=================
            $is_custom  = false;
            $is_special = false;
            $is_combination = false;
            $is_custom_unit_price = false;
            $no_same_parent = false;
            //=================
            $total_sub_total = 0;
            $this_line_data = array();
            //Set các thông tin cần thiết
            //Xét riêng line entry
            //Nếu line cha thay đổi thông số liên quan đến size thì dùng size này gán hết vào option để tìm giá
            if(strpos($fieldchange, 'size')!==false){
                $size_key = $fieldchange;
                $size_value = $data[$size_key];
            }
            $line_no    = $this_line_no     = $data['id'];
            unset($data['id']);
            $query['products'][$this_line_no] = array_merge($query['products'][$this_line_no],$data);
            $line_data  = $this_line_data   = $query['products'][$this_line_no];
            if(isset($this_line_data['option_for'])&&$this_line_data['option_for']!=''){
                $no_same_parent = true;
                if(isset($this_line_data['same_parent'])&&$this_line_data['same_parent']==1){
                    $line_no    = $parent_line_no   = $this_line_data['option_for'];
                    $line_data  = $parent_line_data = $query['products'][$parent_line_no];
                    $no_same_parent = false;
                }
            }
            if($line_data['sell_by']=='combination')
                $is_combination = true;
            //Nếu product custom thì set lại unitprice và sellprice
            if(!is_object($line_data['products_id']))
                $line_data['unit_price'] = $line_data['sell_price'] = 0;
            // if($no_same_parent)
            //     $line_data = array_merge($line_data,$data);
            if(isset($query['tax']) && $query['tax']!='')
                $line_data['taxper'] = $this->get_tax($query['tax']);
            //==============================================
            //Nếu là thay đổi ở line entry, thì foreach option tính lại từ đầu
            //Nếu thay đổi sell price ở line entry thì bỏ qua
            //FIELDCHANGE
            if(($fieldchange!='sell_price')&&!$no_same_parent){
                $line_data['plus_sell_price'] = 0;
                if(isset($line_data['plus_unit_price']))
                    unset($line_data['plus_unit_price']);
                //Lấy tất cả option của line
                $options = $this->new_option_data(array('key'=>$line_no,'products_id'=>$line_data['products_id'],'options'=>$query['options'],'products'=>$query['products'],'date'=>$date),$query['products']);
                if(isset($options['option'])&&!empty($options['option'])){
                    foreach($options['option'] as $option_key=>$option){
                        if(isset($option['same_parent'])&&$option['same_parent']==1){
                            //Set kiểu đặt biệt sẽ tính line cha trước rồi mới + thêm vào plus sell price
                            $is_special = true;
                            if(isset($option['is_custom']) && $option['is_custom']==1)
                                $option['sell_price'] = $option['unit_price'];
                            //Same parent thì lấy size của line cha gán hết vào
                            //=======================
                            $option['sizew']        = $line_data['sizew'];
                            $option['sizew_unit']   = $line_data['sizew_unit'];
                            $option['sizeh']        = $line_data['sizeh'];
                            $option['sizeh_unit']   = $line_data['sizeh_unit'];
                            if(isset($size_key))
                                $option[$size_key]  = $size_value;
                            $option['quantity']     = (isset($option['quantity']) ? (float)$option['quantity'] : 1);
                            //========================
                            $option['plus_sell_price'] = 0;
                            if(!isset($option['sell_by'])||$option['sell_by']==''){
                            	if(isset($uom_list[$option['oum']]))
                                	$option['sell_by'] = $uom_list[$option['oum']];
                                else {
                                	$option['sell_by'] = 'area';
                                	$option['oum'] = 'Sq.ft.';
                                }
                            }
                            //Bắt đầu tính giá trên từng option
                            //========================
                            $cal_price = new cal_price;
                            $cal_price->arr_product_items   = $option;
                            $cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$option['product_id']);
                            if(isset($option['is_custom']) && $option['is_custom']==1){
                                //Nếu là loại custom thì không cần tìm lại unitprice mà cứ nhân size, số lượng
                                $cal_price->field_change = 'sell_price';
                            }else{
                                $cal_price->field_change = $fieldchange;
                                //Nhân cho số lượng line cha để tìm price break chính xác
                                $cal_price->arr_product_items['quantity'] *= $line_data['quantity'];
                                $cal_price->cal_price_items();
                                //Tính xong chỉ cần gán lại sellprice, dùng sell_price này gán tiếp tính lại giá
                                $option['sell_price']           = $cal_price->arr_product_items['sell_price'];
                                $cal_price->arr_product_items   = $option;
                                $cal_price->field_change        = 'sell_price';
                            }
                            $option = $cal_price->cal_price_items();
                            //=========== End calprice =============
                        	if(isset($option['choice'])&&$option['choice']==1)
                            	$line_data['plus_sell_price'] += $option['sub_total'];
                            //=========== Lưu ngược lại vào option nếu có =============
                            if(isset($option['this_line_no'])&&isset($query['options'][$option['this_line_no']])){
                                $this_option_data = $query['options'][$option['this_line_no']];
                                $this_option_data['sell_price'] = $option['sell_price'];
                                $this_option_data['quantity'] = $option['quantity'];
                                $this_option_data['sub_total'] = $option['sub_total'];
                                $this_option_data['sizew']      = $option['sizew'];
                                $this_option_data['sizew_unit'] = $option['sizew_unit'];
                                $this_option_data['sizeh']      = $option['sizeh'];
                                $this_option_data['sizeh_unit'] = $option['sizeh_unit'];
                                $query['options'][$option['this_line_no']] = $this_option_data;
                            }
                            // $fieldchange = '';
                            //==========End tính giá option============
                        } // End if SAMEPARENT
                        //Cộng đồn sub_total để sử dụng cho sellby là combination
                        $total_sub_total += (isset($option['sub_total']) ? (float)$option['sub_total'] : 0);
                    } // End foreach
                }// End If OPTION
            } //END FIELDCHANGE
            //=============Xét custom_unit_price=============
            if(!isset($line_data['custom_unit_price']))
                $line_data['custom_unit_price'] = $line_data['unit_price'];
            else if(isset($line_data['custom_unit_price'])){
                if(!is_object($line_data['products_id']) && $line_data['custom_unit_price']!=0){
                    $line_data['sell_price'] = $line_data['unit_price'] = $line_data['custom_unit_price'];
                    if($fieldchange=='custom_unit_price')
                        $line_data['plus_sell_price'] = 0;
                    $fieldchange = 'custom';
                }
            }
            //=============End Xét custom_unit_price=========
            //=============Tính line chính==============
            $cal_price = new cal_price;
            $cal_price->arr_product_items = $line_data;
            if($is_combination){
                if(isset($line_data['plus_sell_price']))
                    unset($line_data['plus_sell_price']);
                $line_data = array_merge($line_data,$cal_price->combination_cal_price());
                $line_data['sell_price'] = $line_data['unit_price'] += $total_sub_total;
                if($total_sub_total>0){
                    $line_data['sub_total'] = round((float)$line_data['unit_price']*(float)$line_data['quantity'],2);
                    $line_data['tax'] = round(((float)(isset($line_data['taxper']) ? $line_data['taxper'] : 0)/100)*(float)$line_data['sub_total'],3);
                    $line_data['amount'] = round((float)$line_data['sub_total']+(float)$line_data['tax'],2);
                }
            } else {
                $result = array();
                if($fieldchange!='sell_price')
                    $result = $this->change_sell_price_company($query['company_id'],$line_data['products_id']);
                $cal_price->price_break_from_to = $result;
                $cal_price->field_change = $fieldchange;
                $line_data = array_merge($line_data,$cal_price->cal_price_items($is_special));
            }
            //=============Xét custom_unit_price=============
            if(isset($line_data['custom_unit_price'])){
                if(!is_object($line_data['products_id'])){
                    $line_data['custom_unit_price'] = $line_data['unit_price'] = $line_data['sell_price'];
                } else {
                    $unit_price = $line_data['unit_price'];
					if($fieldchange!='custom_unit_price')
                        $line_data['custom_unit_price'] = $line_data['unit_price'];
                    else if($fieldchange=='custom_unit_price'&&$line_data['custom_unit_price']<$line_data['unit_price']
                            &&!$this->check_permission($this->name.'_@_custom_unit_price_@_add'))
                       	$line_data['custom_unit_price'] = $line_data['unit_price'];
                    $line_data['sell_price'] = $line_data['unit_price'] = $line_data['custom_unit_price'];
                    if($is_combination){
                        $line_data['sub_total'] = round((float)$line_data['unit_price']*(float)$line_data['quantity'],2);
                        $line_data['tax'] = round(((float)(isset($line_data['taxper']) ? $line_data['taxper'] : 0)/100)*(float)$line_data['sub_total'],3);
                        $line_data['amount'] = round((float)$line_data['sub_total']+(float)$line_data['tax'],2);
                    } else {
                        $cal_price = new cal_price;
                        $cal_price->arr_product_items = $line_data;
                        $cal_price->field_change = 'custom';
                        $line_data = $cal_price->cal_price_items($is_special);
                    }
                    $line_data['unit_price'] = $unit_price;
                }
            }
            //=============End Xét custom_unit_price=========
            //=============End tính line chính==========
            $query['products'][$line_no] = $line_data;
            $this_line_data = $query['products'][$this_line_no];
            //Nếu line entry đang sửa là option và không phải same parent thì update ngược lại option
            if($no_same_parent){
                if(isset($query['options'])&&!empty($query['options'])){
                    foreach($query['options'] as $option_key=>$option){
                        if(isset($option['deleted'])&&$option['deleted']) continue;
                        if(!isset($option['line_no']) || $option['line_no']!=$line_no) continue;
                        $query['options'][$option_key] = array_merge($option,$line_data); break;
                    }
                }
            }
            //Tính lại sum
            $arr_sum = $this->new_cal_sum($query['products']);
            $query = array_merge($query,$arr_sum);
            //=============Save=========================
            $this->opm->save($query);
            //==========================================
            $arr_data = array();
            $arr_data['sum'] = $arr_sum;
            //===Update Minium Order Adjustment=========
            if($this->name!='Purchaseorders'){
	            $minimum = $this->get_minimum_order();
	            if($arr_sum['sum_sub_total'] < $minimum){
	            	$extra_price = $minimum - $arr_sum['sum_sub_total'];
	            	$arr_data['last_insert']['sub_total'] = $extra_price;
	            	$arr_data['last_insert']['tax'] = $extra_price*$line_data['taxper']/100;
	            	$arr_data['last_insert']['amount'] = $extra_price+$arr_data['last_insert']['tax'];
	            	$arr_data['sum']['sum_amount'] += $arr_data['last_insert']['amount'];
	            	$arr_data['sum']['sum_sub_total'] += $arr_data['last_insert']['sub_total'];
	            	$arr_data['sum']['sum_tax'] += $arr_data['last_insert']['tax'];
	            }
            }
            //===End Update Minium Order Adjustment=====
            if(!isset($parent_line_data))
                $arr_data['self'] = $line_data;
            else {
                $arr_data['parent'] = $line_data;
                $arr_data['self'] = $this_line_data;
            }
            if(isset($_POST['data']))
                echo json_encode($arr_data);
            else
                return $arr_data;
        }
        die;
    }
    function new_option_data($arr_data,$products){
        $data = $option_group = array(); $groupstr = '';
        $idsub = $arr_data['key'];
        $products_id = $arr_data['products_id'];
        $custom_option = $arr_data['options'];
        if(isset($arr_data['date'])){
        	if(is_object($arr_data['date']))
        		$date = $arr_data['date']->sec;
        	else if(is_numeric($arr_data['date']))
        		$date = $arr_data['date'];
        }
        else
        	$date = time();
        $change_date = strtotime(date('2014-03-28'));
        $arr_option_choice = array();
        foreach($custom_option as $value){
            if(!isset($value['choice']))
                $value['choice'] = 0;
            if(isset($value['this_line_no']))
                $arr_option_choice[$value['this_line_no']]['choice'] = $value['choice'];
        }
        if($idsub<0)
            return $data;
        foreach($custom_option as $k_custom=>$v_custom){
            if(!isset($v_custom['parent_line_no'])
               || $v_custom['parent_line_no']!=$idsub ){
                unset($custom_option[$k_custom]);
                continue;
            }
        }
        if(is_object($products_id))
            $products_id = (string)$products_id;

        if($products_id!=''){
            $this->selectModel('Product');
            $products_option = $this->Product->options_data($products_id);
        }
        if($date<$change_date){
	        if(isset($products_option['productoptions']) && !empty($products_option['productoptions'])){
	            $data = $products_option['productoptions'];
	            foreach($data as $kk=>$vv){
	                foreach($custom_option as $k_custom => $v_custom){
	                    if( !isset($v_custom['proline_no']) || $v_custom['proline_no'] == ''){
	                        unset($custom_option[$k_custom]);
	                        continue;
	                    }
	                    if($kk == $v_custom['proline_no']){
	                        $option = $v_custom;
	                        $data[$kk] = array_merge($vv,array_merge($v_custom,array('is_custom'=>true)));
	                        unset($custom_option[$k_custom]);
	                        break;
	                    }
	                }
	            }
	        }else{
	            foreach($custom_option as $key=>$value)
	                $custom_option[$key]['is_custom'] = true;
	            $data = $custom_option;
	        }
    	} else{
    		foreach($custom_option as $key=>$value)
                $custom_option[$key]['is_custom'] = true;
            $data = $custom_option;
    	}
        if(!empty($data)){
            foreach($data as $k=>$v)
                $data[$k]['_id'] = $k;
            $this->opm->aasort($data,'option_group');
        }
        foreach($data as $kks=>$vvs){
            if(!isset($vvs['product_id']))
                continue;
            if(!isset($vvs['proline_no']))
                $data[$kks]['proline_no'] = $vvs['_id'];
            if(!isset($vvs['parent_line_no']))
                $data[$kks]['parent_line_no'] = $idsub;
            if($data[$kks]['proline_no']!=''&&(!isset($vvs['line_no']) || $vvs['line_no']=='')){
                foreach($products as $k_p => $v_p){
                     if($v_p['deleted'] || !isset($v_p['option_for']) || $v_p['option_for']!=$idsub){
                        unset($products[$k_p]);
                        continue;
                    }
                    if($v_p['proids']==$products_id.'_'.$data[$kks]['proline_no']){
                        $data[$kks]['line_no'] = $k_p;
                        unset($products[$k_p]);
                        break;
                    }
                }
            }
            if(isset($vvs['require'])&&$vvs['require']==1){
                $data[$kks]['choice'] = 1;
                if(isset($arr_option_choice[$kks]['choice']))
                    $data[$kks]['choice'] = $arr_option_choice[$kks]['choice'];
                if(isset($vvs['group_type'])&&$vvs['group_type']!='Exc')
                    $data[$kks]['xlock']['choice'] = 1;
            }
            if(is_object($vvs['product_id'])){
            	$data[$kks]['xlock']['product_name'] = 1;
            	$data[$kks]['xlock']['oum'] = 1;
            }
            if(isset($vvs['option_group'])){
                $option_group[$vvs['option_group']] = (string)$vvs['option_group'];
                $groupstr.= (string)$vvs['option_group'].',';
            }
        }
        $arr_return = array();
        $arr_return['option'] = $data;
        $arr_return['groupstr'] = $groupstr;
        $arr_return['option_group'] = $option_group;
        return $arr_return;
    }
    function option_cal_price($arr_tmp_data){
        if(!isset($arr_tmp_data['product_key']))
            return false;
        //Xử lý mảng $_POST tách ra theo stt
        $arr_data = array();
        $arr_options = array();
        switch ($this->name) {
        	case 'Quotations':
        		$date = 'quotation_date';
        		break;
    		case 'Salesorders':
    			$date = 'salesorder_date';
        		break;
    		case 'Salesinvoices':
        		$date = 'invoice_date';
        		break;
        	default:
        		$date = 'quotation_date';
        		break;
        }
        //=====================================
        $uom_list = $this->Setting->uom_option_list();
        //=====================================
        $id = $this->get_id();
        $query = $this->opm->select_one(array('_id'=>new MongoId($id)),array('options','products','company_id','tax',$date));
        $line_no = $arr_tmp_data['product_key'];
        $line_data = $query['products'][$line_no];
        $parent_id = $query['products'][$line_no]['products_id'];
        if(isset($line_data['plus_unit_price']))
            unset($line_data['plus_unit_price']);
        if(!is_object($line_data['products_id']))
            $line_data['unit_price'] = $line_data['sell_price'] = 0;
        $line_data['plus_sell_price'] = 0;
        unset($arr_tmp_data['submit'],$arr_tmp_data['product_key']);
        //=====================================
        foreach($arr_tmp_data as $key=>$value){
            $position = strrpos($key, '_',-1);
            if($position===false) continue;
            $k =substr($key, $position+1);
            if($k=='id') continue;
            if(strpos($key, 'cb_')!==false
               &&$value == 'on')
                $value = 1;
            $key = str_replace(array('_'.$k,'cb_'), '', $key);
            if($key=='product_id'&&strlen($value)==24)
                $value = new MongoId($value);
            if($key=='deleted'){
            	$value = false;
            }
            $arr_data[$k][$key] = $value;
        }
        //Debug - $_POST after group
        // pr($arr_data);die;
        //End xử lý $_POST
        //=====================================
        if(!isset($query['options'])){
            $query['options'] = array();
        }
        $i = count($query['options']);
        $arr_option = array();
        $option_group = array();
        foreach($arr_data as $key=>$value){
        	if((!isset($value['product_id']) || $value['product_id']=='')
        		&&isset($value['oum']))
        		$arr_data[$key]['sell_by'] = $value['sell_by'] = (isset($uom_list[$value['oum']]) ? $uom_list[$value['oum']] : 'unit');
            if(!isset($value['choice']))
                $arr_data[$key]['choice'] = $value['choice'] = 0;
            if(isset($value['option_group'])&&$value['option_group']!=''
               	&&isset($value['group_type'])&&$value['group_type']=='Exc'
               	&&isset($value['choice'])&&$value['choice']==1){
            	if(!isset($option_group[$value['option_group'].'_'.$value['group_type']]))
        			$option_group[$value['option_group'].'_'.$value['group_type']] = 1;
        		else
        			$arr_data[$key]['choice'] = $value['choice'] = 0;
            }
            if(!isset($value['same_parent']))
                $arr_data[$key]['same_parent'] = $value['same_parent'] =  0;
            if(isset($value['thisline_no'])&&$value['thisline_no']!=''
               || isset($value['this_line_no'])&&$value['this_line_no']!=''){
                if(isset($value['thisline_no'])){
                    $value['this_line_no'] = $value['thisline_no'];
                    unset($value['thisline_no']);
                }
                $option_no = $value['this_line_no'];
                $query['options'][$option_no] = array_merge($query['options'][$option_no],$value);
                $query['options'][$option_no]['parent_line_no'] = $line_no;
                $arr_option[$key] = $query['options'][$option_no];
            } else {
                $value['this_line_no'] = $query['options'][$i]['this_line_no'] = $i;
                $query['options'][$i] = $value;
                $query['options'][$i]['parent_line_no'] = $line_no;
                $arr_option[$i] = $query['options'][$i];
                $i++;
            }
        }
        //==========================================================
        //Lấy option
        $options = $this->new_option_data(array('key'=>$line_no,'products_id'=>$parent_id,'options'=>$arr_option,'date'=>$query[$date]),$query['products']);
        $num_of_products = count($query['products']);
        foreach($options['option'] as $key=>$value){
            $option_no = $value['this_line_no'];
            if(!isset($value['choice']))
               $value['choice'] = 0;
            if(isset($value['choice'])){
                if($value['choice']==0
                   &&isset($value['line_no'])&&$value['line_no']!=''){
                    if(isset($query['products'][$value['line_no']])){
                        unset($query['options'][$option_no]['line_no'],$options['option'][$key]['line_no'],$arr_option[$option_no]);
                        $query['products'][$value['line_no']] = array('deleted'=>true);
                    }
                } else if ($value['choice']==1){
                    if(!isset($value['line_no']) || $value['line_no']==''){
                        $query['products'][$num_of_products] = $this->new_line_entry($value,$query['products'][$value['parent_line_no']],$query['company_id'],$query['options'],$option_no);
                        $query['options'][$option_no]['line_no'] = $num_of_products;
                        $num_of_products++;
                    }
                }
            }
            $query['options'][$option_no]['choice'] = $value['choice'];
        }
        //==========================================================
        $arr_update = array('sizew','product_name','sell_by','oum','sizew_unit','sizeh','sizeh_unit','unit_price','quantity','sub_total','sell_price','amount','tax','same_parent');
        $is_special = false;
        $this->selectModel('Setting');
        //===================Tính lại thuế===========================
        $taxper = 0;
        if(isset($query['tax']) && $query['tax']!='')
            $taxper = $this->get_tax($query['tax']);
        //===============End Tính lại thuế===========================
        $total_sub_total = 0;
        //Lặp vòng và tính lại toàn bộ option
        if(isset($options['option'])&&!empty($options['option'])){
            foreach($options['option'] as $option_key=>$option){
                if(!isset($option['same_parent']))
                    $option['same_parent'] = 0;
                $option['sell_price'] = $option['unit_price'];
                if($option['same_parent']==1){
                    //Set kiểu đặt biệt sẽ tính line cha trước rồi mới + thêm vào plus sell price
                    $is_special = true;
                    //Same parent thì lấy size của line cha gán hết vào
                    //=======================
                    $option['sizew']        = $line_data['sizew'];
                    $option['sizew_unit']   = $line_data['sizew_unit'];
                    $option['sizeh']        = $line_data['sizeh'];
                    $option['sizeh_unit']   = $line_data['sizeh_unit'];
                    if(isset($size_key))
                        $option[$size_key]  = $size_value;
                    $option['quantity']     = (isset($option['quantity']) ? (float)$option['quantity'] : 1);
                    //========================
                    $option['plus_sell_price'] = 0;
                    if(!isset($option['sell_by'])||$option['sell_by']=='')
                        $option['sell_by'] = $uom_list[$option['oum']];
                    //Bắt đầu tính giá trên từng option SAMEPARENT
                    $cal_price = new cal_price;
                    $cal_price->arr_product_items   = $option;
                    $cal_price->field_change = 'sell_price';
                    $option = $cal_price->cal_price_items();
                    //=========== End calprice =============
                    if(isset($option['choice'])&&$option['choice']==1)
                    	$line_data['plus_sell_price'] += $option['sub_total'];

                } else if($option['same_parent']==0){
                    //Bắt đầu tính giá trên từng option KHÔNG SAMEPARENT
                    $cal_price = new cal_price;
                    $option['taxper'] = $taxper;
                    $cal_price->arr_product_items = $option;
                    $cal_price->field_change = 'sell_price';
                    $option = array_merge($option,$cal_price->cal_price_items());
                    //=========== End calprice =============
                }
                $option['unit_price'] = $option['sell_price'];
                //=========== Lưu ngược lại vào option=============
                $query['options'][$option['this_line_no']] = array_merge($query['options'][$option['this_line_no']],$option);
                if(isset($option['line_no'])
                   &&isset($query['products'][$option['line_no']])
                   &&!$query['products'][$option['line_no']]['deleted']){
                    foreach($arr_update as $update_field){
                    	if(isset($option[$update_field])){
                    		$p_update_field = $update_field;
	                    	if($update_field=='product_name')
	                    		$p_update_field = 'products_name';
	                    	else if($update_field == 'unit_price')
	                    		$query['products'][$option['line_no']]['custom_unit_price'] = $option[$update_field];
                        	$query['products'][$option['line_no']][$p_update_field] = $option[$update_field];
                        }
                    }
                }
                $total_sub_total += (isset($option['sub_total']) ? (float)$option['sub_total'] : 0);
                //==========End tính giá option====================
            } // End foreach OPTION
        }// End if OPTION
        //==================Xét custom unit price=================
        if(!isset($line_data['custom_unit_price']))
            $line_data['custom_unit_price'] = $line_data['unit_price'];
        else if(isset($line_data['custom_unit_price'])){
            if(!is_object($line_data['products_id']))
                $line_data['sell_price'] = 0;
        }
        //===============End Xét custom unit price================
        if($line_data['sell_by']=='combination'){
            if(isset($line_data['plus_sell_price']))
                unset($line_data['plus_sell_price']);
            $line_data = array_merge($line_data,$cal_price->combination_cal_price());
            $line_data['sell_price'] = $line_data['unit_price'] += $total_sub_total;
            if($total_sub_total>0){
                $line_data['sub_total'] = round((float)$line_data['unit_price']*(float)$line_data['quantity'],2);
                $line_data['tax'] = round(((float)(isset($line_data['taxper']) ? $line_data['taxper'] : 0)/100)*(float)$line_data['sub_total'],3);
                $line_data['amount'] = round((float)$line_data['sub_total']+(float)$line_data['tax'],2);
            }
        } else {
            $cal_price = new cal_price;
            $cal_price->arr_product_items = $line_data;
            $cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$line_data['products_id']);
            $line_data = array_merge($line_data,$cal_price->option_cal_price());
        }
        //==================Xét custom unit price=================
        // if(isset($line_data['custom_unit_price'])){
        //     if(!is_object($line_data['products_id'])){
        //         $line_data['custom_unit_price'] = $line_data['unit_price'] = $line_data['sell_price'];
        //     } else {
        //         $unit_price = $line_data['unit_price'];
        //         $line_data['custom_unit_price'] = $line_data['unit_price'];
        //         $line_data['sell_price'] = $line_data['unit_price'] = $line_data['custom_unit_price'];
        //         if($line_data['sell_by']=='combination'){
        //             $line_data['sub_total'] = round((float)$line_data['unit_price']*(float)$line_data['quantity'],2);
        //             $line_data['tax'] = round(((float)(isset($line_data['taxper']) ? $line_data['taxper'] : 0)/100)*(float)$line_data['sub_total'],3);
        //             $line_data['amount'] = round((float)$line_data['sub_total']+(float)$line_data['tax'],2);
        //         } else {
        //             $cal_price = new cal_price;
        //             $cal_price->arr_product_items = $line_data;
        //             $cal_price->field_change = 'sell_price';
        //             $line_data = $cal_price->cal_price_items($is_special);
        //         }
        //         $line_data['sell_price'] = $line_data['unit_price'] = $unit_price;
        //     }
        // }
        //===============End Xét custom unit price================
        $query['products'][$line_no] = $line_data;
        $arr_sum = $this->new_cal_sum($query['products']);
        $query = array_merge($query,$arr_sum);
        if($this->request->is('ajax')){
        	if($this->opm->save($query))
	            echo 'ok';
	        else
	            echo $this->arr_errors_save[1];
            die;
        } else
        	$this->opm->save($query);
    }
    function create_temporary_product(){
    	$this->layout = 'ajax';
        if(isset($_POST['product_id'])&&isset($_POST['num_of_options'])){
            $product_id = $_POST['product_id'];
            $num_of_options = $_POST['num_of_options'];
            if($product_id != ''){
                $this->selectModel('Product');
                $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('code','name','oum','sell_price','quantity','sku','sizew','sizew_unit','sizeh','sizeh_unit','sell_by'));
                $product['this_line_no'] = $product['_id'].time();
                $product['product_id'] = $product['_id'];
                $product['product_name'] = $product['name'];
                $product['sizew_unit'] = (isset($product['sizew_unit']) ? $product['sizew_unit'] : 'in');
                $product['sizeh_unit'] = (isset($product['sizeh_unit']) ? $product['sizeh_unit'] : 'in');
                $product['sell_by'] = (isset($product['sell_by']) ? $product['sell_by'] : 'unit');
                $product['unit_price'] = (isset($product['sell_price']) ? (float)$product['sell_price'] : 0);
            } else {
                $product = array(
                                'this_line_no' => md5(time()),
                                'code' => '',
                                'product_id' => '',
                                'product_name' => 'This is new record. Click for edit',
                                'sizew_unit' => 'in',
                                'sizeh_unit' => 'in',
                                'sell_by' => 'area',
                                'oum' => 'Sq.ft.',
                                'unit_price' => 0,
                                'quantity' => 1,
                                'sku' => '',
                                'sizew' => 12,
                                'sizeh' => 12,
                                 );
                $this->set('custom_product',true);
            }
            $this->set('option',$product);
            $this->set('bg',($num_of_options%2==0 ? 'bg2' : 'bg1'));
            $this->render('../Elements/render_temporary_product');
        } else
            die;
    }
    function new_line_entry($option,&$parent_line,$company_id,&$current_options,$option_no){
        $option_for = $option['parent_line_no'];
        $option_id = $option['proline_no'];
        $new_line = array();
        $new_line['code']           = $option['code'];
        $new_line['sku']            = (isset($option['sku']) ? $option['sku'] : '');
        $new_line['products_name']  = $option['product_name'];
        $new_line['products_id']    = $option['product_id'];
        $new_line['quantity']       = $option['quantity'];
        $new_line['sub_total']      = $option['sub_total'];
        $new_line['sizew']          = isset($parent_line['sizew']) ? $parent_line['sizew'] : $option['sizew'];
        $new_line['sizew_unit']     = isset($parent_line['sizew_unit'])?$parent_line['sizew_unit']:$option['sizew_unit'];
        $new_line['sizeh']          = isset($parent_line['sizeh']) ? $parent_line['sizeh'] : $option['sizeh'];
        $new_line['sizeh_unit']     = isset($parent_line['sizeh_unit'])?$parent_line['sizeh_unit']:$option['sizeh_unit'];
        $new_line['sell_by']        = (isset($option['sell_by']) ? $option['sell_by'] : 'unit');
        $new_line['oum']            = $option['oum'];
        $new_line['same_parent']    = isset($option['same_parent']) ? (int)$option['same_parent'] : 0;
        $new_line['sell_price']     = (float)$option['unit_price'] - (float)$option['unit_price']*((float)$option['discount']/100);

        if(isset($parent_line['taxper']))
            $new_line['taxper']     = $parent_line['taxper'];
        if(isset($parent_line['tax']))
            $new_line['tax']        = $parent_line['tax'];
        $new_line['option_for']     = $option_for;
        $new_line['deleted']        = false;
        $new_line['proids']         = $parent_line['products_id'].'_'.$option_id;

        if(!isset($company_id))
            $company_id='';

        $cal_price = new cal_price;
        $cal_price->arr_product_items = $new_line;
        $cal_price->price_break_from_to = $this->change_sell_price_company($company_id,$new_line['products_id']);
        $cal_price->field_change = '';
        if(isset($option['original_unit_price'])&&$option['original_unit_price']!=$option['unit_price'])
        	$cal_price->field_change = 'sell_price';
        $cal_price->cal_price_items();
        $new_line = array_merge((array)$new_line,(array)$cal_price->arr_product_items);

        //neu la same_parent thi thay gia cua parent va tinh lai gia
        if($new_line['same_parent']==1){
            $cal_price = new cal_price;
            $cal_price->arr_product_items = $new_line;
            $cal_price->price_break_from_to = $this->change_sell_price_company($company_id,$new_line['products_id']);
            $cal_price->field_change = '';
            $cal_price->cal_price_items();
            $new_line['sell_price'] = $cal_price->arr_product_items['sell_price'];
            if(!isset($parent_line['plus_sell_price']))
                $parent_line['plus_sell_price'] = 0;
            $parent_line['sell_price'] += (float)$new_line['sell_price'];
            $parent_line['plus_sell_price'] += (float)$new_line['sell_price'];
            $cal_price2 = new cal_price;
            $cal_price2->arr_product_items = $parent_line;
            $cal_price2->field_change = 'sell_price';
            $cal_price2->cal_price_items();
            $parent_line = $cal_price2->arr_product_items;
            $new_line['sell_price'] = '';
        }
        return $new_line;
    }
    function new_cal_sum($products){
        $count_sub_total = 0;
        $count_amount = 0;
        $count_tax = 0;
        if(!empty($products)){
            foreach($products as $pro_key=>$pro_data){
                if(!$pro_data['deleted']){
                    if(isset($pro_data['option_for'])&&is_array($products[$pro_data['option_for']])
                        && (isset($products[$pro_data['option_for']]['sell_by'])&&$products[$pro_data['option_for']]['sell_by']=='combination'
                            || (isset($pro_data['same_parent'])&&$pro_data['same_parent']>0)
                            )
                        )
                        continue;
                    if(isset($pro_data['option_for'])
                       && isset($products[$pro_data['option_for']]) && $products[$pro_data['option_for']]['deleted'])
                    	continue;
                    //cộng dồn sub_total
                    if(isset($pro_data['sub_total']))
                        $count_sub_total += (float)$pro_data['sub_total'];
                    //cộng dồn amount
                    if(isset($pro_data['amount']))
                        $count_amount += (float)$pro_data['amount'];
                }
            }
            //tính lại sum tax
            $count_tax = $count_amount - $count_sub_total;
        }
        return array('sum_amount'=>$count_amount,'sum_sub_total'=>$count_sub_total,'sum_tax'=>$count_tax);
    }
    function save_new_line($product_id='',$option_id='',$option_for=''){
        if(isset($_POST['product_id']))
            $product_id = $_POST['product_id'];
        if(isset($_POST['option_id']))
            $option_id = $_POST['option_id'];
        if(isset($_POST['option_for']))
            $option_for = $_POST['option_for'];

        $ids = $this->get_id();
        if($ids!=''){
            $arr_insert = $line_entry = $parent_line = array();
            //lay products hien co
            $query = $this->opm->select_one(array('_id'=>new MongoId($ids)),array('products','company_id','options'));
            if(isset($query['products']) && is_array($query['products']) && !empty($query['products'])){
                $line_entry = $query['products'];
                $key = count($line_entry);
            }

            //khởi tạo line entry mới
            $option_line_data = $this->option_list_data($product_id,$option_for);

            $options_data = $option_line_data['option'];
            if(isset($options_data[$option_id])){
                $vv = $options_data[$option_id];
                $product = array();
                if(isset($vv['product_id'])&&is_object($vv['product_id'])){
                    $this->selectModel('Product');
                    $product = $this->Product->select_one(array('_id'=> new MongoId($vv['product_id'])),array('sizew','sizew_unit','sizeh','sizeh_unit'));
                }
                if(isset($line_entry[$option_for]))
                    $parent_line = $line_entry[$option_for];
                $vv['unit_price'] = (isset($vv['unit_price']) ? (float)$vv['unit_price'] : 0);
                $new_line = array();
                $new_line['code']           = $vv['code'];
                $new_line['sku']            = (isset($vv['sku']) ? $vv['sku'] : '');
                $new_line['products_name']  = $vv['product_name'];
                $new_line['products_id']    = $vv['product_id'];
                $new_line['quantity']       = $vv['quantity'];
                $new_line['sub_total']      = isset($vv['sub_total']) ? $vv['sub_total'] : 0;
                $new_line['sizew']          = isset($product['sizew']) ? $product['sizew'] : 12;
                $new_line['sizew_unit']     = isset($product['sizew_unit'])?$parent_line['sizew_unit']:'unit';
                $new_line['sizeh']          = isset($product['sizeh']) ? $product['sizeh'] : 12;
                $new_line['sizeh_unit']     = isset($product['sizeh_unit'])?$product['sizeh_unit']:'unit';
                $new_line['sell_by']        = (isset($vv['sell_by']) ? $vv['sell_by'] : 'unit');
                $new_line['oum']            = $vv['oum'];
                $new_line['same_parent']    = isset($vv['same_parent']) ? (int)$vv['same_parent'] : 0;
                $new_line['sell_price']     = (float)$vv['unit_price'] - (float)$vv['unit_price']*((float)$vv['discount']/100);

                if(isset($query['products'][$option_for]['taxper']))
                    $new_line['taxper']     = $query['products'][$option_for]['taxper'];
                if(isset($query['products'][$option_for]['tax']))
                    $new_line['tax']        = $query['products'][$option_for]['tax'];
                $new_line['option_for']     = $option_for;
                $new_line['deleted']        = false;
                $new_line['proids']         = $product_id.'_'.$option_id;
                if(!isset($query['company_id']))
                    $query['company_id']='';

                $cal_price = new cal_price;
                $cal_price->arr_product_items = $new_line;
                $cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$new_line['products_id']);
                $cal_price->field_change = 'sell_price';
                $cal_price->cal_price_items();
                $new_line = array_merge((array)$new_line,(array)$cal_price->arr_product_items);


                $line_entry[] = $new_line;
                //Update lại option
                $arr_insert['options'] = $query['options'];
                $arr_insert['options'][$option_id]['unit_price'] = $new_line['unit_price'];
                $arr_insert['options'][$option_id]['sell_by'] = $new_line['sell_by'];
                $arr_insert['options'][$option_id]['oum'] = $new_line['oum'];
                $arr_insert['options'][$option_id]['quantity'] = $new_line['quantity'];
                $arr_insert['options'][$option_id]['sell_price'] = $new_line['sell_price'];
                $arr_insert['options'][$option_id]['amount'] = $new_line['amount'];
                $arr_insert['options'][$option_id]['sub_total'] = $new_line['sub_total'];
                $arr_insert['options'][$option_id]['line_no'] = $key;
                $arr_insert['options'][$option_id]['this_line_no'] = $option_id;

                //neu la nhom Exc thi xoa cac item khac cung nhom
                if(isset($vv['option_group']) && isset($vv['group_type']) &&  $vv['group_type']=='Exc'){
                    foreach ($line_entry as $k=>$vs){
                        if(isset($vs['deleted']) && !$vs['deleted'] && isset($vs['proids']) && $vs['proids'] !=$product_id.'_'.$option_id){
                            $proids = explode("_",$vs['proids']);
                            $proids = $proids[1];
                            //neu cung nhom
                            if(isset($options_data[$proids]['option_group']) && $options_data[$proids]['option_group']==$vv['option_group'] && isset($vs['option_for']) && $vs['option_for']==$option_for){
                                //xoa item
                                $line_entry[$k]['deleted'] = true;

                                //tru ra neu la loai SP
                                if($vs['same_parent']==1){
                                    $cal_price = new cal_price;
                                    $cal_price->arr_product_items = $line_entry[$option_for];
                                    $cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$vs['products_id']);
                                    $cal_price->field_change = '';
                                    $cal_price->cal_price_items();
                                    $sellprice = $cal_price->arr_product_items['sell_price'];

                                    if(!isset($line_entry[$option_for]['plus_sell_price']))
                                        $line_entry[$option_for]['plus_sell_price'] = 0;

                                    $line_entry[$option_for]['sell_price'] -= $sellprice;;
                                    $line_entry[$option_for]['plus_sell_price'] -= $sellprice;

                                    $cal_price2 = new cal_price;
                                    $cal_price2->arr_product_items = $line_entry[$option_for];
                                    $cal_price2->field_change = 'sell_price';
                                    $cal_price2->cal_price_items();
                                    $line_entry[$option_for] = $cal_price2->arr_product_items;
                                }
                            }
                        }
                    }
                }
                //save lai
                $arr_insert['products'] = $line_entry;
                $arr_insert['_id'] = new MongoId($ids);

                if($this->opm->save($arr_insert)){
                    $this->cal_price_line(array('data'=>array('id'=>$key),'fieldchange'=>'products_name'));
                    echo 'ok';
                }else{
                    echo $this->opm->arr_errors_save[1];
                }
            }
        }die;
    }
    function get_minimum_order($modelName='',$id=''){
    	$sell_price = 50;
    	$this->selectModel('Stuffs');
    	$product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
    	if(isset($product['product_id']) && is_object($product['product_id'])){
    		if($modelName=='')
    			$query = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('company_id'));
    		else{
    			$this->selectModel($modelName);
    			$query = $this->$modelName->select_one(array('_id'=> new MongoId($id)),array('company_id'));
    		}
    		if(isset($query['company_id'])&&is_object($query['company_id'])){
    			$this->selectModel('Company');
    			$company = $this->Company->select_one(array('_id'=>new MongoId($query['company_id'])),array('pricing'));
    			if(isset($company['pricing'])&&!empty($company['pricing'])){
    				foreach($company['pricing'] as $pricing){
    					if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
    					if((string)$pricing['product_id']!=(string)$product['product_id']) continue;
    					if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
    					$price_break = reset($pricing['price_break']);
    					return (float)$price_break['unit_price'];
    				}
    			}
    		}
    		$this->selectModel('Product');
    		$product = $this->Product->select_one(array('_id'=> new MongoId($product['product_id'])),array('sell_price'));
    		if(isset($product['sell_price']))
    			$sell_price = (float)$product['sell_price'];
    	}
    	return $sell_price;
    }
    function append_build_salesorder($salesinvoice_id,$salesorder){
		$this->selectModel('Salesinvoice');
		$this->selectModel('Salesorder');
		$salesinvoice = $this->Salesinvoice->select_one(array('_id'=> new MongoId($salesinvoice_id)));
		if(isset($salesinvoice['salesorder_id']) && is_object($salesinvoice['salesorder_id'])){
			$first_salesorder = $this->Salesorder->select_one(array('_id'=> new MongoId($salesinvoice['salesorder_id'])),array('options','products','sum_tax','sum_sub_total','sum_amount','taxval'));
			if(!isset($first_salesorder['options']))
				$first_salesorder['options'] = array();
			$last_insert = array('sub_total'=>0,'amount'=>0,'tax'=>0);
			$salesinvoice['options'] = $first_salesorder['options'];
			$salesinvoice['products'] = $first_salesorder['products'];
			$minimum = $this->get_minimum_order('Salesorder',$first_salesorder['_id']);
			$sub_total = $tax = $amount = 0;
    		if($first_salesorder['sum_sub_total']<$minimum){
    			$more_sub_total = $minimum - (float)$first_salesorder['sum_sub_total'];
    			$sub_total = $more_sub_total;
                $tax = $sub_total*(float)$first_salesorder['taxval']/100;
                $amount = $sub_total+$tax;
    		}
    		$last_insert['sub_total'] += $sub_total;
    		$last_insert['amount'] += $amount;
    		$last_insert['tax'] += $tax;
			$options_num = count($salesinvoice['options']);
			$products_num = count($salesinvoice['products']);
			$existed = false;
			if(isset($salesinvoice['salesorders'])&&!empty($salesinvoice['salesorders'])){
				foreach($salesinvoice['salesorders'] as $key=>$so_id){
					if($key==0) continue;
					if(!is_object($so_id)) continue;
					if((string)$so_id==(string)$salesorder['_id'])
						$existed = true;
					$so = $this->Salesorder->select_one(array('_id'=> new MongoId($so_id)),array('options','products','sum_tax','sum_sub_total','sum_amount','taxval'));
					$arr_data = $this->build_salesorder($so,$products_num,$options_num);
					$salesinvoice['options'] = array_merge($salesinvoice['options'],$arr_data['options']);
					$salesinvoice['products'] = array_merge($salesinvoice['products'],$arr_data['products']);
					$minimum = $this->get_minimum_order('Salesorder',$so['_id']);
					$sub_total = $tax = $amount = 0;
		    		if($so['sum_sub_total']<$minimum){
		    			$more_sub_total = $minimum - (float)$so['sum_sub_total'];
		    			$sub_total = $more_sub_total;
		                $tax = $sub_total*(float)$so['taxval']/100;
		                $amount = $sub_total+$tax;
		    		}
		    		$last_insert['sub_total'] += $sub_total;
		    		$last_insert['amount'] += $amount;
		    		$last_insert['tax'] += $tax;
				}
			}
			if(!$existed){
				$arr_data = $this->build_salesorder($salesorder,$products_num,$options_num);
				$salesinvoice['options'] = array_merge($salesinvoice['options'],$arr_data['options']);
				$salesinvoice['products'] = array_merge($salesinvoice['products'],$arr_data['products']);
				$minimum = $this->get_minimum_order('Salesorder',$salesinvoice['_id']);
				$sub_total = $tax = $amount = 0;
	    		if($salesorder['sum_sub_total']<$minimum){
	    			$more_sub_total = $minimum - (float)$salesorder['sum_sub_total'];
	    			$sub_total = $more_sub_total;
	                $tax = $sub_total*(float)$salesorder['taxval']/100;
	                $amount = $sub_total+$tax;
	    		}
	    		$last_insert['sub_total'] += $sub_total;
	    		$last_insert['amount'] += $amount;
	    		$last_insert['tax'] += $tax;
			}
			//Last insert=======================================================
            if($last_insert['sub_total']>0){
            	$last_insert = array_merge(end($salesinvoice['products']),$last_insert);
				if(isset($last_insert['option_for']))
					unset($last_insert['option_for']);
				foreach($last_insert as $key=>$value){
	                if($key=='deleted' || $key=='sub_total' || $key=='amount' || $key=='tax') continue;
	                $last_insert[$key] = '';
	                $last_insert['xlock'][$key] = 1;
	            }
	            $last_insert['products_name'] = 'Minimum Order Adjustment';
	            $last_insert['xlock']['products_name'] = '1';
	            $last_insert['xlock']['unit_price'] = '1';
	            $last_insert['xlock']['quantity'] = '1';
	            foreach(array('sku','products_id','details','option','sizew','sizew_unit','sizeh','sizeh_unit','receipts','view_costing','sell_by','sell_price','adj_qty','oum','custom_unit_price','unit_price') as $value)
	                $last_insert['xempty'][$value] = '1';
	            $last_insert['sku_disable'] = 1;
	            $last_insert['_id'] = 'Extra_Row';
	            $last_insert['quantity'] = '1';
				$salesinvoice['products'][] = $last_insert;
			}
            //==================================================================
			$arr_sum = $this->new_cal_sum($salesinvoice['products']);
			$salesinvoice = array_merge($salesinvoice,$arr_sum);
			if(!$existed)
				$salesinvoice['salesorders'][] = new MongoId($salesorder['_id']);
			$this->Salesinvoice->save($salesinvoice);
			return true;
		}
		return false;
    }
    function build_salesorder($salesorder,&$products_num,&$options_num){
		$arr_data = array('products'=>array(),'options'=>array());
		if(!isset($salesorder['options']))
			$salesorder['options'] = array();
		if(!isset($salesorder['products']))
			$salesorder['products'] = array();
		foreach($salesorder['products'] as $product_key => $product_value){
			if(isset($product_value['deleted'])&&$product_value['deleted']) continue;
			$product_value['salesorder_id'] = new MongoId($salesorder['_id']);
			if(!isset($product_value['option_for']) || $product_value['option_for']==''){
				$arr_data['products'][$products_num] = $salesorder['products'][$product_key];
				foreach($salesorder['options'] as $option_key => $option_value){
					if(isset($option_value['deleted'])&&$option_value['deleted']) continue;
					$salesorder['options'][$option_key]['salesorder_id'] = new MongoId($salesorder['_id']);
					if(isset($option_value['line_no'])&&$option_value['line_no']==$product_key
					   &&!isset($option_value['line_no_changed'])){
						$salesorder['options'][$option_key]['line_no'] = $products_num;
						$salesorder['options'][$option_key]['line_no_changed'] = true;
					}
					if(isset($option_value['parent_line_no'])&&$option_value['parent_line_no']==$product_key
					   &&!isset($option_value['parent_line_no_changed'])){
						$salesorder['options'][$option_key]['parent_line_no'] = $products_num;
						$salesorder['options'][$option_key]['parent_line_no_changed'] = true;
					}
				}
				foreach($salesorder['products'] as $child_key => $child_value){
					if(isset($child_value['deleted'])&&$child_value['deleted']) continue;
					if(isset($child_value['option_for'])&&$child_value['option_for']==$product_key
					   &&!isset($child_value['option_for_changed'])){
						$salesorder['products'][$child_key]['option_for'] = $products_num;
						$salesorder['products'][$child_key]['option_for_changed'] = true;
					}

				}
			} else {
				$arr_data['products'][$products_num] = $salesorder['products'][$product_key];
				foreach($salesorder['options'] as $option_key => $option_value){
					if(isset($option_value['deleted'])&&$option_value['deleted']) continue;
					$salesorder['options'][$option_key]['salesorder_id'] = new MongoId($salesorder['_id']);
					if(isset($option_value['line_no'])&&$option_value['line_no']==$product_key
					   &&!isset($option_value['line_no_changed'])){
						$salesorder['options'][$option_key]['line_no'] = $products_num;
						$salesorder['options'][$option_key]['line_no_changed'] = true;
					}
				}
			}
			$products_num++;
		}
		foreach($salesorder['options'] as $option_key => $option_value){
			if(isset($option_value['deleted'])&&$option_value['deleted']) continue;
			$option_value['this_line_no'] = $options_num;
			$arr_data['options'][$options_num] = $option_value;
			$options_num++;
		}
		return $arr_data;
	}


	//Back code to data
	public function revert_so_inv(){
		$md = $this->modelName.'bk';

		$this->selectModel($md);
		$qry = $this->$md->select_all(array(
			'arr_where' => array(),
			'arr_field' => array(),
			'arr_order' => array()
		));
		$m=0;$arr_query = array();
		foreach($qry as $key=>$value){
			$arr_query[] = $value['_id'];
			$this->opm->revert_so_inv($value['_id'],$value['code']);
			echo $value['code']."--".$key."</br>";
			$m++;
		}
		echo $m;
		die;
	}
	function remove_deleted_records(){
		$modelName = $this->modelName;
		$this->selectModel($modelName);
		if($this->$modelName->remove(array('deleted'=>true)))
			echo 'Done.';
		die;
	}
	function check_company_contact(){
		$this->selectModel('Contact');
		$this->selectModel('Company');
		$query = $this->opm->select_all(array('arr_field'=>array('contact_id','company_id'),'limit'=>9999));
		echo $query->count().'<br />';
		$i = $m = 0;
		foreach($query as $value){
			$i++;
			$company_id = $value['company_id'];
			$arr_data = array('_id'=>new MongoId($value['_id']));
			if(!isset($value['contact_id']) || !is_object($value['contact_id']))
				$value['contact_id'] = '';
			if(!isset($value['company_id']) || !is_object($value['company_id']))
				$value['company_id'] = '';
			if(is_object($value['contact_id'])){
				$contact = $this->Contact->select_one(array('_id'=>new MongoId($value['contact_id'])),array('_id'));
				if(!isset($contact['_id']))
					$value['contact_id'] = '';
			}
			if(is_object($value['company_id'])){
				$company = $this->Company->select_one(array('_id'=>new MongoId($value['company_id'])),array('_id'));
				if(!isset($company['_id']))
					$value['company_id'] = '';
			}
			if($company_id != $value['company_id'])
				$m++;
			$arr_data['contact_id'] = $value['contact_id'];
			$arr_data['company_id'] = $value['company_id'];
			//$this->opm->rebuild_collection($arr_data);
		}
		echo 'Xong - '.$m.'/'.$i;
		die;
	}


	//Rebuild heading : QT,SO,INV
	public function rebuild_heading(){
		$this->selectModel('Company');
		$this->selectModel('Task');
		$m=0;

		//find result
		$query = $this->opm->select_all(array(
 			'arr_where'=>array(
							'name'=>array('$exists'=>true,'$ne'=>''),
							'heading'=>array('$in'=>array(null,''))
						)
		));

		foreach($query as $id=>$arr){
			$arr_set = array();
			//Rebuil name = code + company_name
			$name = $arr['code'];
			if(isset($arr['company_id']) && (string)$arr['company_id']!=''){
				$company = $this->Company->collection->findOne(array('_id'=>$arr['company_id']), array('name'));
				$name .= '-'.$company['name'];
			}
			//Set lai heading neu name custom
			if($arr['name']!=$name){
				$arr_set['heading'] = $arr['name'];
			}
			//name = new name
			$arr_set['name'] = $name;
			//Run change: update
			$this->opm->collection->update(
					array('_id'=>$arr['_id']),
					array( '$set' => $arr_set)
			);
			//Run change: update task
			/*if($this->name =='Salesorder'){
				$company = $this->Company->collection->findOne(array('_id'=>$arr['company_id']), array('name'));
				$this->Task->collection->update(
					array('_id'=>$arr['_id']),
					array( '$set' => $arr_set)
				);
			}*/

			echo $id.'---'.$arr['name'].'---'.$name.'<br />';
			$m++;
		}
		echo $m;die;
	}


	//Rebuild heading : QT,SO,INV,Task
	public function rebuild_heading_task(){
		$this->selectModel('Task');
		$this->selectModel('Salesorder');
		$this->selectModel('Company');
		$m=$a=0;
		//find result
		$query = $this->Task->select_all(array(
 			'arr_where'=>array(
							'salesorder_id'=>array('$exists'=>true,'$ne'=>'')
						)
		));

		foreach($query as $id=>$arr){
			$arr_set = array();
			//Rebuil name = code + company_name
			if(isset($arr['salesorder_id']) && is_object($arr['salesorder_id'])){
				$salesorder = $this->Salesorder->collection->findOne(array('_id'=>$arr['salesorder_id']), array('code','company_id'));
				$name = $salesorder['code'];

				if(isset($salesorder['company_id'])){
					$company = $this->Company->collection->findOne(array('_id'=>$salesorder['company_id']), array('name'));
					$name .= '-'.$company['name'];
				}
				if(isset($arr['our_rep_type']) && $arr['our_rep_type']=='assets')
					$name .= '-'.$arr['our_rep'];

				$arr_set['name_custom'] = $arr['name'];
				$arr_set['name'] = $name;



				//Run change: update
				$this->Task->collection->update(
						array('_id'=>$arr['_id']),
						array( '$set' => $arr_set)
				);
				echo $name.'<br />'; $a++;
			}

			$m++;
		}
		echo $a.'/'.$m;die;
	}

	//Rebuild heading : QT,SO,INV,Task
	public function revert_rebuild_heading_task(){
		$this->selectModel('Task');
		//find result
		$query = $this->Task->select_all(array(
 			'arr_where'=>array(
							'salesorder_id'=>array('$exists'=>true,'$ne'=>'')
						)
		));
		$m=$a=0;
		foreach($query as $id=>$arr){
			$arr_set = array();
			if(isset($arr['name_custom'])){
				$arr_set['name'] = $arr['name_custom'];
				//Run change: update
				$this->Task->collection->update(
						array('_id'=>$arr['_id']),
						array( '$set' => $arr_set)
				);
				echo $arr_set['name'].'<br />';
				$m++;
			}
		}
		echo $m;die;
	}


	public function change_theme($theme='blue'){
		$_SESSION['theme'] = $theme;
		$this->redirect(URL);
	}

	function test_cal_sum(){
		$query = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())),array('products'));
		$products = $query['products'];
		$count_sub_total = 0;
        $count_amount = 0;
        $count_tax = 0;
        if(!empty($products)){
            foreach($products as $pro_key=>$pro_data){
                if(!$pro_data['deleted']){
                    if(isset($pro_data['option_for'])&&is_array($products[$pro_data['option_for']])
                        && (isset($products[$pro_data['option_for']]['sell_by'])&&$products[$pro_data['option_for']]['sell_by']=='combination'
                            || (isset($pro_data['same_parent'])&&$pro_data['same_parent']>0)
                            )
                        )
                        continue;
                    if(isset($pro_data['option_for'])
                       && isset($products[$pro_data['option_for']]) && $products[$pro_data['option_for']]['deleted'])
                    	continue;
                    pr($pro_data['sub_total']);
                    //cộng dồn sub_total
                    if(isset($pro_data['sub_total']))
                        $count_sub_total += (float)$pro_data['sub_total'];
                    //cộng dồn amount
                    if(isset($pro_data['amount']))
                        $count_amount += (float)$pro_data['amount'];
                }
            }
            //tính lại sum tax
            $count_tax = $count_amount - $count_sub_total;
        }
        pr(array('sum_amount'=>$count_amount,'sum_sub_total'=>$count_sub_total,'sum_tax'=>$count_tax));die;
        return array('sum_amount'=>$count_amount,'sum_sub_total'=>$count_sub_total,'sum_tax'=>$count_tax);
	}


	//Rebuild heading : QT,SO,INV,Task
	public function rebuild_payment_due_date(){
		$this->selectModel('Salesinvoice');
		//find result
		$query = $this->Salesinvoice->select_all(array(
 			'arr_where'=>array(),
			'arr_field'=>array('code','invoice_date','payment_terms')
		));
		$m=$a=0;
		foreach($query as $id=>$arr){
			$arr_set = array();
			if(isset($arr['invoice_date'])){
				if(empty($arr['payment_terms']))
					$arr['payment_terms'] = 0;

				$arr_set['payment_due_date'] = (int)$arr['payment_terms']*86400 + (int)$arr['invoice_date']->sec;
				$arr_set['payment_due_date'] = new MongoDate($arr_set['payment_due_date']);
				$arr_set['payment_terms'] = $arr_set['payment_terms_id'] = $arr['payment_terms'];
				//Run change: update
				$this->Salesinvoice->collection->update(
						array('_id'=>$arr['_id']),
						array( '$set' => $arr_set)
				);
				echo $arr['code'].'====='.$arr['payment_terms'].'<br />';
				$m++;
			}
		}
		echo $m;die;
	}
	function rebuild_index(){
		$arr_models = array('Quotation','Salesinvoice','Salesorder');
		foreach($arr_models as $model)
			$this->selectModel($model);
		$Quotation = array('job_id','company_id','salesorder_id');
		$Salesinvoice = array('job_id','company_id','salesorder_id');
		$Salesorder = array('job_id','company_id','quotation_id');
		foreach($arr_models as $model){
			$arr_where = array();
			foreach($$model as $condition)
				$arr_where['$or'][][$condition] = array('$exists' => false);
			$query = $this->$model->select_all(array(
			                                   'arr_where' => $arr_where,
			                                   'arr_field' => $$model,
			                                   'limit'		=> 9999
			                                   ));
			$count = $query->count();
			if($count){
				echo '<br />'.$model.': '.$count.'<br />';
				$i = 0;
				foreach($query as $value){
					$arr_data = array('_id' => $value['_id']);
					foreach($$model as $index){
						if(!isset($value[$index]))
							$arr_data[$index]  = '';
					}
					if(count($arr_data)>1){
						$this->opm->rebuild_collection($arr_data);
						$i++;
					}

				}
				echo 'Done: '.$i;
			}
		}
		die;
	}

	function remove_images(){
      // Turn off output buffering
	      ini_set('output_buffering', 'off');
	      // Turn off PHP output compression
	      ini_set('zlib.output_compression', false);
	      //Flush (send) the output buffer and turn off output buffering
	      while (@ob_end_flush());

	      // Implicitly flush the buffer(s)
	      ini_set('implicit_flush', true);
	      ob_implicit_flush(true);
	      $this->selectModel('Product');
	      $path = APP.WEBROOT_DIR.DS.'upload'.DS.'2014_04';
	      $files = scandir($path);
	      $i =0;
	      $size = 0;
	      foreach($files as $file){
	       if($file == '.') continue;
	       if($file == '..') continue;
	       if(is_dir($path.DS.$file)) continue;
	       $size += filesize($path.DS.$file);
	       $origin_file =  $file;
	       $file = str_replace(array('(',')'), '.*', $file);
	       $file = new MongoRegex('/'.$file.'/i');
	       $product = $this->Product->select_one(array(
	                                             '$or'=>array(
	                                                          array('big_image'=>'upload/2014_04/'.$file),
	                                                          array('products_upload' => 'upload/2014_04/'.$file),
	                                                          array('image_url_origin' => $file),
	                                                          )
	                                             ),array('_id'));
	       if(!isset($product['_id'])){
	         unlink($path.DS.$origin_file);
		        echo $origin_file.' was removed <br />';
		        $i++;
		        @ob_flush();
		        @flush();
	       }
	      }
	      echo '<hr /><hr /><hr /><hr />'.number_format($size).' was removed!<br />';
	      echo $i.' file(s) have been removed!';
	      die;
     }
   function get_category(){
		$this->mongo_connect();
		$this->tb_nail_category = $this->db->selectCollection('tb_nail_category');
		$nail_category = $this->tb_nail_category->find(array('status'=> 1,'child_item'=>array()),
		                                               array('_id','name')
		                                              )->sort(array('name'=>1));
		return $nail_category;
	}
	function selectNailCollection($collection){
		$this->mongo_connect();
		$this->$collection = $this->db->selectCollection($collection);
	}
	function sku_without_code($category_id,&$name_after_convert){
		$category = $this->tb_nail_category->findOne(array('_id'=>new MongoId($category_id)),array('name','parent_id','is_parent'));
		$name = $this->remove_special_character($category['name']);
		$name = trim($name);
		$name = strtoupper($name);
		$name = explode(' ', $name);
		$tmp_name = '';
		foreach($name as $n){
			if($n == '')
				continue;
			$tmp_name .= $n[0];
		}
		$name_after_convert[] = $tmp_name;
		if((!isset($category['is_parent']) || $category['is_parent']!= 1) && isset($category['parent_id']) && strlen($category['parent_id']) == 24 )
			$this->sku_without_code($category['parent_id'],$name_after_convert);
		return $name_after_convert;
	}
	function get_sku_without_code(){
		if(isset($_POST)){
			//Tim Category
			$name = '';
			$this->selectNailCollection('tb_nail_category');
			$category = explode('_@_', $_POST['category']);
			$category_id = $category[0];
			$category_name = $category[1];
			$sku_without_code = $this->sku_without_code($category_id,$name);
			$sku_without_code = array_reverse($sku_without_code);
			$sku_without_code = implode($sku_without_code, '-');
			//End Tim Category
			$arr_data['sku_without_code'] = $sku_without_code;
			$arr_data['category_id'] = $category_id;
			$arr_data['category_name'] = $category_name;
			echo json_encode($arr_data);
		}
		die;
	}
	function add_product(){
		$this->layout = 'ajax';
		$folder = 'minh';
		if(isset($_POST['submit'])){
			$arr_data = array();
			$url = $_POST['url'];
			$folder = $_POST['folder'];
			$doc = new DOMDocument();
			@$doc->loadHTMLFile($url);
			$xpath = new DOMXPath($doc);
			$images = $xpath->query("//div[@id='category-product-listing']//img");
			foreach ($images as $image) {
				$name = $image->parentNode->attributes->item(1)->value;
				$name = str_replace(array('/','\\',':','*','?','<','>','|','"'), ' ', $name);
				foreach($image->attributes as $attr){
					if($attr->name != 'src') continue;
					$src = $attr->value;
					break;
				}
				$path = APP.'webroot'.DS.'upload'.DS.'test_nail'.DS.$folder;
				if(!file_exists($path))
					mkdir($path,777);
				$big_image_src = str_replace('thumbnail/middle_','',$src);
				$file_name = substr($src, strrpos($src, '/')+1);
				$file_name_ext = substr($file_name, strrpos($file_name, '.'));
				$file_name_without_ext = str_replace($file_name_ext, '', $file_name);
				file_put_contents($path.DS.$file_name_without_ext.'_thumb'.$file_name_ext, file_get_contents($src));
				file_put_contents($path.DS.$file_name, file_get_contents($big_image_src));
				//=======================================================================
				$url = $image->parentNode->attributes->item(0)->value;
				$doc = new DOMDocument();
				@$doc->loadHTMLFile($url);
				$xpath = new DOMXPath($doc);
				$head = $xpath->query("//head/script");
				$price = $head->item(12)->nodeValue;
				$null = substr($price, strpos($price, '";ITEM_IMAGE'));
				$price = str_replace(array($null,'ITEM_UNIT_PRICE = "'), '', $price);
				$arr_data['products'][] = array(
				                                'Product Name'=>$name,
				                                'Big Image'=>$file_name,
				                                'Small Image'=>$file_name_without_ext.'_thumb'.$file_name_ext,
				                                'Discount'=>$price
				                                );
			}
			$this->set('arr_data',$arr_data);
		} else if(isset($_POST['add_product'])){
			$category_id = $_POST['category_id'];
			$category_name = $_POST['category_name'];
			$sku_without_code = $_POST['sku_without_code'];
			$arr_post = $_POST;
			unset($arr_post['category_id'],$arr_post['category_name'],$arr_post['sku_without_code'],$arr_post['add_product']);
			foreach($arr_post as $data){
				$code = $this->Product->get_auto_code('code');
				$sku = $sku_without_code.'-'.$code;
				$slugger = str_replace('-', '', $sku).'-'.$code;
				$arr_save = array(
				                  'product_id'=> $this->Product->get_auto_code('product_id'),
				                  'category'=>$category_name,
				                  'category_id'=>$category_id,
				                  'code'=>$code,
				                  'name'=>$data['product_name'],
				                  'product_slide_image'=>array(
				                                               'upload/2014_04/'.$data['small_image']
				                                               ),
				                  'product_grab'=>array(
		                                               'upload/2014_04/'.$data['small_image']
		                                               ),
				                  'big_image'=>'upload/2014_04/'.$data['big_image'],
				                  'products_upload'=>'upload/2014_04/'.$data['big_image'],
				                  'sell_price'=>(float)$data['sell_price'],
				                  'discount'=>(float)$data['discount'],
				                  'sku'=>$sku,
				                  'slugger'=>$slugger,
				                  'image_url_origin'=>$data['big_image'],
				                  'product_description'=>$data['description'],
				                  'product_ingredients'=>$data['indredents']
				                  );
				$arr_save = $this->get_default_product_info($arr_save);
				$this->Product->save($arr_save);
			}
			echo 'Done';
		}
		$this->set('nail_category',$this->get_category());
		$this->set('folder',$folder);
	}
	function get_default_product_info($arr_save){
		$arr_default_before_save = array(
						  'approved' => 0,
						  'discount' => 0,
						  'assemply_item' => 0,
						  'brand_id' => '',
						  'saved_dir' => 'upload/2014_04/',
						  'category' => '',
						  'category_id' => '',
						  'check_stock_stracking' => 0,
						  'company_id' => '',
						  'company_name' => '',
						  'cost_price' => '',
						  'created_by' => '',
						  'date_modified' => new MongoDate(),
						  'deleted' => false,
						  'description' => '',
						  'group_type' => '',
						  'gst_tax' => '',
						  'in_stock' => '',
						  'is_custom_size' => 0,
						  'is_free_sample' => 0,
						  'locations' =>array(),
						  'makeup' => '',
						  'modified_by' => '',
						  'name' => '',
						  'product_description' => '',
						  'product_ingredients' => '',
						  'product_slide_image' =>array(),
						  'product_grab' =>array(),
						  'big_image' => '',
						  'on_po' => '',
						  'on_so' => '',
						  'options' =>array(),
						  'oum' => '',
						  'oum_depend' => '',
						  'parent_product_code' => '',
						  'parent_product_id' => '',
						  'parent_product_name' => '',
						  'pricebreaks' =>array( ),
						  'products_upload' => '',
						  'profit' => '',
						  'pst_tax' => '',
						  'qty_in_stock' => 0,
						  'sell_by' => 'Square feet',
						  'sell_price' => 0,
						  'sell_price_cb' => 0,
						  'sellprices' =>array(),
						  'serial' => '',
						  'sizeh' => 0,
						  'sizeh_unit' => '',
						  'sizew' => 0,
						  'sizew_unit' => '',
						  'sku' => '',
						  'slugger' => '',
						  'special_order' => '',
						  'status' => false,
						  'stocktakes' =>array(),
						  'thickness' => 0,
						  'thickness_unit' => '',
						  'image_url_origin' => '',
						  'under_over' => '',
						  'unit_price' => 0,
						  'exclusive' => 0,
						  'online_only' => 0,
						  'limited_edition' => 0,
						  'update_price_by' => '',
						  'update_price_by_id' => '',
						  'update_price_date' => '',
		                  );
		return array_merge($arr_default_before_save,$arr_save);
	}
	function remove_tmp_images(){
		if(isset($_POST)){
			$folder = $_POST['folder'];
			$path = APP.'webroot'.DS.'upload'.DS.'test_nail'.DS.$folder;
			$small_image = $_POST['small_image'];
			$big_image = $_POST['big_image'];
			if(file_exists($path.DS.$small_image))
				unlink($path.DS.$small_image);
			if(file_exists($path.DS.$big_image))
				unlink($path.DS.$big_image);
			echo 'ok';
		} die;
	}
	function remove_special_character($string){
		return trim(preg_replace('/[^a-zA-Z0-9 ]/s', '', $string));
	}


}