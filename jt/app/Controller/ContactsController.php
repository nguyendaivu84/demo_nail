<?php
App::import('Vendor', 'cal_price/cal_price');
App::uses('AppController', 'Controller');
class ContactsController extends AppController {

	var $name = 'Contacts';
	public $helpers = array();
	public $opm; //Option Module
    var $modelName = 'Contacts';
	public function beforeFilter(){
		parent::beforeFilter();
		$this->set_module_before_filter('Contact');
		$this->sub_tab_default = 'addresses ';
	}


	public function rebuild_setting($arr_setting=array()){
		// parent::rebuild_setting($arr_setting);
         $arr_setting = $this->opm->arr_settings;
        if(!$this->check_permission($this->name.'_@_entry_@_edit')){
            $arr_setting = $this->opm->set_lock(array(),'out');
            $this->set('address_lock', '1');
        }
        $this->selectModel('Contact');
        $arr_tmp = $this->Contact->select_one(array('_id'=>new MongoId($this->get_id())));

        if($arr_tmp['is_customer'] == 1 && $arr_tmp['is_employee'] == 0){
            unset($arr_setting['relationship']['personal']);
            unset($arr_setting['relationship']['rate']);
            unset($arr_setting['relationship']['expense']);
            unset($arr_setting['relationship']['leave']);
            unset($arr_setting['relationship']['working']);
            unset($arr_setting['relationship']['user_refs']);
        }

        if($arr_tmp['is_employee'] == 1 && $arr_tmp['is_customer'] == 0){
            unset($arr_setting['relationship']['product']);
            unset($arr_setting['relationship']['quote']);
            unset($arr_setting['relationship']['order']);
            unset($arr_setting['relationship']['shipping']);
            unset($arr_setting['relationship']['invoice']);
        }

        if($arr_tmp['is_employee'] == 0 && $arr_tmp['is_customer'] == 0){
            unset($arr_setting['relationship']['product']);
            unset($arr_setting['relationship']['quote']);
            unset($arr_setting['relationship']['order']);
            unset($arr_setting['relationship']['shipping']);
            unset($arr_setting['relationship']['invoice']);

            unset($arr_setting['relationship']['personal']);
            unset($arr_setting['relationship']['rate']);
            unset($arr_setting['relationship']['expense']);
            unset($arr_setting['relationship']['leave']);
            unset($arr_setting['relationship']['working']);
            unset($arr_setting['relationship']['user_refs']);
        }


        $this->opm->arr_settings = $arr_setting;
        $arr_tmp = $this->opm->arr_field_key('cls');
        $arr_link = array();
        if(!empty($arr_tmp))
            foreach($arr_tmp as $key=>$value)
                $arr_link[$value][] = $key;
        $this->set('arr_link',$arr_link);
	}

	// Add action



	//Entry - trang chi tiet
 	public function entry() {

        $arr_set = $this->opm->arr_settings;
        $arr_tmp = array();
        // Get value id
        $iditem = $this->get_id();  // lay  "_id": ObjectId("53315b02005fc3e003001226"),
        if ($iditem == '')
            $iditem = $this->get_last_id();

        $this->set('iditem', $iditem);
        //Load record by id
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if (preg_match("/_date$/", $field) && is_object($arr_tmp[$field]))
                            $arr_set['field'][$ks][$field]['default'] = date('m/d/Y', $arr_tmp[$field]->sec);
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];
                        if ($field == 'contact_name' && isset($arr_tmp['contact_last_name'])) {
                            $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                            $item_title['contact_name'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                        }
                        if ($field=='is_employee' && $arr_tmp[$field] == 1){
                            $arr_set['field']['panel_1']['code']['after_field'] = 'is_employee';
                            $arr_set['field']['panel_1']['is_employee']['type'] = 'checkbox';
                            $arr_set['field']['panel_1']['is_customer']['type'] = 'hidden';
                        }
                    }
                }
            }

            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            $this->Session->write($this->name . 'ViewId', $iditem);

            //BEGIN custom
            if (isset($arr_set['field']['panel_1']['code']['default']))
                $item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
            else
                $item_title['code'] = '1';
            $this->set('item_title', $item_title);

            $this->show_footer_info($arr_tmp);
        }else {
            $nextcode = $this->opm->get_auto_code('code');
            $arr_set['field']['panel_1']['code']['default'] = $nextcode;
            $this->set('item_title', array('code' => $nextcode));
        }

        $this->set('arr_settings', $arr_set);
        $this->sub_tab_default = 'addresses';
        $this->sub_tab('', $iditem);

         $this->set_entry_address($arr_tmp, $arr_set);
        parent::entry();
    }

    public function reload_address($address_key = '') {
        if (isset($_POST['address_key']))
            $address_key = $_POST['address_key'];
        $ids = $this->get_id();
        $arr_tmp = $this->opm->select_one(
            array('_id' => new MongoId($ids)),
            array('addresses')
        );

        $arr_temp = array();
        if (isset($arr_tmp['addresses'][0]))
            foreach ($arr_tmp['addresses'][0] as $kk => $vv) {
                $arr_temp[$kk] = $vv;
                if ($kk == 'province_state') {
                    $arr_province = $this->province();
                    if (isset($arr_province[$vv]))
                        $arr_temp[$kk] = $arr_province[$vv];
                }else if ($kk == 'country') {
                    $arr_country = $this->country();
                    if (isset($arr_country[$vv]))
                        $arr_temp[$kk] = $arr_country[$vv];
                }
            }
        echo json_encode($arr_temp);
        die;
    }

    public function set_entry_address($arr_tmp, $arr_set) {
        $address_fset = array('address_1', 'address_2', 'address_3', 'town_city', 'country', 'province_state', 'zip_postcode');
        $address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
        $address_controller = array('invoice');
        $address_value['invoice'] = array('', '', '', '', "VN", '', '');
        $this->set('address_controller', $address_controller); //set
        $address_key = array(''); // ******chu y ********

        $this->set('address_key', $address_key); //set
        $address_country = $this->country();

        // $arr_set luu nguyen mang db toan bo trang rat lon
        // $arr_tmp la 1 mang lon gom nhiu [addresses], [invoice_address] , đổ dữ liệu từ db ra là nhờ biến nay   ****************
        foreach ($address_key as $kss => $vss) {  // vss = invoice
            //neu ton tai address trong data base
            if (isset($arr_tmp['addresses'][0])) {  // Neu ton tai $arr_tmp['invoice_address'][0] sua lai $arr_tmp['addresses'][0]
                $arr_temp_op = $arr_tmp['addresses'][0];  //  $arr_temp_op là mãng quan trọng nhát : array( country=>Viet Nam, address_1=>'1 oki',....)
                for ($i = 0; $i < count($address_fset); $i++) { //loop field and set value for display
                    if (isset($arr_temp_op[$address_fset[$i]])) {
                        $address_value[$vss][$i] = $arr_temp_op[$address_fset[$i]];  // $address_value chính là giá trị trực tiếp show ra giao diện
                    } else {
                        $address_value[$vss][$i] = '';
                    }
                }

                if (isset($arr_temp_op[$vss . 'country_id']))
                    $address_province[$vss] = $this->province($arr_temp_op['country_id']); // array(California=>'California',New York =>'New York',.)
                else
                    $address_province[$vss] = $this->province();

                //set province
                if (isset($arr_temp_op[$vss . 'province_state_id']) && $arr_temp_op[$vss . 'province_state_id'] != '' && isset($address_province[$vss][$arr_temp_op[$vss . 'province_state_id']]))
                    $address_province_id[$kss] = $arr_temp_op[$vss . 'province_state_id'];
                else if (isset($arr_temp_op['province_state']))
                    $address_province_id[$kss] = $arr_temp_op['province_state'];
                else
                    $address_province_id[$kss] = '';

                //set country
                if (isset($arr_temp_op['country_id'])) {
                    $address_country_id[$kss] = $arr_temp_op['country_id'];
                    $address_province[$vss] = $this->province($arr_temp_op['country_id']);
                } else {
                    $address_country_id[$kss] = "VN";
                    $address_province[$vss] = $this->province("VN");
                }

                $address_add[$vss] = '0';
                //chua co address trong data
            } else {
                $address_country_id[$kss] = "VN";
                $address_province[$vss] = $this->province("VN");
                $address_add[$vss] = '1';
            }
        }
        $this->set('address_value', $address_value);  // $address_value chính là giá trị trực tiếp show ra giao diện
        $address_hidden_field = array('invoice_address');
        $this->set('address_hidden_field', $address_hidden_field); //set
        $address_label[0] = $arr_set['field']['panel_3']['invoice_address']['name'];
        $this->set('address_label', $address_label); //set
        $address_conner[0]['top'] = 'hgt';// fixbor';
        $address_conner[0]['bottom'] = 'jt_ppbot'; //fixbor2
        $address_conner[1]['top'] = 'hgt';
        $address_conner[1]['bottom'] = 'fixbor3 jt_ppbot';
        $this->set('address_conner', $address_conner); //set
        $this->set('address_country', $address_country); //set
        $this->set('address_country_id', $address_country_id); //set
        $this->set('address_province', $address_province); //set
        $this->set('address_province_id', $address_province_id); //set
        $this->set('address_more_line', 2); //set
        $this->set('address_onchange', "save_address_pr('\"+keys+\"');");
        if (isset($arr_tmp['company_id']) && strlen($arr_tmp['company_id']) == 24)
            $this->set('address_company_id', 'company_id');
        if (isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id']) == 24)
            $this->set('address_contact_id', 'contact_id');
        $this->set('address_add', $address_add);
    }


    public function arr_associated_data($field = '', $value = '', $valueid = '',$fieldopt='') {
        $arr_return = array();
        $arr_return[$field] = $value;
        $tmp_data = array();
        if(isset($_POST['arr']) && is_string($_POST['arr']) && $_POST['arr']!='')
            $tmp_data = (array)json_decode($_POST['arr']);
        if(isset($tmp_data['keys'])){
            if( ($tmp_data['keys']=='update' || $tmp_data['keys']=='add')
                &&!$this->check_permission($this->name.'_@_entry_@_edit')){
                echo 'You do not have permission on this action.';
                die;
            }
        }
        /**
         * Chọn Company  ***********************************************
         */
        if ($field == 'company' && $valueid != '') {
            $arr_return = array(
                'company_name' => '',
                'company_id' => '',
                'contact_name' => '',
                'contact_id' => '',
                'our_rep' => '',
                'our_rep_id' => '',
                'phone' => '',
                'email' => '',
                'addresses' => array(),
            );
            //change company
            $arr_return['company_name'] = $value;
            $arr_return['company_id'] = new MongoId($valueid);

            //find contact and more from Company
            $this->selectModel('Company');
            $arr_company = $this->Company->select_one(array('_id' => new MongoId($valueid)));

            $this->selectModel('Contact');
            $arr_contact = $arrtemp = array();
            // is set contact_default_id
            if (isset($arr_company['contact_default_id']) && is_object($arr_company['contact_default_id'])) {
                $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));

                // not set contact_default_id
            } else {
                $arr_contact = $this->Contact->select_all(array(
                    'arr_where' => array('company_id' => new MongoId($valueid)),
                    'arr_order' => array('_id' => -1),
                ));
                $arrtemp = iterator_to_array($arr_contact);
                if (count($arrtemp) > 0) {
                    $arr_contact = current($arrtemp);
                } else
                    $arr_contact = array();
            }


            //change our_rep
            if (isset($arr_company['our_rep']) && isset($arr_company['our_rep_id']) && $arr_company['our_rep_id'] != '') {
                $arr_return['our_rep_id'] = $arr_company['our_rep_id'];
                $arr_return['our_rep'] = $arr_company['our_rep'];
            }else{
                $arr_return['our_rep_id'] = $this->opm->user_id();
                $arr_return['our_rep'] = $this->opm->user_name();
            }


            //change phone
            if (isset($arr_company['phone']))
                $arr_return['company_phone'] = $arr_company['phone'];
            if (isset($arr_contact['direct_dial']))
                    $arr_return['direct_phone'] = $arr_contact['direct_dial'];
            if (isset($arr_contact['home_phone']))
                    $arr_return['home_phone'] = $arr_contact['home_phone'];
            if (isset($arr_contact['mobile']))
                    $arr_return['mobile'] = $arr_contact['mobile'];


            if (!isset($arr_contact['direct_dial']) && !isset($arr_contact['mobile']))
                    $arr_return['phone'] = '';  //bat buoc phai co dong nay khong thi no se lay du lieu cua cty truoc

            if (isset($arr_company['email']) && $arr_company['email']!='')
                $arr_return['email'] = $arr_company['email'];
            elseif (isset($arr_contact['email']))
                $arr_return['email'] = $arr_contact['email'];
            elseif (!isset($arr_contact['email']))
                $arr_return['email'] = '';

            if (isset($arr_company['fax']))
                $arr_return['fax'] = $arr_company['fax'];
            elseif (isset($arr_contact['fax']))
                $arr_return['fax'] = $arr_contact['fax'];
            elseif (!isset($arr_contact['fax']))
                $arr_return['fax'] = '';

            //change address
            if (isset($arr_company['addresses_default_key']))
                $add_default = $arr_company['addresses_default_key'];
            if (isset($add_default) && isset($arr_company['addresses'][$add_default])) {
                foreach ($arr_company['addresses'][$add_default] as $ka => $va) {
                    if ($ka != 'deleted')
                        $arr_return['addresses'][0][$ka] = $va;
                    else
                        $arr_return['addresses'][0][$ka] = $va;
                }
            }

            //change tax
            if((!isset($arr_return['invoice_address'][0]['invoice_province_state_id']) || $arr_return['invoice_address'][0]['invoice_province_state_id']=='') && isset($arr_return['invoice_address'][0]['invoice_province_state'])){
                $province_name = $arr_return['invoice_address'][0]['invoice_province_state'];
                $arr_province = $this->province_reverse('CA');
                if(isset($arr_province[$province_name]))
                    $arr_return['invoice_address'][0]['invoice_province_state_id'] = $arr_province[$province_name];
            }
        }

        return $arr_return;
    }

	//Search function
	public function entry_search() {

    }

	//Swith options function
    public function swith_options($option = ''){
        parent::swith_options($option);
    }

    function get_data_print_requirement($contact_id=''){
        $contact_id = $this->get_id();
        $this->selectModel('contact');
        $cond['_id'] = new MongoId($contact_id);
        $arr_query = $this->Contact->select_all(array(
            'arr_where' => $cond,
        ));
        $arr_tmp = array();
        $arr_tmp =$arr_query;
        $tmp = array();
        foreach($arr_tmp as $value){
            $tmp['company'] = isset($value['company'])?$value['company']:'';
            $tmp['no'] = isset($value['code'])?$value['code']:'';
            $tmp['contact_name'] = isset($value['contact_name'])?$value['contact_name']:'';
            $tmp['company_phone'] = isset($value['company_phone'])?$value['company_phone']:'';
            $tmp['date'] = isset($value['date'])?$value['date']:'';
            $tmp['status'] = isset($value['status'])?$value['status']:'';
            $tmp['rating'] = isset($value['rating'])?$value['rating']:'';
            $tmp['default_address_1'] = isset($value['default_address_1'])?$value['default_address_1']:'';
            $tmp['detail'] = isset($value['detail'])?$value['detail']:'';
        }
        return $tmp;
    }

    function print_requirements_pdf($contact_id){
        $this->layout = 'pdf';
        $date_now = date('Ymd');
        $time=time();
        $filename = 'REQ'.$date_now.$time;
        $tmp = array();
        $tmp = $this->get_data_print_requirement();
        //pr($tmp);die();
            $html='';
            $html .= ' <table cellpadding="4" cellspacing="0" class="tab_nd">';

                //$html .= '<table cellpadding="4" cellspacing="0" class="tab_nd2">';

            $html .= ' <tr class="border_2">
                <td width="9%" class="first top border_left border_btom" text-align="left">';

            if(isset($tmp['no']))
            $html .= $tmp['no'];


            $html .= '</td>
                <td width="25%" class="top border_btom border_left">';

            if(isset($tmp['company']))
            $html .= $tmp['company'];

            $html .='</td>
                <td width="23%" class="top border_btom border_left">';

            if(isset($tmp['contact_name']))
                $html .= $tmp['contact_name'];


            $html .='</td>
                <td align="left" width="13.3%" class="top border_btom border_left">
                    ';


            if(isset($tmp['company_phone']))
            $html .= $tmp['company_phone'];

            $html.='
                </td>
                <td align="left" width="12%" class="top border_btom border_left">
                    ';

            if(isset($tmp['date']))
                //pr($tmp['date']);die();
            $html .= $tmp['date'];

            $html.='
                </td>
                <td width="10%" class="end top border_btom border_left">
                    ';
            if(isset($tmp['status']))
            $html .= $tmp['status'];


            $html.='
                </td>
                <td width="7%" class="end top border_btom border_left">
                    ';


            if(isset($tmp['rating']))
            $html .= $tmp['rating'];


            $html.='
                </td>
            </tr>
        </table>
    ';


            $html .= ' <table cellpadding="4" cellspacing="0" class="tab_nd">';

                //$html .= '<table cellpadding="4" cellspacing="0" class="tab_nd2">';

            $html .= ' <tr class="border_2">
                <td width="99.5%" class="first top border_left border_btom" text-align="left"> Address: ';

            if(isset($tmp['default_address_1']))
            $html .= $tmp['default_address_1'];


            $html .= '</td> ';


            $html.='</tr></table>';


            $html .= ' <table cellpadding="4" cellspacing="0" class="tab_nd">';

                //$html .= '<table cellpadding="4" cellspacing="0" class="tab_nd2">';

            $html .= ' <tr class="border_2">
                <td width="99.5%" class="first top border_left border_btom" text-align="left"> <b>Requirements 1</b>:<br/> ';

            if(isset($tmp['detail']))
            $html .= str_replace("\n", "<br>", $tmp['detail']);


            $html .= '</td> ';


            $html.='</tr></table>';


        $html_new = $html;

        // =================================================== tao file PDF ==============================================//
        include(APP.'Vendor'.DS.'xtcpdf.php');

        $pdf = new XTCPDF();
        date_default_timezone_set('UTC');
        $pdf->today=date("g:i a, j F, Y");
        $textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Anvy Digital');
        $pdf->SetTitle('Anvy Digital Company');
        $pdf->SetSubject('Company');
        $pdf->SetKeywords('Company, PDF');

    // set default header data
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);

    // set default monospaced font
        $pdf->SetDefaultMonospacedFont(2);

    // set margins
        $pdf->SetMargins(10, 52, 10);
        $pdf->file3 = 'img'.DS.'bar_662x23.png';

        $pdf->file2_left=115;
        $pdf->file2='img'.DS.'contact_Requirements_title.png';
        $pdf->file4 = 'img'.DS.'null.png';
        $pdf->file5 = 'img'.DS.'null.png';
        $pdf->file6 = 'img'.DS.'null.png';
        $pdf->file7 = 'img'.DS.'null.png';
        $pdf->print1 = '';


        $pdf->bar_top_left=136;


        $pdf->hidden_left=153;
        $pdf->hidden_content='';

        $pdf->bar_big_content='---------------------------------------------------------------------------------------------------------------------------------------------------------------';

        $pdf->bar_words_content='Ref no    Company                               Contact                                 Phone              Date              Status         Rating ';
        $pdf->bar_mid_content='         |                                                    |                               |                         |                          |              |';


        $pdf->printedat_left=134;
        $pdf->time_left=152;

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 30);

    // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).DS.'lang'.DS.'eng.php')) {
            require_once(dirname(__FILE__).DS.'lang'.DS.'eng.php');
            $pdf->setLanguageArray($l);
        }

    // ---------------------------------------------------------
    // set font
        $pdf->SetFont($textfont, '', 9);

    // add a page
        $pdf->AddPage();
        $pdf->SetMargins(10, 19, 10);

        $pdf->file1 = 'img'.DS.'null.png';
        $pdf->file2 = 'img'.DS.'null.png';
        $pdf->file4 = 'img'.DS.'null.png';
        $pdf->file5 = 'img'.DS.'null.png';
        $pdf->file6 = 'img'.DS.'null.png';
        $pdf->file7 = 'img'.DS.'null.png';
        $pdf->file3_top=10;
        $pdf->bar_words_top=11;
        $pdf->bar_mid_top=10.6;
        $pdf->hidden_content='';
        $pdf->bar_top_content='';
        $pdf->today='';
        $pdf->print='';

        $html='
                <style>
                    table{
                        font-size: 12px;
                        font-family: arial;
                    }
                    td.first{
                        border-left:1px solid #e5e4e3;
                    }
                    td.end{
                        border-right:1px solid #e5e4e3;
                    }
                    td.top{
                        color:#fff;
                        font-weight:bold;
                        background-color:#911b12;
                        border-top:1px solid #e5e4e3;
                    }
                    td.bottom{
                        border-bottom:1px solid #e5e4e3;
                    }
                    .option{
                        color: #3d3d3d;
                        font-weight:bold;
                        font-size:18px;
                        text-align: center;
                        width:100%;
                    }
                    .border_left{
                        border-left:1px solid #A84C45;
                    }
                    .border_1{
                        border-bottom:1px solid #911b12;
                    }

                </style>
                <style>
                        table.tab_nd{
                            font-size: 12px;
                            font-family: arial;
                        }
                        table.tab_nd td.first{
                            border-left:1px solid #e5e4e3;
                        }
                        table.tab_nd td.end{
                            border-right:1px solid #e5e4e3;
                        }
                        table.tab_nd td.top{
                            background-color:#FDFBF9;
                            border-top:1px solid #e5e4e3;
                            font-weight: normal;
                            color: #3E3D3D;
                        }
                        table.tab_nd .border_2{
                            border-bottom:1px solid red;
                        }
                        table.tab_nd .border_left{
                            border-left:1px solid #E5E4E3;
                            border-bottom:1px solid #E5E4E3;
                        }
                        table.tab_nd .border_btom{
                            border-bottom:1px solid #E5E4E3;
                        }

                    </style>
                    <style>
                            table.tab_nd2{
                                font-size: 12px;
                                font-family: arial;
                            }
                            table.tab_nd2 td.first{
                                border-left:1px solid #e5e4e3;
                            }
                            table.tab_nd2 td.end{
                                border-right:1px solid #e5e4e3;
                            }
                            table.tab_nd2 td.top{
                                background-color:#EDEDED;
                                border-top:1px solid #e5e4e3;
                                font-weight: normal;
                                color: #3E3D3D;
                            }
                            table.tab_nd2 .border_2{
                                border-bottom:1px solid red;
                            }
                            table.tab_nd2 .border_left{
                                border-left:1px solid #E5E4E3;
                                border-bottom:1px solid #E5E4E3;
                            }
                            table.tab_nd2 .border_btom{
                                border-bottom:1px solid #E5E4E3;
                            }
                            .size_font{
                                font-size: 12px !important;
                            }

                        </style>
            ';
        $html.=$html_new;
        $html .= '


        <div style=" clear:both; color: #c9c9c9;"><br />
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        </div><br />
        ';
        $pdf->writeHTML($html, true, false, true, true, '');
        $pdf->Output(APP. 'webroot'.DS. 'upload'.DS .$filename.'.pdf', 'F');
        $this->redirect('/upload/'. $filename .'.pdf');
        die;
    }





    //////////////////////////////////////////////14-4-2014////////////////////////////////////////////////////////////
    function addresses(){
        $this->selectModel('Contact');
        $contact = $this->Contact->select_one(array('_id'=> new MongoId($this->get_id())),array('addresses'));
        $this->selectModel('Province');
        $arr_all_province = array();
        $subdatas = array();
        if(!isset($contact['addresses']))
            $contact['addresses'] = array();
        $subdatas['addresses'] = $contact['addresses'];
        $this->set_select_data_list('relationship','addresses');
        $this->set('subdatas', $subdatas);
    }

    function addresses_add($contact_id) {
        $this->selectModel('Contact');
        $this->Contact->collection->update(
                array('_id' => new MongoId($contact_id)), array('$push' => array(
                'addresses' => array(
                    'name' => '',
                    'default' => false,
                    'address_1' => '',
                    'address_2' => '',
                    'address_3' => '',
                    'town_city' => '',
                    'zip_postcode' => '',
                    'province_state' => '',
                    'province_state_id' => '',
                    'country' => 'Canada',
                    'country_id' => "CA",
                    'deleted' => false
                )))
        );
        $this->addresses($contact_id);
        $this->render('addresses');
    }

    function enquiries($contact_id){
        $this->selectModel('Enquiry');
        $arr_enquiries = $this->Enquiry->select_all(array(
                                                    'arr_where' => array('contact_id'=>new MongoId($contact_id))
                                                    ));
        $arr_tmp = array();
        foreach($arr_enquiries as $key => $value){
            $arr_tmp[$key] = $value;
        }
        //pr($arr_tmp);die;
        $subdatas = array();
        $subdatas['enquiries'] = $arr_tmp;
        $this->set('subdatas', $subdatas);
    }

    function enquiries_add($contact_id) {
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        $this->selectModel('Enquiry');
        $arr_tmp = $this->Enquiry->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save = $this->arr_associated_data('contact_name',$arr_contact['name'], $contact_id);
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }

        $key = 0;
        if(isset($this->data['Contact']['addresses_default_key']))
            $key = $this->data['Contact']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Contact']['addresses'][$key]['country_id']))
            $address = $this->data['Contact']['addresses'][$key]['country_id'];

        $key = isset($arr_contact['addresses_default_key'])?$arr_contact['addresses_default_key']:0;
        $arr_save['date']= new MongoDate(time());
        $arr_save['status']='Hot';
        $arr_save['default_country'] = isset($arr_contact['addresses'][$key]['country'])?$arr_contact['addresses'][$key]['country']:'';
        $arr_save['default_country_id'] = isset($arr_contact['addresses'][$key]['country_id'])?$arr_contact['addresses'][$key]['country_id']:0;
        $arr_save['default_province_state'] = isset($arr_contact['addresses'][$key]['province_state'])?$arr_contact['addresses'][$key]['province_state']:'';
        $arr_save['default_province_state_id'] = isset($arr_contact['addresses'][$key]['province_state_id'])?$arr_contact['addresses'][$key]['province_state_id']:'';
        $arr_save['default_address_1'] = isset($arr_contact['addresses'][$key]['address_1'])?$arr_contact['addresses'][$key]['address_1']:'';
        $arr_save['default_address_2'] = isset($arr_contact['addresses'][$key]['address_2'])?$arr_contact['addresses'][$key]['address_2']:'';
        $arr_save['default_address_3'] = isset($arr_contact['addresses'][$key]['address_3'])?$arr_contact['addresses'][$key]['address_3']:'';
        $arr_save['default_town_city'] = isset($arr_contact['addresses'][$key]['town_city'])?$arr_contact['addresses'][$key]['town_city']:'';
        $arr_save['default_zip_postcode'] = isset($arr_contact['addresses'][$key]['zip_postcode'])?$arr_contact['addresses'][$key]['zip_postcode']:'';
        $arr_save['contact_phone'] = isset($arr_contact['phone'])?$arr_contact['phone']:'';
        $arr_save['contact_fax'] = isset($arr_contact['fax'])?$arr_contact['fax']:'';
        $arr_save['contact_email'] = isset($arr_contact['email'])?$arr_contact['email']:'';
        $arr_save['web'] = isset($arr_contact['web'])?$arr_contact['web']:'';
        $arr_save['contact'] = isset($arr_contact['name'])?$arr_contact['name']:'';

        //   14/4/2014
        $arr_save['contact_name'] = isset($arr_contact['name'])?$arr_contact['name']:'';
        $arr_save['contact_id'] = $arr_contact['_id'];
        $arr_save['contact_phone'] = isset($arr_contact['phone'])?$arr_contact['phone']:'';
        //   14/4/2014

        $this->Enquiry->arr_default_before_save = $arr_save;
        if ($this->Enquiry->add())
           echo URL.'/enquiries/entry/' . $this->Enquiry->mongo_id_after_save;
        else
            echo URL.'/enquiries/entry';
        die;
    }

    function jobs($contact_id){
        $this->selectModel('Job');
        $arr_job = $this->Job->select_all(array(
                                          'arr_where'=>array('contact_id'=>new MongoId($contact_id))
                                          ));
        $arr_tmp = array();
        foreach($arr_job as $key => $value){
            $arr_tmp[$key] = $value;
        }

        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id'=> new MongoId($contact_id)),array('markup_rate','rate_per_hour'));
        $subdatas = array();
        $subdatas['jobs'] = $arr_tmp;
        $subdatas['default'] = $arr_contact;
        $this->set('subdatas', $subdatas);
    }

    function jobs_add($contact_id) {
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        $this->selectModel('Job');
        $arr_tmp = $this->Job->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }
        $arr_save['status_id'] = 0;
        $arr_save['name'] = '';
        $arr_save['contacts_default_key'] = 0;
        $arr_save['contacts'][] = array(
            "contact_name" => $_SESSION['arr_user']['contact_name'],
            "contact_id" => $_SESSION['arr_user']['contact_id'],
            "default" => true,
            "deleted" => false
        );
        $arr_save['type'] = '';
        $arr_save['status'] = 'New';

        $arr_save['contact_name'] = isset($arr_contact['name'])?$arr_contact['name']:'';
        $arr_save['contact_id'] = $arr_contact['_id'];
        $arr_save['contact_phone'] = isset($arr_contact['phone'])?$arr_contact['phone']:'';
        $arr_save['fax'] = isset($arr_contact['fax'])?$arr_contact['fax']:'';
        $arr_save['email']=isset($arr_contact['email'])?$arr_contact['email']:'';

        if (isset($arr_contact['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_contact['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];

            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];

            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['direct_phone'] = $arr_contact['direct_dial'];

            if(isset($arr_contact['mobile'])&&$arr_contact['mobile']!='')
                $arr_save['mobile'] = $arr_contact['mobile'];
        }
        else{
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('contact_id'=>new MongoId($contact_id)),
                'arr_order' => array('_id'=>-1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arrtemp)>0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
            if(isset($arr_contact['_id'])){
                $arr_save['contact_id'] = $arr_contact['_id'];
                $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            }

            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];


            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['direct_phone'] = $arr_contact['direct_dial'];

            if(isset($arr_contact['mobile'])&&$arr_contact['mobile']!='')
                $arr_save['mobile'] = $arr_contact['mobile'];
        }
        if (isset($work_start_sec) && $work_start_sec > 0) {
            $arr_save['work_end'] = $arr_save['work_start'] = new MongoDate($work_start_sec);
        } else {
            $arr_save['work_end'] = $arr_save['work_start'] = new MongoDate(strtotime(date('Y-m-d H:00:00')) + 3600);
        }

        $this->Job->arr_default_before_save = $arr_save;
        if ($this->Job->add()) {
          echo URL .'/jobs/entry/'. $this->Job->mongo_id_after_save;
        }
        else
          echo URL . '/jobs/entry';
        die;
    }
    ////////////////////////////////////////////14-4-2014////////////////////////////////////////////////////////////////////////



    function task_add($contact_id) {

        //$this->selectModel('Company');
        //$arr_contact = $this->Company->select_one(array('_id' => new MongoId($contact_id)));

        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id))); // oki

        $this->selectModel('Task');
        $arr_tmp = $this->Task->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }
        $arr_save['status_id'] = 0;
        $arr_save['name'] = '';
        $arr_save['contact_name'] = '';
        $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
        $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        $arr_save['work_start'] = new MongoDate(strtotime(date('Y-m-d' . ' 08:00:00')));
        $arr_save['work_end'] = new MongoDate(strtotime(date('Y-m-d') . ' 09:00:00'));
        $arr_save['contact_no'] = $arr_contact['code'];
        $arr_save['contact_id'] = $arr_contact['_id'];   // oki


        if(isset( $arr_contact['company']) && isset($arr_contact['company_id']) ) {

            $arr_save['company_id'] = $arr_contact['company_id'];

            $arr_save['contact_name'] = $arr_contact['company'];
            $arr_save['company_name'] = $arr_contact['company'];
        }


        //pr($arr_contact);die();
        // pr($arr_save);die();

        $this->Task->arr_default_before_save = $arr_save;
        if ($this->Task->add()) {
            $this->redirect('/tasks/entry/' . $this->Task->mongo_id_after_save);
        }
        $this->redirect('/companies/entry/' . $contact_id);
    }
    public function task() {
        $subdatas['task'] = array();
        $iditem = $this->get_id();
        $this->selectModel('Task');
        $arr_task = $this->Task->select_all(array(
            'arr_where' => array('contact_id' => new MongoId($iditem)),
            'arr_order' => array('_id' => -1)
        ));
        $arr = array();
        foreach ($arr_task as $key => $value) {
            $arr[$key] = $value;
        }
        $subdatas['task'] =  $arr;
        $this->set('subdatas', $subdatas);
    }

    public function personal() {
        $contact_id = $this->get_id();

        $subdatas['emergency'] = $this->get_option_data('emergency');

        $arr_options_custom = $this->set_select_data_list('relationship', 'personal');
        $this->set('arr_options_custom', $arr_options_custom);

        $arr_exp_profile = array();
        $arr_exp_profile = $this->Contact->select_one(array('_id'=>new MongoID($contact_id)),array('status','ssn_no','start_date','finish_date','weeks_worked'));
        $subdatas['profile'] = $arr_exp_profile;


        $this->set('subdatas', $subdatas);
        //hoang vu 3/4/2014
    }

    public function rate() {
        $contact_id = $this->get_id();

        $arr_options_custom = $this->set_select_data_list('relationship', 'rate');
        $this->set('arr_options_custom', $arr_options_custom);

        $arr_exp_profile = array();
        $arr_exp_profile = $this->Contact->select_one(array('_id'=>new MongoID($contact_id)),array('employee_type','employment_type','work_type','paid_by','overtime_starts_at','overtime_ends_at'));
        $subdatas['profile'] = $arr_exp_profile;

        $subdatas['review'] = $this->get_option_data('review');


        $this->set('subdatas', $subdatas);
        //hoang vu 3/4/2014
    }

    public function expense() {
        $contact_id = $this->get_id();

        $arr_options_custom = $this->set_select_data_list('relationship', 'expense');
        $this->set('arr_options_custom', $arr_options_custom);


        $subdatas['expenses'] = $this->get_option_data('expenses');

        $this->set('subdatas', $subdatas);
        //hoang vu 3/4/2014
    }

    public function leave() {
        $contact_id = $this->get_id();

        $arr_options_custom = $this->set_select_data_list('relationship', 'leave');
        $this->set('arr_options_custom', $arr_options_custom);  // show danh sach Select
        //pr($arr_options_custom);die;


        $subdatas['leave'] = $this->get_option_data('leave');
        $subdatas['accummulated'] = $this->get_option_data('accummulated');

        $this->set('subdatas', $subdatas);
        //hoang vu 3/4/2014
    }

    public function working() {
        $contact_id = $this->get_id();

        $subdatas['working_hour'] = $this->get_option_data('working_hour');

        $subdatas['working_hour']['0']['day'] = 'Monday';
        $subdatas['working_hour']['1']['day'] = 'Tuesday';
        $subdatas['working_hour']['2']['day'] = 'Wednesday';
        $subdatas['working_hour']['3']['day'] = 'Thursday';
        $subdatas['working_hour']['4']['day'] = 'Friday';
        $subdatas['working_hour']['5']['day'] = 'Saturday';
        $subdatas['working_hour']['6']['day'] = 'Sunday';

         $subdatas['working_hour']['0']['time1'] = '';




        $this->set('subdatas', $subdatas);
        //hoang vu 3/4/2014
    }


    public function product() {
        $subdatas['pricing_category'] = array();
        $subdatas['product'] = array();
        $subdatas['units'] = array();
        $contact_id = $this->get_id();
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id'=>new MongoId($contact_id)));
        if (isset($arr_contact['pricing'])) {
              $arr_pro_contact = $arr_contact['pricing'];
        } else {
             $arr_pro_contact = array();
        }
        foreach ($arr_pro_contact as $key => $value) {
            foreach ($value['price_break'] as $key2 => $value2) {
                 $arr_pro_contact[$key]['range'] = $value2['range_from'].' - '.$value2['range_to'];
                 $arr_pro_contact[$key]['unit_price'] = $value2['unit_price'];
             }
        }
        //pr($arr_contact['pricing']);die;
        //pr($arr_pro_contact);die;

        $arr = array();
        $subdatas['product'] = $arr_pro_contact ;
        $this->set('subdatas', $subdatas);
    }
    function products_pricing($contact_id, $key = 0, $product_id=''){
        $this->selectModel('Contact');
        $arr_save = $this->Contact->select_one(array('_id'=> new MongoId($contact_id)));
        //pr($arr_save);die;

        if($product_id !=''){
            $this->selectModel('Product');
            $arr_product = $this->Product->select_one(array('_id' => new MongoId($product_id)), array('_id','name','code'));
            $arr_product['product_id'] = $arr_product['_id'];
            unset($arr_product['_id']);
            $arr_product['notes']='';
            $arr_product['deleted'] = false;
            $arr_product['price_break'] = array();

            $check_not_exist = true;
            if(isset($arr_save['pricing'])){
                foreach($arr_save['pricing'] as $pricing_key => $value){
                    if((string)$value['product_id']==$product_id){
                        $check_not_exist = false;
                        $key = $pricing_key;
                        break;
                    }
                }
            }
            //neu chua ton tai thi add vao contact
            if($check_not_exist){
                $arr_save['pricing'][] = $arr_product;
                if(!$this->Contact->save($arr_save)){
                    echo 'Error: ' .$this->Contact->arr_errors_save[1];die;
                }
            }else{
                $key = count($arr_save['pricing']) - 1;
                $arr_product = $arr_save['pricing'][$key];
            }
            $key = count($arr_save['pricing']) - 1;
        }
        else{
            $arr_product = $arr_save['pricing'][$key];
        }
        //pr($arr_product);die;
        $this->set('company_pricing', $arr_save);
        $this->set('arr_product', $arr_product);
        $this->set('contact_id', $contact_id);
        $this->set('key', $key);

        $this->set('return_mod', 1);
        $this->set('return_link', URL.'/contacts/entry/'.$contact_id);
        $this->set('return_title', 'Pricing: ');
    }
    function products_price_break_add($contact_id, $key){
        $this->set('contact_id', $contact_id);
        $this->set('key', $key);

        $this->selectModel('Contact');
        $arr_save = $this->Contact->select_one(array('_id'=> new MongoId($contact_id)));
        $arr_save['pricing'][$key]['price_break'][] = array(
            'deleted' => false,
            'range_from' => '',
            'range_to' => '',
            'unit_price' =>''
        );
        if($this->Contact->save($arr_save)){
        $this->set('arr_product', $arr_save['pricing'][$key]);
            $this->render('products_price_break');
        } else {
            echo 'Error: ' . $this->Company->arr_errors_save[1]; die;
        }
    }
    function product_pricebreak_save($contact_id, $key, $price_break_key){
        if( !empty( $_POST ) ){
            $this->selectModel('Contact');
            $arr_save = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));

            $field = str_replace(array('data[', ']'), '', $_POST['field']);

            $ext = '';
            $value = $_POST['value'];

            if( $field == 'unit_price' ){
                if( substr($value, -3, 1) == '.' ){
                    $ext = substr($value, -3);
                    $value = substr($value, 0, -3);
                }
                $value = (float)(str_replace(array(',', '.'), '', $value).$ext);
            }

            $arr_save['pricing'][$key]['price_break'][$price_break_key][$field] = $value;
            if ($this->Contact->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Contact->arr_errors_save[1];
            }
        }
        die;
    }
    function products_pricebreak_delete($contact_id, $key, $price_break_key){
        $this->selectModel('Contact');
        $arr_save = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        $arr_save['pricing'][$key]['price_break'][$price_break_key]['deleted'] = true;
        if ($this->Contact->save($arr_save)) {
            echo 'ok';
        } else {
            echo 'Error: ' . $this->Contact->arr_errors_save[1];
        }
        die;
    }
    function products_save_notes($contact_id, $key){
        if( !empty( $_POST ) ){
            $this->selectModel('Contact');
            $arr_save = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
            $arr_save['pricing'][$key]['notes'] = $_POST['notes'];
            if ($this->Contact->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Contact->arr_errors_save[1];
            }
        }
        die;
    }


    public function quote() {
        $subdatas['quote'] = array();
        $iditem = $this->get_id();
        $this->selectModel('Quotation');
        $arr_quote = $this->Quotation->select_all(array(
            'arr_where' => array('contact_id' => new MongoId($iditem)),
            'arr_order' => array('_id' => -1),
            'arr_field' => array('sum_sub_total','sum_amount','sum_tax','quotation_type','quotation_status','our_rep','code','quotation_date','payment_due_date','taxval','tax','name')
        ));


        // Hoang Vu 3/4/2013
        $arr_quote = iterator_to_array($arr_quote);
        foreach($arr_quote as $key=>$quote){
            $minimum = $this->get_minimum_order('Quotation',$quote['_id']);
            if($quote['sum_sub_total']<$minimum){
                $more_sub_total = $minimum - (float)$quote['sum_sub_total'];
                $sub_total = $more_sub_total;
                $tax = $sub_total*(float)$quote['taxval']/100;
                $amount = $sub_total+$tax;
                $arr_quote[$key]['sum_sub_total'] += $sub_total;
                $arr_quote[$key]['sum_amount'] += $amount;
                $arr_quote[$key]['sum_tax'] = $arr_quote[$key]['sum_amount']-$arr_quote[$key]['sum_sub_total'];
            }
        }
        $this->selectModel('Tax');
        $arr_tax = $this->Tax->tax_select_list();
        $this->set('arr_tax',$arr_tax);
        $this->set('arr_quotation', $arr_quote);
       /* foreach($arr_quote as $key=>$quote){
            if ($arr_quote[$key]['sum_tax'] == 0)
                $arr_quote[$key]['sum_tax'] = $arr_quote[$key]['sum_tax'].'% (No tax)';
        }*/
        //pr($arr_quote);die();
        $this->set('contact_id', $iditem);
        // Hoang Vu 3/4/2013
        $subdatas['quote'] = $arr_quote;
        $this->set('subdatas', $subdatas);
    }
    function quotes_add($contact_id) {
        $this->selectModel('Quotation');
        $arr_tmp=array();
        $arr_tmp = $this->Quotation->select_one(array(), array(), array('code' => -1));
        $arr_save = array();
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $this->selectModel('contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        //pr($arr_contact);die();

        $arr_save['contact_id'] = $arr_contact['_id'];
        $arr_save['company_name'] = isset($arr_contact['company'])?$arr_contact['company']:'';
        $arr_save['company_id'] = isset($arr_contact['company_id'])?$arr_contact['company_id']:'';
        $arr_save['email'] = isset($arr_contact['email'])?$arr_contact['email']:'';
        $arr_save['phone'] = isset($arr_contact['company_phone'])?$arr_contact['company_phone']:'';
        $arr_invoice_addrress['invoice_name'] = '';
        $arr_invoice_addrress['deleted'] = false;

        $arr_save['invoice_address'][0] = (object)$arr_invoice_addrress;
        $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
        $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);

        if(isset($arr_contact['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_contact['our_rep'])?$arr_contact['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_contact['our_rep_id']);
        }else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }
        $arr_save['quotation_date'] = new MongoDate(strtotime(date('Y-m-d')));
        $arr_save['quotation_type'] = 'Quotation';
        $arr_save['tax'] = 'AB';
        //pr($arr_save);die();
        $this->Quotation->arr_default_before_save = $arr_save;
        if ($this->Quotation->add()) {
            $this->redirect('/quotations/entry/'. $this->Quotation->mongo_id_after_save);
        }
        $this->redirect('/quotations/entry');
    }


    function orders_add($contact_id) {
        $this->selectModel('Salesorder');
        $arr_tmp=array();
        $arr_tmp = $this->Salesorder->select_one(array(), array(), array('code' => -1));
        $arr_save = array();
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $this->selectModel('contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        //pr($arr_contact);die();

        $arr_save['contact_id'] = $arr_contact['_id'];
       /* $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
        $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);*/

        if(isset($arr_contact['assign_to'])){
            $arr_save['our_rep'] = isset($arr_contact['assign_to'])?$arr_contact['assign_to']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_contact['our_rep_id']);
        }else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }
        $arr_save['salesorder_date'] = new MongoDate(strtotime(date('Y-m-d')));
        //$arr_save['payment_due_date'] = new MongoDate(strtotime(date('Y-m-d')));
        //pr($arr_save);die();
        $this->Salesorder->arr_default_before_save = $arr_save;
        if ($this->Salesorder->add()) {
            $this->redirect('/salesorders/entry/'. $this->Salesorder->mongo_id_after_save);
        }
        $this->redirect('/salesorders/entry');
    }
    public function order() {
        $subdatas['order'] = array();
        $contact_id = $this->get_id();
        $this->selectModel('Salesorder');
        $arr_orders = $this->Salesorder->select_all(array(
            'arr_where' => array('contact_id' => new MongoId($contact_id)),
            'arr_order' => array('_id' => -1)
        ));
        $arr = array();
        foreach ($arr_orders as $key => $value) {
            $arr[$key] = $value;
        }

        //hoang vu 3/4/2014
        $arr_salesorder = $this->Salesorder->select_all(array(
            'arr_where' => array('contact_id' => new MongoId($contact_id)),
            'arr_field' => array('sum_sub_total','sum_amount','sum_tax','status','our_rep','code','salesorder_date','payment_due_date','taxval','tax','name','other_comment')
        ));
        $arr_salesorder = iterator_to_array($arr_salesorder);
        foreach($arr_salesorder as $key=>$order){
            $minimum = $this->get_minimum_order('Salesorder',$order['_id']);
            if($order['sum_sub_total']<$minimum){
                $more_sub_total = $minimum - (float)$order['sum_sub_total'];
                $sub_total = $more_sub_total;
                $tax = $sub_total*(float)$order['taxval']/100;
                $amount = $sub_total+$tax;
                $arr_salesorder[$key]['sum_sub_total'] += $sub_total;
                $arr_salesorder[$key]['sum_amount'] += $amount;
                $arr_salesorder[$key]['sum_tax'] = $arr_salesorder[$key]['sum_amount']-$arr_salesorder[$key]['sum_sub_total'];
            }
        }
        $this->set('arr_salesorder', $arr_salesorder);
        //pr($arr_salesorder);die();
        //hoang vu 3/4/2014


        $subdatas['order'] = $arr;
        $this->set('subdatas', $subdatas);
    }



    public function shipping() {
        $subdatas['ship'] = array();
        $iditem = $this->get_id();
        $this->selectModel('Shipping');
        $arr_shipping = $this->Shipping->select_all(array(
            'arr_where' => array('contact_id' => new MongoId($iditem)),
            'arr_order' => array('_id' => -1)
        ));
        $arr = array();
        foreach ($arr_shipping as $key => $value) {
            $arr[$key] = $value;
        }

        $subdatas['ship'] = $arr;
        $this->set('subdatas', $subdatas);
    }
    function shippings_add($contact_id) {
        $this->selectModel('Shipping');
        $arr_tmp=array();
        $arr_tmp = $this->Shipping->select_one(array(), array(), array('code' => -1));
        $arr_save = array();
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $this->selectModel('contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        $arr_save['contact_id'] = $arr_contact['_id'];


        if(isset($arr_contact['assign_to'])){
            $arr_save['our_rep'] = isset($arr_contact['assign_to'])?$arr_contact['assign_to']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_contact['our_rep_id']);
        }else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }
        $arr_save['salesorder_date'] = new MongoDate(strtotime(date('Y-m-d')));

        $this->Shipping->arr_default_before_save = $arr_save;
        if ($this->Shipping->add()) {
            $this->redirect('/shippings/entry/'. $this->Shipping->mongo_id_after_save);
        }
        $this->redirect('/shippings/entry');
    }



    public function invoice() {
        $subdatas['overall'] = array();
        $subdatas['sale_invoice'] = array();
        $contact_id = $this->get_id();
        $this->selectModel('Salesinvoice');


        //hoang vu 3/4/2014
        $arr_invoices = $this->Salesinvoice->select_all(array(
            'arr_where' => array('contact_id' => new MongoId($contact_id)),
            'arr_field' => array('sum_sub_total','sum_amount','sum_tax','invoice_type','code','invoice_date','invoice_status','our_rep','other_comment','taxval')
        ));
        $arr_invoices = iterator_to_array($arr_invoices);
        foreach($arr_invoices as $key=>$invoice){
            $minimum = $this->get_minimum_order('Salesinvoice',$invoice['_id']);
            if($invoice['sum_sub_total']<$minimum){
                $more_sub_total = $minimum - (float)$invoice['sum_sub_total'];
                $sub_total = $more_sub_total;
                $tax = $sub_total*(float)$invoice['taxval']/100;
                $amount = $sub_total+$tax;
                $arr_invoices[$key]['sum_sub_total'] += $sub_total;
                $arr_invoices[$key]['sum_amount'] += $amount;
                $arr_invoices[$key]['sum_tax'] = $arr_invoices[$key]['sum_amount']-$arr_invoices[$key]['sum_sub_total'];
            }
        }
        //pr($arr_invoices);die();
        $this->set('arr_invoices', $arr_invoices);
        $subdatas['sale_invoice'] = $arr_invoices;
        $this->set('subdatas', $subdatas);
        //hoang vu 3/4/2014
    }
    function invoices_add($contact_id) {
        $this->selectModel('Salesinvoice');
        $arr_tmp=array();
        $arr_tmp = $this->Salesinvoice->select_one(array(), array(), array('code' => -1));
        $arr_save = array();
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $this->selectModel('contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        $arr_save['contact_id'] = $arr_contact['_id'];


        if(isset($arr_contact['assign_to'])){
            $arr_save['our_rep'] = isset($arr_contact['assign_to'])?$arr_contact['assign_to']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_contact['our_rep_id']);
        }else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }
        $arr_save['salesorder_date'] = new MongoDate(strtotime(date('Y-m-d')));

        $this->Salesinvoice->arr_default_before_save = $arr_save;
        if ($this->Salesinvoice->add()) {
            $this->redirect('/salesinvoices/entry/'. $this->Salesinvoice->mongo_id_after_save);
        }
        $this->redirect('/salesinvoices/entry');
    }


    function other($contact_id){
        $this->selectModel('Contact');
        $arr_contact = array();
        $arr_contact = $this->Contact->select_one(array('_id'=>new MongoID($contact_id)),array('other'));
        $this->set_select_data_list('relationship','other');
        $subdatas = array();

        if ( isset($arr_contact['other']) )
            $subdatas['other_detail'] = $arr_contact['other'];
        else
            $subdatas['other_detail'] = array();

        $subdatas['2'] = array();
        $subdatas['3'] = array();
        $this->set('subdatas', $subdatas);
    }

    function other_add($contact_id) {
        $this->selectModel('Contact');
        $this->Contact->collection->update(
                array('_id' => new MongoId($contact_id)), array(
                    '$push' => array(
                        'other' => array(
                            'heading' => '',
                            'details' => '',
                            'deleted' => false
                        )
                    )
                )
        );
        $this->other($contact_id);
        $this->render('other');
    }



	// Popup form orther module
    public function popup($key = '') {

        $this->set('key', $key);

        if( $key == '_assets' ){
            $this->selectModel('Equipment');
            $arr_equipment = $this->Equipment->select_all(array('arr_order' => array('name' => 1)));
            $this->set( 'arr_equipment', $arr_equipment );
        }

        $limit = 100; $skip = 0; $cond = array();

        // Nếu là search GET
        if (!empty($_GET)) {

            $tmp = $this->data;

            if (isset($_GET['company_id'])) {
                $cond['company_id'] = new MongoId($_GET['company_id']);
                $tmp['Contact']['company'] = $_GET['company_name'];
            }

            if (isset($_GET['is_customer'])) {
                $cond['is_customer'] = 1;
                $tmp['Contact']['is_customer'] = 1;
            }

            if (isset($_GET['is_employee'])) {
                $cond['is_employee'] = 1;
                $tmp['Contact']['is_employee'] = 1;
            }

            $this->data = $tmp;
        }

        // Nếu là search theo phân trang
        $page_num = 1;
        if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0 ){

            // $limit = $_POST['pagination']['page-list'];
            $page_num = $_POST['pagination']['page-num'];
            $limit = $_POST['pagination']['page-list'];
            $skip = $limit*($page_num - 1);
        }
        $this->set('page_num', $page_num);
        $this->set('limit', $limit);

        $arr_order = array('first_name' => 1);
        if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
            $sort_type = 1;
            if( $_POST['sort']['type'] == 'desc' ){
                $sort_type = -1;
            }
            $arr_order = array($_POST['sort']['field'] => $sort_type);

            $this->set('sort_field', $_POST['sort']['field']);
            $this->set('sort_type', ($sort_type === 1)?'asc':'desc');
            $this->set('sort_type_change', ($sort_type === 1)?'desc':'asc');
        }

        // search theo submit $_POST kèm điều kiện
        $cond['inactive'] = 0;
        if (!empty($this->data) && !empty($_POST)) {
            $arr_post = $this->data['Contact'];

            if (isset($arr_post['name']) && strlen($arr_post['name']) > 0) {
                $cond['full_name'] = new MongoRegex('/' . trim($arr_post['name']) . '/i');
            }

            if (strlen($arr_post['company']) > 0) {
                $cond['company'] = new MongoRegex('/' . $arr_post['company'] . '/i');
            }

            if( $arr_post['inactive'] )
                $cond['inactive'] = 1;

            if (is_numeric($arr_post['is_customer']) && $arr_post['is_customer'])
                $cond['is_customer'] = 1;

            if (is_numeric($arr_post['is_employee']) && $arr_post['is_employee'])
                $cond['is_employee'] = 1;
        }

        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_all(array(
            'arr_where' => $cond,
            'arr_order' => $arr_order,
            'limit' => $limit,
            'skip' => $skip
                // 'arr_field' => array('name', 'is_customer', 'is_employee', 'company_id', 'company_name')
        ));
        $this->set('arr_contact', $arr_contact);

        $total_page = $total_record = $total_current = 0;
        if( is_object($arr_contact) ){
            $total_current = $arr_contact->count(true);
            $total_record = $arr_contact->count();
            if( $total_record%$limit != 0 ){
                $total_page = floor($total_record/$limit) + 1;
            }else{
                $total_page = $total_record/$limit;
            }
        }
        $this->set('total_current', $total_current);
        $this->set('total_page', $total_page);
        $this->set('total_record', $total_record);

        $this->selectModel('Company');
        $this->set('model_company', $this->Company);

        $this->layout = 'ajax';
    }

    public function save_option(array $arr_input = array()) {
        $ids = $this->get_id();
        if(isset($_POST['mongo_id']) && $_POST['mongo_id']!='')
            $ids = $_POST['mongo_id'];
        if(isset($_POST['fieldchage']))
            $fieldchage = $_POST['fieldchage'];
        else
            $fieldchage = '';
        if ($ids != '') {
            if (isset($_POST['arr']))
                $arr_input = (array)json_decode($_POST['arr']);
                // $arr_input = (array) json_decode(stripslashes($_POST['arr'])); // BaoNam: stripslashes sẽ gây lỗi nếu có dấu hai nháy "

            //nhận giá trị record
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($ids)));
            $options = array();

            if (isset($arr_tmp[$arr_input['opname']]))
                $options = $arr_tmp[$arr_input['opname']];

            //tạo giá trị mới
            if ($arr_input['opid'] == '' || $arr_input['keys'] == 'add') {
                $newopt = array();
                //nhận giá trị default
                $newopt = $this->get_default_rel($arr_input['opname']);

                //nhận giá trị input
                if (isset($arr_input['value_object'])) {
                    $temps = array();
                    $temps = (array) $arr_input['value_object'];
                    foreach ($temps as $keys => $values) {

                        if (preg_match("/_id$/", $keys) && strlen($values) == 24)
                            $newopt[$keys] = new MongoId($values); //neu la id
                        else if (preg_match("/_date$/", $keys))
                            $newopt[$keys] = new MongoDate(strtotime($values)); //neu la ngay
                        else if (preg_match("/_price$/", $keys))
                            $newopt[$keys] = (float) $values; //neu la gia
                        else
                            $newopt[$keys] = $values;
                    }
                }
                $newopt = (array) $newopt;
                $options[] = $newopt;

                $data_insert = $arrass = $this->arr_associated_data($arr_input['opname'],$options,count($options)-1,$fieldchage);//set các field liên quan
                unset($arrass[$arr_input['opname']]);
                $arrass = $this->set_mongo_array($arrass);//lọc object
                $data_insert['_id'] = $ids;
                if (count($options) > 0)
                    if ($this->opm->save($data_insert)){
                        if(count($arrass)>0)
                            echo json_encode($arrass);
                        else
                            echo (count($options)-1);
                    }
            }else {//update giá trị cũ
                if (isset($arr_input['value_object'])) {
                    $temps = array();
                    $temps = (array) $arr_input['value_object'];

                    foreach ($temps as $keys => $values) {
                        if (preg_match("/_id$/", $keys) && strlen($values) == 24)
                            $options[$arr_input['opid']][$keys] = new MongoId($values); //neu la id
                        else if (preg_match("/_date$/", $keys))
                            $options[$arr_input['opid']][$keys] = new MongoDate(strtotime($values)); //neu la ngay
                        else if (is_numeric($values) && is_float((float) $values))
                            $options[$arr_input['opid']][$keys] = (float) $values; //neu la gia
                        else
                            $options[$arr_input['opid']][$keys] = $values;
                    }

                }

                $arrass = $data_insert = array();
                $data_insert = $arrass = $this->arr_associated_data($arr_input['opname'],$options,$arr_input['opid'],$fieldchage);
                unset($arrass[$arr_input['opname']]);
                $arrass = $this->set_mongo_array($arrass);//lọc object
                $data_insert['_id'] = $ids;

                if ($this->opm->save($data_insert)){
                    if(count($arrass)>0)
                        echo json_encode($arrass);
                    else
                        echo $arr_input['opid'];
                }
            }
        }
        else
            echo '';
        die;
    }

    function user_refs($contact_id) {

        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));

        $arr_tmp = $this->data; // nếu không gán như vầy sẽ bị lỗi khi ghi nhớ tab và load bị mất $this->data của entry
        $arr_tmp['Contact']['_id'] = $contact_id;
        $arr_tmp['Contact']['user_name_contact'] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
        $arr_tmp['Contact']['password_contact'] = '123456';
        $this->data = $arr_tmp;

        $this->selectModel('Setting');
        $arr_on_login_set_quick_view = $this->Setting->select_option_vl(array('setting_value'=>'on_login_set_quick_view'));
        $this->set('arr_on_login_set_quick_view', $arr_on_login_set_quick_view);

        $arr_language_printing_external_docs = $this->Setting->select_option_vl(array('setting_value'=>'language_printing_external_docs'));
        $this->set('arr_language_printing_external_docs', $arr_language_printing_external_docs);

        $arr_on_login_set_window_sizes = $this->Setting->select_option_vl(array('setting_value'=>'on_login_set_window_sizes'));
        $this->set('arr_on_login_set_window_sizes', $arr_on_login_set_window_sizes);

        $this->selectModel('Language');
        $arr_tmp = array();
        $arr_language = $this->Language->select_all(array('arr_order' => array('name' => 1)));
        foreach($arr_language as $key => $value){
            $arr_tmp[$value['value']] = isset($value['lang'])?$value['lang']:'';
        }
        $this->set('arr_language', $arr_tmp);
        $arr_return = $arr_contact;
        if(isset($arr_return['language']) && isset($arr_tmp['lang']) && in_array($arr_return['language'], $arr_tmp)){
            $arr_return['language'] = $arr_tmp['lang'];
        }
        $this->set('arr_return', $arr_return);
    }

    function workings_holidays($contact_id){
        $this->set('contact_id', $contact_id);
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        if( !isset($arr_contact['working_hour']) ){
            for ($k=0; $k < 6; $k++) {
                $arr_contact['working_hour'][$k]['time1'] = '08:00';
                $arr_contact['working_hour'][$k]['time2'] = '18:00';
            }
            if( !$this->Contact->save($arr_contact) ){
                echo 'Error: ' . $this->Task->arr_errors_save[1];die;
            }
        }

        $this->set('workings_hours', $arr_contact['working_hour']);
    }

    function work_hour_auto_save($contact_id){
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        if( !empty($_POST) ){
            $arr_contact['working_hour'][$_POST['day']][$_POST['time']] = $_POST['value'];

            if( $_POST['time'] == 'time3' && !isset($arr_contact['working_hour'][$_POST['day']]['time4']) ){
                $arr_contact['working_hour'][$_POST['day']]['time4'] = date('H:i', strtotime('2013-12-06 '.$_POST['value'].':00') + 3600);
                $reload = true;
            }

            if( $_POST['time'] == 'time5' && !isset($arr_contact['working_hour'][$_POST['day']]['time6']) ){
                $arr_contact['working_hour'][$_POST['day']]['time6'] = date('H:i', strtotime('2013-12-06 '.$_POST['value'].':00') + 3600);
                $reload = true;
            }

            // Kiểm tra dữ liệu
            $this->_work_hour_check_time($arr_contact['working_hour'][$_POST['day']]);

            if( $this->Contact->save($arr_contact) ){
                if( isset($reload) )
                    echo 'reload';
                else
                    echo 'ok';
            }else{
                echo 'Error: ' . $this->Task->arr_errors_save[1];die;
            }
        }
        die;
    }

    function _work_hour_check_time( $arr_time ){
        $day = '2013-12-06 ';

        if( $arr_time['time1'] != "" ){
            $time1 = strtotime($day.$arr_time['time1'].':00');
            $time2 = strtotime($day.$arr_time['time2'].':00');
            if( $time1 > $time2 ){
                echo 'Time 1: first time can not greater than end time'; die;
            }
        }

        if( isset($arr_time['time3']) ){
            $time3 = strtotime($day.$arr_time['time3'].':00');
            if( $time3 <= $time2 ){
                echo 'First time of Time 2 can not less than end time of Time 1'; die;
            }
        }

        if( isset($arr_time['time3']) && isset($arr_time['time4']) ){
            $time3 = strtotime($day.$arr_time['time3'].':00');
            $time4 = strtotime($day.$arr_time['time4'].':00');
            if( $time3 >= $time4 ){
                echo 'Time 2: first time can not greater than end time'; die;
            }
        }

        if( isset($arr_time['time5']) ){
            $time5 = strtotime($day.$arr_time['time5'].':00');
            if( $time5 <= $time4 ){
                echo 'First time of Time 3 can not less than end time of Time 2'; die;
            }
        }

        if( isset($arr_time['time5']) && isset($arr_time['time6']) ){
            $time5 = strtotime($day.$arr_time['time5'].':00');
            $time6 = strtotime($day.$arr_time['time6'].':00');
            if( $time5 >= $time6 ){
                echo 'Time 3: first time can not greater than end time'; die;
            }
        }
    }

}