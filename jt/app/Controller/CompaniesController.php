<?php
App::uses('AppController', 'Controller');
class CompaniesController extends AppController {

    var $name = 'Companies';
    var $modelName = 'Company';
    public $helpers = array();
    public $opm; //Option Module
    public function beforeFilter(){
        parent::beforeFilter();
        $this->set_module_before_filter('Company');
        $this->sub_tab_default = 'contacts';
    }


    public function rebuild_setting($arr_setting=array()){
        // parent::rebuild_setting($arr_setting);
         $arr_setting = $this->opm->arr_settings;
        if(!$this->check_permission($this->name.'_@_entry_@_edit')){
            $arr_setting = $this->opm->set_lock(array(),'out');
            $this->set('address_lock', '1');
        }
        $this->selectModel('Company');
        $arr_tmp = $this->Company->select_one(array('_id'=>new MongoId($this->get_id())),array('is_supplier','is_customer'));
        if($arr_tmp['is_customer']==1 && $arr_tmp['is_supplier'] != 1){
            unset($arr_setting['relationship']['products']['block']['pricing_category']);
            unset($arr_setting['relationship']['products']['block'][4]);
            unset($arr_setting['relationship']['products']['block'][5]);
            unset($arr_setting['relationship']['orders']['block']['orders_supplier']);
        }else if($arr_tmp['is_supplier'] == 1 && $arr_tmp['is_customer']!=1){
            unset($arr_setting['relationship']['products']['block'][1]);
            unset($arr_setting['relationship']['products']['block']['keyword']);
            unset($arr_setting['relationship']['orders']['block']['orders_cus']);
        }else if($arr_tmp['is_customer'] != 1 && $arr_tmp['is_supplier'] != 1){
            unset($arr_setting['relationship']['products']['block']['pricing_category']);
            unset($arr_setting['relationship']['products']['block'][4]);
            unset($arr_setting['relationship']['products']['block'][5]);
            unset($arr_setting['relationship']['orders']['block']['orders_supplier']);
        }
        $this->opm->arr_settings = $arr_setting;
        $arr_tmp = $this->opm->arr_field_key('cls');
        $arr_link = array();
        if(!empty($arr_tmp))
            foreach($arr_tmp as $key=>$value)
                $arr_link[$value][] = $key;
        $this->set('arr_link',$arr_link);
    }

    // Add action
     public function add() {
        $this->selectModel('Company');
        $query = $this->Company->select_one(array('system' => true));
        $arr_more = $query;
        if(isset($arr_more) && is_array($arr_more) && count($arr_more)>0){
            //lưu các giá trị của company system vào loation
            if(isset($arr_more['name']))
                $arr_tmp['company_name'] = $arr_more['name'];
            if(isset($arr_more['_id']))
                $arr_tmp['company_id'] = $arr_more['_id'];
            if(isset($arr_more['phone']))
                $arr_tmp['phone'] = $arr_more['phone'];
            if(isset($arr_more['fax']))
                $arr_tmp['fax'] = $arr_more['fax'];
            if(isset($arr_more['email']))
                $arr_tmp['email'] = $arr_more['email'];
            if(isset($arr_more['addresses'][0])){
                foreach($arr_more['addresses'][0] as $kk=>$vv){
                    $arr_tmp['shipping_address'][0]['shipping_'.$kk] = $vv;
                }
            }
            //contact
            if(isset($arr_more['contact_default_id'])){
                $this->selectModel('Contact');
                $query = $this->Contact->select_one(array('_id' => new MongoId($arr_more['contact_default_id'])));
                $contact = $query;
                if(isset($contact['first_name']))
                    $arr_tmp['contact_name'] = $contact['first_name'].' ';
                if(isset($contact['last_name']))
                    $arr_tmp['contact_name'] .= $contact['last_name'];
                if(isset($contact['_id']))
                    $arr_tmp['contact_id'] = $contact['_id'];
            }
        }

        $ids = $this->opm->add('name', '',$arr_tmp);
        $newid = explode("||", $ids);
        $this->Session->write($this->name . 'ViewId', $newid[0]);
        $this->redirect('/' . $this->params->params['controller'] . '/entry');
        die;
    }

    public function set_entry_address($arr_tmp, $arr_set) {
        $address_fset = array('address_1', 'address_2', 'address_3', 'town_city', 'country', 'province_state', 'zip_postcode');
        $address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
        $address_controller = array('company');
        $address_value['company'] = array('', '', '', '', "CA", '', '');
        $this->set('address_controller', $address_controller); //set
        $address_key = array('company');
        $this->set('address_key', $address_key); //set
        $address_country = $this->country();
        $arr_address_tmp = array();
        if(!isset($arr_tmp['addresses_default_key']))
            $arr_tmp['addresses_default_key'] = 0;
        if(isset($arr_tmp['addresses'][$arr_tmp['addresses_default_key']])){
            foreach($arr_tmp['addresses'][$arr_tmp['addresses_default_key']] as $key=>$value){
                if($key=='deleted') continue;
                $arr_address_tmp['company_'.$key] = $value;
            }
        }
        $arr_tmp['company_address'][0] = array();
        foreach ($address_key as $kss => $vss) {
            //neu ton tai address trong data base
            if (isset($arr_tmp[$vss . '_address'][0])) {
                if(!empty($arr_address_tmp))
                    $arr_tmp[$vss . '_address'][0] = $arr_address_tmp;
                $arr_temp_op = $arr_tmp[$vss . '_address'][0];
                for ($i = 0; $i < count($address_fset); $i++) { //loop field and set value for display
                    if (isset($arr_temp_op[$vss . '_' . $address_fset[$i]])) {
                        $address_value[$vss][$i] = $arr_temp_op[$vss . '_' . $address_fset[$i]];
                    } else {
                        $address_value[$vss][$i] = '';
                    }
                }//pr($arr_temp_op);die;
                //get province list and country list

                if (isset($arr_temp_op[$vss . '_country_id']))
                    $address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
                else
                    $address_province[$vss] = $this->province();
                //set province
                if (isset($arr_temp_op[$vss . '_province_state_id']) && $arr_temp_op[$vss . '_province_state_id'] != '' && isset($address_province[$vss][$arr_temp_op[$vss . '_province_state_id']]))
                    $address_province_id[$kss] = $arr_temp_op[$vss . '_province_state_id'];
                else if (isset($arr_temp_op[$vss . '_province_state']))
                    $address_province_id[$kss] = $arr_temp_op[$vss . '_province_state'];
                else
                    $address_province_id[$kss] = '';

                //set country
                if (isset($arr_temp_op[$vss . '_country_id'])) {
                    $address_country_id[$kss] = $arr_temp_op[$vss . '_country_id'];
                    $address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
                } else {
                    $address_country_id[$kss] = "CA";
                    $address_province[$vss] = $this->province("CA");
                }

                $address_add[$vss] = '0';
                //chua co address trong data
            } else {
                $address_country_id[$kss] = "CA";
                $address_province[$vss] = $this->province("CA");
                $address_add[$vss] = '1';
            }
        }
        $this->set('address_value', $address_value);
        $address_hidden_field = array('invoice_address');
        $this->set('address_hidden_field', $address_hidden_field); //set

        $address_label[0] = $arr_set['field']['panel_2']['address']['name'];
        $this->set('address_label', $address_label); //set

        $address_conner[0]['top'] = 'hgt fixbor';
        $address_conner[0]['bottom'] = 'fixbor2 jt_ppbot';
        $this->set('address_conner', $address_conner); //set

        $keys = 'company';
        $address_field_name[$keys][0]['name'] = 'address_1';
        $address_field_name[$keys][0]['id']  = ucfirst($keys).'Address1';
        $address_field_name[$keys][1]['name'] = 'address_2';
        $address_field_name[$keys][1]['id']     = ucfirst($keys).'Address2';
        $address_field_name[$keys][2]['name'] = 'address_3';
        $address_field_name[$keys][2]['id']     = ucfirst($keys).'Address3';
        $address_field_name[$keys][3]['name'] = 'town_city';
        $address_field_name[$keys][3]['id']     = ucfirst($keys).'TownCity';
        $address_field_name[$keys][4]['name'] = 'country';
        $address_field_name[$keys][4]['id']     = ucfirst($keys).'Country';
        $address_field_name[$keys][5]['name'] = 'province_state';
        $address_field_name[$keys][5]['id']     = ucfirst($keys).'ProvinceState';
        $address_field_name[$keys][6]['name'] = 'zip_postcode';
        $address_field_name[$keys][6]['id']     = ucfirst($keys).'ZipPostcode';
         $this->set('address_field_name', $address_field_name); //set
        //pr($address_field_name);die;

        $this->set('address_country', $address_country); //set
        $this->set('address_country_id', $address_country_id); //set
        $this->set('address_province', $address_province); //set
        $this->set('address_province_id', $address_province_id); //set
        $this->set('address_more_line', 2); //set
        $this->set('address_onchange', "save_address_pr('\"+keys+\"');");
        if (isset($arr_tmp['company_id']) && strlen($arr_tmp['company_id']) == 24)
            $this->set('address_company_id', 'company_id');
        if (isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id']) == 24)
            $this->set('address_contact_id', 'contact_id');
        $this->set('address_add', $address_add);
    }


    //Entry - trang chi tiet
    public function entry() {
        $arr_set = $this->opm->arr_settings;
        $arr_tmp = array();
        // Get value id
        $iditem = $this->get_id();
        if ($iditem == '')
            $iditem = $this->get_last_id();

        $this->set('iditem', $iditem);
        //Load record by id
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if (preg_match("/_date$/", $field) && is_object($arr_tmp[$field]))
                            $arr_set['field'][$ks][$field]['default'] = date('m/d/Y', $arr_tmp[$field]->sec);
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];
                        if ($field == 'contact_name' && isset($arr_tmp['contact_last_name'])) {
                            $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                            $item_title['contact_name'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                        }
                    }
                }
            }

            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            $this->Session->write($this->name . 'ViewId', $iditem);

            //BEGIN custom


             if($arr_set['field']['panel_1']['is_supplier']['default'] ==  1)
                $is_supplier = 'checked="checked"';
            else
                $is_supplier = '';

             if($arr_set['field']['panel_1']['is_customer']['default'] ==  1)
                $is_customer = 'checked="checked"';
              else
                $is_customer = '';

            $arr_set['field']['panel_1']['no']['after'] = '
                        <div class="jt_company_checkbox" style="margin-right:2%;">
                                <span class="inactive_fix_entry">Supplier</span>
                                <label class="m_check2">
                                    <input type="checkbox" name="is_supplier" value="1" id="is_supplier" '.$is_supplier.'>                                 <span></span>
                                </label>
                            </div>
                            <div class="jt_company_checkbox">
                                <span class="inactive_fix_entry">Customer</span>
                                <label class="m_check2">
                                    <input type="checkbox" name="is_customer" value="1" id="is_customer" '.$is_customer.'>                                 <span></span>
                                </label>
                            </div>';

            if (isset($arr_set['field']['panel_1']['no']['default']))
                $item_title['no'] = $arr_set['field']['panel_1']['no']['default'];
            else
                $item_title['no'] = '1';
            $this->set('item_title', $item_title);

            //END custom
            //$this->set('address_lock', '1');
            //END custom
            //show footer info
            $this->show_footer_info($arr_tmp);


            //add, setup field tự tăng
        }else {
            $nextcode = $this->opm->get_auto_code('no');
            $arr_set['field']['panel_1']['no']['default'] = $nextcode;
            $this->set('item_title', array('no' => $nextcode));
        }
        $this->set('arr_settings', $arr_set);
        $this->sub_tab_default = 'contacts';
        $this->sub_tab('', $iditem);
        parent::entry();
        $this->set_entry_address($arr_tmp, $arr_set);
    }



    //Associated data function
    public function arr_associated_data($field = '', $value = '', $valueid = '') {
        $arr_return = array();
        $arr_return[$field] = $value;
        // ..........more code
        if($field == 'contact_default_id'){
            $arr_return[$field] = new MongoId($value);
        }
        if($field == 'our_rep'){
            $arr_return['our_rep_id'] = new MongoId($valueid);
        }
        if($field == 'our_csr'){
            $arr_return['our_csr_id'] = new MongoId($valueid);
        }
        return $arr_return;
    }


    //Search function
    public function entry_search() {

        if (!empty($this->data) && $this->request->is('ajax')) {

            $post = $this->data['Company'];
            $cond = array();

            $post = $this->Common->strip_search($post);

            if( strlen($post['no']) > 0 )$cond['no'] = (int)$post['no'];
            if( $post['is_supplier'] )$cond['is_supplier'] = 1;
            if( $post['is_customer'] )$cond['is_customer'] = 1;
            if( strlen($post['name']) > 0 )$cond['name'] = new MongoRegex('/' . trim($post['name']).'/i');
            if( strlen($post['type_name_id']) > 0 )$cond['type_name_id'] = $post['type_name_id'];
            if( strlen($post['business_type_id']) > 0 )$cond['business_type_id'] = $post['business_type_id'];
            if( strlen($post['industry_id']) > 0 )$cond['industry_id'] = $post['industry_id'];
            if( strlen($post['size_id']) > 0 )$cond['size_id'] = $post['size_id'];
            if( strlen($post['phone']) > 0 )$cond['phone'] = new MongoRegex('/' . trim($post['phone']).'/i');
            if( strlen($post['fax']) > 0 )$cond['fax'] = new MongoRegex('/' . trim($post['fax']).'/i');
            if( strlen($post['email']) > 0 )$cond['email'] = new MongoRegex('/' . trim($post['email']).'/i');
            if( strlen($post['web']) > 0 )$cond['web'] = new MongoRegex('/' . trim($post['web']).'/i');

            if( strlen($post['default_address_1']) > 0 )$cond['addresses'] = array('$elemMatch' => array('address_1' => new MongoRegex('/' . trim($post['default_address_1']).'/i'), 'default' => true) );
            if( strlen($post['default_address_2']) > 0 )$cond['addresses'] = array('$elemMatch' => array('address_2' => new MongoRegex('/' . trim($post['default_address_2']).'/i'), 'default' => true) );
            if( strlen($post['default_address_3']) > 0 )$cond['addresses'] = array('$elemMatch' => array('address_3' => new MongoRegex('/' . trim($post['default_address_3']).'/i'), 'default' => true) );
            if( strlen($post['default_town_city']) > 0 )$cond['addresses'] = array('$elemMatch' => array('town_city' => new MongoRegex('/' . trim($post['default_town_city']).'/i'), 'default' => true) );
            if( strlen($post['default_province_state']) > 0 )$cond['addresses'] = array('$elemMatch' => array('province_state' => new MongoRegex('/' . trim($post['default_province_state']).'/i'), 'default' => true) );
            if( strlen($post['default_country']) > 0 )$cond['addresses'] = array('$elemMatch' => array('country' => new MongoRegex('/' . trim($post['default_country']).'/i'), 'default' => true) );

            if( strlen($post['our_rep_id']) > 0 )$cond['our_rep_id'] = new MongoId($post['our_rep_id']);
            if( strlen($post['our_csr']) > 0 )$cond['our_csr'] = new MongoId($post['our_csr_id']);
            if( $post['inactive'] )$cond['inactive'] = 1;

            $this->selectModel('Company');
            $tmp = $this->Company->select_one($cond);
            if( $tmp ){
                $this->Session->write('companies_entry_search_cond', $cond);

                $cond['_id'] = array('$ne' => $tmp['_id']);
                $tmp1 = $this->Company->select_one($cond);
                if( $tmp1 ){
                    echo 'yes'; die;
                }
                echo 'yes_1_'.$tmp['_id']; die; // chỉ có 1 kết quả thì chuyển qua trang entry luôn
            }else{
                echo 'no'; die;
            }

            echo 'ok';
            die;
        }

        $this->set('arr_company_type', $this->get_setting_option('company_type'));
        $this->set('arr_business_type', $this->get_setting_option('company_business_type'));
        $this->set('arr_industry', $this->get_setting_option('company_industry'));
        $this->set('arr_size', $this->get_setting_option('company_size'));

        $this->set('set_footer', 'footer_search');
        $this->set('address_country', $this->country());
        $this->set('address_province', $this->province("CA"));

        // Get info for subtask
        // $this->sub_tab('', $arr_tmp['_id']);
    }

    public function entry_search_all(){
        $this->Session->delete('companies_entry_search_cond');
        $this->redirect('/companies/lists');
    }

    //Swith options function
   public function swith_options($keys){
        parent::swith_options($keys);
        if($keys == 'create_enquiry'){
            echo URL . DS. $this->params->params['controller'].DS.'enquiries_add_options'.DS.$this->get_id();
        }else if($keys == 'create_quotation'){
            echo URL.DS.$this->params->params['controller'].DS.'quotes_add_options'.DS.$this->get_id();
        }else if($keys == 'create_job'){
            echo URL.DS.$this->params->params['controller'].DS.'jobs_add_options'.DS.$this->get_id();
        }else if($keys == 'create_task'){
            echo URL.DS.$this->params->params['controller'].DS.'tasks_add_options'.DS.$this->get_id();
        }else if($keys == 'create_sales_order'){
            echo URL.DS.$this->params->params['controller'].DS.'orders_add_purchasesorder_options'.DS.$this->get_id();
        }else if($keys == 'create_purchase_order'){
            echo URL.DS.$this->params->params['controller'].DS.'orders_add_purchasesorder_options'.DS.$this->get_id();
        }else if($keys == 'create_sales_invoice'){
            echo URL.DS.$this->params->params['controller'].DS.'salesinvoice_add_options'.DS.$this->get_id();
        }else if($keys == 'create_shipping'){
            echo URL.DS.$this->params->params['controller'].DS.'shipping_add_options'.DS.$this->get_id();
        }else if($keys == 'active_customers'){
            $or_where = array(
                              'is_customer' => 1,
                              'is_supplier' => 0
                              );
            $or_where1 = array(
                               'is_customer' => 1,
                               'is_supplier' => 1,
                               );
            $cond['$or']=array($or_where,$or_where1);
            $this->Session->write('companies_entry_search_cond',$cond);
            echo URL.DS.$this->params->params['controller'].DS.'lists';
        }else if($keys=='active_suppliers'){
            $or_where = array(
                'is_customer' => 0,
                'is_supplier' => 1

            );
            $or_where1 = array(
                'is_customer' => 1,
                'is_supplier' => 1

            );
            $cond['$or']=array(
                $or_where
                ,$or_where1
            );

            $this->Session->write('companies_entry_search_cond',$cond);
            echo URL . DS . $this->params->params['controller'] .DS.'lists';
        }else if($keys == 'active_customers_that_are_also_suppliers'){
            $or_where = array(
                'is_customer' => 1,
                'is_supplier' => 1

            );
            $this->Session->write('companies_entry_search_cond',$or_where);
            echo URL . DS . $this->params->params['controller'] .DS.'lists';
        }else if($keys == 'companies_that_are_not_a_customer_or_supplier'){
            $or_where = array(
                'is_customer' => 0,
                'is_supplier' => 0

            );
            $this->Session->write('companies_entry_search_cond',$or_where);
            echo URL . DS . $this->params->params['controller'] .DS.'lists';
        }else if($keys == 'print_mini_list'){
            echo URL.DS.$this->params->params['controller'].DS.'view_minilist';
        }else if($keys == 'create_email'){
            echo URL .'/'.$this->params->params['controller']. '/create_email';
        }else if($keys == 'create_fax'){
            echo URL .'/'.$this->params->params['controller']. '/create_fax';
        }else if($keys == 'create_letter'){
            echo URL .'/'.$this->params->params['controller']. '/create_letter';
        }
        die;
   }

    //Subtab function
    public function contacts(){
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_all(array(
                                                  'arr_where' => array('company_id'=>new MongoId($this->get_id())),
                                                  'arr_order' => array('first_name' => 1),
                                                  ));

        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($this->get_id())), array('_id', 'contact_default_id', 'name'));

        $arr = array();
        foreach($arr_contact as $key => $value){
            if($value['_id']==$arr_company['contact_default_id'])
                $value['contact_default'] = 1;
            $arr[$key] = $value;
        }
        $subdatas = array();
        $this->set_select_data_list('relationship','contacts');
        $subdatas['contacts'] = $arr;
        $this->set('subdatas', $subdatas);
    }

    function contacts_add($company_id, $company_name = '') {
        $arr_save = array();
        $arr_save['company_id'] = new MongoId($company_id);
        $key = 0;
        if(isset($this->data['Salesaccount']['addresses_default_key']))
        $key = $this->data['Salesaccount']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Salesaccount']['addresses'][$key]['country_id']))

         $address = $this->data['Salesaccount']['addresses'][$key]['country_id'];

        $this->selectModel('Company');
        $key = 0;
        if(isset($this->data['Salesaccount']['addresses_default_key']))
        $key = $this->data['Salesaccount']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Salesaccount']['addresses'][$key]['country_id']))
        $address = $this->data['Salesaccount']['addresses'][$key]['country_id'];
        $arr_company = $this->Company->select_one(array('_id' => $arr_save['company_id']), array('_id', 'name', 'addresses', 'addresses_default_key', 'system', 'fax', 'phone'));
        if( isset($arr_company['system']) && $arr_company['system'] ){
            $arr_save['is_customer'] = 0;
            $arr_save['is_employee'] = 1;
        }
        $arr_save['company'] = $arr_company['name'];
        $arr_save['fax'] = $arr_company['fax'];
        $arr_save['company_phone'] = $arr_company['phone'];
        $arr_save['addresses'] = array(
            array(
                'name' => '',
                'deleted' => false,
                'default' => true,
                'country' => $arr_company['addresses'][$arr_company['addresses_default_key']]['country'],
                'country_id' => $arr_company['addresses'][$arr_company['addresses_default_key']]['country_id'],
                'province_state' => $arr_company['addresses'][$arr_company['addresses_default_key']]['province_state'],
                'province_state_id' => $arr_company['addresses'][$arr_company['addresses_default_key']]['province_state_id'],
                'address_1' => $arr_company['addresses'][$arr_company['addresses_default_key']]['address_1'],
                'address_2' => $arr_company['addresses'][$arr_company['addresses_default_key']]['address_2'],
                'address_3' => $arr_company['addresses'][$arr_company['addresses_default_key']]['address_3'],
                'town_city' => $arr_company['addresses'][$arr_company['addresses_default_key']]['town_city'],
                'zip_postcode' => $arr_company['addresses'][$arr_company['addresses_default_key']]['zip_postcode']
            )
        );
        $arr_save['addresses_default_key'] = 0;

        // Tìm kiếm trước xem company này hiện tại đã có default chưa, nếu chưa thì khi save xong sẽ save vào company này default contact
        $this->selectModel('Contact');
        $arr_contact_default = $this->Contact->select_one(array('company_id' => new MongoId($company_id)), array('_id', 'name'));

        $this->Contact->arr_default_before_save = $arr_save;
        if (!$this->Contact->add()) {
            echo 'Error: ' . $this->Contact->arr_errors_save[1];
            die;
        } else {
            if (!isset($arr_contact_default['_id'])) {
                $arr_save_company = array();
                $arr_save_company['_id'] = new MongoId($company_id);
                $arr_save_company['contact_default_id'] = $this->Contact->mongo_id_after_save;
                $this->selectModel('Company');
                if (!$this->Company->save($arr_save_company)) {
                    echo 'Error: ' . $this->Company->arr_errors_save[1];
                    die;
                }
            }
        }
        die;
    }

    function contacts_auto_save() {
        if (!empty($this->data)) {
            $arr_post_data = $this->data;
            $arr_save = array();
            $arr_save['_id'] = $arr_post_data['contact_id'];
            $arr_save[$arr_post_data['field']] = $arr_post_data['value'];

            if($arr_post_data['field']=='first_name')
                $arr_save['full_name'] =  $arr_post_data['value'].' '.$arr_post_data['other_name'];
            if($arr_post_data['field']=='last_name')
                $arr_save['full_name'] =  $arr_post_data['other_name'].' '.$arr_post_data['value'];

            $this->selectModel('Contact');
            if ($this->Contact->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Contact->arr_errors_save[1];
            }
        }
        die;
    }

    function contacts_save_default($company_id, $contact_id) {
        if (!$this->request->is('ajax'))
            exit;
        $arr_save['_id'] = $company_id;
        $arr_save['contact_default_id'] = new MongoId($contact_id);

        $this->selectModel('Company');
        if ($this->Company->save($arr_save)) {
            echo 'ok';
        } else {
            echo 'Error: ' . $this->Company->arr_errors_save[1];
        }
        die;
    }

    function addresses(){
        $this->selectModel('Company');
        $company = $this->Company->select_one(array('_id'=> new MongoId($this->get_id())),array('addresses'));
        $this->selectModel('Province');
        $arr_all_province = array();
        // foreach($arr_company['addresses'] as $key => $value){
        //     $arr_all_province[$key] = $value;
        //     //if($value['deleted'])continue;
        //     //if(isset($arr_all_province[$value['country_id']]))continue;
        //     $province =  $this->Province->select_list(array(
        //                                               'arr_where' => array('country_id'=>$value['country_id']),
        //                                               'arr_field' => array('key','name'),
        //                                               'arr_order' => array('name'=>1)
        //                                               ));
        //     $arr_all_province[$value['country_id']] = $province;
        // }
        $subdatas = array();
        if(!isset($company['addresses']))
            $company['addresses'] = array();
        $subdatas['addresses'] = $company['addresses'];
        $this->set_select_data_list('relationship','addresses');
        $this->set('subdatas', $subdatas);
    }

    function addresses_add($company_id) {
        $this->selectModel('Company');
        $this->Company->collection->update(
                array('_id' => new MongoId($company_id)), array('$push' => array(
                'addresses' => array(
                    'name' => '',
                    'default' => false,
                    'address_1' => '',
                    'address_2' => '',
                    'address_3' => '',
                    'town_city' => '',
                    'zip_postcode' => '',
                    'province_state' => '',
                    'province_state_id' => '',
                    'country' => 'Canada',
                    'country_id' => "CA",
                    'deleted' => false
                )))
        );
        $this->addresses($company_id);
        $this->render('addresses');
    }

    function enquiries($company_id){
        $this->selectModel('Enquiry');
        $arr_enquiries = $this->Enquiry->select_all(array(
                                                    'arr_where' => array('company_id'=>new MongoId($company_id))
                                                    ));
        $arr_tmp = array();
        foreach($arr_enquiries as $key => $value){
            $arr_tmp[$key] = $value;
        }
        //pr($arr_tmp);die;
        $subdatas = array();
        $subdatas['enquiries'] = $arr_tmp;
        $this->set('subdatas', $subdatas);
    }

    function enquiries_add($company_id) {
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $this->selectModel('Enquiry');
        $arr_tmp = $this->Enquiry->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save = $this->arr_associated_data('company_name',$arr_company['name'], $company_id);
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }

        $key = 0;
        if(isset($this->data['Company']['addresses_default_key']))
            $key = $this->data['Company']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Company']['addresses'][$key]['country_id']))
            $address = $this->data['Company']['addresses'][$key]['country_id'];

        $key = isset($arr_company['addresses_default_key'])?$arr_company['addresses_default_key']:0;
        $arr_save['company_id'] = $arr_company['_id'];
        $arr_save['date']= new MongoDate(time());
        $arr_save['status']='Hot';
        $arr_save['default_country'] = isset($arr_company['addresses'][$key]['country'])?$arr_company['addresses'][$key]['country']:'';
        $arr_save['default_country_id'] = isset($arr_company['addresses'][$key]['country_id'])?$arr_company['addresses'][$key]['country_id']:0;
        $arr_save['default_province_state'] = isset($arr_company['addresses'][$key]['province_state'])?$arr_company['addresses'][$key]['province_state']:'';
        $arr_save['default_province_state_id'] = isset($arr_company['addresses'][$key]['province_state_id'])?$arr_company['addresses'][$key]['province_state_id']:'';
        $arr_save['default_address_1'] = isset($arr_company['addresses'][$key]['address_1'])?$arr_company['addresses'][$key]['address_1']:'';
        $arr_save['default_address_2'] = isset($arr_company['addresses'][$key]['address_2'])?$arr_company['addresses'][$key]['address_2']:'';
        $arr_save['default_address_3'] = isset($arr_company['addresses'][$key]['address_3'])?$arr_company['addresses'][$key]['address_3']:'';
        $arr_save['default_town_city'] = isset($arr_company['addresses'][$key]['town_city'])?$arr_company['addresses'][$key]['town_city']:'';
        $arr_save['default_zip_postcode'] = isset($arr_company['addresses'][$key]['zip_postcode'])?$arr_company['addresses'][$key]['zip_postcode']:'';
        $arr_save['company_phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';
        $arr_save['company_fax'] = isset($arr_company['fax'])?$arr_company['fax']:'';
        $arr_save['company_email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['web'] = isset($arr_company['web'])?$arr_company['web']:'';
        $arr_save['company'] = isset($arr_company['name'])?$arr_company['name']:'';

        $this->Enquiry->arr_default_before_save = $arr_save;
        if ($this->Enquiry->add())
           echo URL.'/enquiries/entry/' . $this->Enquiry->mongo_id_after_save;
        else
            echo URL.'/enquiries/entry';
        die;
    }

    function enquiries_add_options($company_id) {
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $this->selectModel('Enquiry');
        $arr_tmp = $this->Enquiry->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save = $this->arr_associated_data('company_name',$arr_company['name'], $company_id);
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }

        $key = 0;
        if(isset($this->data['Company']['addresses_default_key']))
            $key = $this->data['Company']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Company']['addresses'][$key]['country_id']))
            $address = $this->data['Company']['addresses'][$key]['country_id'];


        //$key = isset($arr_company['addresses_default_key'])?$arr_company['addresses_default_key']:0;
        $key = $arr_company['addresses_default_key'];
        $arr_save['date']= new MongoDate(time());
        $arr_save['status']='Hot';
        $arr_save['default_country'] = isset($arr_company['addresses'][$key]['country'])?$arr_company['addresses'][$key]['country']:'';
        $arr_save['default_country_id'] = isset($arr_company['addresses'][$key]['country_id'])?$arr_company['addresses'][$key]['country_id']:0;
        $arr_save['default_province_state'] = isset($arr_company['addresses'][$key]['province_state'])?$arr_company['addresses'][$key]['province_state']:'';
        $arr_save['default_province_state_id'] = isset($arr_company['addresses'][$key]['province_state_id'])?$arr_company['addresses'][$key]['province_state_id']:'';
        $arr_save['default_address_1'] = isset($arr_company['addresses'][$key]['address_1'])?$arr_company['addresses'][$key]['address_1']:'';
        $arr_save['default_address_2'] = isset($arr_company['addresses'][$key]['address_2'])?$arr_company['addresses'][$key]['address_2']:'';
        $arr_save['default_address_3'] = isset($arr_company['addresses'][$key]['address_3'])?$arr_company['addresses'][$key]['address_3']:'';
        $arr_save['default_town_city'] = isset($arr_company['addresses'][$key]['town_city'])?$arr_company['addresses'][$key]['town_city']:'';
        $arr_save['default_zip_postcode'] = isset($arr_company['addresses'][$key]['zip_postcode'])?$arr_company['addresses'][$key]['zip_postcode']:'';
        $arr_save['company_phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';
        $arr_save['company_fax'] = isset($arr_company['fax'])?$arr_company['fax']:'';
        $arr_save['company_email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['web'] = isset($arr_company['web'])?$arr_company['web']:'';
        $arr_save['company'] = isset($arr_company['name'])?$arr_company['name']:'';


        $this->Enquiry->arr_default_before_save = $arr_save;
        if ($this->Enquiry->add())
            $this->redirect('/enquiries/entry/' . $this->Enquiry->mongo_id_after_save);
        $this->redirect('/enquiries/entry');
    }

    function jobs($company_id){
        $this->selectModel('Job');
        $arr_job = $this->Job->select_all(array(
                                          'arr_where'=>array('company_id'=>new MongoId($company_id))
                                          ));
        $arr_tmp = array();
        foreach($arr_job as $key => $value){
            $arr_tmp[$key] = $value;
        }

        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id'=> new MongoId($company_id)),array('markup_rate','rate_per_hour'));
        $subdatas = array();
        $subdatas['jobs'] = $arr_tmp;
        $subdatas['default'] = $arr_company;
        $this->set('subdatas', $subdatas);
    }

    function jobs_add($company_id) {
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $this->selectModel('Job');
        $arr_tmp = $this->Job->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }

        $arr_save['company_id'] = $arr_company['_id'];
        $arr_save['status_id'] = 0;
        $arr_save['name'] = '';
        $arr_save['contacts_default_key'] = 0;
        $arr_save['contacts'][] = array(
            "contact_name" => $_SESSION['arr_user']['contact_name'],
            "contact_id" => $_SESSION['arr_user']['contact_id'],
            "default" => true,
            "deleted" => false
        );
        $arr_save['type'] = '';
        $arr_save['status'] = 'New';


        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['company_id'] = $arr_company['_id'];
        $arr_save['company_phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';
        $arr_save['fax'] = isset($arr_company['fax'])?$arr_company['fax']:'';
        $arr_save['email']=isset($arr_company['email'])?$arr_company['email']:'';

        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];

            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];

            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['direct_phone'] = $arr_contact['direct_dial'];

            if(isset($arr_contact['mobile'])&&$arr_contact['mobile']!='')
                $arr_save['mobile'] = $arr_contact['mobile'];
        }
        else{
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=>new MongoId($company_id)),
                'arr_order' => array('_id'=>-1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arrtemp)>0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
            if(isset($arr_contact['_id'])){
                $arr_save['contact_id'] = $arr_contact['_id'];
                $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            }

            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];


            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['direct_phone'] = $arr_contact['direct_dial'];

            if(isset($arr_contact['mobile'])&&$arr_contact['mobile']!='')
                $arr_save['mobile'] = $arr_contact['mobile'];
        }
        if (isset($work_start_sec) && $work_start_sec > 0) {
            $arr_save['work_end'] = $arr_save['work_start'] = new MongoDate($work_start_sec);
        } else {
            $arr_save['work_end'] = $arr_save['work_start'] = new MongoDate(strtotime(date('Y-m-d H:00:00')) + 3600);
        }

        $this->Job->arr_default_before_save = $arr_save;
        if ($this->Job->add()) {
          echo URL .'/jobs/entry/'. $this->Job->mongo_id_after_save;
        }
        else
          echo URL . '/jobs/entry';
        die;
    }

    function jobs_add_options($company_id) {

        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));

        $this->selectModel('Job');
        $arr_tmp = $this->Job->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }
        $arr_save['status_id'] = 0;
        $arr_save['name'] = '';
        $arr_save['contacts_default_key'] = 0;
        $arr_save['contacts'][] = array(
            "contact_name" => $_SESSION['arr_user']['contact_name'],
            "contact_id" => $_SESSION['arr_user']['contact_id'],
            "default" => true,
            "deleted" => false
        );
        $arr_save['type'] = '';
        $arr_save['status'] = 'New';

        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['company_id'] = $arr_company['_id'];
        $arr_save['company_phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';
        $arr_save['fax'] = isset($arr_company['fax'])?$arr_company['fax']:'';
        $arr_save['email']=isset($arr_company['email'])?$arr_company['email']:'';


        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];

            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];

            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['direct_phone'] = $arr_contact['direct_dial'];

            if(isset($arr_contact['mobile'])&&$arr_contact['mobile']!='')
                $arr_save['mobile'] = $arr_contact['mobile'];
        }
        else{
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=>new MongoId($company_id)),
                'arr_order' => array('_id'=>-1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arrtemp)>0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
            if(isset($arr_contact['_id'])){
                $arr_save['contact_id'] = $arr_contact['_id'];
                $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            }

            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];


            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['direct_phone'] = $arr_contact['direct_dial'];

            if(isset($arr_contact['mobile'])&&$arr_contact['mobile']!='')
                $arr_save['mobile'] = $arr_contact['mobile'];
        }
        if (isset($work_start_sec) && $work_start_sec > 0) {
            $arr_save['work_end'] = $arr_save['work_start'] = new MongoDate($work_start_sec);
        } else {
            $arr_save['work_end'] = $arr_save['work_start'] = new MongoDate(strtotime(date('Y-m-d H:00:00')) + 3600);
        }

        $this->Job->arr_default_before_save = $arr_save;
        if ($this->Job->add()) {
            $this->redirect('/jobs/entry/'. $this->Job->mongo_id_after_save);
        }
        $this->redirect('/jobs/entry');
    }

    function tasks($company_id){
        $this->selectModel('Task');
        $arr_task = $this->Task->select_all(array(
                                            'arr_where' => array('company_id' => new MongoId($company_id)),
                                            'arr_order' => array('work_start' => 1)
                                            ));
        $arr_tmp = array();
        foreach($arr_task as $key => $value){
            $arr_tmp[$key] = $value;
        }
        $subdatas = array();
        $subdatas['tasks'] = $arr_tmp;
        $this->set('subdatas', $subdatas);
    }

    function tasks_add($company_id) {
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save = array();
        if (isset($arr_company['our_rep_id']) && $arr_company['our_rep_id'] != '' && $arr_company['our_rep_id']!=null) {
            $arr_save['our_rep_id'] = $arr_company['our_rep_id'];
            $arr_save['our_rep'] = $arr_company['our_rep'];
        }
        $arr_save['company_name'] = $arr_company['name'];
        $arr_save['company_id'] = $arr_company['_id'];
        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
        }

        $this->selectModel('Task');
        $this->Task->arr_default_before_save = $arr_save;
        if ($this->Task->add())
            echo URL.'/tasks/entry/' . $this->Task->mongo_id_after_save;
        else
            echo URL .'/tasks/entry';
        die;
    }


    function tasks_add_options($company_id) {
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save = array();
        if (isset($arr_company['our_rep_id']) && $arr_company['our_rep_id'] != '' && $arr_company['our_rep_id']!=null) {
            $arr_save['our_rep_id'] = $arr_company['our_rep_id'];
            $arr_save['our_rep'] = $arr_company['our_rep'];
        }
        $arr_save['company_name'] = $arr_company['name'];
        $arr_save['company_id'] = $arr_company['_id'];
        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
        }

        $this->selectModel('Task');
        $this->Task->arr_default_before_save = $arr_save;
        if ($this->Task->add())
            $this->redirect('/tasks/entry/' . $this->Task->mongo_id_after_save);
        $this->redirect('/tasks/entry');
    }

    public function comms(){
        $subdatas = array();
        $subdatas['stockcurrent1'] = array();
        $this->set('subdatas', $subdatas);
        //goi ham dung chung cua a.Nam
        $module_id = $this->get_id();
        $this->communications($module_id, true);
    }

    function quotes($company_id){
        $this->selectModel('Quotation');
        $arr_quote = $this->Quotation->select_all(array(
                                                'arr_where' => array('company_id'=>new MongoId($company_id)),
                                                ));
        $arr_tmp = array();
        foreach($arr_quote as $key => $value){
            $arr_tmp[$key] = $value;
        }
        $subdatas = array();
        $subdatas['quotes'] = $arr_tmp;
        $this->set('subdatas', $subdatas);
    }

    function quotes_add_options($company_id) {
        // BaoNam: fix hoàn chỉnh 07 02 2014
        $arr_tmp=array();
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['company_id'] = $arr_company['_id'];
        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';
        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];
            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['phone'] = $arr_contact['direct_dial'];
        }
        if(isset($arr_company['addresses_default_key'])){
            $key_default = $arr_company['addresses_default_key'];
            $arr_save['invoice_address'][0] = array(
                'deleted' => false,
                'invoice_address_1' => $arr_company['addresses'][$key_default]['address_1'],
                'invoice_address_2' => $arr_company['addresses'][$key_default]['address_2'],
                'invoice_address_3' => $arr_company['addresses'][$key_default]['address_3'],
                'invoice_town_city' => $arr_company['addresses'][$key_default]['town_city'],
                'invoice_province_state' => $arr_company['addresses'][$key_default]['province_state'],
                'invoice_province_state_id' => $arr_company['addresses'][$key_default]['province_state_id'],
                'invoice_zip_postcode' => $arr_company['addresses'][$key_default]['zip_postcode'],
                'invoice_country' => $arr_company['addresses'][$key_default]['country'],
                'invoice_country_id' => $arr_company['addresses'][$key_default]['country_id']
            );
        }elseif(isset($arr_company['addresses'][0])){
            $arr_save['invoice_address'][0] = array(
                'deleted' => false,
                'invoice_address_1' => $arr_company['addresses'][0]['address_1'],
                'invoice_address_2' => $arr_company['addresses'][0]['address_2'],
                'invoice_address_3' => $arr_company['addresses'][0]['address_3'],
                'invoice_town_city' => $arr_company['addresses'][0]['town_city'],
                'invoice_province_state' => $arr_company['addresses'][0]['province_state'],
                'invoice_province_state_id' => $arr_company['addresses'][0]['province_state_id'],
                'invoice_zip_postcode' => $arr_company['addresses'][0]['zip_postcode'],
                'invoice_country' => $arr_company['addresses'][0]['country'],
                'invoice_country_id' => $arr_company['addresses'][0]['country_id']
            );
        }
        if(isset($arr_company['our_csr_id']) && is_object($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = $arr_company['our_csr'];
            $arr_save['our_csr_id'] = $arr_company['our_csr_id'];
        }
        if(isset($arr_company['our_rep_id']) && is_object($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = $arr_company['our_rep'];
            $arr_save['our_rep_id'] = $arr_company['our_rep_id'];
        }
        $this->selectModel('Quotation');
        $this->Quotation->arr_default_before_save = $arr_save;
        if ($this->Quotation->add()) {
            $this->redirect('/quotations/entry/'. $this->Quotation->mongo_id_after_save);
        }
        $this->redirect('/quotations/entry');
    }

    function quotes_add($company_id) {
        // BaoNam: fix hoàn chỉnh 07 02 2014
        $arr_tmp=array();
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['company_id'] = $arr_company['_id'];
        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';
        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            if(isset($arr_contact['email'])&&$arr_contact['email']!='')
                $arr_save['email'] = $arr_contact['email'];
            if(isset($arr_contact['direct_dial'])&&$arr_contact['direct_dial']!='')
                $arr_save['phone'] = $arr_contact['direct_dial'];
        }
        if(isset($arr_company['addresses_default_key'])){
            $key_default = $arr_company['addresses_default_key'];
            $arr_save['invoice_address'][0] = array(
                'deleted' => false,
                'invoice_address_1' => $arr_company['addresses'][$key_default]['address_1'],
                'invoice_address_2' => $arr_company['addresses'][$key_default]['address_2'],
                'invoice_address_3' => $arr_company['addresses'][$key_default]['address_3'],
                'invoice_town_city' => $arr_company['addresses'][$key_default]['town_city'],
                'invoice_province_state' => $arr_company['addresses'][$key_default]['province_state'],
                'invoice_province_state_id' => $arr_company['addresses'][$key_default]['province_state_id'],
                'invoice_zip_postcode' => $arr_company['addresses'][$key_default]['zip_postcode'],
                'invoice_country' => $arr_company['addresses'][$key_default]['country'],
                'invoice_country_id' => $arr_company['addresses'][$key_default]['country_id']
            );
        }elseif(isset($arr_company['addresses'][0])){
            $arr_save['invoice_address'][0] = array(
                'deleted' => false,
                'invoice_address_1' => $arr_company['addresses'][0]['address_1'],
                'invoice_address_2' => $arr_company['addresses'][0]['address_2'],
                'invoice_address_3' => $arr_company['addresses'][0]['address_3'],
                'invoice_town_city' => $arr_company['addresses'][0]['town_city'],
                'invoice_province_state' => $arr_company['addresses'][0]['province_state'],
                'invoice_province_state_id' => $arr_company['addresses'][0]['province_state_id'],
                'invoice_zip_postcode' => $arr_company['addresses'][0]['zip_postcode'],
                'invoice_country' => $arr_company['addresses'][0]['country'],
                'invoice_country_id' => $arr_company['addresses'][0]['country_id']
            );
        }
        if(isset($arr_company['our_csr_id']) && is_object($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = $arr_company['our_csr'];
            $arr_save['our_csr_id'] = $arr_company['our_csr_id'];
        }
        if(isset($arr_company['our_rep_id']) && is_object($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = $arr_company['our_rep'];
            $arr_save['our_rep_id'] = $arr_company['our_rep_id'];
        }
        $this->selectModel('Quotation');
        $this->Quotation->arr_default_before_save = $arr_save;
        if ($this->Quotation->add()) {
           echo URL.'/quotations/entry/'. $this->Quotation->mongo_id_after_save;
        }else
           echo URL .'/quotations/entry';
        die;
    }

    //chua hoan thanh
    function orders($company_id){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id'=>new MongoId($company_id)),array('_id','is_customer','is_supplier'));

        $arr_tmp = array();
        if($arr_company['is_customer']){
            $this->selectModel('Salesorder');
            $arr_salesorder = $this->Salesorder->select_all(array(
                                                            'arr_where'=>array('company_id'=>new MongoId($company_id)),
                                                            ));
            foreach($arr_salesorder as $key => $value){
                $arr_tmp[$key]=$value;
            }
        }

        $arr_tmp_supp = array();
        if($arr_company['is_supplier']){
            $this->selectModel('Purchaseorder');
            $arr_purchaseorder = $this->Purchaseorder->select_all(array(
                                                                  'arr_where'=> array('company_id'=>new MongoId($company_id)),
                                                                  ));
            foreach($arr_purchaseorder as $k => $v){
                $arr_tmp_supp[$k] = $v;
            }
        }

        $subdatas = array();
        $subdatas['orders_cus'] = $arr_tmp;
        $subdatas['orders_supplier'] = $arr_tmp_supp;
        $this->set('subdatas', $subdatas);
    }

    function orders_add_salesorder($company_id) {

        $this->selectModel('Salesorder');
        $arr_tmp = $this->Salesorder->select_one(array(), array(), array('code' => -1));
        $arr_save = array();

        $arr_save['code'] = $this->Salesorder->get_auto_code('code');
        $arr_save['heading'] = '';
        $arr_save['company_name'] = '';
        $arr_save['contact_name'] = '';
        $arr_save['status_id'] = 0;
        $arr_save['contact_id'] = '';

        $arr_save['invoice_address_1'] = '';
        $arr_save['invoice_address_2'] = '';
        $arr_save['invoice_address_3'] = '';
        $arr_save['invoice_town_city'] = '';
        $arr_save['invoice_province_state'] = '';
        $arr_save['invoice_province_state_id'] = 0;
        $arr_save['invoice_zip_postcode'] = '';
        $arr_save['invoice_country'] = '';
        $arr_save['invoice_country_id'] = "CA";

        $arr_save['shipping_address_1'] = '';
        $arr_save['shipping_address_2'] = '';
        $arr_save['shipping_address_3'] = '';
        $arr_save['shipping_town_city'] = '';
        $arr_save['shipping_province_state'] = '';
        $arr_save['shipping_province_state_id'] = 0;
        $arr_save['shipping_zip_postcode'] = '';
        $arr_save['shipping_country'] = '';
        $arr_save['shipping_country_id'] = "CA";


        $arr_save['date_in_hour'] = $arr_save['due_date_hour'] = $arr_save['work_start_hour'] = date('08:00');
        $tmp_time = strtotime(date('Y-m-d') . '' . $arr_save['date_in_hour'] . ':00');
        $arr_save['date_in'] = $arr_save['due_date'] = $arr_save['work_start'] = new MongoDate($tmp_time);

        $arr_save['work_end'] = new MongoDate($tmp_time + 3600);
        $arr_save['work_end_hour'] = date("H:00", $tmp_time + 3600);
        $arr_save['due_date'] = new MongoDate($tmp_time + 3600);
        $arr_save['due_date_hour'] = date("H:00", $tmp_time + 3600);

        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));

        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }
        else
        {
            $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }
        else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }


        $arr_save['company_name'] = $arr_company['name'];
        $arr_save['company_id'] = $arr_company['_id'];

        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
        }

        $this->Salesorder->arr_default_before_save = $arr_save;
        if ($this->Salesorder->add()) {
           echo URL.'/salesorders/entry/'. $this->Salesorder->mongo_id_after_save;
        }else
           echo URL.'/salesorders/entry';
        die;
    }

    function orders_add_purchasesorder_options($company_id){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));

        $this->selectModel('Purchaseorder');
        $arr_tmp = $this->Purchaseorder->select_one(array(), array(), array('code' => -1));

        $arr_save = array();

        $arr_save = $this->arr_associated_data('company_name',$arr_company['name'], $company_id);
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $arr_save['purchase_orders_status'] = 'In progress';
        $arr_save['purchord_date'] = new MongoDate(time());
        $arr_save['required_date'] = new MongoDate(15 * 24 * 60 * 60 + (int) time());
        $arr_save['delivery_date'] = '123';

        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }
        else
        {
            $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }
        else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        $arr_save['ship_to_contact_id'] = $this->Company->user_id();
        $arr_save['ship_to_contact_name'] = $this->Company->user_name();

        $collection_company = $this->Company->select_one(array('system' => true));
        if ($collection_company != null) {
            $arr_save['ship_to_company_id'] = $collection_company['_id'];
            $arr_save['ship_to_company_name'] = $collection_company['name'];

            $arr_temp = array();

            foreach ($collection_company['addresses'][$collection_company['addresses_default_key']] as $key => $value) {
                if ($key == 'deleted')
                    $arr_temp[$key] = $value;
                else
                    $arr_temp['shipping_' . $key] = $value;
            }

            $object_child[0] = (object) $arr_temp;
            $object_parent = (object) $object_child;
            $arr_save['shipping_address'] = $object_parent;
        }


        $this->Purchaseorder->arr_default_before_save = $arr_save;

        // BaoNam: sửa ngày 02/12/2013
        if ($this->Purchaseorder->add()) {
            $this->redirect('/purchaseorders/entry/'. $this->Purchaseorder->mongo_id_after_save);
        }
        $this->redirect('/purchaseorders/entry');
    }

    function orders_add_purchasesorder($company_id){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));

        $this->selectModel('Purchaseorder');
        $arr_tmp = $this->Purchaseorder->select_one(array(), array(), array('code' => -1));

        $arr_save = array();

        $arr_save = $this->arr_associated_data('company_name',$arr_company['name'], $company_id);
        $arr_save['code'] = $this->Purchaseorder->get_auto_code('code');

        $arr_save['purchase_orders_status'] = 'In progress';
        $arr_save['purchord_date'] = new MongoDate(time());
        $arr_save['required_date'] = new MongoDate(15 * 24 * 60 * 60 + (int) time());
        $arr_save['delivery_date'] = '123';

        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }
        else
        {
            $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }
        else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        $arr_save['ship_to_contact_id'] = $this->Company->user_id();
        $arr_save['ship_to_contact_name'] = $this->Company->user_name();

        $collection_company = $this->Company->select_one(array('system' => true));
        if ($collection_company != null) {
            $arr_save['ship_to_company_id'] = $collection_company['_id'];
            $arr_save['ship_to_company_name'] = $collection_company['name'];

            $arr_temp = array();

            foreach ($collection_company['addresses'][$collection_company['addresses_default_key']] as $key => $value) {
                if ($key == 'deleted')
                    $arr_temp[$key] = $value;
                else
                    $arr_temp['shipping_' . $key] = $value;
            }

            $object_child[0] = (object) $arr_temp;
            $object_parent = (object) $object_child;
            $arr_save['shipping_address'] = $object_parent;
        }


        $this->Purchaseorder->arr_default_before_save = $arr_save;

        // BaoNam: sửa ngày 02/12/2013
        if ($this->Purchaseorder->add()) {
           echo URL.'/purchaseorders/entry/'. $this->Purchaseorder->mongo_id_after_save;
        }else
           echo URL.'/purchaseorders/entry';
        die;
    }

    function shipping($company_id){
        $this->selectModel('Shipping');
        $arr_shipping = $this->Shipping->select_all(array(
                                                    'arr_where' => array('company_id'=>new MongoId($company_id)),
                                                    ));
        $arr_tmp = array();
        foreach($arr_shipping as $key => $value){
            $arr_tmp[$key] = $value;
        }
        $subdatas = array();
        $subdatas['shipping'] = $arr_tmp;
        $this->set('subdatas', $subdatas);
    }

    function shipping_add($company_id,$type='') {
        $this->selectModel('Shipping');
        $arr_tmp = $this->Shipping->select_one(array(), array(), array('code' => -1));
        $arr_save = array();
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['company_id'] = $arr_company['_id'];
        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }

        if($type=='Incoming')
            $arr_save['shipping_type'] = 'In';
        elseif($type=='Outgoing')
            $arr_save['shipping_type'] = 'Out';

        if($arr_company['is_customer']==0&&$arr_company['is_supplier']==1)
            $arr_save['shipping_type'] = 'In';
        elseif($arr_company['is_customer']==1&&$arr_company['is_supplier']==0)
            $arr_save['shipping_type'] = 'Out';



        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';

        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            if(isset($arr_contact['email']))
                $arr_save['email'] = $arr_contact['email'];
            if(isset($arr_contact['direct_dial']))
                $arr_save['phone'] = $arr_contact['direct_dial'];
        }
        else
        {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=>new MongoId($company_id)),
                'arr_order' => array('_id'=>-1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arrtemp)>0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
            if(isset($arr_contact['_id'])){
                $arr_save['contact_id'] = $arr_contact['_id'];
                $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            }

            if(isset($arr_contact['email']))
                $arr_save['email'] = $arr_contact['email'];
            if(isset($arr_contact['direct_dial']))
                $arr_save['phone'] = $arr_contact['direct_dial'];


        }
        $arr_invoice_addrress=array();
        foreach($arr_company['addresses'][$arr_company['addresses_default_key']] as $key=>$value){
            if($key=='deleted')
                $arr_invoice_addrress[$key]=$value;
            else
                $arr_invoice_addrress['invoice_'.$key]=$value;

        }
        $arr_save['invoice_address'][0] = (object)$arr_invoice_addrress;

        $this->Shipping->arr_default_before_save = $arr_save;

        // BaoNam: sửa ngày 02/12/2013
        if ($this->Shipping->add()) {
            echo URL .'/shippings/entry/'. $this->Shipping->mongo_id_after_save;
        }else
            echo URL .'/shippings/entry';
        die;
    }

    function shipping_add_options($company_id,$type='') {
        $this->selectModel('Shipping');
        $arr_tmp = $this->Shipping->select_one(array(), array(), array('code' => -1));
        $arr_save = array();
        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['company_id'] = $arr_company['_id'];
        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }

        if($type=='Incoming')
            $arr_save['shipping_type'] = 'In';
        elseif($type=='Outgoing')
            $arr_save['shipping_type'] = 'Out';

        if($arr_company['is_customer']==0&&$arr_company['is_supplier']==1)
            $arr_save['shipping_type'] = 'In';
        elseif($arr_company['is_customer']==1&&$arr_company['is_supplier']==0)
            $arr_save['shipping_type'] = 'Out';



        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['phone'] = isset($arr_company['phone'])?$arr_company['phone']:'';

        if (isset($arr_company['contact_default_id'])) {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));
            $arr_save['contact_id'] = $arr_contact['_id'];
            $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            if(isset($arr_contact['email']))
                $arr_save['email'] = $arr_contact['email'];
            if(isset($arr_contact['direct_dial']))
                $arr_save['phone'] = $arr_contact['direct_dial'];
        }
        else
        {
            $this->selectModel('Contact');
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=>new MongoId($company_id)),
                'arr_order' => array('_id'=>-1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arrtemp)>0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
            if(isset($arr_contact['_id'])){
                $arr_save['contact_id'] = $arr_contact['_id'];
                $arr_save['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
            }

            if(isset($arr_contact['email']))
                $arr_save['email'] = $arr_contact['email'];
            if(isset($arr_contact['direct_dial']))
                $arr_save['phone'] = $arr_contact['direct_dial'];


        }
        $arr_invoice_addrress=array();
        foreach($arr_company['addresses'][$arr_company['addresses_default_key']] as $key=>$value){
            if($key=='deleted')
                $arr_invoice_addrress[$key]=$value;
            else
                $arr_invoice_addrress['invoice_'.$key]=$value;

        }
        $arr_save['invoice_address'][0] = (object)$arr_invoice_addrress;

        $this->Shipping->arr_default_before_save = $arr_save;

        // BaoNam: sửa ngày 02/12/2013
        if ($this->Shipping->add()) {
            $this->redirect('/shippings/entry/'. $this->Shipping->mongo_id_after_save);
        }
        $this->redirect('/shippings/entry');
    }

    function rfqs($company_id){
        $subdatas = array();
        $subdatas['rfqs'] = array();
        $this->set('subdatas', $subdatas);
    }

    function products($company_id){
        if(empty($this->data) || !isset($this->data['Company'])){
            $this->selectModel('Company');
            $arr_company = $this->Company->select_one(array('_id'=>new MongoId($company_id)));
            $tmp['Company'] = $arr_company;
            $this->data = $tmp;
        }else{
            $arr_company = $this->data['Company'];
        }
        $arr_pricing = array();
        if(!empty($this->data['Company']['pricing']))
        foreach($this->data['Company']['pricing'] as $k => $v){
            $arr_pricing[$k] = $v;
            if(isset($v['price_break']) && isset($v['price_break'][0])){
                foreach($v['price_break'] as $kk => $vv){
                    if(isset($vv['deleted']) && $vv['deleted']) continue;
                    $arr_pricing[$k]['range'] = $vv['range_from'].'-'.$vv['range_to'];
                    $arr_pricing[$k]['unit_price'] = $vv['unit_price'];
                    break;
                }
            }
        }
        $arr_tmp = array();
        if($arr_company['is_supplier']==1){
            $this->selectModel('Product');
            $arr_product_supplier=$this->Product->select_all(array(
                                                             'arr_where'=>array("company_id"=>new MongoId($company_id)),
                                                             ));
            foreach($arr_product_supplier as $key => $value){
                $arr_tmp[$key] = $value;
            }
        }

        if($arr_company['is_customer']==1){
            $arr_product_customer=array();
        }

        $subdatas = array();
        $subdatas['1'] = $arr_tmp;
        $subdatas['keyword'] = $this->get_option_data('keyword');
        $subdatas['pricing_category'] = $arr_company;
        $subdatas['4'] = $arr_pricing;
        $subdatas['5'] = array();
        $arr_options_custom =  $this->set_select_data_list('relationship','products');
        $this->set('arr_options_custom', $arr_options_custom);
        $this->set('subdatas', $subdatas);
    }

    function products_pricing($company_id, $key = 0, $product_id = ''){
        $this->selectModel('Company');
        $arr_save = $this->Company->select_one(array('_id' => new MongoId($company_id)));

        if( $product_id != '' ){ // Khi add thêm một product mới
            $this->selectModel('Product');
            $arr_product = $this->Product->select_one(array('_id' => new MongoId($product_id)), array('_id', 'name', 'code'));

            $arr_product['product_id'] = $arr_product['_id'];
            unset( $arr_product['_id'] );
            $arr_product['notes'] = '';
            $arr_product['deleted'] = false;
            $arr_product['price_break'] = array();

            // Kiểm tra xem product đã tồn tại chưa
            $check_not_exist = true;
            if( isset($arr_save['pricing']) ){
                foreach ($arr_save['pricing'] as $pricing_key => $value) {
                    if( (string)$value['product_id'] == $product_id ){
                        $check_not_exist = false;
                        $key = $pricing_key;
                        break;
                    }
                }
            }

            // nếu product chưa tồn tại thì save vào company
            if( $check_not_exist ){
                $arr_save['pricing'][] = $arr_product;
                if (!$this->Company->save($arr_save)) {
                    echo 'Error: ' . $this->Company->arr_errors_save[1]; die;
                }
            }else{

                // nếu đã tồn tại rồi thì lấy lại product cũ và key cũ
                $key = count($arr_save['pricing']) - 1;
                $arr_product = $arr_save['pricing'][$key];
            }

            // key mới (nếu có)
            $key = count($arr_save['pricing']) - 1;

        }else{
            $arr_product = $arr_save['pricing'][$key];
        }
        $this->set('company_id', $company_id);
        $this->set('key', $key);

        // hiển thị thông tin pricing cho người dùng chọn
        $this->set('company_pricing', $arr_save);
        $this->set('arr_product', $arr_product);

        $this->set('return_mod', 1);
        $this->set('return_link', URL.'/companies/entry/'.$company_id);
        $this->set('return_title', 'Pricing: ');
    }

    function products_price_break_add($company_id, $key){

        $this->set('company_id', $company_id);
        $this->set('key', $key);

        $this->selectModel('Company');
        $arr_save = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $arr_save['pricing'][$key]['price_break'][] = array(
            'deleted' => false,
            'range_from' => '',
            'range_to' => '',
            'unit_price' => ''
        );
        $this->selectModel('Company');
        if ($this->Company->save($arr_save)) {
            $this->set('arr_product', $arr_save['pricing'][$key]);
            $this->render('products_price_break');
        } else {
            echo 'Error: ' . $this->Company->arr_errors_save[1]; die;
        }
    }
        function product_pricebreak_save($company_id, $key, $price_break_key){
        if( !empty( $_POST ) ){
            $this->selectModel('Company');
            $arr_save = $this->Company->select_one(array('_id' => new MongoId($company_id)));

            $field = str_replace(array('data[', ']'), '', $_POST['field']);

            $ext = '';
            $value = $_POST['value'];

            if( $field == 'unit_price' ){
                if( substr($value, -3, 1) == '.' ){
                    $ext = substr($value, -3);
                    $value = substr($value, 0, -3);
                }
                $value = (float)(str_replace(array(',', '.'), '', $value).$ext);
            }else{

                $check_price_break = false;
                foreach ($arr_save['pricing'][$key]['price_break'] as $key_price_break => $price_break) {

                    if( $price_break['deleted'] )continue;
                    // if( $value > $price_break['range_from'] && $value < $price_break['range_to'] && $key_price_break != $price_break_key ){
                    //  echo 'The "range to" value must not in range of any existed "range from" to "range to"';
                    //  die;
                    // }

                    // if( $field == 'range_from' && $value > $price_break['range_to'] && $key_price_break == $price_break_key ){
                    //  echo 'The "range from" value must be less than "range to"';
                    //  die;
                    // }

                    // if( $field == 'range_to' && $value < $price_break['range_from'] && $key_price_break == $price_break_key ){
                    //  echo 'The "range to" value must be greater than "range from"';
                    //  die;
                    // }
                }
            }

            $arr_save['pricing'][$key]['price_break'][$price_break_key][$field] = $value;

            $this->selectModel('Company');
            if ($this->Company->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Company->arr_errors_save[1];
            }
        }
        die;
    }

    function products_save_notes($company_id, $key){
        if( !empty( $_POST ) ){
            $this->selectModel('Company');
            $arr_save = $this->Company->select_one(array('_id' => new MongoId($company_id)));
            $arr_save['pricing'][$key]['notes'] = $_POST['notes'];
            $this->selectModel('Company');
            if ($this->Company->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Company->arr_errors_save[1];
            }
        }
        die;
    }

    function other($company_id){
        $this->selectModel('Company');
        $arr_company = array();
        $arr_company = $this->Company->select_one(array('_id'=>new MongoID($company_id)),array('other','include_in_phone_book','profile_type','category','rating','no_of_staff','speed_dial','generate_serials','phone_sort_by','email_so_completed','manufacturer_pn','manufacturer_pn1'));
        //if(!empty($arr_company['other'])) continue;
        $arr_tmp = array();
        if(!empty($arr_company['other']))
        foreach($arr_company['other'] as $key => $value){
            $arr_tmp[$key] = $value;
        }
        $arr_options_custom = $this->set_select_data_list('relationship', 'other');
        $this->set('arr_options_custom', $arr_options_custom);
        $subdatas = array();
        $subdatas['other_detail'] = $arr_tmp;
        $subdatas['2'] = array();
        $subdatas['profile'] = $arr_company;
        $this->set('subdatas', $subdatas);
    }

    function other_add($company_id) {
        $this->selectModel('Company');
        $this->Company->collection->update(
                array('_id' => new MongoId($company_id)), array(
                    '$push' => array(
                        'other' => array(
                            'heading' => '',
                            'details' => '',
                            'deleted' => false
                        )
                    )
                )
        );
        $this->other($company_id);
        $this->render('other');
    }

    function salesinvoice_add($company_id){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        $this->selectModel('Salesinvoice');
        $arr_tmp = $this->Salesinvoice->select_one(array(), array(), array('code' => -1));
        $arr_save=array();

        $arr_save = $this->arr_associated_data('company_name',$arr_company['name'], $company_id);

        $arr_save['payment_terms']=isset($arr_company['account']['payment_terms'])?$arr_company['account']['payment_terms']:0;
        if(isset($arr_save['payment_terms']))
            $arr_save['payment_due_date'] = new MongoDate((int)$arr_save['payment_terms']*86400 + (int)time());

        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }else
        {
            $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $this->selectModel('Salesaccount');
        $salesaccount = $this->Salesaccount->select_one(array('company_id' => new MongoId($company_id)));
        if( isset($salesaccount['_id']) ){
            $arr_save['payment_terms'] = $salesaccount['payment_terms'];
            $arr_save['tax'] = $salesaccount['tax_code'];
        }

        $this->Salesinvoice->arr_default_before_save = $arr_save;
        // BaoNam: sửa ngày 02/12/2013
        if ($this->Salesinvoice->add()) {
             echo URL .'/salesinvoices/entry/'. $this->Salesinvoice->mongo_id_after_save;
        }else
            echo URL . '/salesinvoices/entry';
        die;
    }

    function salesinvoice_add_options($company_id){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)));
        //pr($arr_company);die;
        $this->selectModel('Salesinvoice');
        $arr_tmp = $this->Salesinvoice->select_one(array(), array(), array('code' => -1));
        $arr_save=array();

        $arr_save = $this->arr_associated_data('company_name',$arr_company['name'], $company_id);

        $arr_save['payment_terms']=isset($arr_company['account']['payment_terms'])?$arr_company['account']['payment_terms']:0;
        if(isset($arr_save['payment_terms']))
            $arr_save['payment_due_date'] = new MongoDate((int)$arr_save['payment_terms']*86400 + (int)time());

        if(isset($arr_company['our_csr_id'])){
            $arr_save['our_csr'] = isset($arr_company['our_csr'])?$arr_company['our_csr']:'';
            $arr_save['our_csr_id'] = new MongoId($arr_company['our_csr_id']);
        }else
        {
            $arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_csr_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        if(isset($arr_company['our_rep_id'])){
            $arr_save['our_rep'] = isset($arr_company['our_rep'])?$arr_company['our_rep']:'';
            $arr_save['our_rep_id'] = new MongoId($arr_company['our_rep_id']);
        }else
        {
            $arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
            $arr_save['our_rep_id'] = new MongoId($_SESSION['arr_user']['contact_id']);
        }

        $arr_save['code'] = 1;
        if (isset($arr_tmp['code'])) {
            $arr_save['code'] = $arr_tmp['code'] + 1;
        }

        $this->selectModel('Salesaccount');
        $salesaccount = $this->Salesaccount->select_one(array('company_id' => new MongoId($company_id)));
        if( isset($salesaccount['_id']) ){
            $arr_save['payment_terms'] = $salesaccount['payment_terms'];
            $arr_save['tax'] = $salesaccount['tax_code'];
        }

        $this->Salesinvoice->arr_default_before_save = $arr_save;
        // BaoNam: sửa ngày 02/12/2013
        if ($this->Salesinvoice->add()) {
            $this->redirect('/salesinvoices/entry/'. $this->Salesinvoice->mongo_id_after_save);
        }
        $this->redirect('/salesinvoices/entry');
    }

    function account($company_id){
        $this->set('company_id', $company_id);
        $this->selectModel('Salesinvoice');
        $arr_selectinvoice = $this->Salesinvoice->select_all(array(
                                                             'arr_where' => array('company_id'=>new MongoId($company_id)),
                                                             ));
        $arr_salesinvoice = array();
        foreach($arr_selectinvoice as $key => $value){
            $arr_salesinvoice[$key] = $value;
        }

        $this->set_select_data_list('relationship','account');
        $subdatas = array();
        $subdatas['sales_invoice_this_company'] = $arr_salesinvoice;
        $subdatas['account_related'] = array();
        $this->set('subdatas', $subdatas);
        $this->set('button_account', 'add'); //add,view

        $this->selectModel('Setting');
        $this->set( 'arr_status', $this->Setting->select_option(array('setting_value' => 'salesaccounts_status'), array('option')) );
        $this->set( 'arr_payment_terms', $this->Setting->select_option(array('setting_value' => 'salesaccounts_payment_terms'), array('option')) );

        $this->selectModel('Tax');
        $this->set( 'arr_tax_code', $this->Tax->tax_select_list());
        $this->set( 'arr_nominal_code', $this->Setting->select_option(array('setting_value' => 'salesaccounts_nominal_code'), array('option')) );

        $this->selectModel('Salesaccount');
        $salesaccount = $this->Salesaccount->select_one(array('company_id' => new MongoId($company_id)));

        if( isset($salesaccount['_id']) ){
            $arr_tmp['Salesaccount'] = $salesaccount;
            $arr_tmp['Salesaccount']['difference'] = 0;
            if( strlen($arr_tmp['Salesaccount']['credit_limit']) > 0 ){
                $arr_tmp['Salesaccount']['difference'] = number_format($arr_tmp['Salesaccount']['credit_limit'] - $arr_tmp['Salesaccount']['balance'], 2);
            }
            if( strlen($arr_tmp['Salesaccount']['quotation_limit']) > 0 ){
                $arr_tmp['Salesaccount']['quotation_limit'] = number_format($arr_tmp['Salesaccount']['quotation_limit'], 2);
            }

            $arr_tmp['Salesaccount']['balance'] = (is_numeric($arr_tmp['Salesaccount']['balance']))?number_format($arr_tmp['Salesaccount']['balance'], 2):'';
            $arr_tmp['Salesaccount']['credit_limit'] = (is_numeric($arr_tmp['Salesaccount']['credit_limit']))?number_format($arr_tmp['Salesaccount']['credit_limit'], 2):'';
            $this->data = $arr_tmp;
        }
        $this->selectModel('Contact');
        $this->set('model_contact', $this->Contact);
    }

    function account_create($company_id){
        $this->selectModel('Salesaccount');
        $arr_tmp['company_id'] = new MongoId($company_id);
        $this->Salesaccount->arr_default_before_save = $arr_tmp;
        $salesaccount = $this->Salesaccount->add();
        $salesaccount['balance'] = number_format($salesaccount['balance'],2);
        $salesaccount['quotation_limit'] = number_format($salesaccount['quotation_limit'],2);
        $tmp['Salesaccount'] = $salesaccount;
        $this->data = $tmp;
    }

    function account_auto_save($company_id){
        if (!empty($this->data)) {
            $this->selectModel('Salesaccount');
            $arr_save = $this->Salesaccount->select_one(array('company_id' => new MongoId($company_id)));
            foreach ($this->data['Salesaccount'] as $key => $value) {
                if( $key == 'balance' )continue;
                $arr_save[$key] = $value;
                if( $key == 'credit_limit' || $key == 'quotation_limit' ){
                    $ext = '';
                    if( substr($value, -3, 1) == '.' ){
                        $ext = substr($value, -3);
                        $value = substr($value, 0, -3);
                    }
                    $arr_save[$key] = (float)(str_replace(array(',', '.'), '', $value).$ext);
                }
            }
            if ($this->Salesaccount->save($arr_save)) {
                // cập nhật lại toàn bộ khung html------------Account Related-----------
                $arr_tmp = $arr_save;
                $arr_tmp['Salesaccount'] = $arr_save;
                if( strlen($arr_tmp['Salesaccount']['credit_limit']) > 0 ){
                    $arr_tmp['Salesaccount']['difference'] = number_format($arr_tmp['Salesaccount']['credit_limit'] - $arr_tmp['Salesaccount']['balance'], 2);
                }
                $arr_tmp['Salesaccount']['balance'] = (is_numeric($arr_tmp['Salesaccount']['balance']))?number_format($arr_tmp['Salesaccount']['balance'], 2):'';
                $arr_tmp['Salesaccount']['credit_limit'] = (is_numeric($arr_tmp['Salesaccount']['credit_limit']))?number_format($arr_tmp['Salesaccount']['credit_limit'], 2):'';
                //An them truong quotation limit
                $arr_tmp['Salesaccount']['quotation_limit'] = (is_numeric($arr_tmp['Salesaccount']['quotation_limit']))?number_format($arr_tmp['Salesaccount']['quotation_limit'], 2):'';
                $this->data = $arr_tmp;
                $this->selectModel('Setting');
                $this->set( 'arr_status', $this->Setting->select_option(array('setting_value' => 'salesaccounts_status'), array('option')) );
                $this->set( 'arr_payment_terms', $this->Setting->select_option(array('setting_value' => 'salesaccounts_payment_terms'), array('option')) );
                $this->selectModel('Tax');
                $this->set( 'arr_tax_code', $this->Tax->tax_select_list());
                $this->set( 'arr_nominal_code', $this->Setting->select_option(array('setting_value' => 'salesaccounts_nominal_code'), array('option')) );
                echo $this->render('account_related'); die;
                // end cập nhật ---------------------------------------------------

            } else {
                echo 'Error: ' . $this->Salesaccount->arr_errors_save[1];
            }
        }
        die;
    }

    public function popup($key = "") {
        $this->set('key', $key);

        $limit = 100; $skip = 0;
        $page_num = 1;
        if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0){

            // $limit = $_POST['pagination']['page-list'];
            $page_num = $_POST['pagination']['page-num'];
            $limit = $_POST['pagination']['page-list'];
            $skip = $limit*($page_num - 1);
        }
        $this->set('page_num', $page_num);
        $this->set('limit', $limit);

        $arr_order = array('no' => 1);
        if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
            $sort_type = 1;
            if( $_POST['sort']['type'] == 'desc' ){
                $sort_type = -1;
            }
            $arr_order = array($_POST['sort']['field'] => $sort_type);

            $this->set('sort_field', $_POST['sort']['field']);
            $this->set('sort_type', ($sort_type === 1)?'asc':'desc');
            $this->set('sort_type_change', ($sort_type === 1)?'desc':'asc');
        }

        $cond = array();
        $cond['inactive'] = 0;
        if (!empty($this->data)) {
            $arr_post = $this->data['Company'];

            if (strlen($arr_post['name']) > 0)
                $cond['name'] = new MongoRegex('/'. (string)$arr_post['name'] .'/i');

            if( isset($arr_post['inactive']) && $arr_post['inactive'] )
                $cond['inactive'] = 1;

            if ( isset($arr_post['is_customer']) &&is_numeric($arr_post['is_customer']) && $arr_post['is_customer']) {
                $cond['is_customer'] = 1;
            }

            if ( isset($arr_post['is_supplier']) &&is_numeric($arr_post['is_supplier']) && $arr_post['is_supplier']) {
                $cond['is_supplier'] = 1;
            }

            if (isset($arr_post['is_shipper'])) {
                $cond['is_shipper'] = 1;
                $this->set( 'is_shipper', 1 );
            }


        }

        if (!empty($_GET)) {

            $tmp = $this->data;

            if (isset($_GET['is_customer'])) {
                $cond['is_customer'] = 1;
                $tmp['Company']['is_customer'] = 1;
            }

            if (isset($_GET['is_supplier'])) {
                $cond['is_supplier'] = 1;
                $tmp['Company']['is_supplier'] = 1;
            }

            if (isset($_GET['name'])) {

                $cond['name'] = new MongoRegex('/'. $_GET['name'] .'/i');
                $tmp['Company']['name'] = $_GET['name'];
            }

            if (isset($_GET['is_shipper']) && is_numeric($_GET['is_shipper']) && $_GET['is_shipper']) {
                $cond['is_shipper'] = 1;
                $this->set( 'is_shipper', 1 );
            }

            $this->data = $tmp;
        }

        $this->selectModel('Company');
        $arr_companies = $this->Company->select_all(array(
            'arr_where' => $cond,
            'arr_order' => $arr_order,
            'arr_field' => array('name', 'addresses', 'addresses_default_key', 'is_customer', 'is_supplier', 'default_address_1', 'default_address_2', 'default_address_3', 'default_town_city', 'default_country_name', 'default_country', 'default_province_state_name', 'default_province_state', 'default_zip_postcode', 'phone', 'fax', 'email', 'web','our_rep','our_rep_id'),
            'limit' => $limit,
            'skip' => $skip
        ));
        $this->set('arr_company', $arr_companies);

        $total_page = $total_record = $total_current = 0;
        if( is_object($arr_companies) ){
            $total_current = $arr_companies->count(true);
            $total_record = $arr_companies->count();
            if( $total_record%$limit != 0 ){
                $total_page = floor($total_record/$limit) + 1;
            }else{
                $total_page = $total_record/$limit;
            }
        }
        $this->set('total_current', $total_current);
        $this->set('total_page', $total_page);
        $this->set('total_record', $total_record);

        $this->layout = 'ajax';
    }

    public function popup_addresses($key = "") {

        $this->set('key', $key);

        if (!empty($_GET)) {
            if (isset($_GET['company_id'])) {
                $cond = array();
                $cond['_id'] = new MongoId($_GET['company_id']);
                $this->selectModel('Company');
                $this->set('arr_company', $this->Company->select_one($cond, array('_id', 'addresses')));
            }
        }

        if (!empty($_GET)) {
            if (isset($_GET['contact_id'])) {
                $cond = array();
                $cond['_id'] = new MongoId($_GET['contact_id']);
                $this->selectModel('Contact');
                $this->set('arr_contact', $this->Contact->select_one($cond, array('_id', 'addresses')));
            }
        }

        $this->layout = 'ajax';
    }
    // Popup form orther module

    public function view_minilist(){
        $arr_where = array();
        $arr_data = array();
        if($this->Session->check('companies_entry_search_cond'))
            $arr_where = $this->Session->read('companies_entry_search_cond');
        $this->selectModel('Company');
        $companies = $this->Company->select_all(array(
                                   'arr_where'  =>  $arr_where,
                                   'arr_field'  =>  array('is_customer','is_supplier','no','name','contact_default_id','phone','email'),
                                   'arr_order'  =>  array('_id'=>1),
                                   'limit'      =>  5000
                                   ));
        if($companies->count()>0){
            $group = array();
            $html = '';
            $i = 0;
            $this->selectModel('Contact');
            foreach($companies as $company){
                $type = '';
                $contact = '';
                if(isset($company['is_customer'])&&$company['is_customer']==1)
                    $type ='<p>Customer</p>';
                if(isset($company['is_supplier'])&&$company['is_supplier']==1)
                    $type .='<p>Supllier</p>';
                if(isset($company['contact_default_id'])&&is_object($company['contact_default_id'])){
                    $contact = $this->Contact->select_one(array('_id'=>$company['contact_default_id']));
                }
                $html .= '<tr class="'.($i%2==0 ? 'bg_2' : 'bg_1').'" style="valign:middle">';
                $html .= '<td>'.$type.'</td>';
                $html .= '<td class="center_text">'.(isset($company['no']) ? $company['no'] : '').'</td>';
                $html .= '<td>'.$company['name'].'</td>';
                $html .= '<td>'.(isset($contact['full_name']) ? $contact['full_name'] : '').'</td>';
                $html .= '<td>'.(isset($company['phone']) ? $company['phone'] : '').'</td>';
                $html .= '<td>'.(isset($contact['mobile']) ? $contact['mobile'] : '').'</td>';
                $html .= '<td>'.(isset($company['email']) ? $company['email'] : ( isset($contact['email']) ? $contact['email'] : '' ) ).'</td>';
                $html .= '</tr>';
                $i++;
            }
            $html .= '<tr class="last"><td class="bold_text" colspan="7">'.$i.' record(s) listed.</td></tr>';
            $arr_data['title'] = array('Type'=>'width: 10%','Ref no'=>'width: 7%;','Company'=>'width: 40%','Contact'=>'width: 15%','Phone','Mobile','Email');
            $arr_data['content'] = $html;
            $arr_data['report_name'] = 'Company Mini Listing (with main company)';
            $arr_data['report_file_name']='COM_'.md5(time());
            $arr_data['report_orientation'] = 'landscape';
        }
        $this->render_pdf($arr_data);

    }

    function create_email(){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($this->get_id())));

        $arr_save = array();
        $this->selectModel('Communication');
        $arr_save['code'] = $this->Communication->get_auto_code('code');

        $arr_save['comms_type'] = 'Email';

        $arr_save['company_id'] = isset($arr_company['_id'])?$arr_company['_id']:'';
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['module']=isset($this->params->params['controller'])?$this->params->params['controller']:'';

        $this->selectModel('Contact');
        $arr_contact = $arr_temp = array();
        if(isset($arr_company['contact_id']) && is_object($arr_company['contact_id'])){
            $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($arr_company['contact_id'])));
        }
        else{
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=> new MongoId($arr_company['_id'])),
                'arr_order' => array('_id'=> -1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arr_temp) > 0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
        }
        if(isset($arr_contact['_id'])){
            $arr_save['contact_name'] = isset($arr_contact['first_name'])?$arr_contact['first_name']:'';
            $arr_save['last_name']= isset($arr_contact['last_name'])?$arr_contact['last_name']:'';
            $arr_save['contact_id'] = $arr_contact['_id'];
        }
        else{
            $arr_save['contact_name']='';
            $arr_save['contact_id']='';
        }
        $arr_save['contact_from_id']=$this->Contact->user_id();
        $arr_save['contact_from']=$this->Contact->user_name();

        if ($this->Communication->save($arr_save)) {
            $this->redirect('/communications/entry/'. $this->Communication->mongo_id_after_save);
        }
        $this->redirect('/communications/entry');
    }

    function create_fax(){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($this->get_id())));

        $arr_save = array();
        $this->selectModel('Communication');
        $arr_save['code'] = $this->Communication->get_auto_code('code');

        $arr_save['comms_type'] = 'Fax';

        $arr_save['company_id'] = isset($arr_company['_id'])?$arr_company['_id']:'';
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['created_from']=isset($this->params->params['controller'])?$this->params->params['controller']:'';
        $arr_save['fax']=isset($arr_company['fax'])?$arr_company['fax']:'';
        $arr_save['phone']=isset($arr_company['phone'])?$arr_company['phone']:'';

        $this->selectModel('Contact');
        $arr_contact = $arr_temp = array();
        if(isset($arr_company['contact_id']) && is_object($arr_company['contact_id'])){
            $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($arr_company['contact_id'])));
        }
        else{
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=> new MongoId($arr_company['_id'])),
                'arr_order' => array('_id'=> -1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arr_temp) > 0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
        }
        if(isset($arr_contact['_id'])){
            $arr_save['contact_name'] = isset($arr_contact['first_name'])?$arr_contact['first_name']:'';
            $arr_save['last_name']= isset($arr_contact['last_name'])?$arr_contact['last_name']:'';
            $arr_save['contact_id'] = $arr_contact['_id'];
        }
        else{
            $arr_save['contact_name']='';
            $arr_save['contact_id']='';
        }
        $arr_save['contact_from_id']=$this->Contact->user_id();
        $arr_save['contact_from']=$this->Contact->user_name();

        if ($this->Communication->save($arr_save)) {
            $this->redirect('/communications/entry/'. $this->Communication->mongo_id_after_save);
        }
        $this->redirect('/communications/entry');
    }

    function create_letter(){
        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($this->get_id())));

        $arr_save = array();
        $this->selectModel('Communication');
        $arr_save['code'] = $this->Communication->get_auto_code('code');

        $arr_save['comms_type'] = 'Letter';

        $arr_save['company_id'] = isset($arr_company['_id'])?$arr_company['_id']:'';
        $arr_save['company_name'] = isset($arr_company['name'])?$arr_company['name']:'';
        $arr_save['email'] = isset($arr_company['email'])?$arr_company['email']:'';
        $arr_save['created_from']=isset($this->params->params['controller'])?$this->params->params['controller']:'';
        $arr_save['fax']=isset($arr_company['fax'])?$arr_company['fax']:'';
        $arr_save['phone']=isset($arr_company['phone'])?$arr_company['phone']:'';

        $this->selectModel('Contact');
        $arr_contact = $arr_temp = array();
        if(isset($arr_company['contact_id']) && is_object($arr_company['contact_id'])){
            $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($arr_company['contact_id'])));
        }
        else{
            $arr_contact = $this->Contact->select_all(array(
                'arr_where' => array('company_id'=> new MongoId($arr_company['_id'])),
                'arr_order' => array('_id'=> -1),
            ));
            $arrtemp = iterator_to_array($arr_contact);
            if(count($arr_temp) > 0){
                $arr_contact = current($arrtemp);
            }else
                $arr_contact = array();
        }
        if(isset($arr_contact['_id'])){
            $arr_save['contact_name'] = isset($arr_contact['first_name'])?$arr_contact['first_name']:'';
            $arr_save['last_name']= isset($arr_contact['last_name'])?$arr_contact['last_name']:'';
            $arr_save['contact_id'] = $arr_contact['_id'];
        }
        else{
            $arr_save['contact_name']='';
            $arr_save['contact_id']='';
        }
        $arr_save['contact_from_id']=$this->Contact->user_id();
        $arr_save['contact_from']=$this->Contact->user_name();

        if ($this->Communication->save($arr_save)) {
            $this->redirect('/communications/entry/'. $this->Communication->mongo_id_after_save);
        }
        $this->redirect('/communications/entry');
    }

}