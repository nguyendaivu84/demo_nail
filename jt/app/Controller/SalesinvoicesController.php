<?php
// Attach lib cal_price
App::import('Vendor','cal_price/cal_price');

App::uses('AppController', 'Controller');
class SalesinvoicesController extends AppController {

	var $name = 'Salesinvoices';
	public $helpers = array();
	public $opm; //Option Module
	public $cal_price; //Option cal_price
	var $modelName = 'Salesinvoice';

	public function beforeFilter(){
		parent::beforeFilter();
		$this->set_module_before_filter('Salesinvoice');

	}

	//Các điều kiện mở/khóa field trong entry
	public function check_lock(){
		if($this->get_id()!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			if(isset($arr_tmp['invoice_status'])&&$arr_tmp['invoice_status']!='In Progress')
				return true;

		}else
			return false;
	}

	/*
	 * Hàm dùng để điều chỉnh setting(giao diện)/phân quyền
	 */
	public function rebuild_setting($arr_setting=array()){
		parent::rebuild_setting($arr_setting=array());
		$arr_setting = $this->opm->arr_settings;
		$iditem = $this->get_id();
		$this->opm->arr_settings = $arr_setting;
		$query = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('job_id'));
		if(isset($query['job_id'])&&is_object($query['job_id'])){
			$this->selectModel('Job');
			$job = $this->Job->select_one(array('_id'=> new MongoId($query['job_id'])),array('status'));
			if(isset($job['status']) && $job['status'] == 'Completed'){
				$this->opm->set_lock(array('name'), 'out');
				$this->set('address_lock', '1');
				$this->opm->set_lock_option('line_entry', 'products');
				$this->opm->set_lock_option('text_entry', 'products');
			}
		}
	}

	function ajax_save(){
		if( isset($_POST['field'])){
			if($_POST['field'] == 'code'){
				if($_POST['func']=='update'&&!$this->check_permission($this->name.'_@_entry_@_edit')){
					echo 'You do not have permission on this action.';
					die;
				}
				$values = $_POST['value'];
				$value = $values['value'];
				$password = $values['password'];
				$this->selectModel('Stuffs');
				$change = $this->Stuffs->select_one(array('value'=>'Changing Code'));
				if(md5($password)!=$change['password']){
					echo 'wrong_pass';
					die;
				}
				$ids = $this->opm->update($_POST['ids'], $_POST['field'], $value);
				die;
			} else if($_POST['field'] == 'invoice_status'){
				$query = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('company_id','sum_amount','sum_sub_total','taxval'));
				if($field == 'Invoiced')
					$this->update_balace_credit_salesaccount($query);
				else
					$this->update_balace_credit_salesaccount($query,'minus');
			}
		}
		parent::ajax_save();
	}

	public function create_salesinvoice_from_salesorder($salesorder_id){
		if(!$this->check_permission('salesinvoices_@_entry_@_add'))
			$this->error_auth();
		$this->selectModel('Salesorder');
		$arr_salesorder=array();
		if($salesorder_id!=''){
			$arr_salesorder = $this->Salesorder->select_one(array('_id' => new MongoId($salesorder_id)));
			$this->selectModel('Salesinvoice');
            $si = $arr_salesorder;
            $si['code'] = $this->opm->get_auto_code('code');
            $si['invoice_date'] = new MongoDate();
            $si['invoice_status'] = 'In Progress';
            $si['invoice_type'] = 'Invoice';
            $si['salesorder_id'] = new MongoId($salesorder_id);
            $si['salesorders'][0] = $si['salesorder_id'];
            $si['salesorder_number'] = $arr_salesorder['code'];
            $si['salesorder_name'] = $arr_salesorder['name'];
            unset($si['_id']);
            unset($si['created_by']);
            unset($si['date_modified']);
            unset($si['modified_by']);
            unset($si['salesorder_date']);
            unset($si['salesorder_status']);
            unset($si['salesorder_type']);
            if($this->opm->save($si)){
                $id = $this->Salesinvoice->mongo_id_after_save;
                $arr_salesorder['salesinvoice_id'] = new MongoId($id);
                $arr_salesorder['salesinvoice_code'] = $si['code'];
                if(is_array($arr_salesorder['products'])){
					foreach($arr_salesorder['products'] as $key=>$value){
						if($value['deleted']) continue;
						$arr_salesorder['products'][$key]['invoiced']=(int)$value['quantity'];
						$arr_salesorder['products'][$key]['balance_invoiced']=0;

					}
				}
				if(isset($arr_salesorder['sum_amount']) && $arr_salesorder['sum_amount'] > 0){
					$this->selectModel('Salesaccount');
					if(isset($arr_salesorder['company_id']) && is_object($arr_salesorder['company_id'])){
						$this->Salesaccount->update_account($arr_salesorder['company_id'], array(
															'model' => 'Company',
															'balance' => $arr_salesorder['sum_amount'],
															'invoices_credits' => $arr_salesorder['sum_amount'],
															));
					}elseif(isset($arr_salesorder['contact_id'])){
						$this->Salesaccount->update_account($arr_salesorder['contact_id'], array(
															'model' => 'Contact',
															'balance' => $arr_salesorder['sum_amount'],
															'invoices_credits' => $arr_salesorder['sum_amount'],
															));
					}
				}
                $this->Salesorder->save($arr_salesorder);
                $this->redirect('/salesinvoices/entry/'.$id);
            }
		}
	}
	public function entry(){
		$mod_lock = '0';
		if($this->check_lock()){
			$this->opm->set_lock(array('invoice_status'),'out');
			$mod_lock = '1';
			$this->set('address_lock','1');
			$this->opm->set_lock_option('line_entry','products');
			$this->opm->set_lock_option('text_entry','products');

		}

		$arr_set = $this->opm->arr_settings;
		// Get value id
		$iditem = $this->get_id();
		if($iditem=='')
			$iditem = $this->get_last_id();

		$this->set('iditem',$iditem);
		//Load record by id
		if($iditem!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
			if(isset($arr_tmp['shipping_id']) && is_object($arr_tmp['shipping_id']))
				$this->set('shipping_id',$arr_tmp['shipping_id']);
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($arr_tmp[$field])){
						$arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
						if(in_array($field,$arr_set['title_field']))
							$item_title[$field] = $arr_tmp[$field];

						if(preg_match("/_date$/",$field) && is_object($arr_tmp[$field]))
							$arr_set['field'][$ks][$field]['default'] = date('m/d/Y',$arr_tmp[$field]->sec);
						//chế độ lock, hiện name của các relationship custom
						else if(($field=='company_name' || $field=='contact_name') && $mod_lock=='1')
							$arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];

						else if($this->opm->check_field_link($ks,$field)){
							$field_id = $arr_set['field'][$ks][$field]['id'];
							if(!isset($arr_set['field'][$ks][$field]['syncname']))
								$arr_set['field'][$ks][$field]['syncname'] = 'name';
							$arr_set['field'][$ks][$field]['default'] = $this->get_name($this->ModuleName($arr_set['field'][$ks][$field]['cls']),$arr_tmp[$field_id],$arr_set['field'][$ks][$field]['syncname']);

						}else if($field=='company_name' && isset($arr_tmp['company_id']) && $arr_tmp['company_id']!=''){
							$arr_set['field'][$ks][$field]['default'] = $this->get_name('Company',$arr_tmp['company_id']);

						}else if($field=='contact_name' && isset($arr_tmp['contact_id']) && $arr_tmp['contact_id']!=''){
							$arr_set['field'][$ks][$field]['default'] = $this->get_name('Contact',$arr_tmp['contact_id']);
							$item_title[$field] = $this->get_name('Contact',$arr_tmp['contact_id']);
						}
					}
				}
			}
			$this->set('invoice_status',(isset($arr_set['field']['panel_4']['invoice_status']['default']) ? $arr_set['field']['panel_4']['invoice_status']['default'] : ''));
			$arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
			$this->Session->write($this->name.'ViewId',$iditem);

			//BEGIN custom
			if(isset($arr_set['field']['panel_1']['code']['default']))
				$item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
			else
				$item_title['code'] = '1';
			$this->set('item_title',$item_title);

			//custom list tax
			$arr_options_custom['tax'] = '';
			$this->selectModel('Tax');
			$arr_options_custom['tax'] = $this->Tax->tax_select_list();
			$this->set('arr_options_custom',$arr_options_custom);
			//END custom

			//show footer info
			$this->show_footer_info($arr_tmp);


		//add, setup field tự tăng
		}else{
			$nextcode = $this->opm->get_auto_code('code');
			$arr_set['field']['panel_1']['code']['default'] = $nextcode;
			$this->set('item_title',array('code'=>$nextcode));
		}
		$this->set('arr_settings',$arr_set);
		$this->sub_tab_default = 'line_entry';
		$this->sub_tab('',$iditem);
		$this->set_entry_address($arr_tmp,$arr_set);

		parent::entry();
	}


	function arr_associated_data($field = '', $value = '', $valueid = '' , $fieldopt='') {
		$arr_return = array();
		$arr_return[$field]=$value;
		/**
		* Chọn Company
		*/
		if(isset($_POST['arr']) && is_string($_POST['arr']) && $_POST['arr']!='')
			$tmp_data = (array)json_decode($_POST['arr']);
		if(isset($tmp_data['keys'])&&$tmp_data['keys']=='update'
			&&!$this->check_permission($this->name.'_@_entry_@_edit')){
			echo 'You do not have permission on this action.';
			die;
		} else if (isset($tmp_data['keys'])&&$tmp_data['keys']=='add'
			&&!$this->check_permission($this->name.'_@_entry_@_add')){
			echo 'You do not have permission on this action.';
			die;
		}
		if($field == 'company_name' && $valueid !=''){
			$arr_return = array(
				'company_name'	=>'',
				'company_id'	=>'',
				'contact_name'	=>'',
				'contact_id'	=>'',
				'our_csr'		=>'',
				'our_csr_id'	=>'',
				'our_rep'		=>'',
				'our_rep_id'	=>'',
				'phone'			=>'',
				'email'			=>'',
				'invoice_address' => array(),
				'shipping_address' => array(),
			);
			//change company
			$arr_return['company_name'] = $value;
			$arr_return['company_id'] = new MongoId($valueid);

			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $arr_return['company_id']));
			$arr_return['payment_terms'] = (isset($salesaccount['payment_terms']) ? $salesaccount['payment_terms'] : 0);
			$arr_return['payment_terms_id'] = (isset($salesaccount['payment_terms_id']) ? $salesaccount['payment_terms_id'] : 0);

			$salesinvoice = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
			$arr_return['name'] = $salesinvoice['code'].'-'.$value;
			$arr_return['payment_due_date'] = (int)$arr_return['payment_terms_id']*86400 + (int)$salesinvoice['invoice_date']->sec;
			$arr_return['payment_due_date'] = new MongoDate($arr_return['payment_due_date']);

			//find contact and more from Company
			$this->selectModel('Company');
			$arr_company = $this->Company->select_one(array('_id'=>new MongoId($valueid)));

			$this->selectModel('Contact');
			$arr_contact = $arrtemp = array();
			// is set contact_default_id
			if(isset($arr_company['contact_default_id']) && is_object($arr_company['contact_default_id'])){
				$arr_contact = $this->Contact->select_one(array('_id'=>$arr_company['contact_default_id']));

			// not set contact_default_id
			}else{
				$arr_contact = $this->Contact->select_all(array(
					'arr_where' => array('company_id'=>new MongoId($valueid)),
					'arr_order' => array('_id'=>-1),
				));
				$arrtemp = iterator_to_array($arr_contact);
				if(count($arrtemp)>0){
					$arr_contact = current($arrtemp);
				}else
					$arr_contact = array();
			}
			//change contact
			if(isset($arr_contact['_id']))
			{
				$arr_return['contact_name']=$arr_contact['first_name'].' '.$arr_contact['last_name'];
				$arr_return['contact_id'] = $arr_contact['_id'];
			}
			else
			{
				$arr_return['contact_name']='';
				$arr_return['contact_id'] = '';
			}




			//change our_csr
			if(isset($arr_company['our_csr']) && isset($arr_company['our_csr_id']) && $arr_company['our_csr_id']!=''){
				$arr_return['our_csr_id'] = $arr_company['our_csr_id'];
				$arr_return['our_csr'] = $arr_company['our_csr'];
			}else{
				$arr_return['our_csr_id'] = $this->opm->user_id();
				$arr_return['our_csr'] = $this->opm->user_name();
			}


			//change our_rep
			if(isset($arr_company['our_responsible']) && isset($arr_company['our_responsible_id']) && $arr_company['our_responsible_id']!=''){
				$arr_return['our_rep_id'] = $arr_company['our_responsible_id'];
				$arr_return['our_rep'] = $arr_company['our_responsible'];
			}else{
				$arr_return['our_rep_id'] = $this->opm->user_id();
				$arr_return['our_rep'] = $this->opm->user_name();
			}

			//change our_rep
			if(isset($arr_company['our_rep']) && isset($arr_company['our_rep_id']) && $arr_company['our_rep_id']!=''){
				$arr_return['our_rep_id'] = $arr_company['our_rep_id'];
				$arr_return['our_rep'] = $arr_company['our_rep'];
			}else{
				$arr_return['our_rep_id'] = $this->opm->user_id();
				$arr_return['our_rep'] = $this->opm->user_name();
			}


			//change phone
			if(isset($arr_company['phone']))
				$arr_return['phone'] = $arr_company['phone'];
			else  // neu khong co phone thi lay phone cua contact mac dinh
			{
				if(isset($arr_contact['direct_dial']))
					$arr_return['phone']=$arr_contact['direct_dial'];
				elseif(!isset($arr_contact['direct_dial'])&&isset($arr_contact['mobile']))
					$arr_return['phone']=$arr_contact['mobile'];
				elseif(!isset($arr_contact['direct_dial'])&&!isset($arr_contact['mobile']))
					$arr_return['phone']='';  //bat buoc phai co dong nay khong thi no se lay du lieu cua cty truoc
			}


			if(isset($arr_company['email'])  && $arr_company['email']!='')
				$arr_return['email'] = $arr_company['email'];
			elseif (isset($arr_contact['email']))
				$arr_return['email']=$arr_contact['email'];
			elseif  (!isset($arr_contact['email']))
				$arr_return['email']='';

			if(isset($arr_company['fax']))
				$arr_return['fax'] = $arr_company['fax'];
			elseif (isset($arr_contact['fax']))
				$arr_return['fax']=$arr_contact['fax'];
			elseif  (!isset($arr_contact['fax']))
				$arr_return['fax']='';


			//change address
			if(isset($arr_company['addresses_default_key']))
				$add_default = $arr_company['addresses_default_key'];
			if(isset($add_default) && isset($arr_company['addresses'][$add_default])){
				foreach($arr_company['addresses'][$add_default] as $ka=>$va){
					if($ka!='deleted')
						$arr_return['invoice_address'][0]['invoice_'.$ka] = $va;
					else
						$arr_return['invoice_address'][0][$ka] = $va;
				}
			}

		/**
		* Chọn Contact
		*/
		}else if($field == 'contact_name' && $valueid !=''){
			$arr_return = array(
				'contact_name'	=>'',
				'contact_id'	=>'',
				'phone'			=>'',
				'email'			=>'',
			);
			//change company
			$arr_return['contact_name'] = $value;
			$arr_return['contact_id'] = new MongoId($valueid);
			//find more from contact
			$this->selectModel('Contact');
			$arr_contact = $this->Contact->select_one(array('_id'=>new MongoId($valueid)));
			//change phone
			if(isset($arr_contact['direct_dial']) && $arr_contact['direct_dial']!='')
				$arr_return['phone'] = $arr_contact['direct_dial'];
			//change email
			if(isset($arr_contact['email']))
				$arr_return['email'] = $arr_contact['email'];
			//nếu company khác hiện có
			if(isset($arr_contact['company_id'])){
				echo '';
			}

		/**
		* Thay đổi Payment terms
		*/
		}else if($field == 'payment_terms' && $this->get_id()!=''){
			$salesinvoice = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
			if(isset($salesinvoice['invoice_date'])){
				$arr_return['payment_due_date'] = (int)$value*86400 + (int)$salesinvoice['invoice_date']->sec;
				$arr_return['payment_due_date'] = new MongoDate($arr_return['payment_due_date']);
			}

		/**
		* Thay đổi invoice_date
		*/
		}else if($field == 'invoice_date' && $this->get_id()!=''){
			$salesinvoice = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
			if(empty($salesinvoice['payment_terms']))
				$salesinvoice['payment_terms'] = 0;
			$arr_return['payment_due_date'] = (int)$salesinvoice['payment_terms']*86400 + (int)$salesinvoice['invoice_date'];
			$arr_return['payment_due_date'] = new MongoDate($arr_return['payment_due_date']);

		}
		if($field == 'products'){

			if(isset($value[$valueid]) && isset($value[$valueid]['products_id']) && is_object($value[$valueid]['products_id']) && $fieldopt!='code' && $fieldopt!='deleted'){
				//change size other


			//giam gia cho product parrent neu la xoa item option
			}else if(isset($value[$valueid]) && isset($value[$valueid]['products_id']) && is_object($value[$valueid]['products_id']) && $fieldopt=='deleted'){
				$vv = $value[$valueid];

				if(isset($vv['option_for']) && $vv['option_for']!='' && isset($vv['same_parent']) && $vv['same_parent']==1 && isset($value[$vv['option_for']])){
					$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('company_id'));
					$option_for = $vv['option_for'];
					if(!isset($query['company_id']))
						$query['company_id'] = '';

					$result = array();
					$arr_plus_temp = $value[$option_for];
					//tinh gia theo price list
					$arr_plus_temp['plus_sell_price'] = 0;
					$cal_price = new cal_price;
					$cal_price->arr_product_items = $arr_plus_temp;
					$result = $this->change_sell_price_company($query['company_id'],$vv['products_id']);
					$cal_price->price_break_from_to = $result;
					$cal_price->field_change = '';
					$arr_plus_temp = $cal_price->cal_price_items();

					//loai bo gia option
					$value[$option_for]['sell_price'] -= (float)$arr_plus_temp['sell_price'];
					$value[$option_for]['plus_sell_price'] -= (float)$arr_plus_temp['sell_price'];
					//tinh lai unit price
					$cal_price2 = new cal_price;
					$cal_price2->arr_product_items = $value[$option_for];
					$cal_price2->field_change = 'sell_price';
					$value[$option_for] = $cal_price2->cal_price_items();

				}
				$value[$valueid]['deleted'] = true;
				//pr($value);die;



			//truong hop thay Code
			}else if(isset($value[$valueid]) && isset($value[$valueid]['products_id']) && is_object($value[$valueid]['products_id']) && $fieldopt=='code'){
                $query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('products','options','company_id'));
				//remove cac option cu cua $valueid
				foreach($value as $kks=>$vvs){
					if(isset($vvs['option_for']) && $vvs['option_for']==$valueid)
					   $value[$kks] = array('deleted' => true);
				}
				if(isset($query['options']))
	                foreach($query['options'] as $options_key=>$options){
	                    if(isset($options['parent_line_no']) && $options['parent_line_no']==$valueid)
	                        $query['options'][$options_key] = array('deleted' => true);
	                }

				//tim data option cua product
				$this->selectModel('Product');
				$parent = $this->Product->select_one(array('_id'=>$value[$valueid]['products_id']));
				if(isset($parent['sku']))
					$value[$valueid]['sku'] = $parent['sku'];
				else
					$value[$valueid]['sku'] = '';

				//lay danh sach option va luu lai
				$products = $this->Product->options_data((string)$value[$valueid]['products_id']);
				if(isset($products['productoptions']) && is_array($products['productoptions']) && count($products['productoptions'])>0){
                    $total_sub_total = 0;
                    if(!isset($query['options']))
                        $query['options']= array();
                    $arr_return['options'] = $query['options'];
                    $options_num = count($arr_return['options']);
                    $line_num = count($value);
					foreach($products['productoptions'] as $kk=>$vv){
						//loop va tao moi items
						$new_array = array();
						$new_array['code'] 			= $vv['code'];
						$new_array['sku'] 			= $vv['sku'];
                        $new_array['products_name'] = $vv['product_name'];
						$new_array['product_name'] = $vv['product_name'];
                        $new_array['products_id']   = $vv['product_id'];
						$new_array['product_id'] 	= $vv['product_id'];
						$new_array['quantity'] 		= $vv['quantity'];
						$new_array['sub_total'] 	= $vv['sub_total'];
                        $new_array['option_group']  = (isset($vv['option_group']) ? $vv['option_group'] : '');
						if(isset($value[$valueid]['sizew']))
							$new_array['sizew'] 		= $value[$valueid]['sizew'];
						else
							$new_array['sizew'] 		= $vv['sizew'];

						if(isset($value[$valueid]['sizew_unit']))
							$new_array['sizew_unit'] 		= $value[$valueid]['sizew_unit'];
						else
							$new_array['sizew_unit'] 		= $vv['sizew_unit'];

						if(isset($value[$valueid]['sizeh']))
							$new_array['sizeh'] 		= $value[$valueid]['sizeh'];
						else
							$new_array['sizeh'] 		= $vv['sizeh'];

						if(isset($value[$valueid]['sizeh_unit']))
							$new_array['sizeh_unit'] 		= $value[$valueid]['sizeh_unit'];
						else
							$new_array['sizeh_unit'] 		= $vv['sizeh_unit'];


						$new_array['sell_by'] 		= $vv['sell_by'];
						$new_array['oum'] 		= $vv['oum'];

						if(isset($vv['same_parent']))
							$new_array['same_parent'] 	= (int)$vv['same_parent'];
						else
							$new_array['same_parent'] 	= 0;
						$more_discount 				= (float)$vv['unit_price']*((float)$vv['discount']/100);
						$new_array['sell_price'] 	= (float)$vv['unit_price'] - $more_discount;

						$new_array['taxper'] 		= (isset($value[$valueid]['taxper']) ? (float)$value[$valueid]['taxper'] : 0);
						$new_array['tax'] 			= $value[$valueid]['tax'];
						$new_array['option_for'] 	= $valueid;
						$new_array['deleted'] 		= false;
						$new_array['proids'] 		= $value[$valueid]['products_id'].'_'.$kk;

						$this->cal_price = new cal_price;
						//truyen data vao cal_price de tinh gia
						$this->cal_price->arr_product_items = $new_array;
						//lay thong tin khach hang de tinh chiec khau/giam gia
						$result = array();
						if(!isset($query['company_id']))
							$query['company_id'] = '';
						if(isset($new_array['products_id']))
							$result = $this->change_sell_price_company($query['company_id'],$new_array['products_id']);
						//truyen bang chiec khau va gia giam vao
						$this->cal_price->price_break_from_to = $result;
						//kiem tra field nao dang thay doi
						$this->cal_price->field_change = '';
						//chay tinh gia
						$arr_ret = $this->cal_price->cal_price_items();
                        //
                        if(isset($vv['line_no']))
                        	unset($vv['line_no']);
                        $arr_return['options'][$options_num] = $vv;
                        $arr_return['options'][$options_num]['this_line_no'] = $options_num;
                        $arr_return['options'][$options_num]['parent_line_no'] = $valueid;
                        $arr_return['options'][$options_num]['choice'] = 0;
                        if(isset($vv['require']) && (int)$vv['require']==1){
							$value[$line_num] = array_merge((array)$new_array,(array)$arr_ret);
                            $arr_return['options'][$options_num]['line_no'] = $line_num;
                            $arr_return['options'][$options_num]['choice'] = 1;
                            $line_num++;
                        }
                        $options_num++;
					}
				    $query['options'] = $arr_return['options'];
                    $query['products'] = $value;
                    //=============================================
                    $this->opm->save($query);
                    echo $valueid;
                    die;
                }
			}
			$arr_return[$field] = $value;
		}
		return $arr_return;
	}

	public  function add_doc_from_module(){
		$arr_save=array();
		$this->redirect('/docs/entry/');
	}
	public function entry_search(){
		//parent class
		$arr_set = $this->opm->arr_settings;
		$arr_set['field']['panel_1']['code']['lock'] = '';
		$arr_set['field']['panel_1']['invoice_type']['element_input'] = '';
		$arr_set['field']['panel_1']['our_rep']['not_custom'] = '0';
		$arr_set['field']['panel_1']['our_csr']['not_custom'] = '0';
		$arr_set['field']['panel_4']['job_name']['not_custom'] = '0';
		$arr_set['field']['panel_4']['job_number']['lock'] = '0';
		$arr_set['field']['panel_4']['salesorder_name']['not_custom'] = '0';
		$arr_set['field']['panel_4']['salesorder_number']['lock'] = '0';
		$arr_set['field']['panel_1']['invoice_type']['default'] = '';
		$arr_set['field']['panel_1']['invoice_date']['default'] = '';
		$arr_set['field']['panel_4']['invoice_status']['default'] = '';
		$arr_set['field']['panel_4']['payment_due_date']['default'] = '';
		$arr_set['field']['panel_4']['payment_terms']['default'] = '';
		$arr_set['field']['panel_4']['tax']['default'] = '';

		$this->set('search_class','jt_input_search');
		$this->set('search_class2','jt_select_search');
		$this->set('search_flat','placeholder="1"');
		$where = array();
		if($this->Session->check($this->name.'_where'))
			$where = $this->Session->read($this->name.'_where');
		if(count($where)>0){
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($where[$field])){
						$arr_set['field'][$ks][$field]['default'] = $where[$field]['values'];
					}
				}
			}
		}
		//end parent class
		$this->set('arr_settings',$arr_set);

		//set address
		$address_label = array('Invoice Address','Shipping address');
		$this->set('address_label',$address_label);
		$address_conner[0]['top'] 		= 'hgt fixbor';
		$address_conner[0]['bottom'] 	= 'fixbor2 jt_ppbot';
		$address_conner[1]['top'] 		= 'hgt';
		$address_conner[1]['bottom'] 	= 'fixbor3 jt_ppbot';
		$this->set('address_conner',$address_conner);
		$this->set('address_more_line',2);//set
		$address_hidden_field = array('invoice_address','shipping_address');
		$this->set('address_hidden_field',$address_hidden_field);//set
		$address_country = $this->country();
		$this->set('address_country',$address_country);//set
		$this->set('address_country_id','Canada');//set
		$address_province['invoice'] = $address_province['shipping'] = $this->province("CA");
		$this->set('address_province',"");//set
		$this->set('address_province_id',"");//set
		$this->set('address_onchange',"save_address_pr('\"+keys+\"');");
		$address_hidden_value=array('','');
		$this->set('address_hidden_value',$address_hidden_value);
		$this->set('address_mode','search');
	}



	// Options list
	public function swith_options($keys){
		parent::swith_options($keys);
		if($keys=='existing')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys=='under')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys=='over')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys == 'invoiced')
		{
			$arr_where['invoice_status'] = array('values' => 'Invoiced', 'operator' => '=');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL.'/'.$this->params->params['controller'].'/lists';
		}
		else if($keys== 'outstanding')
		{
			$or_where = array(
				array("quotation_status" => 'In progress'),
				array("quotation_status" => 'Submitted'),
				array("quotation_status" => 'Amended')
			);
			$arr_where = array();
			$arr_where[] = array('values' => $or_where, 'operator' => 'or');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL . '/' . $this->params->params['controller'] . '/lists';
		}
		else if($keys=='finished_products')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys=='find_out_sync')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys=='find_history')
			echo URL.'/'.$this->params->params['controller'].'/history';
		else if($keys=='sync_stock_current')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys=='sync_stock_found')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if ($keys == 'report_by_customer_summary')
			echo URL . '/' . $this->params->params['controller'] . '/option_summary_customer_find';
		else if ($keys == 'report_by_customer_detailed')
			echo URL . '/' . $this->params->params['controller'] . '/option_detailed_customer_find';
		else if ($keys == 'report_by_customer_summary_highest_first')
			echo URL . '/' . $this->params->params['controller'] . '/option_summary_highest_customer_find';
		else if ($keys == 'report_by_customer_detailed_tax_amounts')
			echo URL . '/' . $this->params->params['controller'] . '/option_detailed_tax_customer_find';
		else if ($keys == 'report_by_product_summary')
			echo URL . '/' . $this->params->params['controller'] . '/option_summary_product_find';
		else if ($keys == 'report_by_product_detailed')
			echo URL . '/' . $this->params->params['controller'] . '/option_detailed_product_find';
		else if($keys=='create_po')
			echo URL.'/purchaseorders/add/Product@'.$this->get_id();
		else if($keys == 'create_receipt')
			echo URL . '/' . $this->params->params['controller'] . '/create_receipt';
		else if($keys=='print_price_list')
			echo URL.'/'.$this->params->params['controller'].'/entry';
		else if($keys=='print_minilist')
			//echo URL.'/'.$this->params->params['controller'].'/entry';
			echo URL . '/' . $this->params->params['controller'] . '/view_minilist';
		else if($keys=='print_invoice')
			echo URL.'/'.$this->params->params['controller'].'/view_pdf';
		else
			echo '';
		die;
	}

	public function set_cal_price(){
		$this->cal_price = new cal_price; //Option cal_price
		//set arr_price_break default
		$this->cal_price->arr_price_break = array();
		//set arr_product default
		$this->cal_price->arr_product = array();
		//set arr_product item default
		$this->cal_price->arr_product_items = array();
	}

	 public function ajax_cal_line($arr_data = array()) {
        $arr_ret = $arr_product_items = array();
        if(!isset($arr_data['arr'])&&isset($_POST['arr'])){
            $arr_data['arr'] = $_POST['arr'];
        }
        if(!isset($arr_data['field'])&&isset($_POST['field']))
            $arr_data['field'] = $_POST['field'];
        if (isset($arr_data['arr'])) {
            $getdata = $arr_data['arr'];
            $getdata = (array) $getdata;
            if(isset($getdata['custom_unit_price'])){
                $getdata['custom_unit_price'] = (float)$getdata['custom_unit_price'];
            }
            //truong hop co id

            if (isset($getdata['id'])) {
                $get_id = $getdata['id'];
                $query  = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
                $old_balance=isset($query['sum_amount'])?$query['sum_amount']:0;
                if(isset($getdata['custom_unit_price'])){
                    $getdata['custom_unit_price'] = (float)$getdata['custom_unit_price'];
                    if($query['products'][$get_id]['unit_price'] > $getdata['custom_unit_price']
                       &&!$this->check_permission($this->name.'_@_custom_unit_price_@_add')){
                        echo 'You do not have permission to change this value.';
                        die;
                    }
                }
                if(isset($arr_data['company_id'])&&is_object($arr_data['company_id']))
                        $query['company_id'] = $arr_data['company_id'];
                if (isset($query['products']))
                    $arr_pro = $arr_insert['products'] = (array) $query['products'];
                if (is_array($arr_pro) && count($arr_pro) > 0 && isset($arr_pro[$get_id]) && !$arr_pro[$get_id]['deleted']) {
                    $arr_pro = array_merge((array) $arr_pro[$get_id], (array) $getdata);

					if(isset($query['tax']) && $query['tax']!='')
						$arr_pro['tax'] = $this->get_tax($query['tax']);
					//tim va luu them cac thay doi phu thuoc

					if(isset($arr_data['field']))
						$fieldchage = $arr_data['field'];
					else
						$fieldchage = '';
					if($fieldchage=='sell_price' || $fieldchage=='custom')
						$arr_pro['plus_unit_price'] = 0;
					if(!isset($query['company_id']))
						$query['company_id'] = '';
                    $is_special = false;

					//tinh lai plus sell price neu thay doi lien quan den gia
                    $total_sub_total = 0;
                    $product_data = $query['products'][$get_id];
					if($fieldchage!='sell_price'){
                        $parent_no = $get_id;
                        $parent_id = $query['products'][$parent_no]['products_id'];
                        if($fieldchage=='options'&&isset($getdata['data'])){
                            $options_change = true;
                            $parent_no = $getdata['data']['parent_line_no'];
                            if(isset($getdata['data']['line_no']))
                                $this_line_no = $getdata['data']['line_no'];
                            $parent_id = $query['products'][$parent_no]['products_id'];
                            $get_id = $parent_no;
                            $arr_pro = $query['products'][$parent_no];
                        }
						$arr_pro['plus_sell_price'] = 0;
						if(!isset($arr_pro['sell_price']))
							$arr_pro['sell_price'] = 0;
                        if(strpos($fieldchage, 'size')!==false){
                            $size_tmp = $fieldchage;
                        }

						//tinh lai gia option
						$option = $this->option_list_data($parent_id,$parent_no);
						foreach($option['option'] as $value){
							if(isset($value['choice'])&&$value['choice']==1){
                                if(isset($value['same_parent'])&&$value['same_parent']==1){
                                    $is_special = true;
									 if(isset($value['is_custom']) && $value['is_custom']==1)
									 	$value['sell_price'] = $value['unit_price'];
                                    $value['sizew'] = $arr_pro['sizew'];
                                    $value['sizew_unit'] = $arr_pro['sizew_unit'];
                                    $value['sizeh'] = $arr_pro['sizeh'];
                                    $value['sizeh_unit'] = $arr_pro['sizeh_unit'];
                                    $value['quantity'] = (isset($value['quantity']) ? (float)$value['quantity'] : 1);
                                    if(isset($size_tmp))
                                        $value[$size_tmp] = $getdata[$size_tmp];
                                    $value['plus_sell_price'] = 0;
                                    $cal_price = new cal_price;


                                    $cal_price->arr_product_items = $value;
									$cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$value['product_id']);
									if(isset($value['is_custom']) && $value['is_custom']==1){
										$cal_price->field_change = 'sell_price';
									}else{
										$cal_price->field_change = $arr_data['field'];
										$cal_price->arr_product_items['quantity'] *= $arr_pro['quantity'];
										$cal_price->cal_price_items();
										$value['sell_price'] = $cal_price->arr_product_items['sell_price'];
										$cal_price->arr_product_items = $value;
										$cal_price->field_change = 'sell_price';
									}

                                    $value = $cal_price->cal_price_items();
                                    $arr_pro['plus_sell_price'] += $value['sub_total'];
                                    $total_sub_total += (isset($value['sub_total']) ? (float)$value['sub_total'] : 0);
                                    $fieldchage = '';
								//neu khong phai la S.P thi xet loai combination
                                } else if($product_data['sell_by']=='combination'
                                          && (!isset($value['same_parent']) || $value['same_parent'] == 0)
										  && isset($value['choice'])&&$value['choice']==1){
                                    $total_sub_total += (isset($value['sub_total']) ? (float)$value['sub_total'] : 0);
                                }
                            }
						}

						//dau check cuoi cung cua
						if(!$is_special
							&&isset($getdata['data']['same_parent'])
							&&$getdata['data']['same_parent']==0){
							$is_special = true;
						}


					}
					$cal_price = new cal_price;
					$cal_price->arr_product_items = $arr_pro;
					$result = array();
                    //Kiem tra neu nhu product cha la custom(ko co id) thi line nay ko can phai tinh bang pricebreak
                    if(isset($arr_pro['option_for'])&&$arr_pro['option_for']!=''){
                        $option_for = $arr_pro['option_for'];
                        if(!isset($arr_pro['same_parent']) || $arr_pro['same_parent']==0){
                            if(isset($query['products'][$option_for])&&!is_object($query['products'][$option_for]['products_id'])){
                                $is_custom = true;
                            }
                        }
                    }
					if(isset($arr_pro['products_id'])&&!isset($is_custom)){
                    	$result = $this->change_sell_price_company($query['company_id'],$arr_pro['products_id']);
					}

					//truyen bang chiec khau va gia giam vao
					$cal_price->price_break_from_to = $result;
					$cal_price->field_change = $fieldchage;

					//chay tinh gia, neu la combination
                    if($product_data['sell_by']=='combination'){
                        $arr_ret = $cal_price->combination_cal_price();
                        $arr_ret['unit_price'] += $total_sub_total;

                        $arr_ret['sell_price'] = $arr_ret['unit_price'];
                        if($total_sub_total>0){
                            $arr_ret['sub_total'] = round((float)$arr_ret['unit_price']*(float)$arr_ret['quantity'],2);
                            $arr_ret['tax'] = round(((float)(isset($arr_ret['taxper']) ? $arr_ret['taxper'] : 0)/100)*(float)$arr_ret['sub_total'],3);
                            $arr_ret['amount'] = round((float)$arr_ret['sub_total']+(float)$arr_ret['tax'],2);
                        }


					//nếu như có sp thì là loại special, thì tính diện tích rồi nhân vào line chính mới cộng cho plus_sell_price không thì ngược lại
					}else{
					   $arr_ret = $cal_price->cal_price_items($is_special);
                       //Kiem tra neu nhu product cha la custom(ko co id) update thong tin thay doi qua option
                       if(isset($is_custom)){
                            $orginal_query = $query;
                            if(isset($query['options'])&&!empty($query['options'])){
                                foreach($query['options'] as $option_key=>$option_value){
                                    if(isset($option_value['deleted'])&&$option_value['deleted']) continue;
                                    if(!isset($option_value['parent_line_no']) || $option_value['parent_line_no']!=$option_for) continue;
                                    if(!isset($option_value['product_id']) || (string)$option_value['product_id']!=(string)$arr_ret['products_id']) continue;
                                    $query['options'][$option_key]['unit_price'] = $arr_ret['sell_price'];
                                    $query['options'][$option_key]['quantity'] = $arr_ret['quantity'];
                                    $query['options'][$option_key]['sub_total'] = $arr_ret['sub_total'];
                                    break;
                                }
                            }
                            if($query['options']!=$orginal_query['options']){
                                $this->opm->save($query);
                            }
                        }
                       if(isset($arr_ret['option_for']) && isset($arr_ret['same_parent']) && $arr_ret['same_parent']==1){
                            $ids = $arr_ret['option_for'];
                            if(is_array($query['products'][$ids])&&!$query['products'][$ids]['deleted']){
                                $sub_total = (float)$query['products'][$get_id]['sub_total'];
                                $new_sub_total = (float)$arr_ret['sub_total'];
                                $query['products'][$ids]['sell_price'] = $query['products'][$ids]['unit_price'] = (float)$query['products'][$ids]['unit_price'] - $sub_total + $new_sub_total;
                                $query['products'][$ids]['sub_total'] = round((float)$query['products'][$ids]['unit_price']*(float)$query['products'][$ids]['quantity'],2);
                                $query['products'][$ids]['tax'] = round(((float)(isset($query['products'][$ids]['taxper']) ? $query['products'][$ids]['taxper'] : 0)/100)*(float)$query['products'][$ids]['sub_total'],3);
                                $query['products'][$ids]['amount'] = round((float)$query['products'][$ids]['sub_total']+(float)$query['products'][$ids]['tax'],2);
                                $arr_insert['products'][$ids] = $query['products'][$ids];
                                $query['products'][$ids]['ids'] = $ids;
                            }
                       }

                    }
					//Save all data
                    $arr_insert['products'][$get_id] = array_merge((array) $arr_pro, (array) $arr_ret);
                     //custom unit price
                    if(isset($arr_insert['products'][$get_id]['custom_unit_price'])){
                        $product = $arr_insert['products'][$get_id];
                        $is_reverse_update = false;
                        if(!is_object($product['products_id']))
                            $is_reverse_update = true;
                        $is_admin_edit = false;
                        if($this->check_permission($this->name.'_@_custom_unit_price_@_add'))
                            $is_admin_edit = true;
                        if((float)$product['custom_unit_price']<(float)$product['unit_price']){
                            if($arr_data['field']=='custom_unit_price'
                               &&!$this->check_permission($this->name.'_@_custom_unit_price_@_add'))
                               $product['custom_unit_price'] = $product['unit_price'];
                            else if($arr_data['field']!='custom_unit_price')
                                $product['custom_unit_price'] = $product['unit_price'];
                        }
                        if($is_reverse_update)
                            $product['sell_price'] = $product['unit_price'] = $product['custom_unit_price'];
                        $unit_price = $product['custom_unit_price'];
                        $product['sub_total'] = round((float)$unit_price*(float)$product['quantity'],2);
                        $product['tax'] = round(((float)(isset($product['taxper']) ? $product['taxper'] : 0)/100)*(float)$product['sub_total'],3);
                        $product['amount'] = round((float)$product['sub_total']+(float)$product['tax'],2);
                        $arr_insert['products'][$get_id] = $product;
                        $arr_ret = $product;
                    }
                    //end custom
					$arr_insert = $this->arr_associated_data('products',$arr_insert['products'],$get_id,$fieldchage);
					$arr_insert['_id'] = new MongoId($this->get_id());
                    $this->opm->save($arr_insert);
					//update sum
                    $keyfield = array(
                        "sub_total" 	=> "sub_total",
                        "tax" 			=> "tax",
                        "amount" 		=> "amount",
                        "sum_sub_total" => "sum_sub_total",
                        "sum_tax" 		=> "sum_tax",
                        "sum_amount" 	=> "sum_amount"
                    );
                    $arr_sum = $this->update_sum('products', $keyfield);
                    $new_balance=isset($arr_sum['sum_amount'])?$arr_sum['sum_amount']:0;
					// BaoNam: cập nhật lại Sales Account
					if($new_balance != $old_balance){
						$this->selectModel('Salesaccount');
						if(isset($arr_sales_invoice['company_id']) && is_object($arr_sales_invoice['company_id'])){
							$this->Salesaccount->update_account($arr_sales_invoice['company_id'], array(
																'model' => 'Company',
																'balance' =>  $new_balance - $old_balance,
																'invoices_credits' => $new_balance - $old_balance,
																));
						}elseif(isset($arr_sales_invoice['contact_id'])){
							$this->Salesaccount->update_account($arr_sales_invoice['contact_id'], array(
																'model' => 'Contact',
																'balance' => $new_balance - $old_balance,
																'invoices_credits' => $new_balance - $old_balance,
																));
						}
					}
                    $arr_ret = array_merge((array) $arr_ret, (array) $arr_sum);
                    if(isset($ids)){
                        $arr_ret = array('self'=>$arr_ret,'parent'=>$query['products'][$ids]);
                    }
                    else if(isset($options_change)&&isset($this_line_no)&&$this_line_no!=''){
                        foreach($query['options'] as $option){
                            if($option['deleted']) continue;
                            if(!isset($option['line_no']) || $option['line_no']!=$this_line_no) continue;
                            $query['products'][$this_line_no] = array_merge($query['products'][$this_line_no],$option);
                            break;
                        }
                        $arr_ret = array('parent'=>$arr_ret,'self'=>$query['products'][$this_line_no]);
                    }
                    //Return data for display
                    if(!isset($arr_data['company_id']))
                        echo json_encode($arr_ret);
                }

                //truong hop khong chon id nao
            } else {
                if(!isset($arr_data['company_id']))
                    echo '';
            }
        }
        if(!isset($arr_data['company_id']))
            die;
    }
	//Sử dụng thư viện cal_price để tính
	public function ajax_cal_line2(){
		$this->set_cal_price();
		$arr_ret = $arr_product_items = array();
		if(isset($_POST['arr'])){
			$getdata = $_POST['arr'];
			$getdata = (array)$getdata;
			//truong hop co id
			if(isset($getdata['id'])){
				$get_id = $getdata['id'];
				$qr= $this->opm->select_one(array('_id' =>new MongoId($this->get_id())));
				$query = $qr;
				if(isset($query['products']))
					$arr_pro = $arr_insert['products'] = (array)$query['products'];

				if(is_array($arr_pro) && count($arr_pro)>0 && isset($arr_pro[$get_id]) && !$arr_pro[$get_id]['deleted']){
					$arr_pro = array_merge((array)$arr_pro[$get_id],(array)$getdata);

					$tmp_qty = $arr_pro['quantity'];
					$arr_pro['quantity'] = isset($arr_pro['invoiced'])?$arr_pro['invoiced']:0;


					$ids=$this->get_id();
					$arr_sales_invoice=$this->opm->select_one(array('_id'=>new MongoId($ids)));
					$old_balance=isset($arr_sales_invoice['sum_amount'])?$arr_sales_invoice['sum_amount']:0;

					$this->cal_price->arr_product_items = $arr_pro;
					$arr_ret = $this->cal_price->cal_price_items();

					$arr_ret['invoiced']=$arr_ret['quantity'];
					$arr_ret['quantity'] = $tmp_qty;


					//Save all data
					$arr_insert['_id'] = new MongoId($this->get_id());
					$arr_insert['products'][$get_id] = array_merge((array)$arr_pro,(array)$arr_ret);
					$this->opm->save($arr_insert);
					//update sum
					$keyfield = array(
									"sub_total"		=> "sub_total",
									"tax"			=> "tax",
									"amount"		=> "amount",
									"sum_sub_total"	=> "sum_sub_total",
									"sum_tax"		=> "sum_tax",
									"sum_amount"	=> "sum_amount"
								);

					$arr_sum = $this->update_sum('products',$keyfield);
					$new_balance=isset($arr_sum['sum_amount'])?$arr_sum['sum_amount']:0;
					$arr_ret = array_merge((array)$arr_ret,(array)$arr_sum);
					//Return data for display

					// BaoNam: cập nhật lại Sales Account
					if($new_balance != $old_balance){
						$this->selectModel('Salesaccount');
						if(isset($arr_sales_invoice['company_id']) && is_object($arr_sales_invoice['company_id'])){
							$this->Salesaccount->update_account($arr_sales_invoice['company_id'], array(
																'model' => 'Company',
																'balance' =>  $new_balance - $old_balance,
																'invoices_credits' => $new_balance - $old_balance,
																));
						}elseif(isset($arr_sales_invoice['contact_id'])){
							$this->Salesaccount->update_account($arr_sales_invoice['contact_id'], array(
																'model' => 'Contact',
																'balance' => $new_balance - $old_balance,
																'invoices_credits' => $new_balance - $old_balance,
																));
						}
					}

					echo json_encode($arr_ret);
				}
			//truong hop khong chon id nao
			}else{
				echo '';
			}
		}
		die;
	}


	var $is_text = 0;

	//subtab line entry
	public function line_entry() {
		$is_text = $this->is_text;
		if ($this->check_lock()) {
			$this->opm->set_lock_option('line_entry', 'products');
			$this->opm->set_lock_option('text_entry', 'products');
		}
		$subdatas = $arr_ret = array();
		$codeauto = 0;
		$opname = 'products';
		$sum_sub_total = $sum_tax = 0;
		$subdatas[$opname] = array();
		$ids = $this->get_id();
		if ($ids != '') {
			//get entry data
            $arr_ret = $this->line_entry_data($opname, $is_text);
            if(isset($arr_ret[$opname])){
            	$minimum = $this->get_minimum_order();
                if($arr_ret['sum_sub_total']<$minimum){
                    $more_sub_total = $minimum - (float)$arr_ret['sum_sub_total'];
                    if(isset($arr_ret[$opname]) && !empty($arr_ret[$opname])){
                        $last_insert = end($arr_ret[$opname]);
                        foreach($last_insert as $key=>$value){
                            if($key=='deleted' || $key=='taxper' || $key=='tax') continue;
                            $last_insert[$key] = '';
                            $last_insert['xlock'][$key] = 1;
                        }
                    }
                    $last_insert['products_name'] = 'Minimum Order Adjustment';
                    $last_insert['xlock']['products_name'] = '1';
                    $last_insert['xlock']['unit_price'] = '1';
                    $last_insert['xlock']['quantity'] = '1';
                    foreach(array('sku','products_id','details','option','sizew','sizew_unit','sizeh','sizeh_unit','receipts','view_costing','sell_by','sell_price','adj_qty','oum','custom_unit_price','unit_price') as $value)
                        $last_insert['xempty'][$value] = '1';
                    $last_insert['sku_disable'] = 1;
                    $last_insert['_id'] = 'Extra_Row';
                    $last_insert['remove_deleted'] = '1';
                    $last_insert['quantity'] = '1';
                     if(!isset($last_insert['taxper']))
                        $last_insert['taxper']= 0;
                    $sub_total = $more_sub_total;
                    $tax = $sub_total*$last_insert['taxper']/100;
                    $amount = $sub_total+$tax;
                    $last_insert['sub_total'] = number_format($sub_total,2);
                    $last_insert['tax'] = number_format($tax,3);
                    $last_insert['amount'] = number_format($amount,2);
                    array_push($arr_ret[$opname], $last_insert);
                    $arr_ret['sum_sub_total']+=$sub_total;
                    $arr_ret['sum_tax']+=$tax;
                    $arr_ret['sum_amount']+=$amount;
                }
                $subdatas[$opname] = $arr_ret[$opname];
            }
            $query = $this->opm->select_one(array('_id'=> new MongoId($ids)),array('invoice_status'));
            if(isset($query['invoice_status']) && $query['invoice_status'] == 'Cancelled' )
                $arr_ret['sum_sub_total'] = $arr_ret['sum_tax'] = $arr_ret['sum_amount'] = 0;
        }
		$id = $this->get_id();
		$this->set('subdatas', $subdatas);
		$codeauto = $this->opm->get_auto_code('code');
		$this->set('nextcode', $codeauto);
		$this->set('file_name', 'salesorder_' . $id);
		$this->set('sum_sub_total', $arr_ret['sum_sub_total']);
		$this->set('sum_amount', $arr_ret['sum_amount']);
		$this->set('sum_tax', $arr_ret['sum_tax']);
		$link_add_atction['option'] = 'option_list';
		$this->set('link_add_atction', $link_add_atction);
		$this->set_select_data_list('relationship', 'line_entry');
		$this->set('icon_link_id', $id);
		$this->set('mongo_id', $id);
	}

	//check and cal for Line Entry
	 public function line_entry_data($opname = 'products', $is_text = 0,$mod = '') {
        $arr_ret = array(); $option_for = '';
        $this->selectModel('Setting');
        if ($this->get_id() != '') {
            $newdata = $option_select_dynamic = array();
            $query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('options','products','sum_sub_total','sum_amount','sum_tax','rfqs','invoice_date'));
            if(!isset($query['options']))
                $query['options'] = array();
            //set sum
            $arr_ret['sum_sub_total'] = $arr_ret['sum_amount'] = $arr_ret['sum_tax'] = '0.00';
            if (isset($query['sum_sub_total']) && $query['sum_sub_total'] != '')
                $arr_ret['sum_sub_total'] = $query['sum_sub_total'];
            if (isset($query['sum_amount']) && $query['sum_amount'] != '')
                $arr_ret['sum_amount'] = $query['sum_amount'];
            if (isset($query['sum_tax']) && $query['sum_tax'] != '')
                $arr_ret['sum_tax'] = $query['sum_tax'];
            $this->selectModel('Product');
            $arr_product_approved = $this->Product->select_all(array(
                                       'arr_where'=>array('approved'=>1),
                                       'arr_field'=>array('_id')
                                       ));
            $arr_product_approved = iterator_to_array($arr_product_approved);
            $option_for_sort = array();
            if (isset($query[$opname]) && is_array($query[$opname])) {
                $options = array();
                if(isset($query['options']) && !empty($query['options']) )
                    $options = $query['options'];
                foreach ($query[$opname] as $key => $arr) {
                    if (!$arr['deleted']) {
                        $newdata[$key] = $arr;
						//set default Unit price
						if(!isset($arr['custom_unit_price']) && isset($arr['unit_price']))
							$newdata[$key]['custom_unit_price'] = $arr['unit_price'];

                        if(!isset($arr['option_for'])){
                            $option = $this->new_option_data(array('key'=>$key,'products_id'=>$arr['products_id'],'options'=>$query['options'],'products'=>$query['products'],'date'=>$query['invoice_date']),$query['products']);
                            //Khoa sell_by,oum neu nhu line nay co option
                            //Khoa tiep sell_price neu line nay co option same_parent
                            if(isset($option['option'])&&!empty($option['option'])){
                                foreach($option['option'] as $value){
                                    if($value['deleted']) continue;
                                    if(isset($value['choice'])&&$value['choice']==0&&isset($value['require']) && $value['require']!=1) continue;
                                    if($value['oum']!=$arr['oum'])
                                        $newdata[$key]['oum'] = 'Mixed';
                                    $newdata[$key]['xlock']['sell_by'] = '1';
                                    $newdata[$key]['xlock']['oum'] = '1';
                                    break;
                                    // if(!isset($value['same_parent']) || $value['same_parent']==0) continue;
                                    // $newdata[$key]['xlock']['sell_price'] = '1';
                                }
                            }
                            if(isset($arr['products_id'])&&!isset($arr_product_approved[(string)$arr['products_id']]))
                                $newdata[$key]['xcss_element']['sku']= 'approved_product';
                        } else {
                            $newdata[$key]['xlock']['sell_by'] = '1';
                            $newdata[$key]['xlock']['oum'] = '1';
                        }
						$newdata[$key]['option'] = 1;
                        $newdata[$key]['option_group'] = '';
						if (isset($newdata[$key]['products_id']) && $newdata[$key]['products_id']!='')
							$newdata[$key]['xlock']['unit_price']   = '1';
						else
							$newdata[$key]['xlock']['unit_price']   = '0';
                        if (isset($newdata[$key]['products_name'])) {
                            if($is_text != 1){
                                $arrtmp = explode("\n", $newdata[$key]['products_name']);
                                $newdata[$key]['products_name'] = $arrtmp[0];
                                if(isset($arr['same_parent']) && $arr['same_parent']==1)
                                    $get_name_only = true;
                            }
                            if(!empty($option)){
                                foreach($options as $k=>$val){
                                    if(isset($val['deleted']) && $val['deleted']) continue;
                                    if(!isset($val['line_no']) || $val['line_no']!=$key) continue;
                                    $newdata[$key]['option_group'] = (isset($val['option_group']) ? $val['option_group'] : '');
                                    if(isset($get_name_only)){
                                        if(!isset($val['quantity'])) continue;
                                        if($val['quantity']==1) continue;
                                        $newdata[$key]['products_name'] .= ' ('.$val['quantity'].')';
                                        unset($options[$k]);
                                    }
                                }
                            }
                        }
                        if(isset($newdata[$key]['products_name']) && $is_text == 1){
                            $newdata[$key]['products_costing_name'] = '';
                            if(isset($arr['details']))
                                $newdata[$key]['products_costing_name'] .= htmlentities('<p style="margin-left:15px;font-style:italic;">'.nl2br($arr['details']).'</p>');
                            if(isset($option['option'])&&!empty($option['option'])){
                                foreach($option['option'] as $value){
                                    if(isset($value['deleted']) && $value['deleted']) continue;
                                    if(!isset($value['product_name']) || $value['product_name']=='') continue;
                                    if(!isset($value['view_in_detail'])|| $value['view_in_detail']==0)continue;
                                    $newdata[$key]['products_costing_name'] .= htmlentities($value['product_name']).' ('.(isset($value['quantity']) ? $value['quantity'] : 0).')<br />';
                                }
                            }
                            $newdata[$key]['xlock']['products_name']= '1';
                            $newdata[$key]['xlock']['sell_by']  = '1';
                            $newdata[$key]['xlock']['sell_price']   = '1';
                            $newdata[$key]['xlock']['oum']      = '1';
                            $newdata[$key]['xlock']['quantity']     = '1';
                            $newdata[$key]['xlock']['sizew']        = '1';
                            $newdata[$key]['xlock']['sizew_unit']   = '1';
                            $newdata[$key]['xlock']['sizeh']        = '1';
                            $newdata[$key]['xlock']['sizeh_unit']   = '1';
                            $newdata[$key]['xlock']['unit_price']   = '1';
                            $newdata[$key]['xlock']['adj_qty']  = '1';
                            $newdata[$key]['xlock']['sub_total']    = '1';
                            $newdata[$key]['xlock']['tax']      = '1';
                            $newdata[$key]['xlock']['amount']       = '1';
                            $newdata[$key]['xlock']['option']       = '1';
                            $newdata[$key]['xlock']['receipts']     = '1';
                        }
                        //set all price in display
                        if (isset($arr['area']))
                            $newdata[$key]['area'] = (float) $arr['area'];
                        $newdata[$key]['custom_unit_price'] = number_format((isset($arr['custom_unit_price']) ? (float)$arr['custom_unit_price'] : 0), 3);
                        if (isset($arr['unit_price'])){
                            $newdata[$key]['unit_price'] = number_format((float) $arr['unit_price'], 3);
                            if(!isset($arr['custom_unit_price']))
                                $newdata[$key]['custom_unit_price'] = $newdata[$key]['unit_price'];
                        }
                        else
                            $newdata[$key]['unit_price'] = '0.000';
                        if (isset($arr['sub_total']))
                            $newdata[$key]['sub_total'] = number_format((float) $arr['sub_total'], 2);
                        else
                            $newdata[$key]['sub_total'] = '0.00';
                        if (isset($arr['tax']))
                            $newdata[$key]['tax'] = number_format((float) $arr['tax'], 3);
                        else
                            $newdata[$key]['tax'] = '0.000';
                        if (isset($arr['amount']))
                            $newdata[$key]['amount'] = number_format((float) $arr['amount'], 2);
                        else
                            $newdata[$key]['amount'] = '0.00';
						unset($newdata[$key]['id']);
						$newdata[$key]['_id'] = $key;
						$newdata[$key]['sort_key'] = $this->opm->num_to_string($key).'-'.'0';

						$option_for = '';
						if(isset($arr['option_for']) && $arr['option_for']!=''){
                            $newdata[$key]['xempty']['option']      = '1';
                            $newdata[$key]['xempty']['view_costing']      = '1';
                            if(isset($arr['same_parent'])&&$arr['same_parent']==1)
                                $newdata[$key]['xempty']['custom_unit_price']   = '1';
                            $newdata[$key]['_id'] = $key;
							$newdata[$key]['sku_disable'] = '1';
							$newdata[$key]['sku'] = '';
							$newdata[$key]['remove_deleted'] = '1';
							$newdata[$key]['icon']['products_name'] = (is_object($arr['products_id']) ? URL.'/products/entry/'.$arr['products_id'] : '#');
							$newdata[$key]['sort_key'] = $this->opm->num_to_string($arr['option_for']).'-'.$key;
							if($mod!='options_list')
							     unset($newdata[$key]['products_id']);
						}


                        //data RFQ's
                        $receipts = 0;
                        if (isset($query['rfqs']) && is_array($query['rfqs']) && count($query['rfqs']) > 0) {
                            foreach ($query['rfqs'] as $rk => $rv) {
                                if (!$rv['deleted'] && isset($rv['rfq_code']) && (int) $rv['rfq_code'] == $key) {
                                    $receipts = 1;
                                }
                            }
                            $newdata[$key]['receipts'] = $receipts;
                        } else
                            $newdata[$key]['receipts'] = 0;

                        //chặn không cho custom size nếu is_custom_size = 1
                        if(is_object($arr['products_id'])){
                            $product = $this->Product->select_one(array('_id'=>new MongoId($arr['products_id'])),array('is_custom_size'));
                            if(isset($product['is_custom_size'])&&$product['is_custom_size']==1){
                                $newdata[$key]['xlock']['sizeh'] = '1';
                                $newdata[$key]['xlock']['sizew'] = '1';
                                $newdata[$key]['xlock']['sizeh_unit'] = '1';
                                $newdata[$key]['xlock']['sizew_unit'] = '1';
                                $newdata[$key]['xlock']['sell_by'] = '1';
                            }
                        }


						//empty neu same_parent = 1
						if(isset($arr['same_parent']) && $arr['same_parent']==1){
							$newdata[$key]['xlock']['products_name']= '1';
							$newdata[$key]['xempty']['sell_by'] 	= '1';
							$newdata[$key]['xempty']['sell_price'] 	= '1';
							$newdata[$key]['xempty']['oum'] 		= '1';
							$newdata[$key]['xempty']['quantity'] 	= '1';
							$newdata[$key]['xempty']['sizew'] 		= '1';
							$newdata[$key]['xempty']['sizew_unit'] 	= '1';
							$newdata[$key]['xempty']['sizeh'] 		= '1';
							$newdata[$key]['xempty']['sizeh_unit'] 	= '1';
							$newdata[$key]['xempty']['unit_price'] 	= '1';
							$newdata[$key]['xempty']['adj_qty'] 	= '1';
							$newdata[$key]['xempty']['sub_total'] 	= '1';
							$newdata[$key]['xempty']['tax'] 		= '1';
							$newdata[$key]['xempty']['amount'] 		= '1';
							$newdata[$key]['xempty']['option'] 		= '1';
							$newdata[$key]['xempty']['receipts'] 	= '1';
						}


						//empty neu sell_by parent = combination
						if($option_for!='' && isset($query[$opname][$option_for]['sell_by']) && $query[$opname][$option_for]['sell_by']=='combination'){
							//$newdata[$key]['xempty']['sub_total'] = '1';
							$newdata[$key]['xempty']['tax'] 		= '1';
							$newdata[$key]['xempty']['amount'] 		= '1';
						}

						//khoa Sold by neu la combination
						if (isset($newdata[$key]['sell_by']) && $newdata[$key]['sell_by']=='combination') {
							$newdata[$key]['xlock']['sell_by']= '1';
							$newdata[$key]['xlock']['sell_price']= '1';
							$newdata[$key]['xlock']['oum']= '1';
						}



                        //set lại select dựa vào loại sell_by
                        if (isset($newdata[$key]['sell_by'])) {
                            $option_select_dynamic['oum_' . $key] = $this->Setting->select_option_vl(array('setting_value' => 'product_oum_' . strtolower($arr['sell_by'])));
                        }
                        if(isset($arr['option_for']))
                            $option_for_sort['option'][$arr['option_for']][$key] = $newdata[$key];
                        else
                            $option_for_sort['parent'][$key] = $newdata[$key];

                    } //end if
                }
            }
            $arr_ret[$opname] =array();
            $this->selectModel('Product');
            if(isset($option_for_sort['parent'])){
                foreach($option_for_sort['parent'] as $p_key=>$parent){
                    $arr_ret[$opname][] = $parent;
                    if(!isset($option_for_sort['option'][$p_key])) continue;
                    if(is_object($parent['products_id'])){
                        $p_product = $this->Product->select_one(array('_id'=> new MongoId($parent['products_id'])),array('options'));
                        if(isset($p_product['options'])&&!empty($p_product['options'])){
                            foreach($option_for_sort['option'][$p_key] as $k_opt=>$opt){
                                if(!isset($opt['proids'])) continue;
                                $opt_key = str_replace((string)$parent['products_id'].'_', '', $opt['proids']);
                                $option_for_sort['option'][$p_key][$k_opt]['option_group'] = (isset($p_product['options'][$opt_key]['option_group']) ? $p_product['options'][$opt_key]['option_group'] : '');
                                $option_for_sort['option'][$p_key][$k_opt]['option_group_for_sort'] = $option_for_sort['option'][$p_key][$k_opt]['option_group'].'_'.$option_for_sort['option'][$p_key][$k_opt]['products_name'];
                            }
                        }
                    }
                    if(isset($option_for_sort['option'])){
                        if(isset($query['options'])&&!empty($query['options'])){
                            foreach($option_for_sort['option'][$p_key] as $k_opt=>$opt){
                                $line_no = $opt['_id'];
                                foreach($query['options'] as  $custom_opt_k=>$custom_opt_v){
                                    if(isset($custom_opt_v['deleted'])&&$custom_opt_v['deleted']){
                                        unset($query['options'][$custom_opt_k]);
                                        continue;
                                    }
                                    if(!isset($custom_opt_v['line_no']) || $custom_opt_v['line_no']!=$line_no) continue;
                                    if(!isset($custom_opt_v['option_group']))
                                        $custom_opt_v['option_group'] = '';
                                    $option_for_sort['option'][$p_key][$k_opt]['option_group'] = $custom_opt_v['option_group'];
                                    $option_for_sort['option'][$p_key][$k_opt]['option_group_for_sort'] = $option_for_sort['option'][$p_key][$k_opt]['option_group'].'_'.$option_for_sort['option'][$p_key][$k_opt]['products_name'];
                                    unset($query['options'][$custom_opt_k]); break;
                                }
                            }
                        }
                        $option_for_sort['option'][$p_key] = $this->opm->aasort($option_for_sort['option'][$p_key],'option_group_for_sort');
                        foreach($option_for_sort['option'][$p_key] as $value)
                            array_push($arr_ret[$opname], $value);
                    }
                }
            }
        }
        $this->set('option_select_dynamic', $option_select_dynamic);
        return $arr_ret;
    }


	//subtab Text entry
	public function view_product_option(){
		echo '';
		die;
	}

	//subtab Text entry
	public function text_entry(){
		$this->is_text = 1;
		$this->line_entry();
	}

	//Text pdf
	public function test_pdf(){
		$this->layout = 'pdf';
		//set footer
		$this->render('test_pdf');
	}

	public function email_pdf(){
		$this->layout = 'pdf';
		$ids = $this->get_id();
		if($ids!=''){
			$query = $this->opm->select_one(array('_id' =>new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link','img/logo_anvy.jpg');
			$this->set('company_address','Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

			//customer address
			$customer = '';
			if(isset($arrtemp['company_id']) && strlen($arrtemp['company_id'])==24)
				$customer .= '<b>'.$this->get_name('Company',$arrtemp['company_id']).'</b><br />';
			else if(isset($arrtemp['company_name']))
				$customer .= '<b>'.$arrtemp['company_name'].'</b><br />';
			if(isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id'])==24)
				$customer .= $this->get_name('Contact',$arrtemp['contact_id']).'<br />';
			else if(isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'].'<br />';

			//loop 2 address
			$arradd = array('invoice','shipping');
			foreach($arradd as $vvs){
				$kk = $vvs; $customer_address = '';
				if(isset($arrtemp[$kk.'_address']) && isset($arrtemp[$kk.'_address'][0]) && count($arrtemp[$kk.'_address'])>0){
					$temp = $arrtemp[$kk.'_address'][0];
					if(isset($temp[$kk.'_address_1']) && $temp[$kk.'_address_1']!='')
						$customer_address .= $temp[$kk.'_address_1'].' ';
					if(isset($temp[$kk.'_address_2']) && $temp[$kk.'_address_2']!='')
						$customer_address .= $temp[$kk.'_address_2'].' ';
					if(isset($temp[$kk.'_address_3']) && $temp[$kk.'_address_3']!='')
						$customer_address .= $temp[$kk.'_address_3'].'<br />';
					else
						$customer_address .= '<br />';
					if(isset($temp[$kk.'_town_city']) && $temp[$kk.'_town_city']!='')
						$customer_address .= $temp[$kk.'_town_city'];

					if(isset($temp[$kk.'_province_state']))
						$customer_address .= ' '.$temp[$kk.'_province_state'].' ';
					else if(isset($temp[$kk.'_province_state_id']) && isset($temp[$kk.'_country_id'])){
						$keytemp = $temp[$kk.'_province_state_id'];
						$provkey = $this->province($temp[$kk.'_country_id']);
						if(isset($provkey[$temp]))
							$customer_address .= ' '.$provkey[$temp].' ';
					}


					if(isset($temp[$kk.'_zip_postcode']) && $temp[$kk.'_zip_postcode']!='')
						$customer_address .= $temp[$kk.'_zip_postcode'];

					if(isset($temp[$kk.'_country']) && isset($temp[$kk.'_country_id']) && (int)$temp[$kk.'_country_id']!="CA")
						$customer_address .= ' '.$temp[$kk.'_country'].'<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}

			if(isset($arrtemp['name']) && $arrtemp['name']!='')
				$heading = $arrtemp['name'];
			else
				$heading = '';
			if(!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address',$customer.$arr_address['invoice']);
			if(!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			$this->set('shipping_address',$arr_address['shipping']);

			// info data
			$info_data = (object) array();
			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = $arrtemp['job_number'];
			$info_data->date = $this->opm->format_date($arrtemp['invoice_date']);
			$info_data->po_no = isset($arrtemp['customer_po_no'])?$arrtemp['customer_po_no']:'';
			$info_data->ac_no = '';
			$info_data->terms = isset($arrtemp['payment_terms'])?$arrtemp['payment_terms']:'';
			$info_data->required_date = $this->opm->format_date($arrtemp['payment_due_date']);
			$this->set('info_data', $info_data);


			//$this->set('quote_date',$this->opm->format_date($arrtemp['quotation_date']));
			/**Nội dung bảng giá */
			$date_now = date('Ymd');
			$time=time();
			$filename = 'SIN'.$date_now.$time.'-'.$info_data->no;


			$thisfolder = 'upload'.DS.date("Y_m");
			$thisfolder_1='upload'.','.date("Y_m");

			$folder = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.$thisfolder;
			if (!file_exists($folder)) {
				mkdir($folder, 0777, true);
			}

			$this->set('filename', $filename);
			$this->set('heading',$heading);
			$html_cont = '';
			if(isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products'])>0){
				$line = 0; $colum = 7;
				foreach($arrtemp['products'] as $keys=>$values){
					if(!$values['deleted']){
						if($line%2==0)
							$bgs = '#fdfcfa';
						else
							$bgs = '#eeeeee';
						//code
						$html_cont .= '<tr style="background-color:'.$bgs.';"><td class="first">';
						if(isset($values['code']))
							$html_cont .= '  '.$values['code'];
						else
							$html_cont .= '  #'.$keys;
						//desription
						$html_cont .= '</td><td>';
						if(isset($values['products_name']))
							$html_cont .= str_replace("\n","<br />",$values['products_name']);
						else
							$html_cont .= 'Empty';
						//width
						$html_cont .= '</td><td align="right">';
						if(isset($values['sizew']) && $values['sizew']!='' && isset($values['sizew_unit']) && $values['sizew_unit']!='')
							$html_cont .= $values['sizew'].' ('.$values['sizew_unit'].')';
						else if(isset($values['sizew'])&& $values['sizew']!='')
							$html_cont .= $values['sizew'].' (in.)';
						else
							$html_cont .= '';
						//height
						$html_cont .= '</td><td align="right">';
						if(isset($values['sizeh']) && $values['sizeh']!='' && isset($values['sizeh_unit']) && $values['sizeh_unit']!='')
							$html_cont .= $values['sizeh'].' ('.$values['sizeh_unit'].')';
						else if(isset($values['sizeh']) && $values['sizeh']!='' )
							$html_cont .= $values['sizeh'].' (in.)';
						else
							$html_cont .= '';
						//Unit price
						$html_cont .= '</td><td align="right">';
						if(isset($values['unit_price']))
							$html_cont .= $this->opm->format_currency($values['unit_price']);
						else
							$html_cont .= '0.00';
						//Qty
						$html_cont .= '</td><td align="right">';
						if(isset($values['quantity']))
							$html_cont .= $values['quantity'];
						else
							$html_cont .= '';
						//line total
						$html_cont .= '</td><td align="right" class="end">';
						if(isset($values['sub_total']))
							$html_cont .= $this->opm->format_currency($values['sub_total']);
						else
							$html_cont .= '';


						$html_cont .= '</td></tr>';
						$line++;
					}//end if deleted
				}//end for


				if($line%2==0){
					$bgs = '#fdfcfa';$bgs2 = '#eeeeee';
				}else{
					$bgs = '#eeeeee';$bgs2 = '#fdfcfa';
				}

				$sub_total = $total = $taxtotal = 0.00;
				if(isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if(isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if(isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				//Sub Total
				$html_cont .= '<tr style="background-color:'.$bgs.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;border-top:2px solid #aaa;" class="first">Sub Total:</td>
									<td align="right" style="border-top:2px solid #aaa;" class="end">'.$this->opm->format_currency($sub_total).'</td>
							   </tr>';
				//GST
				$html_cont .= '<tr style="background-color:'.$bgs2.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;" class="first">HST/GST:</td>
									<td align="right" class="end">'.$this->opm->format_currency($taxtotal).'</td>
							   </tr>';
				//Total
				$html_cont .= '<tr style="background-color:'.$bgs.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;" class="first bottom">Total:</td>
									<td align="right" class="end bottom">'.$this->opm->format_currency($total).'</td>
							   </tr>';

			}//end if


			$this->set('html_cont',$html_cont);
			if(isset($arrtemp['our_csr'])){
				$this->set('user_name',' '.$arrtemp['our_csr']);
			}else
				$this->set('user_name',' '.$this->opm->user_name());
			//end set content
			$this->set('link_this_folder',$thisfolder);
			$this->render('email_pdf');
			$v_link_pdf= $thisfolder_1.','.$filename.'.pdf';
			$v_file_name=$filename.'.pdf';

			$this->redirect('/docs/add_from_option/'.$this->ModuleName().'/'.$this->get_id().'/'.$v_link_pdf.'/'.$v_file_name.'/'.$this->params->params['controller'].'');



		}
		die;
	}
	public function view_pdf($getfile=false,$ids='') {
		$this->layout = 'pdf';
		$info_data = (object) array();
		if($ids=='')
			$ids = $this->get_id();
		if ($ids != '') {
			$query = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link', 'img/logo_anvy.jpg');
			$this->set('company_address', 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

			//customer address
			$customer = '';
			if (isset($arrtemp['company_id']) && strlen($arrtemp['company_id']) == 24)
				$customer .= '<b>' . $this->get_name('Company', $arrtemp['company_id']) . '</b><br />';
			else if (isset($arrtemp['company_name']))
				$customer .= '<b>' . $arrtemp['company_name'] . '</b><br />';
			if (isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id']) == 24)
				$customer .= $this->get_name('Contact', $arrtemp['contact_id']) . '<br />';
			else if (isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'] . '<br />';

			//loop 2 address
			$arradd = array('invoice');
			foreach ($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
						$customer_address .= $temp[$kk . '_address_1'] . ' ';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
						$customer_address .= $temp[$kk . '_address_2'] . ' ';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
						$customer_address .= $temp[$kk . '_address_3'] . '<br />';
					else
						$customer_address .= '<br />';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
						$customer_address .= $temp[$kk . '_town_city'];

					if (isset($temp[$kk . '_province_state']))
						$customer_address .= ' ' . $temp[$kk . '_province_state'] . ' ';
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp]))
							$customer_address .= ' ' . $provkey[$temp] . ' ';
					}


					if (isset($temp[$kk . '_zip_postcode']) && $temp[$kk . '_zip_postcode'] != '')
						$customer_address .= $temp[$kk . '_zip_postcode'];

					if (isset($temp[$kk . '_country']) && isset($temp[$kk . '_country_id']) && (int) $temp[$kk . '_country_id'] != "CA")
						$customer_address .= ' ' . $temp[$kk . '_country'] . '<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}


			if (isset($arrtemp['heading']) && $arrtemp['heading'] != '')
				$heading = $arrtemp['heading'];
			else
				$heading = '';
			if (!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address', $customer . $arr_address['invoice']);

			$this->set('ref_no', $arrtemp['code']);

			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = $arrtemp['job_number'];
			$info_data->date = $this->opm->format_date($arrtemp['invoice_date']);
			$info_data->po_no = $arrtemp['customer_po_no'];
			$info_data->ac_no = '';
			$info_data->terms = $arrtemp['payment_terms'];
			$info_data->due_date = $this->opm->format_date($arrtemp['payment_due_date']);

			$this->set('info_data', $info_data);
			/*             * Nội dung bảng giá */
			$date_now = date('Ymd');
			$numkey = explode("-",$info_data->no);
			$filename = 'INV-'.$numkey[count($numkey)-1];
			$other_comment = '';
			if(isset($arrtemp['other_comment']))
				$other_comment = str_replace("\n","<br />",'<br />'.$arrtemp['other_comment']);
			$this->set('other_comment',$other_comment);
			$this->set('filename', $filename);
			$this->set('heading', $heading);
			$html_cont = '';
			$line_entry_data = $this->line_entry_data();
			$minimum = $this->get_minimum_order();
			if($arrtemp['sum_sub_total']<$minimum){
                $more_sub_total = $minimum - (float)$arrtemp['sum_sub_total'];
                if(isset($line_entry_data['products']) && !empty($line_entry_data['products'])){
                    $last_insert = end($line_entry_data['products']);
                    foreach($last_insert as $key=>$value){
                        if($key=='deleted' || $key == 'taxper') continue;
                        $last_insert[$key] = '';
                    }
                }
                $last_insert['products_name'] = 'Minimum Order Adjustment';
                $last_insert['quantity'] = '1';
                if(!isset($last_insert['taxper']))
                        $last_insert['taxper']= 0;
                $last_insert['custom_unit_price'] = $last_insert['sub_total'] = $more_sub_total;
                $last_insert['tax'] = $last_insert['sub_total']*$last_insert['taxper']/100;
                $last_insert['amount'] = $last_insert['sub_total']+$last_insert['tax'];
                array_push($line_entry_data['products'], $last_insert);
                $arrtemp['sum_sub_total']+=$more_sub_total;
                $arrtemp['sum_tax']+=$last_insert['tax'];
                $arrtemp['sum_amount']+=$last_insert['amount'];
            }
			if (isset($line_entry_data['products']) && is_array($line_entry_data['products']) && count($line_entry_data['products']) > 0) {
				$line = 0;
				$colum = 7;
				$options = array();
				if(isset($arrtemp['options']) && !empty($arrtemp['options']) )
					$options = $arrtemp['options'];
				foreach ($line_entry_data['products'] as $values) {
						$keys = $values['_id'];
                        if (!$values['deleted']) {
                            if ($line % 2 == 0)
                                $bgs = '#fdfcfa';
                            else
                                $bgs = '#eeeeee';
                            //code
                            $html_cont .= '<tr style="background-color:' . $bgs . ';"><td class="first">';
                            if(isset($values['option_for'])&&is_numeric($values['option_for'])){
                            	$values['sku'] = '';
                            	$values['products_name'] = '&nbsp;&nbsp;&nbsp;•'.$values['products_name'];
                            }
                             if (isset($values['sku']))
                                $html_cont .= '  ' . $values['sku'];
                            else
                                $html_cont .= '  #' . $keys;
                            //desription
                            $html_cont .= '</td><td>';
                            if (isset($values['products_name']))
                                $html_cont .= str_replace("\n", "<br />", $values['products_name']);
                            else
                                $html_cont .= 'Empty';

							//clear các dòng phía sau nếu là same product parent
							if(isset($values['same_parent']) && $values['same_parent']==1){
								$html_cont .= '</td><td></td><td></td><td></td><td></td><td class="end"></td></tr>';
								 $line++;
								continue;
							}


							//width
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizew']) && $values['sizew'] != '' && isset($values['sizew_unit']) && $values['sizew_unit'] != '')
                                $html_cont .= $values['sizew'] . ' (' . $values['sizew_unit'] . ')';
                            else if (isset($values['sizew']) && $values['sizew'] != '')
                                $html_cont .= $values['sizew'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //height
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizeh']) && $values['sizeh'] != '' && isset($values['sizeh_unit']) && $values['sizeh_unit'] != '')
                                $html_cont .= $values['sizeh'] . ' (' . $values['sizeh_unit'] . ')';
                            else if (isset($values['sizeh']) && $values['sizeh'] != '')
                                $html_cont .= $values['sizeh'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //Unit price
                            $html_cont .= '</td><td align="right">';
							if (!isset($values['custom_unit_price']))
                                $values['custom_unit_price'] = (isset($values['unit_price']) ? $values['unit_price'] : 0);
                            $html_cont .= $this->opm->format_currency($values['custom_unit_price']);
                            //Qty
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['quantity']))
                                $html_cont .= $values['quantity'];
                            else
                                $html_cont .= '';
                            //line sub_total
                            $html_cont .= '</td><td align="right" class="end">';
                            if (isset($values['sub_total']))
                                $html_cont .= $this->opm->format_currency($values['sub_total']);
                            else
                                $html_cont .= '';
                            $html_cont .= '</td></tr>';
                            $line++;
                        }//end if deleted
                    }//end for

				if ($line % 2 == 0) {
					$bgs = '#fdfcfa';
					$bgs2 = '#eeeeee';
				} else {
					$bgs = '#eeeeee';
					$bgs2 = '#fdfcfa';
				}

				$sub_total = $total = $taxtotal = 0.00;
				if (isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if (isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if (isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				//Sub Total
				$html_cont .= '<tr style="background-color:' . $bgs . ';">
									<td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;border-top:2px solid #aaa;" class="first">Sub Total:</td>
									<td align="right" style="border-top:2px solid #aaa;" class="end">' . $this->opm->format_currency($sub_total) . '</td>
							   </tr>';
				//GST
				$html_cont .= '<tr style="background-color:' . $bgs2 . ';">
									<td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;" class="first">HST/GST:</td>
									<td align="right" class="end">' . $this->opm->format_currency($taxtotal) . '</td>
							   </tr>';
				//Total
				$html_cont .= '<tr style="background-color:' . $bgs . ';">
									<td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;" class="first bottom">Total:</td>
									<td align="right" class="end bottom">' . $this->opm->format_currency($total) . '</td>
							   </tr>';
			}//end if
			$this->selectModel('Company');
			$company = $this->Company->select_one(array('system' => true),array('_id'));
			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $company['_id']),array('tax_no'));
			if(!isset($salesaccount['tax_no']))
				$salesaccount['tax_no'] = '';

			$this->set('tax',$salesaccount['tax_no']);
			$this->set('html_cont', $html_cont);
			if (isset($arrtemp['our_csr'])) {
				$this->set('user_name', ' ' . $arrtemp['our_csr']);
			} else
				$this->set('user_name', ' ' . $this->opm->user_name());
			//end set content
			//set footer
			$this->render('view_pdf');
			if($getfile)
				return $filename.'.pdf';
			$this->redirect('/upload/' . $filename . '.pdf');
		}
		die;
	}

	public function other($salesinvoice_id){
		$data = $this->opm->select_one(array('_id' => new MongoId($salesinvoice_id)));
		$this->set('data', $data);
		// BaoNam: gọi view ctp communications dùng chung
		$this->communications($salesinvoice_id, true);
	}


	//address
	public function set_entry_address($arr_tmp,$arr_set){
		$address_fset = array('address_1','address_2','address_3','town_city','country','province_state','zip_postcode');
		$address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
		$address_controller = array('invoice','shipping');
		$address_value['invoice'] = $address_value['shipping']= array('','','','',"CA",'','');
		$this->set('address_controller',$address_controller);//set
		$address_key 	= array('invoice','shipping');
		$this->set('address_key',$address_key);//set
		$address_country 	= $this->country();
		foreach($address_key as $kss=>$vss){
			//neu ton tai address trong data base
			if(isset($arr_tmp[$vss.'_address'][0])){
				$arr_temp_op = $arr_tmp[$vss.'_address'][0];
				for($i=0;$i<count($address_fset);$i++){ //loop field and set value for display
					if(isset($arr_temp_op[$vss.'_'.$address_fset[$i]])){
						$address_value[$vss][$i] = $arr_temp_op[$vss.'_'.$address_fset[$i]];
					}else{
						$address_value[$vss][$i] = '';
					}
				}//pr($arr_temp_op);die;
				//get province list and country list

				if(isset($arr_temp_op[$vss.'_country_id']))
					$address_province[$vss] = $this->province($arr_temp_op[$vss.'_country_id']);
				else
					$address_province[$vss] = $this->province();
				//set province
				if(isset($arr_temp_op[$vss.'_province_state_id']) && $arr_temp_op[$vss.'_province_state_id']!='' && isset($address_province[$vss][$arr_temp_op[$vss.'_province_state_id']]) )
					$address_province_id[$kss] = $arr_temp_op[$vss.'_province_state_id'];
				else if(isset($arr_temp_op[$vss.'_province_state']))
					$address_province_id[$kss] = $arr_temp_op[$vss.'_province_state'];
				else
					$address_province_id[$kss] = '';

				//set country
				if(isset($arr_temp_op[$vss.'_country_id'])){
					$address_country_id[$kss] = $arr_temp_op[$vss.'_country_id'];
					$address_province[$vss] = $this->province($arr_temp_op[$vss.'_country_id']);
				}else{
					$address_country_id[$kss] = "CA";
					$address_province[$vss] = $this->province("CA");
				}

				$address_add[$vss] = '0';
			//chua co address trong data
			}else{
				$address_country_id[$kss] = "CA";
				$address_province[$vss] = $this->province("CA");
				$address_add[$vss] = '1';
			}
		}
		//pr($address_province);
		$this->set('address_value',$address_value);
		$address_hidden_field = array('invoice_address','shipping_address');
		$this->set('address_hidden_field',$address_hidden_field);//set
		$address_label[0] = $arr_set['field']['panel_2']['invoice_address']['name'];
		$address_label[1] = $arr_set['field']['panel_2']['shipping_address']['name'];
		$this->set('address_label',$address_label);//set
		$address_conner[0]['top'] 		= 'hgt fixbor';
		$address_conner[0]['bottom'] 	= 'fixbor2 jt_ppbot';
		$address_conner[1]['top'] 		= 'hgt';
		$address_conner[1]['bottom'] 	= 'fixbor3 jt_ppbot';
		$this->set('address_conner',$address_conner);//set
		$this->set('address_country',$address_country);//set
		$this->set('address_country_id',$address_country_id);//set
		$this->set('address_province',$address_province);//set
		$this->set('address_province_id',$address_province_id);//set
		$this->set('address_more_line',2);//set
		$this->set('address_onchange',"save_address_pr('\"+keys+\"');");
		if(isset($arr_tmp['company_id']) && strlen($arr_tmp['company_id'])==24)
		$this->set('address_company_id','company_id');
		if(isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id'])==24)
		$this->set('address_contact_id','contact_id');
		$this->set('address_add',$address_add);
	}


	// Popup form orther module
	function popup($key = "") {
		$this->set('key', $key);
        $limit = 100;
        $skip = 0;
        $cond = array();
        // Nếu là search GET
        if (!empty($_GET)) {

            $tmp = $this->data;

            if (isset($_GET['company_id'])) {
                $cond['company_id'] = new MongoId($_GET['company_id']);
                $tmp['Salesinvoice']['company'] = $_GET['company_name'];
            }

            $this->data = $tmp;
        }

        // Nếu là search theo phân trang
        $page_num = 1;
        if (isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0) {
            // $limit = $_POST['pagination']['page-list'];
            $page_num = $_POST['pagination']['page-num'];
            $limit = $_POST['pagination']['page-list'];
            $skip = $limit * ($page_num - 1);
        }
        $this->set('page_num', $page_num);
        $this->set('limit', $limit);
        $arr_order = array('first_name' => 1);
        if (isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0) {
            $sort_type = 1;
            if ($_POST['sort']['type'] == 'desc') {
                $sort_type = -1;
            }
            $arr_order = array($_POST['sort']['field'] => $sort_type);

            $this->set('sort_field', $_POST['sort']['field']);
            $this->set('sort_type', ($sort_type === 1) ? 'asc' : 'desc');
            $this->set('sort_type_change', ($sort_type === 1) ? 'desc' : 'asc');
        }

        // search theo submit $_POST kèm điều kiện
        if (!empty($this->data) && !empty($_POST) && isset($this->data['Salesinvoice'])) {
            $arr_post = $this->Common->strip_search($this->data['Salesinvoice']);
            if (isset($arr_post['contact_name']) && strlen($arr_post['contact_name']) > 0) {
                $cond['contact_name'] = new MongoRegex('/' . trim($arr_post['contact_name']) . '/i');
            }

            if (strlen($arr_post['company']) > 0) {
                $cond['company_name'] = new MongoRegex('/' . trim($arr_post['company']) . '/i');
            }
        }

        $arr_salesinvoices = $this->opm->select_all(array(
            'arr_where' => $cond,
            'arr_order' => $arr_order,
            'limit' => $limit,
            'skip' => $skip
                // 'arr_field' => array('name', 'is_customer', 'is_employee', 'company_id', 'company_name')
        ));
        $this->set('arr_salesinvoices', $arr_salesinvoices);

        $total_page = $total_record = $total_current = 0;
        if (is_object($arr_salesinvoices)) {
            $total_current = $arr_salesinvoices->count(true);
            $total_record = $arr_salesinvoices->count();
            if ($total_record % $limit != 0) {
                $total_page = floor($total_record / $limit) + 1;
            } else {
                $total_page = $total_record / $limit;
            }
        }
        $this->set('total_current', $total_current);
        $this->set('total_page', $total_page);
        $this->set('total_record', $total_record);

        $this->layout = 'ajax';
	}

	public function receipt(){
		$invoice = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('invoice_status'));
		$this->set('invoice_status',(isset($invoice['invoice_status']) ? $invoice['invoice_status'] : 'In Progress'));
	}
	public function receipt_overall_total($total_receipt=0)
	{
		if($this->check_permission('salesinvoices_@_entry_@_view')){
			$id = $this->get_id();
			$this->selectModel('Salesinvoice');
			$salesinvoice = $this->Salesinvoice->select_one(array('_id'=> new MongoId($id)),array('sum_amount','company_id','taxval','sum_sub_total'));
			$minimum = 50;
	        $this->selectModel('Stuffs');
	        $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
	        $product_id = $product['product_id'];
	        if(isset($product['product_id'])&&is_object($product['product_id'])){
	            $this->selectModel('Product');
	            $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('sell_price'));
	            $minimum = $product['sell_price'];
	            $product_id = $product['_id'];
	        }
			if(is_object($salesinvoice['company_id'])){
					$company_id = $salesinvoice['company_id'];
					$this->selectModel('Company');
					$company = $this->Company->select_one(array('_id'=>$company_id),array('pricing'));
				if(isset($company['pricing'])){
					foreach($company['pricing'] as $pricing){
						if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
						if((string)$pricing['product_id']!=(string)$product_id) continue;
						if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
						$price_break = reset($pricing['price_break']);
						$minimum =  (float)$price_break['unit_price']; break;
					}
				}
			}
			if($salesinvoice['sum_sub_total']<$minimum){
				$salesinvoice['taxval'] = (isset($salesinvoice['taxval']) ? $salesinvoice['taxval'] : 0);
				$salesinvoice['sum_amount'] = $minimum + ($minimum*$salesinvoice['taxval']/100);
			}
			$salesinvoice['total_invoice'] =  $salesinvoice['sum_amount'];
			$salesinvoice['balance'] = $salesinvoice['total_invoice'];
			$salesinvoice['total_receipt'] = 0;
			if($total_receipt!=0){
				$total_receipt = str_replace(',', '', $total_receipt);
				$salesinvoice['total_receipt'] = $total_receipt;
				$salesinvoice['balance'] = (float)($salesinvoice['total_invoice'] - $salesinvoice['total_receipt']);
			}
			$salesinvoice['total_invoice'] = number_format($salesinvoice['total_invoice'],2);
			$salesinvoice['total_receipt'] = number_format($salesinvoice['total_receipt'],2);
			$salesinvoice['balance'] = number_format($salesinvoice['balance'],2);
			echo json_encode($salesinvoice);
			die;
		}
	}
	public function receipt_content()
	{
		if($this->check_permission('salesinvoices_@_entry_@_view')){
			$id = $this->get_id();
			$this->selectModel('Salesinvoice');
			$si = $this->Salesinvoice->select_one(array('_id'=>new MongoId($id)));
			$this->selectModel('Receipt');
			$receipts = $this->Receipt->select_all(array(
												'arr_where'=> array(
																'allocation'=>array(
																				'$elemMatch'=>array(
																					'salesinvoice_id'=>new MongoId($id),
																					'deleted'		=>false,
																								)
																				)
																	),
												'arr_order'=>array('code'=>-1),
												'arr_field'=>array('_id','code','notes','account','amount_received','receipt_date','paid_by','our_bank_account','allocation')
									));
			$arr_data = array();
			$arr_receipt = array();
			foreach($receipts as $value)
				foreach($value['allocation'] as $key=>$val){
					if(!isset($val['salesinvoice_id']) || $val['salesinvoice_id']=='' ) continue;
					if($val['salesinvoice_id']==$id && !$val['deleted'] ){
						$arr_receipt[$value['code']]['_id'] = $value['_id'];
						$arr_receipt[$value['code']]['code'] = $value['code'];
						$arr_receipt[$value['code']]['date'] = $value['receipt_date'];
						$arr_receipt[$value['code']]['paid_by'] = (isset($value['paid_by']) ? $value['paid_by'] : '');
						$arr_receipt[$value['code']]['our_bank_account'] = (isset($value['our_bank_account']) ? $value['our_bank_account'] : '');
						$arr_receipt[$value['code']]['receipts'][$key]['write_off'] = (isset($val['write_off']) ? $val['write_off'] : 0);
						$arr_receipt[$value['code']]['receipts'][$key]['key'] = $key;
						$arr_receipt[$value['code']]['receipts'][$key]['note'] = (isset($val['note']) ? $val['note'] : '');
						$arr_receipt[$value['code']]['receipts'][$key]['amount'] = $val['amount'];
					}
				}
			$arr_data['paid_by'] =$this->Setting->select_option_vl(array('setting_value'=>'receipts_paid_by'));
			$arr_data['account'] = $this->Setting->select_option_vl(array('setting_value'=>'receipts_our_bank_account'));
			$this->set('arr_data',$arr_data);
			$this->set('arr_receipt',$arr_receipt);
		}
	}
	public function update_receipt()
	{
		if($this->check_permission('salesinvoices_@_entry_@_edit')){
			if(empty($_POST))
			{
				echo 'error';
				die;
			}
			$id = $this->get_id();
			$receipt_id = $_POST['id'];
			$this->selectModel('Receipt');
			$receipt = $this->Receipt->select_one(array('_id'=>new MongoId($receipt_id)));
			$this->selectModel('Salesinvoice');
			$invoice = $this->Salesinvoice->select_one(array('_id'=>new MongoId($this->get_id())));

			$key = $_POST['key'];

			$total_receipt = 0;
			if(isset($invoice['total_receipt']))$total_receipt = $invoice['total_receipt'];

			$invoice['total_receipt'] = (isset($invoice['total_receipt']) ? (float)$invoice['total_receipt'] : 0) - (isset($receipt['allocation'][$key]['amount']) ? (float)$receipt['allocation'][$key]['amount'] : 0) + (isset($_POST['amount']) ? (float)$_POST['amount'] : 0);

			//Update lại balance và receipt cho SA
			$this->selectModel('Salesaccount');
			if(isset($invoice['company_id']) && is_object($invoice['company_id'])){
				$this->Salesaccount->update_account($invoice['company_id'], array(
													'model' => 'Company',
													'balance' => $total_receipt - $invoice['total_receipt'],
													'receipts' => $invoice['total_receipt'] - $total_receipt,
													));
			}
			$this->Salesinvoice->save($invoice);
			$key = $_POST['key'];
			$receipt['our_bank_account'] = $_POST['our_bank_account'];
			$receipt['paid_by'] = $_POST['paid_by'];
			$receipt['receipt_date'] = new MongoDate(strtotime($_POST['receipt_date']));
			$receipt['allocation'][$key]['write_off'] = (isset($_POST['write_off']) ? 1 : 0);
			$receipt['allocation'][$key]['note'] = $_POST['note'];
			$receipt['allocation'][$key]['amount'] = $_POST['amount'];
			if($this->Receipt->save($receipt))
				echo 'ok';
		}
		die;
	}

	//
	//  Tung Report
	//
	public function report_pdf($data)
	{

		App::import('Vendor','xtcpdf');
		$pdf = new XTCPDF();
		$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Anvy Digital');
		$pdf->SetTitle('Anvy Digital Quotation');
		$pdf->SetSubject('Quotation');
		$pdf->SetKeywords('Quotation, PDF');

		// set default header data
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(2);

		// set margins
		$pdf->SetMargins(10, 3, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont($textfont, '', 9);

		// add a page
		$pdf->AddPage();


		// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
		// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

		// create some HTML content


		$html = '
		<table cellpadding="2" cellspacing="0" style="width:100%; margin: 0px auto">
		   <tbody>
			  <tr>
				 <td width="32%" valign="top" style="color:#1f1f1f;">
					<img src="img/logo_anvy.jpg" alt="" margin-bottom:0px>
					<p style="margin-bottom:5px; margin-top:0px;">Unit 103, 3016 - 10th Ave NE<br/ >Calgary  AB  T2A  6A3</p>
				 </td>
				 <td width="68%" valign="top" align="right">
					<table>
					   <tbody>
						  <tr>
							 <td width="20%">&nbsp;</td>
							 <td width="80%">
								<span style="text-align:right; font-size:21px; font-weight:bold; color: #919295;">
									'.$data['title'].'<br />';
		if(isset($data['date_equals']))
		  $date = '<span style="font-size:12px; font-weight:normal">'.$data['date_equals'].'</span>';
		else{
			if(isset($data['date_from'])&&isset($data['date_to']))
			  $date = '<span style="font-size:12px; font-weight:normal">( '.$data['date_from'].' - '.$data['date_to'].' )</span>';
			else if(isset($data['date_from']))
			  $date = '<span style="font-size:12px; font-weight:normal">From '.$data['date_from'].'</span>';
			else if(isset($data['date_to']))
			  $date = '<span style="font-size:12px; font-weight:normal">To '.$data['date_to'].'</span>';
			else
			  $date = '';
		}
		$html .= $date;
		$html .=                    '
								</span>
								<div style=" border-bottom: 1px solid #cbcbcb;height:5px">&nbsp;</div>
							 </td>
						  </tr>
						  <tr>
							 <td colspan="2">
									<span style="font-weight:bold;">Printed at: </span>'.$data['current_time'].'
							 </td>
						  </tr>
					   </tbody>
					</table>
				 </td>
			  </tr>
		   </tbody>
		</table>
		<div class="option">'.@$data['heading'].'</div>
		<br />
		<br />
		<div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
		<br />
		<style>
		   td{
		   line-height:2px;
		   }
		   td.first{
			text-align: center;
		   border-left:1px solid #e5e4e3;
		   }
		   td.end{
		   border-right:1px solid #e5e4e3;
		   }
		   td.top{
		   color:#fff;
		   text-align: center;
		   font-weight:bold;
		   background-color:#911b12;
		   border-top:1px solid #e5e4e3;
		   }
		   td.bottom{
		   border-bottom:1px solid #e5e4e3;
		   }
		   td.content{
			border-right: 1px solid #E5E4E3;
			text-align: center;
		   }
		   .option{
		   color: #3d3d3d;
		   font-weight:bold;
		   font-size:20px;
		   text-align: center;
		   width:100%;
		   }
		   table.maintb{
		   }
		</style>
		<br />
		';
		$html .= $data['html_loop'];

		$pdf->writeHTML($html, true, false, true, false, '');



		// reset pointer to the last page
		$pdf->lastPage();



		// ---------------------------------------------------------
		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		//$pdf->Output('example_001.pdf', 'I');




		$pdf->Output('upload/'.$data['filename'].'.pdf', 'F');


	}
	public function option_summary_customer_find()
	{
		if(!$this->check_permission($this->name.'_@_options_@_report_by_customer_summary'))
			$this->error_auth();
		$arr_data['salesinvoices_status'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_status'));
		$arr_data['salesinvoices_type'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_type'));
		$this->selectModel('Tax');
		$arr_data['salesinvoices_tax'] = $this->Tax->tax_select_list();
		$this->set('arr_data',$arr_data);
	}
	public function customer_report($type='') {
        $arr_data = array();
        if(isset($_GET['print_pdf'])){
            $arr_data = Cache::read('salesinvoices_customer_report_'.$type);
        } else {
            if (isset($_POST) && !empty($_POST)) {
                $arr_post = $_POST;
                $arr_post = $this->Common->strip_search($arr_post);
                $arr_where = array('company_id'=>array('$ne'=>''));
                if($arr_post['status'] && $arr_post['status'] != '')
                    $arr_where['invoice_status'] = $arr_post['status'];
                //Check loại trừ cancel thì bỏ các status bên dưới
                if ($arr_post['type'] && $arr_post['type'] != '')
                    $arr_where['invoice_type'] = $arr_post['type'];
                if (isset($arr_post['company']) &&$arr_post['company'] != '')
                    $arr_where['company_name'] = new MongoRegex('/' . trim($arr_post['company']) . '/i');
                if (isset($arr_post['contact'])&&$arr_post['contact'] != '')
                    $arr_where['contact_name'] = new MongoRegex('/' . trim($arr_post['contact']) . '/i');
                if (isset($arr_post['job_no'])&&$arr_post['job_no'] != '')
                    $arr_where['job_number'] = new MongoRegex('/' . trim($arr_post['job_no']) . '/i');
                //tim chinh xac ngay
                if (isset($arr_post['date_equals'])&&$arr_post['date_equals'] != '') {
                    $date_equals = $arr_post['date_equals'];
                    $date_equals = new MongoDate(strtotime(date('Y-m-d', strtotime($date_equals))));
                    $date_equals_to = new MongoDate($date_equals->sec + DAY);
                    $arr_where['invoice_date']['$gte'] = $date_equals;
                    $arr_where['invoice_date']['$lt'] = $date_equals_to;
                } else { //ngay nam trong khoang
                    //neu chi nhap date from
                    if (isset($arr_post['date_from'])&&$arr_post['date_from']!='') {
                        $date_from = new MongoDate(strtotime(date('Y-m-d', strtotime($arr_post['date_from']))));
                        $arr_where['invoice_date']['$gte'] = $date_from;
                    }
                    //neu chi nhap date to
                    if (isset($arr_post['date_to'])&&$arr_post['date_to']!='') {
                        $date_to = new MongoDate(strtotime(date('Y-m-d', strtotime($arr_post['date_to']))));
                        $date_to = new MongoDate($date_to->sec + DAY - 1);
                        $arr_where['invoice_date']['$lte'] = $date_to;
                    }
                }
                if (isset($arr_post['tax'])&&$arr_post['tax'] != '')
                    $arr_where['tax'] = new MongoRegex('/' . $arr_post['tax'] . '/i');
                if (isset($arr_post['employee'])&&$arr_post['employee'] != '')
                    $arr_where['employee'] = new MongoRegex('/' . $arr_post['employee'] . '/i');
                $this->selectModel('Salesinvoice');
                //lay het salesinvoice, voi where nhu tren va lay sum_amount giam dan
                $salesinvoice = $this->Salesinvoice->select_all(array(
                    'arr_where' => $arr_where,
                    'arr_order' => array(
                        'sum_sub_total' => -1
                    ),
                    'arr_field' => array('invoice_type','code','invoice_date','heading','invoice_status','tax','sum_sub_total','company_id','our_rep')
                ));
              	//pr($salesinvoice);die();
                if ($salesinvoice->count() == 0) {
                    echo 'empty';
                } else if(!$this->request->is('ajax')) {
                    $minimum = 50;
                    $this->selectModel('Stuffs');
                    $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
                    if(isset($product['product_id'])&&is_object($product['product_id'])){
                        $this->selectModel('Product');
                        $product = $this->Product->select_one(array('_id'=> new MongoId($product['product_id'])),array('sell_price'));
                        $minimum = $product['sell_price'];
                    }
                    if ($arr_post['report_type'] == 'summary'){
                        $arr_data = $this->summary_customer_report($arr_post, $arr_where,$minimum,$product['_id']);
                        Cache::write('salesinvoices_customer_report_'.$type, $arr_data);
                    }
                    else if ($arr_post['report_type'] == 'detailed'){
                        $arr_data = $this->detailed_customer_report($salesinvoice, $arr_post, $arr_where,$minimum,$product['_id']);
                        Cache::write('salesinvoices_customer_report_'.$type, $arr_data);
                    }
                    else{
                        $arr_data = $this->summary_customer_report($arr_post, $arr_where,$minimum,$product['_id']);
                        Cache::write('salesinvoices_customer_report_'.$type, $arr_data);
                    }
                }
            }
        }
        if($this->request->is('ajax'))
            die;
        else
            $this->render_pdf($arr_data);
    }
    function get_sum_sub_total($product_id,$origin_minimun, $arr_where)
    {
        $query = $this->opm->select_all(array(
                                         'arr_where'=>$arr_where,
                                         'arr_field'=>array('company_id','company_name')
                                         ));
        $arr_data = array();
        $arr_query = array();
        foreach($query as $value)
            $arr_query[(string)$value['company_id']] = $value['company_id'];
        foreach($arr_query as $_id){
            $sum = 0;
            $minimum = $origin_minimun;
            $company = $this->Company->select_one(array('_id'=>new MongoId($_id),'pricing.product_id'=>new MongoId($product_id)),array('pricing','name','our_rep'));
            if(isset($company['pricing'])){
                foreach($company['pricing'] as $pricing){
                    if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
                    if((string)$pricing['product_id']!=(string)$product_id) continue;
                    if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
                    $price_break = reset($pricing['price_break']);
                    $minimum =  (float)$price_break['unit_price']; break;
                }
            } else {
                $company = $this->Company->select_one(array('_id'=>$_id),array('name','our_rep'));
            }
            $arr_where_sum = array(
               'company_id'=>new MongoId($_id),
               'deleted'=>array('$ne'=>true),
               'sum_sub_total'=>array('$gt'=>$minimum)
            );
            $arr_where_sum = array_merge($arr_where,$arr_where_sum);
            $arr_where_count = array(
                'company_id'=>new MongoId($_id),
                '$or'=>array(
                              array('sum_sub_total'=>''),
                              array('sum_sub_total'=>array('$lte'=>$minimum))
                              )
            );
            $arr_where_count = array_merge($arr_where,$arr_where_count);
            $count = $this->opm->count($arr_where_count);
            $sum += $this->opm->sum('sum_sub_total','tb_salesinvoice',$arr_where_sum) + $minimum*$count;
            $_id = (string)$_id;
            $arr_data[$_id]['sum_sub_total'] = $sum;
            $arr_data[$_id]['company_name'] = (isset($company['name']) ? $company['name'] : '');
            $arr_data[$_id]['our_rep'] = (isset($company['our_rep']) ? $company['our_rep'] : '');
            $arr_data[$_id]['minimum'] = $minimum;
            $arr_data[$_id]['number_of_salesinvoices'] = $this->opm->count(array_merge($arr_where,array('company_id'=>new MongoId($_id))));
        }
        return $arr_data;
    }
    public function summary_customer_report($data, $arr_where, $minimum, $product_id) {
        //--------------------------------------
        $html = '';
        $i = $sum = 0;
        $arr_company = array();
        $this->selectModel('Company');
        $arr_company = $this->get_sum_sub_total($product_id,$minimum, $arr_where);
        //pr($arr_company);die;/////////////////////////////////////////////////////////////////////////////////////////////////////// gon summary
        foreach ($arr_company as $value) {
            $html .= '
                <tr style="background-color:' . ( $i%2==0 ? '#eeeeee' : '#fdfcfa'). ';">
                     <td class="first content" align="left">' . $value['company_name'] . '</td>
                     <td class="content" align="left">' . $value['our_rep'] . '</td>
                     <td class="content">' . $value['number_of_salesinvoices'] . '</td>
                     <td colspan="3" class="content"  align="right" class="end">' . number_format($value['sum_sub_total'], 2) . '</td>
                </tr>
            ';
            $sum += ($value['sum_sub_total'] ? $value['sum_sub_total'] : 0);
            $i++;
        }
        $html .= '
                    <tr style="background-color:' . ( $i%2==0 ? '#eeeeee' : '#fdfcfa') . ';">
                         <td colspan="2" class="bold_text right_none">' . $i . ' record(s) listed</td>
                         <td class="bold_text right_none right_text" >Total</td>
                         <td class="bold_text right_text">' . number_format($sum, 2) . '</td>
                    </tr>
                </table>
                ';
        //========================================
        //set header
        if($data['heading']!='')
            $arr_data['report_heading'] = $data['heading'];
        $arr_data['date_from_to'] = '';
        if(isset($data['date_from'])&&$data['date_from']!='')
            $arr_data['date_from_to'] .= '<span class="color_red bold_text">From</span> '.$data['date_from'].' ';
        if(isset($data['date_to'])&&$data['date_to']!='')
            $arr_data['date_from_to'] .= ' <span class="color_red bold_text">To</span> '.$data['date_to'];
        if(isset($data['date_equals'])&&$data['date_equals']!='')
            $arr_data['date_from_to'] .= $data['date_equals'];
        $arr_data['title'] = array('Customer'=>'text-align: center','Our Rep'=>'text-align: center','No. of SI'=>'text-align: center','Ex. Tax total'=>'text-align: center');
        $arr_data['content'] = $html;
        $arr_data['report_name'] = 'Salesinvoice Report By Customer (Summary)';
        $arr_data['report_file_name'] = 'SI_'.md5(time());
        return $arr_data;
    }


	public function option_detailed_customer_find()
	{
		if(!$this->check_permission($this->name.'_@_options_@_report_by_customer_detailed'))
			$this->error_auth();
		$arr_data['salesinvoices_status'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_status'));
		$arr_data['salesinvoices_type'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_type'));
		$this->selectModel('Tax');
		$arr_data['salesinvoices_tax'] = $this->Tax->tax_select_list();

		$this->set('arr_data',$arr_data);
	}
	public function detailed_customer_report($arr_invoices, $data, $arr_where, $minimum, $product_id) {
        $i = $sum = 0;
        $html = '';
        //Loc ra nhung quotation group theo company id
        $group_company = array();
        $this->selectModel('Company');
        $arr_company = $this->get_sum_sub_total($product_id,$minimum, $arr_where);
        $arr_invoices = iterator_to_array($arr_invoices);
        foreach($arr_company as $company_id => $value){
            foreach ($arr_invoices as $invoice_id => $invoice) {
                if(!isset($invoice['company_id']) || !is_object($invoice['company_id'])) continue;
                if($company_id != (string)$invoice['company_id']) continue;
                $arr_company[$company_id]['sale_invoices'][$invoice['code']] = array(
                                                                        'salesinvoice_code'    =>$invoice['code'],
                                                                        'salesinvoice_type'    =>$invoice['invoice_type'],
                                                                        'salesinvoice_date'    =>date('d M, Y',$invoice['invoice_date']->sec),
                                                                        'salesinvoice_heading'    =>(isset($invoice['heading']) ? $invoice['heading'] : ''),
                                                                        'salesinvoice_status'    =>$invoice['invoice_status'],
                                                                        'salesinvoice_our_rep'    =>$invoice['our_rep'],
                                                                        'sum_sub_total'     => ($invoice['sum_sub_total']<$value['minimum'] ? $value['minimum'] : $invoice['sum_sub_total'])
                                                                       );
                unset($arr_invoices[$invoice_id]);
            }
        }
        foreach ($arr_company as $key => $company) {
            $html .= '
            <table class="table_content">
               <tbody>
                  <tr class="tr_right_none" style="background: #911b12;color: white;height: 29px;line-height: 29px;font-weight: bold;">
                     <td width="35%">
                        Company
                     </td>
                     <td width="25%">
                        Our Rep
                     </td>
                     <td class="right_text" width="15%">
                        No. of SI
                     </td>
                     <td class="right_text" colspan="3">
                        Group total (ex. tax)
                     </td>
                  </tr>
                  <tr class="bg_2">
                     <td>' . $company['company_name'] . '</td>
                     <td>' . $company['our_rep'] . '</td>
                     <td class="right_text">' . $company['number_of_salesinvoices'] . '</td>
                     <td colspan="3" class="right_text">' . number_format($company['sum_sub_total'], 2) . '</td>
                  </tr>
               </tbody>
            </table>
            <br />
            <br />
            <div class="line" style="margin-bottom: 10px;"></div>';
            $html .= '<table class="table_content" >
                        <tbody>
                          <tr class="tr_right_none" style="background: #911b12;color: white;height: 29px;line-height: 29px;font-weight: bold;">
                             <td width="7%">
                                SI#
                             </td>
                             <td width="15%">
                                Type
                             </td>
                             <td width="15%">
                                Date
                             </td>
                             <td width="15%">
                                Status
                             </td>
                             <td width="15%">
                                Heading
                             </td>
                             <td width="15%">
                                Our Rep
                             </td>
                             <td class="right_text" colspan="3" width="18%">
                                Ex. Tax total
                             </td>
                          </tr>';
            $i = 0;
            $sum = 0;
            foreach ($company['sale_invoices'] as $key => $invoice) {
                $sum += $invoice['sum_sub_total'];
                $html .= '
                      <tr class="content_asset bg_' . ($i % 2 == 0 ? '1' : '2') . '">
                         <td>' . $invoice['salesinvoice_code'] . '</td>
                         <td>' . $invoice['salesinvoice_type'] . '</td>
                         <td>' . $invoice['salesinvoice_date'] . '</td>
                         <td>' . $invoice['salesinvoice_status'] . '</td>
                         <td class="left_text">' . $invoice['salesinvoice_heading'] . '</td>
                         <td class="left_text">' . $invoice['salesinvoice_our_rep'] . '</td>
                         <td colspan="3" class="right_text">' . number_format((float)$invoice['sum_sub_total'], 2) . '</td>
                      </tr>';
                $i++;
            }
            $html .= '
                            <tr class="bg_' . ($i % 2 == 0 ? '1' : '2') . '">
                             <td colspan="5" class="left_text bold_text right_none">' . $i . ' record(s) listed</td>
                             <td class="bold_text right_text right_none">Total</td>
                             <td colspan="3" class="bold_text right_text">' . number_format($sum, 2) . '</td>
                          </tr>
                        </tbody>
                    </table>
                    <br />
                    <br />';
        }
        //========================================
        //set header
        if($data['heading']!='')
            $arr_data['report_heading'] = $data['heading'];
        $arr_data['date_from_to'] = '';
        if(isset($data['date_from'])&&$data['date_from']!='')
            $arr_data['date_from_to'] .= '<span class="color_red bold_text">From</span> '.$data['date_from'].' ';
        if(isset($data['date_to'])&&$data['date_to']!='')
            $arr_data['date_from_to'] .= ' <span class="color_red bold_text">To</span> '.$data['date_to'];
        if(isset($data['date_equals'])&&$data['date_equals']!='')
            $arr_data['date_from_to'] .= $data['date_equals'];
        $arr_data['content'][]['html'] = $html;
        $arr_data['is_custom'] = true;
        $arr_data['image_logo'] = true;
        $arr_data['report_name'] = 'Sale Invoice Report By Customer (Detailed)';
        $arr_data['report_file_name'] = 'QT_'.md5(time());
        return $arr_data;
    }


	public function option_summary_highest_customer_find()
	{
		if(!$this->check_permission($this->name.'_@_options_@_report_by_customer_summary_highest_first'));
			$this->error_auth();
		$arr_data['salesinvoices_status'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_status'));
		$arr_data['salesinvoices_type'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_type'));
		$this->selectModel('Tax');
		$arr_data['salesinvoices_tax'] = $this->Tax->tax_select_list();

		$this->set('arr_data',$arr_data);
	}
	public function option_detailed_tax_customer_find()
	{
		if(!$this->check_permission($this->name.'_@_options_@_report_by_customer_detailed_tax_amounts'))
			$this->error_auth();
		$arr_data['salesinvoices_status'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_status'));
		$arr_data['salesinvoices_type'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_type'));
		$this->selectModel('Tax');
		$arr_data['salesinvoices_tax'] = $this->Tax->tax_select_list();

		$this->set('arr_data',$arr_data);
	}
	public function check_exist_customer()
	{
		$data = $_POST;
		parse_str($data['data'],$data);
		if(!empty($data))
		{
			$data = $this->Common->strip_search($data);
			$arr_where = array();
			$arr_where['deleted'] = false;
			if($data['status'])
				$arr_where['invoice_status'] = $data['status'];
			if($data['type'])
				$arr_where['invoice_type'] = $data['type'];
			if(isset($data['company'])&&$data['company']!='')
				$arr_where['company_name'] = new MongoRegex('/'.trim($data['company']).'/i');
			if(isset($data['contact'])&&$data['contact']!='')
				$arr_where['contact_name'] = new MongoRegex('/'.trim($data['contact']).'/i');
			if(isset($data['job_no'])&&$data['job_no']!='')
				$arr_where['job_number'] = new MongoRegex('/'.trim($data['job_no']).'/i');
			//tim chinh xac ngay
			if($data['date_equals']!=''){
				$date_equals = $data['date_equals'];
				$date_equals = new MongoDate(strtotime(@date('Y-m-d',strtotime($date_equals))));
				$date_equals_to = new MongoDate($date_equals->sec + DAY -1);
				$arr_where['invoice_date']['$gte'] = $date_equals;
				$arr_where['invoice_date']['$lt'] = $date_equals_to;
			}
			//ngay nam trong khoang
			else if($data['date_equals'] == ''){
				//neu chi nhap date from
				if($date_from = $data['date_from']){
					$date_from = new MongoDate(strtotime(@date('Y-m-d',strtotime(@$date_from))));
					$arr_where['invoice_date']['$gte'] = $date_from;
				}
				//neu chi nhap date to
				if($date_to = $data['date_to']){
					$date_to = new MongoDate(strtotime(@date('Y-m-d',strtotime(@$date_to))));
					$date_to = new MongoDate($date_to->sec + DAY -1);
					$arr_where['invoice_date']['$lte'] = $date_to;
				}
			}
			if(@$data['employee']!=''){
				$arr_where['$or'][]['our_rep'] = new MongoRegex('/'.trim($data['employee']).'/i');
				$arr_where['$or'][]['our_csr'] = new MongoRegex('/'.trim($data['employee']).'/i');
			}
			$this->selectModel('Salesinvoice');
			if(isset($data['highest'])&&$data['highest']==true){
				//lay het salesinvoice, voi where nhu tren va lay sum_sub_total giam dan
				$salesinvoice = $this->Salesinvoice->select_all(array(
					'arr_where'=>$arr_where,
					'arr_order'=>array(
									'sum_sub_total'=>-1
									)
				));
			}
			else{
				//lay het salesinvoice, voi where nhu tren va lay ten company giam dan
				$salesinvoice = $this->Salesinvoice->select_all(array(
					'arr_where'=>$arr_where,
					'arr_order'=>array(
									'company_name'=>1
									),
					'arr_field'=>array('_id','code','company_id','company_name','invoice_date','invoice_type','invoice_status','sum_sub_total','name','sum_tax_total','sum_tax')
				));
			}
			if($salesinvoice->count()==0){
				echo 'empty';
				die;
			}
			else
			{
				$url = '';
				if($data['report_type']=='summary')
					$url = $this->summary_customer_report_pdf($salesinvoice,$data);
				else if($data['report_type'] == 'detailed')
				{
					if(isset($data['tax'])&&$data['tax']==true)
						$url = $this->detailed_tax_customer_report_pdf($salesinvoice,$data);
					else
						$url = $this->detailed_customer_report_pdf($salesinvoice,$data);
				}
				else
					$url = $this->summary_customer_report_pdf($salesinvoice,$data);

				echo URL.$url;
				die;
			}
		}
	}
	public function summary_customer_report_pdf($salesinvoice,$data)
	{
		//--------------------------------------
		$sum = 0;
		$html_loop = '';
		$i = 0;
		$color = '';
		$arr_status = array();
		$arr_company = array();
		$this->selectModel('Company');
		foreach($salesinvoice as $value)
		{
			if($value['company_id'])
			{
				$company = $this->Company->select_one(array('_id'=> new MongoId($value['company_id'])));
				$arr_company[(string)$value['company_id']]['company_name'] = $value['company_name'];
				$arr_company[(string)$value['company_id']]['our_rep'] = @$company['our_rep'];
				if(!isset($arr_company[(string)$value['company_id']]['number_of_salesorder']))
					$arr_company[(string)$value['company_id']]['number_of_salesorder'] = 0;
				$arr_company[(string)$value['company_id']]['number_of_salesorder']++;
				if(!isset($arr_company[(string)$value['company_id']]['sum_sub_total']))
					$arr_company[(string)$value['company_id']]['sum_sub_total'] = 0;
				$arr_company[(string)$value['company_id']]['sum_sub_total'] += (@$value['sum_sub_total']!=''?$value['sum_sub_total']: 0);
			}
		}
		$html_loop = '
				<table cellpadding="3" cellspacing="0" class="maintb">
				  <tr>
					 <td width="30%" class="first top">
						Customer
					 </td>
					 <td width="20%" class="top">
						Our Rep
					 </td>
					 <td width="20%" class="top">
						No. of SI
					 </td>
					<td colspan="3" width="30%" class="end top">
					   Ex. Tax total
					 </td>
				  </tr>
			';
		foreach($arr_company as $value)
		{
			$color = '#fdfcfa';
			if($i%2==0)
				$color = '#eeeeee';
			$html_loop .= '
				<tr style="background-color:'.$color.';">
					 <td class="first content" align="left">'.@$value['company_name'].'</td>
					 <td class="content" align="left">'.@$value['our_rep'].'</td>
					 <td class="content">'.@$value['number_of_salesorder'].'</td>
					 <td colspan="3" class="content"  align="right" class="end">'.number_format($value['sum_sub_total'],2,'.',',').'</td>
				</tr>
			';
			$sum += (@$value['sum_sub_total']? $value['sum_sub_total']: 0);
			$i++;
		}
		$color = '#fdfcfa';
		if($i%2==0)
			$color = '#eeeeee';
		$html_loop .= '
					<tr style="background-color:'.$color.';">
						 <td align="left" class="first bottom">'.$i.' record(s) listed</td>
						 <td class="bottom">&nbsp;</td>
						 <td class="bottom">&nbsp;</td>
						 <td align="left" class="bottom"><span style="font-weight:bold; padding-left:20px">Total:</span></td>
						 <td colspan="2" align="right" class="content bottom">'.number_format($sum,2,'.',',').'</td>
					</tr>
				</table>
				';
		//========================================
		$pdf['current_time'] = date('h:i a m/d/Y');
		$pdf['title'] = '<span style="color:#b32017">S</span>ales <span style="color:#b32017">O</span>der <span style="color:#b32017">R</span>eport <span style="color:#b32017">B</span>y <span style="color:#b32017">C</span>ustomer<br /> (Summary)';
		$this->layout = 'pdf';
			//set header
		$pdf['logo_link'] = 'img/logo_anvy.jpg';
		$pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		$pdf['heading'] = $data['heading'];
		if(isset($data['date_equals'])&&$data['date_equals']!='')
		{
			$pdf['date_equals'] = $data['date_equals'];
		}
		else
		{
			if(isset($data['date_from'])&&$data['date_from']!='')
				$pdf['date_from']  = $data['date_from'];
			if(isset($data['date_to'])&&$data['date_to']!='')
				$pdf['date_to'] = $data['date_to'];
		}
		$pdf['html_loop'] = $html_loop;
		$pdf['filename'] = 'SO_'.md5($pdf['current_time']);
		//pr($html_loop);die;
		$this->report_pdf($pdf);
		return '/upload/'.$pdf['filename'].'.pdf';
	}
	public function detailed_customer_report_pdf($salesinvoice,$data)
	{

		$sum = 0;
		$html_loop = '';
		$i = 0;
		$color = '';
		$group_company = array();
		$this->selectModel('Company');
		foreach($salesinvoice as $value)
		{
			if($value['company_id'])
			{
				$company = $this->Company->select_one(array('_id'=> new MongoId($value['company_id'])));
				//Group chung
				$group_company[(string)$value['company_id']]['company_name'] = @$value['company_name'];
				$group_company[(string)$value['company_id']]['our_rep'] = @$company['our_rep'];
				if(!isset($group_company[(string)$value['company_id']]['number_of_salesinvoices']))
					$group_company[(string)$value['company_id']]['number_of_salesinvoices'] = 0;
				$group_company[(string)$value['company_id']]['number_of_salesinvoices'] ++;
				if(!isset($group_company[(string)$value['company_id']]['total']))
					$group_company[(string)$value['company_id']]['total'] = 0;
				$group_company[(string)$value['company_id']]['total'] += (isset($value['sum_sub_total'])&&$value['sum_sub_total'] != '' ? $value['sum_sub_total']: 0);
				//Tách theo SI code
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_code'] = $value['code'];
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_type'] = $value['invoice_type'];
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_date'] = $this->opm->format_date($value['invoice_date']);
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_heading'] = (isset($value['name']) ? $value['name'] : '');
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_status'] = $value['invoice_status'];
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['sum_sub_total'] = (@$value['sum_sub_total']!='' ? $value['sum_sub_total'] : 0);

			}
		}
		foreach($group_company as $key=>$company)
		{
			$html_loop .= '
			<table cellpadding="3" cellspacing="0" class="maintb">
			   <tbody>
				  <tr>
					 <td width="35%" class="first top">
						Company
					 </td>
					 <td width="25%" class="top">
						Contact
					 </td>
					 <td width="15%" class="top">
						No. of SI
					 </td>
					 <td colspan="3" width="25%" class="top">
						Group total (ex. tax)
					 </td>
				  </tr>
				  <tr style="background-color:#eeeeee;">
					 <td class="first content" align="left">'.@$company['company_name'].'</td>
					 <td class="content">'.$company['our_rep'].'</td>
					 <td class="content">'.$company['number_of_salesinvoices'].'</td>
					 <td colspan="3" class="content" align="right">'.number_format($company['total'],2,'.',',').'</td>
				  </tr>
			   </tbody>
			</table>
			<div class="option"></div><br />';
			$html_loop .= '<table cellpadding="3" cellspacing="0" class="maintb">
						<tbody>
						  <tr>
							 <td width="10%" class="first top">
								SI#
							 </td>
							 <td width="15%" class="top">
								Type
							 </td>
							 <td width="15%" class="top">
								Date
							 </td>
							 <td width="15%" class="top">
								Status
							 </td>
							 <td width="25%" class="top">
								Heading
							 </td>
							 <td colspan="3" width="20%" class="end top">
								Ex. Tax total
							 </td>
						  </tr>';

			$i = 0;
			if(is_array($company))
			{
				foreach($company['salesinvoice'] as $key=>$value)
				{
					$color = '#fdfcfa';
					if($i%2==0)
						$color = '#eeeeee';
					$html_loop .= '
						  <tr style="background-color:'.$color.';">
							 <td class="first content">'.$value['salesinvoice_code'].'</td>
							 <td class="content">'.$value['salesinvoice_type'].'</td>
							 <td class="content">'.$value['salesinvoice_date'].'</td>
							 <td class="content">'.$value['salesinvoice_status'].'</td>
							 <td class="content" align="left">'.@$value['salesinvoice_heading'].'</td>
							 <td colspan="3" class="content"  align="right" class="end">'.number_format($value['sum_sub_total'],2,'.',',').'</td>
						  </tr>';
					$i++;
				}
			}
			$color = '#fdfcfa';
			if($i%2==0)
				$color = '#eeeeee';
			$html_loop .= '
							<tr style="background-color:'.$color.'">
							 <td colspan="2" align="left" class="first bottom">'.$i.' record(s) listed</td>
							 <td class="bottom">&nbsp;</td>
							 <td class="bottom">&nbsp;</td>
							 <td class="bottom">&nbsp;</td>
							 <td  align="left" class="bottom"><span style="font-weight:bold; padding-left:20px">Total:</span></td>
							 <td colspan="2" align="right" class="content bottom">'.number_format($company['total'],2,'.',',').'</td>
						  </tr>
						</tbody>
					</table>
					<br />
					<div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
					<br />';

		}
		//========================================
		$pdf['current_time'] = date('h:i a m/d/Y');
		$pdf['title'] = '<span style="color:#b32017">S</span>ales <span style="color:#b32017">I</span>voice <span style="color:#b32017">R</span>eport <span style="color:#b32017">B</span>y <span style="color:#b32017">C</span>ustomer<br /> (Detail)';
		$this->layout = 'pdf';
			//set header
		$pdf['logo_link'] = 'img/logo_anvy.jpg';
		$pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		$pdf['heading'] = $data['heading'];
		if(isset($data['date_equals'])&&$data['date_equals']!='')
		{
			$pdf['date_equals'] = $data['date_equals'];
		}
		else
		{
			if(isset($data['date_from'])&&$data['date_from']!='')
				$pdf['date_from']  = $data['date_from'];
			if(isset($data['date_to'])&&$data['date_to']!='')
				$pdf['date_to'] = $data['date_to'];
		}
		$pdf['html_loop'] = $html_loop;
		$pdf['filename'] = 'SI_'.md5($pdf['current_time']);

		$this->report_pdf($pdf);
		return '/upload/'.$pdf['filename'].'.pdf';

	}
	public function detailed_tax_customer_report_pdf($salesinvoice,$data)
	{

		$sum = 0;
		$html_loop = '';
		$i = 0;
		$color = '';
		$group_company = array();
		$this->selectModel('Company');
		foreach($salesinvoice as $value)
		{
			if($value['company_id'])
			{
				$company = $this->Company->select_one(array('_id'=> new MongoId($value['company_id'])));
				//Group chung
				$group_company[(string)$value['company_id']]['company_name'] = @$value['company_name'];
				$group_company[(string)$value['company_id']]['our_rep'] = @$company['our_rep'];
				if(!isset($group_company[(string)$value['company_id']]['number_of_salesinvoices']))
					$group_company[(string)$value['company_id']]['number_of_salesinvoices'] = 0;
				$group_company[(string)$value['company_id']]['number_of_salesinvoices'] ++;
				if(!isset($group_company[(string)$value['company_id']]['total']))
					$group_company[(string)$value['company_id']]['total'] = 0;
				$group_company[(string)$value['company_id']]['total'] += (isset($value['sum_sub_total'])&&$value['sum_sub_total'] != '' ? $value['sum_sub_total']: 0);
				if(!isset($group_company[(string)$value['company_id']]['total_with_tax']))
					$group_company[(string)$value['company_id']]['total_with_tax'] = 0;
				$group_company[(string)$value['company_id']]['total_with_tax'] += ($value['sum_amount'] != '' ? $value['sum_amount']: 0);
				if(!isset($group_company[(string)$value['company_id']]['total_tax']))
					$group_company[(string)$value['company_id']]['total_tax'] = 0;
				$group_company[(string)$value['company_id']]['total_tax'] += (isset($value['sum_tax'])&&$value['sum_tax'] != '' ? $value['sum_tax']: 0);
				//Tách theo SI code
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_code'] = $value['code'];
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_type'] = $value['invoice_type'];
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_date'] = $this->opm->format_date($value['invoice_date']);
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_heading'] = (isset($value['name']) ? $value['name'] : '');
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['salesinvoice_status'] = $value['invoice_status'];
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['sum_tax'] = (isset($value['sum_tax'])&&$value['sum_tax']!= '' ? $value['sum_tax'] : 0);
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['sum_sub_total'] = (isset($value['sum_sub_total'])&&$value['sum_sub_total']!='' ? $value['sum_sub_total'] : 0);
				$group_company[(string)$value['company_id']]['salesinvoice'][$value['code']]['sum_amount'] = ($value['sum_amount']!='' ? $value['sum_amount'] : 0);

			}
		}
		foreach($group_company as $key=>$company)
		{
			$html_loop .= '
			<table cellpadding="3" cellspacing="0" class="maintb">
			   <tbody>
				  <tr>
					 <td width="25%" class="first top">
						Company
					 </td>
					 <td width="20%" class="top">
						Contact
					 </td>
					 <td width="15%" class="top">
						No. of SI
					 </td>
					 <td width="20%" class="top">
						Group total (ex. tax)
					 </td>
					 <td colspan="3" width="20%" class="top">
						Group total (inc. tax)
					 </td>
				  </tr>
				  <tr style="background-color:#eeeeee;">
					 <td class="first content" align="left">'.@$company['company_name'].'</td>
					 <td class="content">'.$company['our_rep'].'</td>
					 <td class="content">'.$company['number_of_salesinvoices'].'</td>
					 <td class="content" align="right">'.number_format($company['total'],2,'.',',').'</td>
					 <td colspan="3" class="content" align="right">'.number_format($company['total_with_tax'],2,'.',',').'</td>
				  </tr>
			   </tbody>
			</table>
			<div class="option"></div><br />';
			$html_loop .= '<table cellpadding="3" cellspacing="0" class="maintb">
						<tbody>
						  <tr>
							 <td width="10%" class="first top">
								SI#
							 </td>
							 <td width="15%" class="top">
								Type
							 </td>
							 <td width="15%" class="top">
								Date
							 </td>
							 <td width="10%" class="top">
								Status
							 </td>
							 <td width="15%" class="top">
								Ex. Tax total
							 </td>
							 <td width="15%" class="top">
								Tax
							 </td>
							 <td width="20%" class="end top">
								Total inc. Tax
							 </td>
						  </tr>';

			$i = 0;
			if(is_array($company))
			{
				foreach($company['salesinvoice'] as $key=>$value)
				{
					$color = '#fdfcfa';
					if($i%2==0)
						$color = '#eeeeee';
					$html_loop .= '
						  <tr style="background-color:'.$color.';">
							 <td class="first content">'.$value['salesinvoice_code'].'</td>
							 <td class="content">'.$value['salesinvoice_type'].'</td>
							 <td class="content">'.$value['salesinvoice_date'].'</td>
							 <td class="content">'.$value['salesinvoice_status'].'</td>
							 <td class="content"  align="right">'.number_format($value['sum_sub_total'],2,'.',',').'</td>
							 <td class="content"  align="right">'.number_format($value['sum_tax'],2,'.',',').'</td>
							 <td class="content"  align="right" class="end">'.number_format($value['sum_amount'],2,'.',',').'</td>
						  </tr>';
					$i++;
				}
			}
			$color = '#fdfcfa';
			if($i%2==0)
				$color = '#eeeeee';
			$html_loop .= '
							<tr style="background-color:'.$color.'">
							 <td colspan="2" align="left" class="first bottom">'.$i.' record(s) listed</td>
							 <td class="bottom">&nbsp;</td>
							 <td align="left" class="bottom"><span style="font-weight:bold; padding-left:20px">Total:</span></td>
							 <td align="right" class="content bottom">'.number_format($company['total'],2,'.',',').'</td>
							 <td align="right" class="content bottom">'.number_format($company['total_tax'],2,'.',',').'</td>
							 <td align="right" class="content bottom">'.number_format($company['total_with_tax'],2,'.',',').'</td>
						  </tr>
						</tbody>
					</table>
					<br />
					<div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
					<br />';

		}
		//========================================
		$pdf['current_time'] = date('h:i a m/d/Y');
		$pdf['title'] = '<span style="color:#b32017">S</span>ales <span style="color:#b32017">I</span>voice <span style="color:#b32017">R</span>eport <span style="color:#b32017">B</span>y <span style="color:#b32017">C</span>ustomer<br /> (Detail inc. tax)';
		$this->layout = 'pdf';
			//set header
		$pdf['logo_link'] = 'img/logo_anvy.jpg';
		$pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		$pdf['heading'] = $data['heading'];
		if(isset($data['date_equals'])&&$data['date_equals']!='')
		{
			$pdf['date_equals'] = $data['date_equals'];
		}
		else
		{
			if(isset($data['date_from'])&&$data['date_from']!='')
				$pdf['date_from']  = $data['date_from'];
			if(isset($data['date_to'])&&$data['date_to']!='')
				$pdf['date_to'] = $data['date_to'];
		}
		$pdf['html_loop'] = $html_loop;
		$pdf['filename'] = 'SI_'.md5($pdf['current_time']);

		$this->report_pdf($pdf);
		return '/upload/'.$pdf['filename'].'.pdf';
	}
	public function option_summary_product_find(){
		if(!$this->check_permission($this->name.'_@_options_@_report_by_product_summary'))
			$this->error_auth();
		$arr_data['salesinvoices_status'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_status'));
		$arr_data['salesinvoices_type'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_type'));
		$this->selectModel('Tax');
		$arr_data['salesinvoices_tax'] = $this->Tax->tax_select_list();
		$arr_data['product_category'] = $this->Setting->select_option_vl(array('setting_value'=>'product_category'));
		$this->set('arr_data',$arr_data);
	}
	public function option_detailed_product_find(){
		if(!$this->check_permission($this->name.'_@_options_@_report_by_product_detailed'))
			$this->error_auth();
		$arr_data['salesinvoices_status'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_status'));
		$arr_data['salesinvoices_type'] = $this->Setting->select_option_vl(array('setting_value'=>'salesinvoices_type'));
		$this->selectModel('Tax');
		$arr_data['salesinvoices_tax'] = $this->Tax->tax_select_list();
		$arr_data['product_category'] = $this->Setting->select_option_vl(array('setting_value'=>'product_category'));
		$this->set('arr_data',$arr_data);
	}
	public function get_cate_product($value) {
		$cate = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
		if(isset($cate[$value]))
			echo $cate[$value];
		else
			echo '';
		die();
	}
	public function product_report($type = ''){
        $arr_data = array();
        if(isset($_GET['print_pdf'])){
            $arr_data = Cache::read('salesinvoices_product_report_'.$type);
        } else {
            if(isset($_POST)){
                $data['product_category'] = $this->Setting->select_option_vl(array('setting_value'=>'product_category'));
                $arr_post = $_POST;
                $arr_post = $this->Common->strip_search($arr_post);
                $arr_where = array();
                $arr_where['products']['$ne'] = '';
                if(isset($arr_post['status']) && $arr_post['status'] != '')
                    $arr_where['invoice_status'] = $arr_post['status'];
                //Check loại trừ cancel thì bỏ các status bên dưới
                if(isset($arr_post['is_not_cancel'])&&$arr_post['is_not_cancel']==1){
                    $arr_where['invoice_status'] = array('$nin'=>array('Cancelled'));
                    //Tuy nhiên nếu ở ngoài combobox nếu có chọn, thì ưu tiên nó, set status lại
                    if(isset($arr_post['status'])&&$arr_post['status']!='')
                        $arr_where['invoice_status'] = $arr_post['status'];
                }
                if(isset($arr_post['type']) && $arr_post['type']!= '')
                    $arr_where['invoice_type'] = $arr_post['type'];
                if(isset($arr_post['company']) && $arr_post['company']!='')
                    $arr_where['company_name'] = new MongoRegex('/'.trim($arr_post['company']).'/i');
                if(isset($arr_post['contact']) &&$arr_post['contact']!='')
                    $arr_where['contact_name'] = new MongoRegex('/'.trim($arr_post['contact']).'/i');
                if(isset($arr_post['job_no']) && $arr_post['job_no']!='')
                    $arr_where['job_number'] = trim($arr_post['job_no']);
                if(isset($arr_post['employee']) && trim($arr_post['employee'])!=''){
                    $arr_where['$or'][]['our_rep'] = new MongoRegex('/'.trim($arr_post['employee']).'/i');
                    $arr_where['$or'][]['our_csr'] = new MongoRegex('/'.trim($arr_post['employee']).'/i');
                }
                //Tìm chính xác ngày
                //Vì để = chỉ tìm đc 01/01/1969 00:00:00 nên phải cộng cho 23:59:59 rồi tìm trong khoảng đó
                if(isset($arr_post['date_equals'])&&$arr_post['date_equals']!=''){
                    $date_equals = new MongoDate(strtotime(date('Y-m-d',strtotime($arr_post['date_equals']))));
                    $date_equals_to = new MongoDate($date_equals->sec + DAY);
                    $arr_where['invoice_date']['$gte'] = $date_equals;
                    $arr_where['invoice_date']['$lt'] = $date_equals_to;
                } else{  //Ngày nằm trong khoảng
                    //neu chi nhap date from
                    if(isset($arr_post['date_from']) && $arr_post['date_from'] != ''){
                        $date_from = new MongoDate(strtotime(date('Y-m-d',strtotime($arr_post['date_from']))));
                        $arr_where['invoice_date']['$gte'] = $date_from;
                    }
                    //neu chi nhap date to
                    if(isset($arr_post['date_to']) && $arr_post['date_to'] != ''){
                        $date_to = new MongoDate(strtotime(date('Y-m-d',strtotime($arr_post['date_to']))));
                        $date_to = new MongoDate($date_to->sec + DAY -1);
                        $arr_where['invoice_date']['$lte'] = $date_to;
                    }
                }
                //Kiểm tra nếu có thông tin liên quan đến product tồn tại
                $pro_where = array();
                if(isset($arr_post['product'])&&$arr_post['product']!='')
                    $pro_where['code'] = trim($arr_post['product']);
                if(isset($arr_post['name'])&&$arr_post['name']!='')
                    $pro_where['name'] = new MongoRegex('/' . trim($arr_post['name']) . '/i');
                if(isset($arr_post['category_id'])&&$arr_post['category_id']!='')
                    $pro_where['category'] = new MongoRegex('/'.$arr_post['category_id'].'/i');
                $pro_list = array();
                $arr_products_where = array();
                $arr_products_where['products.deleted'] = $arr_where['deleted'] = false;
                if(isset($arr_post['sell_price_from'])&&$arr_post['sell_price_from']!=''){
                    $arr_where['products']['$elemMatch']['sell_price']['$gte'] = (float)$arr_post['sell_price_from'];
                    $arr_products_where['products.unit_price']['$gte'] = (float)$arr_post['sell_price_from'];
                }
                if(isset($arr_post['sell_price_to'])&&$arr_post['sell_price_to']!=''){
                    $arr_where['products']['$elemMatch']['sell_price']['$lte'] = (float)$arr_post['sell_price_to'];
                    $arr_products_where['products.unit_price']['$lte'] = (float)$arr_post['sell_price_to'];
                }
                if(!empty($pro_where)){
                    //Lấy ra _id của Product phù hợp với điều kiện trên
                    $this->selectModel('Product');
                    $pro_list = $this->Product->select_all(array(
                                            'arr_where'=>$pro_where,
                                            'arr_field'=>array('_id')
                        ));
                    foreach($pro_list as $p_id){
                       $arr_where['products']['$elemMatch']['products_id']['$in'][] = new MongoId($p_id['_id']);
                       $arr_products_where['products.products_id']['$in'][] = new MongoId($p_id['_id']);
                    }
                }
                $arr_where['products']['$elemMatch']['deleted'] = false;
                $arr_salesinvoices = $this->opm->collection->aggregate(
                        array(
                            '$match'=>$arr_where,
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>$arr_products_where
                        ),
                        array(
                            '$project'=>array('invoice_status'=>'$invoice_status','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','invoice_date'=>'$invoice_date','sum_sub_total'=>'$sum_sub_total','products'=>'$products')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','invoice_status'=>'$invoice_status','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','invoice_date'=>'$invoice_date','sum_sub_total'=>'$sum_sub_total'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
                if(empty($arr_salesinvoices['result'])) {
                    echo 'empty';
                    die;
                } else {
                    $arr_salesinvoices = $arr_salesinvoices['result'];
                    if ($arr_post['report_type'] == 'summary'){
                        $arr_data = $this->summary_product_report($arr_salesinvoices,$arr_post);
                        Cache::write('salesinvoices_product_report_'.$type, $arr_data);
                    }
                    else if ($arr_post['report_type'] == 'detailed'){
                        $arr_data = $this->detailed_product_report($arr_salesinvoices,$arr_post);
                        Cache::write('salesinvoices_product_report_'.$type, $arr_data);
                    }
                    else{
                        $arr_data = $this->summary_product_report($arr_salesinvoices,$arr_post);
                        Cache::write('salesinvoices_product_report_'.$type, $arr_data);
                    }

                }

            }
        }
        if($this->request->is('ajax'))
            die;
        else
            $this->render_pdf($arr_data);
    }
	public function summary_product_report($arr_salesinvoices,$data){
        $html = '';
        $i = $sum = 0;
        $this->selectModel('Product');
        $category = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
        $arr_data = array();
        foreach($arr_salesinvoices as $salesinvoice){
            foreach($salesinvoice['products'] as $product){
                $product['code'] = (isset($product['code']) ? $product['code'] : 'empty');
                $arr_data[$product['code']]['products_name'] = $product['products_name'];
                $arr_data[$product['code']]['code'] = $product['code'];
                $arr_data[$product['code']]['products_id'] = $product['products_id'];
                if(!isset($arr_data[$product['code']]['quantity']))
                    $arr_data[$product['code']]['quantity'] = 0;
                $arr_data[$product['code']]['quantity'] += $product['quantity'];
                if(!isset($arr_data[$product['code']]['sum_sub_total']))
                    $arr_data[$product['code']]['sum_sub_total'] = 0;
                $arr_data[$product['code']]['sum_sub_total'] += $salesinvoice['_id']['sum_sub_total'];
            }
        }
        foreach ($arr_data as $value) {
            if(is_object($value['products_id']))
                $product = $this->Product->select_one(array('_id'=>new MongoId($value['products_id'])),array('category'));
            if (!isset($product['category']))
            	$product['category'] = '';
            $html .= '
                <tr style="background-color:' . ( $i%2==0 ? '#eeeeee' : '#fdfcfa'). ';">
                     <td>' . $value['code'] . '</td>
                     <td>' . $value['products_name'] . '</td>
                     <td>' . (isset($category[$product['category']]) ? $category[$product['category']] : '') . '</td>
                     <td class="right_text">' . $value['quantity'] . '</td>
                     <td colspan="3" class="right_text">' . number_format($value['sum_sub_total'], 2) . '</td>
                </tr>
            ';
            $sum += ($value['sum_sub_total'] ? $value['sum_sub_total'] : 0);
            $i++;
        }
        $html .= '
                    <tr style="background-color:' . ( $i%2==0 ? '#eeeeee' : '#fdfcfa') . ';">
                         <td colspan="3" class="bold_text right_none">' . $i . ' record(s) listed</td>
                         <td class="bold_text right_none right_text" >Total</td>
                         <td class="bold_text right_text">' . number_format($sum, 2) . '</td>
                    </tr>
                </table>
                ';
        //========================================
        //set header
        if($data['heading']!='')
            $arr_pdf['report_heading'] = $data['heading'];
        $arr_pdf['date_from_to'] = '';
        if(isset($data['date_from'])&&$data['date_from']!='')
            $arr_pdf['date_from_to'] .= '<span class="color_red bold_text">From</span> '.$data['date_from'].' ';
        if(isset($data['date_to'])&&$data['date_to']!='')
            $arr_pdf['date_from_to'] .= ' <span class="color_red bold_text">To</span> '.$data['date_to'];
        if(isset($data['date_equals'])&&$data['date_equals']!='')
            $arr_pdf['date_from_to'] .= $data['date_equals'];
        $arr_pdf['title'] = array('P. Code'=>'text-align: left; width: 7%;','Product Name'=>'text-align: left','Category'=>'text-align: left','Qty'=>'text-align: right;','Ex. Tax total'=>'text-align: right');
        $arr_pdf['content'] = $html;
        $arr_pdf['report_name'] = 'SI Report By Product (Summary)';
        $arr_pdf['report_file_name'] = 'IV_'.md5(time());
        return $arr_pdf;
    }
	public function detailed_product_report($arr_salesinvoices,$data){
        $i = $sum = 0;
        $html = '';
        $this->selectModel('Product');
        $category = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
        $total_num_of_salesinvoices = $total_sum_sub_total = 0;
        $arr_data = $arr_pdf = array();
        foreach($arr_salesinvoices as $salesinvoice){
            foreach($salesinvoice['products'] as $product){
                $product['code'] = (isset($product['code']) ? $product['code'] : '(empty)');
                $arr_data[$product['code']]['products_name'] = $product['products_name'];
                $arr_data[$product['code']]['code'] = $product['code'];
                $arr_data[$product['code']]['products_id'] = $product['products_id'];
                if(!isset($arr_data[$product['code']]['quantity']))
                    $arr_data[$product['code']]['quantity'] = 0;
                $arr_data[$product['code']]['quantity'] += $product['quantity'];
                if(!isset($arr_data[$product['code']]['sum_sub_total']))
                    $arr_data[$product['code']]['sum_sub_total'] = 0;
                if(!isset( $arr_data[$product['code']]['salesinvoices'][(string)$salesinvoice['_id']['_id']])){
                    $arr_data[$product['code']]['sum_sub_total'] += $salesinvoice['_id']['sum_sub_total'];
                    $arr_data[$product['code']]['salesinvoices'][(string)$salesinvoice['_id']['_id']] = array_merge($salesinvoice['_id'], array('unit_price'=>$product['unit_price'],'quantity'=>$product['quantity']));
                }
                else{
                    $arr_data[$product['code']]['salesinvoices'][(string)$salesinvoice['_id']['_id']]['quantity'] += $product['quantity'];

                }
            }
        }
        foreach ($arr_data as $value) {
            $total_num_of_salesinvoices += count($value['salesinvoices']);
            if(is_object($value['products_id']))
                $product = $this->Product->select_one(array('_id'=>new MongoId($value['products_id'])),array('category'));
            if (!isset($product['category']))
            	$product['category'] = '';
            $html .= '
            <table class="table_content">
               <tbody>
                  <tr class="tr_right_none" style="background: #911b12;color: white;height: 29px;line-height: 29px;font-weight: bold;">
                     <td width="10%">
                        P. Code
                     </td>
                     <td>
                        Product Name
                     </td>
                     <td width="15%">
                        Category
                     </td>
                     <td class="right_text" width="15%">
                        No. of SO
                     </td>
                     <td class="right_text" colspan="3" width="20%">
                        Group total (ex. tax)
                     </td>
                  </tr>
                  <tr class="bg_2">
                     <td>' . $value['code'] . '</td>
                     <td>' . $value['products_name'] . '</td>
                     <td>' . (isset($category[$product['category']]) ? $category[$product['category']] : '') . '</td>
                     <td class="right_text">' . count($value['salesinvoices']) . '</td>
                     <td colspan="3" class="right_text">' . number_format($value['sum_sub_total'], 2) . '</td>
                  </tr>
               </tbody>
            </table>';
            $html .= '<table class="table_content" >
                        <tbody>
                          <tr class="tr_right_none" style="background: #979797;color: white;height: 29px;line-height: 29px;font-weight: bold;">
                             <td width="10%">
                                SO#
                             </td>
                             <td width="30%">
                                Company
                             </td>
                             <td width="15%" class="center_text">
                                Date
                             </td>
                             <td width="15%" class="right_text">
                                Unit Price
                             </td>
                             <td width="15%" class="right_text">
                                Quantity
                             </td>
                             <td class="right_text" colspan="3" width="18%">
                                Ex. Tax total
                             </td>
                          </tr>';
            $i = 0;
            $sum = 0;
            foreach ($value['salesinvoices'] as $salesinvoice) {
                $sum += $salesinvoice['sum_sub_total'];
                $html .= '
                      <tr class="content_asset bg_' . ($i % 2 == 0 ? '1' : '2') . '">
                         <td>' . $salesinvoice['code'] . '</td>
                         <td>' . $salesinvoice['company_name'] . '</td>
                         <td class="center_text">' . date('d M, Y',$salesinvoice['invoice_date']->sec) . '</td>
                         <td class="right_text">' . number_format((float)$salesinvoice['unit_price'],2) . '</td>
                         <td class="right_text">' . $salesinvoice['quantity'] . '</td>
                         <td colspan="3" class="right_text">' . number_format((float)$salesinvoice['sum_sub_total'], 2) . '</td>
                      </tr>';
                $i++;
            }
            $html .= '
                            <tr class="bg_' . ($i % 2 == 0 ? '1' : '2') . '">
                             <td colspan="5" class="left_text bold_text right_none">' . $i . ' record(s) listed</td>
                             <td class="bold_text right_text right_none">Total</td>
                             <td colspan="3" class="bold_text right_text">' . number_format($sum, 2) . '</td>
                          </tr>
                        </tbody>
                    </table>
                    <br />
                    <br />';
            $total_sum_sub_total += $sum;
        }
        $html .= '
                <div class="line" style="margin-bottom: 5px;"></div>
                <table class="table_content">
                    <tr style="background-color: #333; color: white">
                        <td class="bold_text right_none" width="70%">'.$total_num_of_salesinvoices.' record(s) listed</td>
                        <td class="right_text bold_text right_none" >Totals</td>
                        <td class="right_text bold_text" width="15%">'.number_format($total_sum_sub_total,2).'</td>
                    </tr>
                </table>';

        //========================================
        //set header
        if($data['heading']!='')
            $arr_pdf['report_heading'] = $data['heading'];
        $arr_pdf['date_from_to'] = '';
        if(isset($data['date_from'])&&$data['date_from']!='')
            $arr_pdf['date_from_to'] .= '<span class="color_red bold_text">From</span> '.$data['date_from'].' ';
        if(isset($data['date_to'])&&$data['date_to']!='')
            $arr_pdf['date_from_to'] .= ' <span class="color_red bold_text">To</span> '.$data['date_to'];
        if(isset($data['date_equals'])&&$data['date_equals']!='')
            $arr_pdf['date_from_to'] .= $data['date_equals'];
        $arr_pdf['content'][]['html'] = $html;
        $arr_pdf['is_custom'] = true;
        $arr_pdf['image_logo'] = true;
        $arr_pdf['report_name'] = 'SO Report By Product (Detailed)';
        $arr_pdf['report_file_name'] = 'SO_'.md5(time());
        return $arr_pdf;
    }
	public function check_condition_SI()
	{
		$id = $this->get_id();
		$this->selectModel('Salesinvoice');
		$salesinvoice = $this->Salesinvoice->select_one(array('_id' => new MongoId($id)));
		if($salesinvoice!=''){
			if($salesinvoice['company_id']==''
					&&$salesinvoice['contact_id']=='')
				return array('err2');
			else if(empty($salesinvoice['products']))
				return array('err1');
			else if(isset($salesinvoice['shipping_id'])&&$salesinvoice['shipping_id']!='')
				return array('err3');
			else if($salesinvoice['invoice_status']=='Cancelled')
				return array('err4');
			return $salesinvoice;
		}
		return false;

	}
	public function create_shipping_from_invoice(){
		if(!$this->check_permission('shippings_@_entry_@_add')){
			echo 'You do not have permission on this action.';
			die;
		}
		$ids=$this->get_id();
		$total_invoice=0;
		$sum_amount_all=0;
		$arr_salesorder=array();
		$this->selectModel('Salesinvoice');
		if($ids!=''){
			$arr_salesinvoice = $this->opm->select_one(array('_id'=>new MongoId($ids)));

			if(is_object($arr_salesinvoice['company_id'])){
				$all_sales_invoice = $this->Salesinvoice->select_all(array(
					'arr_where'=>array('company_id'=>new MongoId($arr_salesinvoice['company_id']))
				));
				if(is_object($all_sales_invoice)){
					foreach($all_sales_invoice as $key=>$value){
						if(isset($value['sum_amount'])&&!$value['deleted'])
							$sum_amount_all+=(float)$value['sum_amount'];
					}
				}
			}


			$this->selectModel('Shipping');
			$arr_shipping=$this->Shipping->select_one(array('salesinvoice_id'=>new MongoId($ids)));
			if(is_array($arr_shipping)) {
				echo '/shippings/entry/'. $arr_shipping['_id'];
				die;
			}
		}





		if(!isset($arr_salesinvoice['company_id'])||!is_object($arr_salesinvoice['company_id'])){
			echo 'no_company';die;
		}

		$this->selectModel('Salesorder');
		$this->selectModel('Shipping');
		if(isset($arr_salesinvoice['salesorder_id'])&&is_object($arr_salesinvoice['salesorder_id'])){
			$arr_salesorder=$this->Salesorder->select_one(array('_id'=>new MongoId($arr_salesinvoice['salesorder_id'])));
		}


		$v_have_product=0;
		if(is_array($arr_salesinvoice)){

			if(is_array($arr_salesinvoice['products'])){
				foreach($arr_salesinvoice['products'] as $key1=>$value1){
					if(isset($value1['invoiced'])&&!$value1['deleted'])
						$v_have_product+=(int)$value1['invoiced'];
				}
			}
			if($v_have_product==0)
			{
				echo 'no_product';die;
			}

		}

		$this->selectModel('Company');
		$arr_company=array();
		if(is_object($arr_salesinvoice['company_id']))
			$arr_company=$this->Company->select_one(array('_id'=>new MongoId($arr_salesinvoice['company_id'])));

		if(isset($arr_company['account'])&&is_array($arr_company['account']))
		{


			if(is_object($arr_salesinvoice['salesorder_id'])){
				$query_salesinvoice = $this->opm->select_all(array(
					'arr_where' => array('salesorder_id' => new MongoId($arr_salesinvoice['salesorder_id']))
				));

				if(is_object($query_salesinvoice)){
					foreach($query_salesinvoice as $key=>$value){
						$total_invoice += isset($value['sum_amount'])?$value['sum_amount']:0;
					}
				}

			}


			if(isset($arr_company['account']['credit_limit'])&&isset($arr_salesinvoice['sum_amount'])&&$arr_company['account']['credit_limit']!=0)
			{
				if($arr_salesinvoice['sum_amount']>$arr_company['account']['credit_limit']){
					echo 'over';die;
				}

			}

			if(isset($arr_company['account']['credit_limit'])&&$arr_company['account']['credit_limit']!=0)
			{
				if(isset($arr_salesinvoice['salesorder_id'])&&is_object($arr_salesinvoice['salesorder_id'])){
					if($total_invoice>$arr_company['account']['credit_limit']){
						echo 'over1';die;
					}

				}

				if($sum_amount_all>$arr_company['account']['credit_limit']){
					echo 'over2';die;
				}

			}




		}



		$arr_save=array();

		if(isset($arr_salesorder['company_id']))
			$arr_save = $this->arr_associated_data('company_name',$arr_salesorder['company_name'], $arr_salesorder['company_id']);

		$arr_save['shipping_type']='Out';
		$arr_save['shipping_status']='Completed';
		$arr_save['shipping_date']=new MongoDate(time());

		$arr_save['salesorder_id']=isset($arr_salesorder['_id'])?$arr_salesorder['_id']:'';
		$arr_save['salesorder_number']=isset($arr_salesorder['code'])?$arr_salesorder['code']:'';
		$arr_save['salesorder_name']=isset($arr_salesorder['name'])?$arr_salesorder['name']:'';

		$arr_save['salesinvoice_id']=is_object($arr_salesinvoice['_id'])?$arr_salesinvoice['_id']:'';
		$arr_save['salesinvoice_number']=isset($arr_salesinvoice['code'])?$arr_salesinvoice['code']:'';
		$arr_save['salesinvoice_name']=isset($arr_salesinvoice['name'])?$arr_salesinvoice['name']:'';



		$arr_save['products']=isset($arr_salesinvoice['products'])?$arr_salesinvoice['products']:'';

		if(is_array($arr_save['products'])){
			foreach($arr_save['products'] as $key=>$value)
			{

				if(!$arr_save['products'][$key]['deleted']){

					$arr_save['products'][$key]['prev_shipped']=isset($arr_salesorder['products'][$key]['shipped'])?$arr_salesorder['products'][$key]['shipped']:0;

					if(isset($arr_salesorder['products'][$key]['shipped']))
						$arr_salesorder['products'][$key]['shipped'] += isset($arr_save['products'][$key]['invoiced'])?$arr_save['products'][$key]['invoiced']:0;
					$arr_save['products'][$key]['shipped']= isset($arr_save['products'][$key]['invoiced'])?$arr_save['products'][$key]['invoiced']:0;


					$v_quantity= isset($arr_salesorder['products'][$key]['quantity'])?$arr_salesorder['products'][$key]['quantity']:0;

					if(isset($arr_salesorder['products'][$key]['balance_shipped'])&&isset($arr_salesorder['products'][$key]['shipped']))
						$arr_salesorder['products'][$key]['balance_shipped']=$v_quantity-$arr_salesorder['products'][$key]['shipped'];

					if(isset($arr_salesorder['products'][$key]['shipped']))
						$arr_save['products'][$key]['balance_shipped']=$v_quantity-$arr_salesorder['products'][$key]['shipped'];



				}

			}

		}
		if(is_array($arr_salesorder)){
			if(is_array($arr_salesorder['products'])){
				foreach($arr_salesorder['products'] as $key1=>$value1){
					if(isset($value1['balance_shipped'])&&!$value1['deleted']&&$value1['balance_shipped']<0)
					{
						echo 'end_balance';die;
					}
				}
			}
		}
//		die;

		$arr_save['code'] =$this->Shipping->get_auto_code('code');

		if ($this->Shipping->save($arr_save)) {

			if(isset($arr_salesinvoice['salesorder_id'])&&is_object($arr_salesinvoice['salesorder_id'])){
				$this->Salesorder->save($arr_salesorder);
			}
			echo '/shippings/entry/'. $this->Shipping->mongo_id_after_save;
			die;
		}
		echo '/shippings/entry';
		die;



	}
	public function create_shipping(){
		if(!$this->check_permission('shippings_@_entry_@_add')){
			echo 'You do not have permission on this action.';
			die;
		}
		$id = $this->get_id();
		$this->selectModel('Salesinvoice');
		$salesinvoice = $this->Salesinvoice->select_one(array('_id' => new MongoId($id)));
		if($salesinvoice['company_id']==''
				&&$salesinvoice['contact_id']==''){
			echo json_encode(array('status'=>'error','message'=>'This function cannot be performed as there is no company or contact linked to this record.'));
			die;
		}
		else if(empty($salesinvoice['products'])){
			echo json_encode(array('status'=>'error','message'=>'There are no items on this sales invoice that are available for shipping because they were created from a job expense or resource.'));
			die;
		}
		else if(isset($salesinvoice['shipping_id'])&&$salesinvoice['shipping_id']!=''){
			echo json_encode(array('status'=>'error','message'=>'This invoice is already shipped.'));
			die;
		}
		else if($salesinvoice['invoice_status']=='Cancelled'){
			echo json_encode(array('status'=>'error','message'=>'This sales invoice has been cancelled.'));
			die;
		}
		$arr_save = $salesinvoice;
		$this->selectModel('Shipping');
		$arr_save['salesinvoice_id'] = $salesinvoice['_id'];
		$arr_save['salesinvoice_name'] = $salesinvoice['name'];
		$arr_save['salesinvoice_number'] = $salesinvoice['code'];
		$arr_save['code'] = $this->Shipping->get_auto_code('code');
		$arr_save['shipping_type'] =  "Out";
		$arr_save['shipping_status'] = 'Completed';
		$arr_save['shipping_date'] = new MongoDate();
		$arr_save['sum_amount']  = (isset($salesinvoice['sum_amount']) ? $salesinvoice['sum_amount'] : 0);
		$arr_save['sum_sub_total']  = (isset($salesinvoice['sum_sub_total']) ? $salesinvoice['sum_sub_total'] : 0);
		$arr_save['sum_tax'] = (isset($salesinvoice['sum_tax']) ? $salesinvoice['sum_tax'] : 0);
		$arr_save['tax']  = (isset($salesinvoice['tax']) ? $salesinvoice['tax'] : 0);
		$arr_save['taxval'] = (isset($salesinvoice['taxval']) ? $salesinvoice['taxval'] : 0);
		$arr_save['carrier_id'] = '';
		$arr_save['carrier_name'] = '';
		if($arr_save['shipping_address'] == '')
			$arr_save['shipping_address'] = $arr_save['invoice_address'];
		$arr_save['received_date'] = '';
		$arr_save['return_status'] = 0;
		$arr_save['tracking_no'] = '';
		$arr_save['traking'] = 0;
		unset($arr_save['_id']);
		unset($arr_save['date_modifide']);
		unset($arr_save['modified_by']);
		unset($arr_save['paid_date']);
		unset($arr_save['payment_due_date']);
		unset($arr_save['payment_terms']);
		unset($arr_save['invoice_date']);
		if($this->Shipping->save($arr_save)){
			$id = $this->Shipping->mongo_id_after_save;
			$salesinvoice['shipping_code'] = $arr_save['code'];
			$salesinvoice['shipping_id'] = $id;
			$this->selectModel('Salesinvoice');
			$this->Salesinvoice->save($salesinvoice);
			echo json_encode(array('status'=>'ok','url'=>URL.'/shippings/entry/'.$id));
		}
		die;
	}
	public function create_receipt($option='')
	{
		if(!$this->check_permission('salesinvoices_@_entry_@_add')
			&& !$this->check_permission($this->name.'_@_receipt_tab_@_add') ){
			echo 'You do not have permission on this action.';
			die;
		}
		$this->selectModel('Salesinvoice');
		$id = $this->get_id();
		$invoice = $this->Salesinvoice->select_one(array('_id'=>new MongoId($id)),array('company_id','sum_sub_total','sum_amount','taxval','invoice_status','company_name','code'));
		if($invoice['invoice_status']=='Paid'){
			if($option==''){
				echo 'Change status';
			}
			die;
		}
		// $invoice['invoice_status'] = 'Invoiced';
		$this->selectModel("Receipt");
		$arr_save = array();
		$arr_save['deleted'] = false;
		$arr_save['code'] = $this->Receipt->get_auto_code('code');
		$arr_save['amount_received'] = 0;
		$arr_save['description'] = '';
		$this->selectModel('Salesaccount');
		if(isset($invoice['company_id'])&&is_object($invoice['company_id'])){
			$salesaccount = $this->Salesaccount->select_one(array('company_id'=>$invoice['company_id']));
			if(!empty($salesaccount)){
				$arr_save['salesaccount_id'] = new MongoId($salesaccount['_id']);
				$arr_save['salesaccount_name'] = $invoice['company_name'];
			}
			$arr_save['company_name'] = $invoice['company_name'];
			$arr_save['company_id'] = new MongoId($invoice['company_id']);
		}
		$arr_save['receipt_date'] = new MongoDate();
		$arr_save['paid_by'] = '';
		$arr_save['our_bank_account'] = '';
		$arr_save['our_rep'] = (isset($invoice['our_rep']) ? $invoice['our_rep'] : '');
		$arr_save['our_rep_id'] = (isset($invoice['our_rep_id']) ? $invoice['our_rep_id'] : '');
		$arr_save['our_csr'] = (isset($invoice['our_csr']) ? $invoice['our_csr'] : '');
		$arr_save['our_csr_id'] = (isset($invoice['our_csr_id']) ? $invoice['our_csr_id'] : '');
		$arr_save['identity'] = (isset($invoice['indentity']) ? $invoice['indentity'] : '');
		$arr_save['use_own_letterhead'] = 0;
		$arr_save['ext_accounts_sync']  = 0 ;
		$arr_save['notes'] = '';
		$arr_save['allocation'][0] = array(
									'deleted'=>false,
									'salesinvoice_code'=>$invoice['code'],
									'salesinvoice_id'=> new MongoId($invoice['_id']),
									'note'=> '',
									'amount'=>0,
									'mod'=>'Part'
			);
		if($option=='Fully'){
			$sum_amount = $this->update_balace_credit_salesaccount($invoice);
			$arr_save['allocation'][0] = array(
									'deleted'=>false,
									'salesinvoice_code'=>$invoice['code'],
									'salesinvoice_id'=> new MongoId($invoice['_id']),
									'note'=> '',
									'amount'=>$sum_amount,
									'mod'=>'Fully'
			);
			//Update lại status, ngày trả tiền cho SI
			$invoice['invoice_status'] = 'Paid';
			$invoice['paid_date'] = $arr_save['receipt_date'];
			if(!isset($invoice['total_receipt']))
				$invoice['total_receipt'] = $sum_amount;
			else
				$invoice['total_receipt'] += $sum_amount;
			//Trả full thì gán amount_received bằng tổng tiền cả SI luôn
			$arr_save['amount_received'] = $sum_amount;
		}
		$arr_save['comments'] = '';
		$arr_save['total_allocated'] = (isset($arr_save['allocation'][0]['amount']) ? $arr_save['allocation'][0]['amount'] : 0);
		$arr_save['unallocated'] = $arr_save['amount_received'] - $arr_save['total_allocated'];

		$this->Salesinvoice->save($invoice);
		if($this->Receipt->save($arr_save)){
			$id = $this->Receipt->mongo_id_after_save;
			if($option=='')
				$this->redirect('/receipts/entry/'.$id);
			else if($option=="Part")
				echo URL.'/receipts/entry/'.$id;
		}
		die;
	}


	public function delete_all_associate($idopt,$key=''){
		$query=array();
		$ids = $this->get_id();
		if($ids!=0)
			$query=$this->opm->select_one(array('_id'=>new MongoId($ids)),array('company_id','sum_amount','products','taxval','sum_sub_total'));
		else{
			echo 'function SIController -> function delete_all_associate: ids is null'; die;
		}
		//Update lại balance và receipt cho SA
		$this->update_balace_credit_salesaccount($query,'minus');

		if($key=='products'){ // update cac line entry option cua products
			if( $this->check_permission($this->name.'_@_entry_@_delete') ){
				if($ids!=''){
					$arr_insert = $line_entry = array();
					//lay note products hien co
					$query = $this->opm->select_one(array('_id'=>new MongoId($ids)),array('products'));
					if(isset($query['products']) && !empty($query['products'])){
						$line_entry = $query['products'];
						$line_entry[$idopt] =  array('deleted'=>true);
						foreach($query['products'] as $keys=>$values){
							if(isset($values['option_for']) && $values['option_for']==$idopt){
                                $line_entry[$keys] = array('deleted'=>true);
							}
						}
					}
					$arr_insert['products'] = $line_entry;//pr($line_entry);die;
					$arr_insert['_id'] 		= new MongoId($ids);
					$arr_insert = array_merge($arr_insert,$this->new_cal_sum($line_entry));
					$this->opm->save($arr_insert);
				}
			}
		}
	}
	function update_balace_credit_salesaccount($query,$action = 'plus'){
		if(isset($query['company_id']) && is_object($query['company_id'])){
			$minimum = 50;
	        $this->selectModel('Stuffs');
	        $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
	        $product_id = $product['product_id'];
	        if(isset($product['product_id'])&&is_object($product['product_id'])){
	            $this->selectModel('Product');
	            $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('sell_price'));
	            $minimum = $product['sell_price'];
	            $product_id = $product['_id'];
	        }
	        $this->selectModel('Company');
	        $company = $this->Company->select_one(array('_id'=>$company_id),array('pricing'));
			if(isset($company['pricing'])){
				foreach($company['pricing'] as $pricing){
					if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
					if((string)$pricing['product_id']!=(string)$product_id) continue;
					if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
					$price_break = reset($pricing['price_break']);
					$minimum =  (float)$price_break['unit_price']; break;
				}
			}
			if($query['sum_sub_total']<$minimum){
				$query['taxval'] = (isset($query['taxval']) ? $query['taxval'] : 0);
				$query['sum_amount'] = $minimum + ($minimum*$query['taxval']/100);
			}
			$sum_amount = ($action == 'plus' ? $query['sum_amount'] : (-$query['sum_amount']));
			$this->selectModel('Salesaccount');
			$this->Salesaccount->update_account($query['company_id'], array(
												'model' => 'Company',
												'balance' =>   $sum_amount,
												'invoices_credits' =>  $sum_amount,
												));
		}
		return $query['sum_amount'];
	}

	public function check_over_credit_limit($sum_amount=0){
		$ids=$this->get_id();
		$arr_salesinvoice=array();
		$arr_company=array();
		$arr_salesorder=array();
		$sum_amount_all=0;
		$total_invoice=0;
		if($ids!=0){
			$arr_salesinvoice=$this->opm->select_one(array('_id'=>new MongoId($ids)));
			$this->selectModel('Salesorder');
			if(is_object($arr_salesinvoice['salesorder_id'])){
				$query_salesinvoice = $this->opm->select_all(array(
					'arr_where' => array('salesorder_id' => new MongoId($arr_salesinvoice['salesorder_id']))
				));

				if(is_object($query_salesinvoice)){
					foreach($query_salesinvoice as $key=>$value){
						$total_invoice += isset($value['sum_amount'])?$value['sum_amount']:0;
					}
				}

				$arr_salesorder=$this->Salesorder->select_one(array('_id'=>new MongoId($arr_salesinvoice['salesorder_id'])));
			}



		}

		$this->selectModel('Company');
		$this->selectModel('Salesinvoice');
		if(is_object($arr_salesinvoice['company_id'])){
			$arr_company=$this->Company->select_one(array('_id'=>new MongoId($arr_salesinvoice['company_id'])));
			$all_sales_invoice = $this->Salesinvoice->select_all(array(
				'arr_where'=>array('company_id'=>new MongoId($arr_salesinvoice['company_id']))
			));
			if(is_object($all_sales_invoice)){
				foreach($all_sales_invoice as $key=>$value){
					if(isset($value['sum_amount'])&&!$value['deleted'])
						$sum_amount_all+=(float)$value['sum_amount'];
				}
			}


		}

		if(is_array($arr_company['account']))
		{

			if(isset($arr_company['account']['credit_limit'])&&isset($sum_amount)&&$sum_amount>$arr_company['account']['credit_limit']&&$arr_company['account']['credit_limit']!=0)
			{
				echo 'over';die;
			}

			if(isset($arr_company['account']['credit_limit'])&&$arr_company['account']['credit_limit']!=0)
			{
				if($total_invoice>$arr_company['account']['credit_limit']){
					echo 'over1';die;
				}
			}

			if($sum_amount_all>$arr_company['account']['credit_limit']){
				echo 'over2';die;
			}

		}
		die;
	}
	public function check_have_salesorder_id(){
		$ids=$this->get_id();
		$arr_salesinvoice=array();
		if($ids!=0)
			$arr_salesinvoice=$this->opm->select_one(array('_id'=>new MongoId($ids)));
		if(is_object($arr_salesinvoice['salesorder_id']))
		{
			echo 'have_link_to_sales_order';
			die;
		}
		else
		{
			echo 'no_have_link_to_sales_order';
			die;
		}
	}

	public function view_minilist(){
		if(!isset($_GET['print_pdf'])){
			$arr_where = $this->arr_search_where();
			$salesinvoices = $this->opm->select_all(array(
													'arr_where' => $arr_where,
													'arr_field' => array('code','invoice_type','company_name','phone','invoice_date','payment_due_date','our_rep','job_number'),
													'arr_order' => array('_id'=>1),
													'limit'     => 2000
													));

			$arr_data = array();
			if($salesinvoices->count() > 0){
				$group = array();
				$html= '';
				$i = 0;
				foreach($salesinvoices as $key => $salesinvoice){
					$html .= '<tr class="'.($i%2==0 ? 'bg_2' : 'bg_1').'">';
					$html .= '<td>'.(isset($salesinvoice['code']) ? $salesinvoice['code'] : '') .'</td>';
					$html .= '<td>'.(isset($salesinvoice['invoice_type']) ? $salesinvoice['invoice_type'] : '') .'</td>';
					$html .= '<td>'.(isset($salesinvoice['company_name']) ? $salesinvoice['company_name'] : '') .'</td>';
					$html .= '<td>'.(isset($salesinvoice['phone']) ? $salesinvoice['phone'] : '') .'</td>';
					$html .= '<td class="center_text">'.(isset($salesinvoice['invoice_date']) ?date('m/d/Y',$salesinvoice['invoice_date']->sec):'') .'</td>';
					$html .= '<td class="center_text">'.(isset($salesinvoice['payment_due_date']) && is_object($salesinvoice['payment_due_date'])?date('m/d/Y',$salesinvoice['payment_due_date']->sec):'') .'</td>';
					$html .= '<td>'.(isset($salesinvoice['our_rep']) ? $salesinvoice['our_rep'] : '') .'</td>';
					$html .= '<td>'.(isset($salesinvoice['job_number']) ? $salesinvoice['job_number'] : '') .'</td>';
					$html .= '</tr>';
	                $i++;
				}
				$html .='<tr class="last">
					<td colspan="8" class="bold_text right_none">'.$i.' record(s) listed.</td>
				</tr>';
				$arr_data['title'] = array('Ref No','Type'=>'text-align: left', 'Customer'=>'text-align: left', 'Phone'=>'text-align: left', 'Date', 'Due', 'Our Rep'=>'text-align: left', 'Job No'=>'text-align: left');
				$arr_data['content'] = $html;
				$arr_data['report_name'] = 'Salesinvoice Mini  Listing';
				$arr_data['report_file_name'] = 'SI_'.md5(time());
				$arr_data['report_orientation'] = 'landscape';
				Cache::write('salesinvoices_minilist', $arr_data);
	        }
    	} else
    		$arr_data = Cache::read('salesinvoices_minilist');
		$this->render_pdf($arr_data);
	}
	public function option_list_data($products_id='',$idsub=-1) {
		$data = $option_group = array(); $groupstr = '';

		if($idsub<0)
			return $data;

		if(is_object($products_id))
			$products_id = (string)$products_id;

		if($products_id!=''){
			$this->selectModel('Product');
			$products = $this->Product->options_data($products_id);
		}
		$custom_option = $this->salesinvoice_options_data($idsub);
		if(isset($products['productoptions']) && count($products['productoptions'])>0){
			$data = $products['productoptions'];
			foreach($data as $kk=>$vv){
				if(isset($custom_option[$kk])){
                    $option = $custom_option[$kk];
                    if(isset($option['quantity'])&&$option['quantity']!=$vv['quantity']
                       || isset($option['unit_price'])&&$option['unit_price']!=$vv['unit_price']
                       || isset($option['discount'])&&$option['discount']!=$vv['discount']){
					   $data[$kk] = array_merge($vv,array_merge($custom_option[$kk],array('is_custom'=>true)));
                    }
                }
			}
		}else{
            foreach($custom_option as $key=>$value)
                $custom_option[$key]['is_custom'] = true;
			$data = $custom_option;
        }
        if(!empty($data)){
            foreach($data as $k=>$v)
                $data[$k]['_id'] = $k;
            $this->opm->aasort($data,'option_group');
        }
		//pr($products['productoptions']);pr($custom_option); pr($data);die;
		//tim danh sach field proids trong cac option cua line dang xu ly
		$arr_lineid = $this->find_sub_line_entry_for_line($idsub,'proids','swap');
		//tim danh sach cac product id
		$arr_proid = $this->find_sub_line_entry_for_line($idsub,'proids');

		//pr($arr_lineid);pr($arr_proid);die;echo $proids."</br>";

		foreach($data as $kks=>$vvs){
			if(!isset($vvs['product_id']))
				continue;

			$proids = $products_id.'_'.$kks;

			if(in_array($proids,$arr_proid))
				$data[$kks]['choice'] = 1;
			else
				$data[$kks]['choice'] = 0;

			if(isset($arr_lineid[$products_id.'_'.$kks]))
				$data[$kks]['line_no'] = $arr_lineid[$products_id.'_'.$kks];
			else
				$data[$kks]['line_no'] = '';

			if(isset($vvs['require']) && (int)$vvs['require']==1){
				if(isset($vvs['group_type']) && $vvs['group_type']=='Exc')
					$m=0;
				else
					$data[$kks]['xlock']['choice'] = 1;
			}

			if(!isset($vvs['proline_no']))
				$data[$kks]['proline_no'] = $kks;

			if(!isset($vvs['parent_line_no']))
				$data[$kks]['parent_line_no'] = $idsub;

			if(isset($vvs['option_group'])){
				$option_group[$vvs['option_group']] = (string)$vvs['option_group'];
				$groupstr.= (string)$vvs['option_group'].',';
			}
		}
		$arr_return = array();
		$arr_return['option'] = $data;
		$arr_return['groupstr'] = $groupstr;
		$arr_return['option_group'] = $option_group;
		return $arr_return;

	}
	 public function find_sub_line_entry($keysearch = 'proids',$keys='') {
		 $arr = array();
		 if ($this->get_id() != '') {
			$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			if(isset($query['products']) && is_array($query['products']) && count($query['products'])>0){
				foreach($query['products'] as $kk=>$vv){
					if(isset($vv['deleted']) && $vv['deleted']==false && isset($vv[$keysearch]) && $vv[$keysearch]!=''){
						if($keys=='')
							$arr[] = $vv[$keysearch];
						else if($keys=='swap')
							$arr[$vv[$keysearch]] = $kk;
						else
							$arr[$kk] = $vv[$keysearch];
					}
				}
			}
		 }
		 return $arr;
	 }
	 public function find_sub_line_entry_for_line($option_for='',$keysearch = 'proids',$keys='') {
		 $arr = array();
		 if ($this->get_id() != '' && $option_for!='') {
			$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			if(isset($query['products']) && is_array($query['products']) && count($query['products'])>0){
				foreach($query['products'] as $kk=>$vv){
					if(isset($vv['deleted']) && $vv['deleted']==false && isset($vv[$keysearch]) && $vv[$keysearch]!='' && $vv['option_for'] == $option_for){
						if($keys=='')
							$arr[] = $vv[$keysearch];
						else if($keys=='swap')
							$arr[$vv[$keysearch]] = $kk;
						else
							$arr[$kk] = $vv[$keysearch];
					}
				}
			}
		 }
		 return $arr;
	 }
	 public function salesinvoice_options_data($parent_line_no=0){
		$arr_op = array();
		$ids = $this->get_id();
		if($ids!=''){
			$query = $this->opm->select_one(array('_id'=>new MongoId($ids)),array('products','options'));
			if(isset($query['options']) && is_array($query['options']) && count($query['options'])>0){
				foreach($query['options'] as $k=>$vs){
					//lay thong tin cac option custom cua line entry cha $parent_line_no
					if(isset($vs['deleted']) && !$vs['deleted'] &&  isset($vs['parent_line_no']) && $vs['parent_line_no']==$parent_line_no ){
						if(!isset($vs['proline_no']) || $vs['proline_no']=='')
							$vs['proline_no'] = $k;
						$arr_op[$vs['proline_no']] = $vs;
						$arr_op[$vs['proline_no']]['thisline_no'] = $k;
						$arr_op[$vs['proline_no']]['proline_no'] = $k;
					}
				}
			}
		}
		return $arr_op;
	}
	public function costing_list(){
		if(isset($_POST['submit'])){
            $this->option_cal_price($_POST);
        }
        $this->set('return_mod', true);
        $this->set('return_title', 'Making for this item');
        $this->set('return_link', URL . '/salesinvoices/entry');
        $opname = 'products';
        $arr_set = $this->opm->arr_settings;
        $subdatas = $arr_subsetting = $custom_option_group = array();
        $salesorder_code = $sumrfq = 0; $products_id = $idsub = $groupstr = '';
		//neu idopt khac rong
		if ($this->params->params['pass'][1] != '') {
            //DATA: salesorder line details
            $idsub = $this->params->params['pass'][1];
            $arr_ret = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('products','options','invoice_date'));
            if(!isset($arr_ret['options']))
            	$arr_ret['options'] = array();
            $subdatas['salesinvoice_line_details'] = array();
            $products_id = '';
            if(!empty($arr_ret[$opname])){
                foreach($arr_ret[$opname] as $key => $value){
                    if($key!=$idsub) continue;
                    if(isset($value['deleted'])&&$value['deleted']) continue;
                    $subdatas['salesinvoice_line_details'] = $value;
                    $subdatas['salesinvoice_line_details']['key'] = $key;
                    $products_id = $value['products_id'];
                    break;
                }
            }
            //DATA: option list
            $arr_ret[$opname][$idsub]['products_id'] = $products_id;
            $option_list_data = $this->new_option_data(array('key'=>$idsub,'products_id'=>$products_id,'options'=>$arr_ret['options'],'date'=>$arr_ret['invoice_date']),$arr_ret['products']);
            $subdatas['option'] = $option_list_data['option'];
        }
        //VIEW: option list
         $arr_field_options['option']['option'] = array(
                'title'     => "Making for this item",
                'type'      => 'listview_box',
                'link'      => array('w' => '1', 'cls' => 'products','field'=>'product_id'),
                'css'       => 'width:100%;',
                'height'    => '420',
                'reltb'     => 'tb_product@options',
                'footlink'  => array('label' => 'Click to view and edit in this product', 'link' => ''.URL.'/products/entry/'.$products_id),
                'field'     => array(
                        'code' => array(
                            'name' => __('Code'),
                            'type' => 'text',
                            'width' => '5',
                            'align' => 'center',
                        ),
                        'product_name' => array(
                            'name' => __('Name'),
                            'width' => '28',
                            'edit'  => 1,
                        ),
                        'product_id' => array(
                            'name' => __('ID'),
                            'type'=>'hidden',
                        ),
                        'require' => array(
                            'width' => '5',
                            'align'=>'center',
                            'type'=>'hidden',
                        ),
                        'choice' => array(
                            'width' => '5',
                            'align'=>'center',
                            'type'=>'hidden',
                        ),
                        'same_parent' => array(
                            'name' => __('<span title="Same info as parent product">S.P.</span>'),
                            'width' => '3',
                            'align'=>'center',
                            'type'=>'checkbox',
                        ),
                        'unit_price' => array(
                            'name' => __('Unit cost'),
                            'width' => '7',
                            'type' => 'price',
                            'edit'  => '1',
                            'align' => 'right',
                            'numformat'=>3,
                        ),
                        'oum' => array(
                            'name' => __('UOM'),
                            'width' => '5',
                            'type'      => 'select',
                            'droplist'  => 'product_oum_area',
                            'edit'  => 1
                        ),
                        'discount' => array(
                            'name' => __('%Discount'),
                            'width' => '8',
                            'edit'  => '1',
                            'type' => 'hidden',
                            'align' => 'right',
                        ),
                        'quantity' => array(
                            'name' => __('Quantity'),
                            'width' => '5',
                            'edit'  => '1',
                            'type' => 'text',
                            'align' => 'right',
                        ),
                        'sub_total' => array(
                            'name' => __('Sub total'),
                            'width' => '7',
                            'align' => 'right',
                            'type' => 'price',
                        ),
                        'group_type' => array(
                            'name' => __('Type'),
                            'width' => '5',
                            'type'=>'text',
                            'droplist' => 'product_group',
                        ),
                        'option_group' => array(
                            'name' => __('Group'),
                            'width' => '7',
                            'type'=>'select',
                            'droplist' => 'product_group',
                        ),
                        //so thu tu line entry cua option nay
                        'line_no' => array(
                            'width' => '0',
                            'type'=>'hidden',
                        ),
                        //so thu tu option ben product
                        'proline_no' => array(
                            'width' => '0',
                            'type'=>'hidden',
                        ),
                        //so thu tu trong option cua quota
                        'thisline_no' => array(
                            'type'=>'hidden',
                        ),
                        'this_line_no' => array(
                            'type'=>'hidden',
                        ),
                        //so thu tu line entry cha
                        'parent_line_no' => array(
                            'width' => '0',
                            'type'=>'hidden',
                        ),
                        'delete' => array(
                            'type'  => 'delete_icon',
                            'rev'   => 'option',
                            'node'   => 'options',
                            'width' => 2
                        )
                ),
        );
        if(!is_object($arr_ret[$opname][$idsub]['products_id']) || $this->Product->count(array('_id'=>$arr_ret[$opname][$idsub]['products_id'],'options.deleted'=>false))==0){
             $arr_field_options['option']['option']['add'] = 'Add more option';
			 $arr_field_options['option']['option']['field']['require']['edit'] = '1';
			 $arr_field_options['option']['option']['field']['same_parent']['edit'] = '1';
			 $arr_field_options['option']['option']['field']['group_type']['type'] = 'select';
			 $arr_field_options['option']['option']['field']['group_type']['edit'] = '1';
			 $arr_field_options['option']['option']['field']['option_group']['edit'] = '1';
			 $option_select_custom = array();
			 $option_select_custom['option_group'] = $option_list_data['option_group'];
			 $option_select_custom['group_type'] = array('Inc'=>'Inc','Exc'=>'Exc');
			 $this->selectModel('Setting');
             $option_select_custom['oum'] = $this->Setting->uom_option_list(true);
             $this->set('option_select_custom', $option_select_custom);
			 $this->set('groupstr', $option_list_data['groupstr']);
        }
        $this->set('subdatas', $subdatas);
        $this->set('arr_subsetting', $arr_subsetting);
		$this->set('arr_field_options', $arr_field_options);
        $this->set('line_sum', 18);
        $this->set('salesinvoiceline', 'salesinvoice_line_details');
        $this->set('salesinvoice_code', $salesorder_code);
        $this->set('sumrfq', $sumrfq);
		$this->set('products_id', (string)$arr_ret[$opname][$idsub]['products_id']);
        $this->set('subitems', $idsub);
        $this->set('employee_id', $this->opm->user_id());
        $this->set('employee_name', $this->opm->user_name());
        if(!isset($arr_ret[$opname][$idsub]['is_saved']))
		  $this->set('custom_product2', '1');
	}
	public function option_list() {
		if(isset($_POST['submit'])){
            $this->option_cal_price($_POST);
        }
        $opname = 'products';
        $arr_set = $this->opm->arr_settings;
        $subdatas = $arr_subsetting = $custom_option_group = array();
        $salesorder_code = $sumrfq = 0; $products_id = $idsub = $groupstr = '';
		//neu idopt khac rong
        if ($this->params->params['pass'][1] != '') {
            //DATA: salesorder line details
            $idsub = $this->params->params['pass'][1];
            $arr_ret = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('products','options','invoice_date'));
            if(!isset($arr_ret['options']))
            	$arr_ret['options'] = array();
            $subdatas['salesinvoice_line_details'] = array();
            $products_id = '';
            if(!empty($arr_ret[$opname])){
                if(isset($arr_ret[$opname][$idsub])&&!$arr_ret[$opname][$idsub]['deleted']){
                    $products_note = '';
                    $subdatas['salesinvoice_line_details'] = $arr_ret[$opname][$idsub];
                    $products_id = $arr_ret[$opname][$idsub]['products_id'];
                    if(is_object($products_id)){
                        $this->selectModel('Product');
                        $notes = $this->Product->select_one(array('_id'=>$products_id),array('otherdetails'));
                        if(isset($notes['otherdetails'])){
                            foreach($notes['otherdetails'] as $note){
                                if(isset($note['deleted']) && $note['deleted']) continue;
                                $products_note = '<b>'.$note['heading'].'</b> '.$note['details'];
                            }
                        }
                    }
                    $this->set('products_name',$arr_ret[$opname][$idsub]['products_name']);
                    $this->set('products_note',$products_note);
                }
            }
            //DATA: option list
            $arr_ret[$opname][$idsub]['products_id'] = $products_id;
            $option_list_data = $this->new_option_data(array('key'=>$idsub,'products_id'=>$products_id,'options'=>$arr_ret['options'],'date'=>$arr_ret['invoice_date']),$arr_ret['products']);
            $subdatas['option'] = $option_list_data['option'];

        }

        //VIEW: option list
        $arr_field_options['option']['option'] = array(
				'title' 	=> "Options for this item",
				'type' 		=> 'listview_box',
    			'link' 		=> array('w' => '1', 'cls' => 'products','field'=>'product_id'),
        		'css'  		=> 'width:100%;',
        		'height'    => '420',
				'reltb' => 'tb_product@options',
        		'footlink'  => array('label' => 'Click to view and edit in this product', 'link' => ''.URL.'/products/entry/'.$products_id),
        		'field'		=> array(
						'choice' => array(
							'name' => __('Choice'),
							'width' => '5',
							'type'=>'checkbox',
							'edit'=>'1',
							'default'=>0,
						),
						'code' => array(
							'name' => __('Code'),
							'type' => 'text',
							'width' => '7',
							'align' => 'center',
						),
						'product_name' => array(
							'name' => __('Name'),
							'width' => '28',
						),
						'product_id' => array(
							'name' => __('ID'),
							'type'=>'hidden',
						),
						'require' => array(
							'name' => __('Req'),
							'width' => '5',
							'align'=>'center',
							'type'=>'hidden',
						),
						'same_parent' => array(
							'width' => '3',
							'align'=>'center',
							'type'=>'hidden',
						),
						'unit_price' => array(
							'name' => __('Unit cost'),
							'width' => '7',
							'type' => 'price',
                            'edit'  => '1',
							'align' => 'right',
							'numformat'=>3,
						),
						'oum' => array(
							'name' => __('UOM'),
							'width' => '5',
							'type' 		=> 'text',
							'droplist'	=> 'product_oum_area',
							'align' => 'center',
						),
						'sell_by' => array(
							'type' 		=> 'hidden',
						),
						'discount' => array(
							'name' => __('%Discount'),
							'width' => '8',
                            'edit'  => '1',
							'type' => 'hidden',
							'align' => 'right',
						),
						'quantity' => array(
							'name' => __('Quantity'),
							'width' => '5',
                            'edit'  => '1',
							'type' => 'text',
							'align' => 'right',
						),
						'sub_total' => array(
							'name' => __('Sub total'),
							'width' => '7',
							'align' => 'right',
							'type' => 'price',
						),
						'group_type' => array(
							'name' => __('Type'),
							'width' => '7',
							'type'=>'text',
							'droplist' => 'product_group',
						),
						'option_group' => array(
							'name' => __('Group'),
							'width' => '7',
							'type'=>'select',
							'droplist' => 'product_group',
						),
						//so thu tu line entry cua option nay
						'line_no' => array(
							'width' => '0',
							'type'=>'hidden',
						),
						//so thu tu option ben product
                        'proline_no' => array(
                            'width' => '0',
                            'type'=>'hidden',
                        ),
						//so thu tu trong option cua quota
                        'this_line_no' => array(
                            'type'=>'hidden',
                        ),
                        'thisline_no' => array(
                            'type'=>'hidden',
                        ),
						//so thu tu line entry cha
                        'parent_line_no' => array(
                            'width' => '0',
                            'type'=>'hidden',
                        ),
				),
		);
        if(!isset($arr_ret[$opname][$idsub]['products_id']) || !is_object($arr_ret[$opname][$idsub]['products_id'])){
             $arr_field_options['option']['option']['add'] = 'Add more option';
			 $arr_field_options['option']['option']['delete'] = '1';
			 $arr_field_options['option']['option']['field']['require']['edit'] = '1';
			 $arr_field_options['option']['option']['field']['same_parent']['edit'] = '1';
			 $arr_field_options['option']['option']['field']['group_type']['type'] = 'select';
			 $arr_field_options['option']['option']['field']['group_type']['edit'] = '1';
			 $arr_field_options['option']['option']['field']['option_group']['edit'] = '1';
			 $option_select_custom = array();
			 $option_select_custom['option_group'] = $option_list_data['option_group'];
			 $option_select_custom['group_type'] = array('Inc'=>'Inc','Exc'=>'Exc');
			 $this->set('option_select_custom', $option_select_custom);
			 $this->set('groupstr', $option_list_data['groupstr']);
        }
        $this->set('subdatas', $subdatas);
        $this->set('arr_subsetting', $arr_subsetting);
		$this->set('arr_field_options', $arr_field_options);
        $this->set('line_sum', 18);
        $this->set('salesinvoiceline', 'salesinvoice_line_details');
        $this->set('salesinvoice_code', $salesorder_code);
        $this->set('sumrfq', $sumrfq);
		$this->set('products_id', (string)$arr_ret[$opname][$idsub]['products_id']);
        $this->set('subitems', $idsub);
        $this->set('employee_id', $this->opm->user_id());
        $this->set('employee_name', $this->opm->user_name());
        if(!isset($arr_ret[$opname][$idsub]['is_saved']))
		  $this->set('custom_product2', '1');

    }
    public function save_new_line_entry_option($product_id='',$option_id='',$option_for=''){
		if(isset($_POST['product_id']))
			$product_id = $_POST['product_id'];
		if(isset($_POST['option_id']))
			$option_id = $_POST['option_id'];
		if(isset($_POST['option_for']))
			$option_for = $_POST['option_for'];

		$ids = $this->get_id();
		if($ids!=''){
			$arr_insert = $line_entry = $parent_line = array();
			//lay note products hien co
			$query = $this->opm->select_one(array('_id'=>new MongoId($ids)),array('products','company_id'));
			if(isset($query['products']) && is_array($query['products']) && count($query['products'])>0){
				$line_entry = $query['products'];
                $key = count($line_entry);
			}
			//khởi tạo line entry mới
			$option_line_data = $this->option_list_data($product_id,$option_for);
			$options_data = $option_line_data['option'];
			if(isset($options_data[$option_id])){
				$vv = $options_data[$option_id];

				if(isset($line_entry[$option_for]))
					$parent_line = $line_entry[$option_for];

				$new_line = array();
				$new_line['code'] 			= $vv['code'];
				$new_line['sku'] 			= (isset($vv['sku']) ? $vv['sku'] : '');
				$new_line['products_name'] 	= $vv['product_name'];
				$new_line['products_id'] 	= $vv['product_id'];
				$new_line['quantity'] 		= $vv['quantity'];
				$new_line['sub_total'] 		= $vv['sub_total'];
				$new_line['sizew'] 			= isset($parent_line['sizew']) ? $parent_line['sizew'] : $vv['sizew'];
				$new_line['sizew_unit'] 	= isset($parent_line['sizew_unit'])?$parent_line['sizew_unit']:$vv['sizew_unit'];
				$new_line['sizeh'] 			= isset($parent_line['sizeh']) ? $parent_line['sizeh'] : $vv['sizeh'];
				$new_line['sizeh_unit'] 	= isset($parent_line['sizeh_unit'])?$parent_line['sizeh_unit']:$vv['sizeh_unit'];
				$new_line['sell_by'] 		= (isset($vv['sell_by']) ? $vv['sell_by'] : 'unit');
				$new_line['oum'] 			= $vv['oum'];
				$new_line['same_parent'] 	= isset($vv['same_parent']) ? (int)$vv['same_parent'] : 0;
				$new_line['sell_price'] 	= (float)$vv['unit_price'] - (float)$vv['unit_price']*((float)$vv['discount']/100);

				if(isset($query['products'][$option_for]['taxper']))
					$new_line['taxper'] 	= $query['products'][$option_for]['taxper'];
				if(isset($query['products'][$option_for]['tax']))
					$new_line['tax'] 		= $query['products'][$option_for]['tax'];
				$new_line['option_for'] 	= $option_for;
				$new_line['deleted'] 		= false;
				$new_line['proids'] 		= $product_id.'_'.$option_id;

				if(!isset($query['company_id']))
					$query['company_id']='';

				$cal_price = new cal_price;
				$cal_price->arr_product_items = $new_line;
				$cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$new_line['products_id']);
				$cal_price->field_change = '';
				$cal_price->cal_price_items();
				$new_line = array_merge((array)$new_line,(array)$cal_price->arr_product_items);

				//neu la same_parent thi thay gia cua parent va tinh lai gia
				if($new_line['same_parent']==1){
					$cal_price = new cal_price;
					$cal_price->arr_product_items = $new_line;
					$cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$new_line['products_id']);
					$cal_price->field_change = '';
					$cal_price->cal_price_items();
					$new_line['sell_price'] = $cal_price->arr_product_items['sell_price'];
					if(!isset($line_entry[$option_for]['plus_sell_price']))
						$line_entry[$option_for]['plus_sell_price'] = 0;

					$line_entry[$option_for]['sell_price'] += (float)$new_line['sell_price'];
					$line_entry[$option_for]['plus_sell_price'] += (float)$new_line['sell_price'];
					$cal_price2 = new cal_price;
					$cal_price2->arr_product_items = $line_entry[$option_for];
					$cal_price2->field_change = 'sell_price';
					$cal_price2->cal_price_items();
					$line_entry[$option_for] = $cal_price2->arr_product_items;
					$new_line['sell_price'] = '';
				}

				$line_entry[] = $new_line;


				//neu la nhom Exc thi xoa cac item khac cung nhom
				if(isset($vv['option_group']) && isset($vv['group_type']) &&  $vv['group_type']=='Exc'){
					foreach ($line_entry as $k=>$vs){
						if(isset($vs['deleted']) && !$vs['deleted'] && isset($vs['proids']) && $vs['proids'] !=$product_id.'_'.$option_id){
							$proids = explode("_",$vs['proids']);
							$proids = $proids[1];
							//neu cung nhom
							if(isset($options_data[$proids]['option_group']) && $options_data[$proids]['option_group']==$vv['option_group'] && isset($vs['option_for']) && $vs['option_for']==$option_for){
								//xoa item
								$line_entry[$k]['deleted'] = true;

								//tru ra neu la loai SP
								if($vs['same_parent']==1){
									$cal_price = new cal_price;
									$cal_price->arr_product_items = $line_entry[$option_for];
									$cal_price->price_break_from_to = $this->change_sell_price_company($query['company_id'],$vs['products_id']);
									$cal_price->field_change = '';
									$cal_price->cal_price_items();
									$sellprice = $cal_price->arr_product_items['sell_price'];

									if(!isset($line_entry[$option_for]['plus_sell_price']))
										$line_entry[$option_for]['plus_sell_price'] = 0;

									$line_entry[$option_for]['sell_price'] -= $sellprice;;
									$line_entry[$option_for]['plus_sell_price'] -= $sellprice;

									$cal_price2 = new cal_price;
									$cal_price2->arr_product_items = $line_entry[$option_for];
									$cal_price2->field_change = 'sell_price';
									$cal_price2->cal_price_items();
									$line_entry[$option_for] = $cal_price2->arr_product_items;
								}



							}

						}
					}
				}
                $keyfield = array(
                        "sub_total"     => "sub_total",
                        "tax"           => "tax",
                        "amount"        => "amount",
                        "sum_sub_total" => "sum_sub_total",
                        "sum_tax"       => "sum_tax",
                        "sum_amount"    => "sum_amount"
                    );
                $this->update_sum('products', $keyfield);
				//save lai
				$arr_insert['products'] = $line_entry;
				$arr_insert['_id'] = new MongoId($ids);
				if($this->opm->save($arr_insert)){
					//output
					if(isset($_POST['product_id'])){
                        $new_line['key'] = $key;
						echo json_encode($new_line);die;
					}else
						return $new_array;
				}else{
					if(isset($_POST['product_id'])){
						echo 'error'; die;
					}else
						return false;
				}
			}
		}die;
	}
    public function duplicate_option_product(){
        if(isset($_POST['product_line'])){
            $product_line = $_POST['product_line'];
            $query = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
            if(!isset($query['products']) || empty($query['products'])
                || !isset($query['products'][$product_line]) //Neu khong ton tai product nay
                || (isset($query['products'][$product_line]['deleted'])&&$query['products'][$product_line]['deleted']) /*product nay da bi xoa*/ ){
                echo 'This record is deleted or does not exist!';
                die;
            }
            $this_product = $query['products'][$product_line];
            $product_id = '';
            $product = array();
            $this->selectModel('Product');
            $this->Product->arrfield();
            $default_field = $this->Product->arr_temp;
            //Neu product co ID
            if( isset($query['products'][$product_line]['products_id'])&&is_object($query['products'][$product_line]['products_id']) ){
                $product_id = $query['products'][$product_line]['products_id'];
                $product = $this->Product->select_one(array('_id'=>new MongoId($product_id)));
            } else { //Custom product
                //Lay default
                $product = $default_field;
                $product['sell_price'] = $product['unit_price'] = $product['cost_price'] =  (float)$this_product['unit_price'];
                $product['product_type'] = 'Custom Product';
            }
            $current_user_id = $this->opm->user_id();
            $option = $this->option_list_data($product_id,$product_line);
            foreach($option['option'] as $key=>$value){
                if(isset($value['choice'])&&$value['choice']==1)
                    $option['option'][$key]['require'] = 1;
                if(isset($value['products_id'])&&is_object($value['products_id'])) continue;
                if(isset($value['product_id'])&&is_object($value['product_id'])) continue;
                $option_product = $default_field;
                $option_product['sell_price'] = $option_product['unit_price'] = $option_product['cost_price'] =  $value['sell_price'];
                $option_product['product_type'] = 'Custom Product';
                $option_product['code'] = $this->Product->get_auto_code('code');
                $option_product['name'] = $value['product_name'];
                $option_product['sizeh'] = $value['sizeh'];
                $option_product['sizeh_unit'] = $value['sizeh_unit'];
                $option_product['sizew'] = $value['sizew'];
                $option_product['sizew_unit'] = $value['sizew_unit'];
                $option_product['sku'] = $value['sku'];
                $option_product['sell_by'] = $value['sell_by'];
                $option_product['oum'] = $value['oum'];
                $option_product['options'] = array();
                $option_product['created_by'] = new MongoId($current_user_id);
                unset($option_product['_id']);
                $this->Product->save($option_product);
                unset($option['option'][$key]['original_unit_price']);
                unset($option['option'][$key]['is_tempory_product']);
                unset($option['option'][$key]['sub_total']);
                unset($option['option'][$key]['this_line_no']);
                unset($option['option'][$key]['parent_line_no']);
                unset($option['option'][$key]['line_no']);
                unset($option['option'][$key]['proline_no']);
                unset($option['option'][$key]['adj_qty']);
                unset($option['option'][$key]['plus_unit_price']);
                unset($option['option'][$key]['amount']);
                unset($option['option'][$key]['thisline_no']);
                $new_option_product_id = $this->Product->mongo_id_after_save;
                $option['option'][$key]['name'] = $value['product_name'];
                $option['option'][$key]['code'] = $option_product['code'];
                $option['option'][$key]['product_id'] = new MongoId($new_option_product_id);
                if(isset($value['this_line_no']) && $value['this_line_no']!=''){
                    $query['options'][$value['this_line_no']]['product_id'] = $query['options'][$value['this_line_no']]['products_id'] = new MongoId($new_option_product_id);
                    $query['options'][$value['this_line_no']]['code'] = $option_product['code'];
                }
                if(isset($value['line_no']) && $value['line_no']!=''){
                    $query['products'][$value['line_no']]['products_id']  = new MongoId($new_option_product_id);
                    $query['products'][$value['line_no']]['code']  = $option_product['code'];
                }
            }
            $product['code'] = $this->Product->get_auto_code('code');
            $product['name'] = $this_product['products_name'];
            $product['sizeh'] = $this_product['sizeh'];
            $product['sizeh_unit'] = $this_product['sizeh_unit'];
            $product['sizew'] = $this_product['sizew'];
            $product['sizew_unit'] = $this_product['sizew_unit'];
            $product['sku'] = $this_product['sku'];
            $product['sell_by'] = $this_product['sell_by'];
            $product['oum'] = $this_product['oum'];
            $product['options'] = $option['option'];
            $product['created_by'] = new MongoId($current_user_id);
            unset($product['_id']);
            unset($product['modified_by']);
            $this->Product->save($product);
            $new_product_id = $this->Product->mongo_id_after_save;
            $query['products'][$product_line]['products_id'] = $new_product_id;
            $query['products'][$product_line]['code'] = $product['code'];
            $query['products'][$product_line]['is_saved'] = true;
            if($this->opm->save($query)){
                echo 'ok';
                die;
            } else {
                echo $this->opm->arr_errors_save[1];
                die;
            }
        }
        die;
    }
    function replace_option_product(){
        if(isset($_POST['product_line'])){
            if(!isset($_POST['replace_id']) || strlen($_POST['replace_id'])!=24){
                echo 'There is something wrong. Please refresh and try again!';
                die;
            }
            $product_line = $_POST['product_line'];
            $query = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
            if(!isset($query['products']) || empty($query['products'])
                || !isset($query['products'][$product_line]) //Neu khong ton tai product nay
                || (isset($query['products'][$product_line]['deleted'])&&$query['products'][$product_line]['deleted']) /*product nay da bi xoa*/ ){
                echo 'This record is deleted or does not exist!';
                die;
            }
            $this_product = $query['products'][$product_line];
            $product_id = '';
            $product = array();
            $this->selectModel('Product');
            //Lay default
            $this->Product->arrfield();
            $default_fields = $this->Product->arr_temp;
            //Neu product co ID
            if( isset($query['products'][$product_line]['products_id'])&&is_object($query['products'][$product_line]['products_id']) ){
                $product_id = $query['products'][$product_line]['products_id'];
                $product = $this->Product->select_one(array('_id'=>new MongoId($product_id)));
                $arr_tmp['sellprices'] = (isset($product['sellprices']) ? $product['sellprices'] : array());
                $arr_tmp['pricebreaks'] = (isset($product['pricebreaks']) ? $product['pricebreaks'] : array());
                $arr_tmp['price_note'] = (isset($product['price_note']) ? $product['price_note'] : '');
                if(isset($product['pricing_method'])&&$product['pricing_method']!='')
                    $arr_tmp['pricing_method'] = $product['pricing_method'];
                $arr_tmp['sell_price'] = (isset($product['sell_price']) ? (float)$product['sell_price'] : 0);
                $arr_tmp['code'] = $product['code'];
                $product = $default_fields;
                $product = array_merge($product,$arr_tmp);
            } else { //Custom product
                $product_id = $_POST['replace_id'];
                $product_replace = $this->Product->select_one(array('_id'=>new MongoId($product_id)));
                $product_id = '';
                $product = $default_fields;
                $product['sell_price'] = $product['unit_price'] = $product['cost_price'] =  0;
                $product['product_type'] = 'Custom Product';
                $product['code'] = $product_replace['code'];
            }
            $option = $this->option_list_data($product_id,$product_line);
            foreach($option['option'] as $key=>$value){
                if(isset($value['choice'])&&$value['choice']==1)
                    $option['option'][$key]['require'] = 1;
            }
            $product['name'] = $this_product['products_name'];
            $product['sizeh'] = $this_product['sizeh'];
            $product['sizeh_unit'] = $this_product['sizeh_unit'];
            $product['sizew'] = $this_product['sizew'];
            $product['sizew_unit'] = $this_product['sizew_unit'];
            $product['sku'] = $this_product['sku'];
            $product['sell_by'] = $this_product['sell_by'];
            $product['oum'] = $this_product['oum'];
            $product['options'] = (isset($option['option'])&&!empty($option['option']) ? $option['option'] : array());
            $product['created_by'] = new MongoId($this->opm->user_id());
            $product['_id'] = new MongoId($_POST['replace_id']);
            unset($product['modified_by']);
            $this->Product->save($product);
            $new_product_id = $product['_id'];
            $query['products'][$product_line]['products_id'] = $new_product_id;
            $query['products'][$product_line]['code'] = $product['code'];
            $query['products'][$product_line]['is_saved'] = true;
            if($this->opm->save($query)){
                echo 'ok';
                die;
            } else {
                echo $this->opm->arr_errors_save[1];
                die;
            }
        }
        die;
    }
   	public function rebuild_invoice_option(){
        $invoice = $this->opm->select_all(array('limit'=>999999));
        $i =0;
        echo $invoice->count();
        foreach($invoice as $value){
        	$arr_data = array();
            $arr_data['_id']  = new MongoId($value['_id']);
            $arr_data['sum_tax']  = $value['sum_tax'];
            $arr_data['sum_amount']  = $value['sum_amount'];
            $arr_data['sum_sub_total']  = $value['sum_sub_total'];
            if(isset($value['products'])&&!empty($value['products'])){
                foreach($value['products'] as $product_k => $product){
                	$arr_data['products'][$product_k] = $product;
                	if(isset($product['deleted'])&&$product['deleted'])
                		$arr_data['products'][$product_k] = $value['products'][$product_k] = array('deleted'=>true);
                	if(isset($product['option_for'])&&$product['option_for']!=''){
                		$option_for = $product['option_for'];
                		if(!isset($value['products'][$option_for])
                		   || (isset($value['products'][$option_for]['deleted'])&&$value['products'][$option_for]['deleted'])
                		   )
                			$arr_data['products'][$product_k] = $value['products'][$product_k] = array('deleted'=>true);
                	}
                }
                $arr_sum = $this->new_cal_sum($value['products']);
                $arr_data = array_merge($arr_data,$arr_sum);
            }
            $this->opm->rebuild_collection($arr_data);
            $i++;
        }
        echo '<br />Xong - '.$i;
        die;
    }
    public function salesorders(){
    	$subdatas['salesorders'] = array();
    	$salesinvoice = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('salesorder_id','salesorders'));
    	$salesorders = array();
    	if(isset($salesinvoice['salesorder_id'])&&is_object($salesinvoice['salesorder_id']))
    		$salesorders[] = $salesinvoice['salesorder_id'];
    	if(isset($salesinvoice['salesorders'])){
    		foreach($salesinvoice['salesorders'] as $key=>$value)
				if((string)$salesinvoice['salesorder_id']==(string)$value)
					unset($salesinvoice['salesorders'][$key]);
    		$salesorders = array_merge($salesorders,$salesinvoice['salesorders']);
    	}
    	$this->selectModel('Salesorder');
    	$i = 0;
    	foreach($salesorders as $salesorder_id){
    		$salesorder = $this->Salesorder->select_one(array('_id'=> new MongoId($salesorder_id)),array('code','contact_name','salesorder_date','payment_due_date','status','our_rep','sum_sub_total','sum_tax','sum_amount','taxval'));
    		$minimum = $this->get_minimum_order('Salesorder',$salesorder['_id']);
    		if($salesorder['sum_sub_total']<$minimum){
    			$more_sub_total = $minimum - (float)$salesorder['sum_sub_total'];
    			$sub_total = $more_sub_total;
                $tax = $sub_total*(float)$salesorder['taxval']/100;
                $amount = $sub_total+$tax;
    			$salesorder['sum_sub_total'] += $sub_total;
    			$salesorder['sum_amount'] += $amount;
    			$salesorder['sum_tax'] = $salesorder['sum_amount']-$salesorder['sum_sub_total'];
    		}
    		$subdatas['salesorders'][$i++] = array(
    		                                       'salesorder_id'=>$salesorder['_id'],
    		                                       'code'=>$salesorder['code'],
    		                                       'contact_name'=>$salesorder['contact_name'],
    		                                       'salesorder_date'=>date('d M, Y',$salesorder['salesorder_date']->sec),
    		                                       'payment_due_date'=>date('d M, Y',$salesorder['payment_due_date']->sec),
    		                                       'status'=>$salesorder['status'],
    		                                       'our_rep'=>$salesorder['our_rep'],
    		                                       'sum_sub_total'=> number_format((float)$salesorder['sum_sub_total'],2),
    		                                       'sum_tax'=> number_format((float)$salesorder['sum_tax'],2),
    		                                       'sum_amount'=> number_format((float)$salesorder['sum_amount'],2),
    		                                       );
    	}
        $this->set('subdatas', $subdatas);
    }
    function add_salesorder(){
    	if(!$this->check_permission('salesinvoices_@_entry_@_edit')){
			echo 'You do not have permission on this action.';
			die;
		}
		if(!isset($_POST['ids']) || strlen(trim($_POST['ids']))!=24)
			die;
		$salesorder_id = $_POST['ids'];
		$arr_salesinvoice=array();
		$ids = $this->get_id();
		$this->selectModel('Salesinvoice');
		$this->selectModel('Salesorder');
		$arr_salesorder=$this->Salesorder->select_one(array('_id'=>new MongoId($salesorder_id)));
		if(!is_object($arr_salesorder['company_id'])){
			echo 'no_company';die;
		}
		$v_have_product=0;
		if(isset($arr_salesorder['products'])&&is_array($arr_salesorder['products']))
			foreach($arr_salesorder['products'] as $products){
				if(!$products['deleted']){
					$v_have_product=1;
					break;
				}
			}
		if($v_have_product==0){
			echo 'no_product';die;
		}
		if($this->append_build_salesorder($ids,$arr_salesorder))
			echo 'ok';
		die;
    }
    function batch_invoices_popup($key = ""){
		$this->set('key', $key);
    	$limit = 100;
        $skip = 0;
        $cond = array();
        // Nếu là search GET
        // Nếu là search theo phân trang
        $page_num = 1;
        if (isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0) {
            // $limit = $_POST['pagination']['page-list'];
            $page_num = $_POST['pagination']['page-num'];
            $limit = $_POST['pagination']['page-list'];
            $skip = $limit * ($page_num - 1);
        }
        $arr_order = array('_id'=>-1);
        $this->set('page_num', $page_num);
        $this->set('limit', $limit);
        if (isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0) {
            $sort_type = 1;
            if ($_POST['sort']['type'] == 'desc') {
                $sort_type = -1;
            }
            $arr_order = array($_POST['sort']['field'] => $sort_type);

            $this->set('sort_field', $_POST['sort']['field']);
            $this->set('sort_type', ($sort_type === 1) ? 'asc' : 'desc');
            $this->set('sort_type_change', ($sort_type === 1) ? 'desc' : 'asc');
        }

        // search theo submit $_POST kèm điều kiện
        if (!empty($this->data) && !empty($_POST) && isset($this->data['Salesinvoice'])) {
            $arr_post = $this->Common->strip_search($this->data['Salesinvoice']);
            if (isset($arr_post['code']) && strlen($arr_post['code']) > 0) {
                $cond['code'] = new MongoRegex('/' . trim($arr_post['code']) . '/i');
            }

             if (isset($arr_post['invoice_status']) && strlen($arr_post['invoice_status']) > 0) {
             	$this->set('invoice_status',$arr_post['invoice_status']);
                $cond['invoice_status'] = new MongoRegex('/' . trim($arr_post['invoice_status']) . '/i');
            }

            if (isset($arr_post['company']) && strlen($arr_post['company']) > 0) {
                $cond['company_name'] = new MongoRegex('/' . trim($arr_post['company']) . '/i');
            }
        } else {
        	$company = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('company_name'));
        	if(isset($company['company_name'])&&$company['company_name']!=''){
                $cond['company_name'] = new MongoRegex('/' . trim($company['company_name']) . '/i');
                $this->set('company_name',$company['company_name']);
        	}
			$cond = array('invoice_status'=>array('$ne'=>'Invoiced'));
        }
		$this->set('arr_invoice_status', $this->Setting->select_option(array('setting_value' => 'salesinvoices_status'), array('option')));
        $this->selectModel('Salesinvoice');
        $arr_salesinvoices = $this->Salesinvoice->select_all(array(
            'arr_where' => $cond,
            'arr_order' => $arr_order,
            'limit' => $limit,
            'skip' => $skip
                // 'arr_field' => array('name', 'is_customer', 'is_employee', 'company_id', 'company_name')
        ));
        $this->set('arr_salesinvoices', $arr_salesinvoices);

        $total_page = $total_record = $total_current = 0;
        if (is_object($arr_salesinvoices)) {
            $total_current = $arr_salesinvoices->count(true);
            $total_record = $arr_salesinvoices->count();
            if ($total_record % $limit != 0) {
                $total_page = floor($total_record / $limit) + 1;
            } else {
                $total_page = $total_record / $limit;
            }
        }
        $this->set('total_current', $total_current);
        $this->set('total_page', $total_page);
        $this->set('total_record', $total_record);

        $this->layout = 'ajax';
    }
    function batch_invoices(){
    	$arr_data = $this->data;
    	if(isset($arr_data['batch_salesinvoices']) && !empty($arr_data['batch_salesinvoices'])){
    		$this->selectModel('Contact');
    		$this->selectModel('Doc');
    		$this->selectModel('DocUse');
    		$company = $comms = array();
    		foreach($arr_data['batch_salesinvoices'] as $id=>$value){
    			if(strlen($id)!=24) continue;
    			$company_id = $this->opm->select_one(array('_id'=>new MongoId($id)),array('company_id','company_name'));
    			if(!isset($company_id['company_id']) || !is_object($company_id['company_id'])) continue;
    			$company[(string)$company_id['company_id']]['company_name'] = $company_id['company_name'];
    			$company[(string)$company_id['company_id']]['invoices'][] = $id;
    		}
    		foreach($company as $company_id=>$value){
    			$contact = $this->Contact->select_one(
    			                           array(
	    			                           'company_id'=>new MongoId($company_id),
	    			                           'position'=>'Account Payable',
	    			                           'email'=>array('$ne'=>'')
	    			                        ),
    										array('email','full_name')
    									);
    			if(!isset($contact['email']) || !filter_var($contact['email'], FILTER_VALIDATE_EMAIL)) continue;
    			$files = array();
    			foreach($value['invoices'] as $salesinvoice_id){
    				$this->opm->save(array('_id'=>new MongoId($salesinvoice_id),'invoice_status'=>'Invoiced','other_comment'=>'Emailed '.date('d M, Y')));
    				$files[] = $this->view_pdf(true,$salesinvoice_id);
    			}
    			$arr_data = array(
    			                  'contact_name'=>$contact['full_name'],
    			                  'contact_id'	=>$contact['_id'],
    			                  'company_name'=>$value['company_name'],
    			                  'company_id'	=>new MongoId($company_id),
    			                  'to'			=>$contact['email'],
    			                  'subject'		=> 'Batch Invoice',
    			                  'template'	=> ''
    			                  );
    			if(!empty($files))
    				$arr_data['attachments'] = $files;
    			$option = array(
								'not_redirect'	=>	true,
								'name'			=>	$arr_data['subject'],
								'content'		=>	$arr_data['template'],
								'comms_status'	=> 	'Draft',
								'contact_name'	=>	$arr_data['contact_name'],
								'contact_id'	=>	$arr_data['contact_id'],
								'company_name'	=>	$arr_data['company_name'],
								'company_id'	=>	$arr_data['company_id'],
								'email'			=>	$arr_data['to'],
								'contact_from'	=> 	$this->opm->user_name(),
								'identity'		=> 	'Auto Send',
								'sign_off'		=> 	'',
								);
    			$comms[] = $comms_id = $this->add_from_module($this->get_id(),'Email',$option);
				foreach($files as $file){
					$arr_save = array(
							'deleted' 			=>	false,
							'no'				=>	$this->Doc->get_auto_code('no'),
							'create_by_module'	=>	'Sales invoice',
							'path'				=>	DS.'upload'.DS.$file,
							'name'				=>	$file,
							'ext'				=> 'pdf',
							'location'			=>	'Salesinvoice',
							'type'				=> 	'application/pdf',
							'description'		=>	'Created at: '.date("h:m a, M d, Y"),
							'create_by'			=>	new MongoId($this->opm->user_id()),
							);
					$this->Doc->save($arr_save);
					$doc_id = $this->Doc->mongo_id_after_save;
					$arr_save = array(
						'deleted'		=> false,
						'controller'	=> 'communications',
						'module'		=> 'Communication',
						'module_no'		=> 	'',
						'doc_id'		=> new MongoId($doc_id),
						'module_id'		=> new MongoId($comms_id),
						'created_by'	=> new MongoId($this->opm->user_id()),
						);
					$this->DocUse->save($arr_save);
				}
    		}
    		if(!empty($comms))
				echo json_encode($comms);
			else
				echo 'No payable contact found.';
    	} else
    		echo 'You must choose at least one Sales invoice.';
    	die;
    }
    function batch_invoices_pdf_only(){
    	$arr_data = $this->data;
    	if(isset($arr_data['batch_salesinvoices']) && !empty($arr_data['batch_salesinvoices'])){
    		$send = false;
    		foreach($arr_data['batch_salesinvoices'] as $id=>$value){
    			if(strlen($id)!=24) continue;
    			$query = $this->opm->select_one(array('_id'=>new MongoId($id)),array('_id'));
    			if(!isset($query['_id'])) continue;
    			$send = true;
    			$this->opm->save(array('_id'=>new MongoId($id),'invoice_status'=>'Invoiced','other_comment'=>'Mail out '.date('d M, Y')));
    		}
    		if($send)
				echo 'ok';
			else
				echo 'Something wrong! Please refresh and try again.';

    	} else
    		echo 'You must choose at least one Sales invoice.';
    	die;
    }
}