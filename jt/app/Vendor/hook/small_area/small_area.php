<?php
App::import('Controller','AppController');
class small_area{
	public $area_limit = 1;
	public $up_price = 50;
	public $cake;
	public function small_area(&$arr_data){
		$this->arr_data = $arr_data;

		$this->cake = new AppController;
		$this->cake->selectModel('Setting');
		if(isset($arr_data['setting'])){
			return $this->setting($arr_data);
		}

		$option_data = $this->cake->Setting->select_option_vl(array('setting_value'=>'small_area'));
		if(isset($option_data['area_limit']) && (float)$option_data['area_limit']>0)
			$this->area_limit = (float)$option_data['area_limit'];
		if(isset($option_data['up_price']) && (float)$option_data['up_price']>0)
			$this->up_price = (float)$option_data['up_price'];

		if($this->check_condition()){
			$this->process_data();
		}
		//Tung: 08/02/2014. Gán ngược lại để thay đổi dữ liệu sau khi đã xử lý
		$arr_data = $this->arr_data;
	}


	public function check_condition(){
		if($this->arr_data['sell_by']=='area' && $this->arr_data['area']>0 && $this->arr_data['area']<$this->area_limit)
			return true;
		return false;
	}


	public function process_data(){
		if(isset($this->arr_data['sell_price'])){
			if(isset($this->arr_data['products_id']) && is_object($this->arr_data['products_id'])){
				$this->cake->selectModel('Product');
				$product = $this->cake->Product->select_one(array('_id'=> new MongoId($this->arr_data['products_id'])),array('sell_price'));
				$this->arr_data['sell_price'] = (isset($product['sell_price']) ? (float)$product['sell_price'] : 0);
			}
			$this->arr_data['sell_price'] += (float)$this->arr_data['sell_price']*$this->up_price/100;
			return true;
		}
		return false;
	}


	private function setting($arr_data){
		if(!isset($arr_data['setting_data']) || empty($arr_data['setting_data']))
			$arr_save = array (
							'name' => 'Pricing Rules',
							'option' =>
								array (
								    array (
								      'value' => 'area_limit',
								      'name' => 1,
								      'deleted' => false,
								    ),
								    array (
								      'value' => 'up_price',
								      'name' => 50,
								      'deleted' => false,
								    ),
								),
							'setting_name' => 'Small area',
							'setting_value' => 'small_area',
							'sort_abc' => false,
							'system_admin' => true,
						);
		else
			$arr_save = $arr_data['setting_data'];
		$this->cake->selectModel('Setting');
		$this->cake->Setting->save($arr_save);
		return true;
	}
}