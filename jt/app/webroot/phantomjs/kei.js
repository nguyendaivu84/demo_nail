var page = require('webpage').create(),
system = require('system'),
address, output, size, orientation;
orientation = 'portrait';

address = system.args[1];
output = system.args[2];
page.viewportSize = { width: 600, height: 600 };
if(system.args.length>4 && system.args[4]!=''){
    orientation = system.args[4];
    if(system.args[4]!='landscape')
        orientation = 'portrait';
}
if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
    size = system.args[3].split('*');
    page.paperSize = size.length === 2 ? { width: size[0], height: size[1], margin: '0.5cm'}
                                       : { format: system.args[3], orientation: orientation, margin: '1cm'
                                            ,footer: {
                                                height: "1cm",
                                                contents: phantom.callback(function(pageNum, numPages) {
                                                    if(pageNum==2)
                                                          page.evaluate(function() {
                                                            return $('thead').append('<tr style="height:1px"></tr>');
                                                          });
                                                    return '<span style="float: right;font-size:8px">Page '+pageNum + '</span>';
                                                })
                                            },
                                        };
}
// if (system.args.length > 5) {
//     page.zoomFactor = system.args[5];
// }
page.zoomFactor = 0.85;
page.open(address, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit();
    } else {
        window.setTimeout(function () {
            page.injectJs('../default/js/jquery-1.10.2.min.js');
            page.render(output);
            console.log('ok');
            phantom.exit();
        }, 200);
    }
});