<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" >
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Nail</title>
	<link href="<?php echo URL; ?>/favicon.ico" type="image/x-icon" rel="icon">
	<link href="<?php echo URL; ?>/favicon.ico" type="image/x-icon" rel="shortcut icon">
	<?php
		echo $this->Html->css('reset');
		echo $this->Html->css('style');

		echo $this->Html->css('jt_screen');
		echo $this->Html->css('jt_vunguyen');

		// echo $this->Html->css('style_v1');

		// jQuery
		//echo $this->Html->script('jquery-1.9.1.min'); // resize, cookie, common
		echo $this->Html->script('jquery-1.10.2.min'); // resize, cookie, common

		echo $this->Html->css('jquery-ui-1.10.3.custom.min');

		//kendo plugin
		// echo $this->Html->css('kendo/kendo.default.min');
		echo $this->Html->css('kendo/kendo.common.min');
		echo $this->Html->css('kendo/kendo.anvy.min');
		// echo $this->Html->script('kendo/kendo.all.min');


		echo $this->Html->script('jquery-ui-1.10.3.custom.min');

		echo $this->Html->script('jquery.combobox');
		echo $this->Html->script('jquery.autosize.min');

		echo $this->Html->script('jquery.simulate');
		echo $this->Html->script('dropzone');
		echo $this->Html->css('dropzone');

		//Scrollbar
		echo $this->Html->css('jquery.mCustomScrollbar');
		echo $this->Html->script('jquery.mCustomScrollbar.concat.min');

	?>

</head>

<body>
	<iframe id='manifest_iframe_hack'
	  style='display: none;'
	  src='<?php echo URL; ?>/kei.html'>
	</iframe>
	<div id="wrapper">
		<?php echo $this->element('header'); ?>
		<?php echo $this->fetch('content'); ?>

		<?php if(!isset($set_footer))
				$set_footer = 'footer';
			  echo $this->element($set_footer);
		?>
	</div>

	<?php
		echo $this->element('loading');
		echo $this->element('window');
		echo $this->element('sql_dump');
		echo $this->Html->script('main.js');
		echo $this->Html->script('kendo/kendo.web');
		echo $this->Js->writeBuffer();
		echo $this->Html->script('jquery.mCustomScrollbar.concat.min');
	?>
	<script type="text/javascript">
	$(function(){
		$.ajax({
			url: '<?php echo URL; ?>/homes/alerts_check',
			success: function(html){
				var json = JSON.parse(html);

				if( json.has_alert == 1){
					set_alert_footer(json);
				}else{
					remove_alert_footer();
				}

				set_interval_check_alert();
			}
		});
	});

	function set_interval_check_alert(){
		var stillAlive = setInterval(function () {
			var now = new Date();
			var sec = now.getSeconds();
			if( sec == 0 ){
				$.ajax({
					url: '<?php echo URL; ?>/homes/alerts_check',
					success: function(html){
						var json = JSON.parse(html);
						if( json.has_alert == 1){
							set_alert_footer(json);
						}else{
							remove_alert_footer();
						}
					}
				});
			}
		}, 1000);
	}

	function set_alert_footer(json){
		var alerts_footer = $("#alerts_footer");
		$("#alerts_title", alerts_footer).addClass("color_ac");
		alerts_footer.addClass("active").attr("onclick", "open_alerts()");

		var str = "";
		if( json.communication > 0 ){
			str += "(" + json.communication + ") ";
		}
		if( json.task > 0 ){
			str += "(" + json.task + ") ";
		}
		$("#alerts_num", alerts_footer).html(": " +str);
	}
	function remove_alert_footer(){
		var alerts_footer = $("#alerts_footer");
		$("#alerts_title", alerts_footer).removeClass("color_ac");
		alerts_footer.removeClass("active").attr("onclick", "return false;");
		$("#alerts_num", alerts_footer).html("");
	}
	function open_alerts(){
		$.ajax({
			url: '<?php echo URL; ?>/homes/alerts_open',
			success: function(html){
				popup_show("Your alerts", html);
			}
		});
	}

	</script>
</body>
</html>