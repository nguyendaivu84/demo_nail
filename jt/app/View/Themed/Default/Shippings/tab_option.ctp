<?php echo $this->element('tab_option'); ?>

<script type="text/javascript">
	$(function(){
		$('#create_sales_invoice').attr('onclick', 'check_condition_shipping()');
	});
	function check_condition_shipping()
	{
		$.ajax({
			url: '<?php echo URL; ?>/shippings/create_sales_invoice',
			type: 'GET',
			success: function(result){
				//console.log(result);
				var result = jQuery.parseJSON(result);
				if(result.status == 'error')
				{
					alerts('Message',result.mess);
				}
				else if(result.status == 'exist')
				{
					confirms('Message',result.mess,function(){window.location.replace(result.url);},function(){return false;});
				}
				else if(result.status='ok')
				{
					window.location.replace(result.url);
				}
			}
		});
		return false;
	}

</script>