<?php
	$total_amount_received = 0;
	$total_allocated = 0;
	$total_unllocated = 0;
	if(!empty($arr_receipt))
	foreach($arr_receipt as $key => $value){
		$total_amount_received += number_format($value['amount_received'],2);
		$total_allocated  += number_format($value['total_allocated'],2);
		$total_unllocated += number_format($value['unallocated'],2);
	}
?>

<div style="margin:0 5% 0 0; width:67%;padding:0; float:right;">
    <input class="input_w2" type="text" style=" width:14.5%; text-align:right;padding:0 1% 0 0; margin:-2px 1.5% 0 0%;float:right;color:#333; " id="sum_amount"  readonly="readonly" value="<?php echo number_format($total_unllocated,2); ?>"  />
    <input class="input_w2" type="text" style=" width:15.5%;text-align:right;padding:0 1% 0 0;margin:-2px 0.5% 0 0%;float:right;color:#333;" id="sum_tax" readonly="readonly" value="<?php echo number_format($total_allocated,2); ?>"  />
    <input class="input_w2" type="text" style=" width:16%; text-align:right; padding:0 1% 0 0;margin:-2px 0.5% 0 0%;float:right;color:#333;" id="sum_sub_total"  readonly="readonly" value="<?php echo number_format($total_amount_received,2);?>" />
     <div class="float_left" style=" width:20%;margin:0;padding:0; text-align:right;float:right;">
        Totals&nbsp;
    </div>
</div>