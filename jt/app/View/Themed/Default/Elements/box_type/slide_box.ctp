<?php
	$block =  $arr_settings['relationship'][$sub_tab]['block'][$blockname]['field'];
	//pr($image_slide);die;
?>

<style type="text/css">
	#box_image{
    padding:0; height:147px;
    vertical-align:middle;
    overflow: hidden;
}
.slide{
	width: 24%;
	max-height:140px;
    display:inline;
    float:left;
    position: absolute;
}
</style>

<div id="box_image">
<span id='next'>next</span>
	<div>
		<?php 
		$i = 0;
		foreach ($image_slide as $key => $value) {
		?>
		    <img onclick='link_img("<?php echo (string)$image_slide_id[$key]; ?>")' src="<?php echo URL.$value;?>" alt="Slide 1" class="slide"  <?php if ($i==0) echo 'id="firstSlide"'; else echo 'style="display:none;"'; ?> />
	    <?php 
	    $i++;
	    } 
	    ?>
	</div>
</div>

<script type="text/javascript">
	function slideShow() {
	    var displayToggled = false;
	    var current1 = $('.slide:visible');
	    var nextSlide = current1.next('.slide');
	    var hideoptions = {
	        "direction": "left",
	        "mode": "hide"
	    };
	    var showoptions = {
	        "direction": "right",
	        "mode": "show"
	    };
	    if (current1.is(':last-child')) {
	        current1.effect("slide", hideoptions, 1000);
	        $("#firstSlide").effect("slide", showoptions, 1000);
	    }
	    else {
	        current1.effect("slide", hideoptions, 1000);
	        nextSlide.effect("slide", showoptions, 1000);
	    }
	};
	function link_img(doc_id) {
		window.location='<?php echo URL.'/docs/entry/';?>'+doc_id;
	}
	setInterval(slideShow, 3000);
	slideShow();
</script>