<?php
      $block =  $arr_settings['relationship'][$sub_tab]['block'][$blockname]['field'];
      //pr($image_slide);die;
?>
<style type="text/css">
      @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300,600);
      #slider {
          position: relative;
          overflow: hidden;
          /*height:100%; /* */
          width: 100% !important; /* */*/
          float: left;
         /* vertical-align:middle;*/
          /*width: 100%;*/
      }
      #slider ul {
          position: relative;
          height: 100%;
          list-style: none;
      }
      #slider ul li {
          position: relative;
          display: block;
          float: left;
          width: 384px;
          height: 140px;
      }
      a.control_prev, a.control_next {
          position: absolute;
          top: 40%;
          z-index: 999;
          display: block;
          padding: 4% 3%;
          width: auto;
          height: auto;
          background: #B6B6B6;
          color: #fff;
          text-decoration: none;
          font-weight: 600;
          font-size: 18px;
          opacity: 0.8;
          cursor: pointer;
      }
      a.control_prev:hover, a.control_next:hover {
          opacity: 1;
          -webkit-transition: all 0.2s ease;
      }
      a.control_prev {
          border-radius: 0 5px 5px 0;
      }
      a.control_next {
          right: 0;
          border-radius: 5px 0 0 5px;
      }
      .slider_option {
          position: relative;
          margin: 0px auto;
          width: 160px;
          font-size: 12px;
      }
      .slide{
          /*width: 100%;*/
          height:100%;
          display: block;
          margin-left: auto;
          margin-right: auto;
      }
</style>


<script type="text/javascript">
      function link_img(doc_id) {
          window.location='<?php echo URL.'/docs/entry/';?>'+doc_id;
      }
      jQuery(document).ready(function ($) {
          $('#checkbox').change(function(){
              if ($('#checkbox').is(':checked')==true){
                    myVar = setInterval(function () {
                    moveRight();
                }, 3000);
              } else {
                  clearInterval(myVar);
              }
          });

          var slideCount = $('#slider ul li').length;
          var slideWidth = $('#slider ul li').width();
          var slideHeight = $('#slider ul li').height();
          var sliderUlWidth = slideCount * slideWidth;

          $('#slider').css({height: slideHeight });

          $('#slider ul').css({ marginLeft: - slideWidth });

          $('#slider ul li:last-child').prependTo('#slider ul');

          function moveLeft() {
              $('#slider ul').animate({
                  left: + slideWidth
              }, 200, function () {
                  $('#slider ul li:last-child').prependTo('#slider ul');
                  $('#slider ul').css('left', '');
              });
          };
          function moveRight() {
              $('#slider ul').animate({
                  left: - slideWidth
              }, 200, function () {
                  $('#slider ul li:first-child').appendTo('#slider ul');
                  $('#slider ul').css('left', '');
              });
          };

          $('a.control_prev').click(function (event) {
              event.preventDefault();
              moveLeft();
          });

          $('a.control_next').click(function (event) {
              event.preventDefault();
              moveRight();
          });

      });
</script>

<?php
    if (count($image_slide) == 0) :
?>
    <div style=" width:100%; height:100%; display:table; text-align:center; font-size:11px; vertical-align:middle; <?php if(isset($block['files']['css'])) echo $block['files']['css']; ?>">
        <?php if(isset($block['files']['name'])) echo '<br /><br /><br /><br />'.$block['files']['name']; ?>
    </div>

<?php
    elseif (count($image_slide) == 1) :
?>
<div id="slider">
      <a href="#" class="control_next">></a>
      <a href="#" class="control_prev"><</a>
      <ul>
          <li>
              <img onclick='link_img("<?php echo (string)$image_slide_id[0]; ?>")' src="<?php echo URL.$image_slide[0];?>" alt="Slide 0" class="slide" />
          </li>
          <li>
              <img onclick='link_img("<?php echo (string)$image_slide_id[0]; ?>")' src="<?php echo URL.$image_slide[0];?>" alt="Slide 0" class="slide" />
          </li>
      </ul>
</div>

<?php
    else :
?>
<div id="slider">
      <a href="#" class="control_next">></a>
      <a href="#" class="control_prev"><</a>
      <ul>
<?php
        foreach ($image_slide as $key => $value) {
?>
          <li>
              <img onclick='link_img("<?php echo (string)$image_slide_id[$key]; ?>")' src="<?php echo URL.$value;?>" alt="Slide <?php echo $key;?>" class="slide" />
          </li>
<?php
        }
?>
      </ul>
</div>
<?php
    endif;
?>


<!-- <div class="slider_option">
      <input type="checkbox" id="checkbox">
      <label for="checkbox">Autoplay Slider</label>
</div>
 -->
