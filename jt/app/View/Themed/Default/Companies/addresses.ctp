<?php
	if(isset($arr_settings['relationship'][$sub_tab]['block']))
	foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){
		echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));
	}
?>
<p class="clear"></p>

<script type="text/javascript">
$(function(){
	//add addresses
	$('#bt_add_addresses').click(function(){
		var ids = $("#mongo_id").val();
		$.ajax({
			url:"<?php echo URL;?>/companies/addresses_add/" + ids,
			timeout: 15000,
			success: function(html){
				console.log(html);
				reload_subtab('addresses');
			}
		});
	})

	//change input
	$("input","#container_addresses").change(function(){
		var names = $(this).attr("name");
		var inval = $(this).val();
		var ids = names.split("_");
		ids = ids[ ids.length - 1];
		names = names.replace("_"+ids,"");
		names = names.replace("cb_","");
		var values = new Object();
		if(names == 'default'){
			if($(this).is(':checked')){
				inval = true;
				$("input[type=checkbox]","#container_addresses").each(function(){
					if($(this).is(":checked")){
						var checkbox_values = new Object();
						checkbox_values['default'] = 0;
						var checkbox_ids = $(this).attr("name");
						checkbox_ids = checkbox_ids.split("_");
						checkbox_ids = checkbox_ids[ checkbox_ids.length - 1];
						if(ids != checkbox_ids)
							save_option("addresses",checkbox_values,checkbox_ids,0,'addresses');
					}
				})
				save_field('addresses_default_key',ids,'');
			}else{
				$(this).prop("checked",true);
				return false;
			}
		}
		values[names] = inval;
		$("#CompanyAddress1").val($("#address_1_"+ids).val());
		$("#CompanyAddress2").val($("#address_2_"+ids).val());
		$("#CompanyAddress3").val($("#address_3_"+ids).val());
		$("#CompanyTownCity").val($("#town_city_"+ids).val());
		$("#CompanyProvinceState").val($("#province_state_"+ids).val());
		$("#CompanyZipPostcode").val($("#zip_postcode_"+ids).val());
		$("#CompanyCountry").val($("#country_"+ids).val());
		save_option("addresses",values,ids,1,'addresses');
	});
})
</script>