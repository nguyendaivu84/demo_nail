<?php
    if(isset($arr_settings['relationship'][$sub_tab]['block']))
    foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){
        echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));
    }
?>
<p class="clear"></p>

<script type="text/javascript">
	$(function(){
		fixHiddenCombobox("container_contacts");
		$('#bt_add_contacts').click(function(){
			var ids = $("#mongo_id").val();
			$.ajax({
				url:"<?php echo URL;?>/companies/contacts_add/" + ids,
				timeout: 15000,
				success: function(html){
					console.log(html);
					reload_subtab('contacts');
				}
			});
		})

		$("input","#block_full_contacts").change(function(){
			var names = $(this).attr("name");
			var inval = $(this).val();
			var ids = names.split("_");
			ids = ids[ids.length - 1];
			names = names.replace("_"+ids,"");
			names = names.replace("cb_","");
			if(names == "contact_default"){
					$('.viewcheck_contact_default').prop('checked',false);
					$(this).prop("checked",true);
					save_data('contact_default_id',ids,'',function(){
				});
				return false;
			}
			save_data(names,inval,ids,'',function(){},"contacts");
		});
	})
</script>
