<?php echo $this->element('js_entry');?>
<?php echo $this->element('js/permission_product_modules');?>
<style>
.jt_ajax_note{
z-index: 59;
}
</style>
<script type="text/javascript">
$(function(){
	$("#code").focus(function(){
		$(this).attr("rel",$(this).val());
	});
	$("#payment_due_date").next("a").click(function(){
		$("#payment_terms").trigger("change");
	});
//reload_subtab('line_entry');
	$("#pst_tax").change(function(){
		var ids = $(this).attr("id");
		var val = $(this).val();
		ids = "mx_"+ids;
		$("#"+ids).html(val);
	});

	$("#country").change(function(){
		if($(this).val()!=40)
			$("#province").html('');
	});

	// Xu ly save, update
	$("form input,form select").change(function() {
		var fixkendo = $(this).attr('class');

		var fieldname = $(this).attr("name");
		var fieldid = $(this).attr("id");
		var fieldtype = $(this).attr("type");
			modulename = 'mongo_id';
		var ids = $("#"+modulename).val();
		var values = $(this).val();
		var func = ''; var titles = new Array();

		if(ids!='')
			func = 'update'; //add,update
		else
			func = 'add';

		//check address
		var check_address = fieldname.split("[");
		if(check_address[0]=='data'){
			save_address(check_address,values,fieldid,function(){
				change_tax_entry();
			});
			return '';
		}
		if(fieldname=='code'){
			$(".jt_ajax_note").html("");
			if( $("#confirms_window" ).attr("id") == undefined ){
				var html = '<div id="password_confirm" >';
					html +=	   '<div class="jt_box" style=" width:100%;">';
					html +=		  '<div class="jt_box_line"><div class=" jt_box_label " style=" width:25%;"></div><div id="alert_message"></div></div>';
					html +=	      '<div class="jt_box_line">';
					html +=	         '<div class=" jt_box_label " style=" width:25%;height: 61px">Password</div>';
					html +=	         '<div class="jt_box_field " style=" width:71%"><input name="password" id="password" class="input_1 float_left" type="password" value=""></div><input style="margin-top:2%" type="button" class="jt_confirms_window_cancel" id="confirms_cancel" value=" Cancel " /><input style="margin-top:2%" type="button" class="jt_confirms_window_ok" value=" Ok " id="confirms_ok" />';
					html +=	      '</div>';
					html +=	      '</div>';
					html +=	   '</div>';
					html +=	'</div>';
				$('<div id="confirms_window" style="width: 99%; padding: 0px; overflow: auto;">'+html+'</div>').appendTo("body");
			}
			var confirms_window = $("#confirms_window");
			confirms_window.kendoWindow({
				width: "355px",
				height: "85px",
				title: "Enter password",
				visible: false,
				activate: function(){
				  $('#password').focus();
				}
			});
			$("#password").val("");
			confirms_window.data("kendoWindow").center();
			confirms_window.data("kendoWindow").open();
			$("#confirms_ok").unbind("click");
			$("#confirms_ok").click(function() {
				$("#alert_message").html("");
				if( $("#password").val().trim()==''  ){
					$("#alert_message").html('<span style="margin-left: 5px; color:red; font-weight: 700">Password must not be empty.</span');
					$("#password").focus();
					return false;
				}
				var values = {};
				values["password"] = $("#password").val();
				values["value"] = $("#code").val();
				$.ajax({
					url: '<?php echo URL.'/'.$controller;?>/ajax_save',
					type:"POST",
					data: {field:fieldname,value:values,func:func,ids:ids},
					success: function(text_return){
						if(text_return=='wrong_pass'){
							ajax_note("Wrong password.");
							$("#code").val($("#code").attr("rel"));
						} else
							ajax_note("Saving...Saved !");
					}
				});
		       	confirms_window.data("kendoWindow").close();
			});
			$('#confirms_cancel').click(function() {
				$("#alert_message").html("");
				$("#code").val($("#code").attr("rel"));
		       	confirms_window.data("kendoWindow").close();
		    });
		    return false;
		}

		//check invoice_date < payment_due_date
		if(fieldname=='invoice_date' || fieldname=='payment_due_date'){
			var invoice_date = convert_date_to_num($('#invoice_date').val());
			var payment_due_date = convert_date_to_num($('#payment_due_date').val());
			if(invoice_date>payment_due_date){
				alerts('Message','<?php msg('DUE_DATE_AND_QUOTE_DATE');?>');
				$(this).css('color','#f00');
				return '';
			}else{
				$(this).css('color','#545353');
			}
		}

		var taxval;
		if(fieldname=='tax'){
			arrva = values.split('%');
			taxval = arrva[0];
			var arrvalue =  {"taxper":taxval};
			update_all_option('products',arrvalue,function(){
				reload_subtab('line_entry');
			});
			values = $('#'+fieldid+'Id').val();
		}

		if(fieldtype=='checkbox'){
			if($(this).is(':checked'))
				values = 1;
			else
				values = 0;
		}
			$(".jt_ajax_note").html("Saving...       ");

			$.ajax({
				url: '<?php echo URL.'/'.$controller;?>/ajax_save',
				type:"POST",
				data: {field:fieldname,value:values,func:func,ids:ids},
				success: function(text_return){ //alert(text_return);
					text_return = text_return.split("||");
						 if (text_return == "email_not_valid"){
								$("#email").addClass('error_input');
								ajax_note('Email not valid, please check email field!');
						 }else{
							$("#email").removeClass('error_input');
							$("#"+modulename).val(text_return[0]);
							// change tittle, thay đổi tiêu đề của items
							<?php foreach($arr_settings['title_field'] as $ks=>$vls){?>
								titles[<?php echo $ks;?>] = '<?php echo $vls;?>';
							<?php } ?>
							if(titles.indexOf(fieldname)!=-1){
								$("#md_"+fieldname).html(values);
								$(".md_center").html("-");
							}
							ajax_note("Saving...Saved !");

							// if status
							if(fieldname=='invoice_status' || fieldname=='payment_terms' || fieldname=='invoice_date')
                                location.reload();

                            if(fieldname=='name')
								$("form#other_record input#name").val(values);
							if(fieldname=='tax')
								save_field('taxval',taxval,'');

							if(fieldname=='company_name')
								change_tax_entry();
						}
				}
			});

	});

	$(".jt_ajax_note").html('');

	//View and cutom Option value
	$( document ).delegate(".view_option","click",function(){
		view_product_option($(this).attr('rel'));
	});

	//RFQ's List
	$('#bt_add_rfqs, .entry_menu_add_rfqs ').click(function(){
		var d = new Date();
		var itemid = $('#itemid').val();
		var subitems = $('#subitems').val();
		var employee_id = $('#employee_id').val();
		var employee_name = $('#employee_name').val();
		var quote_code = $('#quote_code').val();
		var sumrfq = parseInt($('#sumrfq').val()); sumrfq = sumrfq+1;
		var dates = parseInt(d.getTime());
			dates = Math.round(dates/1000);
		var datas = {
			'rfq_no' : quote_code+'/'+sumrfq,
			'rfq_code' : subitems,
			'rfq_date' : dates,
			'employee_id' : employee_id,
			'employee_name' : employee_name
		};
		save_option('rfqs',datas,'',0,'rfqs','add',function(sms){
			window.location.assign("<?php echo URL;?>/invoices/rfqs_entry/"+itemid+'/'+(sumrfq-1));
		});

	});


});

// Hàm dùng cho module Quotation :=========================================
/*
	after_choose_companies(ids,names,keys)
	after_choose_contacts(ids,names,keys)
	after_choose_jobs(ids,names,keys)
	save_address(arr,values,fieldid)
	save_address_pr(keys)
*/



/**
* Thay đổi tax entry khi province thay đổi
*/
function change_tax_entry(){

	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/change_tax_entry',
		dataType: "json",
		type:"POST",
		success: function(jsondata){
			console.log('tesing:');
			console.log(jsondata);
			$('#tax').val(jsondata['texttax']);
			$('#taxId').val(jsondata['keytax']);
			reload_subtab('line_entry');
			/*arrva = values.split('%');
			var arrvalue =  {"taxper":arrva[0]};
			update_all_option('products',arrvalue,function(){
				reload_subtab('line_entry');
			});*/
		}
	});
}




//xử lý sau khi chọn company
//cách xử lý mới
//dùng hàm save_data
function after_choose_companies(ids,names,keys){
	if(keys=='company_name'){
		$("#company_id").val(ids);
		$("#company_name").val(names);
		$("#md_company_name").html(names);
		$(".k-window").fadeOut('slow');
		$(".link_to_company_name").addClass('jt_link_on');
		save_data('company_name',names,'',ids,function(arr_ret){

			$(".link_to_contact_name").removeClass('jt_link_on');
			if(arr_ret['contact_id']!='')
				$(".link_to_contact_name").addClass('jt_link_on');
			$("#md_contact_name").html('');
			if(arr_ret['contact_name']!='')
				$("#md_contact_name").html(arr_ret['contact_name']);

			window_popup('contacts', 'Specify Contact','contact_name','click_open_window_contactscontact_name',get_para_contact(),'force_re_install');
			reload_address('invoice_');
			reload_address('shipping_');
		});

	}
}

// xử lý sau khi chọn contact
//cách xử lý theo 2 hàm độc lập:
//Lấy data(get_data_form_module) + Save data (save_muti_field)

function after_choose_contacts(ids,names,keys){
	var mongoid,func;
	mongoid = $("#mongo_id").val();
	if(mongoid!='')
		func = 'update';
	else
		func = 'add';
	$(".k-window").fadeOut('slow');

	if(keys=='contact_name'){
		$("#contact_id").val(ids);
		$("#contact_name").val(names);
		$("#md_contact_name").html(names);
		$(".k-window").fadeOut('slow');
		$(".link_to_contact_name").addClass('jt_link_on');
		save_data('contact_name',names,'',ids,function(arr_ret){

			/*$(".link_to_contact_name").removeClass('jt_link_on');
			if(arr_ret['contact_id']!='')
				$(".link_to_contact_name").addClass('jt_link_on');
			$("#md_contact_name").html('');
			if(arr_ret['contact_name']!='')
				$("#md_contact_name").html(arr_ret['contact_name']);

			window_popup('contacts', 'Specify Contact','contact_name','click_open_window_contactscontact_name',get_para_contact(),'force_re_install');*/
			//reload_address('invoice_');
			//reload_address('shipping_');
		});

	}
	else if(keys=='our_rep'){
		$(".link_to_our_rep").attr("onclick", "window.location.assign('/jobtraq/contacts/entry/"+ids+"')");
		$("#our_rep_id").val(ids);
		$("#our_rep").val(names);
		var id_invoice = $("#mongo_id").val();
		$("#md_our_rep").html(names);
		$(".link_to_our_rep").addClass('jt_link_on');
		$.ajax({
			url: '<?php echo URL.'/'.$controller;?>/ajax_save',
			type:"POST",
			data: {field:'our_rep_id',value:ids,func:func,ids:mongoid},
			success: function(text_return){
				text_return = text_return.split("||");

				//save_field('our_rep',names,text_return[0]);
				save_data('our_rep',names,'',id_invoice,function(arr_ret){});
			}
		});

	}else if(keys=='our_csr'){
		$(".link_to_our_csr").attr("onclick", "window.location.assign('/jobtraq/contacts/entry/"+ids+"')");
		$("#our_csr_id").val(ids);
		$("#our_csr").val(names);
		$("#md_our_csr").html(names);
		$(".link_to_our_csr").addClass('jt_link_on');
		$.ajax({
			url: '<?php echo URL.'/'.$controller;?>/ajax_save',
			type:"POST",
			data: {field:'our_csr_id',value:ids,func:func,ids:mongoid},
			success: function(text_return){
				text_return = text_return.split("||");
				save_field('our_csr',names,text_return[0]);
			}
		});
	}
}

// xử lý sau khi chọn job,
function after_choose_jobs(ids,names,keys){
	if(keys=='job_name'){
		var module_from = 'Job';
		var arr = {
					"_id"	:"job_id",
					"name"	:"job_name",
					"no"	:"job_number"
				 }; //danh sách các field cần nhận về từ module jobs, và fields cần lưu
		$(".k-window").fadeOut('slow');
		$(".link_to_job_name").addClass('jt_link_on');
		save_data_form_to(module_from,ids,arr);
	}
}

// xử lý sau khi chọn job,
function after_choose_salesorders(ids,names,keys){
	if(keys=='salesorder_name'){
		var module_from = 'Salesorder';
		var arr = {
					"_id"	:"salesorder_id",
					"name":"salesorder_name",
					"code"	:"salesorder_number"
				 }; //danh sách các field cần nhận về từ module jobs, và fields cần lưu
		$(".k-window").fadeOut('slow');
		$(".link_to_salesorder_name").addClass('jt_link_on');
		save_data_form_to(module_from,ids,arr);
		reload_subtab('line_entry');
	}
}


function save_address(arr,values,fieldid,handleData){
	var	keys = arr[1].replace("]","");
	var keyups = keys.charAt(0).toUpperCase() + keys.slice(1);
	var opname = keys + "_address";
	var address_field = arr[2].replace("]","");
	var datas = new Object();
	if(address_field!='invoice_country' && address_field!='shipping_country' && address_field=='invoice_province_state' && address_field=='shipping_province_state'){
		datas[address_field] = values;

	//luu province
	}else if(address_field=='invoice_province_state' || address_field=='shipping_province_state'){
		var vtemp = $("#"+fieldid+'Id').val();
			datas[address_field] = $("#"+fieldid).val();//luu gia tri custom cua province
			datas[address_field+'_id'] = vtemp;
		$("#"+keyups+'ProvinceState').css('border','none');
		$("#"+keyups+'ProvinceState').css('border-bottom','1px solid #dddddd');
		//$("#"+keyups+'ProvinceState').focus();

	//luu country
	}else{
		vtemp = $("#"+fieldid+'Id').val();
		datas[address_field] = $("#"+fieldid).val();
		datas[address_field+'_id'] = vtemp;
		if(vtemp=='CA' || vtemp=='US'){
			$("#"+keyups+'ProvinceState').css('border','1px solid #f00');
			$("#"+keyups+'ProvinceState').focus();
		}else{
			$("#"+keyups+'ProvinceState').css('border','none');
			$("#"+keyups+'ProvinceState').css('border-bottom','1px solid #dddddd');
		}
	}
	var olds = $("#"+opname).val();
	if(olds!=''){
		olds = '';
		idas = '0';
	}else{
		olds = 'add';
		idas = '';
		$("#"+opname).val(values+',');
	}
	save_option(opname,datas,idas,0,'',olds,function(arr_return){
		if(handleData!=undefined)
			handleData(arr_return);
	});
	ajax_note("Saving...Saved !");
}


function save_address_pr(keys){
	var keyups = keys.charAt(0).toUpperCase() + keys.slice(1);
	var fieldid = keyups+'ProvinceState';
	var values = $("#"+fieldid).val();
	var arr = new Array();
	arr[1] = keys+']';
	arr[2] = keys+'_province_state]';
	save_address(arr,values,fieldid);

	$("#"+keyups+'ProvinceState').css('border','none');
	$("#"+keyups+'ProvinceState').css('border-bottom','1px solid #dddddd');
}

function view_product_option(proid){
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/view_product_option',
		type:"POST",
		data: {proid:proid},
		success: function(text_return){
			build_popup(text_return,'Option of product');
		}
	});
}


function build_popup(html,title,h,w,wfocus){
	if( $("#build_popup_window").attr("id") == undefined ){
		$('<div id="build_popup_window"><div class="popup_window_cont"></div></div>').appendTo("body");
	}
	if(title==undefined)
		var title = 'Message';
	if(h==undefined)
		var h = '250px';
	if(w==undefined)
		var w = '400px';
	if(wfocus==undefined)
		var wfocus = 'build_popup_window';
	if(html==undefined)
		var html = '';
	var build_popup_window = $("#build_popup_window");
		build_popup_window.kendoWindow({
			width: w,
			height: h,
			title: title,
			visible: false,
			activate: function(){
			  $('#'+wfocus).focus();
			}
		});
	//setup html
	$(".popup_window_cont").html(html);
	//show popup
	build_popup_window.data("kendoWindow").center();
	build_popup_window.data("kendoWindow").open();
}


function after_choose_addresses(ids,names,keys){
		var address = new Object();
		var directs = ['name','address_1','address_2','address_3','town_city','province_state','province_state_id','zip_postcode','country'];
		for(var n in directs){
			address[keys+'_'+directs[n]] = $("#window_popup_addresses_"+names+"_"+directs[n]+'_'+ids+keys).val();
		}
		address[keys+'_country_id'] = parseInt($("#window_popup_addresses_"+names+"_country_id_"+ids+keys).val());
		address[keys+'_default'] = true;
		address['deleted'] = false;

		var address_0={'0':address};
		var invoice_address={'addresses':address_0};
		var jsonString = JSON.stringify(invoice_address);
		var arr_field = {'addresses':keys+'_address'};
		$(".k-window").fadeOut();
		save_muti_field(arr_field,jsonString,'',function(arr_ret){
			ajax_note('Saved.');
			address = arr_ret[keys+'_address'];
			address = address[0];
			for(var i in address){
				$("#"+ChangeFormatId(i)).val(address[i]);
			}
			//save tax
			var ShippingAddId = $("#ShippingProvinceStateId").val()
			if(keys=='shipping' || (keys=='invoice' && ShippingAddId=='')){
				var taxid = address[keys+'_province_state_id'];
				var allListElements = $( 'li[value="'+taxid+'"]' );
				var html = $("#tax").parent().find(allListElements);
				//console.log(taxid);
				//console.log(html);
				var tax = html[0].innerHTML;
				var taxval = tax.split("%");
				taxval = taxval[0];
				$('#tax').val(tax);
				$('#taxId').val(taxid);
				$('#tax').change();
			}
		});
}

</script>