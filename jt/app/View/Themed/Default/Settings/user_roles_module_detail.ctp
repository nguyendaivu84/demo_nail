<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name"><?php
				echo translate('Modules: ');
				if (isset($arr_persmiss['name']))
					echo $arr_persmiss['name'];
				?>
			</h4>
		</span>
	</span>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:18%">Page</li>
		<li class="hg_padd center_text" style="width:12%">Name</li>
		<li class="hg_padd center_text" style="width:50%">Description</li>
		<li class="hg_padd center_text no_border" style="width:15%">Select</li>
	</ul>

	<div class="container_same_category" style="height: 150px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = 0;

		if(!function_exists('cmp')){
			function cmp($a, $b)
			{
				return strcmp($a["name"], $b["name"]);
			}
		}

		if( isset($arr_persmiss['permission']) ){
			foreach ($arr_persmiss['permission'] as $roles) {
				ksort($roles);
				foreach ($roles as $key_page => $page) {
					usort($page, "cmp");
					foreach ($page as $key => $value) {
						$count += 1;
						$i = 3 - $i;

						if( isset($value['deleted']) && $value['deleted'] )continue;
			?>

				<ul class="ul_mag clear bg<?php echo $i; ?>">

					<li class="hg_padd" style="width:18%"><?php echo $key_page; ?></li>
					<li class="hg_padd" style="width:12%"><?php echo $value['name']; ?></li>
					<li class="hg_padd" style="width:50%"><?php if(isset($value['description']))echo $value['description']; ?></li>
					<li class="hg_padd center_text no_border" style="width:15%">

						<?php $value_role = $arr_persmiss['controller'].'_@_'.$key_page.'_@_'.$value['codekey']; ?>
						<?php
							// Kiem tra toan quyen tren Module
							$check_role = false;
							if( isset($arr_user_roles['all']) || (isset($arr_user_roles['roles']) && ( isset($arr_user_roles['roles']['all']) || isset($arr_user_roles['roles'][$value_role]) ) ) ){
								$check_role = true;
							}
						?>

						<input name="contact_id" type="hidden" value="<?php echo $contact_id; ?>">
						<input name="permission_id" type="hidden" value="<?php echo $permission_id; ?>">
						<input name="permission_path" type="hidden" value="<?php echo $value_role; ?>">
						<input name="controller" type="hidden" value="<?php echo $arr_persmiss['controller']; ?>">

						<input name="ownership" type="checkbox" <?php if( $check_role ){ ?>disabled="disabled"<?php } ?> id="checkbox_<?php echo $key_page.'_'.$value['codekey']; ?>" rel="<?php echo $key_page; ?>" value="all" <?php if( $check_role || isset($arr_user_roles[$value_role]) ){ ?>checked="checked"<?php } ?>>
					</li>
				</ul>
		<?php 		}
				}
			}
		} ?>

		<?php
			if ($count < 8) {
				$count = 6 - $count;
				for ($j = 0; $j < $count; $j++) {
					$i = 3 - $i;
					?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
					<?php
				}
			}
		?>
	</div>
	<span class="title_block bo_ra2">
		<span class="float_left bt_block"></span>
	</span>
</div>

<!-- OPTION -->
<div class="tab_1 full_width" style="margin-top:1%">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name"><?php echo translate('Option'); ?>
			</h4>
		</span>
	</span>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:82%">Option Name</li>
		<li class="hg_padd center_text no_border" style="width:15%">Select</li>
	</ul>
	<div class="container_same_category" style="height: 223px;overflow-y: auto">
		<?php $count = 0;
			foreach($arr_persmiss['option_list'] as $roles){
				foreach($roles as $key=>$value){
					foreach($value as $val){
						if( isset($val['deleted']) && $val['deleted'] )continue;
						$count += 1;
						$i = 3 - $i;
						// $option_name = ucfirst(str_replace('_', ' ', $key));
		?>
			<ul class="ul_mag clear bg<?php echo $i; ?>">
				<li class="hg_padd" style="width:82%;cursor:default;"><?php echo $val['name']; ?></li>
				<li class="hg_padd center_text no_border" style="width:15%">
					<?php $value_role = $arr_persmiss['controller'].'_@_options_@_'.$val['codekey']; ?>
					<?php if( !isset($val['permission']) ){ ?>
						<?php
							// Kiem tra toan quyen tren Module
							$check_role = false;
							if( isset($arr_user_roles['all']) || (isset($arr_user_roles['roles']) && ( isset($arr_user_roles['roles']['all']) || isset($arr_user_roles['roles'][$value_role]) ) ) ){
								$check_role = true;
							}
						?>
						<input name="contact_id" type="hidden" value="<?php echo $contact_id; ?>">
						<input name="permission_path" type="hidden" value="<?php echo $value_role; ?>">
						<input name="controller" type="hidden" value="<?php echo $arr_persmiss['controller']; ?>">
						<input type="checkbox" class="option_child role_module_detail" <?php if( $check_role ){ ?>disabled="disabled"<?php } ?> value="all" <?php if( $check_role || isset($arr_user_roles[$value_role])   ){ ?>checked="checked"<?php } ?>>
					<?php }else{
							$type='and';
							$permission = array($val['permission']);
							if(strpos($val['permission'],'||')!==false){
								$type="or";
								$permission = explode('||', $val['permission']);
							} else if (strpos($val['permission'],'&&')!==false){
								$type="and";
								$permission = explode('&&', $val['permission']);
							}
							$checked = false;
							foreach ($permission as $value_check) {
								if( isset($arr_user_roles['roles'][$value_role]) || isset($arr_user_roles[$value_check]) )
									$checked = true;
							}
					 ?>
						<input type="checkbox" class="option_child role_module_detail" value="all" disabled="disabled" <?php if( $checked ){ ?>checked="checked"<?php } ?>>
					<?php } ?>

				</li>
			</ul>
		<?php
					}
				}
			}
		?>
		<?php
			$count++;

		if ($count < 11) {
			$count = 10 - $count;
			for ($j = 0; $j < $count; $j++) {
				$i = 3 - $i;
				?>
				<ul class="ul_mag clear bg<?php echo $i; ?>">
				</ul>
				<?php
			}
		}
	?>
	</div>
	<span class="title_block bo_ra2">
		<span class="float_left bt_block"></span>
	</span>
</div>

<script type="text/javascript">
	$(function(){
		$("#option_parent").click(function(){
			if($(this).is(":checked"))
				$(".option_child").prop({checked: true});
			else
				$(".option_child").prop({checked: false});
			option_list_permission($(this));
		});
	})
	function option_list_permission(object){
		var li = object.parents("li");
		$.ajax({
			url: '<?php echo URL; ?>/settings/user_roles_module_detail_option_list/',
			timeout: 15000,
			type:"post",
			data: $(":input, :checkbox", li).serialize(),
			success: function(html){
				if( html != "ok" )
					alerts("Message: ", html);
			}
		});
	}
</script>