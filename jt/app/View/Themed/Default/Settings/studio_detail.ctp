<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Droppable - Simple photo manager</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <style>
  .field_header{
		width: 97%;
		padding-left: 3%;
		height: 25px;
		float: left;
		display: table;
		background: #797979;
		color: white;
		line-height: 25px;
		font-size: 14px;
	}
	.field_list{
		width: 100%;
		min-height: 100px;
		float: left;
		display: table;
		background: #ccc;
		padding-bottom: 10px;
		list-style:none;
	}
	.field_item,.group_item,.panel_item{
		width:12%;
		margin: 1% 1% 0.5% 2%;
		padding: 0.5%;
		float: left;
		background: #eee;
		height:18px;
		overflow:hidden;
	}
	.field_item_text{
		width:100%;
		height:16px;
		float:left;
		position:relative;
		overflow:hidden;
		z-index:1;
	}
	.field_item_delete{
		background:#fff;
		border:1px solid #ddd;
		float:right;
		cursor:pointer;
		padding:2px 6px 2px 6px;
		margin:-3px 0 0 10%;
		line-height:16px;
		font-size:14px;
		font-family: Verdana, Geneva, sans-serif;
		color:#ddd;
		position: absolute;
		z-index:2;
	}
	.field_item_delete:hover{
		color:#444;
	}
	.group_div{
		margin: 1% 1% 0.5% 2%;
		padding: 0.5%;
		float: left;
		background: #eee;
		height:100px;
		float: left;
		display: table;
		list-style:none;
	}
	.panel_div{
		width:30%;
		margin: 1% 1% 0.5% 2%;
		padding: 0.5%;
		float: left;
		background: #fff;
		height:100px;
		float: left;
		display: table;
		list-style:none;
	}
	.layout_highlight{
		background:#FFC;
	}

	.label{
    font-weight: bold;
    font-size: 12px;
    cursor: default;
	}
	#notifyTop {
	    display: none;
	    position: fixed;
	    top: 101px;
	    left: 30%;
	    z-index: 9999;
	    background-color: #852020;
	    color: #FFF;
	    border-radius: 3px;
	    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
	    padding: 6px 28px 6px 10px;
	    font-weight: bold;
	    width: 40%;
	    text-align: center;
	    overflow: hidden;
	    line-height: 1.3;
	}
	#notifyTop .flash_message{
	    margin-right: 40px;
	}
	#notifyTop #notifyTopsub{
	    position: absolute;
	    right: 4px;
	    top: 6px;
	    text-decoration: underline;
	}#notifyTop #notifyTopsub a:hover{
	    color: #D1B9B9;
	}
  </style>


  <script>
  $(function() {

	 // there's the gallery and the trash
    var $field_list = $("#field_list");
	var $panel_div = $(".panel_div");
	var $group_div = $(".group_div");
	var $layout_struct = $("#layout_struct");
	// let the gallery items be draggable
    $( ".field_item,.panel_item,.group_item", $field_list ).draggable({
      cancel: ".field_item_delete", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });

	$panel_div.droppable({
      accept: ".field_item",
      activeClass: "layout_highlight",
      drop: function( event, ui ) {
        MoveField( ui.draggable,$(this));
      }
    });


	$group_div.droppable({
      accept: ".panel_item",
      activeClass: "layout_highlight",
      drop: function( event, ui ) {
        MovePanel( ui.draggable,$(this) );
      }
    });


	$layout_struct.droppable({
      accept: ".group_item",
      activeClass: "layout_highlight",
      drop: function( event, ui ) {
        MoveGroup( ui.draggable);
      }
    });

    // image deletion function
    function MoveField( $item,$layout) {
      $item.fadeOut(function(){
		  //console.log($item);
		  var $list = $( "ul", $layout ).length ?
          $( "ul", $layout ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $layout);

		  $item.appendTo($list).fadeIn(function(){
		  });
      });
    }

	function MovePanel( $item,$layout) {
      $item.fadeOut(function(){
		  //console.log($item);
		  var $list = $( "ul", $layout ).length ?
          $( "ul", $layout ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $layout);
		  $item.find( ".field_item_delete" ).remove();
		  $item.appendTo($list).fadeIn(function(){
			  $item.addClass("panel_div");
			  $item.attr("id","panel_div_100");
			  $item.text('');
			  $item.removeClass("panel_item");
			  $item.animate({ width: "248px" })
		  });
      });
    }


	function MoveGroup( $item ) {
      $item.fadeOut(function(){

      });
    }

  });
  </script>
</head>
<body>


<div class="field_header">Field list</div>
<div>
    <ul class="field_list" id="field_list">
       <!--  <li class="group_item">
            <input type="hidden" value="this_is_field_name this_is_field_name this_is_field_name" name="test_<?php echo $i;?>" />
            <span class="field_item_delete">x</span>
            <span class="field_item_text">Group item</span>
        </li>
        <li class="panel_item">
            <input type="hidden" value="this_is_field_name this_is_field_name this_is_field_name" name="test_<?php echo $i;?>" />
            <span class="field_item_delete">x</span>
            <span class="field_item_text">Panel item</span>
        </li> -->
       <?php foreach($data_field_name as $key =>$value){
       		foreach($value as $k =>$v){?>
            <li class="field_item">
                <input type="hidden" value="this_is_field_name this_is_field_name this_is_field_name" name="test_<?php echo $k;?>" />
                <span class="field_item_delete">x</span>
                <span class="field_item_text"><?php echo $v;?></span>
            </li>
        <?php } } ?>
    </ul>
</div>
<div id="notifyTop"></div>
	<form action="<?php echo URL ?>/settings/save_layout"  enctype="multipart/form-data" method="post" id="save_layout_form">
	    <div class="field_header" style="margin-top:20px;">Layout</div>
	    <div>

	        <ul class="field_list" id="layout_struct">
	            <?php foreach($arr_setting['group'] as $key => $v_group){ ?>
	                <li>
	                    <ul class="group_div" id="<?php echo $key;?>" style=" width:<?php if($key=='group_1') echo '20';else echo '60';?>%;">
	                        <?php foreach($v_group as $panel => $title){ ?>
	                            <li>
	                                <ul class="panel_div" id="<?php echo $panel;?>">
	                            		 <?php $m=0; foreach($arr_setting['field'][$panel] as $field => $value){ ?>

	                            		 	<li class="field_item">
								                <input type="hidden" value="<?php echo $field;?>" name="data[<?php echo $panel;?>][<?php echo $m;?>]" />
								                <span class="field_item_delete">x</span>
								                <span class="field_item_text"><?php echo isset($value['name'])?$value['name']:'';?></span>
								            </li>
	                            		 <?php $m++; } ?>
	                                </ul>
	                            </li>
	                        <?php } ?>
	                    </ul>
	                </li>
	            <?php } ?>
	        </ul>
	    </div>
	    <input type="hidden" name="ok" value="ok"/>
	    <input type="button" id="save_layout" name="save_layout" value="Save layout"/>
	</form>

</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#save_layout").click(function(){
			$("notifyTop").html('Processing...');
			var empty = false;
			if(!empty){
				$.ajax({
					url:  '<?php echo URL ?>/settings/save_layout',
					type: 'POST',
					data: $("#save_layout_form").serialize(),
					success: function(result){
						if(result!='ok')
							alerts('Message',result);
						else{
							notifyTop('Saved!');
							$("input[type=hidden]","save_layout_form").each(function(){
								$(this).val('');
							});
						}
					}
				});
			}
		});
	});
	function notifyTop(html){
    if($.trim(html) != "" )
        $("#notifyTop").html(html).fadeIn(600).append('<div id="notifyTopsub"><a href="javascript:void(0)" onclick="$(\'#notifyTop\').fadeOut()">Hide</a></div>');
	}
</script>