<style type="text/css">
#confirms_window .jt_confirms_window_ok{
	width: 15%;
	height: 18%;
	margin-top: 14%;
	margin-left: 80%;
}
.changePadding ul li{
	padding-left: 4px !important;
}
</style>
<div class="clear_percent">
	<div class="clear_percent_19 float_left" style="width:19%">
		<div class="tab_1 full_width changePadding">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo $roles['name']; ?></h4></span>
			</span>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd center_text no_border"><?php echo translate('List of modules'); ?></span></li>
			</ul>
			<div id="roles_module_height" class="container_same_category" style="height: 449px;overflow-y: auto">
				<ul class="find_list setup_menu">
				<?php $i = 1;$count = 0;
				foreach ($arr_permissions as $key => $value) { $count += 1; ?>
					<li>
						<a class="clickfirst" href="javascript:void(0)" onclick="settings_roles_module(this, '<?php echo $value['_id']; ?>')"><?php echo $value['name']; ?></a>
					</li>

				<?php $i = 3 - $i;
				}
				echo '</ul>';

				if ($count < 20) {
				$count = 20 - $count;
				for ($j = 0; $j < $count; $j++) {
					$i = 3 - $i;
					?>
					<ul class="find_list setup_menu">
					</ul>
					<?php
				}
			}
			?>
			</div>
			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
	<div class="clear_percent_9_arrow float_left">
		<div class="full_width box_arrow">
			<span class="icon_emp" style="cursor:default"></span>
		</div>
	</div>
	<div class="clear_percent_11 float_left" id="roles_module_detail" style="width: 79%;">
		<!-- Detail -->
	</div>
</div>

<style type="text/css">
#roles_module_height ul:hover, #roles_module_height ul:hover input{
	background-color: #B8B8B8;
}
</style>

<script type="text/javascript">

	$(function(){

		$('.container_same_category').mCustomScrollbar({
			scrollButtons:{
				enable:false
			}
		});

		$(".clickfirst:first", "#roles_module_height").click();
	});

	// click cot 2
	function settings_roles_module(object, id){

		$("#roles_module_height a").removeClass("active").parents("li").removeClass("active");
		$(object).addClass("active").parents("li").addClass("active");

		var ul = $(object).parents("ul");
		$(ul).attr("style", "background-color: #B8B8B8;");
		$("input", ul).attr("style", "background-color: #B8B8B8;");

		$.ajax({
			url: '<?php echo URL; ?>/settings/roles_module_detail/<?php echo $role_id; ?>/' + id,
			timeout: 15000,
			success: function(html){
				$("div#roles_module_detail").html(html);
				$(".container_same_category", "div#roles_module_detail").mCustomScrollbar({
					scrollButtons:{
						enable:false
					}
				});
				roles_module_detail_input_change();
			}
		});
	}

	// click checkbox chon quyen o cot 3
	function roles_module_detail_input_change(){
		$(":input", "#roles_module_detail").change(function() {
			if($(this).attr('id')=="option_parent") return false;
			roles_module_detail_input_change_run_ajax($(this), "true");
		});
	};

	// click checkall cot 3 tren cung`
	function roles_module_detail_checkall(object){
		$("#option_content").slideDown();
		$(".role_module_detail").each(function(){
			if( $(object).prop("checked") != $(this).prop("checked") ){
				$(this).prop("checked", $(object).prop("checked"));
				roles_module_detail_input_change_run_ajax($(this), "false");
			}
		});
	}

	function roles_module_detail_input_change_run_ajax(object, user_change){
		if( object.attr("id") == "checkall" )return false; // bỏ input checkbox all
		var ul = object.parents("ul");
		$.ajax({
			url: '<?php echo URL; ?>/settings/roles_module_detail_auto_save/' + object.prop("checked"),
			timeout: 15000,
			type:"post",
			data: $(":input, :checkbox", ul).serialize(),
			success: function(html){

				if( html != "ok" ){
					alerts("Message: ", html);
				}else{
					// nếu đang là toàn quyền full
					$("#Role_<?php echo $role_id; ?>").prop("checked", false);

					if( user_change == "true" ){
						// nếu stick vào delete thì có luôn quyền view và edit
						if( object.attr("id") == ("checkbox_" + object.attr("rel") + "_delete" ) ){
							var edit = $("#checkbox_" + object.attr("rel") + "_edit" );
							if(edit.attr("id") != undefined && !edit.prop("checked") ){
								edit.prop("checked", true);
								roles_module_detail_input_change_run_ajax(edit);
							}
							var view = $("#checkbox_" + object.attr("rel") + "_view" );
							if(view.attr("id") != undefined && !view.prop("checked") ){
								view.prop("checked", true);
								roles_module_detail_input_change_run_ajax(view);
							}
						}else if( object.attr("id") == ("checkbox_" + object.attr("rel") + "_edit" ) ){

							// nếu có quyền edit thì có luôn view
							if( object.prop("checked") ){
								var view = $("#checkbox_" + object.attr("rel") + "_view" );
								if(view.attr("id") != undefined && !view.prop("checked") ){
									view.prop("checked", true);
									roles_module_detail_input_change_run_ajax(view);
								}
							}else{ // nếu bỏ quyền edit thì bỏ delete luôn
								var input_delete = $("#checkbox_" + object.attr("rel") + "_delete" );
								if(input_delete.attr("id") != undefined && input_delete.prop("checked") ){
									input_delete.prop("checked", false);
									roles_module_detail_input_change_run_ajax(input_delete);
								}
							}

						}else if( object.attr("id") == ("checkbox_" + object.attr("rel") + "_view" ) ){

							// nếu có quyền view thì có luôn add
							if( !object.prop("checked") ){
								var add = $("#checkbox_" + object.attr("rel") + "_add" );
								if(add.attr("id") != undefined && add.prop("checked") ){
									add.prop("checked", false);
									roles_module_detail_input_change_run_ajax(add);
								}

								var edit = $("#checkbox_" + object.attr("rel") + "_edit" );
								if(edit.attr("id") != undefined && edit.prop("checked") ){
									edit.prop("checked", false);
									roles_module_detail_input_change_run_ajax(edit);
								}

								var input_delete = $("#checkbox_" + object.attr("rel") + "_delete" );
								if(input_delete.attr("id") != undefined && input_delete.prop("checked") ){
									input_delete.prop("checked", false);
									roles_module_detail_input_change_run_ajax(input_delete);
								}
							}

						}else if( object.attr("id") == ("checkbox_" + object.attr("rel") + "_add" ) ){

							// nếu có quyền add thì có luôn view
							if( object.prop("checked") ){
								var view = $("#checkbox_" + object.attr("rel") + "_view" );
								if(view.attr("id") != undefined && !view.prop("checked") ){
									view.prop("checked", true);
									roles_module_detail_input_change_run_ajax(view);
								}
							}
						}
					}
				}
			}
		});
	}
</script>