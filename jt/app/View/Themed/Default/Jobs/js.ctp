<?php echo $this->element('js/permission_entry'); ?>
<input type="hidden" id="oldStatus" value="" />
<script type="text/javascript">
	$(function() {
		jobs_update_entry_header();
		$("#JobNo").focus(function(){
			$(this).attr("rel",$(this).val());
		});
		var oldStatus = "";
		$("#JobStatus").focus(function(){
			oldStatus = $("#JobStatusId").val();
			$("#oldStatus").val(oldStatus);
		});
		$("#JobStatus").change(function(){
			if(oldStatus=="Completed" && $("#JobStatusId").val()!="Completed")
				password_popup();
		});
	});
	function password_popup(){
		$("#password_store").val('').removeAttr("name");
		if( $("#password_window" ).attr("id") == undefined ){
			var html = '<div id="password_confirm" >';
				html +=	   '<div class="jt_box" style=" width:100%;">';
				html +=		  '<div class="jt_box_line"><div class=" jt_box_label " style=" width:25%;"></div><div id="alert_message"></div></div>';
				html +=	      '<div class="jt_box_line">';
				html +=	         '<div class=" jt_box_label " style=" width:25%;height: 61px">Password</div>';
				html +=	         '<div class="jt_box_field " style=" width:71%"><input name="password" id="password" class="input_1 float_left" type="password" value=""></div><input style="margin-top:2%" type="button" class="jt_confirms_window_cancel" id="confirms_cancel" value=" Cancel " /><input style="margin-top:2%" type="button" class="jt_confirms_window_ok" value=" Ok " id="confirms_ok" />';
				html +=	      '</div>';
				html +=	      '</div>';
				html +=	   '</div>';
				html +=	'</div>';
			$('<div id="password_window" style="width: 99%; padding: 0px; overflow: auto;">'+html+'</div>').appendTo("body");
		}
		var password_window = $("#password_window");
		password_window.kendoWindow({
			width: "355px",
			height: "85px",
			title: "Enter password",
			visible: false,
			activate: function(){
			  $('#password').focus();
			}
		});
		$("#password").val("");
		password_window.data("kendoWindow").center();
		password_window.data("kendoWindow").open();
		$("#confirms_ok").unbind("click");
		$("#confirms_ok").click(function() {
			$("#alert_message").html("");
			if( $("#password").val().trim()==''  ){
				$("#alert_message").html('<span style="margin-left: 5px; color:red; font-weight: 700">Password must not be empty.</span');
				$("#password").focus();
				return false;
			}
			$("#password_store").val($("#password").val()).attr("name","password");
	       	password_window.data("kendoWindow").close();
			$("#JobName").trigger("change");
		});
		$('#confirms_cancel').click(function() {
			$("#alert_message").html("");
			$("#JobStatus").val('Completed');
			$("#JobStatusId").val('Completed');
	       	password_window.data("kendoWindow").close();
	    });
	}
	function jobs_update_entry_header() {
		$("#job_name_header").html($("#JobName").val());
		if ($.trim($("#JobCompanyName").val()) == "") {
			$("#jobs_right_h1_header").html($("#JobType option[value='" + $("#JobType").val() + "']").text());

		} else {

			$("#jobs_right_h1_header").html('<span id="job_company_name_header"></span> | <span id="job_status_header"></span>');
			$("#job_company_name_header").html($("#JobCompanyName").val());
			$("#job_status_header").html($("#JobStatus option[value='" + $("#JobStatus").val() + "']").text());
		}
	}

	function jobs_auto_save_entry(object,callBack) {
		if($(object).attr("id")=='JobStatus'
		   &&$("#oldStatus").val()=="Completed" && $("#JobStatusId").val()!="Completed")
			return false;
		if ($.trim($("#JobHeading").val()) == "") {
			$("#JobHeading").val("#" + $("#JobNo").val() + "-" + $("#JobCompanyName").val());
		}

		jobs_update_entry_header();

		$("form :input", "#jobs_form_auto_save").removeClass('error_input');

		$.ajax({
			url: '<?php echo URL; ?>/jobs/auto_save',
			timeout: 15000,
			type: "post",
			data: $("form", "#jobs_form_auto_save").serialize(),
			success: function(result) {
				result = $.parseJSON(result);
				if(result.status != 'ok'){
					console.log(result.message);
					if (result.message == 'must_be_numberic'){
						$("#JobNo").addClass('error_input');
						alerts('Message', 'This ref no must be a number.');

					}else if (result.message == "so_completed") {
						$("#JobStatus").addClass('error_input');
						alerts('Message', 'All Sales orders must be completed first.');

					}else if (result.message == "si_invoiced") {
						$("#JobStatus").addClass('error_input');
						alerts('Message', 'All Sales orders must be invoiced first.');

					}else if (result.message == "sum_different") {
						$("#JobStatus").addClass('error_input');
						alerts('Message', 'Totals of sales orders are different from sales invoices.');

					}else if (result.message == "ref_no_existed") {
						$("#JobNo").addClass('error_input');
						alerts('Message', 'This ref no existed');

					} else if (result.message == "date_work") {
						$("#JobWorkStart").addClass('error_input');
						$("#JobWorkEnd").addClass('error_input');
						alerts("Message", '<b>Work start</b> can not greater than <b>Work end</b>');

					} else if (result.message == "wrong_pass") {
						alerts("Message", 'Wrong password.');
						$("#JobStatus").val('Completed');
						$("#JobStatusId").val('Completed');

					} else if (result.message == "need_pass") {
						alerts("Message", 'You need enter password to change Job no.');
						$("#JobStatus").val('Completed');
						$("#JobStatusId").val('Completed');
					}
				}
				else if (result.contact_id!=undefined) {
					$("#JobContactId").val(result.contact_id);
                    $("#JobContactName").val(result.contact_name);
				}
				if(callBack != undefined)
					callBack();
			}
		});
	}
</script>