<?php echo $this->element('tab_option'); ?>

<script type="text/javascript">
	$(function(){
		$(".entry_menu_add").attr({"onclick":"confirm_add();","href":"javascript:void(0)"});
        $('#duplicate_current_quotation').attr('onclick', 'confirm_duplicate_current_quotation()');
        $('#create_job').attr('onclick','createJob()');
        $('#create_sales_invoice').attr('onclick','create_sales_invoice()');
        $('#create_sales_order').attr('onclick','create_sales_order()');
	});
	function confirm_add(){
		confirms("Message","Are you sure you want to create a new Quotation?"
	 		,function(){//Yes
	 			window.location.replace("<?php echo URL.'/'.$controller.'/add'; ?>");
	 		}
	 		,function(){
	 			return false;
	 		});
	}
	function ajax_request(type,url){
		$.ajax({
			url: url,
			type: 'POST',
			data: {type:type},
			success: function(result){
				result = jQuery.parseJSON(result);
				if(result.status=='error')
					alerts('Message',result.message);
				else if(result.status=='ok')
					window.location.replace(result.url);
			}
		});

	}
	 function confirm_duplicate_current_quotation(){
	 	var arr = ['Duplicate','New rev',''];
	 	confirms3("Message","Create a 'New Revision' or a 'Duplicate' of this record?",arr
	 		,function(){//Duplicate
	 			ajax_request('duplicate','<?php echo URL ?>/quotations/duplicate_revise_quotation/');
	 		}
	 		,function(){//New rev
	 			ajax_request('new_rev','<?php echo URL ?>/quotations/duplicate_revise_quotation/');
	 		}
	 		,function(){
	 			return false;
	 		});
	}
	function createJob(){
		$.ajax({
			url : '<?php echo URL ?>/quotations/check_condition_create_job/',
			success: function(result){
				result = jQuery.parseJSON(result);
				if(result.status=='error')
					alerts('Message',result.message);
				else
				{
					if(result.confirm=='yes')
					{
						confirms("Message","Set the status of the Quotation to 'Approved'?"
				 		,function(){//Yes
				 			ajax_request('change_status','<?php echo URL ?>/quotations/create_job/');
				 		}
				 		,function(){//No
				 			ajax_request('none','<?php echo URL ?>/quotations/create_job/');
				 		});
					}
					else
					{
						ajax_request('none','<?php echo URL ?>/quotations/create_job/');
					}
				}
			}
		});
	}
	function create_sales_invoice(){
		$.ajax({
			url : '<?php echo URL ?>/quotations/check_condition_create_salesinvoice/',
			success: function(result){
				result = jQuery.parseJSON(result);
				if(result.status=='error')
					alerts('Message',result.message);
				else{
					if(result.confirm=='yes'){
						confirms("Message","Set the status of the Quotation to 'Approved'?"
				 		,function(){//Yes
				 			ajax_request('change_status','<?php echo URL ?>/quotations/create_salesinvoice/');
				 		}
				 		,function(){//No
				 			ajax_request('none','<?php echo URL ?>/quotations/create_salesinvoice/');
				 		});
					}
					else
						ajax_request('none','<?php echo URL ?>/quotations/create_salesinvoice/');
				}
			}
		});
	}
	function create_sales_order(){
		$.ajax({
			url : '<?php echo URL ?>/quotations/create_salesorder/',
			success: function(result){
				result = jQuery.parseJSON(result);
				if(result.status=='ok')
					window.location.replace(result.url);
				else
					alerts('Message',result.message);
			}
		});
	}
</script>