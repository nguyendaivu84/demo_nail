<?php echo $this->Html->script('jquery-1.10.2.min'); ?>
<?php if(!isset($arr_data)){ ?>
<form method="POST" id="add_product">
	<table>
		<tr>
			<td>URL</td>
			<td><input type="text" size="80" name="url" placeholder="http://www.nailsrus.ca/feet/foot-lotions" /></td>
		</tr>
		<tr>
			<td>Folder</td>
			<td>
				<select name="folder">
					<option value="minh" <?php if($folder == 'minh') echo 'selected' ?>>Thim Minh</option>
					<option value="cang" <?php if($folder == 'cang') echo 'selected' ?>>Chi Cang</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input type="submit" name="submit" value="OK"/>
			</td>
		</tr>
	</table>
</form>
<?php } else { ?>
<form method="POST">
	<table>
		<tr>
			<td colspan="2">
				<?php echo count($arr_data['products']); ?> product(s)
			</td>
		</tr>
		<tr>
			<td width="20%">Folder</td>
			<td><input id="folder" readonly="readonly" value="<?php echo $folder; ?>" /></td>
		</tr>
		<tr>
			<td width="25%">Category</td>
			<td>
				<select id="category">
					<?php
						foreach($nail_category as $category){
							echo "<option value=\"{$category['_id']}_@_{$category['name']}\">{$category['name']}</option>";
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td width="20%">SKU without code</td>
			<td><input id="sku_without_code" name="sku_without_code" value="" /></td>
		</tr>
		<tr>
			<td width="20%">Category Name</td>
			<td>
				<input id="category_name" name="category_name" value="" />
				<input type="hidden" id="category_id" name="category_id" value="" />
			</td>
		</tr>
		<?php foreach($arr_data['products'] as $key=>$product){ ?>
		<tr >
			<td style="font-weight: bold;border-bottom: 1px solid #000">
				Product <?php echo ($key+1); ?>
				<span class="delete" id="<?php echo $key; ?>" style="margin-left: 15px;font-weight: bold; cursor: pointer;">X</span>
			</td>
			<td style="border-bottom: 1px solid #000">
				<table>
				<?php foreach($product as $field=>$value){ ?>
					<tr>
						<td width="20%"><?php echo $field; ?></td>
						<td>
							<input type="text" id="<?php echo str_replace(' ', '_', strtolower($field)).'_'.$key; ?>" name="<?php echo $key; ?>[<?php echo str_replace(' ', '_', strtolower($field)); ?>]" value="<?php echo $value; ?>" size="75">
						</td>
					</tr>
				<?php } ?>
					<tr>
						<td>Sell Price</td>
						<td>
							<input type="text" name="<?php echo $key; ?>[sell_price]" size="75"/>
						</td>
					</tr>
					<tr>
						<td>Product description</td>
						<td>
							<textarea cols="58" name="<?php echo $key; ?>[description]"></textarea>
						</td>
					</tr>
					<tr>
						<td>Product Indredents</td>
						<td>
							<textarea cols="58" name="<?php echo $key; ?>[indredents]"></textarea>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php } ?>
	</table>
	<input style="float: left;right: 35%;top: 20%;position: fixed;" type="submit" name="add_product" value="Add Products" />
</form>
<script type="text/javascript">
	$(".delete").click(function(){
		var delete_span = $(this);
		var id = $(this).attr("id");
		var big_image = $("#big_image_"+id).val();
		var small_image = $("#small_image_"+id).val();
		$.ajax({
			url: "<?php echo URL.'/products/remove_tmp_images' ?>",
			type: "POST",
			data: {big_image: big_image, small_image: small_image, folder: $("#folder").val()},
			success: function(result){
				if(result == "ok"){
					console.log(delete_span);
					delete_span.parent().parent().remove();
				}
				else
					alert(result);
			}
		})
	})
	$("#category").change(function(){
		var category = $(this).val();
		$.ajax({
			url: "<?php echo URL.'/products/get_sku_without_code' ?>",
			type: "POST",
			data: {category: category},
			success: function(result){
				result = $.parseJSON(result);
				$("#sku_without_code").val(result.sku_without_code);
				$("#category_name").val(result.category_name);
				$("#category_id").val(result.category_id);
			}
		});
	})
</script>
<?php } ?>
