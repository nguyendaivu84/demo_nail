 <div id="dialog-form" title="Import barcode" style='display:none'>
    <div class="col-check">
        <input type="radio" id='radio_1' name="case" value="simple" checked>Simple
        <input type="radio" id='radio_2' name="case" value="group">Group
    </div>
    <fieldset>
        <div class="check_number">
            <input type="text" name="product_stock" id="product_stock">
        </div>

        <form id='barcode-form' style="width:100%" onsubmit="alert('stop submit'); return false;" >
            <div id="users-contain">
                <table id="users" style="width:100%">
                    <thead>
                        <tr class="ui-widget-header ">
                            <th>Product BarCode 1</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="record">
                            <td><input type='text' readonly/></td>
                            <td><input type='text' readonly/></td>
                        </tr>
                    </tbody>
                </table>
                <div id="element_move">
                    <div class="div_total">Total:</div>
                    <input id='total' type='text' readonly></input>
                </div>
            </div>
        </form>
    </fieldset>
</div>

<?php
	foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){
		if($key=='stock_summary')
			echo $this->element('../Products/stock_summary');
		else
			echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));
	}
?>
<input type="hidden" id="old_qty" value="<?php echo $old_qty; ?>" />
<input type="hidden" value="<?php if(isset($first_amended)) echo $first_amended;?>" id="first_amended" />
<p class="clear"></p>
<script type="text/javascript">
    $(function(){
		//build popup
		build_popup_location('add_locations');
		build_popup_location('add_stocktakes');
        //$('#total').insertAfter('#dialog-form');

        var id_Qty = "qty_counted_" + <?php echo $first_amended; ?>;
        $("#"+id_Qty).prop('readonly', 'readonly');

        $("#dialog-form").on('keydown',".qty_group",function(event){
                // Allow special chars + arrows
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
                        return;
                } else {
                    // If it's not a number stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                        event.preventDefault();
                    }
                }
        });
        $("#dialog-form").on('keypress',".qty_group",function(event){
            if (event.keyCode == 10 || event.keyCode == 13)
                event.preventDefault();

        });

        $("#dialog-form").on('focus',".qty_group",function(e){
            if ($(this).val()!='') {
                pre =  parseInt($(this).val());
                //alert('Pre=' + pre);
            } else {
                //alert('Carefully');
            }
        });

        $("#dialog-form").on('change',".qty_group",function(e){
            if ($(this).val()!='') {
                var now = parseInt($(this).val());
                //alert('Pre=' + pre);
                //alert('Now=' + now);
                total = parseInt($("#total").val()) + now - pre;
                $("#total").val(total);
                //$( "#product_stock" ).focus();
            }else {
                alert("Not blank")
            }
        });

		$("#block_full_locations input").change(function(){
			var ids = $(this).attr("id");
			var irr = ids.split("_");
			var ix = irr.length;
			var opid = irr[ix-1];
			var field = ids.replace("_"+opid,"");
			var datas = new Object();
			datas[field] = $(this).val();
			save_option('locations',datas,opid,1,'stock','update');
		});

        $("#block_full_stocktakes input").click(function(){
            var ids = $(this).attr("id");
            var irr = ids.split("_");
            var ix = irr.length;
            opid = irr[ix-1];
            var field = ids.replace("_"+opid,"");
             if (field == 'qty_counted' && $('#use_barcode').is(':checked')==true ){
                    $(this).trigger( "change" );
             }
        });

		$("#block_full_stocktakes input").change(function(){
			var ids = $(this).attr("id");
			var irr = ids.split("_");
			var ix = irr.length;
			opid = irr[ix-1];
			var field = ids.replace("_"+opid,"");
			var value = $(this).val();
			var datas = new Object();
			datas[field] = value;
			var amended = $("#first_amended").val();

			if(amended==opid){
                if (field == 'qty_counted' && $('#use_barcode').is(':checked')==true){
                    $( ".record" ).remove();
                    $( "#dialog-form" ).dialog( "open");
                    $('#element_move').insertAfter('#dialog-form');
                    $( "#total" ).val('0');
                    $( "#product_stock" ).focus();
                    $("#radio_1").prop("checked", true);
                }else
                    save_option('stocktakes',datas,opid,1,'stock','update');
			}else{
				alerts('Message','<?php msg('PRODUCT_NOT_CHANGE');?>',function(){
					reload_subtab('stock');
				});
			}
		});

        var product_stock = $( "#product_stock" ),
        allFields = $([]).add( product_stock );

        $("#dialog-form").dialog({
            closeOnEscape: false,
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
            buttons: {
                "Save": function() {
                        $( this ).dialog( "close" );
                        $.ajax({
                                url: '<?php echo URL.'/'.$controller;?>/import_barcode',
                                type:"POST",
                                data: $('#barcode-form').serialize(),
                                success: function(data_return){
                                    var datas = new Object();
                                    datas['qty_counted']  = data_return;
                                    save_option('stocktakes',datas,opid,1,'stock','update');
                                }
                        });
                        console.log('Phan ra =' + $('#barcode-form').serialize());
                },
                Cancel: function() {
                    <?php if (isset($first_amended)) { ?>
                        oldqty = $("#old_qty").val();
                        $("#qty_counted_<?php echo $first_amended; ?>").val(oldqty);
                    <?php } ?>
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });

        var i = 0;
        var j = 0;
        $("#product_stock").keypress(function(e) {
            if(e.which == 13 &&  $("#product_stock").val()!='') {
                case_checked = $('input[name=case]:checked', '#dialog-form').val();
                if (case_checked == 'simple' ) {
                    i++;
                    $("#users tbody").prepend("<tr class=record id=barcode" + i + ">" + "<td><input type='text' name=barcode[] value='" + $("#product_stock").val() + "' readonly style='width:100%'/></td>" + "<td><input type='text' name='qty[]' value='1' readonly style='width:100%' /><input type='hidden' name=type[] value='simple'></td>" + "</tr>" );
                        $("#product_stock").val("");
                        var temp = parseInt($("#total").val()) + 1;
                        $("#total").val(temp);
                }else if( case_checked == 'group' ) {
                    j++;
                    $("#users tbody").prepend( "<tr class=record id=group" + i + ">" + "<td><input type='text' name=barcode[] value='" + $("#product_stock").val() + "' readonly style='color:red;width:100%'/></td>" + "<td> <input type='text' class='qty_group' name='qty[]' value='1' style='color:red;width:100%' /><input type='hidden' name=type[] value='group'/></td>" + "</tr>" );
                        $("#product_stock").val("");
                        var temp = parseInt($("#total").val()) + 1;
                        $("#total").val(temp);
                }
                return false;
            }
        });
        // end --------------- 26/4/2014 ---------------------

	});
	function build_popup_location(keys){
		var parameter_get = "?is_supplier=1";
		window_popup("locations", "Specify location", keys, "bt_"+keys, parameter_get);
	}

	function after_choose_locations(ids,names,keys){
		if(keys=='add_locations'){
			var data_return = JSON.parse($("#after_choose_locations"+ keys + ids).val());
			var datas = new Object();
				datas['location_name'] 	= data_return.name;
				datas['location_id'] 	= ids;
				datas['min_stock'] 		= '';
				datas['location_type'] 	= data_return.location_type;
				datas['stock_usage'] 	= data_return.stock_usage;
			save_option('locations',datas,'',1,'stock','add');

		}else if(keys=='add_stocktakes'){
			var data_return = JSON.parse($("#after_choose_locations"+ keys + ids).val());
			var datas = new Object();
				datas['location_name'] 	= data_return.name;
				datas['location_id'] 	= ids;
				datas['stocktakes_date'] = '<?php echo time();?>';
				datas['location_type'] 	= data_return.location_type;
				datas['stock_usage'] 	= data_return.stock_usage;
				datas['qty_counted'] 	= '';
				datas['qty_amended'] 	= '';
			save_option('stocktakes',datas,'',1,'stock','add');
		}
	}

</script>