<div class="float_left hbox_form" style="width:auto;">
    <a href="<?php echo URL . '/' . $controller; ?>/create_email_pdf/">
        <input class="btn_pur" id="emailexport_products" type="button" value="Email Order" style="width:99%;" />
    </a>
</div>
<div class="float_left hbox_form" style="width:auto; margin-left:5px;">
    <a href="<?php echo URL . '/' . $controller; ?>/view_pdf/">
        <input class="btn_pur" id="printexport_products" type="button" value="Export PDF" style="width:99%;" />
    </a>

</div>
<?php if($this->Common->check_permission('shippings_@_entry_@_add',$arr_permission)): ?>
<div class="float_right hbox_form" style="margin-right: 20.6%;position: relative;width: 5%;">
     <a href="<?php echo URL . '/' . $controller; ?>/return_item/"><input class="btn_pur" type="button" value="Return" style="width:99%;" /></a>
</div>
<?php endif; ?>
<?php if($this->Common->check_permission('products_@_entry_@_edit',$arr_permission)): ?>
<div class="float_right hbox_form" style="margin-right: 0%;position: relative;width: 10.2%;">
     <a href="<?php echo URL . '/' . $controller; ?>/receive_item/"><input class="btn_pur" type="button" value="Receive" style="width:99%;" /></a>
</div>
<?php endif; ?>
