<?php
    if(isset($err)&&$err)
        echo '
            <script type="text/javascript">
                $(function(){
                    alerts("Message", "There are not any stock locations, you must create one first.", function() {
                        window.location.replace("'.URL.'/locations/entry");
                    });
                });
            </script>';

?>
<style type="text/css">
ul.ul_mag li.hg_padd {
  overflow: visible !important;
}
.bg4 {
  background: none repeat scroll 0 0 #949494;
  color: #fff;
}

.bg4 span h4 {
  margin-left: 1%;
  width: 100%;
}
</style>
<?php echo $this->element('../' . $name . '/tab_option'); ?>
<div class="tab_1 full_width" style="margin:1% 1%; width:98%;">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
               <?php
                    if(isset($arr_po['code']))
                        echo translate('Purchase Order').': '.$arr_po['code'].' ('.translate('Receive').')';
                ?>
            </h4>
        </span>
    </span>
    <p class="clear"></p>
    <ul class="ul_mag clear bg3">
        <li class="hg_padd center_text" style="width:7%"><?php echo translate('Code'); ?></li>
        <li class="hg_padd" style="width:30%"><?php echo translate('Name / detail'); ?></li>
        <li class="hg_padd right_txt" style="width:6%"><?php echo translate('Original qty'); ?></li>
        <div class="float_left" style="width:15%;">
            <div class="tab_title_purchasing">
                <span class="block_purcharsing"><?php echo translate('Shipping') ; ?></span>
            </div>
            <div>
                <li class="hg_padd line_mg right_txt" style="width:50%">Shipped</li>
                <li class="hg_padd line_mg right_txt" style="width:47%">Balance</li>
            </div>
        </div>
        <div class="float_left" style="width:15%;">
            <div class="tab_title_purchasing">
                <span class="block_purcharsing"><?php echo translate('Receiving'); ?></span>
            </div>
            <div>
                <li class="hg_padd line_mg right_txt" style="width:50%">Received</li>
                <li class="hg_padd line_mg right_txt" style="width:47%">Balance</li>
            </div>
        </div>
        <li class="hg_padd center_text" style="width:6%"><?php echo translate('Return'); ?></li>
        <li class="hg_padd right_txt" style="width:6%"><?php echo translate('Receive now'); ?></li>
        <li class="hg_padd" style="width:8%"><?php echo translate('Location'); ?></li>
    </ul>
    <div class="container_same_category mCustomScrollbar _mCS_4">
        <div class="mCustomScrollBox mCS-light" style="position:relative; height:100%; overflow:scroll; max-width:100%;">
            <div class="mCSB_container mCS_no_scrollbar" style="position: relative; top: 0px;">
                <form method="POST" enctype="multipart/form-data" id="form_receive">

                    <?php
                    $i = 1;

                    ?>
                    <?php if(isset($arr_po['products'])&&!empty($arr_po['products']))
                         foreach ($arr_po['products'] as $key => $po): ?>
                        <?php if (!isset($po['deleted']) || ($po['deleted'] == false && isset($po['quantity']) && $po['quantity'] > 0 && (isset($po['balance_received'])&&$po['balance_received']!=0|| !isset($po['balance_received']) ))):
                        ?>

                            <?php
                            // set pg;
                            $pg = ($i%2==0 ? 2 : 1);
                            ?>
                            <ul class="ul_mag clear bg<?php echo $pg; ?>" style="overflow:none;">
                                <input type="hidden" id="balance_value_<?php echo $key; ?>" value="<?php echo (isset($po['balance_received']) ? $po['balance_received'] : $po['quantity']); ?>" disabled="disabled" />
                                <li class="hg_padd center_text" style="width:7%"><?php if (isset($po['code'])) echo $po['code']; ?></li>
                                <li class="hg_padd" style="width:30%"><?php if (isset($po['products_name'])) echo $po['products_name']; ?></li>
                                <li id="product_quantity" class="hg_padd right_txt" style="width:6%"><?php if (isset($po['quantity'])) echo $po['quantity']; ?></li>
                                <li class="hg_padd right_txt" style="width:6.7%"><?php echo (isset($po['quantity_shipped']) ? $po['quantity_shipped'] : 0); ?></li>
                                <li class="hg_padd right_txt" style="width:6.4%">
                                    <?php
                                        echo (isset($po['balance_shipped']) ? $po['balance_shipped'] : $po['quantity']);
                                    ?>
                                </li>
                                <li id="received_<?php echo $key; ?>" class="hg_padd right_txt" style="width:6.7%">
                                    <?php echo (isset($po['quantity_received']) ? $po['quantity_received'] : 0); ?>

                                </li>
                                <li id="balance_<?php echo $key; ?>" class="hg_padd right_txt" style="width:6.2%">
                                    <?php
                                        echo (isset($po['balance_received']) ? $po['balance_received'] : $po['quantity']);
                                    ?>

                                </li>
                                <li class="hg_padd right_txt" style="width:6%"><?php echo (isset($po['quantity_returned']) ? $po['quantity_returned'] : 0 ); ?></li>
                                <li class="hg_padd right_text" style="width:6%">

                                    <input type="text" rel="0" onkeypress="return isPrice(event);" id="receive_now_<?php echo $key; ?>" name="quantity[<?php echo $key ?>]" value="0" class="validate input_inner jt_box_save right_txt"/>
                                </li>
                                <li class="hg_padd right_text" style="width:8%; overflow: visible;">
                                    <?php echo $this->Form->input("location_".$key, array(
                                            'class'=>'input_inner jt_box_save viewprice_products_name',
                                            'name'=>'location_name['.$key.']',
                                            'value'=>(isset($po['receive_item']['location_name']) ? $po['receive_item']['location_name'] : reset($arr_location)),
                                            'stype'=>'width:150px',
                                    )); ?>
                                    <input type="hidden" name='location_id[<?php echo $key; ?>]' id="location_<?php echo $key; ?>Id" value="<?php echo (isset($po['receive_item']['location_id']) ? $po['receive_item']['location_id']: key($arr_location) ); ?>" />
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#location_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_location); ?>);
                                        });
                                    </script>
                                </li>
                            </ul>
                        <?php $i++; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>

                </form>
                <?php
                    $k = 8 - $i;
                    if($k>0)
                        for($j=0; $j < $k ; $j++ )
                        {
                            $pg = ($i%2==0 ? 2 : 1);
                            echo ' <ul class="ul_mag clear bg'.$pg.'" style="overflow:none;"></ul>';
                            $i++;
                        }
                ?>
            </div>
        </div>
    </div>
    <span class="title_block bo_ra2">
        <span class="bt_block float_right no_bg">
            <div class="dent_input float_right">
                <input type="button" class="btn_pur" id="bt_cancel" onclick="window.location.replace('<?php echo URL; ?>/purchaseorders/entry')" name="cancel" value="Cancel" style="width: 100px; cursor: pointer">
                <input type="button" class="btn_pur" onclick="fr_submit()" id="bt_continue" name="continue" value="Continue" style="width: 100px; cursor: pointer">
            </div>
        </span>
    </span>
</div>

<script>
    $(function() {
        $('.validate').change(function() {
            $("#bt_continue").removeAttr('disabled');
            var ids = $(this).attr('id');
            var values = $(this).val();
            var old = $(this).attr('rel');
            var tmp = ids.split("_");
            tmp = tmp[2];
            var balance = parseInt($("#balance_value_" + tmp).val());
            if (values > balance) {
                $("#bt_continue").attr('disabled', 'disabled');

                alerts('Message', 'Product received is not greater than the remaining products', function() {
                    $("#" + ids).val(parseInt(old));
                    if(parseInt(old) <= parseInt(balance))
                        $("#bt_continue").removeAttr('disabled');
                });
            }
            else {
                 $("#" + ids).val(parseInt(values));
                $("#" + ids).attr('rel', parseInt(values));
            }
        });
    });
    function fr_submit() {
        $("#bt_continue").attr('disabled', 'disabled');
        $("form#form_receive").submit();

    }
</script>