<?php echo $this->element('tab_option'); ?>
<input type="hidden" id="click_open_window_salesinvoice" />
<script type="text/javascript">
	$(function(){
		window_popup("salesinvoices","Specify Sales invoice","salesinvoice_options","click_open_window_salesinvoice");
		$('#create_shipping_for_full_sales_order').attr('onclick', 'create_full_shipping()');
		$('#create_sales_invoice_for_full_sales_order').attr('onclick', 'create_full_salesinvoice()');
		$('#back_orders_report_shipping_details').attr('onclick', 'back_order_shipping()');
		$('#back_orders_report_invoicing_details').attr('onclick','back_order_invoice()');
	})
	function create_full_shipping(){
	     $.ajax({
                    url: "<?php echo URL; ?>/<?php echo $controller;?>/create_full_shipping",
                    timeout: 15000,
                    type: "POST",
                    success: function(html){

                       if(html=='full_shipping')
                           alerts('Message','This sales order has already been part or fully shipped.');
                       else if(html=='no_company')
                           alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
                       else if(html=='no_product')
                           alerts('Message','No items have been entered on this transaction yet.');
                       else if(html=='over')
                          alerts('Message','This sales order was greater than this company credit limit.');
                       else
                          window.location.assign(html);
                    }
                });

         return false;
	}
	function create_full_salesinvoice(){
        $.ajax({
            url: "<?php echo URL; ?>/<?php echo $controller;?>/create_full_salesinvoice",
            timeout: 15000,
            type: "POST",
            success: function(html){
                if(html=='full_invoiced'){
                	confirms("Message","This Job already have an invoice link to it.<br />Do you want to append to this?",
                	         function(){
                	         	window.location.assign("<?php echo URL.'/salesorders/replace_salesinvoice' ?>");
                	         },function(){
                	         	return false;
                	         });
                }
                else if(html=='no_company')
                    alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
                else if(html=='no_product')
                    alerts('Message','No items have been entered on this transaction yet.');
                else{
                	confirms3("Message","Do you want to create new Sales invoice or append to an existed one?",['New','Append','']
                	         ,function(){//New
                   				window.location.assign(html);
                	         },function(){//Apend
                	         	$("#click_open_window_salesinvoice").click();
                	         },function(){
                	         	return false;
                	         });

                }
            }
        });

        return false;
	}
	function check_condition_salesorder(){
		$.ajax({
			url: '<?php echo URL; ?>/salesorders/create_shipping',
			type: 'GET',
			success: function(result){
				//console.log(result);
				var result = jQuery.parseJSON(result);
				if(result.status == 'error')
					alerts('Message',result.mess);
				else if(result.status == 'exist')
					confirms('Message',result.mess,function(){window.location.replace(result.url);},function(){return false;});
				else if(result.status='ok')
					window.location.replace(result.url);
			}
		});
		return false;
	}
	function back_order_shipping(){
		$.ajax({
			url: '<?php echo URL; ?>/salesorders/back_order_shipping',
			type: 'GET',
			success: function(result){
				//console.log(result);
				var result = jQuery.parseJSON(result);
				if(result.status == 'error')
					alerts('Message',result.mess);
				else if(result.status='ok')
					console.log(result.url);
					window.location.replace(result.url);
			}
		});
		return false;
	}
	function back_order_invoice(){
		$.ajax({
			url: '<?php echo URL; ?>/salesorders/back_order_invoice',
			type: 'GET',
			success: function(result){
				//console.log(result);
				var result = jQuery.parseJSON(result);
				if(result.status == 'error')
					alerts('Message',result.mess);
				else if(result.status='ok')
					window.location.replace(result.url);
			}
		});
		return false;
	}
	function after_choose_salesinvoices(ids,names,keys){
		$(".k-window").fadeOut('slow');
		$.ajax({
			url: "<?php echo URL.'/salesorders/append_salesinvoice/' ?>",
			type: "POST",
			data: {ids:ids},
			success: function(html){
				if(html=='no_company')
                    alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
                else if(html=='no_product')
                    alerts('Message','No items have been entered on this transaction yet.');
                else
                	window.location.replace(html);
			}
		})
	}
</script>