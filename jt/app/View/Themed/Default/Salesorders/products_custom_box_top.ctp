<style type="text/css">
    #button_group{
        float: right;
        padding-right: 10px;
    }
</style>
<?php if($this->Common->check_permission('communications_@_entry_@_add',$arr_permission)): ?>
<div class="float_left hbox_form" style="width:auto;">
    <a href="<?php echo URL.'/'.$controller;?>/email_pdf/">
    	<input class="btn_pur" id="emailexport_products" type="button" value="Email Order" style="width:99%;" />
    </a>
</div>
<?php endif; ?>
<div class="float_left hbox_form" style="width:auto; margin-left:5px;">
     <a href="<?php echo URL.'/'.$controller;?>/view_pdf/" target="_blank">
     	<input class="btn_pur" id="printexport_products" type="button" value="Export PDF" style="width:99%;" />
     </a>
</div>
<div class="middle_check" style="float: right;padding-right: 3.5%;">
    <label class="m_check2">
        <input type="checkbox" id="docket_check_checkall">
        <span class="bx_check"></span>
    </label>
</div>
<div style="float:right; margin-right: 15px">
    <a href="javascript:void(0)" id="asset_tag_report" target="_blank">
        <input class="btn_pur" id="asset_tag_pdf" type="button" value="Generate Dockets" style="width:99%;">
    </a>
</div>

<div id="docket_popup" class="hidden">
    <ul class="ul_mag clear bg3">
        <li class="hg_padd" style="text-align:left;width:39.5%;">
            Product Name
        </li>
        <li class="hg_padd" style="text-align:right;width:5%;">
            Size-W
        </li>
        <li class="hg_padd" style="text-align:right;width:5%;">
            Size-H
        </li><li class="hg_padd" style="text-align:right;width:6%;">
            Current Quantity
        </li>
        <li class="hg_padd" style="text-align:right;width:37%;">
            Repair Quantity
        </li>
    </ul>
    <form id="docket_form">
        <div id="docket_content">
        </div>

    </form>
</div>
<script type="text/javascript">
    $("#asset_tag_report").click(function(){
        $.ajax({
            url: '<?php echo URL; ?>/salesorders/get_uncompleted_docket/',
            success: function(result){
                result = $.parseJSON(result);
                if(result.length){
                    var content = '';
                    for(var i = 0; i < result.length; i++){
                        content += '<ul class="ul_mag clear bg'+(i%2==0? 1 : 2)+'""><li class="class="hg_padd" style="text-align:left;width:40%;">'+result[i].product_name+'</li><li class="class="hg_padd" style="text-align:right;width:5.5%;"><span style="margin-right: 10px" id="current_qty_'+result[i].key+'">'+result[i].sizew+'</span></li><li class="class="hg_padd" style="text-align:right;width:5.5%;"><span style="margin-right: 10px" id="current_qty_'+result[i].key+'">'+result[i].sizeh+'</span></li><li class="class="hg_padd" style="text-align:right;width:7%;"><span style="margin-right: 10px" id="current_qty_'+result[i].key+'">'+result[i].quantity+'</span></li><li class="hg_padd" style="text-align:right;width:36.5%;"><input type="text" name="repair_qty_'+result[i].key+'" id="repair_qty_'+result[i].key+'" rel="'+result[i].key+'" class="input_inner jt_box_save repair_qty_docket" style="text-align: right" onkeypress="return isPrice(event);" value="'+result[i].quantity+'" /></li></ul>';
                    }
                    appendPopup(content);
                } else
                    alerts("Message","All asset tags are completed.");
            }
        })


    });
    function appendPopup(content){
        $("#docket_content").html(content);
        var docket_popup = $("#docket_popup");
        docket_popup.kendoWindow({
            width: "60%",
            height: "35%",
            title: 'Docket',
            visible: false,
        });
        //show popup
        docket_popup.data("kendoWindow").center();
        docket_popup.data("kendoWindow").open();
        if($("#button_group").attr("id")==undefined){
            var input_html = '<div id="button_group"><input type="button" class="jt_confirms_window_cancel" value=" Cancel " id="docket_cancel" /><input type="button" class="jt_confirms_window_ok" value=" Ok " id="docket_ok" /></div>';
            $("#docket_popup_wnd_title").after(input_html);
        }
        var error = false;
        $(".repair_qty_docket").change(function(){
            error = false;
            $(this).removeClass("error_input");
            var id = $(this).attr("rel");
            var current_qty = parseInt($("#current_qty_"+id).html());
            var value = $(this).val();
            if(value>current_qty){
                error = true;
                alerts("Message","Please enter valid quantity.");
                $(this).addClass("error_input");
            }
        });
        $("#docket_ok").click(function(){
            if(error)
                return false;
            var data = $("input","#docket_form").serialize();
            $.ajax({
                url: "<?php echo URL.'/'.$controller.'/docket_repair_save' ?>",
                type: "POST",
                data: data,
                async: false,
                success: function(result){
                    if(result!="ok")
                        alerts("Message",result);
                    else{
                        docket_popup.data("kendoWindow").close();
                        window.open("<?php echo URL.'/salesorders/asset_tag_report/'.$mongo_id; ?>");
                    }
                }
            });

        });
        $("#docket_cancel").click(function(){
            docket_popup.data("kendoWindow").close();
        });
    }
</script>
