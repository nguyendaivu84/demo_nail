
<?php
		foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){

			echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));

		}
?>
<p class="clear"></p>
<script>
$(document).ready(function() {
	//tạo thêm 1 products line mới
	$("#bt_add_products").click(function() {
		var taxper = $('#tax').val();
		taxper = taxper.split("%");
		taxper = taxper[0];
		var datas = {
			'products_id' : '',
			'products_name' : 'This is new record. Click for edit',
			'sizew_unit' : 'in',
			'sizeh_unit' : 'in',
			'sell_by' : 'area',
			'quantity':0,
			'oum' : 'Sq.ft.',
			'taxper' : taxper,
		};
		save_option('products',datas,'',1,'line_entry','add');
	});
	//Dung de lay receive_item cho vao 1 hidden div tu khi load, de hover nhanh hon
	$.ajax({
		url: '<?php echo URL; ?>/purchaseorders/getShippingReceiveQuantity',
		success: function(html){
			if(html!='')
				$('#shipping_receive_quantity').html(html);
		}
	});
	$('.txt_quantity_received_lv').kendoTooltip({
		content: function(e){
			var Object = e.target;
			var key = Object.attr('id');
			key = key.split('_');
			var index = key.length;
			key  = parseInt(key[index-1]);
			if($('span#receive_item_'+key).html()!=undefined&&$('span#receive_item_'+key).html()!='')
				return $('span#receive_item_'+key);
			return false;

		},
		position: 'right',
		animation: {
	        open: {
	        effects: "slideIn:right",
	        duration: 200
	    	},
	    close: {
	        effects: "slideIn:right",
	        reverse: true,
	        duration: 200
	    	}
		}
	});
	$(".del_products").focusin(function(){
		ajax_note_set("");
		var ids = $(this).attr("id");
		ids  = ids.split("_");
		var ind = ids.length;
		var idfield =  parseInt(ids[ind-1]);
		ajax_note_set(" Press ENTER to delete the line:"+(idfield+1));
	});

	$(".del_products").focusout(function(){
		ajax_note("");
		var ids = $(this).attr("id");
			ids = ids.split("_");
		var index = ids.length;
		ids  = parseInt(ids[index-1])+1;
		$(".jt_line_over").removeClass('jt_line_over');
		$("#listbox_products_"+ids).addClass('jt_line_over');
	});

	$(".choice_code").click(function(){
		var ids = $(this).attr("id");
		var key_click_open = ids;
		ids  = ids.split("_");
		var ind = ids.length;
		var ids = ids[ind-1];
		//window_popup('products', 'Specify Purchase items','change_'+ids, 'choice_code_'+ids, get_where_com_po(),'force_re_install');
	});

	$( "#load_subtab" ).delegate(".rowedit input,.viewcheck_omit","change",function(){
		//nhan id
		var isreload=0;
		var names = $(this).attr("name");
		var intext = 'box_test_'+names;
		var inval = $(this).val();
		var ids  = names.split("_");
		var index = ids.length;
		var ids = ids[index-1];
		var price_key = new Array("sizew","sizeh","sell_price","area","unit_price","sub_total","taxper","tax","amount");
		var pricetext_key = new Array("unit_price","sub_total","tax","amount");
		//khoi tao gia tri luu
		names = names.replace("_"+ids,"");
		names = names.replace("cb_","");
		if(names=='sizew' || names=='sizeh' || names=='sell_price' || names=='area' || names=='sub_total' || names=='taxper' || names=='tax' || names=='amount' || names=='unit_price')
			inval = UnFortmatPrice(inval);
		var values = new Object();
			values[names]=inval;

		//format price
		if(price_key.indexOf(names) != -1){
			$('#'+names+'_'+ids).val(FortmatPrice(inval));
		}else
			$('#'+names+'_'+ids).val(inval);

		//xử lý ký hiệu enter /n
		/*var tmprr;
		if(names=='products_name'){
			$('#'+names+'_'+ids).val(inval);
			tmprr = inval.split("\n");
			var newtrs = '';
			for(var m=0;m<tmprr.length;m++){
				newtrs +=tmprr[m]+'<br>';
			}
			values[names]=newtrs;
		}*/

		//if is select box
		if($('#'+names+'_'+ids).parent().attr('class')=='combobox'){
			values[names]=$('#'+names+'_'+ids+'Id').val();
		}
		//set default oum
		if(names=='sell_by'){
			var newval = $('#sell_by_'+ids+'Id').val();
			if(newval=='unit')
				values['oum'] = 'unit';//set default
			else
				values['oum'] = 'Sq.ft.';//set default
			//change_uom_item(newval,ids);
			location.reload();
		}

		values['id']=ids;
		cal_line_entry(values,function(ret){
			var i,tem,txtval;

			for(i in ret){
				if(i=='products_name'){
					txtval = ret[i];
					if(ret[i]!=''){
						txtval = txtval.split("\n");
						txtval = txtval[0];
					}
					$('#'+i+'_'+ids).val(txtval);
				}else if($('#'+i+'_'+ids).parent().attr('class')=='combobox'){
					$('#'+i+'_'+ids+'Id').val(ret[i]);
				}else if(i=='unit_price' || i=='tax'){
					txtval = parseFloat(ret[i]);
					txtval = txtval.formatMoney(3, '.', ',');
					$('#txt_'+i+'_'+ids).html(txtval);
				}else if(i=='sub_total' || i=='amount'){
					txtval = parseFloat(ret[i]);
					txtval = txtval.formatMoney(2, '.', ',');
					$('#txt_'+i+'_'+ids).html(txtval);
				}else if(price_key.indexOf(i) != -1){
					$('#'+i+'_'+ids).val(FortmatPrice(ret[i]));
				}else
					$('#'+i+'_'+ids).val(ret[i]);
			}
			//hiển thị lại tổng
			$('#sum_sub_total').val(FortmatPrice(ret['sum_sub_total']));
			$('#sum_tax').val(FortmatPrice(ret['sum_tax']));
			$('#sum_amount').val(FortmatPrice(ret['sum_amount']));

		});

	});


	$(".icon_emp4").click(function(){
		var ids = $('#mongo_id').val();
		window.location.assign("<?php echo URL;?>/quotations/rfqs_list/"+ids);
	});

});

/**
/* Tính lại tổng trong database
/*
**/
function update_sum(handleData){
	//định nghĩa lại field trong database để tính
	var keyfield = {
			"sub_total"		: "sub_total",
			"tax"			: "tax",
			"amount"		: "amount",
			"sum_sub_total"	: "sum_sub_total",
			"sum_tax"		: "sum_tax",
			"sum_amount"	: "sum_amount"
		};
	var keyfield = JSON.stringify(keyfield);
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/update_sum',
		type:"POST",
		dataType: "json",
		data: {subdoc:'products',keyfield:keyfield},
		success: function(ret){
			$('#sum_sub_total').val(FortmatPrice(sum_ret['sum_sub_total']));
			$('#sum_tax').val(FortmatPrice(sum_ret['sum_tax']));
			$('#sum_amount').val(FortmatPrice(sum_ret['sum_amount']));
			if(handleData!=undefined)
				handleData(ret);
		}
	});
}



/**
/* Tính lại giá cho 1 line
/* Use: values là mảng data cần thay đổi
**/
function cal_line_entry(values,handleData){
	var jsonString = JSON.stringify(values);
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/ajax_cal_line',
		type:"POST",
		dataType: "json",
		data: {arr:values},
		success: function(ret){
			if(handleData!=undefined)
				handleData(ret);
		}
	});
}


/**
/* Change list box OUM (after Sell price)
/*
**/
function change_uom_item(val,ids){
	var units = '';
	if(val=='unit')
		units = 'unit';
	else
		units = 'Sq.ft.';
	var old_html = $("#box_edit_oum"+"_"+ids).html();
		old_html = old_html.split("<span");
		old_html = old_html[1].split(">");
		old_html = old_html[1];
		old_html = old_html.replace('value','value="'+units+'" title');
		$("#box_edit_oum"+"_"+ids+" .combobox").remove();
		$("#box_edit_oum"+"_"+ids).prepend(old_html+"  />");

	$.ajax({
		url: '<?php echo URL.'/';?>products/select_render',
		dataType: "json", //luu y neu dung json
		type:"POST",
		data: {sell_by:val},
		success: function(jsondata){
			$("#oum"+"_"+ids).combobox(jsondata);
		}
	});
}



function after_choose_products(ids,names,keys, stt){ // stt dùng cho trường họp riêng của popup sẽ thấy Số Thứ Tự
	var datas = JSON.parse($("#after_choose_products"+ keys + ids).val()); //(1)
	$("#window_popup_products"+ keys).data("kendoWindow").close();
	var opid  = keys.split("_");
	var	index = opid.length;
		opid = opid[index-1];
		keys = keys.replace("_"+opid,"");
	if(keys=='change'){
		var values = { 
					"products_id":ids,
					"code"		:datas.code,
					"products_name"	:datas.name,
					"sku"		:datas.sku,
					"sell_by"	:datas.sell_by, // BaoNam: ẩn đi vì đã gán vào chỗ (1) ở for bên dưới
					"sell_price":datas.sell_price,
					"sizeh"		:datas.sizeh,
					"sizeh_unit":datas.sizeh_unit,
					"sizew"		:datas.sizew,
					"sizew_unit":datas.sizew_unit,
					//"unit_price":datas.unit_price,
					"oum_depend":datas.oum_depend,
					"oum"		:datas.oum,
					//"markup"	:datas.markup",
					//"profit"	:datas.profit",
					"is_custom_size":datas.is_custom_size
				 };
		var taxx = $('#tax').val();
			taxx = taxx.split('%');
		values['products_id'] = ids;
		values['tax'] = taxx[0];

		save_option("products",values,opid,1,'line_entry','',function(opid){
			//$("#products_name_" + opid).trigger("change"); // //////////////////////////////// (2) /////////////////
			//$("#listbox_products_" + opid + " li:first").html('<span class="icon_emp"></span>');
			//$("#listbox_products_" + opid + " li:first").attr("onclick", " window.location.assign('<?php echo URL; ?>/products/entry/" + ids + "')");
			//reload_subtab('line_entry');
			//setTimeout("reload_subtab('line_entry')", 1800);
		});


	}
}

</script>