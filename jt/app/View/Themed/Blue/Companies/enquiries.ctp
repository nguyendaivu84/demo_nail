	<div class="full_width">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo translate('Enquiries from this company'); ?></h4></span>
				<?php if( $this->Common->check_permission('enquiries_@_entry_@_add', $arr_permission) ){ ?>
				<a href="<?php echo URL; ?>/companies/enquiries_add/<?php echo $company_id; ?>" title="Add new enquiry"><span class="icon_down_tl top_f"></span></a>
				<?php } ?>
			</span>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd" style="width: 3%"></li>
				<li class="hg_padd center_txt" style="width: 4%"><?php echo translate('Ref no'); ?></li>
				<li class="hg_padd center_txt" style="width: 6%"><?php echo translate('Date'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Status'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Contact'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Our rep'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Referred by'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Enquiry value'); ?></li>
				<li class="hg_padd center_txt" style="width: 21%"><?php echo translate('Requirements'); ?></li>
				<li class="hg_padd bor_mt" style="width: 1%"></li>
			</ul>

		<div class="container_same_category">
			<?php
			$i = 1; $count = 0;
			foreach ($arr_enquiry as $key => $value) {
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Enquiry_<?php echo $value['_id']; ?>">
				<li class="hg_padd" style="width: 3%">
					<a href="<?php echo URL; ?>/enquiries/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd center_txt" style="width: 4%"><?php echo $value['no']; ?></li>
				<li class="hg_padd center_txt" style="width: 6%"><?php if(isset($value['date']) && strlen($value['date']) > 0)echo $this->Common->format_date( $value['date']->sec, false); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php if(isset($value['status']))echo $value['status']; ?></li>
				<li class="hg_padd" style="width: 10%"><?php if(isset($value['contact_name']))echo $value['contact_name']; ?></li>
				<li class="hg_padd" style="width: 10%"><?php if(isset($value['our_rep']))echo $value['our_rep']; ?></li>
				<li class="hg_padd" style="width: 10%"><?php if(isset($value['referred']))echo $value['referred']; ?></li>
				<li class="hg_padd right_txt" style="width: 10%"><?php if(isset($value['enquiry_value']))echo $value['enquiry_value']; ?></li>
				<li class="hg_padd" style="width: 21%"><?php if(isset($value['detail']))echo $value['detail']; ?></li>
				<?php if( $this->Common->check_permission('enquiries_@_entry_@_delete', $arr_permission) ){ ?>
				<li class="hg_padd bor_mt" style="width: 1%">
					<div class="middle_check">
						<a title="Delete link" href="javascript:void(0)" onclick="companies_enquiry_delete('<?php echo $value['_id']; ?>')">
							<span class="icon_remove2"></span>
						</a>
					</div>
				</li>
				<?php } ?>
			</ul>
			<?php $i = 3 - $i; $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>

			<span class="hit"></span>
			<span class="title_block bo_ra2">
				<span class="bt_block">
					<?php echo translate('Click to view full details'); ?>
				</span>
			</span>
		</div>
	</div>

<?php if( $this->Common->check_permission('enquiries_@_entry_@_delete', $arr_permission) ){ ?>
<script type="text/javascript">
function companies_enquiry_delete(enquiry_id){

		confirms( "Message", "Are you sure you want to delete?",
			function(){
				$.ajax({
					url: '<?php echo URL; ?>/companies/enquiries_delete/' + enquiry_id,
					timeout: 15000,
					success: function(html){
						if(html == "ok"){
							$("#Enquiry_" + enquiry_id).fadeOut();
						}else{
							alerts("Error: ", html);
						}
						console.log(html);
					}
				});
			},function(){
				//else do somthing
		});
		return false;
	}
</script>
<?php } ?>