 <?php if(isset($arr_salesorder)){ ?>
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Sales orders for this company'); ?></h4></span>
			<?php if( $this->Common->check_permission('salesorders_@_entry_@_add', $arr_permission) ){ ?>
			<a href="<?php echo URL; ?>/companies/orders_add_salesorder/<?php echo $company_id; ?>" title="Add new sales order">
				<span class="icon_down_tl top_f"></span>
			</a>
			<?php } ?>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1%;"></li>
			<li class="hg_padd center_txt" style="width:3%;"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd center_txt" style="width:7%"><?php echo translate('Type') ?></li>
			<li class="hg_padd center_txt" style="width:8%;"><?php echo translate('Date in'); ?></li>
			<li class="hg_padd center_txt" style="width:8%;"><?php echo translate('Due date'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Status'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Our Rep'); ?></li>
			<li class="hg_padd center_txt" style="width:6%";><?php echo translate('Our CSR'); ?></li>
			<li class="hg_padd center_txt" style="width:7%"><?php echo translate('Total (bf.Tax)'); ?></li>
			<li class="hg_padd center_txt" style="width:34%"><?php echo translate('Heading'); ?></li>
			<li class="hg_padd bor_mt" style="width:1%;"></li>
		</ul>
		<div class="container_same_category" style="height:176px;overflow-y:auto;">
			<?php
			$i = 1; $count = 0;
			foreach ($arr_salesorder as $key => $value) {
				//echo '<pre>'; print_r($value);
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_salesorder_<?php echo $value['_id']; ?>">
				<li class="hg_padd" style="width:1%;">
					<a href="<?php echo URL; ?>/salesorders/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd center_txt" style="width:3%;"><?php echo @$value['code']; ?></li>
				<li class="hg_padd center_txt" style="width:7%;"><?php echo @$value['sales_order_type']; ?></li>
				<li class="hg_padd center_txt center_txt" style="width:8%;">
						<?php
							if(isset($value['salesorder_date']))
								echo $this->Common->format_date($value['salesorder_date']->sec,false);
						?>
				   </li>
				<li class="hg_padd center_txt center_txt" style="width:8%;">
						<?php
							if(isset($value['payment_due_date']))
								echo$this->Common->format_date($value['payment_due_date']->sec,false);
						?>
				</li>
				<li class="hg_padd center_txt" style="width:6%"><?php echo @$value['status']; ?></li>
				<li class="hg_padd center_txt" style="width:6%">
					<?php
						if (isset($value['our_rep_id'])){

							if (is_object($value['our_rep_id'])) {
								if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
								if( !isset($arr_contact_tmp[(string)$value['our_rep_id']]) ){
									$arr_contact = $model_contact->select_one(array('_id' => $value['our_rep_id']), array('_id', 'first_name', 'last_name'));
									if(isset($arr_contact['first_name'])){
										$arr_contact_tmp[(string)$value['our_rep_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
										echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
									}
								}else{
									echo $arr_contact_tmp[(string)$value['our_rep_id']];

								}
							}

						}
					?>
				</li>
				<li class="hg_padd" style="width:6%">
					<?php
						if (isset($value['our_csr_id'])){

							if (is_object($value['our_csr_id'])) {
								if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
								if( !isset($arr_contact_tmp[(string)$value['our_csr_id']]) ){
									$arr_contact = $model_contact->select_one(array('_id' => $value['our_csr_id']), array('_id', 'first_name', 'last_name'));
									if(isset($arr_contact['first_name'])){
										$arr_contact_tmp[(string)$value['our_csr_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
										echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
									}
								}else{
									echo $arr_contact_tmp[(string)$value['our_csr_id']];

								}
							}

						}
					?>
				</li>
				<li class="hg_padd right_txt" style="width:7%"><?php echo (@$value['sum_sub_total'] == 0 ? 0 : number_format($value['sum_sub_total'],2,'.',',')); ?></li>

				<li class="hg_padd" style="width:34%;"><?php echo @$value['name']; ?></li>
				<?php if( $this->Common->check_permission('salesorders_@_entry_@_delete', $arr_permission) ){ ?>
				<li class="hg_padd bor_mt  center_txt" style="width:1%;">
					<div class="middle_check">
						<a title="Delete link" href="javascript:void(0)" onclick="salesorder_remove_salesorder_company('<?php echo $value['_id']; ?>')">
							<span class="icon_remove2"></span>
						</a>
					</div>
				</li>
				<?php } ?>
			</ul>

			<?php $i = 3 - $i; $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>

		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
				<?php echo translate('Click to view full details'); ?>
			</span>
		</span>
	</div>

	<?php if( $this->Common->check_permission('salesorders_@_entry_@_delete', $arr_permission) ){ ?>
	<script type="text/javascript">
	function salesorder_remove_salesorder_company(salesorder_id){
		confirms( "Message", "Are you sure you want to delete?",
			function(){

				$.ajax({
					 url: '<?php echo URL; ?>/companies/orders_delete/' + salesorder_id + '/Salesorder',
					 timeout: 15000,
					 success: function(html){
						 if(html == "ok"){
							 $("#Company_salesorder_" + salesorder_id).fadeOut();
						 }else{
							 alerts("Error: ", html);
						 }
					 }
				 });
			},function(){
				//else do somthing
		});
	}
	</script>
	<?php } ?>

<?php } ?>

<br> <!-- =================================================== PURCHASE ORDERS ================================== -->

 <?php if(isset($arr_purchaseorder)){ ?>

<!-- Purchase Order -->
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Purchase orders for this company'); ?></h4></span>
			<?php if( $this->Common->check_permission('purchasesorders_@_entry_@_add', $arr_permission) ){ ?>
			<a href="<?php echo URL; ?>/companies/orders_add_purchasesorder/<?php echo $company_id; ?>" title="Add new purchases order">
				<span class="icon_down_tl top_f"></span>
			</a>
			<?php } ?>
		</span>
		<p class="clear"></p>

		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1%;"></li>
			<li class="hg_padd center_txt" style="width:3%;"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd center_txt center_txt" style="width:8%;"><?php echo translate('Required date'); ?></li>
			<li class="hg_padd center_txt center_txt" style="width:8%;"><?php echo translate('Delivery date'); ?></li>
			<li class="hg_padd center_txt center_txt" style="width:6%;"><?php echo translate('Status'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Supplier contact'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Our Rep'); ?></li>
			<li class="hg_padd center_txt" style="width:7%"><?php echo translate('Total (bf.Tax)'); ?></li>
			<li class="hg_padd center_txt" style="width:42%"><?php echo translate('Heading'); ?></li>
			<li class="hg_padd center_txt bor_mt" style="width:1%;"></li>
		</ul>
		<div class="container_same_category" style="height:176px;overflow-y:auto;">
			<?php
			$i = 1; $count = 0;
			foreach ($arr_purchaseorder as $key => $value) {
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_purchasesorder_<?php echo $value['_id']; ?>">
				<li class="hg_padd" style="width:1%;">
					<a href="<?php echo URL; ?>/purchaseorders/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd" style="width:3%;"><?php echo @$value['code']; ?></li>
				<li class="hg_padd center_txt center_txt" style="width:8%;">
					<?php echo @$this->Common->format_date($value['required_date']->sec, false); ?>
				</li>
				<li class="hg_padd center_txt center_txt" style="width:8%;"><?php if(isset($value['delivery_date']) && is_object($value['delivery_date']))echo $this->Common->format_date($value['delivery_date']->sec, false); ?></li>
				<li class="hg_padd center_txt" style="width:6%;"><?php if(isset($value['purchase_orders_status'])) echo $value['purchase_orders_status']; ?></li>
				<li class="hg_padd center_txt" style="width:6%;"><?php echo @$value['ship_to_contact_name']; ?></li>
				<li class="hg_padd center_txt" style="width:6%;">
					<?php
						if (isset($value['our_rep_id'])){
							if (is_object($value['our_rep_id'])) {
								if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
								if( !isset($arr_contact_tmp[(string)$value['our_rep_id']]) ){
									$arr_contact = $model_contact->select_one(array('_id' => $value['our_rep_id']), array('_id', 'first_name', 'last_name'));
									if(isset($arr_contact['first_name'])){
										$arr_contact_tmp[(string)$value['our_rep_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
										echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
									}
								}else{
									echo $arr_contact_tmp[(string)$value['our_rep_id']];
								}
							}
						}
					?>
				</li>
				<li class="hg_padd right_txt" style="width:7%;"><?php echo (@$value['sum_sub_total'] == 0 ? 0 : number_format($value['sum_sub_total'],2,'.',',')); ?></li>
				<li class="hg_padd" style="width:42%;"><?php echo @$value['name']; ?></li>
				<?php if( $this->Common->check_permission('purchasesorders_@_entry_@_delete', $arr_permission) ){ ?>
				<li class="hg_padd bor_mt  center_txt" style="width:1%;">
					<div class="middle_check">
						<a title="Delete link" href="javascript:void(0)" onclick="purchasesorder_remove_purchasesorder_company('<?php echo $value['_id']; ?>')">
							<span class="icon_remove2"></span>
						</a>
					</div>
				</li>
				<?php } ?>
			</ul>

			<?php $i = 3 - $i; $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>

		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
				<?php echo translate('Click to view full details'); ?>
			</span>
		</span>
	</div>

	<?php if( $this->Common->check_permission('purchasesorders_@_entry_@_delete', $arr_permission) ){ ?>
	<script type="text/javascript">
	function purchasesorder_remove_purchasesorder_company(purchasesorder_id){
		confirms( "Message", "Are you sure you want to delete?",
			function(){

				$.ajax({
					 url: '<?php echo URL; ?>/companies/orders_delete/' + purchasesorder_id + '/Salesorder',
					 timeout: 15000,
					 success: function(html){
						 if(html == "ok"){
							 $("#Company_purchasesorder_" + purchasesorder_id).fadeOut();
						 }else{
							 alerts("Error: ", html);
						 }
					 }
				 });
			},function(){
				//else do somthing
		});
	}
	</script>
	<?php } ?>
<?php } ?>