<?php echo $this->element('entry_tab_option', array('no_show_delete' => true)); ?>
<?php echo $this->element('js/lists_view'); ?>
<div id="content" class="fix_magr">
	<form method="POST" id="sort_form">
		<div class="w_ul2 ul_res2">
			<ul class="ul_mag clear bg top_header_inner2 ul_res2" id="sort">
				<li class="hg_padd" style="width:1%"></li>
				<li class="hg_padd" style="width:6%">
					<label>Type</label>
					<span id="is_customer" class="desc"></span>
				</li>
				<li class="hg_padd" style="width:4%">
					<label>Ref no</label>
					<span id="no" class="desc"></span>
				</li>
				<li class="hg_padd" style="width:24%">
					<label>Company</label>
					<span id="name" class="desc"></span>
				</li>
				<li class="hg_padd" style="width:16%">
					<label>Contact</label>
					<span id="contact_name" class="desc"></span>
				</li>
				<li class="hg_padd" style="width:8%">
					<label>Phone</label>
					<span id="phone" class="desc"></span>
				</li>
				<li class="hg_padd" style="width:23%">
					<label>Our rep</label>
					<span id="our_rep" class="desc"></span>
				</li>
				<li class="hg_padd center_txt" style="width:4%">
					<label>Inactive</label>
					<span id="inactive" class="desc"></span>
				</li>
				<li class="hg_padd bor_mt" style="width:3%"></li>
			</ul>
			<div id="lists_view_content">
				<!-- goi lists ajax -->
				<?php echo $this->element('../Companies/lists_ajax') ?>
			</div>
		</form>

	</div>
</div>

<?php echo $this->element('js/permission_lists') ?>