<?php echo $this->element('entry_tab_option');?>
<div id="content" class="fix_magr">

	<div class="jt_ajax_note" style="display: none;"></div>

	<div class="clear">

		<div class="clear_percent">
			<div class="block_dent_a">
				<div class="title_1 float_left"><h1><span id="company_name_header"></span></h1></div>
				<div class="title_1 right_txt float_right"><h1><span id="company_phoneprefix_header" style="display:none"><?php echo translate('Phone'); ?>: </span><span id="company_phone_header"></span></h1></div>
			</div>
		</div>

		<div id="companies_form_auto_save">
			<?php echo $this->Form->create('Company'); ?>
			<?php echo $this->Form->hidden('Company._id', array('value' => (string)$this->data['Company']['_id'])); ?>
			<?php echo $this->Form->hidden('Company.addresses_default_key'); ?>
			<div class="clear_percent">
				<div class="clear_percent_1 float_left">
					<div class="tab_1 block_dent_a">
						<p class="clear">
							<span class="label_1 float_left fixbor"><?php echo translate('Company no'); ?></span>
						</p>
						<div class="indent_new width_in float_left">
							<?php echo $this->Form->input('Company.no', array(
									'class' => 'input_4 float_left width_ina22',
									'style' => 'margin-top: 1.6%;width: 12% !important;'
							)); ?>
							<div class="in_active width_active_in2">
								<input type="hidden" name="data[Company][is_supplier]" id="CompanyIsSupplier_" value="0">
								<span class="inactive_fix_entry"><?php echo translate('Supplier'); ?></span>
								<label class="m_check2">
									<?php echo $this->Form->input('Company.is_supplier', array(
											'type' => 'checkbox',
									)); ?>
									<span></span>
								</label>
							</div>
							<div class="in_active width_active_in2">
								<input type="hidden" name="data[Company][is_customer]" id="CompanyIsCustomer_" value="0">
								<span class="inactive_fix_entry"><?php echo translate('Customer'); ?></span>
								<label class="m_check2">
									<?php echo $this->Form->input('Company.is_customer', array(
											'type' => 'checkbox'
									)); ?>
									<span></span>
								</label>
							</div>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Company name'); ?></span>
						</p>
						<div class="width_in float_left indent_input_tp">
							<?php echo $this->Form->input('Company.name', array(
									'class' => 'input_1 float_left'
							)); ?>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Type'); ?></span>
						</p>

						<div class="width_in float_left indent_input_tp">
							<?php echo $this->Form->input('Company.type_name', array(
									'class' => 'input_select',
							)); ?>
							<?php echo $this->Form->hidden('Company.type_name_id'); ?>
							<script type="text/javascript">
								$(function () {
									$("#CompanyTypeName").combobox(<?php echo json_encode($arr_company_type); ?>);
								});
							</script>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Phone'); ?></span>
						</p>
						<div class="indent_new width_in float_left">
							<?php echo $this->Form->input('Company.phone', array(
									'class' => 'input_1 float_left',
									'type' => 'tel',
									'onkeypress' => 'return isPhone(event);',
									'maxLength' => 20,
							)); ?>
							<span class="icon_phonec" title="Not yet implemented" class="jt_box_line_span"></span>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Fax'); ?></span>
						</p>
						<div class="indent_new width_in float_left">
							<?php echo $this->Form->input('Company.fax', array(
									'class' => 'input_1 float_left'
							)); ?>
							<span class="icon_down_pl" title="Not yet implemented"></span>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Email'); ?></span>
						</p>
						<div class="indent_new width_in float_left">
							<?php echo $this->Form->input('Company.email', array(
									'class' => 'input_1 float_left',
									'type' => 'email'
							)); ?>
							<span class="icon_emaili" title="Not yet implemented"></span>
						</div>

						<p class="clear">
							<span class="label_1 float_left fixbor2">
								<?php $link = 'javascript:void(0)';
									if(strlen(trim($this->data['Company']['web'])) > 0){
										$link = ' http://'. str_replace(' http://', '', $this->data['Company']['web']);
									} ?>
								<span style="display:inline;" onclick="var a=$(this).attr('href');window.open(a);" class="jt_box_line_span" id="link_to_web" href="<?php echo $link; ?>" ><?php echo translate('Web'); ?></span>
							</span>
						</p>
						<div class="indent_new width_in float_left">
							<?php echo $this->Form->input('Company.web', array(
									'class' => 'input_1 float_left'
							)); ?>
						</div>

						<p class="clear"></p>
					</div><!--END Tab1 -->
				</div>

				<div class="float_right" style="width: 72%;">
					<div class="tab_1 float_left block_dent8">
						<?php
							isset( $this->data['Company']['addresses_default_key']);
							$key = $this->data['Company']['addresses_default_key'];
							echo $this->element('box_type/address', array(
								'address_label' => array('Default address'),
								'address_more_line' => 0,
								'address_onchange' => 'companies_auto_save_entry()',
								'address_controller' => array('Company'),
								'address_country_id' => array($this->data['Company']['addresses'][$key]['country_id']),
								'address_province_id' => array($this->data['Company']['addresses'][$key]['province_state_id']),
								'address_value' => array(
									'default' => array(
										$this->data['Company']['addresses'][$key]['address_1'],
										$this->data['Company']['addresses'][$key]['address_2'],
										$this->data['Company']['addresses'][$key]['address_3'],
										$this->data['Company']['addresses'][$key]['town_city'],
										$this->data['Company']['addresses'][$key]['country_id'],
										$this->data['Company']['addresses'][$key]['province_state'],
										$this->data['Company']['addresses'][$key]['zip_postcode']
									)
								),
								'address_key' => array('default'),
								'address_conner' => array(
									array(
										'top' => 'hgt fixbor',
										'bottom' => 'fixbor3 fix_bottom_address fix_bot_bor'
									)
								)
						)); ?>

						<div class="tab_1_inner float_left">

							<!-- Business Type -->
							<p class="clear">
								<span class="label_1 float_left  minw_lab "><?php echo translate('Business Type'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Company.business_type', array(
										'class' => 'input_select',
								)); ?>
								<?php echo $this->Form->hidden('Company.business_type_id'); ?>
							</div>
							<script type="text/javascript">
								$(function () {
									$("#CompanyBusinessType").combobox(<?php echo json_encode($arr_business_type); ?>);
								});
							</script>

							<!-- Industry -->
							<p class="clear">
								<span class="label_1 float_left  minw_lab "><?php echo translate('Industry'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Company.industry', array(
										'class' => 'input_select',
								)); ?>
								<?php echo $this->Form->hidden('Company.industry_id'); ?>
							</div>
							<script type="text/javascript">
								$(function () {
									$("#CompanyIndustry").combobox(<?php echo json_encode($arr_industry); ?>);
								});
							</script>

							<!-- Industry -->
							<p class="clear">
								<span class="label_1 float_left  minw_lab "><?php echo translate('Size'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Company.size', array(
										'class' => 'input_select',
								)); ?>
								<?php echo $this->Form->hidden('Company.size_id'); ?>
							</div>
							<script type="text/javascript">
								$(function () {
									$("#CompanySize").combobox(<?php echo json_encode($arr_size); ?>);
								});
							</script>

							<!-- Our rep -->
							<p class="clear">
								<span class="label_1 float_left minw_lab">
									<?php
										$link = 'javascript:void(0)';
										if(isset($this->data['Company']['our_rep_id']) && is_object($this->data['Company']['our_rep_id'])){
											$link = URL . '/contacts/entry/' . $this->data['Company']['our_rep_id'];
										}
									?>
									<span style="display:inline;" onclick="jt_link_module(this, '<?php echo msg('QUOTATION_CREATE_LINK'); ?> Contacts', '<?php echo URL; ?>/contacts/add')" href="<?php echo $link; ?>" class="link_Contact jt_box_line_span" id="link_to_contacts" ><?php echo translate('Our Rep'); ?></span>

								</span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->hidden('Company.our_rep_id'); ?>

								<?php
									$arr_contact_tmp = array();
									$our_rep = '';
									if (isset($this->data['Company']['our_rep_id'])&&is_object($this->data['Company']['our_rep_id'])) {
										if( !isset($arr_contact_tmp[(string)$this->data['Company']['our_rep_id']]) ){
											$arr_contact = $model_contact->select_one(array('_id' => new MongoId($this->data['Company']['our_rep_id'])), array('_id', 'first_name', 'last_name'));
											if(isset($arr_contact['first_name'])){
												$our_rep = $arr_contact_tmp[(string)$this->data['Company']['our_rep_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
											}
										}else{
											$our_rep = $arr_contact_tmp[(string)$this->data['Company']['our_rep_id']];

										}
									}
								?>
								<?php echo $this->Form->input('Company.our_rep', array(
										'class' => 'input_1 float_left ',
										'value' => $our_rep,
										'readonly' => true,
								)); ?>
								<span id="click_open_window_contacts_responsible" class="iconw_m indent_dw_m" title="<?php echo translate('Choose our rep'); ?>"></span>
								<script type="text/javascript">
									$(function(){
										// kiểm tra xem đã chọn company chưa
										company_init_popup_contacts_responsible();
									});

									function company_init_popup_contacts_responsible(){
										var parameter_get = "?is_employee=1";
										window_popup("contacts", "Specify contact", "_responsible", "", parameter_get);

									}

									function after_choose_contacts_responsible(contact_id, contact_name){
										$("#link_to_contacts").attr("href", "<?php echo URL; ?>/contacts/entry/" + contact_id);
										$("#CompanyOurRepId").val(contact_id);
										$("#CompanyOurRep").val(contact_name);
										$("#window_popup_contacts_responsible").data("kendoWindow").close();
										companies_auto_save_entry();
										return false;
									}
								</script>
							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab">
									<?php
										$link = 'javascript:void(0)';
										if(isset($this->data['Company']['our_csr_id']) && is_object($this->data['Company']['our_csr_id'])){
											$link = URL . '/contacts/entry/' . $this->data['Company']['our_csr_id'];
										}
									?>
									<span style="display:inline;" onclick="jt_link_module(this, '<?php echo msg('QUOTATION_CREATE_LINK'); ?> Contacts', '<?php echo URL; ?>/contacts/add')" href="<?php echo $link; ?>" class="link_Contact jt_box_line_span" id="link_to_contacts_csr" ><?php echo translate('Our CSR'); ?></span>
								</span>
							</p>

							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->hidden('Company.our_csr_id'); ?>

								<?php
									$arr_contact_tmp = array();
									$our_csr = '';
									if (is_object($this->data['Company']['our_csr_id'])) {
										if( !isset($arr_contact_tmp[(string)$this->data['Company']['our_csr_id']]) ){
											$arr_contact = $model_contact->select_one(array('_id' => new MongoId($this->data['Company']['our_csr_id'])), array('_id', 'first_name', 'last_name'));
											if(isset($arr_contact['first_name'])){
												$our_csr = $arr_contact_tmp[(string)$this->data['Company']['our_csr_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
											}
										}else{
											$our_csr = $arr_contact_tmp[(string)$this->data['Company']['our_csr_id']];

										}
									}
								?>

								<?php echo $this->Form->input('Company.our_csr', array(
										'class' => 'input_1 float_left ',
										'value' => $our_csr,
										'readonly' => true,
								)); ?>
								<span id="click_open_window_contacts_csr" class="iconw_m indent_dw_m" title="<?php echo translate('Choose our CSR'); ?>"></span>
								<script type="text/javascript">
									$(function(){
										// kiểm tra xem đã chọn company chưa
										company_init_popup_contacts_csr();
									});

									function company_init_popup_contacts_csr(){
										var parameter_get = "?is_employee=1";
										window_popup("contacts", "Specify contact", "_csr", "", parameter_get);

									}

									function after_choose_contacts_csr(contact_id, contact_name){
										$("#link_to_contacts_csr").attr("href", "<?php echo URL; ?>/contacts/entry/" + contact_id);
										$("#CompanyOurCsrId").val(contact_id);
										$("#CompanyOurCsr").val(contact_name);
										$("#window_popup_contacts_csr").data("kendoWindow").close();
										companies_auto_save_entry();
										return false;
									}
								</script>
							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<input type="text" readonly="true" class="input_select">
							</div>

							<p class="clear">
								<input type="hidden" name="data[Company][inactive]" id="CompanyInactive_" value="0">
								<span class="label_1 float_left minw_lab fixbor3"><?php echo translate('Inactive'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp" style="margin-top: 9px;padding-left: 8px;width: 41%;">
								<label class="m_check2">
									<?php echo $this->Form->input('Company.inactive', array(
											'type' => 'checkbox'
									)); ?>
									<span></span>
								</label>
								<span class="inactive dent_check"></span>
							</div>

						</div>

						<!-- TAB 3 -->
						<div class="tab_1_inner float_left">

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Is shipper'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp" style="padding-left: 8px;width: 48%;">
								<input type="hidden" name="data[Contact][inactive]" id="ContactIsShipper_" value="0">
								<div class="in_active2">
									<label class="m_check2">
										<?php echo $this->Form->input('Company.is_shipper', array(
												'type' => 'checkbox'
										)); ?>
										<span></span>
									</label>
									<span class="inactive dent_check"></span>

									<p class="clear"></p>
								</div>
							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Tracking URL'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Company.tracking_url', array(
								        'class' => 'input_1 float_left',
								)); ?>
							</div>

							<!-- TIME TO WORK -->
							<p class="clear">
								<span class="label_1 float_left minw_lab"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
							</div>
							<p class="clear">
								<span class="label_1 float_left minw_lab"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
							</div>
							<p class="clear">
								<span class="label_1 float_left fixbor3 minw_lab hgt3"><!-- Identity --></span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<?php echo $this->Form->end(); ?>
		</div><!--  END DIV companies_form_auto_save -->

		<!-- DIV MENU NGANG -->
		<div class="clear block_dent3">
			<div class="box_inner">
				<ul id="companies_ul_sub_content" class="ul_tab">
					<li id="contacts" class="<?php if($sub_tab == 'contacts'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Contacts'); ?></a>
					</li>
					<li id="addresses" class="<?php if($sub_tab == 'addresses'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Addresses'); ?></a>
					</li>
					<li id="enquiries" class="<?php if($sub_tab == 'enquiries'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Enquiries'); ?></a>
					</li>
					<li id="jobs" class="<?php if($sub_tab == 'jobs'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Jobs'); ?></a>
					</li>
					<li id="tasks" class="<?php if($sub_tab == 'tasks'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Tasks'); ?></a>
					</li>

					<li id="products" class="<?php if($sub_tab == 'products'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Products'); ?></a>
					</li>

					<style type="text/css">
						<?php if( !$this->data['Company']['is_supplier'] ){ ?>
						.disp{
							display: none;
						}
						<?php } ?>
					</style>

					<!-- UL FOR CUSTOMER -->
					<li id="quotes" class="<?php if($sub_tab == 'quotes'){ ?>active<?php } ?> dispquote">
						<a href="javascript:void(0)"><?php echo translate('Quotes'); ?></a>
					</li>

					<!-- UL FOR SUPPLIER -->
					<li id="rfqs" class="<?php if($sub_tab == 'rfqs'){ ?>active<?php } ?> disp">
						<a href="javascript:void(0)"><?php echo translate('RFQ\'s'); ?></a>
					</li>
					<!-- END === UL FOR SUPPLIER -->

					<li id="orders" class="<?php if($sub_tab == 'orders'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Orders'); ?></a>
					</li>
					<li id="shipping" class="<?php if($sub_tab == 'shipping'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Shipping'); ?></a>
					</li>

					<li id="account" class="<?php if($sub_tab == 'account'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Account'); ?></a>
					</li>
					<li id="documents" class="<?php if($sub_tab == 'documents'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Documents'); ?></a>
					</li>

					<li id="communications" class="<?php if($sub_tab == 'communications'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Communications'); ?></a>
					</li>

					<li id="other" class="<?php if($sub_tab == 'other'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Other'); ?></a>
					</li>

				</ul>
			</div>
		</div>

		<!-- DIV NOI DUNG CUA CAC SUBTAB -->
		<div id="companies_sub_content" class="jt_sub_content clear_percent">
			<?php echo $this->element('../Companies/' . $sub_tab); ?>
		</div>

	</div>
	<div class="clear"></div>
</div>

<?php echo $this->element('../Companies/js'); ?>