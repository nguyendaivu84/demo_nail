<?php $i = 1 ?><br>
<?php foreach ($arr_companies as $value):

    $i = 3 - $i;
?>

    <ul class="ul_mag clear bg<?php echo $i ?>" id="companies_<?php if(isset($value['_id']))echo (string) $value['_id']; ?>">

        <li class="hg_padd" style="width:1%">
            <a style="color: blue" href="<?php echo URL; ?>/companies/entry/<?php if(isset($value['_id']))echo $value['_id']; ?>"><span class="icon_emp"></span></a>
        </li>
        <li class="hg_padd" style="width:6%">
            <?php
            if (isset($value['is_customer'])&&$value['is_customer']==1&&isset($value['is_supplier'])&&$value['is_supplier']==0)
            {
                echo 'Customer';
            }
            elseif(isset($value['is_customer'])&&$value['is_customer']==0&&isset($value['is_supplier'])&&$value['is_supplier']==1)
            {
                echo 'Supplier';
            }
            elseif(isset($value['is_customer'])&&$value['is_customer']==1&&isset($value['is_supplier'])&&$value['is_supplier']==1)
            {
                echo 'All';
            }
            elseif(isset($value['is_customer'])&&$value['is_customer']==0&&isset($value['is_supplier'])&&$value['is_supplier']==0)
            {
                echo 'Not selected';
            }

            ?>
        </li>
        <li class="hg_padd center_txt" style="width:4%"><?php if(isset($value['no']))echo $value['no']; ?></li>
        <li class="hg_padd" style="width:24%"><?php if(isset($value['name']))echo $value['name']; ?></li>
        <li class="hg_padd" style="width:16%">
            <?php
            if (isset($value['contact_default_id'])){

                if (is_object($value['contact_default_id'])) {
                    if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
                    if( !isset($arr_contact_tmp[(string)$value['contact_default_id']]) ){
                        $arr_contact = $model_contact->select_one(array('_id' => $value['contact_default_id']), array('_id', 'first_name', 'last_name'));
                        if(isset($arr_contact['first_name'])){
                            $arr_contact_tmp[(string)$value['contact_default_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
                            echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
                        }
                    }else{
                        echo $arr_contact_tmp[(string)$value['contact_default_id']];

                    }
                }

            }
            ?>
        </li>
        <li class="hg_padd" style="width:8%"><?php if(isset($value['phone']))echo $value['phone']; ?></li>
        <li class="hg_padd" style="width:23%">
            <?php if (isset($value['our_rep_id'])){

                if (is_object($value['our_rep_id'])) {
                    if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
                    if( !isset($arr_contact_tmp[(string)$value['our_rep_id']]) ){
                        $arr_contact = $model_contact->select_one(array('_id' => $value['our_rep_id']), array('_id', 'first_name', 'last_name'));
                        if(isset($arr_contact['first_name'])){
                            $arr_contact_tmp[(string)$value['our_rep_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
                            echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
                        }
                    }else{
                        echo $arr_contact_tmp[(string)$value['our_rep_id']];

                    }
                }

            }
            ?>
        </li>
        <li class="hg_padd center_txt" style="width:4%">
            <div class="select_inner width_select" style="width: 100%; margin: 0;margin-left: 23px;">
                <label class="m_check2">
                    <?php
                    echo $this->Form->input('Contact.inactive', array(
                        'type' => 'checkbox',
                        'checked' => isset($value['inactive']) ? $value['inactive'] : false
                    ));
                    ?>
                </label>
            </div>
        </li>

        <?php if( $this->Common->check_permission('<?php echo $controller; ?>_@_entry_@_delete', $arr_permission) ){ ?>
        <li class="hg_padd" style="width:3%">
            <div class="middle_check">
                <a href="javascript:void(0)" title="Delete link" onclick="companies_lists_delete('<?php if(isset($value['_id']))echo $value['_id']; ?>')">
                    <span class="icon_remove2"></span>
                </a>
            </div>
        </li>
        <?php } ?>

    </ul>
<?php endforeach; ?>

<?php echo $this->element('popup/pagination_lists'); ?>