 <?php if(isset($arr_salesinvoice)){ ?>
 <div class="clear_percent_11 float_left right_pc">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Sales invoices for this company'); ?></h4></span>
			<?php if( $this->Common->check_permission('salesinvoices_@_entry_@_add', $arr_permission) ){ ?>
			<a href="<?php echo URL; ?>/companies/salesinvoice_add/<?php echo $company_id; ?>" title="Add new sales order">
				<span class="icon_down_tl top_f"></span>
			</a>
			<?php } ?>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1%"></li>
			<li class="hg_padd" style="width:4%"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd" style="width:7%"><?php echo translate('Type'); ?></li>
			<li class="hg_padd center_txt" style="width:8%"><?php echo translate('Date'); ?></li>
			<li class="hg_padd" style="width:8%"><?php echo translate('Status'); ?></li>
			<li class="hg_padd" style="width:8%"><?php echo translate('Our rep'); ?></li>
			<li class="hg_padd" style="width:29%"><?php echo translate('Comments'); ?></li>
			<li class="hg_padd right_txt" style="width:8%"><?php echo translate('Total'); ?></li>
			<li class="hg_padd right_txt" style="width:8%"><?php echo translate('Receipts'); ?></li>
			<li class="hg_padd right_txt" style="width:7%"><?php echo translate('Balance'); ?></li>
		</ul>
		<div class="container_same_category" style="height:206px;overflow-y:auto;">
			<?php
			$i = 1; $count = 0; $total_sub_balance = $total_receipt = $balance = 0;
			foreach ($arr_salesinvoice as $key => $value) {
				$sub_balance = 0;
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_salesinvoice_<?php echo $value['_id']; ?>">
				<li class="hg_padd" style="width:1%;">
					<a href="<?php echo URL; ?>/salesinvoices/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd center_txt" style="width:4%;"><?php echo $value['code']; ?></li>
				<li class="hg_padd center_txt" style="width:7%;"><?php if(isset($value['type']))echo $value['type']; ?></li>
				<li class="hg_padd center_txt center_txt" style="width:8%;">
					<?php echo $this->Common->format_date($value['invoice_date']->sec,false); ?>
				</li>
				<li class="hg_padd center_txt" style="width:8%"><?php if(isset($value['invoice_status']))echo $value['invoice_status']; ?></li>
				<li class="hg_padd center_txt" style="width:8%">
					<?php
						if (isset($value['our_rep_id'])){
							if (is_object($value['our_rep_id'])) {
								if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
								if( !isset($arr_contact_tmp[(string)$value['our_rep_id']]) ){
									$arr_contact = $model_contact->select_one(array('_id' => $value['our_rep_id']), array('_id', 'first_name', 'last_name'));
									if(isset($arr_contact['first_name'])){
										$arr_contact_tmp[(string)$value['our_rep_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
										echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
									}
								}else{
									echo $arr_contact_tmp[(string)$value['our_rep_id']];
								}
							}

						}
					?>
				</li>
				<li class="hg_padd" style="width:29%"><?php if(isset($value['other_comment']))echo $value['other_comment']; ?></li>
				<li class="hg_padd right_txt" style="width:8%">
					<?php if(isset($value['sum_amount']) && is_numeric($value['sum_amount'])){
							echo number_format($value['sum_amount'], 2);
							$total_sub_balance += $value['sum_amount'];
							if( isset($value['invoice_status']) && $value['invoice_status'] == "Invoiced" ){
								$sub_balance = $value['sum_amount'];
								$balance += $sub_balance;
							}
						}
					?>
				</li>
				<li class="hg_padd right_txt" style="width:8%">
					<?php if(isset($value['total_receipt']) && is_numeric($value['total_receipt'])){
							echo number_format($value['total_receipt'], 2);
							$total_receipt += $value['total_receipt'];
						}
					?>
				</li>
				<li class="hg_padd right_txt" style="width:7%"><?php echo number_format($sub_balance, 2); ?></li>
			</ul>

			<?php $i = 3 - $i; $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>
		<span class="title_block bo_ra2">
			<span class="float_left left_text">
				<?php echo translate('Click to view full details'); ?>
			</span>
			<span class="bt_block float_right no_bg">
				<?php echo translate('Total'); ?>
				<input type="text" class="input_w2 right_txt" value="<?php echo number_format($total_sub_balance, 2); ?>" readonly="true">
				<input type="text" class="input_w2 right_txt" value="<?php echo number_format($total_receipt, 2); ?>" readonly="true">
				<input type="text" class="input_w2 right_txt" value="<?php echo number_format($balance, 2); ?>" readonly="true">
			</span>
		</span>
	</div>
</div>
<div id="account_related" class="clear_percent_10 float_left no_right">
	<div class="tab_1 full_width" id="company_sc_related">

		<!-- NEU CO ACCOUNT ROI-->
		<?php if(isset($this->data['Salesaccount'])){
			echo $this->element('..'.DS.'Companies'.DS.'account_related');
		}else{ ?>
		<!-- NEU CHUA CO ACCOUNT -->
		<div class="title_block bo_ra1">
			<span class="title_block_inner">
				<h4><?php echo translate('Account related'); ?></h4>
			</span>
			<?php if( $this->Common->check_permission('salesaccounts_@_entry_@_add', $arr_permission) ){ ?>
			<span class="title_block_inner3 center_txt">
				<input class="btn_pur" type="button" value="Create account" onclick="company_sc_create()">
			</span>
			<?php } ?>
		</div>
		<div class="tab_2_inner">
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Account'); ?></span>
				</p><div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Salesaccount.status', array(
							'class' => 'input_select',
							'readonly' => true
					)); ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Account balance'); ?></span>
				</p><div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Salesaccount.balance', array(
							'class' => 'input_1 float_left',
							'readonly' => true
					)); ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Credit limit'); ?></span>
				</p><div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Salesaccount.credit_limit', array(
							'class' => 'input_1 float_left',
							'readonly' => true
					)); ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Difference'); ?></span>
				</p><div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Salesaccount.difference', array(
							'class' => 'input_1 float_left',
							'readonly' => true
					)); ?>
					<span class="icon_search_ip float_right" title="Not implemented yet"></span>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Payment terms'); ?></span>
				</p><div class="width_in4 float_left indent_input_tp">
					<div class="once_colum top_se">
						<?php echo $this->Form->input('Salesaccount.payment_terms', array(
							'class' => 'input_select',
							'readonly' => true
						)); ?>
					</div>
					<div class="two_colum">
						<input class="input_1 float_left" type="text" value="days" readonly="true">
						<span class="icon_search_ip float_right" title="Not implemented yet"></span>
					</div>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Default Tax code'); ?></span>
				</p><div class="width_in4 float_left indent_input_tp">
					<div class="once_colum top_se">
						<?php echo $this->Form->input('Salesaccount.tax_code', array(
							'class' => 'input_select',
							'readonly' => true
						)); ?>
					</div>
					<div class="two_colum">
						<input class="input_1 float_left" type="text" readonly="true">
					</div>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2"><?php echo translate('Default nominal code'); ?></span>
				</p><div class="width_in4 float_left indent_input_tp">
					<div class="once_colum top_se">
						<?php echo $this->Form->input('Salesaccount.nominal_code', array(
							'class' => 'input_select',
							'readonly' => true
						)); ?>
					</div>
					<div class="two_colum">
						<input class="input_1 float_left" type="text" readonly="true">
					</div>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2 fixbor3" style="padding-bottom: 6px;"><?php echo translate('Tax no'); ?></span>
				</p><div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Salesaccount.tax_no', array(
							'class' => 'input_select',
							'readonly' => true,
							'style' => 'width: 99%;'
					)); ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left minw_lab2 fixbor3" style="padding-bottom: 6px;"><?php echo translate('Quotation limit'); ?></span>
				</p><div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Salesaccount.quotation_limit', array(
							'class' => 'input_select',
							'readonly' => true,
							'style' => 'width: 99%;'
					)); ?>
				</div>
			<p></p>
			<p class="clear"></p>
		</div>
		<span class="title_block bo_ra2"></span>
		<?php } ?>

	</div><!--END Tab1 -->
</div>

<script type="text/javascript">

// co quyen EDIT
<?php if( $this->Common->check_permission('salesaccounts_@_entry_@_edit', $arr_permission) ){ ?>
$(function(){
	<?php if(isset($this->data['Salesaccount'])){ ?>
	company_sc_auto_save();
	<?php } ?>
});

function company_sc_auto_save(){
	$(":input", "#account_related").change(function(){
		$.ajax({
			url: "<?php echo URL; ?>/companies/account_auto_save/" + $("#CompanyId").val(),
			type: 'post',
			data: $(":input", "#account_related").serialize(),
			success: function(html){
				// if( html != "ok" ){
				// 	alerts("Error:", html);
				// }
				$("#company_sc_related").html(html);
				company_sc_auto_save();
			}
		});
	});
}
// co quyen VIEW
<?php }else{ ?>

$(function(){
	$(":input", "#input_sc_disabled").each(function() {
		$(this).attr("disabled", true);
	});
	$(".combobox_button, .indent_dw_m, .icon_down_new", "#input_sc_disabled").each(function() {
		$(this).remove();
	});
});
<?php } ?>

<?php if( $this->Common->check_permission('salesaccounts_@_entry_@_add', $arr_permission) ){ ?>
function company_sc_create(){
	$.ajax({
		url: "<?php echo URL; ?>/companies/account_create/" + $("#CompanyId").val(),
		type: 'post',
		data: $(":input", "#account_related").serialize(),
		success: function(html){
			$("#company_sc_related").html(html);
			company_sc_auto_save();
		}
	});
}
<?php } ?>
</script>

<?php } ?>
<!-- <br> -->
 <?php if(isset($arr_purchaseorder)){ ?>

<!-- Purchase Order -->
<div class="clear_percent_11 float_left right_pc">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Purchase orders for this company'); ?></h4></span>
			<a href="<?php echo URL; ?>/companies/orders_add_purchasesorder/<?php echo $company_id; ?>" title="Add new purchases order">
				<span class="icon_down_tl top_f"></span>
			</a>
		</span>
		<p class="clear"></p>

		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1%;"></li>
			<li class="hg_padd center_txt" style="width:3%;"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd center_txt center_txt" style="width:8%;"><?php echo translate('Required date'); ?></li>
			<li class="hg_padd center_txt center_txt" style="width:8%;"><?php echo translate('Delivery date'); ?></li>
			<li class="hg_padd center_txt center_txt" style="width:6%;"><?php echo translate('Status'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Supplier contact'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Our Rep'); ?></li>
			<li class="hg_padd center_txt" style="width:7%"><?php echo translate('Total (bf.Tax)'); ?></li>
			<li class="hg_padd center_txt" style="width:42%"><?php echo translate('Heading'); ?></li>
			<li class="hg_padd center_txt bor_mt" style="width:1%;"></li>
		</ul>
		<div class="container_same_category" style="height:176px;overflow-y:auto;">
			<?php
			$i = 1; $count = 0;
			foreach ($arr_purchaseorder as $key => $value) {
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_purchasesorder_<?php echo $value['_id']; ?>">
				<li class="hg_padd" style="width:1%;">
					<a href="<?php echo URL; ?>/purchaseorders/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd" style="width:3%;"><?php echo $value['code']; ?></li>
				<li class="hg_padd center_txt center_txt" style="width:8%;">
					<?php echo $this->Common->format_date($value['required_date']->sec, false); ?>
				</li>
				<li class="hg_padd center_txt center_txt" style="width:8%;"><?php if(isset($value['delivery_date']) && is_object($value['delivery_date']))echo $this->Common->format_date($value['delivery_date']->sec, false); ?></li>
				<li class="hg_padd center_txt" style="width:6%;"><?php if(isset($value['purchase_orders_status'])) echo $value['purchase_orders_status']; ?></li>
				<li class="hg_padd center_txt" style="width:6%;"><?php echo $value['ship_to_contact_name']; ?></li>
				<li class="hg_padd center_txt" style="width:6%;">
					<?php
						if (isset($value['our_rep_id'])){

							if (is_object($value['our_rep_id'])) {
								if(!isset($arr_contact_tmp))$arr_contact_tmp = array();
								if( !isset($arr_contact_tmp[(string)$value['our_rep_id']]) ){
									$arr_contact = $model_contact->select_one(array('_id' => $value['our_rep_id']), array('_id', 'first_name', 'last_name'));
									if(isset($arr_contact['first_name'])){
										$arr_contact_tmp[(string)$value['our_rep_id']] = $arr_contact['first_name'].' '.$arr_contact['last_name'];
										echo $arr_contact['first_name'].' '.$arr_contact['last_name'];
									}
								}else{
									echo $arr_contact_tmp[(string)$value['our_rep_id']];

								}
							}
						}
					?>
				</li>
				<li class="hg_padd right_txt" style="width:7%;"><?php echo ($value['sum_sub_total'] == 0 ? 0 : number_format($value['sum_sub_total'],2,'.',',')); ?></li>
				<li class="hg_padd" style="width:42%;"><?php echo $value['name']; ?></li>
				<li class="hg_padd bor_mt  center_txt" style="width:1%;">
					<div class="middle_check">
						<a title="Delete link" href="javascript:void(0)" onclick="purchasesorder_remove_purchasesorder_company('<?php echo $value['_id']; ?>')">
							<span class="icon_remove2"></span>
						</a>
					</div>
				</li>
			</ul>

			<?php $i = 3 - $i; $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>

		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
				<?php echo translate('Click to view full details'); ?>
			</span>
		</span>
	</div>
</div>
<script type="text/javascript">
function purchasesorder_remove_purchasesorder_company(purchasesorder_id){
	confirms( "Message", "Are you sure you want to delete?",
		function(){

			$.ajax({
				 url: '<?php echo URL; ?>/companies/orders_delete/' + purchasesorder_id + '/Salesorder',
				 timeout: 15000,
				 success: function(html){
					 if(html == "ok"){
						 $("#Company_purchasesorder_" + purchasesorder_id).fadeOut();
					 }else{
						 alerts("Error: ", html);
					 }
				 }
			 });
		},function(){
			//else do somthing
	});
}
</script>

<?php } ?>