    <div class="clear_percent_6a float_left">
        <div class="full_width">
            <div class="tab_1 full_width">
                <span class="title_block bo_ra1">
                    <span class="fl_dent"><h4><?php echo translate('Jobs for this company'); ?></h4></span>
                    <?php if( $this->Common->check_permission('jobs_@_entry_@_add', $arr_permission) ){ ?>
                    <a href="<?php echo URL; ?>/companies/jobs_add/<?php echo $company_id; ?>" title="Add new job"><span class="icon_down_tl top_f"></span></a>
                    <?php } ?>
                </span>

            <div class="container_same_category">
                <ul class="ul_mag clear bg3">
                    <li class="hg_padd center_txt" style="width: 3%"></li>
                    <li class="hg_padd center_txt" style="width: 4%"><?php echo translate('Job no'); ?></li>
                    <li class="hg_padd center_txt" style="width: 30%"><?php echo translate('Job name'); ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Job type'); ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Start'); ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Finish'); ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Status'); ?></li>
                    <li class="hg_padd center_txt" style="width: 14%"><?php echo translate('Job manager'); ?></li>
                </ul>
                <?php
                $i = 1; $count = 0;
                foreach ($arr_job as $key => $value) {
                ?>
                <ul class="ul_mag clear bg<?php echo $i; ?>" id="Job_<?php echo $value['_id']; ?>">
                    <li class="hg_padd" style="width: 3%">
                        <a href="<?php echo URL; ?>/jobs/entry/<?php echo $value['_id']; ?>">
                            <span class="icon_emp"></span>
                        </a>
                    </li>
                    <li class="hg_padd" style="width: 4%"><?php echo $value['no']; ?></li>
                    <li class="hg_padd" style="width: 30%"><?php echo $value['name']; ?></li>
                    <li class="hg_padd" style="width: 10%"><?php echo $value['type']; ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php if(isset($value['work_start']) && strlen($value['work_start']) > 0)echo $this->Common->format_date( $value['work_start']->sec, false); ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php if(isset($value['work_end']) && strlen($value['work_end']) > 0)echo $this->Common->format_date( $value['work_end']->sec, false); ?></li>
                    <li class="hg_padd center_txt" style="width: 10%"><?php echo $value['status']; ?></li>
                    <li class="hg_padd" style="width: 14%"><?php echo $value['contacts'][$value['contacts_default_key']]['contact_name']; ?></li>
                </ul>

                <?php $i = 3 - $i; $count += 1;
                }

                $count = 8 - $count;
                if( $count > 0 ){
                    for ($j=0; $j < $count; $j++) { ?>
                    <ul class="ul_mag clear bg<?php echo $i; ?>">
                        <li class="hg_padd center_txt" style="width: 3%"></li>
                        <li class="hg_padd center_txt" style="width: 4%"></li>
                        <li class="hg_padd center_txt" style="width: 30%"></li>
                        <li class="hg_padd center_txt" style="width: 10%"></li>
                        <li class="hg_padd center_txt" style="width: 10%"></li>
                        <li class="hg_padd center_txt" style="width: 10%"></li>
                        <li class="hg_padd center_txt" style="width: 10%"></li>
                        <li class="hg_padd center_txt" style="width: 14%"></li>
                    </ul>
              <?php $i = 3 - $i;
                    }
                }
                ?>
            </div>

                <span class="title_block bo_ra2">
                    <span class="bt_block">
                        <?php echo translate('Click to view full details'); ?>
                    </span>
                </span>
            </div>
        </div>
    </div>
    <div class="clear_percent_7a float_left">
        <div class="tab_1 full_width">
            <span class="title_block bo_ra1">
                <span class="float_left">
                    <span class="fl_dent"><h4><?php echo translate('Defaults for new jobs'); ?></h4></span>
                </span>
            </span>
            <div class="tab_2_inner">
                <p class="clear">
                    <span class="label_1 float_left minw_lab2"><?php echo translate('Markup rate'); ?></span>
                    </p>
                    <div class="width_in3a float_left indent_input_tp">
                        <?php echo $this->Form->input('Company.markup_rate', array(
                            'class' => 'input_1 float_left',
                            'value' => isset($arr_company_job['markup_rate'])?$arr_company_job['markup_rate']:'',
                            'onchange' => 'company_jobs_save_default_for_new_job(this, "markup_rate", "'.$arr_company_job['_id'].'" )'
                        )); ?>
                    </div>
                <p></p>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 "><?php echo translate('Change rate per hour'); ?></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                        <?php echo $this->Form->input('Company.rate_per_hour', array(
                            'class' => 'input_1 float_left',
                            'value' => isset($arr_company_job['rate_per_hour'])?$arr_company_job['rate_per_hour']:'',
                            'onchange' => 'company_jobs_save_default_for_new_job(this, "rate_per_hour", "'.$arr_company_job['_id'].'" )'
                        )); ?>
                    </div>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 "></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                    </div>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 "></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                    </div>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 "></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                    </div>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 "></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                    </div>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 "></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                    </div>
                <p class="clear">
                    <span class="label_1 float_left minw_lab2 " style="height: 11px;"></span>
                    </p><div class="width_in3a float_left indent_input_tp">
                    </div>

                <p></p>
                <p class="clear"></p>
            </div>
            <span class="title_block bo_ra2"></span>
        </div>
    </div>
<script type="text/javascript">
<?php if($this->Common->check_permission($controller.'_@_jobs_tab_@_edit', $arr_permission)){?>
function company_jobs_save_default_for_new_job(object, field, company_id){
    $.ajax({
        url: '<?php echo URL; ?>/companies/jobs_save_default',
        timeout: 15000,
        type:"post",
        data: { "key": field, "company_id": company_id, value: $(object).val() },
        success: function(html){
            if(html != "ok"){
                alerts("Error: ", html);
            }
            console.log(html); // view log when debug
        }
    });
}
<?php } else {?>
$(function(){
    $("input", "#<?php echo $controller; ?>_sub_content").each(function() {
        $(this).attr("disabled", true).css("background-color", "transparent");
    });
});
<?php } ?>
</script>