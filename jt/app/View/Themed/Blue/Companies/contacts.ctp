<style type="text/css">
ul.ul_mag li.hg_padd {
	overflow: visible !important;
}
.bg4 {
  background: none repeat scroll 0 0 #949494;
  color: #fff;
}

.bg4 span h4 {
  margin-left: 1%;
  width: 100%;
}
#companies_sub_content .combobox {
	margin-top: 1px !important;
}
</style>

	<div class="full_width">
		<div class="tab_1 full_width">

			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo translate('Contacts for this company'); ?></h4></span>

				<?php if( $this->Common->check_permission('contacts_@_entry_@_add', $arr_permission) ){ ?>
				<?php echo $this->Js->link( '<span class="icon_down_tl top_f"></span>', URL.'/companies/contacts_add/'.$company_id.'/'.$company_info['name'],
					array(
					   'update' => '#companies_sub_content',
					   'title' => 'Add new contact',
					   'escape' => false
					)
				); ?>
				<?php } ?>
			</span>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd" style="width: 3%"></li>
				<li class="hg_padd center_txt" style="width: 6%"><?php echo translate('Title'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('First Name'); ?></li>
				<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Last Name'); ?></li>
				<li class="hg_padd center_txt" style="width: 3%"><?php echo translate('Default'); ?></li>
				<li class="hg_padd center_txt" style="width: 7%"><?php echo translate('Position'); ?></li>
				<li class="hg_padd center_txt" style="width: 8%"><?php echo translate('Direct dial'); ?></li>
				<li class="hg_padd center_txt" style="width: 3%"><?php echo translate('Ext no'); ?></li>
				<li class="hg_padd center_txt" style="width: 8%"><?php echo translate('Mobile'); ?></li>
				<li class="hg_padd center_txt" style="width: 14%"><?php echo translate('Email address'); ?></li>
				<li class="hg_padd center_txt" style="width: 6%"><?php echo translate('Mail Fax'); ?></li>
				<li class="hg_padd center_txt" style="width: 3%"><?php echo translate('Inactive'); ?></li>
				<li class="hg_padd bor_mt" style="width: 3%"></li>
			</ul>

			<div id="companies_contacts" class="container_same_category">
				<?php
				$i = 1; $count = 0;

				// Hiển thị contact default đầu tiên trên cùng
				if( isset($contact_default) ){
					$value = $contact_default;
					$key = $value['_id'];

				?>
				<?php echo $this->Form->create('Contact', array('id' => 'ContactEntryForm_'.$key)); ?>
				<?php echo $this->Form->hidden('Contact._id', array( 'value' => $value['_id'] )); ?>

				<ul class="ul_mag clear bg<?php echo $i; ?>" id="companies_contacts_<?php echo $key; ?>">
					<li class="hg_padd" style="width: 3%">
						<?php if( $this->Common->check_permission('contacts_@_entry_@_view', $arr_permission) ){ ?>
						<a href="<?php echo URL; ?>/contacts/entry/<?php echo $value['_id']; ?>">
							<span class="icon_emp"></span>
						</a>
						<?php } ?>
					</li>
					<li class="hg_padd" style="width: 6%">
						<?php echo $this->Form->input('Contact.title', array(
								'class' => 'input_select bg'.$i,
								'value' => $value['title'],
								'style' => 'border-bottom:none; margin-top:-3px'
						)); ?>
						<?php echo $this->Form->hidden('Contact.title_id'); ?>
						<script type="text/javascript">
							$(function () {
								$("#ContactTitle", "#companies_contacts_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_contacts_title); ?>);
							});
						</script>
					</li>
					<li class="hg_padd" style="width: 10%">
						<?php echo $this->Form->input('Contact.first_name', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['first_name']
						)); ?>
					</li>
					<li class="hg_padd" style="width: 10%">
						<?php echo $this->Form->input('Contact.last_name', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['last_name']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<input type="hidden" name="data[Company][contact_default]" id="CompanyContactDefault_" value="0">
						<div class="select_inner width_select" style="width: 100%; margin: 0;margin-left: 23px;">
							<label class="m_check2">
								<?php echo $this->Form->input('Company.contact_default', array(
										'type' => 'checkbox',
										'checked' => ($contact_default_id == (string)$value['_id'])?true:false,
										'class' => 'checkbox-default'
								));?>
								<span></span>
							</label>
						</div>
					</li>
					<li class="hg_padd" style="width: 7%">
						<?php echo $this->Form->input('Contact.position', array(
								'class' => 'input_select bg'.$i,
								'value' => $value['position'],
						)); ?>
						<?php echo $this->Form->hidden('Contact.position_id'); ?>
						<script type="text/javascript">
							$(function () {
								$("#ContactPosition", "#companies_contacts_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_contacts_position); ?>);
							});
						</script>
					</li>
					<li class="hg_padd center_txt" style="width: 8%">
						<?php echo $this->Form->input('Contact.direct_dial', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['direct_dial']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<?php echo $this->Form->input('Contact.extension_no', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['extension_no']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 8%">
						<?php echo $this->Form->input('Contact.mobile', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['mobile']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 14%">
						<?php echo $this->Form->input('Contact.email', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['email']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 2%">
						<a href="<?php echo URL; ?>/contacts/create_letter/<?php echo $value['_id']; ?>">
							<img src="/img/mailfax_letter.png">
						</a>
						</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<a href="<?php echo URL; ?>/contacts/create_fax/<?php echo $value['_id']; ?>">
							<img src="/img/mailfax_fax.png">
						</a>
						</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<input type="hidden" name="data[Contact][inactive]" id="ContactInactive_" value="0">
						<div class="select_inner width_select" style="width: 100%; margin: 0;margin-left: 23px;" style="display:none" id="inactive_checkbox_<?php echo $value['_id']; ?>">
							<label class="m_check2">
								<?php echo $this->Form->input('Contact.inactive', array(
										'type' => 'checkbox',
										'checked' => $value['inactive']
								));?>
								<span></span>
							</label>
						</div>
					</li>
					<li class="hg_padd bor_mt" style="width: 3%">

						<div class="middle_check checkbox_delete" id="checkbox_delete_<?php echo $value['_id']; ?>" <?php if( $contact_default_id == (string)$value['_id'] ){ ?>style="display:none"<?php } ?>>
							<a title="Delete link" href="javascript:void(0)" onclick="companies_contacts_delete('<?php echo $value['_id']; ?>')">
								<span class="icon_remove2"></span>
							</a>
						</div>

					</li>
				</ul>
				<?php echo $this->Form->end();
					$i = 3 - $i;  $count += 1;
				}

				foreach ($arr_contact as $key => $value) {
					if( $contact_default_id == (string)$value['_id'] )continue;
				?>

				<?php echo $this->Form->create('Contact', array('id' => 'ContactEntryForm_'.$key)); ?>
				<?php echo $this->Form->hidden('Contact._id', array( 'value' => $value['_id'] )); ?>

				<ul class="ul_mag clear bg<?php echo $i; ?>" id="companies_contacts_<?php echo $key; ?>">
					<li class="hg_padd" style="width: 3%">
						<a href="<?php echo URL; ?>/contacts/entry/<?php echo $value['_id']; ?>">
							<span class="icon_emp"></span>
						</a>
					</li>
					<li class="hg_padd" style="width: 6%">
						<?php echo $this->Form->input('Contact.title', array(
								'class' => 'input_select bg'.$i,
								'value' => $value['title'],
								'style' => 'border-bottom:none; margin-top:-3px'
						)); ?>
						<?php echo $this->Form->hidden('Contact.title_id'); ?>
						<script type="text/javascript">
							$(function () {
								$("#ContactTitle", "#companies_contacts_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_contacts_title); ?>);
							});
						</script>
					</li>
					<li class="hg_padd" style="width: 10%">
						<?php echo $this->Form->input('Contact.first_name', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['first_name']
						)); ?>
					</li>
					<li class="hg_padd" style="width: 10%">
						<?php echo $this->Form->input('Contact.last_name', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['last_name']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<input type="hidden" name="data[Company][contact_default]" id="CompanyContactDefault_" value="0">
						<div class="select_inner width_select" style="width: 100%; margin: 0;margin-left: 23px;">
							<label class="m_check2">
								<?php echo $this->Form->input('Company.contact_default', array(
										'type' => 'checkbox',
										'checked' => false,
										'class' => 'checkbox-default'
								));?>
								<span></span>
							</label>
						</div>
					</li>
					<li class="hg_padd" style="width: 7%">
						<?php echo $this->Form->input('Contact.position', array(
								'class' => 'input_select bg'.$i,
								'value' => $value['position'],
						)); ?>
						<?php echo $this->Form->hidden('Contact.position_id'); ?>
						<script type="text/javascript">
							$(function () {
								$("#ContactPosition", "#companies_contacts_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_contacts_position); ?>);
							});
						</script>
					</li>
					<li class="hg_padd center_txt" style="width: 8%">
						<?php echo $this->Form->input('Contact.direct_dial', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['direct_dial']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<?php echo $this->Form->input('Contact.extension_no', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['extension_no']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 8%">
						<?php echo $this->Form->input('Contact.mobile', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['mobile']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 14%">
						<?php echo $this->Form->input('Contact.email', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['email']
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width: 2%">
						<a href="<?php echo URL; ?>/contacts/create_letter/<?php echo $value['_id']; ?>">
							<img src="/img/mailfax_letter.png">
						</a>
						</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<a href="<?php echo URL; ?>/contacts/create_fax/<?php echo $value['_id']; ?>">
							<img src="/img/mailfax_fax.png">
						</a>
						</li>
					<li class="hg_padd center_txt" style="width: 3%">
						<input type="hidden" name="data[Contact][inactive]" id="ContactInactive_" value="0">
						<div class="checkbox_inactive select_inner width_select" style="width: 100%; margin: 0;margin-left: 23px;" id="inactive_checkbox_<?php echo $value['_id']; ?>" >
							<label class="m_check2">
								<?php echo $this->Form->input('Contact.inactive', array(
										'type' => 'checkbox',
										'checked' => $value['inactive']
								));?>
								<span></span>
							</label>
						</div>
					</li>

					<?php if( $this->Common->check_permission('contacts_@_entry_@_delete', $arr_permission) ){ ?>
					<li class="hg_padd bor_mt" style="width: 3%">
						<?php if( $contact_default_id != (string)$value['_id'] ){ ?>
						<div class="middle_check checkbox_delete" id="checkbox_delete_<?php echo $value['_id']; ?>" >
							<a title="Delete link" href="javascript:void(0)" onclick="companies_contacts_delete('<?php echo $value['_id']; ?>')">
								<span class="icon_remove2"></span>
							</a>
						</div>
						<?php } ?>
					</li>
					<?php } ?>

				</ul>
				<?php echo $this->Form->end();
					$i = 3 - $i;  $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>">
						</ul>
			  <?php $i = 3 - $i;
					}
				}
				?>
			</div>

			<span class="hit"></span>

			<ul class="ul_mag clear bg4 bo_ra2">
				<span class="icon_vwie indent_down_vwie2">
					<?php echo translate('Click to view contact'); ?>
				</span>
			</ul>

		</div><!--END Tab1 -->
	</div>

<script type="text/javascript">
$(function(){
	// co quyen EDIT
	<?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
	$("form :input", "#companies_sub_content").change(function() {

		if( $(this).attr("class") == "checkbox-default" ){

			var contain = $(this).closest('form');
			var contact_id = $("#ContactId", contain).val();
			$( ".checkbox-default" ).prop('checked', false);
			$( this ).prop('checked', true);

			$(".checkbox_inactive").show();
			$("#inactive_checkbox_" + contact_id).hide();

			$(".checkbox_delete").show();
			$("#checkbox_delete_" + contact_id).hide();

			// lưu contact default cho company table
			$.ajax({
				url: '<?php echo URL; ?>/companies/contacts_save_default/<?php echo $company_id; ?>/' + contact_id,
				timeout: 15000,
				success: function(html){
					if( html != "ok" )alerts("Error: ", html);
				}
			});
		}

		$.ajax({
			url: '<?php echo URL; ?>/companies/contacts_auto_save',
			timeout: 15000,
			type:"post",
			data: $(this).closest('form').serialize(),
			success: function(html){
				if( html != "ok" )alerts("Error: ", html);
			}
		});
	});

	// co quyen VIEW
	<?php }else{ ?>
		$("form :input", "#<?php echo $controller; ?>_sub_content").each(function() {
			$(this).attr("disabled", true).css("background-color", "transparent");
		});
		$(".combobox_button, .indent_dw_m", "#<?php echo $controller; ?>_sub_content").each(function() {
			$(this).remove();
		});
	<?php } ?>
});

function companies_adddress_update_entry(object){
	// thay đổi địa chỉ của entry trên luôn
	var data_form = $(object).closest('form').serializeArray();
	$("#CompanyDefaultAddress1").val(data_form[6]["value"]);
	$("#CompanyDefaultAddress2").val(data_form[7]["value"]);
	$("#CompanyDefaultAddress3").val(data_form[8]["value"]);
	$("#CompanyDefaultTownCity").val(data_form[9]["value"]);
	$("#CompanyDefaultZipPostcode").val(data_form[10]["value"]);
	$("#CompanyDefaultProvinceState").val(data_form[11]["value"]);
	$("#CompanyDefaultCountry").val(data_form[12]["value"]);
	companies_auto_save_entry();
}

<?php if( $this->Common->check_permission('contacts_@_entry_@_delete', $arr_permission) ){ ?>
function companies_contacts_delete(contact_id){
	confirms( "Message", "Are you sure you want to delete?",
		function(){
			$.ajax({
				url: '<?php echo URL; ?>/companies/contacts_delete/' + contact_id,
				success: function(html){
					if(html == "ok"){
						$("#companies_contacts_" + contact_id).fadeOut();
					}else{
						console.log(html);
					}
				}
			});
		},function(){
			//else do somthing
	});
}
<?php } ?>
</script>