<?php if(isset($arr_product_supplier)){ ?>
	<style>
	.combobox{
		margin-top: 4px;
	}
	</style>

	<script type="text/javascript">
	    $(function(){
	           $("form#supplier_also_supplies input,select").change(function() {
	               var fieldname ='supplier_also_supplies';
	               var values = $(this).val();
	               var ids = $("#CompanyId").val();


	               $.ajax({
	                   url: '<?php echo URL; ?>/<?php echo $controller;?>/save_data_for_non_model',
	                   timeout: 15000,
	                   type: "POST",
	                   data: { fieldname : fieldname,values:values,ids:ids },
	                   success: function(html){
	                      console.log(html);
	                   }
	               });
	               return false;
	           //	console.log(fieldname+"_"+values+"_"+ids);
	           });
	    });
	</script>
	<div class="float_left " style=" width:74%; float: left;">

		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo translate('Products / services from this supplier'); ?></h4></span>
			</span>
			<p class="clear"></p>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd" style="width:1%;"></li>
				<li class="hg_padd" style="width:6%;"><?php echo translate('Code'); ?></li>
				<li class="hg_padd left_txt" style="width:24%;"><?php echo translate('Name / details'); ?></li>
				<li class="hg_padd left_txt" style="width:8%;"><?php echo translate('SKU'); ?></li>
				<li class="hg_padd left_txt" style="width:8%;"><?php echo translate('Type'); ?></li>
				<li class="hg_padd left_txt" style="width:5%;"><?php echo translate('Size-W'); ?></li>
				<li class="hg_padd" style="width:3%;"></li>
				<li class="hg_padd left_txt" style="width:5%;"><?php echo translate('Size-H'); ?></li>
				<li class="hg_padd" style="width:3%;"></li>
				<li class="hg_padd left_txt" style="width:6%;"><?php echo translate('Sold by'); ?></li>
				<li class="hg_padd right_txt" style="width:9%;"><?php echo translate('Cost price'); ?></li>
				<li class="hg_padd right_txt" style="width:8%;"><?php echo translate('Unit price'); ?></li>
			</ul>
			<div class="container_same_category" style="height: 176px;overflow-y: auto;">
				<?php
				$i = 1; $count = 0;
				foreach ($arr_product_supplier as $key => $value) {
				?>
				<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_salesorder_<?php echo $value['_id']; ?>">
					<li class="hg_padd" style="width:1%;">
						<a href="<?php echo URL; ?>/products/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
					</li>
					<li class="hg_padd" style="width:6%;">
						<?php if(isset($value['code']))echo $value['code']; ?>
					</li>
					<li class="hg_padd left_txt" style="width:24%;">
						<?php if(isset($value['name']))echo $value['name']; ?>
					</li>
					<li class="hg_padd left_txt" style="width:8%;">
						<?php if(isset($value['sku']))echo $value['sku']; ?>
					</li>
					<li class="hg_padd left_txt" style="width:8%;">
						<?php if(isset($value['product_type']))echo $value['product_type']; ?>
					</li>
					<li class="hg_padd right_txt" style="width:5%;">
						<?php if(isset($value['sizew']) && $value['sizew'] > 0){
								$v_sizew=$value['sizew'];
								echo number_format($v_sizew,2);
							} ?>
					</li>
					<li class="hg_padd" style="width:3%;">
					 <?php if(isset($value['sizew_unit']))echo $value['sizew_unit']; ?>
					</li>
					<li class="hg_padd right_txt" style="width:5%;">
						<?php if(isset($value['sizeh']) && $value['sizeh'] > 0){
								$v_sizeh=$value['sizeh'];
								echo number_format($v_sizeh,2);
							} ?>
					</li>
					<li class="hg_padd" style="width:3%;">
						<?php if(isset($value['sizeh_unit']))echo $value['sizeh_unit']; ?>
					</li>
					<li class="hg_padd left_txt" style="width:6%;">
					<?php if(isset($value['sell_by']))echo $value['sell_by']; ?>
					</li>
					<li class="hg_padd right_txt" style="width:9%;">
					<?php if(isset($value['sell_price']) && $value['sell_price'] > 0)echo number_format($value['sell_price'],2); ?>
					</li>
					<li class="hg_padd right_txt" style="width:8%;">
					<?php if(isset($value['unit_price']) && $value['unit_price'] > 0)echo number_format(str_replace(',', '', $value['unit_price']),2); ?>
					</li>
				</ul>
				<?php $i = 3 - $i;
				 $count += 1;
					}
					$count = 8 - $count;
					if( $count > 0 ){
						for ($j=0; $j < $count; $j++) { ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>">
						</ul>
				  <?php $i = 3 - $i;
						}
					}
				?>
			</div>

			<span class="title_block bo_ra2">
				<span class="float_left bt_block">
					<?php echo translate('Click to view full details'); ?>
				</span>
			</span>
		</div>
	</div>
	<div class="float_left " style=" width:25%; float: right;">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo translate('Supplier also supplies'); ?></h4></span>
			</span>
			<p class="clear"></p>


	        <form method="POST" id="supplier_also_supplies">
			<div class="container_same_category" style="height: 197px;overflow-y: auto;">
				<?php
				$i = 1; $count = 0; $vitri = 0;
				if( isset($arr_company['supplier_also_supplies']) ){
	                foreach( $arr_company['supplier_also_supplies'] as $key => $value ) {

	                    if( !$value['deleted']) {
	                ?>
	                <ul class="ul_mag clear bg<?php echo $i; ?>" id="supplier_also_supplies_<?php echo $key; ?>">
	                    <?php echo $this->Form->input('Supplier.supply', array(
	                                            'class' => 'input_select bg'.$i,
	                                            'value' => $value['value']
	                                        )); ?>
	                                        <?php //echo $this->Form->hidden('Supplier.supply_id', array('value' => $value['supply_id'])); ?>
	                                        <script type="text/javascript">
	                                            $(function () {
	                                                $("#SupplierSupply", "#supplier_also_supplies_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_supplier_also_supplies); ?>);
	                                            });
	                                        </script>
	                </ul>
	                  <?php
	                  }
	                  $i = 3 - $i;
	                    $vitri += 1;
	                    }
	                }
				?>

				<?php   $i = 3 - $i;
					    $max = (isset($vitri)?$vitri:0);
						for( $j=$max; $j < 9; $j++  ) {
						    $key = $j;
						 ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>" id="supplier_also_supplies_<?php echo $key; ?>">
						    <?php echo $this->Form->input('Supplier.supply', array(
	                                        'class' => 'input_select bg'.$i,
	                                    )); ?>
	                                    <?php echo $this->Form->hidden('Supplier.supply_id'); ?>
	                                    <script type="text/javascript">
	                                        $(function () {
	                                            $("#SupplierSupply", "#supplier_also_supplies_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_supplier_also_supplies); ?>);
	                                        });
	                                    </script>
						</ul>
				  <?php $i = 3 - $i;
						}
				?>

			</div>
	        </form>




			<span class="title_block bo_ra2">
				<span class="float_left bt_block">
					<?php echo translate('Enter keywords to allow searching on.'); ?>
				</span>
			</span>
		</div>
	</div>
<?php } ?>

<?php if(isset($arr_product_supplier)&&isset($arr_product_customer)){ ?>
<br><div style="clear:both"></div><br>
<?php } ?>

<?php if(isset($arr_product_customer)){ ?>
	<div class="float_left " style=" width:16%; float: left;">
		<div class="tab_1 full_width" style="">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Pricing category'); ?></h4></span>
				</span>
			</span>
			<div class="tab_2_inner" style="height:198px;" id="company_product_left">
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Use category'); ?></span>
				</p>
				<div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Company.sell_category', array(
							'class' => 'input_select',
							'onchange' => 'company_products_save_left(this)',
							'readonly' => true
					)); ?>
					<?php echo $this->Form->hidden('Company.sell_category_id'); ?>
					<script type="text/javascript">
						$(function () {
							$("#CompanySellCategory").combobox(<?php echo json_encode($arr_products_sell_category); ?>);
						});
					</script>
				</div>
				<p></p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 fixbor4"><?php echo translate('Discount %'); ?></span>
				</p>
				<div class="width_in3a float_left indent_input_tp">
					<?php echo $this->Form->input('Company.discount', array(
							'class' => 'input_1 float_left',
							'onchange' => 'company_products_save_left(this)',
					)); ?>
				</div>

				<script type="text/javascript">
				function company_products_save_left(){
					$.ajax({
						url: "<?php echo URL; ?>/companies/products_save_left/" + $("#CompanyId").val(),
						timeout: 15000,
						type: "POST",
						data: $( ":input", "#company_product_left").serialize(),
						success: function(html){
							if(html != "ok"){
								alerts( "Errors: ", html );
							}
						}
					});
				}
				</script>

				<p class="clear">
					<span class="label_1 float_left minw_lab2 "></span>
				</p><div class="width_in3a float_left indent_input_tp">
				</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "></span>
				</p><div class="width_in3a float_left indent_input_tp">
				</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "></span>
				</p><div class="width_in3a float_left indent_input_tp">
				</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "></span>
				</p><div class="width_in3a float_left indent_input_tp">
				</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 " style="height: 37px;"></span>
				</p><div class="width_in3a float_left indent_input_tp">
				</div>

				<p></p>
				<p class="clear"></p>
			</div>
			<span class="title_block bo_ra2"></span>
		</div>
	</div>
	<div class="float_left" style=" width:49%;margin-left:1%;float: left;">
		<div class="tab_1 full_width">

			<span class="title_block bo_ra1">
				<span class="fl_dent">
					<h4><?php echo translate('Specific pricing for this customer'); ?></h4>
				</span>
				<span class="icon_down_tl top_f" id="click_to_opent_window_choose_product"></span>
				<script type="text/javascript">
					$(function(){
						window_popup('products', 'Specify Product', '', 'click_to_opent_window_choose_product', "?no_supplier=1&products_product_type=Product");
					});
					function after_choose_products(product_id){
						window.location = "<?php echo URL; ?>/companies/products_pricing/" + $("#CompanyId").val() + "/0/" + product_id;
					}
				</script>
			</span>

			<p class="clear"></p>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd" style="width:3%;"></li>
				<li class="hg_padd" style="width:9%;">Code</li>
				<li class="hg_padd" style="width:56%;">Name</li>
				<li class="hg_padd center_txt" style="width:11%;">Range</li>
				<li class="hg_padd right_txt" style="width:8%;">Unit price</li>
				<li class="hg_padd bor_mt" style="width:1.8%;"></li>
			</ul>
			<div id="company_pricing_container_same" class="container_same_category" style="height: 176px;overflow-y: auto;">
				<?php
					$i = 1; $count = 0;
					if( isset($this->data['Company']['pricing']) ){

						foreach ($this->data['Company']['pricing'] as $key => $value) {

							if( $value['deleted'] )continue;
						?>
						<ul class="ul_mag clear bg<?php echo $i; ?>" id="company_pricing_<?php echo $key; ?>">
							<li class="hg_padd" style="width:3%;">
								<a href="<?php echo URL; ?>/companies/products_pricing/<?php echo $company_id; ?>/<?php echo $key; ?>">
									<span class="icon_emp"></span>
								</a>
							</li>
							<li class="hg_padd" style="width:9%;"><?php echo $value['code']; ?></li>
							<li class="hg_padd" style="width:56%;"><?php echo $value['name']; ?></li>
							<li class="hg_padd center_txt" style="width:11%;">
								<?php
									$range = '';
									if( isset($value['price_break']) && isset($value['price_break'][0]) ){
										foreach ($value['price_break'] as $val_price_break) {
											if( $val_price_break['deleted'] )continue;
											$range = $val_price_break['range_from'].' - '.$val_price_break['range_to'];
											break;
										}
									}
									echo $range;
								?>
							</li>
							<li class="hg_padd right_txt" style="width:8%;">
								<?php
									$unit_price = '';
									if( isset($value['price_break']) && isset($value['price_break'][0]) && is_numeric($value['price_break'][0]['unit_price']) ){
										foreach ($value['price_break'] as $val_price_break) {
											if( $val_price_break['deleted'] )continue;
											$unit_price = number_format($val_price_break['unit_price'], 2);
											break;
										}
									}
									echo $unit_price;
								?>
							</li>
							<li class="hg_padd bor_mt" style="width:1.8%;">
								<div class="middle_check">
									<a title="Delete link" href="javascript:void(0)" onclick="company_price_delete(<?php echo $key; ?>)">
										<span class="icon_remove2"></span>
									</a>
								</div>
							</li>
						</ul>
						<?php $i = 3 - $i;
							$count += 1;
						}
					}

					$count = 8 - $count;
					if( $count > 0 ){
						for ($j=0; $j < $count; $j++) { ?>
							<ul class="ul_mag clear bg<?php echo $i; ?>">
							</ul>
							<?php $i = 3 - $i;
						}
					}
				?>
			</div>
			<span class="title_block bo_ra2">
				<span class="float_left bt_block">
					<?php echo translate('Click to view full details'); ?>
				</span>
			</span>
		</div>

		<script type="text/javascript">
		function company_price_delete(key){
			confirms( "Message", "Are you sure you want to delete?",
				function(){
					$.ajax({
						 url: '<?php echo URL; ?>/companies/products_delete/' + $("#CompanyId").val() + "/" + key,
						 timeout: 15000,
						 success: function(html){
							 if(html == "ok"){
								$("#company_pricing_" + key).remove();
							 }else{
								alerts("Error: ", html);
							 }
						 }
					 });
				},function(){
					//else do somthing
			});
		}
		</script>
	</div>
	<div class="float_left " style=" width:33%;margin-left:1%; float: left;">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Units / assets for this customer'); ?></h4></span>
				</span>
			</span>
			<p class="clear"></p>
			<ul class="ul_mag clear bg3">
			</ul>
			<div class="container_same_category" style="height: 176px;overflow-y: auto;">
				<?php
				$i = 1; $count = 0;

					?>
					<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_salesorder">
					</ul>
					<?php $i = 3 - $i;
					$count += 1;
				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>">
						</ul>
						<?php $i = 3 - $i;
					}
				}
				?>
			</div>
			<span class="title_block bo_ra2">
				<span class="float_left bt_block">
					<?php echo translate('Click to view full details'); ?>
				</span>
			</span>
		</div>
	</div>
<?php } ?>