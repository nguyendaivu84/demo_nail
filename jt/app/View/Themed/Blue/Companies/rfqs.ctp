<div class="full_width">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Supplier RFQ\'s (Requtest for quotes)'); ?></h4></span>
		</span>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width: 1%"></li>
			<li class="hg_padd center_txt" style="width: 3%"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd center_txt" style="width: 6%"><?php echo translate('Date'); ?></li>
			<li class="hg_padd center_txt" style="width: 6%"><?php echo translate('Deadline'); ?></li>
			<li class="hg_padd center_txt" style="width: 7%"><?php echo translate('Status'); ?></li>
			<li class="hg_padd center_txt" style="width: 2%"><?php echo translate('Late'); ?></li>
			<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Supplier contact'); ?></li>
			<li class="hg_padd center_txt" style="width: 8%"><?php echo translate('Supplier quote ref'); ?></li>
			<li class="hg_padd center_txt" style="width: 6%"><?php echo translate('Price quoted'); ?></li>
			<li class="hg_padd center_txt" style="width: 10%"><?php echo translate('Employee'); ?></li>
			<li class="hg_padd center_txt" style="width: 4%"><?php echo translate('Code'); ?></li>
			<li class="hg_padd center_txt" style="width: 19%"><?php echo translate('Name / details'); ?></li>
			<li class="hg_padd center_txt" style="width: 4%"><?php echo translate('Quote ref'); ?></li>
		</ul>

		<?php
		$i = 1; $count = 0;
		foreach ($arr_rfqs as $key => $rfqs) {
			foreach ($rfqs['rfqs'] as $rfqs_key => $value) {

				if( (string)$value['company_id'] != $company_id )continue;
		?>
		<ul class="ul_mag clear bg<?php echo $i; ?>">
			<li class="hg_padd" style="width: 1%">
				<a href="<?php echo URL; ?>/quotations/rfqs_entry/<?php echo $rfqs['_id']; ?>/<?php echo $rfqs_key; ?>">
					<span class="icon_emp"></span>
				</a>
			</li>

			<li class="hg_padd center_txt" style="width: 3%"><?php echo $value['rfq_no']; ?></li>
			<li class="hg_padd center_txt" style="width: 6%"><?php echo $this->Common->format_date($value['rfq_date']->sec); ?></li>
			<li class="hg_padd center_txt" style="width: 6%"><?php echo $this->Common->format_date($value['deadline_date']->sec); ?></li>
			<li class="hg_padd center_txt" style="width: 7%"><?php echo $value['rfq_status']; ?></li>
			<li class="hg_padd center_txt" style="width: 2%">
				<?php if(1==2){ ?>
					<span class="Late">X</span>
				<?php } ?>
			</li>
			<li class="hg_padd center_txt" style="width: 10%"><?php if(isset($value['contact_name']))echo $value['contact_name']; ?></li>
			<li class="hg_padd center_txt" style="width: 8%"><?php if(isset($value['supplier_quote_ref']))echo $value['supplier_quote_ref']; ?></li>
			<li class="hg_padd center_txt" style="width: 6%"><?php if(isset($value['unit_price_quoted']) && is_numeric($value['unit_price_quoted']))echo number_format($value['unit_price_quoted'], 2); ?></li>
			<li class="hg_padd center_txt" style="width: 10%"><?php if(isset($value['employee_name']))echo $value['employee_name']; ?></li>
			<li class="hg_padd center_txt" style="width: 4%"></li>
			<li class="hg_padd center_txt" style="width: 19%"><?php if(isset($rfqs['rfqs'][$value['include_name_details']]['product_name']))echo $rfqs['rfqs'][$value['include_name_details']]['product_name']; ?></li>
			<li class="hg_padd" style="width:4%">
				<a style="color: blue" href="<?php echo URL; ?>/quotations/entry/<?php echo $rfqs['_id']; ?>">
					<span class="icon_emp" style="float: left;"></span>
				</a><?php if(isset($rfqs['code']))echo $rfqs['code']; ?>
			</li>

		</ul>
		<?php $i = 3 - $i; $count += 1;
			}
		}

		$count = 8 - $count;
		if( $count > 0 ){
			for ($j=0; $j < $count; $j++) { ?>
			<ul class="ul_mag clear bg<?php echo $i; ?>">
			</ul>
	  <?php $i = 3 - $i;
			}
		}
		?>

		<span class="hit"></span>
		<span class="title_block bo_ra2">
			<span class="bt_block">
				<?php echo translate('Click to view full details'); ?>
			</span>
		</span>
	</div>
</div>