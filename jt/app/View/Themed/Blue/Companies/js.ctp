<?php echo $this->element('js/permission_entry'); ?>
<script type="text/javascript">
	$(function() {

		companies_update_entry_header();
	});

	function companies_update_entry_header() {
		$("#company_name_header").html($("#CompanyName").val());

		if ($.trim($("#CompanyPhone").val()).length > 0) {
			$("#company_phone_header").html($("#CompanyPhone").val());
			$("#company_phoneprefix_header").show();
		}
	}

	function companies_auto_save_entry(object) {
		if ($.trim($("#CompanyHeading").val()) == "") {
			$("#CompanyHeading").val("#" + $("#CompanyNo").val() + "-" + $("#CompanyCompanyName").val());
		}

		// checkbox customer or employee
		if (object != undefined) {
			if ($(object).attr("id") == "CompanyIsCustomer") {
				$("#contacts").click();
			} else if ($(object).attr("id") == "CompanyIsSupplier") {
				$("#contacts").click();
			}
		}

		companies_update_entry_header();
		$("form :input", "#companies_form_auto_save").removeClass('error_input').removeClass('error_color ');
		$(".jt_ajax_note").hide();

		companies_adddress_update_addresses_in_subtab();

		$.ajax({
			url: '<?php echo URL; ?>/companies/auto_save',
			timeout: 15000,
			type: "post",
			data: $("form", "#companies_form_auto_save").serialize(),
			success: function(html) {

				if ($.trim(html) == "ref_no_existed") {
					$("#CompanyNo").addClass('error_color');
					alerts('Message', 'This "no" existed');

				} else if (html == "email_not_valid") {
					$("#CompanyEmail").addClass('error_input');
					ajax_note_set('Email not valid, please check email field!');

				} else if (html == "company_exists") {
					$("#CompanyName").addClass('error_input');
					ajax_note_set('This company existed!');

				} else if (html == "company_no_exists") {
					$("#CompanyNo").addClass('error_input');
					ajax_note_set('This no existed!');

				} else if (html != "ok") {
					alerts('Message', html);
				}
				console.log(html); // view log when debug
			}
		});
	}

	function companies_adddress_update_addresses_in_subtab(){

		var CompanyAddressesDefaultKey = $("#CompanyAddressesDefaultKey").val();

		// kiểm tra xem tab Addresses có active không
		if( $("#companies_addresses_" + CompanyAddressesDefaultKey).attr("id") != undefined ){

			var contain = $("#companies_addresses_" + CompanyAddressesDefaultKey);
			var contain_entry_panel = $("#companies_form_auto_save");
			// thay đổi địa chỉ default

			$("#AddressAddress1", contain).val($("#DefaultAddress1", contain_entry_panel).val());
			$("#AddressAddress2", contain).val($("#DefaultAddress2", contain_entry_panel).val());
			$("#AddressAddress3", contain).val($("#DefaultAddress3", contain_entry_panel).val());
			$("#AddressTownCity", contain).val($("#DefaultTownCity", contain_entry_panel).val());
			$("#AddressZipPostcode", contain).val($("#DefaultZipPostcode", contain_entry_panel).val());

			// kiểm tra xem có đổi country không để load lại danh sách tỉnh thành
			if( $("#DefaultCountryId", contain_entry_panel).val() != $("#Address" +CompanyAddressesDefaultKey+ "CountryId", contain).val() ){
				change_province(CompanyAddressesDefaultKey, $("#DefaultCountryId", contain_entry_panel).val());
				setTimeout("companies_adddress_update_addresses_in_subtab_country_province(" +CompanyAddressesDefaultKey+ ")", 1800);

			}else{
				companies_adddress_update_addresses_in_subtab_country_province(CompanyAddressesDefaultKey);
			}
		}
	}

	function companies_adddress_update_addresses_in_subtab_country_province(CompanyAddressesDefaultKey){
		console.log(CompanyAddressesDefaultKey);
		var contain = $("#companies_addresses_" + CompanyAddressesDefaultKey);
		var contain_entry_panel = $("#companies_form_auto_save");
		$("#Address" +CompanyAddressesDefaultKey+ "Country", contain).val($("#DefaultCountry", contain_entry_panel).val());
		$("#Address" +CompanyAddressesDefaultKey+ "CountryId", contain).val($("#DefaultCountryId", contain_entry_panel).val());
		$("#Address" +CompanyAddressesDefaultKey+ "ProvinceState", contain).val($("#DefaultProvinceState", contain_entry_panel).val());
		$("#Address" +CompanyAddressesDefaultKey+ "ProvinceStateId", contain).val($("#DefaultProvinceStateId", contain_entry_panel).val());
	}
</script>