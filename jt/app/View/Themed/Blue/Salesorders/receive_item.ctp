<style type="text/css">
	ul.ul_mag li.hg_padd {
		overflow: visible !important;
	}
	.bg4 {
		background: none repeat scroll 0 0 #949494;
		color: #fff;
	}

	.bg4 span h4 {
		margin-left: 1%;
		width: 100%;
	}
</style>
<?php echo $this->element('../' . $name . '/tab_option'); ?>
<div class="tab_1 full_width" style="margin:1% 1%; width:98%;">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
	            Sales Order : <?php if(isset($salesorder_code)) echo $salesorder_code;?>
            </h4>
        </span>
    </span>
	<p class="clear"></p>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:7%"><?php echo translate('Code'); ?></li>
		<li class="hg_padd" style="width:30%"><?php echo translate('Name / detail'); ?></li>
		<li class="hg_padd right_txt" style="width:6%"><?php echo translate('Original qty'); ?></li>
		<div class="float_left" style="width:15%;">
			<div class="tab_title_purchasing">
				<span class="block_purcharsing"><?php echo translate('Invoice') ; ?></span>
			</div>
			<div>
				<li class="hg_padd line_mg right_txt" style="width:50%">Invoiced</li>
				<li class="hg_padd line_mg right_txt" style="width:47%">Balance</li>
			</div>
		</div>
		<div class="float_left" style="width:15%;">
			<div class="tab_title_purchasing">
				<span class="block_purcharsing"><?php echo translate('Shipping') ; ?></span>
			</div>
			<div>
				<li class="hg_padd line_mg right_txt" style="width:50%">Shipped</li>
				<li class="hg_padd line_mg right_txt" style="width:47%">Balance</li>
			</div>
		</div>
		<li class="hg_padd right_txt" style="width:7%"><?php echo translate('Ship/Inv Now'); ?></li>

	</ul>
	<div class="container_same_category mCustomScrollbar _mCS_4">
		<div class="mCustomScrollBox mCS-light" style="position:relative; height:100%; overflow:scroll; max-width:100%;">
			<div class="mCSB_container mCS_no_scrollbar" style="position: relative; top: 0px;">
				<form method="POST" enctype="multipart/form-data" id="form_shipped" name="form_shipped" >

					<div style="height:176px;overflow:auto;">
						<?php
						$i = 1; $count = 0;
						if(is_array($arr_product)){

							foreach($arr_product as $key=>$value){ $id=$key;?>

								<ul class="ul_mag clear bg<?php echo $i; ?>">
									<li class="hg_padd center_txt" style="width: 7%"><?php if(isset($value['code'])) echo $value['code']; ?></li>
									<li class="hg_padd" style="width: 30%"><?php if(isset($value['products_name'])) echo $value['products_name']; ?></li>
									<li class="hg_padd right_txt" style="width: 6%">


										<input disabled type="text" name="quantity[<?php echo $id; ?>]" id="quantity_<?php echo $id; ?>" class="input_inner jt_box_save viewprice_quantity" value="<?php if(isset($value['quantity'])) echo $value['quantity']; ?>" style=" text-align:right; ">

									</li>

									<li class="hg_padd right_txt" style="width: 7%">

										<?php if(isset($value['invoiced'])) echo $value['invoiced']; else echo 0; ?>

									</li>

									<li class="hg_padd right_txt" style="width: 6%">


										<input disabled type="text" name="balance_invoiced[<?php echo $id; ?>]" id="balance_invoiced_<?php echo $id; ?>" class="input_inner jt_box_save viewprice_quantity" value=" <?php if(isset($value['balance_invoiced'])) echo $value['balance_invoiced']; else if(isset($value['quantity'])) echo $value['quantity']; ?>" style=" text-align:right; ">

									</li>





									<li class="hg_padd right_txt" style="width: 7%">

										<?php if(isset($value['shipped'])) echo $value['shipped']; else echo 0; ?>

									</li>

									<li class="hg_padd right_txt" style="width: 6%">

										<input disabled type="text" name="balance_shipped[<?php echo $id; ?>]" id="balance_shipped_<?php echo $id; ?>" class="input_inner jt_box_save viewprice_quantity" value=" <?php if(isset($value['balance_shipped'])) echo $value['balance_shipped']; else if(isset($value['quantity'])) echo $value['quantity']; ?>" style=" text-align:right; ">


									</li>

									<li class="hg_padd right_txt" style="width: 7%">

										<input type="text"  name="shipped[<?php echo $id; ?>]" id="shipped_<?php echo $id; ?>" class="input_inner jt_box_save viewprice_quantity input" value="0" style=" text-align:right; ">

									</li>



								</ul>

								<?php
								$i = 3 - $i; $count += 1;
							}
							$count = 8 - $count;
							if( $count > 0 ){
								for ($j=0; $j < $count; $j++) { ?>
									<ul class="ul_mag clear bg<?php echo $i; ?>">
										<li class="hg_padd" style="width: 7%"></li>
										<li class="hg_padd" style="width: 30%"></li>
										<li class="hg_padd" style="width: 6%"></li>
										<li class="hg_padd" style="width: 7%"></li>
										<li class="hg_padd" style="width: 6%"></li>
										<li class="hg_padd" style="width: 7%"></li>
									</ul>
									<?php $i = 3 - $i;
								}
							}
						}
						?>





						<p class="clear"></p>
					</div>




			</div>
		</div>
	</div>
    <span class="title_block bo_ra2">
        <span class="bt_block float_right no_bg">
            <div class="dent_input float_right">
            	<?php if($this->Common->check_permission('salesinvoices_@_entry_@_edit',$arr_permission)): ?>
	            <input type="radio" name="type_sales" onchange="handleChange();" value="invoice" id="create_invoice" ><label for="create_invoice">Create invoice</label>
	        	<?php endif; ?>
	        	<?php if($this->Common->check_permission('shippings_@_entry_@_edit',$arr_permission)): ?>
	            <input type="radio" name="type_sales" onchange="handleChange();" value="shipping" id="create_shipment"><label for="create_shipment">Create shipment</label>
	        	<?php endif; ?>
	            <?php if($this->Common->check_permission('salesinvoices_@_entry_@_edit',$arr_permission)
	            	&&$this->Common->check_permission('shippings_@_entry_@_edit',$arr_permission)): ?>
	            <input type="radio" name="type_sales" onchange="handleChange();" value="both" checked="1" id="create_both"><label for="create_both">Create both</label>
	        	<?php endif; ?>
	            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </form>

	            <?php if(is_array($arr_product)&&!empty($arr_product)){?>
		            <input type="button" class="btn_pur" id="bt_cancel" onclick="window.location.replace('<?php echo URL; ?>/salesorders/entry')" name="cancel" value="Cancel" style="width: 100px; cursor: pointer">
		            <input type="button" class="btn_pur" onclick="fr_submit();" id="bt_continue" name="continue" value="Continue" style="width: 100px; cursor: pointer">
	            <?php }?>

            </div>
        </span>
    </span>
</div>

<script type="text/javascript">




	function validateNumber(event) {
		var key = window.event ? event.keyCode : event.which;

		if (event.keyCode == 8 || event.keyCode == 46
			|| event.keyCode == 37 || event.keyCode == 39) {
			return true;
		}
		else if ( key < 48 || key > 57 ) {
			return false;
		}
		else return true;
	};

	$(document).ready(function(){
		$('input.input_inner.jt_box_save.viewprice_quantity').keypress(validateNumber);
	});
	var salesorder_id='<?php if(isset($salesorder_id)) echo $salesorder_id;?>';


var j_bool=new Array();
	function handleChange(){


		$.ajax({
			url: "<?php echo URL; ?>/salesorders/check_full_balance/",
			type: 'POST',
			data: $('#form_shipped').serialize(),
			success: function(result) {

				ret = result.split(",");

                for(var i=0 ; i<ret.length-1;i++)
                {
                    res=ret[i].split("_");
                    //$("#shipped_"+res[2]).css({"border" : "1px solid red"});
                    //j_bool.push("1");

                    if(res[1]!=undefined)
                    {
                        $("#shipped_"+i).css({"border" : "1px solid red"});

                        j_bool.push(i);
                    }
                    else
                    {
                         for(var j = j_bool.length - 1; j >= 0; j--) {
                             if(j_bool[j] == i) {
                                j_bool.splice(j, 1);
                             }
                         }
                        $("#shipped_"+i).css({"border" : "0px"});
                    }


                }



			}
		});



	}
	$("form#form_shipped input").change(function(){


		var id=$(this).attr("id");
		var name=$(this).attr("name");
		var value= $(this).val();

		var location = id.split("shipped_");

		var org="#quantity_"+location[1];
		var bl_i="#balance_invoiced_"+location[1];
		var bl_s="#balance_shipped_"+location[1];
		var vl_org=$(org).val();
		var vl_bl_i=$(bl_i).val();
		var vl_bl_s=$(bl_s).val();

		var v_org = parseInt(value);
		var v_vl_org=parseInt(vl_org);


		if(v_org>v_vl_org){

			j_bool.push(parseInt(location[1]));
			$(this).css({"border" : "1px solid red"});
			$(this).focus();

		}
		else if(v_org>vl_bl_i && $("input:radio[name='type_sales']:checked").val()=='invoice'){

			j_bool.push(parseInt(location[1]));
			$(this).css({"border" : "1px solid red"});
			$(this).focus();
		}
		else if(v_org>vl_bl_s && $("input:radio[name='type_sales']:checked").val()=='shipping'){

			j_bool.push(parseInt(location[1]));
			$(this).css({"border" : "1px solid red"});
			$(this).focus();
		}
		else if($("input:radio[name='type_sales']:checked").val()=='both'){
			if(v_org>vl_bl_s || v_org>vl_bl_i){


				j_bool.push(parseInt(location[1]));
				$(this).css({"border" : "1px solid red"});
				$(this).focus();


			}
			else
			{

                 for(var i = j_bool.length - 1; i >= 0; i--) {
                     if(j_bool[i] == location[1]) {
                        j_bool.splice(i, 1);
                     }
                 }
				$(this).css({"border" : "0px"});
				$(this).focus();
			}

		}
		else
		{


               for(var i = j_bool.length - 1; i >= 0; i--) {
                   if(j_bool[i] == location[1]) {
                      j_bool.splice(i, 1);
                   }
               }
			$(this).css({"border" : "0px"});
			$(this).focus();
		}

		// $(id).addClass('error_input');
	});

	<?php if($this->Common->check_permission($controller.'_@_line_entry_tab_@_edit',$arr_permission)
	         ||$this->Common->check_permission($controller.'_@_text_entry_tab_@_edit',$arr_permission)): ?>
	function fr_submit() {
            if(j_bool.length==0){
            			$.ajax({
            				url: "<?php echo URL; ?>/salesorders/receive_item/",
            				type: 'POST',
            				data: $('#form_shipped').serialize(),
            				success: function(result) {
            					console.log(result);return false;
            					console.log(result);
            					if(result!='')
            						window.location.assign("<?php echo URL; ?>"+result+"");


            				}
            			});
            			return false;
            }
            else
            {
                alert("Please enter valid quantities");
            }

	}
	<?php endif; ?>
</script>