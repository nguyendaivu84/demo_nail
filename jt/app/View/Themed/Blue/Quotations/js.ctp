<?php if(!isset($no_alert_input)) echo $this->element('js_entry'); else echo $this->element('js_entry',array('no_alert_input'=>true));?>
<?php echo $this->element('js/permission_product_modules');?>
<script type="text/javascript">
$(function(){
	localStorage["quotation_status"] = $("#quotation_status").val();
	localStorage["quotation_status_id"] = $("#quotation_status_id").val();
	$("#pst_tax").change(function(){
		var ids = $(this).attr("id");
		var val = $(this).val();
		ids = "mx_"+ids;
		$("#"+ids).html(val);
	});

	$("#country").change(function(){
		if($(this).val()!=40)
			$("#province").html('');
	});

	// Xu ly save, update
	$("#quotations_form_auto_save input,#quotations_form_auto_save select").change(function() {
		var fixkendo = $(this).attr('class');

		var fieldname = $(this).attr("name");
		var fieldid = $(this).attr("id");
		var fieldtype = $(this).attr("type");
			modulename = 'mongo_id';
		var ids = $("#"+modulename).val();
		var values = $(this).val();
		var func = ''; var titles = new Array();

		if(ids!='')
			func = 'update'; //add,update
		else
			func = 'add';

		//check address
		var check_address = fieldname.split("[");
		if(check_address[0]=='data'){
			save_address(check_address,values,fieldid,function(){
				change_tax_entry();
			});
			return '';
		}


		//check quotation_date < payment_due_date
		if(fieldname=='quotation_date' || fieldname=='payment_due_date'){
			var quotation_date = convert_date_to_num($('#quotation_date').val());
			var payment_due_date = convert_date_to_num($('#payment_due_date').val());
			if(quotation_date>payment_due_date){
				alerts('Message','<?php msg('DUE_DATE_AND_QUOTE_DATE');?>');
				$(this).css('color','#f00');
				return '';
			}else{
				$(this).css('color','#545353');
			}
		}


		var taxval;
		if(fieldname=='tax'){
			arrva = values.split('%');
			taxval = arrva[0];
			var arrvalue =  {"taxper":taxval};
			update_all_option('products',arrvalue,function(){
				reload_subtab('line_entry');
			});
			values = $('#'+fieldid+'Id').val();
		}

		if(fieldtype=='checkbox'){
			if($(this).is(':checked'))
				values = 1;
			else
				values = 0;
		}
			$(".jt_ajax_note").html("Saving...       ");
			var status = ['Submitted','Completed','Approved'];
			if(fieldname=='quotation_status'&&jQuery.inArray(values,status)>-1)
				$(".jt_ajax_email").removeClass("hidden");
			$.ajax({
				url: '<?php echo URL.'/'.$controller;?>/ajax_save',
				type:"POST",
				data: {field:fieldname,value:values,func:func,ids:ids},
				success: function(text_return){ //alert(text_return);
					$(".jt_ajax_email").addClass("hidden");
					if(text_return.indexOf("||")==-1&&text_return!="email_not_valid"){
						$("#quotation_status").val( localStorage["quotation_status"] );
						$("#quotation_status_id").val( localStorage["quotation_status_id"] );
						alerts('Message',text_return);
						return false;
					}
					text_return = text_return.split("||");
						 if (text_return == "email_not_valid"){
								$("#email").addClass('error_input');
								ajax_note('Email not valid, please check email field!');
						 }else{
							$("#email").removeClass('error_input');
							$("#"+modulename).val(text_return[0]);
							// change tittle, thay đổi tiêu đề của items
							<?php foreach($arr_settings['title_field'] as $ks=>$vls){?>
								titles[<?php echo $ks;?>] = '<?php echo $vls;?>';
							<?php } ?>
							if(titles.indexOf(fieldname)!=-1){
								$("#md_"+fieldname).html(values);
								$(".md_center").html("-");
							}
							ajax_note("Saving...Saved !");

							// if status
							if(fieldname=='quotation_status')
								location.reload();
                            if(fieldname=='name')
								$("form#other_record input#name").val(values);
							if(fieldname=='tax')
								save_field('taxval',taxval,'');

							if(fieldname=='company_name')
								change_tax_entry();
						}



				}
			});

	});

	$(".jt_ajax_note").html('');

	//View and cutom Option value
	$( document ).delegate(".view_option","click",function(){
		view_product_option($(this).attr('rel'));
	});

	<?php if($this->Common->check_permission($controller.'_@_entry_@_edit',$arr_permission)): ?>
	//RFQ's List
	$('#bt_add_rfqs, .entry_menu_add_rfqs ').click(function(){
		var d = new Date();
		var itemid = $('#itemid').val();
		var subitems = $('#subitems').val();
		var employee_id = $('#employee_id').val();
		var employee_name = $('#employee_name').val();
		var quote_code = $('#quote_code').val();
		var sumrfq = parseInt($('#sumrfq').val()); sumrfq = sumrfq+1;
		var dates = parseInt(d.getTime());
			dates = Math.round(dates/1000);
		var datas = {
			'rfq_no' : quote_code+'/'+sumrfq,
			'rfq_code' : subitems,
			'rfq_date' : dates,
			'employee_id' : employee_id,
			'employee_name' : employee_name
		};
		save_option('rfqs',datas,'',0,'rfqs','add',function(sms){
			window.location.assign("<?php echo URL;?>/quotations/rfqs_entry/"+itemid+'/'+(sumrfq-1));
		});

	});
	<?php endif; ?>

});

// Hàm dùng cho module Quotation :=========================================
/*
	after_choose_companies(ids,names,keys)
	after_choose_contacts(ids,names,keys)
	after_choose_jobs(ids,names,keys)
	save_address(arr,values,fieldid)
	save_address_pr(keys)
*/



/**
* Thay đổi tax entry khi province thay đổi
*/
function change_tax_entry(){
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/change_tax_entry',
		dataType: "json",
		type:"POST",
		success: function(jsondata){
			console.log('tesing:');
			console.log(jsondata);
			$('#tax').val(jsondata['texttax']);
			$('#taxId').val(jsondata['keytax']);
			reload_subtab('line_entry');
			/*arrva = values.split('%');
			var arrvalue =  {"taxper":arrva[0]};
			update_all_option('products',arrvalue,function(){
				reload_subtab('line_entry');
			});*/
		}
	});
}




//xử lý sau khi chọn company
//cách xử lý mới
//dùng hàm save_data
function after_choose_companies(ids,names,keys){
	if(keys=='company_name'){
		$("#company_id").val(ids);
		$("#company_name").val(names);
		$("#md_company_name").html(names);
		$("#window_popup_companiescompany_name").data("kendoWindow").close();
		$(".link_to_company_name").addClass('jt_link_on');
		save_data('company_name',names,'',ids,function(arr_ret){

			$(".link_to_contact_name").removeClass('jt_link_on');
			if(arr_ret['contact_id']!='')
				$(".link_to_contact_name").addClass('jt_link_on');
			$("#md_contact_name").html('');
			if(arr_ret['contact_name']!='')
				$("#md_contact_name").html(arr_ret['contact_name']);

			if(arr_ret['tax']!='' && arr_ret['taxtext']!=''){
				$("#tax").val(arr_ret['taxtext']);
			}

			window_popup('contacts', 'Specify Contact','contact_name','click_open_window_contactscontact_name',get_para_contact(),'force_re_install');
			reload_address('invoice_');
			reload_address('shipping_');

			// BaoNam
			reload_payment_term_tax_company(ids);
			reload_subtab('line_entry');
		});

	}
}

// BaoNam
function reload_payment_term_tax_company(company_id){
	$.ajax({
		url: '<?php echo URL;?>/salesaccounts/get_info_company/' + company_id,
		dataType: "json",
		success: function(json){
			if( $.trim(json.payment_terms) != "" ){
				$("#payment_terms").val(json.payment_terms);
				$("#payment_terms_id").val(json.payment_terms_id);
				$("#tax").val(json.tax_code);
				$("#tax_id").val(json.tax_code_id);
				$("#payment_terms").trigger("change");
				$("#tax").trigger("change");
			}
		}
	});
}

// xử lý sau khi chọn contact
//cách xử lý theo 2 hàm độc lập:
//Lấy data(get_data_form_module) + Save data (save_muti_field)

function after_choose_contacts(ids,names,keys){
	var mongoid,func;
	mongoid = $("#mongo_id").val();
	if(mongoid!='')
		func = 'update';
	else
		func = 'add';

	if(keys=='contact_name'){
		$("#window_popup_contactscontact_name").data("kendoWindow").close();
		$("#contact_id").val(ids);
		$("#contact_name").val(names);
		$("#md_contact_name").html(names);
		$(".k-window").fadeOut('slow');
		$(".link_to_contact_name").addClass('jt_link_on');
		save_data('contact_name',names,'',ids,function(arr_ret){

			// BaoNam:
			reload_payment_term_tax_contact(ids);

			/*$(".link_to_contact_name").removeClass('jt_link_on');
			if(arr_ret['contact_id']!='')
				$(".link_to_contact_name").addClass('jt_link_on');
			$("#md_contact_name").html('');
			if(arr_ret['contact_name']!='')
				$("#md_contact_name").html(arr_ret['contact_name']);

			window_popup('contacts', 'Specify Contact','contact_name','click_open_window_contactscontact_name',get_para_contact(),'force_re_install');*/
			//reload_address('invoice_');
			//reload_address('shipping_');
		});

	}
	else if(keys=='our_rep'){
		$("#window_popup_contactsour_rep").data("kendoWindow").close();
		$(".link_to_our_rep").attr("onclick", "window.location.assign('/jobtraq/contacts/entry/"+ids+"')");
		$("#our_rep_id").val(ids);
		$("#our_rep").val(names);
		$("#md_our_rep").html(names);
		$(".link_to_our_rep").addClass('jt_link_on');
		$.ajax({
			url: '<?php echo URL.'/'.$controller;?>/ajax_save',
			type:"POST",
			data: {field:'our_rep_id',value:ids,func:func,ids:mongoid},
			success: function(text_return){
				text_return = text_return.split("||");
				save_field('our_rep',names,text_return[0]);
			}
		});

	}else if(keys=='our_csr'){
		$("#window_popup_contactsour_csr").data("kendoWindow").close();
		$(".link_to_our_csr").attr("onclick", "window.location.assign('/jobtraq/contacts/entry/"+ids+"')");
		$("#our_csr_id").val(ids);
		$("#our_csr").val(names);
		$("#md_our_csr").html(names);
		$(".link_to_our_csr").addClass('jt_link_on');
		$.ajax({
			url: '<?php echo URL.'/'.$controller;?>/ajax_save',
			type:"POST",
			data: {field:'our_csr_id',value:ids,func:func,ids:mongoid},
			success: function(text_return){
				text_return = text_return.split("||");
				save_field('our_csr',names,text_return[0]);
			}
		});
	}
}

// BaoNam:
function reload_payment_term_tax_contact(contact_id){
	if($.trim($("#company_id").val()) == "" && $.trim($("#company_name").val()) == ""){
		$.ajax({
			url: '<?php echo URL;?>/salesaccounts/get_info_contact/' + contact_id,
			dataType: "json",
			success: function(json){
				if( $.trim(json.payment_terms) != "" ){
					$("#payment_terms").val(json.payment_terms);
					$("#payment_terms_id").val(json.payment_terms_id);
					$("#tax").val(json.tax_code);
					$("#tax_id").val(json.tax_code_id);
					$("#payment_terms").trigger("change");
					$("#tax").trigger("change");
				}
			}
		});
	}

}

// xử lý sau khi chọn job,
function after_choose_jobs(ids,names,keys){
	if(keys=='job_name'){

		var module_from = 'Job';
		var arr = {
					"_id"	:"job_id",
					"name"	:"job_name",
					"no"	:"job_number"
				 }; //danh sách các field cần nhận về từ module jobs, và fields cần lưu
		$(".k-window").fadeOut('slow');
		$("#window_popup_jobsjob_name").data("kendoWindow").close();
		$(".link_to_job_name").addClass('jt_link_on');
		save_data_form_to(module_from,ids,arr);
	}
}

// xử lý sau khi chọn job,
function after_choose_salesorders(ids,names,keys){
	if(keys=='salesorder_name'){
		var module_from = 'Salesorder';
		var arr = {
					"_id"	:"salesorder_id",
					"name":"salesorder_name",
					"code"	:"salesorder_number"
				 }; //danh sách các field cần nhận về từ module jobs, và fields cần lưu
		$("#window_popup_salesorderssalesorder_name").data("kendoWindow").close();
		$(".link_to_salesorder_name").addClass('jt_link_on');
		save_data_form_to(module_from,ids,arr);
	}
}


function save_address(arr,values,fieldid,handleData){
	var	keys = arr[1].replace("]","");
	var keyups = keys.charAt(0).toUpperCase() + keys.slice(1);
	var opname = keys + "_address";
	var address_field = arr[2].replace("]","");
	var datas = new Object();
	if(address_field!='invoice_country' && address_field!='shipping_country' && address_field=='invoice_province_state' && address_field=='shipping_province_state'){
		datas[address_field] = values;

	//luu province
	}else if(address_field=='invoice_province_state' || address_field=='shipping_province_state'){
		var vtemp = $("#"+fieldid+'Id').val();
			datas[address_field] = $("#"+fieldid).val();//luu gia tri custom cua province
			datas[address_field+'_id'] = vtemp;
		$("#"+keyups+'ProvinceState').css('border','none');
		$("#"+keyups+'ProvinceState').css('border-bottom','1px solid #dddddd');
		//$("#"+keyups+'ProvinceState').focus();

	//luu country
	}else{
		vtemp = $("#"+fieldid+'Id').val();
		datas[address_field] = $("#"+fieldid).val();
		datas[address_field+'_id'] = vtemp;
		if(vtemp=='CA' || vtemp=='US'){
			$("#"+keyups+'ProvinceState').css('border','1px solid #f00');
			$("#"+keyups+'ProvinceState').focus();
		}else{
			$("#"+keyups+'ProvinceState').css('border','none');
			$("#"+keyups+'ProvinceState').css('border-bottom','1px solid #dddddd');
		}
	}
	var olds = $("#"+opname).val();
	if(olds!=''){
		olds = '';
		idas = '0';
	}else{
		olds = 'add';
		idas = '';
		$("#"+opname).val(values+',');
	}
	save_option(opname,datas,idas,0,'',olds,function(arr_return){
		if(handleData!=undefined)
			handleData(arr_return);
		//save tax
	});
	ajax_note("Saving...Saved !");
}


function save_address_pr(keys){
	var keyups = keys.charAt(0).toUpperCase() + keys.slice(1);
	var fieldid = keyups+'ProvinceState';
	var values = $("#"+fieldid).val();
	var arr = new Array();
	arr[1] = keys+']';
	arr[2] = keys+'_province_state]';
	save_address(arr,values,fieldid);

	$("#"+keyups+'ProvinceState').css('border','none');
	$("#"+keyups+'ProvinceState').css('border-bottom','1px solid #dddddd');
}

function view_product_option(proid){
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/view_product_option',
		type:"POST",
		data: {proid:proid},
		success: function(text_return){
			build_popup(text_return,'Option of product');
		}
	});
}



function after_choose_addresses(ids,names,keys){
		var address = new Object();
		var directs = ['name','address_1','address_2','address_3','town_city','province_state','province_state_id','zip_postcode','country'];
		for(var n in directs){
			address[keys+'_'+directs[n]] = $("#window_popup_addresses_"+names+"_"+directs[n]+'_'+ids+keys).val();
		}
		address[keys+'_country_id'] = parseInt($("#window_popup_addresses_"+names+"_country_id_"+ids+keys).val());
		address[keys+'_default'] = true;
		address['deleted'] = false;

		var address_0={'0':address};
		var invoice_address={'addresses':address_0};
		var jsonString = JSON.stringify(invoice_address);
		var arr_field = {'addresses':keys+'_address'};
		$(".k-window").fadeOut();
		save_muti_field(arr_field,jsonString,'',function(arr_ret){
			ajax_note('Saved.');
			address = arr_ret[keys+'_address'];
			address = address[0];
			for(var i in address){
				$("#"+ChangeFormatId(i)).val(address[i]);
			}
			//save tax
			var ShippingAddId = $("#ShippingProvinceStateId").val()
			if(keys=='shipping' || (keys=='invoice' && ShippingAddId=='')){
				var taxid = address[keys+'_province_state_id'];
				var allListElements = $( 'li[value="'+taxid+'"]' );
				var html = $("#tax").parent().find(allListElements);
				//console.log(taxid);
				//console.log(html);
				var tax = html[0].innerHTML;
				var taxval = tax.split("%");
				taxval = taxval[0];
				$('#tax').val(tax);
				$('#taxId').val(taxid);
				$('#tax').change();
			}
		});
}

</script>