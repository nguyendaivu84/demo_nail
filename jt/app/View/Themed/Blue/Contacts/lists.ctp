<?php echo $this->element('entry_tab_option', array('no_show_delete' => true)); ?>
<?php echo $this->element('js/lists_view'); ?>
<div id="content" class="fix_magr">
    <form method="POST" id="sort_form">
        <div class="w_ul2 ul_res2">
            <ul class="ul_mag clear bg top_header_inner2 ul_res2" id="sort">
                <li class="hg_padd" style="width:1%"></li>
                <li class="hg_padd" style="width:6%">
                    <label>Type</label>
                    <span id="is_employee" class="desc"></span>
                </li>
                <li class="hg_padd" style="width:6%">
                    <label>Title</label>
                    <span id="title" class="desc"></span>
                </li>
                <li class="hg_padd" style="width:6%">
                    <label>Contact</label>
                    <span id="first_name" class="desc"></span>
                </li>
                <li class="hg_padd" style="width:6%">
                    <label>Direct dial</label>
                    <span id="direct_dial" class="desc"></span>
                </li>
                <li class="hg_padd" style="width:6%">
                    <label>Mobile</label>
                    <span id="mobile" class="desc"></span>
                </li>
                <li class="hg_padd center_txt" style="width:12%">
                    <label> Email address</label>
                    <span id="email" class="desc"></span>
                </li>
                <li class="hg_padd center_txt" style="width:3%">
                    <label>Inactive</label>

                </li>
                <li class="hg_padd center_txt" style="width:20%">
                    <label>Company</label>
                    <span id="company" class="desc"></span>
                </li>
                <li class="hg_padd center_txt" style="width:6%">
                    <label>Type</label>
                    <span id="is_customer" class="desc"></span>
                </li>
                <li class="hg_padd bor_mt" style="width:3%"></li>
            </ul>
            <div id="lists_view_content">
                <!-- goi lists ajax -->
                <?php echo $this->element('../Contacts/lists_ajax') ?>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    function contacts_lists_delete(id) {
        confirms("Message", "Are you sure you want to delete?",
                function() {
                    $.ajax({
                        url: '<?php echo URL; ?>/contacts/lists_delete/' + id,
                        timeout: 15000,
                        success: function(html) {
                            if (html == "ok") {
                                $("#contacts_" + id).fadeOut();
                            } else {
                                console.log(html);
                            }
                        }
                    });
                }, function() {
            //else do somthing
        });
        return false;
    }
</script>