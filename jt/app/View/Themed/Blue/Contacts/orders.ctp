<div class="full_width">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent">
				<h4>
					<?php echo translate('Sales orders for this contacts'); ?></h4>
			</span>
			<?php if($this->Common->check_permission('salesorders_@_entry_@_add', $arr_permission)){?>
			<a href="<?php echo URL; ?>/contacts/orders_add/<?php echo $contact_id; ?>" title="Add new sales order">
				<span class="icon_down_tl top_f"></span>
			</a>
			<?php } ?>
		</span>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:3%"></li>
			<li class="hg_padd" style="width:3%"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd center_txt" style="width:9%"><?php echo translate('Date'); ?></li>
			<li class="hg_padd center_txt" style="width:9%"><?php echo translate('Due date'); ?></li>
			<li class="hg_padd" style="width:8%"><?php echo translate('Status'); ?></li>
			<li class="hg_padd" style="width:19%"><?php echo translate('Heading'); ?></li>
			<li class="hg_padd" style="width:19%"><?php echo translate('Comments'); ?></li>
		</ul>
		<div class="container_same_category" style="height: auto;overflow: visible;">
			<?php
			$i = 1; $count = 0;
			foreach ($arr_salesorder as $key => $value) {
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Salesorder_Job_<?php echo $value['_id']; ?>">

				<li class="hg_padd" style="width:3%">
					<a href="<?php echo URL; ?>/salesorders/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd" style="width:3%"><?php echo $value['code']; ?></li>
				<li class="hg_padd line_mg center_txt" style="width:9%"><?php if(isset($value['salesorder_date']) && is_object($value['salesorder_date']))echo $this->Common->format_date($value['salesorder_date']->sec); ?></li>
				<li class="hg_padd line_mg center_txt" style="width:9%"><?php if(isset($value['payment_due_date']) && is_object($value['payment_due_date']))echo $this->Common->format_date($value['payment_due_date']->sec); ?></li>
				<li class="hg_padd" style="width:8%"><?php echo $value['status']; ?></li>
				<li class="hg_padd" style="width:19%"><?php echo $value['name']; ?></li>
				<li class="hg_padd" style="width:19%"><?php if(isset($value['other_comment']))echo $value['other_comment']; ?></li>
			</ul>
			<?php
				 $i = 3 - $i; $count += 1;
			}

			$count = 8 - $count;
			if( $count > 0 ){
				for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
		  <?php $i = 3 - $i;
					}
				}
			?>
		</div>

		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
				<?php echo translate('Click to view full details'); ?>
			</span>
			<span class="bt_block float_left no_bg dent_bl_txt">
				<!-- <?php echo translate('Totals'); ?>
				<input class="input_w2" type="text">
				<input class="input_w2" type="text">
				<input class="input_w2" type="text"></span> -->
		</span>
	</div>
</div>