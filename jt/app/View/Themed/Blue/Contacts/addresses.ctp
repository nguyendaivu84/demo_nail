<style type="text/css">
ul.ul_mag li.hg_padd {
	overflow: visible !important;
}
</style>
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Addresses for this contact'); ?></h4></span>

			<?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
			<?php echo $this->Js->link( '<span class="icon_down_tl top_f"></span>', '/contacts/addresses_add/'.$contact_id,
				array(
					'update' => '#contacts_sub_content',
					'title' => 'Add new address',
					'escape' => false
				) );
			?>
			<div class="float_left hbox_form dent_left_form">
				<input onclick="contacts_addresses_use_company()" class="btn_pur auto_width" type="button" value="<?php echo translate('Use company address as default'); ?>">
			</div>
			<?php } ?>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1.5%"></li>
			<li class="hg_padd" style="width:7%"><?php echo translate('Name'); ?></li>
			<li class="hg_padd center_txt" style="width:4%"><?php echo translate('Default'); ?></li>
			<li class="hg_padd line_mg" style="width:12%"><?php echo translate('Address line 1'); ?></li>
			<li class="hg_padd line_mg" style="width:12%"><?php echo translate('Address line 2'); ?></li>
			<li class="hg_padd line_mg" style="width:12%"><?php echo translate('Address line 3'); ?></li>
			<li class="hg_padd line_mg" style="width:10%"><?php echo translate('Town / City'); ?></li>
			<li class="hg_padd line_mg" style="width:10%"><?php echo translate('Province / State'); ?></li>
			<li class="hg_padd line_mg" style="width:8%"><?php echo translate('Zip / Post code'); ?></li>
			<li class="hg_padd line_mg" style="width:10%;"><?php echo translate('Country'); ?></li>
			<li class="hg_padd bor_mt" style="width:1.5%"></li>
		</ul>
		<div id="contacts_addresses" class="container_same_category">
			<?php $count = 0;$i = 1;  $k = 0;
			if(isset($arr_contact['addresses'])){
				foreach($arr_contact['addresses'] as $key => $value){
					$k=$k+1;
					if( $value['deleted'] )continue;
				?>

				<?php echo $this->Form->create('Address', array('id' => 'AddressEntryForm_'.$key)); ?>
				<?php echo $this->Form->hidden('Address.key', array( 'value' => $key )); ?>
				<?php echo $this->Form->hidden('Address._id', array( 'value' => $contact_id )); ?>

				<ul class="ul_mag clear bg<?php echo $i; ?>" id="contacts_addresses_<?php echo $key; ?>">
					<li class="hg_padd" style="width:1.5%">
						<a href="javascript:void(0)" onclick="addresses_subtab_run_map('<?php echo $key; ?>')">
							<span class="icosp_addr"></span>
						</a>
					</li>
					<li class="hg_padd" style="width:7%">
						<?php echo $this->Form->input('Address.name', array(
							'class' => 'input_select bg'.$i,
							'value' => $value['name'],
						)); ?>
						<?php echo $this->Form->hidden('Address.name_id'); ?>
						<script type="text/javascript">
					        $(function () {
					            $("#AddressName", "#contacts_addresses_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_addresses_name); ?>);
					        });
					    </script>
					</li>
					<li class="hg_padd center_txt" style="width:4%">
						<input type="hidden" name="data[Address][default]" id="AddressDefault_" value="0">
						<div class="select_inner width_select" style="width: 100%; margin: 0;margin-left: 23px;">
							<label class="m_check2">
								<?php echo $this->Form->input('Address.default', array(
											'type' => 'checkbox',
											'checked' => $value['default'],
											'class' => 'checkbox-default'
								));?>
								<span></span>
							</label>
						</div>
					</li>
					<li class="hg_padd line_mg" style="width:12%">
						<?php echo $this->Form->input('Address.address_1', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['address_1']
						)); ?>
					</li>
					<li class="hg_padd line_mg" style="width:12%">
						<?php echo $this->Form->input('Address.address_2', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['address_2']
						)); ?>
					</li>
					<li class="hg_padd line_mg" style="width:12%">
						<?php echo $this->Form->input('Address.address_3', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['address_3']
						)); ?>
					</li>
					<li class="hg_padd line_mg" style="width:10%">
						<?php echo $this->Form->input('Address.town_city', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['town_city']
						)); ?>
					</li>
					<li class="hg_padd line_mg" style="width:10%">
						<?php echo $this->Form->input('Address.'.$key.'.province_state', array(
							'class' => 'input_select bg'.$i,
							'value' => $value['province_state'],
						)); ?>
						<?php echo $this->Form->hidden('Address.'.$key.'.province_state_id', array('value' => $value['province_state_id'])); ?>
						<script type="text/javascript">
					        $(function () {
					        	<?php
					        		$arr_province = array();
					        		if( isset($arr_all_province[$value['country_id']]) ){
					        			$arr_province = $arr_all_province[$value['country_id']];
					        		}
					        	?>
					            $("#Address<?php echo $key; ?>ProvinceState", "#contacts_addresses_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_province); ?>);
					        });
					    </script>
					</li>
					<li class="hg_padd line_mg" style="width:8%">
						<?php echo $this->Form->input('Address.zip_postcode', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['zip_postcode']
						)); ?>
					</li>
					<li class="hg_padd" style="width:10%;">
						<?php echo $this->Form->input('Address.'.$key.'.country', array(
								'class' => 'country input_select bg'.$i,
								'value' => $value['country'],
								// 'onchange' => 'update_ajax_province_input(this, "#Address'.$key.'ProvinceStateId", "contain")',
								'readonly' => true
						)); ?>
						<?php echo $this->Form->hidden('Address.'.$key.'.country_id', array('value' => $value['country_id'])); ?>
						<script type="text/javascript">
					        $(function () {
					            $("#Address<?php echo $key; ?>Country", "#contacts_addresses_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_country); ?>);
					        });
					    </script>
					</li>

					<?php if( $this->Common->check_permission('contacts_@_addresses_tab_@_delete', $arr_permission) ){ ?>
					<li class="hg_padd bor_mt" style="width:1.5%">
						<?php if(isset($value['default']) && !$value['default'] ){ ?>
						<div class="middle_check">
							<a title="Delete link" href="javascript:void(0)" onclick="contacts_adddress_delete(<?php echo $key; ?>)">
								<span class="icon_remove2"></span>
							</a>
						</div>
						<?php } ?>
					</li>
					<?php } ?>
				</ul>

				<?php echo $this->Form->end(); ?>

				<?php $i = 3 - $i; $count += 1;
					}
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>">
						</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>
		<span class="hit"></span>
		<span class="title_block bo_ra2">
			<span class="float_left bt_block"><a href=""><?php echo translate('Click to view full details'); ?></a></span>
			<span class="float_left left_text dent_bl_txt"><?php echo translate('Note: You can assign a default address for each contact for this contact or enter specific addresses for them as well'); ?></span>
		</span>
	</div><!--END Tab1 -->

<script type="text/javascript">

$(function(){

	<?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
	$("form :input", "#contacts_sub_content").change(function() {

		var contain = $(this).closest('form');
		$("#AddressCountryName", contain).val($("#AddressCountry option[value='"+ $("#AddressCountry", contain).val() +"']", contain).text());
		$("#AddressProvinceStateName", contain).val($("#AddressProvinceState option[value='"+ $("#AddressProvinceState", contain).val() +"']", contain).text());
		if( $(this).attr("class") == "checkbox-default" ){

			$(".checkbox-default").prop('checked', false);
			$(this).prop('checked', true);

			// submit all forms
			$("form", "#contacts_sub_content").each(function(){
				$.ajax({
					url: '<?php echo URL; ?>/contacts/addresses_auto_save',
					timeout: 15000,
					type:"post",
					data: $(this).serialize(),
					success: function(html){
						if( html != "ok" )alerts("Error: ", html);
						console.log(html);
					}
				});
			});

			contacts_adddress_update_entry(this);

		}else{

			$.ajax({
				url: '<?php echo URL; ?>/contacts/addresses_auto_save',
				timeout: 15000,
				type:"post",
				data: $(this).closest('form').serialize(),
				success: function(html){
					if( html != "ok" )alerts("Error: ", html);
				}
			});

			// nếu address chỉnh sửa là default của contact thì sửa luôn contact
			var data_form = $(this).closest('form');
			if( $("#AddressDefault", contain).prop("checked") ){
				contacts_adddress_update_entry(this);
			}
		}

		change_province_begin_do(this);
	});

	<?php }else{ ?>
	$("input", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).attr("disabled", true).css("background-color", "transparent");
	});
	$(".combobox_button, .indent_dw_m, .icon_down_new", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).remove();
	});
	<?php } ?>
});

function addresses_subtab_run_map( key ){
	var contain = $("#contacts_addresses_" + key);
	var address_1 = $("#AddressAddress1", contain).val();
	var address_2 = $("#AddressAddress2", contain).val();
	var address_3 = $("#AddressAddress3", contain).val();
	var town_city = $("#AddressTownCity", contain).val();
	var province_state = $("#Address" + key + "ProvinceState", contain).val();
	var zip_postcode = $("#AddressZipPostcode", contain).val();
	var country = $("#Address" + key + "Country", contain).val();
	window.open("https://maps.google.com/maps?q=" + address_1 + " " + address_2 + " " + address_3 + " " + town_city + " " + province_state + " " + zip_postcode + " " + country,"_blank");
}


function contacts_adddress_update_entry(object){

	// thay đổi địa chỉ của entry trên luôn
	var form = $(object).closest('form');
	// console.log(data_form);
	$("#ContactAddressesDefaultKey").val($("#AddressKey", form).val());

	$("#DefaultAddress1").val($("#AddressAddress1", form).val());
	$("#DefaultAddress2").val($("#AddressAddress2", form).val());
	$("#DefaultAddress3").val($("#AddressAddress3", form).val());
	$("#DefaultTownCity").val($("#AddressTownCity", form).val());
	$("#DefaultZipPostcode").val($("#AddressZipPostcode", form).val());
	$("#DefaultCountry").val($("#Address" +$("#AddressKey", form).val()+ "Country", form).val());
	$("#DefaultCountryId").val($("#Address" +$("#AddressKey", form).val()+ "CountryId", form).val());
	$("#DefaultProvinceState").val($("#Address" +$("#AddressKey", form).val()+ "ProvinceState", form).val());
	$("#DefaultProvinceStateId").val($("#Address" +$("#AddressKey", form).val()+ "ProvinceStateId", form).val());

	contacts_auto_save_entry();
	// setTimeout("contacts_auto_save_entry();", 1800);
}

function change_province_begin_do(object){
	// thay đổi địa chỉ của entry trên luôn
	var form = $(object).closest('form');
	// console.log(data_form);
	$("#ContactAddressesDefaultKey").val($("#AddressKey", form).val());

	// thay doi tinh thanh
	if( $(object).hasClass("country") ){
		change_province($("#AddressKey", form).val(), $("#Address" +$("#AddressKey", form).val()+ "CountryId", form).val() );
	}
}

function change_province(key, country_id){
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/ajax_general_province',
		dataType: "json",
		type:"POST",
		data: {country_id:country_id},
		success: function(jsondata){
			// alert(jsondata);
			var li = $("#Address"+key+"ProvinceState").parent("span").parent("li");
			$("#Address"+key+"ProvinceState").parent("span").remove();

			if( jsondata != "[]" ){
				var str = '<input name="data[Address]['+key+'][province_state]" class="input_select bg1" type="text" id="Address'+key+'ProvinceState">';
				li.prepend(str);

				$("#Address"+key+"ProvinceState").combobox(jsondata);
			}

		}
	});
}

<?php if( $this->Common->check_permission('contacts_@_addresses_tab_@_delete', $arr_permission) ){ ?>
function contacts_adddress_delete(key){
	confirms( "Message", "Are you sure you want to delete?",
	    function(){
	        $.ajax({
				url: '<?php echo URL; ?>/contacts/addresses_delete/'+ key + '/<?php echo $contact_id; ?>',
				success: function(html){
					if(html == "ok"){
						$("#AddressEntryForm_" + key).fadeOut();
					}
					console.log(html);
				}
			});
	    },function(){
	        //else do somthing
	});
}
<?php } ?>

// khi click vào input "Use company address as default" sẽ run function này
function contacts_addresses_use_company(){
	if( $("#ContactCompanyId").val() == "" ){
		alerts("Error", "Please specify company first");

	}else{

		$.ajax({
			url: '<?php echo URL; ?>/contacts/addresses_get_company_info/' + $("#ContactCompanyId").val(),
			timeout: 15000,
			success: function(html){
				var obj = jQuery.parseJSON(html);

				$("#DefaultAddress1").val(obj.address_1);
				$("#DefaultAddress2").val(obj.address_2);
				$("#DefaultAddress3").val(obj.address_3);
				$("#DefaultTownCity").val(obj.town_city);
				$("#DefaultZipPostcode").val(obj.zip_postcode);
				$("#DefaultProvinceState").val(obj.province_state);
				$("#DefaultProvinceStateId").val(obj.province_state_id);
				$("#DefaultCountry").val(obj.country);
				$("#DefaultCountryId").val(obj.country_id);
				contacts_auto_save_entry();

				console.log(html); // view log when debug
			}
		});
	}

}
</script>