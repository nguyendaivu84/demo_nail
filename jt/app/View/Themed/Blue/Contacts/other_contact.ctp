<?php
	if(isset($other_contact['emergency_other_contacts']))
	{

		$i = '0';
		foreach($other_contact['emergency_other_contacts'] as $key=>$value)
		{
			echo  '<form action="javascript:void()" id="'.$key.'" method="POST" accept-charset="utf-8" >';
			$bg = ($i%2==0 ? 'bg2' : 'bg1');
			echo  '<ul class="ul_mag clear '.$bg.'">
					<li class="hg_padd" style="width:1.5%"></li>
					<li class="hg_padd" style="width:30%"><input type="text" name="name" class="input_inner input_inner_w '.$bg.'" id="Personal_Name" value="'.$value['name'].'" /></li>
					<li class="hg_padd" style="width:9%"><input type="text" name="phone" class="input_inner input_inner_w '.$bg.'" id="Personal_Phone" value="'.$value['phone'].'" /></li>
					<li class="hg_padd" style="width:50%;"><input type="text" name="relationship_note" class="input_inner input_inner_w '.$bg.'" id="Personal_Relationship_Note" value="'.$value['relationship_note'].'" /></li>
					<li class="hg_padd bor_mt" style="width:1.5%">
						<div class="middle_check">
							<a title="Delete link" onclick="delete_other_contact('.$key.')" href="javascript:void()">
								<span class="icon_remove2"></span>
							</a>
						</div>
					</li>
				</ul>';
			$i++;
			echo  '</form>';
		}
		if($i < 9)
		{
			$j = 8- $i;
			for($k =0; $k<$j; $k++)
			{
				$bg = ($i%2==0 ? 'bg2' : 'bg1');
				echo '<ul class="ul_mag clear '.$bg.'"></ul>';
				$i++;
			}
		}
	}
	else
		for($j = 0; $j <9; $j ++)
		{
			$bg = ($j%2==0 ? 'bg2' : 'bg1');
			echo '<ul class="ul_mag clear '.$bg.'"></ul>';
		}
?>

<script type="text/javascript">
	//Auto Save khi input change
	$(function(){
		$("form :input").change(function(){
			var id = $('#ContactId').val();
			var key = $(this).closest('form').attr('id');
			var info = $(this).closest('form').serialize();
			$.ajax({
				url: '<?php echo URL; ?>/contacts/other_contact/'+id,
				type: 'post',
				data: {type: 'update', key:key, info : info},
				success: function(html){
					if( html != "ok" ){
						alerts("Error:", html);
					}
				}
			});
		});
	});
	//Delete
	function delete_other_contact(no)
	{
		confirms( "Message", "Are you sure you want to delete?",
			function(){
				var id = $('#ContactId').val();
				$.ajax({
					url: '<?php echo URL; ?>/contacts/other_contact/'+id,
					type: 'post',
					data: {type: 'delete', no : no},
					success: function(html){
						$('#contacts_other_contact').html(html);
					}
				});
			},function(){
				return false;
			}
		)
	}

</script>