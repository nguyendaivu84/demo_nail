<style type="text/css">
ul.ul_mag li.hg_padd {
	overflow: visible !important;
}
</style>
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Expenses for this employee'); ?></h4></span>

			<?php if( $this->Common->check_permission('contacts_@_expenses_tab_@_edit', $arr_permission) ){ ?>
			<?php echo $this->Js->link( '<span class="icon_down_tl top_f"></span>', '/contacts/expenses_add/'.$contact_id,
				array(
					'update' => '#contacts_sub_content',
					'title' => 'Add expense',
					'escape' => false,
					'success' => '$(".container_same_category", "#contacts_sub_content").mCustomScrollbar({scrollButtons:{enable:false}});'
				) );
			?>
			<?php } ?>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1.5%"></li>
			<li class="hg_padd" style="width:7%">Type</li>
			<li class="hg_padd center_txt" style="width:5%">Date</li>
			<li class="hg_padd" style="width:10%">Code</li>
			<li class="hg_padd" style="width:25%">Name / details</li>
			<li class="hg_padd" style="width:7%">Reference</li>
			<li class="hg_padd right_txt" style="width:6%">Cost price</li>
			<li class="hg_padd right_txt" style="width:5%">Quantity</li>
			<li class="hg_padd right_txt" style="width:6%">Total</li>
			<li class="hg_padd right_txt" style="width:6%">Debit</li>
			<li class="hg_padd right_txt" style="width:6%">Credit</li>
			<li class="hg_padd bor_mt" style="width:1.5%"></li>
		</ul>
		<div id="contacts_expenses" class="container_same_category">
			<?php $count = 0; $i = 1;  $k = 0;
			if(isset($arr_contact['expenses'])){

				foreach($arr_contact['expenses'] as $key => $value){
					$k=$k+1;
					if( $value['deleted'] )continue;
				?>

				<?php echo $this->Form->create('Expense', array('id' => 'ExpenseEntryForm_'.$key)); ?>
				<?php echo $this->Form->hidden('Expense.key', array( 'value' => $key )); ?>
				<?php echo $this->Form->hidden('Expense._id', array( 'value' => $contact_id )); ?>

				<ul class="ul_mag clear bg<?php echo $i; ?>" id="contacts_expenses_<?php echo $key; ?>">
					<li class="hg_padd" style="width:1.5%"></li>
					<li class="hg_padd" style="width:7%">
						<?php echo $this->Form->input('Expense.type', array(
							'class' => 'input_select bg'.$i,
							'value' => $value['type'],
						)); ?>
						<?php echo $this->Form->hidden('Expense.type_id'); ?>
						<script type="text/javascript">
							$(function () {
								$("#ExpenseType", "#contacts_expenses_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_expenses_type); ?>);
							});
						</script>
					</li>
					<li class="hg_padd center_txt" style="width:5%">
						<?php echo $this->Form->input('Expense.date', array(
								'class' => 'JtSelectDate input_inner input_inner_w bg'.$i,
								'style' => 'width: 70px',
								'id' => 'RatesWagework_start'.rand(0,99999999),
								'readonly' => true,
								'rel' => $key,
								'value' => (is_object($value['date'])?date('m/d/Y', $value['date']->sec):'')
						)); ?>
					</li>
					<li class="hg_padd" style="width:10%">
						<?php echo $this->Form->input('Expense.code', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['code']
						)); ?>
					</li>
					<li class="hg_padd" style="width:25%">
						<?php echo $this->Form->input('Expense.name', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['name']
						)); ?>
					</li>
					<li class="hg_padd" style="width:7%">
						<?php echo $this->Form->input('Expense.reference', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['reference']
						)); ?>
					</li>
					<li class="hg_padd right_txt" style="width:6%">
						<?php echo $this->Form->input('Expense.cost_price', array(
								'class' => 'right_txt input_inner input_inner_w bg'.$i,
								'value' => (is_numeric($value['cost_price']))?number_format($value['cost_price'], 2):''
						)); ?>
					</li>
					<li class="hg_padd right_txt" style="width:5%">
						<?php echo $this->Form->input('Expense.quantity', array(
								'class' => 'right_txt input_inner input_inner_w bg'.$i,
								'value' => $value['quantity']
						)); ?>
					</li>
					<li class="hg_padd right_txt" style="width:6%"><span id="total_<?php echo $key; ?>"></span></li>
					<li class="hg_padd right_txt" style="width:6%">
						<?php echo $this->Form->input('Expense.debit', array(
								'class' => 'right_txt input_inner input_inner_w bg'.$i,
								'value' => (is_numeric($value['debit']))?number_format($value['debit'], 2):''
						)); ?>
					</li>
					<li class="hg_padd right_txt" style="width:6%">
						<?php echo $this->Form->input('Expense.credit', array(
								'class' => 'right_txt input_inner input_inner_w bg'.$i,
								'value' => (is_numeric($value['credit']))?number_format($value['credit'], 2):''
						)); ?>
					</li>
					<?php if( $this->Common->check_permission('contacts_@_expenses_tab_@_delete', $arr_permission) ){ ?>
					<li class="hg_padd bor_mt" style="width:1.5%">
						<div class="middle_check">
							<a title="Delete link" href="javascript:void(0)" onclick="contacts_expense_delete(<?php echo $key; ?>)">
								<span class="icon_remove2"></span>
							</a>
						</div>
					</li>
					<?php } ?>
				</ul>

				<?php echo $this->Form->end(); ?>

				<?php $i = 3 - $i; $count += 1;
					}
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>">
						</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>
		<span class="hit"></span>
		<span class="title_block bo_ra2">
			<span class="float_left bt_block"><a href=""><?php echo translate('Click to view full details'); ?></a></span>
			<span class="float_left left_text dent_bl_txt"><?php echo translate('Note: You can assign a default address for each contact for this contact or enter specific expenses for them as well'); ?></span>
		</span>
	</div><!--END Tab1 -->

<script type="text/javascript">

$(function(){

	<?php if( $this->request->is('ajax') ){ ?>
	input_show_select_calendar(".JtSelectDate", "#contacts_sub_content");
	<?php } ?>

	<?php if( $this->Common->check_permission('contacts_@_expenses_tab_@_edit', $arr_permission) ){ ?>
	$("form :input", "#contacts_sub_content").change(function() {
		var contain = $(this).closest('form');
		$.ajax({
			url: '<?php echo URL; ?>/contacts/expenses_auto_save',
			timeout: 15000,
			type:"post",
			data: $(this).closest('form').serialize(),
			success: function(html){
				if( html != "ok" )alerts("Error: ", html);
			}
		});
	});

	<?php }else{ ?>
	$("input", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).attr("disabled", true).css("background-color", "transparent");
	});
	$(".combobox_button, .indent_dw_m, .icon_down_new", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).remove();
	});
	<?php } ?>
});

<?php if( $this->Common->check_permission('contacts_@_expenses_tab_@_delete', $arr_permission) ){ ?>
function contacts_expense_delete(key){
	confirms( "Message", "Are you sure you want to delete?",
		function(){
			$.ajax({
				url: '<?php echo URL; ?>/contacts/expenses_delete/'+ key + '/<?php echo $contact_id; ?>',
				success: function(html){
					if(html == "ok"){
						$("#contacts_expenses_" + key).fadeOut();
					}
					console.log(html);
				}
			});
		},function(){
			//else do somthing
	});
}
<?php } ?>
</script>