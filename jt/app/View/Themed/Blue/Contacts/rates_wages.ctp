<style type="text/css">
/*.clear_percent_10, .clear_percent_12 {
width: 16.5%;
margin-right: .5%;
}
.clear_percent_11 {
width: 81%;
}
.width_in5 {
padding: 0 1%;
width: 47%;
}
.title_block_inner {
width: 46.3%;
}*/
</style>
<div class="clear_percent_10 float_left">
	<div id="rates_wages_left" class="tab_1 full_width">
		<div class="title_block bo_ra1">
			<span class="title_block_inner" style="width:100%"><h4><?php echo translate('Employee details'); ?></h4></span>
		</div>
		<div class="tab_2_inner">
			<?php echo $this->Form->hidden('Contact._id', array( 'value' => $contact_id)); ?>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Employee type'); ?></span>
				</p>
				<div class="width_in5 float_left indent_input_tp"><!-- width_in float_left indent_input_tp -->
					<?php echo $this->Form->input('Contact.employee_type', array(
							'class' => 'input_select hasBoder',
							'readonly' => true
					)); ?>
					<?php echo $this->Form->hidden('Contact.employee_type_id'); ?>
					<script type="text/javascript">
						$(function () {
							$("#ContactEmployeeType").combobox(<?php echo json_encode($arr_employee_type); ?>);
						});
					</script>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Employment type'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php echo $this->Form->input('Contact.employment_type', array(
							'class' => 'input_select hasBoder',
							'readonly' => true
					)); ?>
					<?php echo $this->Form->hidden('Contact.employment_type_id'); ?>
					<script type="text/javascript">
						$(function () {
							$("#ContactEmploymentType").combobox(<?php echo json_encode($arr_employment_type); ?>);
						});
					</script>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Work type'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php echo $this->Form->input('Contact.work_type', array(
							'class' => 'input_select hasBoder',
							'readonly' => true
					)); ?>
					<?php echo $this->Form->hidden('Contact.work_type_id'); ?>
					<script type="text/javascript">
						$(function () {
							$("#ContactWorkType").combobox(<?php echo json_encode($arr_work_type); ?>);
						});
					</script>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Paid by'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php echo $this->Form->input('Contact.paid_by', array(
							'class' => 'input_select hasBoder',
							'readonly' => true
					)); ?>
					<?php echo $this->Form->hidden('Contact.paid_by_id'); ?>
					<script type="text/javascript">
						$(function () {
							$("#ContactPaidBy").combobox(<?php echo json_encode($arr_paid_by); ?>);
						});
					</script>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Overtime starts at'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php echo $this->Form->input('Contact.overtime_starts_at', array(
							'class' => 'input_1 float_left',
					)); ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Overtime ends at'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php echo $this->Form->input('Contact.overtime_ends_at', array(
							'class' => 'input_1 float_left',
					)); ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner fixbor8"></span>
				</p><div class="width_in5 float_left indent_input_tp"></div>
			<p></p>
			<p class="clear"></p>
		</div>
		<span class="title_block bo_ra2"></span>
	</div><!--END Tab1 -->

	<script type="text/javascript">
	$(function() {
		$(":input", "#rates_wages_left").change(function() {
			$.ajax({
				url: '<?php echo URL; ?>/contacts/rates_wages_auto_save',
				timeout: 15000,
				type: "post",
				data: $(":input", "#rates_wages_left").serialize(),
				success: function(html) {
					if (html != "ok") {
						alerts("Error: ", html);
					}
					console.log(html); // view log when debug
				}
			});
		});
	});
	</script>
</div>
<div class="clear_percent_11 float_left">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Employment reviews and rates'); ?></h4></span>
			<?php echo $this->Js->link( '<span class="icon_down_tl top_f"></span>', '/contacts/rates_wages_add/'.$contact_id,
				array(
					'update' => '#contacts_sub_content',
					'title' => 'Add new',
					'escape' => false
				) );
			?>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:6%"><?php echo translate('Reviewed'); ?></li>
			<li class="hg_padd center_txt" style="width:7%"><?php echo translate('Start'); ?></li>
			<li class="hg_padd center_txt" style="width:7%"><?php echo translate('Finish'); ?></li>
			<li class="hg_padd center_txt" style="width:4%"><?php echo translate('Weeks'); ?></li>
			<li class="hg_padd right_txt" style="width:3%"><?php echo translate('Wage'); ?></li>
			<li class="hg_padd center_txt" style="width:4%"><?php echo translate('Per'); ?></li>

			<div class="float_left" style="width:19%;">
				<div class="tab_title_purchasing">
					<span class="block_purcharsing"><?php echo translate('Rate per hour'); ?></span>
				</div>
				<div>
					<li class="hg_padd line_mg right_txt" style="width:32%"><?php echo translate('Cost rate'); ?></li>
					<li class="hg_padd right_txt line_mg right_txt" style="width:32%"><?php echo translate('Overtime'); ?></li>
					<li class="hg_padd right_txt line_mg right_txt" style="width:31.2%"><?php echo translate('Bill rate'); ?></li>

				</div>
			</div>
			<div class="float_left" style="width:12%;">
				<div class="tab_title_purchasing">
					<span class="block_purcharsing"><?php echo translate('Rate per day'); ?></span>
				</div>
				<div>
					<li class="hg_padd right_txt line_mg" style="width:50%"><?php echo translate('Cost rate'); ?></li>
					<li class="hg_padd right_txt line_mg" style="width:46.2%"><?php echo translate('Bill rate'); ?></li>
				</div>
			</div>
			<li class="hg_padd right_txt right_txt" style="width:7%;"><?php echo translate('Commission'); ?></li>
			<li class="hg_padd" style="width:19%;"><?php echo translate('Notes'); ?></li>
			<li class="hg_padd bor_mt" style="width:1.5%"></li>
		</ul>

		<style type="text/css">
		ul.ul_mag li.hg_padd {
			overflow: visible !important;
		}
		ul.ul_mag li.hg_padd {
			margin-top: 0;
		}
		#contacts_sub_content .combobox {
			margin-top: 1px;
		}
		#contacts_sub_content .combobox_arrow {
			background-position: -1px 3px;
		}
		</style>

		<div id="contacts_rates_wages_right">
			<?php
			$i = 2; $count = 0;
			if( isset($this->data['Contact']['rates_wages']) ){
				foreach ($this->data['Contact']['rates_wages'] as $key => $value) {

					if( isset($value['deleted']) && $value['deleted'])continue;
				?>
				<ul class="ul_mag clear bg<?php echo $i; ?>" id="Contacts_RatesWages_<?php echo $key; ?>">
					<?php echo $this->Form->hidden('RatesWage._id', array( 'value' => $contact_id )); ?>
					<li class="hg_padd" style="width:6%">
						<?php echo $this->Form->input('RatesWage.reviewed', array(
								'class' => 'JtSelectDate input_inner input_inner_w bg'.$i,
								'style' => 'width: 70px',
								'id' => 'RatesWagework_reviewed'.rand(0,99999999),
								'rel' => $key,
								'readonly' => true,
								'value' => (is_object($value['reviewed'])?date('m/d/Y', $value['reviewed']->sec):'')
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width:7%">
						<?php echo $this->Form->input('RatesWage.start', array(
								'class' => 'JtSelectDate input_inner input_inner_w bg'.$i,
								'style' => 'width: 70px',
								'id' => 'RatesWagework_start'.rand(0,99999999),
								'readonly' => true,
								'rel' => $key,
								'value' => (is_object($value['start'])?date('m/d/Y', $value['start']->sec):'')
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width:7%">
						<?php echo $this->Form->input('RatesWage.finish', array(
								'class' => 'JtSelectDate input_inner input_inner_w bg'.$i,
								'style' => 'width: 70px',
								'id' => 'RatesWagework_finish'.rand(0,99999999),
								'readonly' => true,
								'rel' => $key,
								'value' => (is_object($value['finish'])?date('m/d/Y', $value['finish']->sec):'')
						)); ?>
					</li>
					<li class="hg_padd right_txt" style="width:4%" id="week_<?php echo $key; ?>">
						<?php echo $value['weeks']; ?>
					</li>
					<li class="hg_padd right_txt" style="width:3%">
						<?php echo $this->Form->input('RatesWage.wage', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['wage'],
								'readonly' => true,
								'style' => 'text-align:center;'
						)); ?>
					</li>
					<li class="hg_padd center_txt" style="width:4%">
						<?php echo $this->Form->input('RatesWage.per', array(
								'class' => 'input_select bg'.$i,
								'rel' => $key,
								'value' => (isset($value['per'])?$value['per']:''),
						)); ?>
						<?php echo $this->Form->hidden('RatesWage.per_id', array('value' => $value['per_id'])); ?>
						<script type="text/javascript">
							$(function () {
								$("#RatesWagePer", "#Contacts_RatesWages_<?php echo $key; ?>").combobox(<?php echo json_encode($arr_rates_wages_per); ?>);
							});
						</script>
					</li>
					<div class="float_left" style="width:19%;">
						<li class="hg_padd line_mg right_txt" style="width: 32.6%;">
							<?php echo $this->Form->input('RatesWage.hour_cost_rate', array(
									'class' => 'input_inner input_inner_w bg'.$i,
									'value' => number_format((float)$value['hour_cost_rate'], 2),
									'rel' => $key,
									'style' => 'text-align:right;'
							)); ?>
						</li>
						<li class="hg_padd right_txt line_mg right_txt" style="width:31.6%;">
							<?php echo $this->Form->input('RatesWage.hour_overtime', array(
									'class' => 'input_inner input_inner_w bg'.$i,
									'value' => number_format((float)$value['hour_overtime'], 2),
									'rel' => $key,
									'style' => 'text-align:right;'
							)); ?>
						</li>
						<li class="hg_padd right_txt line_mg right_txt" style="width:30.8%;">
							<?php echo $this->Form->input('RatesWage.hour_bill_rate', array(
									'class' => 'input_inner input_inner_w bg'.$i,
									'value' => number_format((float)$value['hour_bill_rate'], 2),
									'rel' => $key,
									'style' => 'text-align:right;'
							)); ?>
						</li>
					</div>

					<div class="float_left" style="width:12%;">
						<li class="hg_padd line_mg right_txt" style="width:50.6%;">
							<?php echo $this->Form->input('RatesWage.day_cost_rate', array(
									'class' => 'input_inner input_inner_w bg'.$i,
									'value' => number_format((float)$value['day_cost_rate'], 2),
									'rel' => $key,
									'style' => 'text-align:right;'
							)); ?>
						</li>
						<li class="hg_padd right_txt line_mg right_txt" style="width:45.6%;">
							<?php echo $this->Form->input('RatesWage.day_bill_rate', array(
									'class' => 'input_inner input_inner_w bg'.$i,
									'value' => number_format((float)$value['day_bill_rate'], 2),
									'rel' => $key,
									'style' => 'text-align:right;'
							)); ?>
						</li>
					</div>
					<li class="hg_padd right_txt line_mg right_txt" style="width:7%;">
						<?php echo $this->Form->input('RatesWage.commission', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['commission'].( ($value['commission'] > 0)?'%':'' ),
								'rel' => $key,
						)); ?>
					</li>
					<li class="hg_padd" style="width:19%;">
						<?php echo $this->Form->input('RatesWage.notes', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['notes'],
								'rel' => $key,
						)); ?>
					</li>
					<li class="hg_padd bor_mt center_txt" style="width:1.5%">
						<div class="middle_check">
							<a title="Delete link" href="javascript:void(0)" onclick="contacts_rates_wages_delete('<?php echo $key; ?>')">
								<span class="icon_remove2"></span>
							</a>
						</div>
					</li>
				</ul>
				<?php $i = 3 - $i; $count += 1;
				}
			}

			$count = 8 - $count;
			if( $count > 0 ){
				for ($j=0; $j < $count; $j++) { ?>
				<ul class="ul_mag clear bg<?php echo $i; ?>">
				</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>
		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
				<a href="">Sorted by most recent start date</a>
			</span>
		</span>
	</div><!--END Tab1 -->
</div>
<script type="text/javascript">
$(function(){

	<?php if( $this->request->is('ajax') ){ ?>
	input_show_select_calendar(".JtSelectDate", "#contacts_rates_wages_right");
	<?php } ?>

	$(":input", "#contacts_rates_wages_right").change(function() {

		object = $(this);

		$.ajax({
			url: '<?php echo URL; ?>/contacts/rates_wages_auto_save/<?php echo $contact_id; ?>',
			timeout: 15000,
			type:"post",
			data: { rates_wages_right: 1, key: object.attr("rel"), name: object.attr("name"), value: object.val()  },
			success: function(html){
				if( html.substring(0, 6) == "weeks_" )
					$("#week_" + object.attr("rel")).html(html.substring(6));
				else if( html != "ok" )
					alerts("Error", html);
			}
		});
	});
});

function contacts_rates_wages_delete(key){
	confirms( "Message", "Are you sure you want to delete?",
	    function(){
	        $.ajax({
				url: '<?php echo URL; ?>/contacts/rates_wages_delete/'+ key + '/<?php echo $contact_id; ?>',
				success: function(html){
					$("#Contacts_RatesWages_" + key).remove();
				}
			});
	    },function(){
	        //else do somthing
	});

}
</script>