<style type="text/css">
#contacts_form_auto_save .clear_percent_10, .clear_percent_12 {
width: 19.5%;
}
#contacts_form_auto_save .clear_percent_11 {
width: 80%;
}
#contacts_form_auto_save .width_in5 {
width: 52%;
}
#contacts_form_auto_save .title_block_inner {
width: 42.3%;
}
</style>
<?php echo $this->element('entry_tab_option'); ?>
<div id="content" class="fix_magr">

	<div class="jt_ajax_note" style="display: none;"></div>

	<div class="clear">

		<div class="clear_percent">
			<div class="block_dent_a">
				<div class="title_1 float_left"><h1><span id="contact_name_header"></span> - <span id="contact_account_header"></span></h1></div>
				<div class="title_1 right_txt float_right"><h1><span id="contact_company_header"></span></h1></div>
			</div>
		</div>

		<div id="contacts_form_auto_save">
			<?php echo $this->Form->create('Contact'); ?>
			<?php echo $this->Form->hidden('Contact._id', array('value' => (string)$this->data['Contact']['_id'])); ?>
			<?php echo $this->Form->hidden('Contact.addresses_default_key'); ?>
			<div class="clear_percent">
				<div class="clear_percent_1 float_left">
					<div class="tab_1 block_dent_a">
						<p class="clear">
							<span class="label_1 float_left fixbor"><?php echo translate('Contact no'); ?></span>
							<div class="indent_new width_in float_left">

								<?php echo $this->Form->input('Contact.no', array(
										'class' => 'input_1 float_left width_ina22',
										'style' => 'margin-top: 1.6%;width: 53% !important;'
								)); ?>

								<?php if( $this->data['Contact']['is_employee'] ){ ?>
								<div class="in_active width_active_in2" style="padding-top:5px">
									<span class="inactive_fix_entry">Employee</span>
								</div>
								<?php }else{ ?>
								<div class="in_active width_active_in2">
									<input type="hidden" name="data[Contact][is_customer]" id="ContactIsCustomer_" value="0">
									<span class="inactive_fix_entry">Customer</span>
									<label class="m_check2">
										<?php echo $this->Form->input('Contact.is_customer', array(
												'type' => 'checkbox',
												'class' => 'customer-employee'
										)); ?>
										<span></span>
									</label>
								</div>
								<?php } ?>

							</div>
						</p>
						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Title'); ?></span>
							<div class="width_in float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.title', array(
										'class' => 'input_select',
										'readonly' => true
								)); ?>
								<?php echo $this->Form->hidden('Contact.title_id'); ?>
								<script type="text/javascript">
							        $(function () {
							            $("#ContactTitle").combobox(<?php echo json_encode($arr_contacts_title); ?>);
							        });
							    </script>
							</div>
							<p class="clear"></p>
						</p>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('First name'); ?></span>
						</p>
						<div class="width_in float_left indent_input_tp">
							<?php echo $this->Form->input('Contact.first_name', array(
									'class' => 'input_1 float_left',
									// 'readonly' => true,
									// 'onclick' => 'open_window_select_company()'
							)); ?>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Last name'); ?></span>
						</p>
						<div class="width_in float_left indent_input_tp">
							<?php echo $this->Form->input('Contact.last_name', array(
									'class' => 'input_1 float_left'
							)); ?>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Type'); ?></span>
							<div class="width_in float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.type', array(
										'class' => 'input_select',
								)); ?>
								<?php echo $this->Form->hidden('Contact.type_id'); ?>
								<script type="text/javascript">
							        $(function () {
							            $("#ContactType").combobox(<?php echo json_encode($arr_contacts_type); ?>);
							        });
							    </script>

							</div>
							<p class="clear"></p>
						</p>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Direct dial'); ?></span>
						</p>
						<div class="indent_new width_in float_left">
							<?php echo $this->Form->input('Contact.direct_dial', array(
									'class' => 'input_1 float_left'
							)); ?>
							<span class="icon_phonec" title="Not yet implemented"></span>
						</div>

						<p class="clear">
							<span class="label_1 float_left"><?php echo translate('Mobile'); ?></span>
						</p>
						<div class="width_in float_left indent_input_tp">
							<?php echo $this->Form->input('Contact.mobile', array(
									'class' => 'input_1 float_left'
							)); ?>
							<span class="icon_phonec" title="Not yet implemented"></span>
						</div>

						<p class="clear">
							<span class="label_1 float_left fixbor2"><?php echo translate('Email'); ?></span>
						</p>
						<div class="width_in float_left indent_input_tp">
							<?php echo $this->Form->input('Contact.email', array(
									'class' => 'input_1 float_left'
							)); ?>
							<span class="icon_emaili" title="Not yet implemented"></span>
						</div>
						<p class="clear"></p>
					</div><!--END Tab1 -->
				</div>
				<div class="clear_percent_2 float_right">
					<div class="tab_1 float_left block_dent8">
						<div class="tab_1_inner float_left">
							<p class="clear">
								<span class="label_1 float_left minw_lab fixbor"><?php echo translate('Fax'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.fax', array(
										'class' => 'input_1 float_left '
								)); ?>
								<span class="icon_down_pl" title="Not yet implemented"></span>
							</div>
							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Home phone'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.home_phone', array(
										'class' => 'input_1 float_left '
								)); ?>
								<span class="icon_phonec" title="Not yet implemented"></span>
							</div>
							<p class="clear">
								<span class="label_1 float_left minw_lab">
									<?php
										$link = 'javascript:void(0)';
										if(isset($this->data['Contact']['company_id']) && is_object($this->data['Contact']['company_id'])){
											$link = URL . '/companies/entry/' . $this->data['Contact']['company_id'];
										}
									?>

									<span style="display:inline;" onclick="jt_link_module(this, '<?php echo msg('QUOTATION_CREATE_LINK'); ?> Companies', '<?php echo URL; ?>/companies/add')" href="<?php echo $link; ?>" class="jt_box_line_span" id="link_to_companies" ><?php echo translate('Company'); ?></span>
								</span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->hidden('Contact.company_id'); ?>

								<?php
								$company_name = '';
					            if (is_object($this->data['Contact']['company_id'])) {
					            	 $company_name = $this->data['Contact']['company'];
					                if(!isset($arr_company_tmp))$arr_company_tmp = array();
					                if( !isset($arr_company_tmp[(string)$this->data['Contact']['company_id']]) ){
					                    $arr_company = $model_company->select_one(array('_id' => new MongoId($this->data['Contact']['company_id'])), array('_id', 'name'));
					                    if(isset($arr_company['name'])){
					                        $company_name = $arr_company['name'];
					                    }
					                }else{
					                    $company_name = $arr_company_tmp[(string)$this->data['Contact']['company_id']];
					                }
					            }
					            ?>
								<?php echo $this->Form->input('Contact.company', array(
										'class' => 'input_1 float_left ',
										'style' => 'width:92%',
										'value' => $company_name,
										'readonly' => true
								)); ?>

								<?php
									$style = "";
									if( isset($this->data['Contact']['is_employee']) && $this->data['Contact']['is_employee'] ){
										$style = "display:none";
								} ?>
								<span class="iconw_m indent_dw_m" id="click_open_window_companies" style="<?php echo $style; ?>"></span>
								<script type="text/javascript">
									$(function(){
										window_popup('companies', 'Specify company');
									});

									function after_choose_companies(company_id, company_name){

										$("#link_to_companies").attr("href", "<?php echo URL; ?>/companies/entry/" + company_id);

										var json = $("#after_choose_companies" + company_id).val();

										$("#ContactCompanyId").val(company_id);
										$("#ContactCompany").val(JSON.parse(json).name);
										$("#ContactCompanyPhone").val(JSON.parse(json).phone);
								        $("#ContactFax").val(JSON.parse(json).fax);

										var addresses_default_key = JSON.parse(json).addresses_default_key;
										$("#DefaultAddress1").val(JSON.parse(json).addresses[addresses_default_key].address_1);
										$("#DefaultAddress2").val(JSON.parse(json).addresses[addresses_default_key].address_2);
										$("#DefaultAddress3").val(JSON.parse(json).addresses[addresses_default_key].address_3);
										$("#DefaultTownCity").val(JSON.parse(json).addresses[addresses_default_key].town_city);
									    $("#DefaultProvinceState").val(JSON.parse(json).addresses[addresses_default_key].province_state);
										$("#DefaultZipPostcode").val(JSON.parse(json).addresses[addresses_default_key].zip_postcode);
									    $("#DefaultCountry").val(JSON.parse(json).addresses[addresses_default_key].country);
										$("#DefaultProvinceStateId").val(JSON.parse(json).addresses[addresses_default_key].province_state_id);
										$("#DefaultCountryId").val(JSON.parse(json).addresses[addresses_default_key].country_id);

										$("#window_popup_companies").data("kendoWindow").close();

										contacts_auto_save_entry();

										var parameter_get = "?is_customer=1";
	                                    if( $("#SalesorderCompanyId").val() != "" ){
	                                        parameter_get += "&company_id=" + $("#SalesorderCompanyId").val() + "&company_name=" + $("#SalesorderCompanyName").val();
	                                    }
										window_popup("addresses", "Specify contact","default","click_open_window_addressesdefault", parameter_get, "force_re_install");
										return false;
									}
								</script>
							</div>
							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Company phone'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.company_phone', array(
										'class' => 'input_1 float_left ',
										'readonly' => true
								)); ?>
								<span class="icon_phonec" title="Not yet implemented"></span>
							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Position'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.position', array(
										'class' => 'input_select',
								)); ?>
								<?php echo $this->Form->hidden('Contact.position_id'); ?>
								<script type="text/javascript">
							        $(function () {
							            $("#ContactPosition").combobox(<?php echo json_encode($arr_contacts_position); ?>);
							        });
							    </script>

							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Department'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.department', array(
										'class' => 'input_select',
								)); ?>
								<?php echo $this->Form->hidden('Contact.department_id'); ?>
								<script type="text/javascript">
							        $(function () {
							            $("#ContactDepartment").combobox(<?php echo json_encode($arr_contacts_department); ?>);
							        });
							    </script>

							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Extension no'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.extension_no', array(
										'class' => 'input_1 float_left '
								)); ?>
								<span class="icon_phonec" title="Not yet implemented"></span>
							</div>

							<p class="clear">
								<span class="label_1 float_left fixbor2 minw_lab " style="height:25px"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
							</div>

						</div>

						<?php

						$key = $this->data['Contact']['addresses_default_key'];
						echo $this->element('box_type/address', array(
								'address_label' => array('Default address'),
								'address_more_line' => 0,
								'address_conner' => array(
									array('top' => 'hgt', 'bottom' => 'fixbor3 hgt2'),
								),
								'address_onchange' => 'contacts_auto_save_entry()',
								'address_controller' => array('Contact'),
								'address_country_id' => array($this->data['Contact']['addresses'][$key]['country_id']),
								'address_province_id' => array($this->data['Contact']['addresses'][$key]['province_state_id']),
								'address_company_id' => "ContactCompanyId",
								'address_value' => array(
									'default' => array(
										$this->data['Contact']['addresses'][$key]['address_1'],
										$this->data['Contact']['addresses'][$key]['address_2'],
										$this->data['Contact']['addresses'][$key]['address_3'],
										$this->data['Contact']['addresses'][$key]['town_city'],
										$this->data['Contact']['addresses'][$key]['country_id'],
										$this->data['Contact']['addresses'][$key]['province_state'],
										$this->data['Contact']['addresses'][$key]['zip_postcode']
									)
								),
								'address_key' => array('default'),
								'address_conner' => array(
									array(
										'top' => 'hgt',
										'bottom' => 'fixbor3 fix_bottom_address  fix_bottom_address_28'
									)
								),
								'address_more_line' => 1
						)); ?>

						<script type="text/javascript">
						function after_choose_addresses(position_key, type, key){

							$("#DefaultAddress1").val($("#window_popup_addresses_" +type+ "_address_1_"+position_key+ key).val());
							$("#DefaultAddress2").val($("#window_popup_addresses_" +type+ "_address_2_"+position_key+ key).val());
							$("#DefaultAddress3").val($("#window_popup_addresses_" +type+ "_address_3_"+position_key+ key).val());
							$("#DefaultTownCity").val($("#window_popup_addresses_" +type+ "_town_city_"+position_key+ key).val());
							$("#DefaultProvinceState").val($("#window_popup_addresses_" +type+ "_province_state_"+position_key+ key).val());
							$("#DefaultZipPostcode").val($("#window_popup_addresses_" +type+ "_zip_postcode_"+position_key+ key).val());
							$("#DefaultCountry").val($("#window_popup_addresses_" +type+ "_country_"+position_key+ key).val());
							$("#DefaultProvinceStateId").val($("#window_popup_addresses_" +type+ "_province_state_id_"+position_key+ key).val());
							$("#DefaultCountryId").val($("#window_popup_addresses_" +type+ "_country_id_"+position_key+ key).val());

							$("#window_popup_addresses" + key).data("kendoWindow").close();
							contacts_auto_save_entry();
						}
						</script>

						<div class="tab_1_inner float_left">

							<?php for ($i=0; $i < 24; $i++) {
								$j = $i;
								if($j < 10)$j = '0'.$j;
								if($i > 7 && $i < 18){
									$arr_hour[$j.':00'] = array('name'=> $j.':00', 'value'=> $j.':00', 'class'=>'BgOptionHour');
									$arr_hour[$j.':30'] = array('name'=> $j.':30', 'value'=> $j.':30', 'class'=>'BgOptionHour');
								}else{
									$arr_hour[$j.':00'] = $j.':00';
									$arr_hour[$j.':30'] = $j.':30';
								}
							}
							?>

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Date of birth'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
								<?php echo $this->Form->input('Contact.date_of_birth', array(
										'class' => 'JtSelectDate input_1 float_left',
										'readonly' => true,
										'style' => 'width: 92%;'
								)); ?>
							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"><?php echo translate('Inactive'); ?></span>
							</p>
							<div class="width_in3 float_left indent_input_tp" style="padding-left: 8px;width: 48%;">
								<input type="hidden" name="data[Contact][inactive]" id="ContactInactive_" value="0">
								<div class="in_active2">
									<label class="m_check2">
										<?php echo $this->Form->input('Contact.inactive', array(
												'type' => 'checkbox'
										)); ?>
										<span></span>
									</label>
									<span class="inactive dent_check"></span>

									<p class="clear"></p>
								</div>
							</div>

							<!-- TIME TO WORK -->
							<p class="clear">
								<span class="label_1 float_left minw_lab"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">


							</div>

							<p class="clear">
								<span class="label_1 float_left minw_lab"></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
							</div>

							<p class="clear">
								<span class="label_1 float_left fixbor3 minw_lab hgt3"><!-- Identity --></span>
							</p>
							<div class="width_in3 float_left indent_input_tp">
							</div>
							<div class="width_in3 float_left indent_input_tp">
							</div>
							<div class="width_in3 float_left indent_input_tp">
							</div>

						</div>
					</div><!--END Tab1 -->
				</div>
			</div>
			<div class="clear"></div>
			<?php echo $this->Form->end(); ?>
		</div><!--  END DIV contacts_form_auto_save -->

		<!-- DIV MENU NGANG -->
		<div class="clear block_dent3">
			<div class="box_inner">

				<ul id="contacts_ul_sub_content" class="ul_tab">
					<!--  <li id="general" class="<?php if($sub_tab == 'general'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('General'); ?></a>
					</li> -->
					<li id="addresses" class="<?php if($sub_tab == 'addresses'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Addresses'); ?></a>
					</li>

					<style type="text/css">
						.displ<?php echo $this->data['Contact']['is_customer']; ?>{
							/*display: none;*/
						}
					</style>

					<!-- UL FOR CUSTOMER -->
					<!-- UL FOR EMPLOYEE -->

					<li id="enquiries" class="<?php if($sub_tab == 'enquiries'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Enquiries'); ?></a>
					</li>
					<li id="jobs" class="<?php if($sub_tab == 'jobs'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Jobs'); ?></a>
					</li>
					<li id="tasks" class="<?php if($sub_tab == 'tasks'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Tasks'); ?></a>
					</li>
					<?php if(isset($this->data['Contact']['is_customer']) && $this->data['Contact']['is_customer']) { ?>
					<li id="products" class="<?php if($sub_tab == 'products'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Products'); ?></a>
					</li>
					<li id="quotes" class="<?php if($sub_tab == 'quotes'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Quotes'); ?></a>
					</li>
					<li id="orders" class="<?php if($sub_tab == 'orders'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Orders'); ?></a>
					</li>
					<li id="shipping" class="<?php if($sub_tab == 'shipping'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Shipping'); ?></a>
					</li>
					<li id="account" class="<?php if($sub_tab == 'account'){ ?>active<?php } ?> li-toggle displ0">
						<a href="javascript:void(0)"><?php echo translate('Account'); ?></a>
					</li>
					<?php } ?>

					<!-- UL FOR EMPLOYEE -->
					<?php if(isset($this->data['Contact']['is_employee']) && $this->data['Contact']['is_employee']) { ?>
					<li id="personal" class="<?php if($sub_tab == 'personal'){ ?>active<?php } ?> li-toggle displ1">
						<a href="javascript:void(0)"><?php echo translate('Personal'); ?></a>
					</li>
					<li id="rates_wages" class="<?php if($sub_tab == 'rates_wages'){ ?>active<?php } ?> li-toggle displ1">
						<a href="javascript:void(0)"><?php echo translate('Rates/Wages'); ?></a>
					</li>
					<li id="expenses" class="<?php if($sub_tab == 'expenses'){ ?>active<?php } ?> li-toggle displ1">
						<a href="javascript:void(0)"><?php echo translate('Expenses'); ?></a>
					</li>
					<li id="leave" class="<?php if($sub_tab == 'leave'){ ?>active<?php } ?> li-toggle displ1">
						<a href="javascript:void(0)"><?php echo translate('Leave'); ?></a>
					</li>
					<li id="workings_holidays" class="<?php if($sub_tab == 'workings_holidays'){ ?>active<?php } ?> li-toggle displ1">
						<a href="javascript:void(0)"><?php echo translate('Working & Holidays'); ?></a>
					</li>
					<li id="user_refs" class="<?php if($sub_tab == 'user_refs'){ ?>active<?php } ?> li-toggle displ1">
						<a href="javascript:void(0)"><?php echo translate('User Prefs'); ?></a>
					</li>
					<?php } ?>
					<!-- END === UL FOR EMPLOYEE -->

					<li id="documents" class="<?php if($sub_tab == 'documents'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Documents'); ?></a>
					</li>
					<li id="other" class="<?php if($sub_tab == 'other'){ ?>active<?php } ?>">
						<a href="javascript:void(0)"><?php echo translate('Other'); ?></a>
					</li>
					<p class="clear"></p>
				</ul>

			</div>
		</div>

		<!-- DIV NOI DUNG CUA CAC SUBTAB -->
		<div id="contacts_sub_content" class="jt_sub_content clear_percent">
			<?php echo $this->element('../Contacts/' . $sub_tab); ?>
		</div>

	</div>
	<div class="clear"></div>
</div>

<?php echo $this->element('../Contacts/js'); ?>