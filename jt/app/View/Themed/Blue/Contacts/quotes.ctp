	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4><?php echo translate('Quotations for this contact'); ?></h4></span>
			<?php if( $this->Common->check_permission('quotations_@_entry_@_add', $arr_permission) ){ ?>
			<a href="<?php echo URL; ?>/contacts/quotes_add/<?php echo $contact_id; ?>" title="Add new quotation">
				<span class="icon_down_tl top_f"></span>
			</a>
			<?php } ?>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:6%;"></li>
			<li class="hg_padd" style="width:6%;"><?php echo translate('Ref no'); ?></li>
			<li class="hg_padd" style="width:8%;"><?php echo translate('Type'); ?></li>
			<li class="hg_padd center_txt" style="width:6%;"><?php echo translate('Date'); ?></li>
			<li class="hg_padd" style="width:6%;"><?php echo translate('Due date'); ?></li>
			<li class="hg_padd" style="width:8%;"><?php echo translate('Status'); ?></li>
			<li class="hg_padd" style="width:10%;"><?php echo translate('Our rep'); ?></li>
			<li class="hg_padd" style="width:6%;"><?php echo translate('Tax'); ?></li>
			<li class="hg_padd" style="width:32%;"><?php echo translate('Heading'); ?></li>
			<li class="hg_padd bor_mt" style="width:1%;"></li>
		</ul>
		<div class="container_same_category" style="height: auto;overflow: visible;">
			<?php
			$i = 1; $count = 0;
			foreach ($arr_quotation as $key => $value) {
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="Contact_Quotation_<?php echo $value['_id']; ?>">

				<li class="hg_padd" style="width:6%;">
					<a href="<?php echo URL; ?>/quotations/entry/<?php echo $value['_id']; ?>">
						<span class="icon_emp"></span>
					</a>
				</li>
				<li class="hg_padd" style="width:6%;"><?php echo $value['code']; ?></li>
				<li class="hg_padd" style="width:8%;"><?php echo $value['quotation_type']; ?></li>
				<li class="hg_padd center_txt" style="width:6%;"><?php echo $this->Common->format_date($value['quotation_date']->sec); ?></li>
				<li class="hg_padd" style="width:6%;"><?php if(isset($value['payment_due_date']) && is_object($value['payment_due_date']))echo $this->Common->format_date($value['payment_due_date']->sec); ?></li>
				<li class="hg_padd" style="width:8%;"><?php echo $value['quotation_status']; ?></li>
				<li class="hg_padd" style="width:10%;"><?php echo $value['our_rep']; ?></li>
				<li class="hg_padd" style="width:6%;"><?php echo $value['tax']; ?></li>
				<li class="hg_padd" style="width:32%;"><?php echo $value['name']; ?></li>
				<?php if($this->Common->check_permission('quotations_@_entry_@_delete', $arr_permission)){?>
				<li class="hg_padd bor_mt" style="width:1%;">
					<div class="middle_check">
						<a title="Delete link" href="javascript:void(0)" onclick="resource_remove_quotation_contact('<?php echo $value['_id']; ?>')">
							<span class="icon_remove2"></span>
						</a>
					</div>
				</li>
				<?php } ?>
			</ul>

			<?php $i = 3 - $i; $count += 1;
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
					</ul>
			  <?php $i = 3 - $i;
					}
				}
			?>
		</div>

		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
				<?php echo translate('Click to view full details'); ?>
			</span>
		</span>
	</div>

<script type="text/javascript">
<?php if($this->Common->check_permission('quotations_@_entry_@_delete', $arr_permission)){?>
function resource_remove_quotation_contact(quotation_id){
	confirms( "Message", "Are you sure you want to delete?",
		function(){
			$.ajax({
				 url: '<?php echo URL; ?>/contacts/quotes_delete/' + quotation_id,
				 timeout: 15000,
				 success: function(html){
					 if(html == "ok"){
						 $("#Contact_Quotation_" + quotation_id).fadeOut();
					 }else{
						 alerts("Error: ", html);
					 }
				 }
			 });
		},function(){
			//else do somthing
	});
}
<?php } ?>
</script>