<div class="clear_percent_10 float_left">
	<div id="employee_details" class="tab_1 full_width">
		<?php echo $this->element('../Contacts/employee_details'); ?>
	</div><!--END Tab1 -->
</div>
<div id="load_other_contact" class="clear_percent_11 float_left">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent">
				<h4><?php echo translate('Emergency'); ?> / <?php echo translate('other contacts'); ?></h4>
			</span>
			<a id="other_contact_add" href="javascript:void()" title="Link a contact">
				<span class="icon_down_tl top_f"></span>
			</a>
		</span>
		<p class="clear"></p>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:1.5%"></li>
			<li class="hg_padd" style="width:30%"><?php echo translate('Name'); ?></li>
			<li class="hg_padd" style="width:9%"><?php echo translate('Phone'); ?></li>
			<li class="hg_padd" style="width:50%;"><?php echo translate('Relationship'); ?> / <?php echo translate('notes'); ?></li>
			<li class="hg_padd bor_mt" style="width:1.5%"></li>
		</ul>
		<div class="container_same_category other_contact" id="contacts_other_contact">
			<?php echo $this->element('../Contacts/other_contact'); ?>
		</div>
		<span class="title_block bo_ra2"></span>
	</div>
	<!--END Tab1 -->
</div>
<script type="text/javascript">
	$(function(){
		var id = $('#ContactId').val();
		$('#other_contact_add').click(function(){
			$.ajax({
				url: '<?php echo URL; ?>/contacts/other_contact/'+id,
				type: 'post',
				data: {type: 'add'},
				success: function(html){
					$("#contacts_other_contact").html(html);
				}
			});
			return false;
		});
	});
</script>