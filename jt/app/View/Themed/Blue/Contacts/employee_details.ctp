<?php
	if(isset($contact['employee_details'])&&count($contact['employee_details'])>0)
		$value = $contact['employee_details'];
?>
<div class="title_block bo_ra1">
	<span class="title_block_inner"><h4><?php echo translate('Employee details'); ?></h4></span>
</div>
<div class="tab_2_inner">
	<p class="clear">
		<span class="label_1 float_left title_block_inner"><?php echo translate('Marital status') ; ?></span>
		</p><div class="width_in5 float_left indent_input_tp">
				<?php
					echo $this->Form->input('marital_status',array(
							   'class'=>'input_1 float_left',
							   'name'=>'marital_status',
							   'value'=> (isset($value['marital_status']) ? $value['marital_status']  : ''),
							   ));
				?>
				<input type="hidden" name="marital_status_id" id="marital_statusId" value="<?php echo (isset($value['marital_status']) ? $value['marital_status']  : ''); ?>" />
			   <script type="text/javascript">
				   $(function () {
					   $("#marital_status").combobox(<?php echo json_encode($arr_data['marital_status']); ?>);
				   });
				</script>
		</div>
	<p></p>
	<p class="clear">
		<span class="label_1 float_left title_block_inner"><?php echo translate('SSN/PPS no') ; ?></span>
		</p><div class="width_in5 float_left indent_input_tp">
			<input class="input_1 float_left" name="ssb_pps_no" id="ssb_pps_no" type="text" value="<?php if(isset($value['ssb_pps_no'])) echo $value['ssb_pps_no']; ?>">
		</div>
	<p></p>
	<p class="clear">
		<span class="label_1 float_left title_block_inner fixbor3a"></span>
		</p><div class="width_in5 float_left indent_input_tp"></div>
	<p></p>
	<p class="clear"></p>
	<div>
		<div class="title_block">
			<span class="fl_dent">
				<h4><?php echo translate('Employment details'); ?></h4>
			</span>
		</div>
		<div class="tab_2_inner">
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Start date'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php
					   echo $this->Form->input('start_date',array(
						   'class'=>'JtSelectDate input_1 float_left',
						   'name'=>'start_date',
						   'value'=> (isset($value['start_date'])&&$value['start_date']!='' ? date('m/d/Y', $value['start_date']->sec) : ''),
						   ));
				   ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Finish date'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<?php
					   echo $this->Form->input('finish_date',array(
						   'class'=>'JtSelectDate input_1 float_left',
						   'name'=>'finish_date',
						   'value'=> (isset($value['finish_date'])&&$value['finish_date']!='' ? date('m/d/Y', $value['finish_date']->sec) : ''),
						   ));
				   ?>
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner"><?php echo translate('Weeks worked'); ?></span>
				</p><div class="width_in5 float_left indent_input_tp">
					<input class="input_1 float_left" disable="disable" readonly="readonly" id="weeks_worked" name="weeks_worked" type="text" value="<?php if(isset($value['weeks_worked'])) echo $value['weeks_worked']; ?>">
				</div>
			<p></p>
			<p class="clear">
				<span class="label_1 float_left title_block_inner fixbor3a"></span>
				</p><div class="width_in5 float_left indent_input_tp"></div>
			<p></p>
			<p class="clear"></p>
		</div>
	</div>
	<p class="clear"></p>
</div>
<span class="title_block bo_ra2"></span>



<script type="text/javascript">
$(function(){
    $('#employee_details input,select').change(function(){
		var key = $(this).attr('name');
		var value = $(this).val();
		$.ajax({
			url: '<?php echo URL; ?>/contacts/employee_details',
			type: 'POST',
			data: $(':input', '#employee_details').serialize(),
			success: function(result){
				if( $.trim(result) != "" )
					$('#weeks_worked').val(result);
				console.log(result);
			}
		});
    })
});
</script>