<?php echo $this->element('js/permission_entry');?>
<script type="text/javascript">
    $(function() {
        contacts_update_entry_header();
    });

    function contacts_update_entry_header() {
        $("#contact_name_header").html($("#ContactFirstName").val() + " " + $("#ContactLastName").val());
        if ($("#ContactIsCustomer").is(":checked")) {
            $("#contact_account_header").html("Customer");

        } else if ($("#ContactIsEmployee").is(":checked")) {
            $("#contact_account_header").html("Employee");

        } else {
            $("#contact_account_header").html("Contact");
        }
        $("#contact_company_header").html($("#ContactCompany").val());
    }

    function contacts_adddress_update_addresses_in_subtab(){

        var ContactAddressesDefaultKey = $("#ContactAddressesDefaultKey").val();

        // kiểm tra xem tab Addresses có active không
        if( $("#contacts_addresses_" + ContactAddressesDefaultKey).attr("id") != undefined ){

            var contain = $("#contacts_addresses_" + ContactAddressesDefaultKey);
            var contain_entry_panel = $("#contacts_form_auto_save");
            // thay đổi địa chỉ default

            $("#AddressAddress1", contain).val($("#DefaultAddress1", contain_entry_panel).val());
            $("#AddressAddress2", contain).val($("#DefaultAddress2", contain_entry_panel).val());
            $("#AddressAddress3", contain).val($("#DefaultAddress3", contain_entry_panel).val());
            $("#AddressTownCity", contain).val($("#DefaultTownCity", contain_entry_panel).val());
            $("#AddressZipPostcode", contain).val($("#DefaultZipPostcode", contain_entry_panel).val());

            // kiểm tra xem có đổi country không để load lại danh sách tỉnh thành
            if( $("#DefaultCountryId", contain_entry_panel).val() != $("#Address" +ContactAddressesDefaultKey+ "CountryId", contain).val() ){
                change_province(ContactAddressesDefaultKey, $("#DefaultCountryId", contain_entry_panel).val());
                setTimeout("contacts_adddress_update_addresses_in_subtab_country_province(" +ContactAddressesDefaultKey+ ")", 1800);

            }else{
                contacts_adddress_update_addresses_in_subtab_country_province(ContactAddressesDefaultKey);
            }
        }
    }

    function contacts_adddress_update_addresses_in_subtab_country_province(ContactAddressesDefaultKey){
        var contain = $("#contacts_addresses_" + ContactAddressesDefaultKey);
        var contain_entry_panel = $("#contacts_form_auto_save");
        $("#Address" +ContactAddressesDefaultKey+ "Country", contain).val($("#DefaultCountry", contain_entry_panel).val());
        $("#Address" +ContactAddressesDefaultKey+ "CountryId", contain).val($("#DefaultCountryId", contain_entry_panel).val());
        $("#Address" +ContactAddressesDefaultKey+ "ProvinceState", contain).val($("#DefaultProvinceState", contain_entry_panel).val());
        $("#Address" +ContactAddressesDefaultKey+ "ProvinceStateId", contain).val($("#DefaultProvinceStateId", contain_entry_panel).val());

    }

    function contacts_auto_save_entry(object) {
        if ($.trim($("#ContactHeading").val()) == "") {
            $("#ContactHeading").val("#" + $("#ContactNo").val() + "-" + $("#ContactCompanyName").val());
        }
        contacts_update_entry_header();
        $("form :input", "#contacts_form_auto_save").removeClass('error_input').removeClass('error_input');
        $(".jt_ajax_note").hide();

        if( $(object).attr("id") == "ContactFirstName" || $(object).attr("id") == "ContactLastName" ){
            if( $("#user_refs").hasClass("active") ){
                $("#ContactUserNameContact").val( $("#ContactFirstName").val() + " " + $("#ContactLastName").val() );
            }
        }

        contacts_adddress_update_addresses_in_subtab();

        $.ajax({
            url: '<?php echo URL; ?>/contacts/auto_save',
            timeout: 15000,
            type: "post",
            data: $("form", "#contacts_form_auto_save").serialize(),
            success: function(html) {
                if($.trim(html) == "ref_no_existed") {
                    $("#ContactNo").addClass('error_input');
                    ajax_note_set('This "no" existed');

                }else if($.trim(html) == "full_name_existed") {
                    $("#ContactFirstName").addClass('error_input');
                    $("#ContactLastName").addClass('error_input');
                    ajax_note_set('This first name and last name existed, please change to anothor');

                }else if (html == "email_not_valid") {
                    $("#ContactEmail").addClass('error_input');
                    ajax_note_set('Email not valid, please check email field!');

                } else if (html != "ok") {
                    alerts('Message', html);
                }

                if( $(object).attr("id") == "ContactIsCustomer" )location.reload(true);

                console.log(html); // view log when debug
            }
        });
    }
</script>
