	<div class="clear_percent_18 float_left" id="other_details">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left h_form">
					<span class="fl_dent"><h4>Other details</h4></span>
					<?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
					<?php echo $this->Js->link( '<span class="icon_down_tl top_f"></span>', '/contacts/other_add/'.$contact_id,
						array(
							'update' => '#contacts_sub_content',
							'title' => 'Add new line',
							'escape' => false
						) );
					?>
					<?php } ?>
				</span>
			</span>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd" style="width:30%;">Heading</li>
				<li class="hg_padd" style="width:64%;">Details</li>
				<li class="hg_padd bor_mt" style="width:1.8%;"></li>
			</ul>
			<div id="contacts_other">
				<?php $count = 0;

				$i = 1;  $k = 0;
				if(isset($arr_contact['other'])){

					foreach($arr_contact['other'] as $key => $value){
						$k=$k+1;
						if( $value['deleted'] )continue;
					?>

					<?php echo $this->Form->create('Other', array('id' => 'OtherEntryForm_'.$key)); ?>
					<?php echo $this->Form->hidden('Other.key', array( 'value' => $key )); ?>
					<?php echo $this->Form->hidden('Other._id', array( 'value' => $contact_id )); ?>

					<ul class="ul_mag clear bg<?php echo $i; ?>" id="contacts_other_<?php echo $key; ?>">

						<li class="hg_padd" style="width:30%;">
							<?php echo $this->Form->input('Other.heading', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['heading']
							)); ?>

						</li>
						<li class="hg_padd" style="width:64%;">
							<?php echo $this->Form->input('Other.details', array(
								'class' => 'input_inner input_inner_w bg'.$i,
								'value' => $value['details']
							)); ?>
						</li>
					    <?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
						<li class="hg_padd bor_mt" style="width:1.8%">
							<div class="middle_check">
								<a title="Delete link" href="javascript:void(0)" onclick="contacts_other_delete(<?php echo $key; ?>)">
									<span class="icon_remove2"></span>
								</a>
							</div>
						</li>
						<?php } ?>
					</ul>

					<?php echo $this->Form->end(); ?>

				<?php $i = 3 - $i; $count += 1;
					}
				}

				$count = 8 - $count;
				if( $count > 0 ){
					for ($j=0; $j < $count; $j++) { ?>
						<ul class="ul_mag clear bg<?php echo $i; ?>">
						</ul>
			  <?php $i = 3 - $i;
					}
				}
				?>
			</div>
			<p class="clear"></p>
			<span class="hit"></span>
			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
	<div class="clear_percent_16 float_left">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left h_form">
					<span class="fl_dent"><h4>Group linked to this copany</h4></span>
					<!-- <a title="Link a contact" href="javascript:void(0)">
						<span class="icon_down_tl top_f"></span>
					</a> -->
				</span>
			</span>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd bor_mt" style="width:98%;">Group</li>
			</ul>
			<?php

			$count = 0; $i = 1;

			$count = 8 - $count;
			if( $count > 0 ){ $i = 3 - $i;
				for ($j=0; $j < $count; $j++) { ?>
					<ul class="ul_mag clear bg<?php echo $i; ?>">
						<li class="hg_padd bor_mt" style="width:98%;"></li>
					</ul>
		  <?php $i = 3 - $i;
				}
			}
			?>

			<p class="clear"></p>
			<span class="hit"></span>
			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
	<div class="clear_percent_17 float_right">
		<form method="POST" id="profile_form">
			<div class="tab_1 full_width" id="block_full_otherpricing">
				<!-- Header-->
				<span class="title_block bo_ra1">
				  <span class="fl_dent">
					 <h4>Custom fields</h4>
				  </span>
				</span>
				<!--CONTENTS-->
				<div class="jt_subtab_box_cont" style=" height:209px;">
				  <div class="tab_2_inner">
					   <p class="clear">
						  <span class="label_1 float_left minw_lab2">Custom 1 </span>
					   </p>
					   <div class="width_in3 float_left indent_input_tp" id="shipping_province" style="width:61.5%">
						   <input name="custom_field_1" value="<?php if(isset($arr_return['custom_field_1'])) echo $arr_return['custom_field_1'];?>"  id="custom_field_1" class="input_select" readonly="readonly" type="text">
					   </div>
					   <p></p>
					   <p class="clear">
						  <span class="label_1 float_left minw_lab2">Custom 2</span>
					   </p>
					   <div class="width_in3 float_left indent_input_tp" style=" width: 61.5%; ">
							  <input name="custom_field_2" value="<?php if(isset($arr_return['custom_field_2'])) echo $arr_return['custom_field_2'];?>"  id="custom_field_2" class="input_select" readonly="readonly" type="text">
					   </div>
					   <p></p>
					   <p class="clear">
						  <span class="label_1 float_left minw_lab2" style="height:50%" >Custom 3</span>
					   </p>
					   <div class="width_in3 float_left indent_input_tp" style=" width: 61.5%; ">
							<input name="custom_field_3" value="<?php if(isset($arr_return['custom_field_3'])) echo $arr_return['custom_field_3'];?>"  id="custom_field_3" class="input_select" readonly="readonly" type="text">
							<script type="text/javascript">
							   $(function () {
								  $("#custom_field_1").combobox(<?php echo json_encode($arr_custom_field); ?>);
								   $("#custom_field_2").combobox(<?php echo json_encode($arr_custom_field); ?>);
									$("#custom_field_3").combobox(<?php echo json_encode($arr_custom_field); ?>);
							   });
							</script>
					   </div>
					   <p></p>

					 <?php for($i=2;$i<6;$i++){?>
						<p class="clear"><span class="label_1 float_left minw_lab2" >&nbsp;</span></p>
						<div class="width_in3 float_left indent_input_tp" style=" width: 61.5%; ">&nbsp;</div>
						<p></p>
					<?php } ?>
						<p class="clear"><span class="label_1 fixbor3 float_left minw_lab2" >&nbsp;</span></p>
						<div class="width_in3 float_left indent_input_tp" style=" width: 61.5%; ">&nbsp;</div>
						<p></p>
					</div>
				</div><!--END Tab2  inner-->
				<span class="title_block bo_ra2" style="display: block;"></span>
			</div>
		</form>

	</div><!--END Tab2  inner-->
	<div style="clear:both"></div>


<script type="text/javascript">
$(function(){
	<?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
	$("form :input", "#other_details").change(function() {
		var contain = $(this).closest('form');
		$.ajax({
			url: '<?php echo URL; ?>/contacts/other_auto_save',
			timeout: 15000,
			type:"post",
			data: $(this).closest('form').serialize(),
			success: function(html){
				if( html != "ok" )alerts("Error: ", html);
			}
		});
	});

	$("form#profile_form input,select").change(function() {
		var fieldname =$(this).attr("name");
		var values = $(this).val();
		var ids = $("#ContactId").val();
		var fieldtype = $(this).attr("type");
		if(fieldtype=='checkbox'){
			if($(this).is(':checked'))
				values = 1;
			else
				values = 0;
		}
		$.ajax({
			url: '<?php echo URL; ?>/<?php echo $controller;?>/save_data_for_non_model',
			timeout: 15000,
			type: "POST",
			data: { fieldname : fieldname,values:values,ids:ids },
			success: function(html){
				console.log(html);
			}
		});
		return false;
	});

	// co quyen VIEW
	<?php }else{ ?>
		$(":input", "#contacts_sub_content").each(function() {
			$(this).attr("disabled", true);
		});
		$(".combobox_button, .indent_dw_m, .icon_down_new", "#contacts_sub_content").each(function() {
			$(this).remove();
		});
	<?php } ?>
});

<?php if( $this->Common->check_permission('contacts_@_entry_@_edit', $arr_permission) ){ ?>
function contacts_other_delete(key){
	confirms( "Message", "Are you sure you want to delete?",
		function(){
			$.ajax({
				url: '<?php echo URL; ?>/contacts/other_delete/'+ key + '/<?php echo $contact_id; ?>',
				success: function(html){
					if(html == "ok"){
						$("#OtherEntryForm_" + key).fadeOut();
					}
					console.log(html);
				}
			});
		},function(){
			console.log("Cancel 123");
			return false;
		}
	);
}
<?php } ?>
</script>