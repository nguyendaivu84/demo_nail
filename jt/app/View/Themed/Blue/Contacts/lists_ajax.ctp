<?php $i = 1; ?><br>
<?php foreach ($arr_contacts as $value): ?>
	<?php
		$i = 3 - $i;
	?>
	<ul class="ul_mag clear bg<?php echo $i ?>" id="contacts_<?php echo (string) $value['_id']; ?>">
		<li class="hg_padd" style="width:1%">
			<a style="color: blue" href="<?php echo URL; ?>/contacts/entry/<?php echo (string) $value['_id']; ?>"><span class="icon_emp"></span></a>
		</li>
		<li class="hg_padd" style="width:6%">
			<?php
			if ($value['is_employee']) {
				echo 'Employee';
			} else {
				echo 'Customer';
			}
			?>
		</li>
		<li class="hg_padd" style="width:6%">
			<?php if (isset($arr_contacts_title[$value['title']])) echo $arr_contacts_title[$value['title']]; ?>
		</li>
		<li class="hg_padd" style="width:6%"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></li>
		<li class="hg_padd" style="width:6%"><?php echo $value['direct_dial']; ?></li>
		<li class="hg_padd" style="width:6%"><?php echo $value['mobile']; ?></li>
		<li class="hg_padd" style="width:12%"><?php echo $value['email']; ?></li>
		<li class="hg_padd center_txt" style="width:3%">
			<div class="select_inner width_select" style="width: 100%; margin: 0;margin-left: 17px;">
				<label class="m_check2">
					<?php
					echo $this->Form->input('Contact.inactive', array(
						'type' => 'checkbox',
						'disabled' => true,
						'checked' => (isset($value['inactive'])) ? $value['inactive'] : false
					));
					?>
				</label>
			</div>
		</li>
		<li class="hg_padd" style="width:20%">
			<?php
			if (is_object($value['company_id'])) {
				echo '<a style="color: blue" href="'.URL.'/companies/entry/' . $value['company_id'] . '"><span class="icon_emp" style="float: left;"></span></a>';
				if(!isset($arr_company_tmp))$arr_company_tmp = array();
				if( !isset($arr_company_tmp[(string)$value['company_id']]) ){
					$arr_company = $model_company->select_one(array('_id' => new MongoId($value['company_id'])), array('_id', 'name'));
					if(isset($arr_company['name'])){
						$arr_company_tmp[(string)$value['company_id']] = $arr_company['name'];
						echo $arr_company['name'];
					}
				}else{
					echo $arr_company_tmp[(string)$value['company_id']];

				}
			}
			?>
		</li>
		<li class="hg_padd center_txt" style="width:6%">
			<?php
			if (isset($arr_companies[(string) $value['company_id']])) {
				if ($arr_companies[(string) $value['company_id']]['is_customer']) {
					echo 'Customer';
				} else {
					echo 'Supplier';
				}
			}
			?>
		</li>
		<?php if( $this->Common->check_permission($controller.'_@_entry_@_delete', $arr_permission) ){ ?>
		<li class="hg_padd bor_mt" style="width:3%">
			<div class="middle_check">
				<a href="javascript:void(0)" title="Delete link" onclick="contacts_lists_delete('<?php echo (string) $value['_id']; ?>')">
					<span class="icon_remove2"></span>
				</a>
			</div>
		</li>
		<?php } ?>
	</ul>
<?php endforeach ?>

<?php echo $this->element('popup/pagination_lists'); ?>