<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent"><h4><?php echo translate('Working hours for this contact'); ?></h4></span>
	</span>
	<p class="clear"></p>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd" style="width:1.5%"></li>
		<li class="hg_padd" style="width:7%"><?php echo translate('DAY'); ?></li>
		<li class="hg_padd" style="width:19%"><?php echo translate('Time 1'); ?></li><!-- center_txt -->
		<li class="hg_padd" style="width:19%"><?php echo translate('Time 2'); ?></li>
		<li class="hg_padd line_mg" style="width:19%"><?php echo translate('Time 3'); ?></li>
	</ul>
	<div id="contacts_work_hour">
		<?php
			$arr_day = array( 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' );
			for ($i=0; $i < 24; $i++) {
				$j = $i;
				if($j < 10)$j = '0'.$j;
				if($i > 7 && $i < 18){
					$arr_hour[$j.':00'] = array('name'=> $j.':00', 'value'=> $j.':00', 'class'=>'BgOptionHour');
					$arr_hour[$j.':30'] = array('name'=> $j.':30', 'value'=> $j.':30', 'class'=>'BgOptionHour');
				}else{
					$arr_hour[$j.':00'] = $j.':00';
					$arr_hour[$j.':30'] = $j.':30';
				}
			}



			$i = 1;
			for ($k=0; $k < 7; $k++) {
		?>
			<?php echo $this->Form->hidden('Work._id', array( 'value' => $contact_id )); ?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="contacts_work_hour_<?php echo $k; ?>">
				<li class="hg_padd" style="width:1.5%"></li>
				<li class="hg_padd" style="width:7%"><?php echo $arr_day[$k]; ?></li>

				<!-- TIME 1 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from1', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time1',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time1']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to1', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time2',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time2']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:7%"></li>

				<!-- TIME 2 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from2', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time3',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k]['time3'])?$workings_hours[$k]['time3']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to2', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time4',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k]['time4'])?$workings_hours[$k]['time4']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:7%"></li>

				<!-- TIME 3 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from3', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time5',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k]['time5'])?$workings_hours[$k]['time5']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to3', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time6',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k]['time6'])?$workings_hours[$k]['time6']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:7%"></li>
			</ul>
			<?php $i = 3 - $i;
			}
		?>
	</div>
	<span class="hit"></span>
	<span class="title_block bo_ra2">
		<span class="float_left bt_block"></span>
		<span class="float_left left_text dent_bl_txt"></span>
	</span>
</div>

<script type="text/javascript">
$(function(){
	$(":input", "#contacts_sub_content").change(function() {
		$.ajax({
			url: '<?php echo URL; ?>/contacts/work_hour_auto_save/' + $("#ContactId").val(),
			timeout: 15000,
			type:"post",
			data: { day: $(this).attr("rel"), time: $(this).attr("time"), value: $(this).val() },
			success: function(html){
				if( html == "reload" ){
					$("#workings_holidays").click();
				}else if( html != "ok" ){
					alerts("Error: ", html, function(){ $("#workings_holidays").click(); });
				}
			}
		});
	});
});
</script>