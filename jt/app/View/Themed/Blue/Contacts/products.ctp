<div class="float_left " style=" width:16%; float: left;">
    <div class="tab_1 full_width" style="">
        <span class="title_block bo_ra1">
            <span class="float_left">
                <span class="fl_dent"><h4><?php echo translate('Pricing category'); ?></h4></span>
            </span>
        </span>
        <div class="tab_2_inner" style="height:198px;" id="contact_product_left">
            <p class="clear">
                <span class="label_1 float_left minw_lab2"><?php echo translate('Use category'); ?></span>
            </p>
            <div class="width_in3a float_left indent_input_tp">
                <?php echo $this->Form->input('Contact.sell_category', array(
                        'class' => 'input_select',
                        'onchange' => 'contact_products_save_left(this)',
                        'readonly' => true
                )); ?>
                <script type="text/javascript">
                    $(function(){
                        $("#ContactSellCategory").combobox(<?php echo json_encode($arr_products_sell_category); ?>)
                    });
                </script>
            </div>
            <p></p>
            <p class="clear">
                <span class="label_1 float_left minw_lab2 fixbor4"><?php echo translate('Discount %'); ?></span>
            </p>
            <div class="width_in3a float_left indent_input_tp">
                <?php echo $this->Form->input('Contact.discount', array(
                        'class' => 'input_1 float_left',
                        'onchange' => 'contact_products_save_left(this)',
                )); ?>
            </div>
            <!--Save-->
            <script type="text/javascript">
                function contact_products_save_left(){
                    $.ajax({
                        url:"<?php echo URL; ?>/contacts/products_save_left/" + $("#ContactId").val(),
                        timeout:15000,
                        type:"POST",
                        data: $(":input","#contact_product_left").serialize(),
                        success: function(html){
                            if(html !="ok"){
                                alerts("Error: ", html);
                            }
                        }
                    });
                }
            </script>

            <p class="clear">
                <span class="label_1 float_left minw_lab2 " style="height:133px"></span>
            </p><div class="width_in3a float_left indent_input_tp">
            <div class="color_hidden">Note: Discount is applied to all products and services from the Products module. <br><br> Specific pricing overrides category pricing <br><br> Discount is not applied to specific pricing.</div>
            </div>
            <p></p>
            <p class="clear"></p>
        </div>
        <span class="title_block bo_ra2"></span>
    </div>
</div>
<div class="float_left" style=" width:49%;margin-left:1%;float: left;">
    <div class="tab_1 full_width">

        <span class="title_block bo_ra1">
            <span class="fl_dent">
                <h4><?php echo translate('Specific pricing for this contact'); ?></h4>
            </span>
            <span class="icon_down_tl top_f" id="click_to_opent_window_choose_product"></span>
            <script type="text/javascript">
                $(function(){
                    window_popup('products', 'Specify Product', '', 'click_to_opent_window_choose_product', "?no_supplier=1&products_product_type=Product&products_group_type=SELL");
                });
                function after_choose_products(product_id){
                   window.location = "<?php echo URL; ?>/contacts/products_pricing/" + $("#ContactId").val() + "/0/" + product_id;
                }
            </script>
        </span>

        <p class="clear"></p>
        <ul class="ul_mag clear bg3">
            <li class="hg_padd" style="width:3%;"></li>
            <li class="hg_padd" style="width:9%;">Code</li>
            <li class="hg_padd" style="width:56%;">Name</li>
            <li class="hg_padd center_txt" style="width:11%;">Range</li>
            <li class="hg_padd right_txt" style="width:8%;">Unit price</li>
            <li class="hg_padd bor_mt" style="width:1.8%;"></li>
        </ul>
        <div id="company_pricing_container_same" class="container_same_category" style="height: 176px;overflow-y: auto;">
            <?php
                $i = 1; $count = 0;
                if( isset($this->data['Contact']['pricing']) ){

                    foreach ($this->data['Contact']['pricing'] as $key => $value) {
                        //pr($value);

                        if( $value['deleted'] )continue;
                    ?>
                    <ul class="ul_mag clear bg<?php echo $i; ?>" id="company_pricing_<?php echo $key; ?>">
                        <li class="hg_padd" style="width:3%;">
                            <a href="<?php echo URL; ?>/contacts/products_pricing/<?php echo $contact_id; ?>/<?php echo $key; ?>">
                                <span class="icon_emp"></span>
                            </a>
                        </li>
                        <li class="hg_padd" style="width:9%;"><?php echo $value['code']; ?></li>
                        <li class="hg_padd" style="width:56%;"><?php echo $value['name']; ?></li>
                        <li class="hg_padd center_txt" style="width:11%;">
                            <?php
                                $range = '';
                                if( isset($value['price_break']) && isset($value['price_break'][0]) ){
                                    $range = $value['price_break'][0]['range_from'].' - '.$value['price_break'][0]['range_to'];
                                }
                                echo $range;
                            ?>
                        </li>
                        <li class="hg_padd right_txt" style="width:8%;">
                            <?php
                                $unit_price = '';
                                if( isset($value['price_break']) && isset($value['price_break'][0]) && is_numeric($value['price_break'][0]['unit_price']) ){
                                    $unit_price = number_format($value['price_break'][0]['unit_price'], 2);
                                }
                                echo $unit_price;
                            ?>
                        </li>
                        <li class="hg_padd bor_mt" style="width:1.8%;">
                            <div class="middle_check">
                                <a title="Delete link" href="javascript:void(0)" onclick="company_price_delete(<?php echo $key; ?>)">
                                    <span class="icon_remove2"></span>
                                </a>
                            </div>
                        </li>
                    </ul>
                    <?php $i = 3 - $i;
                        $count += 1;
                    }
                }

                $count = 8 - $count;
                if( $count > 0 ){
                    for ($j=0; $j < $count; $j++) { ?>
                        <ul class="ul_mag clear bg<?php echo $i; ?>">
                        </ul>
                        <?php $i = 3 - $i;
                    }
                }
            ?>
        </div>
        <span class="title_block bo_ra2">
            <span class="float_left bt_block">
                <?php echo translate('Click to view full details'); ?>
            </span>
        </span>
    </div>

    <script type="text/javascript">
        function company_price_delete(key){
            confirms( "Message", "Are you sure you want to delete?",
                function(){
                    $.ajax({
                         url: '<?php echo URL; ?>/companies/products_delete/' + $("#CompanyId").val() + "/" + key,
                         timeout: 15000,
                         success: function(html){
                             if(html == "ok"){
                                $("#company_pricing_" + key).remove();
                             }else{
                                alerts("Error: ", html);
                             }
                         }
                     });
                },function(){
                    //else do somthing
            });
        }
    </script>
</div>

<div class="float_left " style=" width:33%;margin-left:1%; float: left;">
        <div class="tab_1 full_width">
            <span class="title_block bo_ra1">
                <span class="float_left">
                    <span class="fl_dent"><h4><?php echo translate('Units / assets for this contact'); ?></h4></span>
                </span>
            </span>
            <p class="clear"></p>
            <ul class="ul_mag clear bg3">
            </ul>
            <div class="container_same_category" style="height: 176px;overflow-y: auto;">
                <?php
                $i = 1; $count = 0;

                    ?>
                    <ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_salesorder">
                    </ul>
                    <?php $i = 3 - $i;
                    $count += 1;
                $count = 8 - $count;
                if( $count > 0 ){
                    for ($j=0; $j < $count; $j++) { ?>
                        <ul class="ul_mag clear bg<?php echo $i; ?>">
                        </ul>
                        <?php $i = 3 - $i;
                    }
                }
                ?>
            </div>
            <span class="title_block bo_ra2">
                <span class="float_left bt_block">
                    <?php echo translate('Click to view full details'); ?>
                </span>
            </span>
        </div>
    </div>
