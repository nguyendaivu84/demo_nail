<style type="text/css">
	ul.ul_mag li.hg_padd {
	overflow: visible !important;
	}
	.bg4 {
	background: none repeat scroll 0 0 #949494;
	color: #fff;
	}
	.bg4 span h4 {
	margin-left: 1%;
	width: 100%;
	}

</style>
<?php echo $this->element('window'); ?>
<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4>General</h4>
		</span>
	</span>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd" style="width:20%"><?php echo translate('Name'); ?></li>
		<li class="hg_padd" style="width:75%"></li>
	</ul>
	<div class="container_same_category" style="height: 449px;overflow-y: auto">
		<ul class="ul_mag clear bg1" style="height:30px;line-height: 30px">
			<li class="hg_padd line_mg" style="width:20%;line-height:19px;height:22px;">
				<input type="hidden" id="_idAccountant" value="<?php if(isset($accountant['_id'])) echo $accountant['_id']; ?>" />
				Accountant
				<span class="iconw_m"  id="click_open_window_contactsaccountant" title="Specify Accountant"></span>
			</li>
			<li class="hg_padd line_mg" style="width:75%;line-height:19px;height:22px;;">
				<input type="text" id="Accountant" class="input_inner input_inner_w float_left" name"data[accountant]" value="<?php if(isset($accountant['accountant'])) echo $accountant['accountant']; ?>" />
                <input type="hidden" id="AccountantId" name"data[accountant_id]" value="<?php if(isset($accountant['accountant_id'])) echo $accountant['accountant_id']; ?>" />
                 <script type="text/javascript">
                        $(function(){
                            window_popup('contacts', 'Specify Accountant','accountant','click_open_window_contactsaccountant','?is_employee=1');
                        });
                </script>
			</li>
		</ul>
		<ul class="ul_mag clear bg2" style="height:30px;line-height: 30px">
			<li class="hg_padd line_mg" style="width:20%;line-height:19px;height:22px;">
				<input type="hidden" id="_idProduct" value="<?php if(isset($product['_id'])) echo $product['_id']; ?>" />
				Minimum Order Adjustment
				<span class="iconw_m"  id="click_open_window_productsminimum" title="Specify Products"></span>
			</li>
			<li class="hg_padd line_mg" style="width:75%;line-height:19px;height:22px;;">
				<input type="text" id="ProductName" class="input_inner input_inner_w float_left bg2" name"data[product]" value="<?php if(isset($product['product_name'])) echo htmlentities($product['product_name']); ?>" />
                <input type="hidden" id="ProductId" name"data[product_id]" value="<?php if(isset($product['product_id'])) echo $product['product_id']; ?>" />
                 <script type="text/javascript">
                        $(function(){
                            window_popup('products', 'Specify Products','minimun_order','click_open_window_productsminimum');
                        });
                </script>
			</li>
		</ul>
		<ul class="ul_mag clear bg1" style="height:30px;line-height: 30px">
			<li class="hg_padd line_mg" style="width:20%;line-height:19px;height:22px;">
				<input type="hidden" id="_idChangingCode" value="<?php if(isset($changing_code['_id'])) echo $changing_code['_id']; ?>" />
				Changing Code
			</li>
			<li class="hg_padd line_mg" style="width:75%;line-height:19px;height:22px;;">
				<input type="password" id="ChangingCodePassword" onchange="save_changing_code();" class="input_inner input_inner_w float_left bg1" name"data[product]" value="password" />
			</li>
		</ul>
	</div>
	<span class="title_block bo_ra2">
	</span>
</div>
<script type="text/javascript">
function after_choose_contacts(contact_id,contact_name){
    $("#AccountantId").val(contact_id);
    $("#Accountant").val(contact_name);
    save_accountant();
    $("#window_popup_contactsaccountant").data("kendoWindow").close();
}
function after_choose_products(product_id){
    $("#ProductId").val(product_id);
    var data = JSON.parse($("#after_choose_productsminimun_order"+product_id).val());
    $("#ProductName").val(data.code+' - '+data.name);
    save_product();
    $("#window_popup_productsminimun_order").data("kendoWindow").close();
}
function save_accountant(){
     $.ajax({
            url: '<?php echo URL ?>/settings/save_accountant',
            type: 'POST',
            data: {'accountant':$("#Accountant").val(),'accountant_id':$("#AccountantId").val(),'_id':$("#_idAccountant").val()},
            success: function(result){
                if(result!='ok')
                    alerts('Message',result);
            }
        });
}
function save_product(){
     $.ajax({
            url: '<?php echo URL ?>/settings/save_product',
            type: 'POST',
            data: {'product_name':$("#ProductName").val(),'product_id':$("#ProductId").val(),'_id':$("#_idProduct").val()},
            success: function(result){
                if(result!='ok')
                    alerts('Message',result);
            }
        });
}
function save_changing_code(){
     $.ajax({
            url: '<?php echo URL ?>/settings/save_changing_code',
            type: 'POST',
            data: {'password':$("#ChangingCodePassword").val(),'_id':$("#_idChangingCode").val()},
            success: function(result){
                if(result!='ok')
                    alerts('Message',result);
            }
        });
}
</script>