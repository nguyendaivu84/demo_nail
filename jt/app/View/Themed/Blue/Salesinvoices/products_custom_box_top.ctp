<?php if($this->Common->check_permission('communications_@_entry_@_add',$arr_permission)): ?>
<div class="float_left hbox_form" style="width:auto;">
    <a href="<?php echo URL.'/'.$controller;?>/email_pdf/">
    	<input class="btn_pur" id="emailexport_products" type="button" value="Email Invoice" style="width:99%;" />
    </a>
</div>
<?php endif; ?>
<div class="float_left hbox_form" style="width:auto; margin-left:5px;">
     <a href="<?php echo URL.'/'.$controller;?>/view_pdf/" target="_blank">
     	<input class="btn_pur" id="printexport_products" type="button" value="Export PDF" style="width:99%;" />
     </a>
</div>
