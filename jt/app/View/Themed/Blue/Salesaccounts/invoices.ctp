<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4><?php echo translate('Invoices relating to this Account'); ?></h4>
		</span>
		<?php if($this->Common->check_permission('salesinvoices_@_entry_@_add', $arr_permission)){
				if( isset($company_id) ){
					$link = URL.'/companies/salesinvoice_add/'.$company_id;
				}else{
					$link = URL.'/contacts/salesinvoice_add/'.$contact_id;
				}

		?>
		<a href="<?php echo $link; ?>" title="Add new invoice">
			<span class="icon_down_tl top_f"></span>
		</a>
		<?php } ?>
	</span>
	<p class="clear"></p>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd" style="width:1.5%"></li>
		<li class="hg_padd" style="width:3%"><?php echo translate('Ref no'); ?></li>
		<li class="hg_padd center_txt" style="width:6%"><?php echo translate('Date'); ?></li>
		<li class="hg_padd" style="width:4%"><?php echo translate('Status'); ?></li>
		<li class="hg_padd" style="width:4%"><?php echo translate('Term'); ?></li>
		<li class="hg_padd center_txt" style="width:6%"><?php echo translate('Due'); ?></li>
		<!-- <li class="hg_padd center_txt" style="width:4%"><?php echo translate('Day left'); ?></li> -->
		<li class="hg_padd center_txt" style="width:6%"><?php echo translate('Paid'); ?></li>
		<li class="hg_padd" style="width:8%"><?php echo translate('Our Rep'); ?></li>
		<li class="hg_padd" style="width:8%"><?php echo translate('Our CSR'); ?></li>
		<li class="hg_padd center_txt" style="width:10%"><?php echo translate('Tax'); ?></li>
		<li class="hg_padd center_txt" style="width:6%"><?php echo translate('Total'); ?></li>
		<li class="hg_padd right_txt" style="width:5%"><?php echo translate('Receipts'); ?></li>
		<li class="hg_padd right_txt" style="width:5%"><?php echo translate('Balance'); ?></li>
	</ul>
	<div class="container_same_category" style="height: auto;overflow: visible;">
		<?php
		$i = 1; $count = 0;
		foreach ($arr_invoice as $key => $value) {
		?>
		<ul class="ul_mag clear bg<?php echo $i; ?>" id="Company_Invoice_<?php echo $value['_id']; ?>">
			<li class="hg_padd center_txt" style="width:1.5%">
				<a href="<?php echo URL; ?>/salesinvoices/entry/<?php echo $value['_id']; ?>">
					<span class="icon_emp"></span>
				</a>
			</li>
			<li class="hg_padd center_txt" style="width:3%"><?php echo $value['code']; ?></li>
			<li class="hg_padd center_txt" style="width:6%"><?php if(isset($value['invoice_date']) && is_object($value['invoice_date']))echo $this->Common->format_date($value['invoice_date']->sec); ?></li>
			<li class="hg_padd" style="width:4%"><?php echo $value['invoice_status']; ?></li>
			<li class="hg_padd" style="width:4%"><?php echo $value['payment_terms']; ?></li>
			<li class="hg_padd center_txt" style="width:6%"><?php if(isset($value['payment_due_date']) && is_object($value['payment_due_date']))echo $this->Common->format_date($value['payment_due_date']->sec, false); ?></li>
			<!-- <li class="hg_padd center_txt" style="width:4%"></li> -->
			<li class="hg_padd center_txt" style="width:6%"><?php if(isset($value['paid_date']) && is_object($value['paid_date']))echo $this->Common->format_date($value['paid_date']->sec, false); ?></li>
			<li class="hg_padd" style="width:8%"><?php if(isset($value['our_rep']))echo $value['our_rep']; ?></li>
			<li class="hg_padd" style="width:8%"><?php if(isset($value['our_csr']))echo $value['our_csr']; ?></li>
			<li class="hg_padd right_txt" style="width:10%"><?php if(isset($value['tax']))echo $arr_tax_code[$value['tax']]; ?></li>
			<li class="hg_padd right_txt" style="width:6%"><?php if(isset($value['sum_amount']) && is_numeric($value['sum_amount']))echo number_format($value['sum_amount'], 2); ?></li>
			<li class="hg_padd right_txt" style="width:5%"><?php if(isset($value['total_receipt']) && is_numeric($value['total_receipt']))echo number_format($value['total_receipt'], 2); ?></li>
			<li class="hg_padd right_txt" style="width:5%"><?php if(isset($value['balance_invoiced']) && is_numeric($value['balance_invoiced']))echo number_format($value['balance_invoiced'], 2); ?></li>
		</ul>
		<?php $i = 3 - $i; $count += 1;
			}
			$count = 8 - $count;
			if( $count > 0 ){
				for ($j=0; $j < $count; $j++) { ?>
				<ul class="ul_mag clear bg<?php echo $i; ?>">
				</ul>
		  <?php $i = 3 - $i;
				}
			}
		?>
	</div>

	<span class="title_block bo_ra2">
		<span class="float_left bt_block">
			<?php echo translate('Click to view full details'); ?>
		</span>
	</span>
</div>