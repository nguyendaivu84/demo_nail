<?php echo $this->element('tab_option'); ?>
<script type="text/javascript">
	$(function(){
		$('#create_shipping').attr('onclick', 'check_condition_SI()');
	});
	function check_condition_SI()
	{
		$.ajax({
			url: '<?php echo URL; ?>/salesinvoices/create_shipping',
			type: 'GET',
			success: function(result){
				//console.log(result);
				var result = jQuery.parseJSON(result);
				if(result.status == 'error')
				{
					alerts('Message',result.mess);
				}
				else if(result.status='ok')
				{
					//console.log(result);
					window.location.replace(result.url);
				}
				else
					console.log(result);
			}
		});
		return false;
	}

</script>