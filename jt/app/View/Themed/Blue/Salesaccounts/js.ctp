<?php echo $this->element('js/permission_entry'); ?>
<script type="text/javascript">
$(function(){
	$("form :input", "#salesaccounts_form_auto_save").change(function() {
		salesaccounts_auto_save_entry(this);
	});

	salesaccounts_update_entry_header();
});

<?php if(!$this->Common->check_permission('communications_@_entry_@_add', $arr_permission)){?>
$(function(){
	$("#comms_create, #form_comms", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).remove();
	});
});
<?php }?>

<?php if(!$this->Common->check_permission('communications_@_entry_@_view', $arr_permission)){?>
$(function(){
	$(".container_same_category").find("a").each(function(){$(this).remove();});
});
<?php } ?>

<?php if(!$this->Common->check_permission('tasks_@_entry_@_view', $arr_permission)){?>
	$(".container_same_category").find("a").each(function(){$(this).remove();});
<?php } ?>

function salesaccounts_update_entry_header(){
	<?php
		if( $this->data['Salesaccount']['difference'] < 0 ){ ?>
			$("#h1_salesaccount_company_header").html("Warning: Credit limit exceeded.").attr("style", "color:red");
	<?php }else{ ?>
			$("#salesaccount_company_header").html( "<?php echo translate('Account'); ?> " + $("#SalesaccountAccountName").val());
	<?php
		}
	?>
	$("#salesaccount_status_header").html($("#SalesaccountStatus").val());
}

function salesaccounts_auto_save_entry(object){

	salesaccounts_update_entry_header();

	$("form :input", "#salesaccounts_form_auto_save").removeClass('error_input');

	$.ajax({
		url: '<?php echo URL; ?>/salesaccounts/auto_save',
		timeout: 15000,
		type:"post",
		data: $("form", "#salesaccounts_form_auto_save").serialize(),
		success: function(html){
			if(html != "ok"){

				if( $(object).attr("name") == "data[Salesaccount][credit_limit]" ){
					location.reload();
					return false;
				}

				if( $.trim(html) == "difference_ok" ){
					$("#SalesaccountDifference").attr("style", "");
					$("#h1_salesaccount_company_header").html( "<?php echo translate('Account '); ?> " + $("#SalesaccountAccountName").val()).attr("style", "");
				}else if( $.trim(html) == "difference_error" ){
					$("#h1_salesaccount_company_header").html("Warning: Credit limit exceeded.").attr("style", "color:red");
					$("#SalesaccountDifference").attr("style", "color:red");

				}else{
					alerts("Error: ", html);
				}
			}
			console.log(html); // view log when debug
		}
	});
}


</script>