 <div class="float_left" style ="width:100%;">
		<div class="float_left" style ="width:59.7%; margin-right:1.3%;">
				<div class="tab_1 full_width">
					<span class="title_block bo_ra1">
						<span class="fl_dent"><h4><?php echo translate('Receipts for this account'); ?></h4></span>
						<?php if( $this->Common->check_permission('receipts_@_entry_@_add', $arr_permission) ){
								$link = URL.'/salesaccounts/receipt_add/'.$salesaccount_id;

						?>
						<a title="Create a receipt" href="<?php echo $link; ?>">
							<span class="icon_down_tl top_f"></span>
						</a>
						<?php } ?>
					</span>
					<p class="clear"></p>
					<ul class="ul_mag clear bg3">
						<li class="hg_padd" style="width:1%"></li>
						<li class="hg_padd" style="width:5%"><?php echo translate('Ref no'); ?></li>
						<li class="hg_padd center_txt" style="width:9%"><?php echo translate('Date'); ?></li>
						<li class="hg_padd" style="width:10%"><?php echo translate('Paid by'); ?></li>
						<li class="hg_padd" style="width:10%"><?php echo translate('Reference</li>'); ?></li>
						<li class="hg_padd" style="width:19.8%"><?php echo translate('Notes'); ?></li>
						<li class="hg_padd right_txt" style="width:11%"><?php echo translate('Total receipt'); ?></li>
						<li class="hg_padd right_txt" style="width:11%"><?php echo translate('Allocated'); ?></li>
						<li class="hg_padd right_txt" style="width:11%"><?php echo translate('Unallocated'); ?></li>
					</ul>
				<!-- Receipt Content -->
				<div id="receipt_content" style="height:192px;overflow-y:auto;">
					<?php
						$i = 0;
						if(isset($sales_account['receipt'])&&!empty($sales_account['receipt'])){
							$total_receipt = 0;
							$total_unallocated = 0;
							foreach($sales_account['receipt'] as $receipt){
								$amount_received = $receipt['amount_received'];
								$unallocated = $receipt['unallocated'];
								$allocated = $amount_received - $unallocated;
								$total_receipt += $amount_received;
								$total_unallocated += $unallocated;
								$bg = ($i%2==0 ? 'bg1':'bg2');
					?>
					<ul class="ul_mag clear <?php echo $bg; ?>">
						<div id="keepping_record" style="display: none">
							<input type="hidden" id="total_receipt" value="<?php echo $total_receipt; ?>" />
							<input type="hidden" id="total_unallocated" value="<?php echo $total_unallocated; ?>" />
						</div>
						<li class="hg_padd" style="width:1%">
							<a href="<?php echo URL; ?>/receipts/entry/<?php echo $receipt['id']; ?>" >
								<span class="icon_emp"  title="View Receipt"></span>
							</a>
						</li>
						<li class="hg_padd" style="width:5%"><span id="code"><?php echo $receipt['code']; ?></span></li>
						<li class="hg_padd center_txt" style="width:9%"><span id="receipt_date"><?php echo $receipt['date']; ?></span></li>
						<li class="hg_padd" style="width:10%"><span id="paid_by"><?php echo $receipt['paid_by']; ?></span></li>
						<li class="hg_padd" style="width:10%"><span id="reference"><?php echo $receipt['reference']; ?></span></li>
						<li class="hg_padd" style="width:19.8%"><span id="notes"><?php echo $receipt['notes']; ?></span></li>
						<li class="hg_padd right_txt" style="width:11%"><span id="amount_received"><?php echo number_format($amount_received,2); ?></span></li>
						<li class="hg_padd right_txt" style="width:11%"><span id="allocated"><?php echo number_format($allocated,2); ?></span></li>
						<li class="hg_padd right_txt" style="width:11%"><span id="unallocated"><?php echo number_format($unallocated,2); ?></li>
					</ul>
					<?php
							$i++;
							}
						}
						if($i<9)
						{
							$k = $i;
							for($j=0;$j <8 - $i;$j++)
							{
								$bg = ($k%2==0 ? 'bg1':'bg2');
								echo '<ul class="ul_mag clear '.$bg.'"></ul>';
								$k++;
							}
						}
					?>
				</div>
				<!-- End Receipt Content -->
					<span class="title_block bo_ra2">
						<span class="float_left bt_block"><?php echo translate('Click to view full details'); ?></span>
						<span class="bt_block float_right no_bg" style="margin-right:15px;">
							<span class="float_left" style="margin-right: 43px;"><?php echo translate('Totals'); ?></span>
							<input id="total_unallocated" readonly="readonly" class="input_w2 float_right right_txt" type="text" value="<?php echo isset($total_unallocated) ? number_format($total_unallocated,2) : ''; ?>" style="margin-top: -1.6px;" >
							<input id="total_allocated" readonly="readonly" class="input_w2 float_right right_txt" type="text" value="<?php echo isset($total_receipt)&&isset($total_unallocated) ? number_format($total_receipt - $total_unallocated,2) : ''; ?>" style="margin-top: -1.6px;" >
							<input id="total_amount_receive" readonly="readonly" class="input_w2 float_right right_txt" type="text" value="<?php echo isset($total_receipt) ? number_format($total_receipt,2) : ''; ?>" style="margin-top: -1.6px;">
							<div class="clear"></div>
						</span>
					</span>

				</div><!--END Tab1 -->
			</div>
			<div class="float_left" style ="width:39%;">
				<div class="tab_1 full_width">
					<span class="title_block bo_ra1">
						<span class="fl_dent"><h4><?php echo translate('Receipt allocations'); ?></h4></span>
					</span>
					<p class="clear"></p>
					<ul class="ul_mag clear bg3">
						<li class="hg_padd" style="width:1.5%"></li>
						<li class="hg_padd" style="width:8%"><?php echo translate('Ref no'); ?></li>
						<li class="hg_padd center_txt" style="width:13%"><?php echo translate('Date'); ?></li>
						<li class="hg_padd" style="width:10%"><?php echo translate('Invoice'); ?></li>
						<li class="hg_padd" style="width:30.5%"><?php echo translate('Notes for customers'); ?></li>
						<li class="hg_padd center_txt" style="width:9.5%"><?php echo translate('Write off'); ?></li>
						<li class="hg_padd right_txt" style="width:15.5%"><?php echo translate('Amount'); ?></li>
						<li class="hg_padd bor_mt" style="width:2%"></li>
					</ul>
				<!-- Receipt allocation content 192 -->
				<div id="receipt_allocation_content" style="height:192px;overflow-y:auto;">
					<?php
						if(isset($sales_account['receipt_allocation'])&&!empty($sales_account['receipt_allocation']))
						{
							$i=0;
							$total_allocation = 0;
							foreach($sales_account['receipt_allocation'] as $values):
								foreach($values as $receipt_allocation):
								$total_allocation += $receipt_allocation['amount'];
								$bg = ($i%2==0 ? 'bg1':'bg2');
					?>
					<ul class="ul_mag clear <?php echo $bg; ?>">
						<li class="hg_padd" style="width:1.5%">
							<a href="<?php echo URL.'/receipts/entry/'.$receipt_allocation['receipt_id'] ?>">
								<span class="icon_emp" title="View Receipt"></span>
							</a>
						</li>
						<li class="hg_padd" style="width:8%"><?php echo $receipt_allocation['receipt_code']; ?></li>
						<li class="hg_padd center_txt" style="width:13%"><?php echo $receipt_allocation['date']; ?></li>
						<li class="hg_padd" style="width:10%"><?php echo $receipt_allocation['salesinvoice_code']; ?>
							<a <?php if($receipt_allocation['salesinvoice_id']=='') echo 'onclick="return false;"'; else echo 'href="'.URL.'/salesinvoices/entry/'.$receipt_allocation['salesinvoice_id'].'"'; ?>>
								<span class="icon_linkleft" title="View Sales invoice"></span>
							</a>
						</li>
						<li class="hg_padd" style="width:30.5%"><?php echo $receipt_allocation['note']; ?></li>
						<li class="hg_padd center_txt" style="width:9.5%"><input type="checkbox" <?php if($receipt_allocation['write_off']==1) echo 'checked'; ?> disabled /></li>
						<li class="hg_padd right_txt" style="width:15.5%"><?php echo number_format($receipt_allocation['amount'],2); ?></li>
						<li class="hg_padd bor_mt" style="width:2%">
						</li>
					</ul>
					<?php
								$i++;
								endforeach;
							endforeach;
						}
						if($i<9)
						{
							$k = $i;
							for($j = 0; $j< 8 - $i; $j++)
							{
								$bg = ($k%2==0 ? 'bg1' : 'bg2');
								echo '<ul class="ul_mag clear '.$bg.'"></ul>';
								$k++;
							}
						}
					?>
				</div>
				<!-- End Receipt allocation content -->
					<span class="title_block bo_ra2">
						<span class="float_left bt_block"><?php echo translate('Click to view full details'); ?></span>.
						<span class="bt_block float_right no_bg" style="margin-right:11px;">
							<span class="float_left" style="margin-right:10px"><?php echo translate('Totals receipt allocations'); ?></span>
								<input readonly="readonly" class="input_w2 float_right right_txt" id="total_allocation" type="text" value="<?php echo (isset($total_allocation) ? number_format($total_allocation,2) : ''); ?>" style="width:34.1%;height: 15px;color: #000; margin-right:1px; margin-top:-3.5px">
							<div class="clear"></div>
						</span>
					</span>
				</div><!--END Tab1 -->
			</div>

 </div>