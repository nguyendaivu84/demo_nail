<?php echo $this->Form->create('Salesaccount', array('id' => 'salesaccount_popup_form' . $key)); ?>
<div style="margin-right: 3%;width: 60%;position:relative">
	<?php
		// Ẩn nút submit này
		echo $this->Js->submit('Search', array(
			'id' => 'salesaccounts_popup_submit_subtton_' . $key,
			'style' => 'height:1px; width:1px;opacity:0.1',
			'success' => '$("#window_popup_salesaccounts' . $key . '").html(data);window_popup_extra("#window_popup_salesaccounts' . $key . '");'
		));
	?>
</div>

<div style="clear:both;height:6px"></div>

<div class="block_dent2 container_same_category" style="overflow: auto;overflow-x: hidden;max-width:1000px; margin: 0 auto; height:430px;" id="list_view_salesaccount">
	<table class="jt_tb" id="Form_add" style="font-size:12px; ">
		<thead style="position: fixed;" id="pagination_sort">
			<tr>
				<th style="width:299px"><?php echo translate('Name'); ?></th>
				<th style="width:77px;"><?php echo translate('Company'); ?></th>
				<th style="width:77px;"><?php echo translate('Contact'); ?></th>
				<th style="width:360px"><?php echo translate('Address'); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>&nspb;</th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			<?php
			$i = 0; $STT = 0;
			foreach ($arr_salesaccount as $value) {

				$i = 1 - $i; $STT += 1;
				?>
				<?php
					if (isset($value['company_id'])&&is_object($value['company_id']))
						$type = 'Company';
					else
						$type = 'Contact';
				?>

				<?php
					$name = '';
					// nếu SA là company
					if (isset($value['company_id']) && is_object($value['company_id'])){
						$arr_company = $model_company->select_one(array('_id' => $value['company_id']), array('_id', 'name', 'addresses_default_key', 'addresses'));
						if(isset($arr_company['name'])){
							$arr_company_tmp[(string)$value['company_id']] = $arr_company['name'];
							$name = $arr_company['name'];
							$address = $arr_company['addresses'][$arr_company['addresses_default_key']];
						}

					// nếu SA là contact
					}elseif (isset($value['contact_id'])){
						if (is_object($value['contact_id'])) {
							$arr_contact = $model_contact->select_one(array('_id' => $value['contact_id']), array('_id', 'first_name', 'last_name', 'addresses_default_key', 'addresses'));
							if(isset($arr_contact['first_name'])){
								$name = $arr_contact['first_name'].' '.$arr_contact['last_name'];
								$address = $arr_contact['addresses'][$arr_contact['addresses_default_key']];
							}
						}
					}
				?>

				<tr class="jt_line_<?php if ($i == 1) { ?>black<?php } else { ?>light<?php } ?>" onclick="after_choose_salesaccounts<?php if (substr($key, 0, 1) == '_') echo $key; ?>('<?php echo $type; ?>', '<?php echo $value['_id']; ?>', '<?php echo addslashes($name); ?>', '<?php echo $key; ?>');">
					<td align="left" style="width:300px">
						<?php echo $name; ?>
						<input type="hidden" id="after_choose_salesaccounts<?php echo $key; ?><?php echo $value['_id']; ?>" value="<?php echo htmlentities(json_encode($value)); ?>">
					</td>
					<td align="center" style="width:77px;"><?php if($type == 'Company')echo 'X'; ?>&nbsp;</td>
					<td align="center" style="width:77px;"><?php if($type == 'Contact')echo 'X'; ?>&nbsp;</td>
					<td style="width:360px;">&nbsp;
						<?php echo $address['address_1'] . ' ' . $address['address_2'] . ' ' . $address['address_3'] . (isset($address['town_city']) ? ', ' . $address['town_city'] : '') . (isset($address['province_state_name']) ? ', ' . $address['province_state_name'] : '') . (isset($address['country_name']) ? ', ' . $address['country_name'] : '' . (isset($address['zip_postcode']) ? ', ' . $address['zip_postcode'] : '')); ?>

						<input type="hidden" id="after_choose_companies<?php echo $key; ?><?php echo $value['_id']; ?>" value="<?php echo htmlentities(json_encode($value)); ?>">
					</td>
				</tr>
			<?php } ?>

			<?php if( $STT > 0 && $STT < 10 ){ // chỉ khi nào số lượng nhỏ hơn 10 mới add thêm mà thôi
				$loop_for = $limit - $STT;
				for ($j=0; $j < $loop_for; $j++) {
					$i = 1 - $i;
				  ?>
				<tr class="jt_line_<?php if ($i == 1) { ?>black<?php } else { ?>light<?php } ?>"><td>&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>
			<?php
				}
			} ?>
		</tbody>
	</table>

	<?php if( $STT == 0 ){ ?>
	<center style="margin-top:30px">(No data)</center>
	<?php } ?>
</div>
<?php echo $this->element('popup/pagination'); ?>
<?php echo $this->Form->end(); ?>