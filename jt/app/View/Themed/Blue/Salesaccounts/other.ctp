	<div class="clear_percent_7a float_left" style="width:28%">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Additional account details'); ?></h4></span>
				</span>
			</span>
			<div class="tab_2_inner">
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Company reg no'); ?></span>
					</p>
					<div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.company', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['company'])?$data['additional_account_details']['company']:'',
							'onchange' => 'save_default_additional(this, "company", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p></p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Tax no'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.tax_no', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['tax_no'])?$data['additional_account_details']['tax_no']:'',
							'onchange' => 'save_default_additional(this, "tax_no", "'.isset($arr_additional['_id']).'" )'
						)); ?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Year established'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.year_established', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['year_established'])?$data['additional_account_details']['year_established']:'',
							'onchange' => 'save_default_additional(this, "year_established", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Bank name'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.bank_name', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_name'])?$data['additional_account_details']['bank_name']:'',
							'onchange' => 'save_default_additional(this, "bank_name", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Bank address'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.bank_address', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_address'])?$data['additional_account_details']['bank_address']:'',
							'onchange' => 'save_default_additional(this, "bank_address", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Bank no'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.bank_no', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_no'])?$data['additional_account_details']['bank_no']:'',
							'onchange' => 'save_default_additional(this, "bank_no", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Bank transit no'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.bank_transit_no', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_transit_no'])?$data['additional_account_details']['bank_transit_no']:'',
							'onchange' => 'save_default_additional(this, "bank_transit_no", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Bank account no'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.bank_account', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_account'])?$data['additional_account_details']['bank_account']:'',
							'onchange' => 'save_default_additional(this, "bank_account", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Bank sort code'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.bank_sort_code', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_sort_code'])?$data['additional_account_details']['bank_sort_code']:'',
							'onchange' => 'save_default_additional(this, "bank_sort_code", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2"style="height: 30px;"><?php echo translate('Bank IBAN no'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.bank_iban', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['additional_account_details']['bank_iban'])?$data['additional_account_details']['bank_iban']:'',
							'onchange' => 'save_default_additional(this, "bank_iban", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>

				<p></p>
				<p class="clear"></p>
			</div>
			<span class="title_block bo_ra2"></span>
		</div>
	</div>

	<!--panner 2 -->
	<div class="clear_percent_7a float_left" style="margin-left: 1%; width: 20%">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Trade reference 1'); ?></h4></span>
				</span>
			</span>
			<div class="tab_2_inner">
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Name'); ?></span>
					</p>
					<div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.name', array(
							'class' => 'input_1 float_left',
							'rel'=>1,
							'value' => isset($data['trade_reference'][1]['name'])?$data['trade_reference'][1]['name']:'',
							'onchange' => 'save_default(this, "name", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p></p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Contact'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.contact', array(
							'class' => 'input_1 float_left',
							'rel'=>1,
							'value' => isset($data['trade_reference'][1]['contact'])?$data['trade_reference'][1]['contact']:'',
							'onchange' => 'save_default(this, "contact", "'.isset($arr_other['_id']).'" )'
						)); ?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Phone'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.phone', array(
								'class' => 'input_1 float_left',
								'rel'=>1,
								'value' => isset($data['trade_reference'][1]['phone'])?$data['trade_reference'][1]['phone']:'',
								'onchange' => 'save_default(this, "phone", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Fax'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.Fax', array(
							'class' => 'input_1 float_left',
							'rel'=>1,
							'value' => isset($data['trade_reference'][1]['fax'])?$data['trade_reference'][1]['fax']:'',
							'onchange' => 'save_default(this, "fax", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 " style="height:150px"><?php echo translate('Address'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.address', array(
							'class' => ' float_left',
							'type' => 'textarea',
							'style' => 'margin-top: 6px; border-top: 1px solid #fff; border-bottom: 1px solid #ccc; border-left:0; border-right:0; height: 100px; color: #545353; font: normal 11px arial, verdana, sans-serif;',
							'rel'=>1,
							'value' => isset($data['trade_reference'][1]['address'])?$data['trade_reference'][1]['address']:'',
							'onchange' => 'save_default(this, "address", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>

				<p class="clear"></p>
			</div>
			<span class="title_block bo_ra2"></span>
		</div>
	</div>

	<!-- panned 3 -->
	<div class="clear_percent_7a float_left" style="margin-left: 1%; width: 20%">
	 <div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Trade reference 2'); ?></h4></span>
				</span>
			</span>
			<div class="tab_2_inner">
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Name'); ?></span>
					</p>
					<div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.name', array(
							'class' => 'input_1 float_left',
							'rel'=>2,
							'value' => isset($data['trade_reference'][2]['name'])?$data['trade_reference'][2]['name']:'',
							'onchange' => 'save_default(this, "name", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p></p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Contact'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.contact', array(
							'class' => 'input_1 float_left',
							'rel'=>2,
							'value' => isset($data['trade_reference'][2]['contact'])?$data['trade_reference'][2]['contact']:'',
							'onchange' => 'save_default(this, "contact", "'.isset($arr_other['_id']).'" )'
						)); ?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Phone'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.phone', array(
								'class' => 'input_1 float_left',
								'rel'=>2,
								'value' => isset($data['trade_reference'][2]['phone'])?$data['trade_reference'][2]['phone']:'',
								'onchange' => 'save_default(this, "phone", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Fax'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.Fax', array(
							'class' => 'input_1 float_left',
							'rel'=>2,
							'value' => isset($data['trade_reference'][2]['fax'])?$data['trade_reference'][2]['fax']:'',
							'onchange' => 'save_default(this, "fax", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 " style="height:150px"><?php echo translate('Address'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.address', array(
							'class' => 'float_left' ,
							'type' => 'textarea',
							'style' => 'margin-top: 6px; border-top: 1px solid #fff; border-bottom: 1px solid #ccc; border-left:0; border-right:0; height: 100px; color: #545353; font: normal 11px arial, verdana, sans-serif;',
							'rel'=>2,
							'value' => isset($data['trade_reference'][2]['address'])?$data['trade_reference'][2]['address']:'',
							'onchange' => 'save_default(this, "address", "'.isset($arr_other['_id']).'" )'
						));?>
					</div>
				<p class="clear"></p>
			</div>
			<span class="title_block bo_ra2"></span>
		</div>
	</div>
	<!--panne 4-->
 <div class="clear_percent_7a float_left" style="margin-left: 1%; width: 29%">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Account application'); ?></h4></span>
					<span class="pdf">
						<a href="<?php echo URL ;?>/salesaccounts/print_application_pdf/<?php echo $salesaccount_id; ?>">
							<input  style="margin-top:-16px;margin-left:150px; width: 50%;" class="btn_pur" type="button" value="Print application">
						</a>
					</span>
				</span>
			</span>
			<div class="tab_2_inner">
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Applicant name'); ?></span>
					</p>
					<div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->hidden('Salesaccount.application_id'); ?>
						<?php echo $this->Form->input('Salesaccount.application_name', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['account_application']['application_name'])?$data['account_application']['application_name']:'',
							'onchange' => 'save_default_account(this, "application_name", "'.isset($arr_additional['_id']).'" )'
						));?>
						<span class="icon_down_new float_right" id="click_open_window_contacts_123"></span>
						<script type="text/javascript">
							$(function(){
								// kiểm tra xem đã chọn company chưa
								salesaccounts_init_popup_contacts_123();
							});

							function salesaccounts_init_popup_contacts_123( force_re_install ){
								var parameter_get = "?company_id="+$("#SalesaccountId").val()+"&company_name="+$("#SalesaccountName").val();
								window_popup("contacts", "Specify contact", "_123", "", parameter_get);
							}
							function after_choose_contacts_123(application_id, application_name){
								$("#link_to_contacts").attr("href", "<?php echo URL; ?>/contacts/entry/" + application_id);
								$("#SalesaccountApplicationId").val(application_id);
								$("#SalesaccountApplicationName").val(application_name);
								$("#window_popup_contacts_123").data("kendoWindow").close();
								save_default_account('#SalesaccountApplicationName',"application_name");
								return false;
							}
						</script>
				</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Position'); ?></span>
					</p>
					<div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.position', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['account_application']['position'])?$data['account_application']['position']:'',
							'onchange' => 'save_default_account(this, "position", "'.isset($arr_additional['_id']).'" )'
						)); ?>
						 <script type="text/javascript">
							$(function () {
								$("#SalesaccountPosition").combobox(<?php echo json_encode($position); ?>);
							});
						</script>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Application date'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<?php echo $this->Form->input('Salesaccount.application_date', array(
							'class' => 'JtSelectDate input_1 float_left',
							'value' => isset($data['account_application']['application_date'])?$data['account_application']['application_date']:'',
							'onchange' => 'save_default_account(this, "application_date", "'.isset($arr_additional['_id']).'" )'
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.markup_rate', array(
							'class' => 'input_1 float_left',
							'value' => 'Account approval',
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Approved'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						<span class="float_left jtchecktype" style=" width:98%;margin-left:0%;">
							<label class="m_check2">
								<?php echo $this->Form->input('Salesaccount.approved', array(
									'class' => 'float_left ',
									'type' => 'checkbox',
									'value' => (isset($data['account_application']['approved'])? $data['account_application']['approved'] : 0),
									'onchange' => 'save_default_account(this, "approved", "'.isset($arr_additional['_id']).'")'
								));?>
								<span style=" margin-left:3%;"></span>
							</label>
							<span class="fl_dent" for="include_signature">&nbsp;&nbsp;</span>
						</span>
					</div>

				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Approved by'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php echo $this->Form->input('Salesaccount.approved_by', array(
							'class' => 'input_1 float_left',
							'value' => isset($data['account_application']['approved_by'])?$data['account_application']['approved_by']:'',
							'readonly' => true,
						));?>
					</div>
				<p></p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 "><?php echo translate('Approved date'); ?></span>
					</p><div class="width_in3a float_left indent_input_tp">
						 <?php
						 echo $this->Form->input('Salesaccount.approved_date', array(
							'class' => 'input_1 float_left',
						   'value' => isset($data['account_application']['approved_date'])?$data['account_application']['approved_date']:'',
							'readonly' => true
						));?>
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 " style="height:31px"></span>
					</p><div class="width_in3a float_left indent_input_tp">
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 " style="height:31px"></span>
					</p><div class="width_in3a float_left indent_input_tp">
					</div>
				<p class="clear">
					<span class="label_1 float_left minw_lab2 " style="height:17px"></span>
					</p><div class="width_in3a float_left indent_input_tp">
					</div>
				<p></p>
				 <p class="clear"></p>
			</div>
			<span class="title_block bo_ra2"></span>
		</div>
	</div>
<script type="text/javascript">
$(function(){
	$("#SalesaccountApproved").click(function(){
		if($(this).is(':checked')){
			$("#SalesaccountApprovedBy").val("<?php echo $_SESSION['arr_user']['contact_name'];?>");
			$("#SalesaccountApprovedDate").val("<?php echo date('M d, Y',time()); ?>");
		}else{
			$("#SalesaccountApprovedBy").val('');
			$("#SalesaccountApprovedDate").val("");
		}
	});
})

function save_default(object, field){
	$.ajax({
		url: '<?php echo URL; ?>/salesaccounts/other_auto_save/'  + $("#SalesaccountId").val() ,
		timeout: 15000,
		type:"post",
		data: { "field": field, value: $(object).val(), key: $(object).attr('rel') },
		success: function(html){
			if(html != "ok"){
				alerts("Error: ", html);
			}
			console.log(html); // view log when debug
		}
	});
}
<?php if($this->Common->check_permission('salesaccounts_@_other_tab_@_edit', $arr_permission)){ ?>
function save_default_additional(object, field){
	$.ajax({
		url:'<?php echo URL;?>/salesaccounts/other_additional_auto_save/' + $("#SalesaccountId").val(),
		timeout: 15000,
		type: "POST",
		data:{"field": field, value: $(object).val()},
		success: function(html){
			if(html != "ok"){
				alerts("Error: ", html);
			}
			console.log(html);
		}
	});
}
<?php }else{ ?>
	$(function(){
	$(":input", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).attr("disabled", true);
		$(this).css("background-color", "transparent");
	});
	$(".icon_down_new , .combobox_button", "#<?php echo $controller; ?>_sub_content").each(function() {
		$(this).remove();
	});
	$(".pdf").find("a").each(function(){$(this).remove();});
})
<?php } ?>
function save_default_account(object, field){
	$.ajax({
		url:'<?php echo URL;?>/salesaccounts/other_account_auto_save/' + $("#SalesaccountId").val(),
		timeout: 15000,
		type: "POST",
		data:{"field": field, value: $(object).val()},
		success: function(html){
			if(html != "ok"){
				alerts("Error: ", html);
			}
			console.log(html);
		}
	});
}
</script>
