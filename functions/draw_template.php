<?php
function draw_user_design($v_dir_templates,$db,$v_package_id,$p_user_id){
    add_class("cls_tb_design_save");
    $cls_tb_design_save = new cls_tb_design_save($db);
    $arr_design_item = array();
    $tpl_load_design = new Template('dsp_load_design.tpl',$v_dir_templates);
    $tpl_load_design->set('URL',URL);
    $arr_design_item = draw_save_item($v_dir_templates,$db,$v_package_id,$p_user_id);
    $dsp_item = Template::merge($arr_design_item);
    $tpl_load_design->set('SAVE_DESIGN_ITEM',$dsp_item);
    echo $tpl_load_design->output();
}
function draw_save_item($v_dir_templates,$db,$v_package_id,$p_user_id){
    add_class("cls_tb_design_save");
    $cls_tb_design_save = new cls_tb_design_save($db);
    $arr_design_item = array();
    $arr_select = $cls_tb_design_save->select(array("package_id"=>$v_package_id,"cart_item_id"=>0,'user_id'=>$p_user_id));
    foreach($arr_select as $arr){
        $tpl_load_design_item = new Template('dsp_load_design_item.tpl',$v_dir_templates);
        $tpl_load_design_item->set('DESIGN_NAME',$arr['design_name']);
        $tpl_load_design_item->set('DESIGN_DATE',date('d-M-Y',$arr['date_save']->sec));
        $tpl_load_design_item->set('SAVE_ID',$arr['design_save_id']);
        $tpl_load_design_item->set('PACKAGE_ID',$v_package_id);
        $arr_design_item [] = $tpl_load_design_item;
    }
    return $arr_design_item;
}
function delete_design($v_dir_templates,$db,$v_save_id,$p_user_id){
    add_class("cls_tb_design_save");
    $cls_tb_design_save = new cls_tb_design_save($db);

    add_class("cls_tb_design_temp");
    $cls_tb_design_temp = new cls_tb_design_temp($db);
    $cls_tb_design_save->delete(array("design_save_id"=>$v_save_id,'user_id'=>$p_user_id));
    $cls_tb_design_temp->delete(array("saved_id"=>$v_save_id));
}
function get_max_quantity($v_package_id,$v_product_id,$v_saved_id,$db){
    $v_quantity = 0;
    $v_orgin_quantity = 0;
    $v_design_quantity = 0;
    if(isset($_SESSION['ss_package']) && $v_saved_id==0){
        $arr_content = unserialize($_SESSION['ss_package']);
        for($j=0;$j<count($arr_content);$j++){
            if($arr_content[$j]['package_id']==$v_package_id && $v_product_id == $arr_content[$j]['product_id']){
                $arr_design = $arr_content[$j]['is_designed'];
                $v_orgin_quantity = $arr_content[$j]['product_quantity'];
                $v_design_quantity = 0;
                for($k=0;$k<count($arr_design);$k++){
                    if(isset($arr_design[$k]['product_id']) && $arr_design[$k]['product_id']==$v_product_id){
                        $v_design_quantity += $arr_design[$k]['product_design_quantity'];
                    }
                }
            }
        }
        $v_quantity = $v_orgin_quantity - $v_design_quantity;
    }else if($v_saved_id>0){
        add_class("cls_tb_design_temp");
        $cls_tb = new cls_tb_design_temp($db);
        $arr_where = array("package_id"=>(int)$v_package_id,"product_id"=>(int)$v_product_id,"saved_id"=>(int)$v_saved_id);

        $arr_select = $cls_tb->select($arr_where);
        foreach($arr_select as $arr){
            $v_design_quantity += $arr['product_design_quantity'];
            $v_orgin_quantity = $arr['total_product_origin_quantity'];
        }
        $v_quantity = $v_orgin_quantity - $v_design_quantity;
    }
    return $v_quantity;
}
?>