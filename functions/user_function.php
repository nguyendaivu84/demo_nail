<?php
/**
 *	function create temporary or resume current function
 *	@return array
 */
function create_user(){
    global $_SESSION;
    $v_result = isset($_SESSION['ss_user']);
    if($v_result){
        $arr_user = unserialize($_SESSION['ss_user']);
    }
    if(!isset($arr_user) || !is_array($arr_user)){
        $arr_user = array(
            'user_id' =>0
            ,'user_name'=>''
            ,'user_right'=>''
            ,'user_sex'=>0
            ,'user_email'=>''
            ,'user_login'=>0
            ,'user_type'=>0 //0: Admin 1: Head Office 2: Branch 3: Orther
            ,'user_status'=>0
            ,'mongo_id'=>''
            ,'contact_id'=>0
            ,'company_id'=>0
            ,'contact_name'=>''
            ,'company_name'=>''
            ,'location'=>array()
            ,'location_default'=>0
            ,'user_rule'=>array()
            ,'user_ip'=>get_real_ip_address()
        );
    }
    $_SESSION['ss_user'] = serialize($arr_user);

    return $arr_user;
}
function get_unserialize_user($p_filter)
{
    $arr = unserialize($_SESSION['ss_user']);
    if(!isset($arr[$p_filter])) return "";
    return $arr[$p_filter];
}
function is_admin_by_user($p_user_name){
    $arr_admin = array('admin');
    return in_array($p_user_name, $arr_admin);
}
function config_user($arr_temp_user){
    global $db;
    $arr_user['user_id'] = (int)$arr_temp_user['user_id'];
    $arr_user['user_name'] = $arr_temp_user['user_name'];
    $arr_user['user_status'] = $arr_temp_user['user_status'];
    $arr_user['user_type'] = $arr_temp_user['user_type'];
    $arr_user['mongo_id'] = $arr_temp_user['mongo_id'];
    $arr_user['contact_id'] = $arr_temp_user['contact_id'];
    $arr_user['contact_name'] = $arr_temp_user['contact_name'];
    $arr_user['company_id'] = $arr_temp_user['company_id'];
    $arr_user['company_code'] = $arr_temp_user['company_code'];
    $arr_user['user_rule'] = $arr_temp_user['user_rule'];
    $arr_user['location_banner'] = $arr_temp_user['location_banner'];
    $arr_user['user_login'] = 1;
    $arr_user['location_default']= $arr_temp_user['location_default'];
    $arr_user['break'] = isset($arr_temp_user['location_default']) ? $arr_temp_user['break'] : 0;
    $v_tmp_location_approve = $arr_temp_user['user_location_approve'];
    $v_tmp_location_allocate = $arr_temp_user['user_location_allocate'];
    add_class('cls_tb_company');
    add_class('cls_tb_location');
    $cls_tb_company = new cls_tb_company($db);
    $cls_tb_location = new cls_tb_location($db);
    $v_list_modules = $cls_tb_company->select_scalar('modules',array('company_id'=>(int) $arr_user['company_id']));
    $v_confirm_allocate = 0;

    if(strpos($v_list_modules,'require_allocate')!== false)
        $v_confirm_allocate =1;

    $arr_user['confirm_allocate'] = $v_confirm_allocate;
    $arr_location = array();
    $arr_location[0] = $arr_user['location_default'];
    if($v_tmp_location_approve!=''){
        $j=1;
        $arr_tmp = explode(',', $v_tmp_location_approve);
        for($i=0; $i<count($arr_tmp); $i++){
            $v_one = (int) $arr_tmp[$i];
            if($v_one>0 && $v_one!=$arr_user['location_default']){
                $arr_location[$j++] = $v_one;
            }
        }
    }

    $arr_user['location_approve']= $arr_location;

    $arr_location = array();
    //$arr_location[0] = $v_tmp_location_id;
    if($v_tmp_location_allocate!=''){
        $j=0;

        if(strpos($v_tmp_location_allocate.',',$arr_user['location_default'].',')!==false){
            $arr_location[$j] = $arr_user['location_default'];
            $j++;
        }else{
            $arr_user['confirm_allocate'] = 1;
        }

        $arr_tmp = explode(',', $v_tmp_location_allocate);
        for($i=0; $i<count($arr_tmp); $i++){
            $v_one = (int) $arr_tmp[$i];
            if($v_one>0 && $v_one!=$arr_user['location_default']){
                $arr_location[$j++] = $v_one;
            }
        }

    }
    else{
        $arr_location[0] = $arr_user['location_default'];
    }
    $arr_user['location']= $arr_location;
    /*
    $arr_temp = array();
    $arr_where_clause = array('location_id'=>array('$in'=>$arr_location), 'status'=>array('$in'=>array(1,3)));
    $arr_location = $cls_tb_location->select($arr_where_clause);
    foreach($arr_location as $a){
        $arr_temp[] = $a;
    }
    $arr_user['location'] = $arr_temp;
    */
    //print_r($arr_user['location']);
    //die();

    //$arr_user['location']= $arr_location;

    $arr_user['user_ip'] = get_real_ip_address();
    //Load user themes
    $v_theme_file = ROOT_DIR.DS.'resources'.DS.THEMES_SAVED;
    $arr_user_theme = array();
    if(file_exists($v_theme_file)){
        $fp = fopen($v_theme_file, 'r');
        $v_content = fread($fp, filesize($v_theme_file));
        fclose($fp);
        $arr_content = unserialize($v_content);
        if(isset($arr_content[$arr_user['user_name']])) $arr_user_theme = $arr_content[$arr_user['user_name']];
    }
    $arr_user['user_theme'] = $arr_user_theme;
    $arr_user[' break'] = 1;
    $_SESSION['ss_user'] = serialize($arr_user);
}
?>