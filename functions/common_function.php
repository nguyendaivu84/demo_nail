<?php
function images_resize_by_height_width($new_height,$source_file,$dest_file, $delete_source = false){
    list($width, $height, $type, $attr) = @getimagesize($source_file);
    echo $width."/";
    echo $height;
    echo $new_height;
    if (($width*$height)==0) return false;
    if ($new_height==0) return false;
    $v_percent = $new_height / $height;
    $new_width = $width * $v_percent;
    settype($new_width, 'int');
    $im_source = false;
    switch($type){
        case 1;//Gif
            $im_source = @imagecreatefromgif($source_file);
            break;
        case 2;//Jpg
            $im_source = @imagecreatefromjpeg($source_file);
            break;
        case 3;//Png
            $im_source = @imagecreatefrompng($source_file);
            break;
        default ;
            $im_source = @imagecreatefromjpeg($source_file);
            break;
    };
    $im_dest = @imagecreatetruecolor(135,135);
    if ($im_dest) $v_ok = true;
    else $v_ok = false;
    $background = @imagecolorallocate($im_dest, 255, 255, 255);
    @imagefill($im_dest,0,0,$background);
    imagecopyresized($im_dest, $im_source,0,0,0,0, 135, 135, $width, $height);
    if($delete_source) @unlink($source_file);
    @imagejpeg($im_dest,$dest_file,100);
    @imagedestroy($im_dest);
    @imagedestroy($im_source);
    return $v_ok;
}
function addslash(){
    //global $_GET, $_REQUEST, $_POST;
    if (!get_magic_quotes_gpc() ){
        foreach($_GET as $key=>$val){
            if(is_array($val)){
                $_GET[$key] = array_map ('addslashes', $_GET[$key]) ;
            }else{
                $_GET[$key] = addslashes($val);
            }
        }

        foreach($_POST as $key=>$val){
            if(is_array($val)){
                $_POST[$key] = array_map ('addslashes', $_POST[$key]) ;
            }else{
                $_POST[$key] = addslashes($val);
            }
        }
        foreach($_COOKIE as $key=>$val){
            if(is_array($val)){
                $_COOKIE[$key] = array_map ('addslashes', $_COOKIE[$key]) ;
            }else{
                $_COOKIE[$key] = addslashes($val);
            }
        }
        foreach($_REQUEST as $key=>$val){
            if(is_array($val)){
                $_REQUEST[$key] = array_map ('addslashes', $_REQUEST[$key]) ;
            }else{
                $_REQUEST[$key] = addslashes($val);
            }
        }
    }
}
function draw_option_type($arr_type, $p_selected, $p_start_index=0){
    $v_ret='';
    for($i=$p_start_index;$i<count($arr_type);$i++){
        $v_ret.='<option value="'.$i.'"'.($i==$p_selected?' selected="selected"':'').'">'.$arr_type[$i].'</option>';
    }
    return $v_ret;
}
function seo_friendly_url($string){
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    return strtolower(trim($string, '-'));
}
function draw_option_sort_key(array $arr_fields, $p_selected_field='', $p_asc = 1){
    $v_ret = '';
    for($i=0; $i<count($arr_fields); $i++){
        $v_key_value = $arr_fields[$i];
        $v_key_text = ucwords(implode(' ',explode('_', $v_key_value)));
        $v_ret .= '<option value="'.$v_key_value.'"'.($v_key_value==$p_selected_field?' selected="selected"':'').'>'.$v_key_text.'</option>';
    }
    if($v_ret!=''){
        $v_ret = '<select id="txt_sort_by" name="txt_sort_by"><option value="" selected="selected">--- Select One ---</option>'.$v_ret.'</select>&nbsp;&nbsp;';
        $v_ret .= '<label><input type="radio" name="txt_sort_type" value="1"'.($p_asc==1?' checked="checked"':'').' /> Ascending</label> / ';
        $v_ret .= '<label><input type="radio" name="txt_sort_type" value="-1"'.($p_asc!=1?' checked="checked"':'').' /> Descending</label>';
    }
    return $v_ret;
}
function write($p_str = '', $p_file = 'hung.txt', $p_write='w', $p_dir = ROOT_DIR){
    $v_file = $p_dir.DIRECTORY_SEPARATOR.$p_file;
    $fp = fopen($v_file, $p_write);
    fwrite($fp, $p_str, strlen($p_str));
    fclose($fp);
}
function get_value_uri($uri, $key, $default=''){
    $find = $key.'=';
    $v_pos = strpos($uri, $find);
    $v_p = $v_pos;
    if($v_pos!==false){
        $v_pos += strlen($find);
        $v_tmp = substr($uri,$v_pos);
        $v_pos = strpos($v_tmp,'&');
        if($v_pos>0){
            return substr($v_tmp,0,$v_pos);
        }else{
            return $v_tmp;
        }
    }else return $default;
}
function redir($p_url){
    //if(ob_get_length()>0){
    @ob_end_clean();
    @ob_start();
    //}
    if(isset($p_url) && $p_url!='')
        header("location:$p_url");
    else
        header("location:".URL);
}
function get_real_ip_address(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
function remove_invalid_char($p_str)
{
    $p_str = trim($p_str);
    $p_str = str_replace(" ","_",$p_str);
    $p_str = str_replace('"'," &quot; ",$p_str);
    $p_str = str_replace("'"," &quot; ",$p_str);
    $tmp = "";
    for ($i=0;$i<strlen($p_str);$i++){
        $v_code = ord(substr($p_str,$i,1));
        if (($v_code>=48 && $v_code<=57) || ($v_code>=65 && $v_code<=90) || ($v_code>=97 && $v_code<=122) || ($v_code==95)){
            $tmp.=substr($p_str,$i,1);
        }
    }
    while (strpos($tmp,"__")!==false){
        $tmp = str_replace("__","_",$tmp);
    }
    return $tmp;
}
function get_select_one($array_expression,$p_display)
{
    foreach($array_expression as  $key => $value ){
        if($key==$p_display) return $value;
    }
    return "";
}
function get_select_id_one($array_expression,$p_value=1)
{

    foreach($array_expression as  $key => $value ){
        if($p_value==1) return $key;
        else return $value;
    }
    return "0";
}
function get_show_all_array($arr=array())
{
    $v_array = "";

    if(is_array($arr))
    {
        foreach($arr as  $key => $value ){
            $v_array .= $value .'<br>';
        }
    }
    return $v_array;
}
function images_resize_by_width($new_width,$source_file,$dest_file, $delete_source = false){
    list($width, $height, $type, $attr) = @getimagesize($source_file);
    if (($width*$height)==0) return false;
    if ($new_width==0) return false;
    $v_percent = $new_width / $width;
    $new_height = $height * $v_percent;
    settype($new_height, 'int');
    $im_source = false;
    switch($type){
        case 1;//Gif
            $im_source = @imagecreatefromgif($source_file);
            break;
        case 2;//Jpg
            $im_source = @imagecreatefromjpeg($source_file);
            break;
        case 3;//Png
            $im_source = @imagecreatefrompng($source_file);
            break;
        default ;
            $im_source = @imagecreatefromjpeg($source_file);
            break;
    }
    //$im_dest = @imagecreatetruecolor($new_width,$new_height);
    //$background = @imagecolorallocate($im_dest, 255, 255, 255);
    //@imagefill($im_dest,0,0,$background);
    //@imagecopyresampled ($im_dest,$im_source, 0,0,0,0,$new_width,$new_height,$width,$height);
    $im_dest = @imagecreatetruecolor($new_width,$new_height);
    if ($im_dest) $v_ok = true;
    else $v_ok = false;

    $background = @imagecolorallocate($im_dest, 255, 255, 255);
    @imagefill($im_dest,0,0,$background);
    imagecopyresized($im_dest, $im_source,0,0,0,0, $new_width, $new_height, $width, $height);
    if($delete_source) @unlink($source_file);
    @imagejpeg($im_dest,$dest_file,100);
    @imagedestroy($im_dest);
    @imagedestroy($im_source);
    return $v_ok;
}
function images_resize_by_height($new_height,$source_file,$dest_file, $delete_source = false){
    list($width, $height, $type, $attr) = @getimagesize($source_file);
    if (($width*$height)==0) return false;
    if ($new_height==0) return false;
    $v_percent = $new_height / $height;
    $new_width = $width * $v_percent;
    settype($new_width, 'int');
    $im_source = false;
    switch($type){
        case 1;//Gif
            $im_source = @imagecreatefromgif($source_file);
            break;
        case 2;//Jpg
            $im_source = @imagecreatefromjpeg($source_file);
            break;
        case 3;//Png
            $im_source = @imagecreatefrompng($source_file);
            break;
        default ;
            $im_source = @imagecreatefromjpeg($source_file);
            break;
    }
    $im_dest = @imagecreatetruecolor($new_width,$new_height);
    if ($im_dest) $v_ok = true;
    else $v_ok = false;

    $background = @imagecolorallocate($im_dest, 255, 255, 255);
    @imagefill($im_dest,0,0,$background);
    imagecopyresized($im_dest, $im_source,0,0,0,0, $new_width, $new_height, $width, $height);
    if($delete_source) @unlink($source_file);
    @imagejpeg($im_dest,$dest_file,100);
    @imagedestroy($im_dest);
    @imagedestroy($im_source);
    return $v_ok;
}
function news_pagination($page_count, $cur_page, $link, $limit=4, $end=".html", $split=""){
    $v_url = $link;
    if(substr($v_url,strlen($v_url),1)!='/') $v_url.='/';
    $current_range = array(($cur_page-2 < 1 ? 1 : $cur_page-2), ($cur_page+2 > $page_count ? $page_count : $cur_page+2));

    $first_page = $cur_page > 3 ? '<li> <a  title="Page 1" href="'.$v_url.'page1'.$end.'">1</a></li> '.($cur_page < $limit+1 ? "{$split} " : ' <a href="#">...</a> ') : null;
    $last_page = $cur_page < $page_count-2 ? ($cur_page > $page_count-$limit ? "{$split} " : ' <li><a href="#">...</a></a> </li> ').'<li> <a title="Page '.$page_count.'" href="'.$v_url."page{$page_count}".$end.'">'.(($page_count<10)?"{$page_count}":$page_count).'</a></li>' : null;

    $previous_page = $cur_page > 1 ? '<li class="previous-off"> <a title="Page '.($cur_page-1).'" href="'.$v_url.'page'.($cur_page-1).$end.'">« Previous</a>' : null .'</li>' ;
    $next_page = $cur_page < $page_count ? ' <a title="Page '.($cur_page+1).'" href="'.$v_url.'page'.($cur_page+1).$end.'"> Next » </a>' : null;

    // Display pages that are in range
    for ($x=$current_range[0];$x <= $current_range[1]; ++$x){
        if($x==$cur_page)
            $pages[] = '<li class="active">'.$x.'</li>';
        else
            $pages[] = '<li> <a title="Page '.($x).'" href="'.$v_url.'page'.$x.$end.'">'.$x.'</a></li>';
    }

    if ($page_count > 1)
        return '<ul id="pagination"> '.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul>';
    else
        return "";
}
function pagination2($page_count, $cur_page, $link, $limit=4, $end="", $split=""){
    $v_url = $link;
    if(substr($v_url,strlen($v_url),1)!='/') $v_url.='';
    //$v_url.='page';
    $current_range = array(($cur_page-2 < 1 ? 1 : $cur_page-2), ($cur_page+2 > $page_count ? $page_count : $cur_page+2));

    // First and Last pages
    $first_page = $cur_page > 3 ? '<li><a title="Page 1" href="'.$v_url.$end.'">1</a></li>'.($cur_page < $limit+1 ? "{$split} " : '<li><a data-page="'.($cur_page-3).'" title="..." href="'.$v_url.($cur_page-3).$end.'">...</a></li>') : null; //class="current"
    $last_page = $cur_page < $page_count-2 ? ($cur_page > $page_count-$limit ? "{$split} " : ' <li><a title="..." data-page="'.($cur_page+3).'" href="'.$v_url.($cur_page+3).$end.'">...</a></li> ').//class="current"
        '<li><a title="Page '.$page_count.'" href="'.$v_url."{$page_count}".$end.'">'.(($page_count<10)?"{$page_count}":$page_count).'</a></li>' : null;
//
    // Previous and next page
    $previous_page = $cur_page > 1 ? '<li><a title="Page '.($cur_page-1).'" href="'.$v_url.''.($cur_page-1).$end.'"><span class="icon_prev"></span></a></li>' : null;
    $next_page = $cur_page < $page_count ? '<li><a title="Page '.($cur_page+1).'" href="'.$v_url.''.($cur_page+1).$end.'"><span class="icon_next"></span></a></li>' : null;

    // Display pages that are in range
    for ($x=$current_range[0];$x <= $current_range[1]; ++$x){
        if($x==$cur_page)
            //$pages[] = '<li class="current">'.$x.'</li>';
            $pages[] = '<li class="active"><a href="'.$v_url.''.$x.$end.'"><strong>'.$x.'</strong></a></li>';
        else
            $pages[] = '<li><a title="Page '.($x).'" href="'.$v_url.''.$x.$end.'">'.$x.'</a></li>';
    }

    if ($page_count > 1)
        //return '<div class="paging"><ul>'.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul></div>';
        return '<div class="pagination pagination-centered"><ul>'.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul></div>';
    else
        return "";
}
function pagination($page_count, $cur_page, $link, $limit=4, $end="", $split=""){
    $v_url = $link;
    if(substr($v_url,strlen($v_url),1)!='/') $v_url.='/';
    //$v_url.='page';
    $current_range = array(($cur_page-2 < 1 ? 1 : $cur_page-2), ($cur_page+2 > $page_count ? $page_count : $cur_page+2));

    // First and Last pages
    $first_page = $cur_page > 3 ? '<li><a title="Page 1" href="'.$v_url.'page1'.$end.'">1</a></li>'.($cur_page < $limit+1 ? "{$split} " : '<li><a data-page="'.($cur_page-3).'" title="..." href="">...</a></li>') : null; //class="current"
    $last_page = $cur_page < $page_count-2 ? ($cur_page > $page_count-$limit ? "{$split} " : ' <li><a title="..." data-page="'.($cur_page+3).'" href="">...</a></li> ').//class="current"
        '<li><a title="Page '.$page_count.'" href="'.$v_url."page{$page_count}".$end.'">'.(($page_count<10)?"{$page_count}":$page_count).'</a></li>' : null;
//
    // Previous and next page
    //$previous_page = $cur_page > 1 ? '<li><a title="Page '.($cur_page-1).'" href="'.$v_url.'page'.($cur_page-1).$end.'"><span   class="icon_prev" ></span></a></li>' : null;
    //$next_page = $cur_page < $page_count ? '<li><a title="Page '.($cur_page+1).'" href="'.$v_url.'page'.($cur_page+1).$end.'"><span   class="icon_next" ></span></a></li>' : null;
    $previous_page = $cur_page > 1 ? '<li><a title="Page '.($cur_page-1).'" href="'.$v_url.'page'.($cur_page-1).$end.'"><span class="icon_prev"></span></a></li>' : null;
    $next_page = $cur_page < $page_count ? '<li><a title="Page '.($cur_page+1).'" href="'.$v_url.'page'.($cur_page+1).$end.'"><span class="icon_next"></span></a></li>' : null;

    // Display pages that are in range
    for ($x=$current_range[0];$x <= $current_range[1]; ++$x){
        if($x==$cur_page)
            //$pages[] = '<li class="current">'.$x.'</li>';
            $pages[] = '<li class="active"><a href="'.$v_url.'page'.$x.$end.'"><strong>'.$x.'</strong></a></li>';
        else
            $pages[] = '<li><a title="Page '.($x).'" href="'.$v_url.'page'.$x.$end.'">'.$x.'</a></li>';
    }

    if ($page_count > 1)
        //return '<div class="paging"><ul>'.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul></div>';
    return '<div class="pagination pagination-centered"><ul>'.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul></div>';
    else
        return "";
}
function is_admin()
{
    global $arr_user;
    if($arr_user['user_type']==0) return true;
    else return false;
}
function check_permission($p_permission, $p_user_rule,$p_title="")
{
    if(is_admin()) return true;
    else{
        if($p_user_rule=='') return false;
        if(strpos($p_permission,$p_user_rule)!==false) return true;
    }
    return false;
}
function cut_str($p_str,$p_start,$p_end){

    if ($p_start!=""){
        $v_start_post = strpos($p_str,$p_start);
        if ($v_start_post===false) return "";
        $p_str = substr($p_str,$v_start_post+strlen($p_start));
        if ($p_end=="") return $p_str;
        $v_end_post = strpos($p_str,$p_end);
        if ($v_end_post===false) return "";
        $p_str = substr($p_str,0,$v_end_post);
        return $p_str;
    }else{
        if ($p_end!=""){
            $v_end_post = strpos($p_str,$p_end);
            if ($v_end_post===false) return "";
            $p_str = substr($p_str,0,$v_end_post);
            return $p_str;
        }else{
            return "";
        }
    }
}
function cutString ($p_string, $p_separate){
    if(strlen(trim($p_string))==0){
        return false;
    }
    elseif(strpos($p_string, $p_separate)===false){
        return $p_string;
    }
    else{
        $v_separateLen = strlen($p_separate);
        $v_separatePos = strpos($p_string, $p_separate);

        if($v_separatePos === false || $v_separateLen ==0){
            $part[0] = $p_string;
            $part[1] = '';
        }
        else{
            $part[0] = substr($p_string, 0, $v_separatePos);
            $part[1] = substr($p_string, $v_separatePos + $v_separateLen);
        }
        return $part;
    }
}
function rewriteUrl(){
    $self = $_SERVER['PHP_SELF'];
    $stringParams = substr($self,strpos($self,'.php')+5);
    $arrayParams = explode('/', $stringParams);
    foreach ($arrayParams as $param){
        $aItem = cutString($param, '-');
        $_GET[$aItem[0]] = $aItem[1];
    }
}
//function get_list_order_email_back_up(MongoDB $db, $p_company_id,  $p_order_id, $p_location_id = 0, array $arr_user_info = array(), $p_role_key='', array $arr_location = array(),$p_territory = false, $p_receive_location = false, $p_production = false){
function get_list_order_email(MongoDB $db, $p_company_id,  $p_order_id, $p_location_id = 0, array $arr_user_info = array(), $p_role_key='', array $arr_location = array(),$p_territory = false, $p_receive_location = false, $p_production = false){
    $arr_list_email = array();
    $arr_list_extra_email = array();
    $v_list_other_mail = '';
    add_class('cls_tb_company');
    add_class('cls_tb_location');
    add_class('cls_tb_region');
    add_class('cls_tb_contact');
    add_class('cls_tb_user');
    add_class('cls_tb_role');
    add_class('cls_settings');
    add_class('cls_tb_order');
    add_class('cls_tb_order_items');
    $arr_contact = array();
    $cls_company = new cls_tb_company($db, LOG_DIR);
    $cls_location = new cls_tb_location($db, LOG_DIR);
    $cls_order = new cls_tb_order($db, LOG_DIR);
    $cls_item = new cls_tb_order_items($db, LOG_DIR);
    $cls_user = new cls_tb_user($db, LOG_DIR);
    $cls_region = new cls_tb_region($db, LOG_DIR);
    $cls_contact = new cls_tb_contact($db, LOG_DIR);
    $v_row = $cls_company->select_one(array('company_id'=>$p_company_id));
    if($v_row==1){
        $v_contact_id = $cls_company->get_sales_rep();
        settype($v_contact_id, 'int');
        if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
            $arr_contact[] = $v_contact_id;
            $arr_list_email[$v_contact_id] = array(
                'name'=>'', 'email'=>'', 'info'=>'Sales Rep.'
            );
        }
        $v_list_other_mail = $cls_company->get_sales_rep_email();
        $v_list_other_mail = str_replace(',',';', $v_list_other_mail);

        if($v_list_other_mail!=''){

            $arr_tmp = explode(';', $v_list_other_mail);
            for($i=0; $i<count($arr_tmp);$i++){
                $arr_list_extra_email[] = array(
                    'email'=>$arr_tmp[$i],
                    'name'=>'',
                    'info'=>'Extra Sales Rep'
                );
            }
        }
        $v_list_email_head_office = $cls_company->get_email_head_office();
        $v_list_other_mail = $cls_company->get_email_head_office();
        $v_list_other_mail = str_replace(',',';', $v_list_other_mail);
        if($v_list_other_mail!=''){
            $arr_tmp = explode(';', $v_list_other_mail);
            for($i=0; $i<count($arr_tmp);$i++){
                $arr_list_extra_email[] = array(
                    'email'=>$arr_tmp[$i],
                    'name'=>'',
                    'info'=>'Extra HO'
                );
            }
        }
        if($v_list_email_head_office!='' && $v_list_other_mail!='')
            $v_list_other_mail .= ';'. $v_list_email_head_office;
        else if($v_list_other_mail=='')
            $v_list_other_mail = $v_list_email_head_office;
    }
    $v_user_name = '';
    if(/*(($p_territory || $p_receive_location) && sizeof($arr_location)==0)|| */$p_location_id==0 || sizeof($arr_user_info)==0){
        $v_row = $cls_order->select_one(array('order_id'=>$p_order_id));
        if($v_row==1){
            $v_user_name = $cls_order->get_user_name();
            $p_location_id = $cls_order->get_location_id();
        }
    }else
        $v_user_name = $arr_user_info['name'];
    if($v_user_name!=''){
        if(sizeof($arr_user_info)==0){
            $v_contact_id = $cls_user->select_one(array('user_name'=>$v_user_name));
            settype($v_contact_id, 'int');
            if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
                $arr_contact[] = $v_contact_id;
                $arr_list_email[$v_contact_id] = array(
                    'name'=>'', 'email'=>'', 'info'=>'Creator'
                );
            }
        }else{
            $v_contact_id = $arr_user_info['contact'];
            $arr_list_email[$v_contact_id] = array(
                'name'=>$arr_user_info['full'], 'email'=>$arr_user_info['email'], 'info'=>'Creator'
            );
        }
    }
    if($p_location_id>0){
        $v_contact_id = $cls_location->select_scalar('approved_contact', array('location_id'=>$p_location_id));
        settype($v_contact_id, 'int');
        if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
            $arr_contact[] = $v_contact_id;
            $arr_list_email[$v_contact_id] = array(
                'name'=>'', 'email'=>'', 'info'=>'Approver'
            );
        }
    }
    if($p_territory || $p_receive_location){
        if(sizeof($arr_location)==0){
            $arr_item = $cls_item->select(array('order_id'=>$p_order_id));
            $arr_tmp_location = array();
            foreach($arr_item as $arr){
                $arr_allocation = isset($arr['allocation'])?$arr['allocation']:array();
                if(is_array($arr_allocation) && sizeof($arr_allocation)>0){
                    for($i=0; $i<count($arr_allocation);$i++){
                        $v_location_id = (int) $arr_allocation[$i]['location_id'];
                        if(!isset($arr_tmp_location[$v_location_id])){
                            $arr_location[] = $v_location_id;
                            $arr_tmp_location[$v_location_id] = true;
                        }
                    }
                }
            }
        }
        for($i=0;$i<count($arr_location); $i++){
            $v_location_id = $arr_location[$i];
            $v_row = $cls_location->select_one(array('location_id'=>$v_location_id));
            if($v_row==1){
                if($p_territory){// Territory Manager
                    $v_region_id = $cls_location->get_location_region();
                    settype($v_region_id,"int");
                    if($v_region_id>0){
                        $v_contact_id = $cls_region->select_scalar("region_contact",array("region_id"=>$v_region_id));
                        settype($v_contact_id,"int");
                        if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
                            $arr_contact[] = $v_contact_id;
                            $arr_list_email[$v_contact_id] = array(
                                'name'=>'', 'email'=>'', 'info'=>'Territory'
                            );
                        }
                    }
                }
                if($p_receive_location){//Location Receive Mail
                    $v_contact_id = $cls_location->get_main_contact();
                    settype($v_contact_id, 'int');
                    $arr_order_status = $cls_contact->select_scalar("order_status_list",array("contact_id"=>(int)($v_contact_id)));
                    $arr_order_status = is_array($arr_order_status) ? $arr_order_status : array();
                    $v_order_status = (string)$cls_order->select_scalar("status",array("order_id"=>(int)$p_order_id));
                    if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])  && (in_array($v_order_status,$arr_order_status) || $arr_order_status==NULL || count($arr_order_status)==0 )){
                        //  if(in_array($v_order_status,$arr_order_status)) echo "in array order status <br />";
                        //  if($arr_order_status==NULL || count($arr_order_status)==0) echo "order status list = 0 => send all <br />";
                        $arr_contact[] = $v_contact_id;
                        $arr_list_email[$v_contact_id] = array(
                            'name'=>'', 'email'=>'', 'info'=>'Receiver Main Contact'
                        );
                    }
                }
            }
        }
    }
    if($p_role_key!=''){
        $cls_role = new cls_tb_role($db, LOG_DIR);
        $v_role_id = $cls_role->select_scalar('role_id', array('role_key'=>$p_role_key));
        settype($v_role_id, "int");
        if($v_role_id>0){
            $arr_where = array('user_role'=>array('$in'=>array($v_role_id)), '$or'=>array(array('company_id'=>10000),array('company_id'=>$p_company_id)));
            $arr_users = $cls_user->select($arr_where);
            foreach($arr_users as $arr){
                $v_contact_id = (int) (isset($arr['contact_id'])?$arr['contact_id']:0);
                if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
                    $arr_contact[] = $v_contact_id;
                    $arr_list_email[$v_contact_id] = array(
                        'name'=>'', 'email'=>'', 'info'=>'Role Granted'
                    );
                }
            }
        }
    }
    $arr_contacts = $cls_contact->select(array('contact_id'=>array('$in'=>$arr_contact)));
    foreach($arr_contacts as $arr){
        $v_contact_id = (int) (isset($arr['contact_id'])?$arr['contact_id']:0);
        if(trim($arr['middle_name'])!='')
            $v_full_name = $arr['first_name'].' '.$arr['middle_name'].' '.$arr['last_name'];
        else
            $v_full_name = $arr['first_name'].' '.$arr['last_name'];
        $v_full_name = trim($v_full_name);
        $v_email = $arr['email'];
        if(isset($arr_list_email[$v_contact_id])){
            $arr_list_email[$v_contact_id]['name'] = $v_full_name;
            $arr_list_email[$v_contact_id]['email'] = $v_email;
        }
    }
    $arr_return = array();
    $arr_distinct = array();
    foreach($arr_list_email as $arr){
        if(!isset($arr_distinct[strtolower($arr['email'])])){
            $arr_return[] = array('name'=>$arr['name'], 'email'=>$arr['email'], 'info'=>$arr['info']);
            $arr_distinct[strtolower($arr['email'])]=1;
        }
    }
    if($p_production){
        $cls_set = new cls_settings($db, LOG_DIR);
        $v_list_product_mail = $cls_set->get_option_name_by_key('email', 'list_email_production','');
        $v_list_other_mail = $v_list_product_mail;
        $v_list_other_mail = str_replace(',',';', $v_list_other_mail);
        if($v_list_other_mail!=''){
            $arr_tmp = explode(';', $v_list_other_mail);
            for($i=0; $i<count($arr_tmp);$i++){
                $arr_list_extra_email[] = array(
                    'email'=>$arr_tmp[$i],
                    'name'=>'',
                    'info'=>'Production'
                );
            }
        }

        if($v_list_product_mail!=''){
            if($v_list_other_mail!='')
                $v_list_other_mail .= ';'.$v_list_product_mail;
            else
                $v_list_other_mail = $v_list_product_mail;
        }
    }
    if(count($arr_list_extra_email)>0){
        for($i=0; $i<count($arr_list_extra_email);$i++){
            $v_one_email = strtolower($arr_list_extra_email[$i]['email']);
            if(is_valid_email($v_one_email)){
                if(!isset($arr_distinct[$v_one_email])){
                    $v_row = $cls_contact->select_one(array('email'=>new MongoRegex('/'.$v_one_email.'/i')));
                    $arr_return[] = array('email'=>$v_one_email, 'name'=>$v_row==1?$cls_contact->get_full_name():'', 'info'=>$arr_list_extra_email[$i]['info']);
                }
            }
        }
    }
    return $arr_return;
}
function get_list_order_email_by_role(MongoDB $db, $p_company_id,  $p_order_id, $p_location_id = 0, array $arr_user_info = array(), $p_role_key='', array $arr_location = array(),$p_territory = false, $p_receive_location = false, $p_production = false){
    $arr_list_email = array();
    $arr_list_extra_email = array();
    $v_list_other_mail = '';
    add_class('cls_tb_company');
    add_class('cls_tb_location');
    add_class('cls_tb_region');
    add_class('cls_tb_contact');
    add_class('cls_tb_user');
    add_class('cls_tb_role');
    add_class('cls_settings');
    add_class('cls_tb_order');
    add_class('cls_tb_order_items');

    $arr_contact = array();
    $cls_company = new cls_tb_company($db, LOG_DIR);
    $cls_location = new cls_tb_location($db, LOG_DIR);
    $cls_order = new cls_tb_order($db, LOG_DIR);
    $cls_item = new cls_tb_order_items($db, LOG_DIR);
    $cls_user = new cls_tb_user($db, LOG_DIR);
    $cls_region = new cls_tb_region($db, LOG_DIR);
    $cls_contact = new cls_tb_contact($db, LOG_DIR);

    //===================================================================== new

    $v_row = $cls_company->select_one(array('company_id'=>$p_company_id));
    $v_company_modules = $cls_company->get_modules();

    $v_sendmail_to_creator = false;
    $v_sendmail_to_terrytory = false;
    $v_sendmail_to_main_contact = false;
    $v_sendmail_to_sale_rep = false;
    $v_sendmail_to_csr = 0;

    if(strpos($v_company_modules,'creator_mail')!== false)
        $v_sendmail_to_creator = true;

    if(strpos($v_company_modules,'terrytori_mail')!== false)
        $v_sendmail_to_terrytory = true;

    if(strpos($v_company_modules,'send_mail_to_main_contact')!== false)
        $v_sendmail_to_main_contact = true;

    if(strpos($v_company_modules,'anvy_sales_rep_mail')!== false)
        $v_sendmail_to_sale_rep = true;

    if(strpos($v_company_modules,'anvy_csr_mail')!== false){
        $v_sendmail_to_csr = $cls_company->get_csr();
    }

    //=====================================================================

    if($v_row==1){
        $v_contact_id = $cls_company->get_sales_rep();
        settype($v_contact_id, 'int');
        if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id]) && $v_sendmail_to_sale_rep){
            $arr_contact[] = $v_contact_id;
            $arr_list_email[$v_contact_id] = array(
                'name'=>'', 'email'=>'', 'info'=>'Sales Rep.'
            );
        }
        $v_list_other_mail = $cls_company->get_sales_rep_email();
        $v_list_other_mail = str_replace(',',';', $v_list_other_mail);

        if($v_list_other_mail!='' && $v_sendmail_to_sale_rep){

            $arr_tmp = explode(';', $v_list_other_mail);
            for($i=0; $i<count($arr_tmp);$i++){
                $arr_list_extra_email[] = array(
                    'email'=>$arr_tmp[$i],
                    'name'=>'',
                    'info'=>'Extra Sales Rep'
                );
            }
        }

        $v_list_email_head_office = $cls_company->get_email_head_office();
        $v_list_other_mail = $cls_company->get_email_head_office();
        $v_list_other_mail = str_replace(',',';', $v_list_other_mail);
        if($v_list_other_mail!=''){
            $arr_tmp = explode(';', $v_list_other_mail);
            for($i=0; $i<count($arr_tmp);$i++){
                $arr_list_extra_email[] = array(
                    'email'=>$arr_tmp[$i],
                    'name'=>'',
                    'info'=>'Extra HO'
                );
            }
        }


        if($v_list_email_head_office!='' && $v_list_other_mail!='')
            $v_list_other_mail .= ';'. $v_list_email_head_office;
        else if($v_list_other_mail=='')
            $v_list_other_mail = $v_list_email_head_office;
    }

    $v_user_name = '';
    if(/*(($p_territory || $p_receive_location) && sizeof($arr_location)==0)|| */$p_location_id==0 || sizeof($arr_user_info)==0){
        $v_row = $cls_order->select_one(array('order_id'=>$p_order_id));
        if($v_row==1){
            $v_user_name = $cls_order->get_user_name();
            $p_location_id = $cls_order->get_location_id();
        }
    }else
        $v_user_name = $arr_user_info['name'];


    if($v_user_name!=''){
        if(sizeof($arr_user_info)==0){
            $v_contact_id = $cls_user->select_one(array('user_name'=>$v_user_name));
            settype($v_contact_id, 'int');
            if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id]) && $v_sendmail_to_creator){
                $arr_contact[] = $v_contact_id;
                $arr_list_email[$v_contact_id] = array(
                    'name'=>'', 'email'=>'', 'info'=>'Creator'
                );
            }
        }else{
            $v_contact_id = $arr_user_info['contact'];
            if($v_sendmail_to_creator){
                $arr_list_email[$v_contact_id] = array(
                    'name'=>$arr_user_info['full'], 'email'=>$arr_user_info['email'], 'info'=>'Creator'
                );
            }
        }
    }

    if($p_location_id>0){
        $v_contact_id = $cls_location->select_scalar('approved_contact', array('location_id'=>$p_location_id));
        settype($v_contact_id, 'int');
        if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
            $arr_contact[] = $v_contact_id;
            $arr_list_email[$v_contact_id] = array(
                'name'=>'', 'email'=>'', 'info'=>'Approver'
            );
        }
    }
    //if($p_territory || $p_receive_location){
    if($v_sendmail_to_terrytory || ($p_receive_location && $v_sendmail_to_main_contact)){
        if(sizeof($arr_location)==0){
            $arr_item = $cls_item->select(array('order_id'=>$p_order_id));
            $arr_tmp_location = array();
            foreach($arr_item as $arr){
                $arr_allocation = isset($arr['allocation'])?$arr['allocation']:array();
                if(is_array($arr_allocation) && sizeof($arr_allocation)>0){
                    for($i=0; $i<count($arr_allocation);$i++){
                        $v_location_id = (int) $arr_allocation[$i]['location_id'];
                        if(!isset($arr_tmp_location[$v_location_id])){
                            $arr_location[] = $v_location_id;
                            $arr_tmp_location[$v_location_id] = true;
                        }
                    }
                }
            }
        }
        for($i=0;$i<count($arr_location); $i++){
            $v_location_id = $arr_location[$i];
            $v_row = $cls_location->select_one(array('location_id'=>$v_location_id));
            if($v_row==1){
                //if($p_territory){// Territory Manager
                if($v_sendmail_to_terrytory){// Territory Manager
                    $v_region_id = $cls_location->get_location_region();
                    settype($v_region_id,"int");
                    if($v_region_id>0){
                        $v_contact_id = $cls_region->select_scalar("region_contact",array("region_id"=>$v_region_id));
                        settype($v_contact_id,"int");
                        if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
                            $arr_contact[] = $v_contact_id;
                            $arr_list_email[$v_contact_id] = array(
                                'name'=>'', 'email'=>'', 'info'=>'Territory'
                            );
                        }
                    }
                }
                if($p_receive_location && $v_sendmail_to_main_contact){//Location Receive Mail
                    $v_contact_id = $cls_location->get_main_contact();
                    settype($v_contact_id, 'int');
                    $arr_order_status = $cls_contact->select_scalar("order_status_list",array("contact_id"=>(int)($v_contact_id)));
                    $arr_order_status = is_array($arr_order_status) ? $arr_order_status : array();
                    $v_order_status = (string)$cls_order->select_scalar("status",array("order_id"=>(int)$p_order_id));
                    if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])  && (in_array($v_order_status,$arr_order_status) || $arr_order_status==NULL || count($arr_order_status)==0 )){
                        //  if(in_array($v_order_status,$arr_order_status)) echo "in array order status <br />";
                        //  if($arr_order_status==NULL || count($arr_order_status)==0) echo "order status list = 0 => send all <br />";
                        $arr_contact[] = $v_contact_id;
                        $arr_list_email[$v_contact_id] = array(
                            'name'=>'', 'email'=>'', 'info'=>'Receiver Main Contact'
                        );
                    }
                }
            }
        }
    }
    if($p_role_key!=''){
        $cls_role = new cls_tb_role($db, LOG_DIR);
        $v_role_id = $cls_role->select_scalar('role_id', array('role_key'=>$p_role_key));
        settype($v_role_id, "int");
        if($v_role_id>0){
            $arr_where = array('user_role'=>array('$in'=>array($v_role_id)), '$or'=>array(array('company_id'=>10000),array('company_id'=>$p_company_id)));
            $arr_users = $cls_user->select($arr_where);
            foreach($arr_users as $arr){
                $v_contact_id = (int) (isset($arr['contact_id'])?$arr['contact_id']:0);
                if($v_contact_id>0 && !isset($arr_list_email[$v_contact_id])){
                    $arr_contact[] = $v_contact_id;
                    $arr_list_email[$v_contact_id] = array(
                        'name'=>'', 'email'=>'', 'info'=>'Role Granted'
                    );
                }
            }
        }
    }
    settype($v_sendmail_to_csr,"int");
    if($v_sendmail_to_csr > 0){
        if(!in_array($v_sendmail_to_csr,$arr_contact)){
            $arr_contact[] = $v_sendmail_to_csr;
            $arr_list_email[$v_sendmail_to_csr] = array(
                'name'=>'', 'email'=>'', 'info'=>'CSR email'
            );
        }
    }


    $arr_contacts = $cls_contact->select(array('contact_id'=>array('$in'=>$arr_contact)));
    foreach($arr_contacts as $arr){
        $v_contact_id = (int) (isset($arr['contact_id'])?$arr['contact_id']:0);
        if(trim($arr['middle_name'])!='')
            $v_full_name = $arr['first_name'].' '.$arr['middle_name'].' '.$arr['last_name'];
        else
            $v_full_name = $arr['first_name'].' '.$arr['last_name'];
        $v_full_name = trim($v_full_name);
        $v_email = $arr['email'];
        if(isset($arr_list_email[$v_contact_id])){
            $arr_list_email[$v_contact_id]['name'] = $v_full_name;
            $arr_list_email[$v_contact_id]['email'] = $v_email;
        }
    }


    $arr_return = array();
    $arr_distinct = array();
    foreach($arr_list_email as $arr){
        if(!isset($arr_distinct[strtolower($arr['email'])])){
            $arr_return[] = array('name'=>$arr['name'], 'email'=>$arr['email'], 'info'=>$arr['info']);
            $arr_distinct[strtolower($arr['email'])]=1;
        }
    }
    if($p_production){
        $cls_set = new cls_settings($db, LOG_DIR);
        $v_list_product_mail = $cls_set->get_option_name_by_key('email', 'list_email_production','');
        $v_list_other_mail = $v_list_product_mail;
        $v_list_other_mail = str_replace(',',';', $v_list_other_mail);
        if($v_list_other_mail!=''){
            $arr_tmp = explode(';', $v_list_other_mail);
            for($i=0; $i<count($arr_tmp);$i++){
                $arr_list_extra_email[] = array(
                    'email'=>$arr_tmp[$i],
                    'name'=>'',
                    'info'=>'Production'
                );
            }
        }

        if($v_list_product_mail!=''){
            if($v_list_other_mail!='')
                $v_list_other_mail .= ';'.$v_list_product_mail;
            else
                $v_list_other_mail = $v_list_product_mail;
        }
    }
    /*
    if($v_list_other_mail!=''){
        $arr_list_other_email = explode(';', $v_list_other_mail);
        for($i=0; $i<count($arr_list_other_email);$i++){
            $v_one_email = strtolower($arr_list_other_email[$i]);
            if(is_valid_email($v_one_email)){
                if(!isset($arr_distinct[$v_one_email])){
                    $v_row = $cls_contact->select_one(array('email'=>new MongoRegex('/'.$v_one_email.'/i')));
                    $arr_return[] = array('email'=>$v_one_email, 'name'=>$v_row==1?$cls_contact->get_full_name():'', 'info'=>'Extra');
                }
            }
        }
    }
    */
    if(count($arr_list_extra_email)>0){
        for($i=0; $i<count($arr_list_extra_email);$i++){
            $v_one_email = strtolower($arr_list_extra_email[$i]['email']);
            if(is_valid_email($v_one_email)){
                if(!isset($arr_distinct[$v_one_email])){
                    $v_row = $cls_contact->select_one(array('email'=>new MongoRegex('/'.$v_one_email.'/i')));
                    $arr_return[] = array('email'=>$v_one_email, 'name'=>$v_row==1?$cls_contact->get_full_name():'', 'info'=>$arr_list_extra_email[$i]['info']);
                }
            }
        }
    }
/*
    $v_str = json_encode($arr_return).' \r\n ==================================================== \r\n ';
    $v_str .= '\r\n copany module csr'.$v_company_modules.'\r\n';
    $v_str .= '\r\n email csr'.$v_sendmail_to_csr.'\r\n';
    $v_str .= '=====arr list email = '.json_encode($arr_list_email).' \r\n ==================================================== \r\n ';
    $fp = fopen('xem.txt','w');
    fwrite($fp, $v_str, strlen($v_str));
    fclose($fp);
 */

    return $arr_return;
}
function send_email_el(PHPMailer $mail, $p_from_name, $p_from_mail, $arr_to_mail, $p_mail_subject, $p_mail_body,$p_company_status_key=''){
    global $db;
    $mail->IsSMTP();                           // tell the class to use SMTP
    $mail->SMTPDebug   = 0;
    $mail->Host        = "mail.anvydigital.com"; // Sets SMTP server
    $mail->SMTPAuth    = TRUE; // enable SMTP authentication
    $mail->SMTPSecure  = "ssl"; //Secure conection
    $mail->Port        = 465; // set the SMTP port
    $mail->Username    = 'worktraq@anvydigital.com'; // SMTP account username
    $mail->Password    = 'worktraq1234'; // SMTP account password
    $mail->FromName = $p_from_name;
    $mail->From     = $p_from_mail;
    $mail->SMTPDebug = 2;
    $v_add_content = '';
    $mail->AddAddress($arr_to_mail['email']);
    $mail->IsHTML(true);
    $mail->Subject  = $p_mail_subject;
    $mail->Body     = $p_mail_body .$v_add_content;
    try{
        return $mail->Send();
    }catch(phpmailerException $p){
        return false;
    }
}
function send_email(PHPMailer $mail, $p_from_name, $p_from_mail, $arr_to_mail, $p_mail_subject, $p_mail_body,$p_company_status_key=''){
    global $db;
    $mail->IsSMTP();                           // tell the class to use SMTP
    $mail->SMTPAuth   = true;                  // enable SMTP authentication
    $mail->Port       = 25;
    $mail->IsSendmail();  // tell the class to use Sendmail
    $mail->FromName = $p_from_name;
    $mail->From     = $p_from_mail;
    add_class('cls_tb_global');
    $cls_tb_global = new cls_tb_global($db);
    //$v_send = false;
    //$v_result = 0;
    $v_add_content = '';
    $v_website_testing = false;
    if($cls_tb_global->select_scalar('setting_key',array('global_key'=> 'website_testing'))=='enable')
        $v_website_testing =true;

    if($p_company_status_key=='testing') $v_website_testing =true;

    if($v_website_testing==true)
    {
        $v_add_content .='<br> Email receviced :<br>';

        for($i=0;$i<count($arr_to_mail);$i++){
            if(filter_var($arr_to_mail[$i],FILTER_VALIDATE_EMAIL)){
                $v_add_content .= $i .'---'. $arr_to_mail[$i] .'<br>';
            }
        }
        add_class('cls_settings');
        $cls_settings = new cls_settings($db);
        $v_email_testing = $cls_settings->get_option_name_by_key('email','email_testing');
        $arr_to_mail = explode(';',$v_email_testing);
        for($i=0;$i<count($arr_to_mail);$i++){
            if(filter_var($arr_to_mail[$i],FILTER_VALIDATE_EMAIL)){
                $mail->AddAddress($arr_to_mail[$i]);
            }
        }
    }
    else
    {
        $found = false;
        $v_justin_mail = 'justin@anvydigital.com';
        if(isset($arr_to_mail) && count($arr_to_mail)>0){
            for($i=0; $i<count($arr_to_mail); $i++){
                if(isset($arr_to_mail[$i]) && filter_var($arr_to_mail[$i],FILTER_VALIDATE_EMAIL)){
                    $mail->AddAddress($arr_to_mail[$i]);
                    if(strtolower($arr_to_mail[$i])==$v_justin_mail) $found = true;
                }
            }
            if(! $found) $mail->AddAddress($v_justin_mail);

            for($i=0; $i<count($arr_to_mail); $i++){
               if(isset($arr_to_mail[$i])){
                   $v_name = $arr_to_mail[$i]['name'];
                   $v_email = $arr_to_mail[$i]['email'];
                   if(is_valid_email($v_email)){
                       $mail->AddAddress($v_email, $v_name);
                   }
               }
            }
        }
    }
    $mail->IsHTML(true);
    $mail->Subject  = $p_mail_subject;
    $mail->Body     = $p_mail_body .$v_add_content;
    try{
        return $mail->Send();
    }catch(phpmailerException $p){
        return false;
    }
}
function send_mail(PHPMailer $mail, $p_from_name, $p_from_mail, $arr_to_mail, $p_mail_subject, $p_mail_body,$p_company_status_key='',$v_is_support = false){
    global $db;
    $mail->IsSMTP();                           // tell the class to use SMTP
    $mail->SMTPAuth   = true;                  // enable SMTP authentication
    $mail->Port       = 25;
    $mail->IsSendmail();  // tell the class to use Sendmail
    $mail->FromName = $p_from_name;
    $mail->From     = $p_from_mail;
    add_class('cls_tb_global');
    $cls_tb_global = new cls_tb_global($db);
    //$v_send = false;
    //$v_result = 0;
    $v_add_content = '';
    $v_website_testing = false;
    if($cls_tb_global->select_scalar('setting_key',array('global_key'=> 'website_testing'))=='enable')
        $v_website_testing =true;

    if($p_company_status_key=='testing') $v_website_testing =true;

    if($v_website_testing==true)
    {
        $v_add_content .='<br> Email receviced :<br>';

        for($i=0;$i<count($arr_to_mail);$i++){
            if(filter_var($arr_to_mail[$i],FILTER_VALIDATE_EMAIL)){
                $v_add_content .= $i .'---'. $arr_to_mail[$i] .'<br>';
            }
        }
        add_class('cls_settings');
        $cls_settings = new cls_settings($db);
        $v_email_testing = $cls_settings->get_option_name_by_key('email','email_testing');
        $arr_to_mail = explode(';',$v_email_testing);
        for($i=0;$i<count($arr_to_mail);$i++){
            if(filter_var($arr_to_mail[$i],FILTER_VALIDATE_EMAIL)){
                $mail->AddAddress($arr_to_mail[$i]);
            }
        }
    }
    else
    {
        $found = false;
        $v_justin_mail = 'justin@anvydigital.com';
        for($i=0; $i<count($arr_to_mail); $i++){
            if(filter_var($arr_to_mail[$i],FILTER_VALIDATE_EMAIL)){
                $mail->AddAddress($arr_to_mail[$i]);
                if(strtolower($arr_to_mail[$i])==$v_justin_mail) $found = true;
            }
        }
        if(! $found && !$v_is_support) $mail->AddAddress($v_justin_mail);
    }

    $mail->IsHTML(true);
    $mail->Subject  = $p_mail_subject;
    $mail->Body     = $p_mail_body .$v_add_content;
    try{
        return $mail->Send();
    }catch(phpmailerException $p){
        return false;
    }
}
function check_rule_approve($p_order_location_id, array $arr_user){
    if($p_order_location_id==$arr_user['location_default']) return true;
    $v_found = false;
    $arr_location = $arr_user['location_approve'];
    for($i=0; $i<count($arr_location) && !$v_found;$i++){
        if($arr_location[$i]==$p_order_location_id){
            $v_found = true;
        }
    }
    return $v_found;
}
function add_class($p_class_name,$p_class_file="")
{
    if($p_class_file=="") $v_tmp_class_name = $p_class_name .'.php';
    else $v_tmp_class_name = $p_class_file;
    if(!class_exists($p_class_name)){
        if(file_exists('classes/'.$v_tmp_class_name))
            require 'classes/'.$v_tmp_class_name;
        else
        {
            die('Can not require class '. $p_class_name. ' check dir (' .'classes/'.$v_tmp_class_name  .')' );

        }
    }
}
function highlight_text($v_highlight_text , $_text,$v_color_code="#FFFF00"){
    $v_str = str_replace($v_highlight_text,'<span class="highlight" >'. $v_highlight_text .'</span>' ,$_text);
    return $v_str;
}
function create_thumb($p_file_path, $p_thumb_path, $p_pos_tfix, $p_new_width, $p_new_height, $p_old_width, $p_old_height, $p_quality=75)
{

    $gd_formats	= array('jpg','jpeg','png','gif');//web formats
    $file_name	= pathinfo($p_file_path);
    if(empty($p_format)) $p_format = $file_name['extension'];

    if(!in_array(strtolower($file_name['extension']), $gd_formats))
    {
        return false;
    }

    $thumb_name	= $p_pos_tfix.'_'. $file_name['filename'].'.'.$p_format;


    // Get new dimensions
    $newW	= $p_new_width;
    $newH	= $p_new_height;

    // Resample
    $thumb = imagecreatetruecolor($newW, $newH);
    $image = imagecreatefromstring(file_get_contents($p_file_path));

    imagecopyresampled($thumb, $image, 0, 0, 0, 0, $newW, $newH, $p_old_width, $p_old_height);

    // Output
    switch (strtolower($p_format)) {
        case 'png':
            imagepng($thumb, $p_thumb_path.$thumb_name, 9);
            break;

        case 'gif':
            imagegif($thumb, $p_thumb_path.$thumb_name);
            break;

        default:
            imagejpeg($thumb, $p_thumb_path.$thumb_name, $p_quality);
            break;
    }
    imagedestroy($image);
    imagedestroy($thumb);
}
function pad_left($p_str, $p_len = 0, $p_char=' '){
    $v_str = trim($p_str);
    if($p_len>0){
        $v_count = $p_len - strlen($v_str);
        if($v_count > 0){
            for($i=0;$i<$v_count;$i++){
                $v_str = $p_char.$v_str;
            }
        }
    }
    return $v_str;
}
function record_sort($arr_array, $p_field, $p_reverse=false){
    $arr_hash = array();
    foreach($arr_array as $arr){
        $arr_hash[$arr[$p_field]] = $arr;
    }
    ($p_reverse)? krsort($arr_hash) : ksort($arr_hash);
    $arr_return = array();
    foreach($arr_hash as $arr){
        $arr_return []= $arr;
    }

    return $arr_return;
}
function str_format($p_str,$p_format = 'camel'){
    $p_str = str_replace("_"," ",$p_str);
    switch($p_format){
        case 'camel':
            $p_str = ucwords($p_str);
            break;
        default:
            $p_str = ucwords($p_str);
    }
    return $p_str;
}
function get_mail_list_of_special_key(cls_tb_role $cls_role, cls_tb_user $cls_user, cls_tb_contact $cls_contact,$p_company_id=0, $p_role_key = 'send_mail_submit_order'){
    $arr_email = array();
    $v_role_id = $cls_role->select_scalar('role_id', array('role_key'=>$p_role_key));
    settype($v_role_id, "int");
    if($v_role_id>0){
        $arr_where = array('user_role'=>array('$in'=>array($v_role_id)), '$or'=>array(array('company_id'=>10000),array('company_id'=>$p_company_id)));
        $arr_user_id = $cls_user->select($arr_where);
        foreach($arr_user_id as $arr){
            $arr_email[] = $cls_contact->select_scalar("email",array("contact_id"=>(int)$arr['contact_id']));
        }
    }
    return $arr_email;
}
function check_exist(cls_tb_user $cls_user,cls_tb_location $cls_location,cls_tb_company $cls_comp,cls_tb_order $cls_order,$p_user_name,$p_user_pwd,$p_company_id,$p_location_id,$p_order_id){
    $v_exits = 0;
    $arr_user_encry = $cls_user->select_encry("user_name",array());
    if(in_array($p_user_name,$arr_user_encry)){
        $arr_user_pwd_envry = $cls_user->select_notencry("user_password",array());
        if(in_array($p_user_pwd,$arr_user_pwd_envry)){
            $arr_comp_encry = $cls_comp->select_encry("company_id",array());
            if(in_array($p_company_id,$arr_comp_encry)){
                $arr_location_encry = $cls_location->select_encry("location_id",array());
                if(in_array($p_location_id,$arr_location_encry)){
                    $arr_order_encry = $cls_order->select_encry("order_id",array());
                    if(in_array($p_order_id,$arr_order_encry)){
                        // vao toi day la` cac user name,pwd ... la` co ton` tai roi nhung chua chac chung la` cua cung` 1 users
                        $v_exits = 1;
                    }
                }
            }
        }
    }
    $v_same_user = 0;
    if($v_exits){
        $v_count = $cls_user->get_user_name_by_($p_user_name,array("user_password"=>$p_user_pwd));
        if($v_count==1){
            $v_user_name = $cls_user->get_user_name();
            $v_user_name_envy = md5($v_user_name);
            $v_company_envy = md5($cls_user->get_company_id());
            $v_location_envy = md5($cls_user->get_location_id());
            if($v_user_name_envy == $p_user_name){ // user_name va` user_password la` cua cung` 1 nguoi
                if($v_company_envy == $p_company_id){ // dung company id
                    if($v_location_envy == $p_location_id){ // dung location id
                        $arr_order = $cls_order->select(array("user_name"=>$v_user_name));// kiem tra xem users da~ tao order chua
                        foreach($arr_order as $arr){// kiem tra xem order do co dung do users do tao ra hay khong ? dung thi` tra ve order id do luon
                            if(md5($arr['order_id'])==$p_order_id){
                                $v_same_user = (int)$arr['order_id'];
                                break;
                            }
                        }

                    }
                }
            }
        }
    }
    return $v_same_user;
}
function check_product_banner($arr_location_banner,$arr_product_banner = array(),$p_user_tpe){
    if(count($arr_product_banner)<=0 || !is_array($arr_product_banner) || is_null($arr_product_banner))
        return true;
    //if(count($arr_location_banner)<=0 || !is_array($arr_location_banner) || is_null($arr_location_banner))
        //return true;
    if($arr_location_banner=='' || is_null($arr_location_banner) )
        return true;
    settype($p_user_tpe,"int");
    if($p_user_tpe <=3) return true;
    for($i=0;$i<count($arr_product_banner);$i++){
        $v_index = $arr_product_banner[$i];
        //if(in_array($v_index,$arr_location_banner)) return true;
        for($j=0;$j<count($arr_location_banner);$j++){
            if($v_index == $arr_location_banner[$j]) return true;
        }
        //if(in_array($v_index,$arr_location_banner)) return true;
    }
    return false;
}
function insert_user_log($db,$arr_device,$arr_information,$arr_get = array(),$arr_post = array(),$p_url){
    //die(var_dump($arr_information));
    add_class("cls_tb_user_log");
    $cls_tb_user_log = new cls_tb_user_log($db);
    $v_total_record = $cls_tb_user_log->count();
    if($v_total_record > 10000000) $cls_tb_user_log->delete();
    $v_log_id = $cls_tb_user_log->select_next("log_id");
    $cls_tb_user_log->set_log_id($v_log_id);
    $cls_tb_user_log->set_company_id($arr_information['company_id']);
    $cls_tb_user_log->set_company_name(isset($arr_information['company_name'])?$arr_information['company_name']:'');
    $cls_tb_user_log->set_log_datetime(date('Y-m-d H:i:s'));
    $cls_tb_user_log->set_user_id($arr_information['user_id']);
    $cls_tb_user_log->set_user_name($arr_information['user_name']);
    $cls_tb_user_log->set_log_get($arr_get);
    $cls_tb_user_log->set_log_post($arr_post);
    $cls_tb_user_log->set_log_device($arr_device);
    $cls_tb_user_log->set_log_ipaddress($arr_information['user_ip']);
    $cls_tb_user_log->set_log_url($p_url);
    $cls_tb_user_log->insert();
}
function translate_language($db,$v_language){
    if(!isset($_SESSION['ss_language_value'])){
        $arr_ss [$v_language] = get_data_language($db,$v_language);
        $_SESSION['ss_language_value'] = serialize($arr_ss);
    }else{
        $arr_ss = unserialize($_SESSION['ss_language_value']);
        if(!isset($arr_ss[$v_language])){
            $arr_ss_language = get_data_language($db,$v_language);
            $arr_ss [$v_language] = $arr_ss_language;
            $_SESSION['ss_language_value'] = serialize($arr_ss);
        }
    }
}
function auto_generate_product_sku($v_product_category_id,$v_code,$db){
    add_class("cls_tb_nail_category");
    $cls_category = new cls_tb_nail_category($db);
    if($v_product_category_id!='' && $v_product_category_id!=0){
        $select_one = $cls_category->select_one(array("_id"=>new MongoId($v_product_category_id)));
        $v_return = '';
        $v_return_string = '';
        if($select_one){
            if($select_one){
                $v_parent_id = 0;
                do{
                    $v_return = '';
                    $v_name = $cls_category->get_name();
                    $arr_name = explode(" ",$v_name);
                    for($i=0;$i<count($arr_name);$i++){
                        $v_name = $arr_name[$i];
                        $v_name = remove_invalid_char($v_name);
                        $v_return .=substr($v_name,0,1);
                    }
                    $v_return .='-';
                    $v_return_string = $v_return.$v_return_string;
                    $v_parent_id = (string)$cls_category->get_parent_id();
                    try{
                        $select_one = $cls_category->select_one(array("_id"=>new MongoId((string)$v_parent_id)));
                    }catch (Exception $ex){
                        $select_one = $cls_category->select_one(array("_id"=>new MongoId()));
                    }
                    if(!$select_one) break;
                }while($v_parent_id!=0);
            }
        }
        $v_return_string .=$v_code;
        return $v_return_string;
    }else{
        return $v_code;
    }
}
function get_data_language($db,$v_language){
    add_class("cls_tb_message");
    $cls_tb_message = new cls_tb_message($db);
    $arr_ss_language ['HEADER_BASKET'] = $cls_tb_message->select_value($v_language.'_HEADER_BASKET');
    $arr_ss_language ['SEARCH_SORT'] = $cls_tb_message->select_value($v_language.'_SEARCH_SORT');
    $arr_ss_language ['SEARCH_VIEW'] = $cls_tb_message->select_value($v_language.'_SEARCH_VIEW');
    $arr_ss_language ['SEARCH_FOUND'] = $cls_tb_message->select_value($v_language.'_SEARCH_FOUND');
    $arr_ss_language ['SIGN_UP_TEXT'] = $cls_tb_message->select_value($v_language.'_SIGN_UP_TEXT');
    $arr_ss_language ['COPY_RIGHT'] = $cls_tb_message->select_value($v_language.'_COPY_RIGHT');
    $arr_ss_language ['STORES'] = $cls_tb_message->select_value($v_language.'_STORES');
    $arr_ss_language ['NOT_YOU'] = $cls_tb_message->select_value($v_language.'_NOT_YOU');
    $arr_ss_language ['SIGN_UP_TEXT_INPUT_PLACEHOLDER'] = $cls_tb_message->select_value($v_language.'_SIGN_UP_TEXT_INPUT_PLACEHOLDER');
    $arr_ss_language ['STORE_LOCATION_AND_EVENT'] = $cls_tb_message->select_value($v_language.'_STORE_LOCATION_AND_EVENT');
    $arr_ss_language ['STORE_LOCATION_AND_EVENT_INPUT_PLACEHOLDER'] = $cls_tb_message->select_value($v_language.'_STORE_LOCATION_AND_EVENT_INPUT_PLACEHOLDER');
    $arr_ss_language ['SEARCH_FORM'] = $cls_tb_message->select_value($v_language.'_SEARCH_FORM');
    $arr_ss_language ['LIMITED_EDITION'] = $cls_tb_message->select_value($v_language."_LIMITED_EDITION");
    $arr_ss_language ['VIEW_LAGER'] = $cls_tb_message->select_value($v_language."_VIEW_LAGER");
    $arr_ss_language ['ADD_TO_LOVE'] = $cls_tb_message->select_value($v_language."_ADD_TO_LOVE");
    $arr_ss_language ['ADD_TO_BASKET'] = $cls_tb_message->select_value($v_language."_ADD_TO_BASKET");
    $arr_ss_language ['FIND_IN_STORE'] = $cls_tb_message->select_value($v_language."_FIND_IN_STORE");
    $arr_ss_language ['FIND_IN_STORE_PLACEHOLDER'] = $cls_tb_message->select_value($v_language."_FIND_IN_STORE_PLACEHOLDER");
    $arr_ss_language ['DESCRIPTION'] = $cls_tb_message->select_value($v_language."_DESCRIPTION");
    $arr_ss_language ['INGREDIENTS'] = $cls_tb_message->select_value($v_language."_INGREDIENTS");
    $arr_ss_language ['SHIPPING_INFO'] = $cls_tb_message->select_value($v_language."_SHIPPING_INFO");
    $arr_ss_language ['ALSO_LIKE'] = $cls_tb_message->select_value($v_language."_ALSO_LIKE");
    $arr_ss_language ['RATING_REVIEWS'] = $cls_tb_message->select_value($v_language."_RATING_REVIEWS");
    $arr_ss_language ['TAB_RATING_REVIEW'] = $cls_tb_message->select_value($v_language."_TAB_RATING_REVIEW");
    $arr_ss_language ['TAB_Q_A'] = $cls_tb_message->select_value($v_language."_TAB_Q_A");
    $arr_ss_language ['WRITE_REVIEW'] = $cls_tb_message->select_value($v_language."_WRITE_REVIEW");
    $arr_ss_language ['ASK_QUESTION'] = $cls_tb_message->select_value($v_language."_ASK_QUESTION");
    return $arr_ss_language;
}
function session_basket(&$arr_ss_basket,&$arr_ss_basket_content,$p_product_id,$p_quantity,$p_user_id,$db){
    add_class("cls_tb_nail_cart");
    $cls_tb_cart = new cls_tb_nail_cart($db);
    add_class("cls_tb_product");
    $cls_tb_product = new cls_tb_product($db);
    if(in_array($p_product_id,$arr_ss_basket)) return false;
    if($p_user_id!=''){
        // has log-in
        if(!in_array($p_product_id,$arr_ss_basket)){
            // neu basket nay` chua co trong session[ sau khi dang nhap thi` dong nghia voi viec chua co trong database ]
            $v_select_one = $cls_tb_product->select_one(array("_id"=>new MongoId($p_product_id)));
            if($v_select_one){
                $v_product_image = $cls_tb_product->get_products_upload();
                $v_product_name = $cls_tb_product->get_name();
                $v_product_sku = $cls_tb_product->get_sku();
                $v_product_sell_price = $cls_tb_product->get_sell_price();
                $v_product_sell_price_discount = $cls_tb_product->get_product_sell_discount();
                $v_product_slugger = $cls_tb_product->get_slugger();
                $v_category_id = $cls_tb_product->get_category_id();
                $v_brand_id = $cls_tb_product->get_brand_id();
                $v_product_type = $cls_tb_product->get_is_free_sample();
                $v_product_code = $cls_tb_product->get_code();
                $v_result = insert_one_cart_item($db,$p_product_id,$v_product_image,$v_product_name,$v_product_sku,$v_product_sell_price,$p_quantity,$p_user_id,$v_product_slugger,$v_category_id,$v_brand_id,$v_product_type,$v_product_code,$v_product_sell_price_discount);
                if($v_result){
                    add_more_item_to_basket($arr_ss_basket_content,$p_product_id,$p_quantity,$v_product_image,$v_product_slugger,$p_user_id,$v_product_name,$v_product_sku,$v_product_sell_price,$v_category_id,$v_brand_id,$db,$v_product_type,$v_product_code,$v_product_sell_price_discount);
                    $arr_ss_basket [] = $p_product_id;
                }
            }
        }else{
            // neu da~ co thi` update quantity
            // 1) update table cart
            $v_select_one = $cls_tb_cart->select_one(array("product_id"=>$p_product_id,"user_id"=>$p_user_id));
            if($v_select_one==1){
                $cls_tb_cart->set_product_quantity($p_quantity);
                $cls_tb_cart->update();
            }
            // 2) update session basket
            for($i=0;$i<count($arr_ss_basket_content);$i++){
                $v_product_id_temp = $arr_ss_basket_content[$i]['product_id'];
                if($v_product_id_temp != $p_product_id) continue;
                $arr_ss_basket_content[$i]['product_quantity'] = $p_quantity;
            }
        }
    }else{
        // chua dang nhap, kiem tra va` them vao session
        if(!in_array($p_product_id,$arr_ss_basket)){
            // neu chua co
            $v_select_one = $cls_tb_product->select_one(array("_id"=>new MongoId($p_product_id)));
            if($v_select_one){
                $v_product_image = $cls_tb_product->get_products_upload();
                $v_product_name = $cls_tb_product->get_name();
                $v_product_sku = $cls_tb_product->get_sku();
                $v_product_sell_price = $cls_tb_product->get_sell_price();
                $v_product_slugger = $cls_tb_product->get_slugger();
                $v_category_id = $cls_tb_product->get_category_id();
                $v_brand_id = $cls_tb_product->get_brand_id();
                $v_product_type = $cls_tb_product->get_is_free_sample();
                $v_product_code = $cls_tb_product->get_code();
                $v_product_sell_price_discount = $cls_tb_product->get_product_sell_discount();
                add_more_item_to_basket($arr_ss_basket_content,$p_product_id,$p_quantity,$v_product_image,$v_product_slugger,$p_user_id,$v_product_name,$v_product_sku,$v_product_sell_price,$v_category_id,$v_brand_id,$db,$v_product_type,$v_product_code,$v_product_sell_price_discount);
                $arr_ss_basket [] = $p_product_id;
            }
        }else{
            // neu da co
            for($i=0;$i<count($arr_ss_basket_content);$i++){
                $v_product_id_temp = $arr_ss_basket_content[$i]['product_id'];
                if($v_product_id_temp != $p_product_id) continue;
                $arr_ss_basket_content[$i]['product_quantity'] = $p_quantity;
            }
        }
    }
    return true;
}
function get_mongo_id_new_version($v_brand_id){
    try{
        $v_brand_id = new MongoId($v_brand_id);
    }catch(Exception $ex){
        $v_brand_id = new MongoId();
    }
    return $v_brand_id;
}
function insert_one_cart_item($db,$p_product_id,$v_product_image,$v_product_name,$v_product_sku,$v_product_sell_price,$p_quantity,$p_user_id,$v_product_slugger,$v_category_id,$v_brand_id,$v_product_type,$v_product_code,$v_product_sell_price_discount){

    add_class("cls_tb_nail_cart");
    $cls_tb_cart = new cls_tb_nail_cart($db);
    add_class("cls_tb_nail_category");
    $cls_tb_category = new cls_tb_nail_category($db);
    add_class("cls_tb_banner");
    $cls_tb_banner = new cls_tb_banner($db);
    $v_cart_id = $cls_tb_cart->select_next("cart_id");
    $cls_tb_cart->set_tax(5);
    $cls_tb_cart->set_cart_id($v_cart_id);
    $cls_tb_cart->get_product_code($v_product_code);
    $cls_tb_cart->set_category_id($v_category_id);
    $cls_tb_cart->set_product_type($v_product_type);
    $cls_tb_cart->set_category_name($cls_tb_category->select_scalar("name",array("_id"=>get_mongo_id_new_version($v_brand_id))) );
    $cls_tb_cart->set_brand_id($v_brand_id);

    $cls_tb_cart->set_brand_name($cls_tb_banner->select_scalar("banner_name",array("_id"=>get_mongo_id_new_version($v_brand_id))));
    $cls_tb_cart->set_product_id($p_product_id);
    $cls_tb_cart->set_product_image($v_product_image);
    $cls_tb_cart->set_product_name($v_product_name);
    $cls_tb_cart->set_product_sku($v_product_sku);
    $cls_tb_cart->set_product_price($v_product_sell_price);
    $cls_tb_cart->set_product_sell_discount($v_product_sell_price_discount);
    $cls_tb_cart->set_product_quantity($p_quantity);
    $cls_tb_cart->set_user_id($p_user_id);
    $cls_tb_cart->set_product_slugger($v_product_slugger);
    return $cls_tb_cart->insert();
}

function add_more_item_to_basket(&$arr_ss_basket_content,$p_product_id,$p_quantity,$v_product_image,$v_product_slugger,$p_user_id,$v_product_name,$v_product_sku,$v_product_sell_price,$v_category_id,$v_brand_id,$db,$v_product_type,$v_product_code,$v_product_sell_price_discount){
    add_class("cls_tb_nail_category");
    $cls_tb_category = new cls_tb_nail_category($db);
    add_class("cls_tb_banner");
    $cls_tb_banner = new cls_tb_banner($db);
    $v_category_name = $cls_tb_category->select_scalar("name",array("_id"=>get_mongo_id_new_version($v_category_id)));
//    $v_banner_name = $cls_tb_banner->select_scalar("banner_name",array("_id"=>new MongoId($v_brand_id)));
    return $arr_ss_basket_content [] = array(
        'product_id' => $p_product_id
        ,'product_type' => $v_product_type
        ,'product_quantity' => $p_quantity
        ,'category_id' => $v_category_id
        ,'category_name' => $v_category_name
        ,'brand_id' => ''
        ,'product_code' => $v_product_code
        ,'brand_name' => 'no brand'
        ,'product_image' =>$v_product_image
        ,'product_slugger' => $v_product_slugger
        ,'user_id' => $p_user_id
        ,'product_name' => $v_product_name
        ,'product_sku' => $v_product_sku
        ,'product_price' => $v_product_sell_price
        ,'tax' => 5
        ,'discount' => $v_product_sell_price_discount
    );
}
function get_data_table_cart($arr_basket,$arr_basket_content,$db,$p_user_id,&$p_count){
    add_class("cls_tb_nail_cart");
    $cls_tb_cart = new cls_tb_nail_cart($db);
    add_class("cls_tb_product");
    $cls_tb_product = new cls_tb_product($db);
    $arr_select_cart = $cls_tb_cart->select_distinct("product_id",$db,array('$or'=>array(array("user_id"=>$p_user_id),array("user_id"=>(string)$p_user_id))));
    $arr_cart_product_id = $arr_select_cart['values'];
    for($i=0;$i<count($arr_basket_content);$i++){
        $v_product_id_temp = $arr_basket_content[$i]['product_id'];
        $v_product_image = $arr_basket_content[$i]['product_image'];
        $v_product_name = $arr_basket_content[$i]['product_name'];
        $v_product_sku = $arr_basket_content[$i]['product_sku'];
        $v_product_sell_price = $arr_basket_content[$i]['product_price'];
        $p_quantity = $arr_basket_content[$i]['product_quantity'];
        $v_category_id = $arr_basket_content[$i]['category_id'];
        $v_brand_id = $arr_basket_content[$i]['brand_id'];
        $v_product_slugger = $arr_basket_content[$i]['product_slugger'];
        $v_product_type = $arr_basket_content[$i]['product_type'];
        $v_product_code = $arr_basket_content[$i]['product_code'];
        $v_product_sell_price_discount = $arr_basket_content[$i]['discount'];
        if(!in_array($v_product_id_temp,$arr_cart_product_id)){ // chua co thi add
            insert_one_cart_item($db,$v_product_id_temp,$v_product_image,$v_product_name,$v_product_sku,$v_product_sell_price,$p_quantity,$p_user_id,$v_product_slugger,$v_category_id,$v_brand_id,$v_product_type,$v_product_code,$v_product_sell_price_discount);
        }else{
            $cls_tb_cart->update_field("product_quantity",$p_quantity,array("user_id"=>$p_user_id,"product_id"=>$v_product_id_temp));
        }
    }
    return get_data_ss_basket_after_login($db,$p_user_id,$p_count);
}
function get_data_ss_basket_after_login($db,$v_user_id,&$p_count){
    add_class("cls_tb_nail_cart");
    $cls_tb_cart = new cls_tb_nail_cart($db);
//    $arr_select = $cls_tb_cart->select(array("user_id"=>$v_user_id));
    $arr_select = $cls_tb_cart->select(array('$or'=>array(array("user_id"=>$v_user_id),array("user_id"=>(string)$v_user_id))));
    $arr_return = array();
    $tax = isset($arr['tax']) ? $arr['tax'] : 5;
    $discount = isset($arr['discount']) ? $arr['discount'] : 0;
    $discount = (float) $discount;
    $arr_check = array(); // just in case...
    foreach($arr_select as $arr){
        if(in_array($arr['product_id'],$arr_check)) continue;
        $arr_check[] = $arr['product_id'];
        $arr_return[] = array(
            'product_id' => $arr['product_id']
            ,'product_quantity' => $arr['product_quantity']
            ,'product_image' => $arr['product_image']
            ,'product_slugger' => $arr['product_slugger']
            ,'user_id' => $arr['user_id']
            ,'product_code' => $arr['product_code']
            ,'product_type' => $arr['product_type']
            ,'category_id' => $arr['category_id']
            ,'category_name' => $arr['category_name']
            ,'brand_id' => $arr['brand_id']
            ,'tax' => $tax
            ,'brand_name' => $arr['brand_name']
            ,'product_name' => $arr['product_name']
            ,'product_sku' => $arr['product_sku']
            ,'product_price' => $arr['product_price']
            ,'discount' =>$discount
        );
    }
    $p_count = count($arr_return);
    return $arr_return;
}
function delete_basket($db,$p_user_id,&$arr_basket,&$arr_basket_content,$p_product_id,&$v_total_order,&$v_product_price){
    add_class("cls_tb_nail_cart");
    $cls_tb_cart = new cls_tb_nail_cart($db);
    if($p_user_id!=''){
        // da dang nhap
        // 1) xoa trong talbe cart
        $cls_tb_cart->delete(array("product_id"=>$p_product_id,"user_id"=>$p_user_id));
        // 2) xoa trong session
        delete_one_item_ss_basket($arr_basket,$arr_basket_content,$p_product_id,$v_total_order,$v_product_price);
    }else{
        // xoa trong session
        delete_one_item_ss_basket($arr_basket,$arr_basket_content,$p_product_id,$v_total_order,$v_product_price);
    }
}
function delete_one_item_ss_basket(&$arr_basket,&$arr_basket_item,$p_product_id,&$v_total_order,&$v_product_price){
    $arr_new_basket = array();
    $arr_new_basket_content = array();
//    for($i=0;$i<count($arr_basket);$i++){
//        if($arr_basket[$i]!=$p_product_id) $arr_new_basket[] = $arr_basket[$i];
//    }
    for($i=0;$i<count($arr_basket_item);$i++){
        if($arr_basket_item[$i]['product_id']!=$p_product_id) {
            $arr_new_basket_content[] = $arr_basket_item[$i];
            $v_total_order +=(float)$arr_basket_item[$i]['product_price']*$arr_basket_item[$i]['product_quantity'];
            $arr_new_basket[] = $arr_basket[$i];
        }else {
            $v_product_price =(float)$arr_basket_item[$i]['product_price']*$arr_basket_item[$i]['product_quantity'];
        }
    }
    $arr_basket = $arr_new_basket;
    $arr_basket_item = $arr_new_basket_content;
}
function delete_all_free_sample(&$arr_basket,&$arr_basket_item,&$arr_delete_id){
    $arr_new_basket = array();
    $arr_new_basket_content = array();
    for($i=0;$i<count($arr_basket_item);$i++){
        if($arr_basket_item[$i]['product_type']!=1){
            $arr_new_basket_content[] = $arr_basket_item[$i];
            $arr_new_basket[] = $arr_basket[$i];
        }else{
            $arr_delete_id [] = $arr_basket[$i];
        }
    }
    $arr_basket = $arr_new_basket;
    $arr_basket_item = $arr_new_basket_content;
}
function add_product_to_favorite_list($p_user_id,&$arr_favorite_list,&$arr_favorite_list_content,$p_product_id,$db){
    add_item_to_favorite_list($p_user_id,$arr_favorite_list,$arr_favorite_list_content,$p_product_id,$db);
}
function delete_love($p_user_id,&$arr_favorite_list,&$arr_favorite_list_content,$p_product_id,$db){
    if($p_user_id!=''){
        add_class("cls_tb_nail_favorite_list");
        $cls_favorite_list = new cls_tb_nail_favorite_list($db);
        $cls_favorite_list->delete(array("user_id"=>$p_user_id,"product_id"=>$p_product_id));
    }
    $arr_new_list = array();
    $arr_new_list_item = array();
    for($i=0;$i<count($arr_favorite_list_content);$i++){
        if($p_product_id != $arr_favorite_list_content[$i]['product_id']){
            $arr_new_list [] = $arr_favorite_list_content[$i]['product_id'];
            $arr_new_list_item [] = $arr_favorite_list_content[$i];
        }
    }
    $arr_favorite_list = $arr_new_list;
    $arr_favorite_list_content = $arr_new_list_item;
}
function add_item_to_favorite_list($p_user_id,&$arr_favorite_list,&$arr_favorite_list_content,$p_product_id,$db){
    add_class("cls_tb_nail_favorite_list");
    $cls_favorite_list = new cls_tb_nail_favorite_list($db);
    add_class("cls_tb_product");
    $cls_tb_product = new cls_tb_product($db);
    $cls_tb_product->select_one(array("_id"=>new MongoId($p_product_id)));
    if($p_user_id==''){
        // chua dang nhap
        if(empty($arr_favorite_list)){
            $arr_favorite_list [] = $p_product_id;
            $arr_favorite_list_content [] = array(
                'product_id'=>$p_product_id
                ,'user_id' => $p_user_id
                ,'product_image' => $cls_tb_product->get_products_upload()
                ,'product_name' => $cls_tb_product->get_name()
                ,'product_sku' => $cls_tb_product->get_sku()
                ,'product_price' =>$cls_tb_product->get_sell_price()
            );
        }else if(!in_array($p_product_id,$arr_favorite_list)){
            $arr_favorite_list [] = $p_product_id;
            $arr_favorite_list_content [] = array(
                'product_id'=>$p_product_id
                ,'user_id' => $p_user_id
                ,'product_image' => $cls_tb_product->get_products_upload()
                ,'product_name' => $cls_tb_product->get_name()
                ,'product_sku' => $cls_tb_product->get_sku()
                ,'product_price' =>$cls_tb_product->get_sell_price()
            );
        }
    }else{
        //1) insert to table
        $v_favorite_id = $cls_favorite_list->select_next("favorite_id");
        $cls_favorite_list->set_favorite_id($v_favorite_id);
        $cls_favorite_list->set_product_id($p_product_id);
        $cls_favorite_list->set_product_image($cls_tb_product->get_products_upload());
        $cls_favorite_list->set_product_name($cls_tb_product->get_name());
        $cls_favorite_list->set_product_price($cls_tb_product->get_sell_price());
        $cls_favorite_list->set_product_sku($cls_tb_product->get_sku());
        $cls_favorite_list->set_user_id($p_user_id);
        $v_mongo_id = $cls_favorite_list->insert();
        $arr_favorite_list [] = $p_product_id;
        $arr_favorite_list_content [] = array(
            'product_id'=>$p_product_id
            ,'favorite_id' => (string) $v_mongo_id
            ,'user_id' => $p_user_id
            ,'product_image' => $cls_tb_product->get_products_upload()
            ,'product_name' => $cls_tb_product->get_name()
            ,'product_sku' => $cls_tb_product->get_sku()
            ,'product_price' =>$cls_tb_product->get_sell_price()
        );
        // kiem ra ben ngoai
//        $v_count = $cls_favorite_list->count(array("user_id"=>$p_user_id,"product_id"=>$p_product_id));
//        if(!$v_count){
//            $v_select_one = $cls_tb_product->select_one(array("_id"=>new MongoId($p_product_id)));
//            if($v_select_one){
//
//            }
//        }
        //2) add to session
    }
}
function get_data_favorite_list_after_login($db,$p_user_id,&$arr_favorite_list,&$arr_favorite_list_content){
    add_class("cls_tb_nail_favorite_list");
    $cls_favorite_list = new cls_tb_nail_favorite_list($db);
    if(!empty($arr_favorite_list_content)){
        for($i=0;$i<count($arr_favorite_list_content);$i++){
            $v_product_id = $arr_favorite_list_content[$i]['product_id'];
            $v_check = $cls_favorite_list->count(array("product_id"=>$v_product_id,"user_id"=>$p_user_id));
            if($v_check==0){
                $v_fv_id = $cls_favorite_list->select_next("favorite_id");
                $cls_favorite_list->set_favorite_id($v_fv_id);
                $cls_favorite_list->set_product_id($v_product_id);
                $cls_favorite_list->set_user_id($p_user_id);
                $cls_favorite_list->set_product_name($arr_favorite_list_content[$i]['product_name']);
                $cls_favorite_list->set_product_sku($arr_favorite_list_content[$i]['product_sku']);
                $cls_favorite_list->set_product_price($arr_favorite_list_content[$i]['product_price']);
                $cls_favorite_list->set_product_image($arr_favorite_list_content[$i]['product_image']);
                $cls_favorite_list->insert();
            }
        }
    }
    $arr_qry_all_data_by_user_id = $cls_favorite_list->select(array("user_id"=>$p_user_id));
    foreach($arr_qry_all_data_by_user_id as $arr){
        if(!in_array($arr['product_id'],$arr_favorite_list)){
            $arr_favorite_list [] = $arr['product_id'];
            $arr_favorite_list_content [] = array(
                'product_id'=>$arr['product_id']
                ,'user_id' => $arr['user_id']
                ,'product_image' => $arr['product_image']
                ,'product_name' => $arr['product_name']
                ,'product_sku' => $arr['product_sku']
                ,'product_price' => $arr['product_price']
            );
        }
    }
}
function checkProvincesKeyValid($provinceKey,$db){
    $cls_tb_provinces = new cls_tb_province($db);
    return $cls_tb_provinces->select_one(array("key"=>$provinceKey));
}
function update_session_tax($provincesVal,$arr_session,$db){
    $cls_tb_tax = new cls_tb_tax($db);
    $checkProvinceExisted = $cls_tb_tax->select_one(array("province_key"=>$provincesVal));
    $totalPrice = 0;
    $taxValue = 0;
    if(!$checkProvinceExisted){
        updateSessionTax($arr_session,5,$totalPrice,$taxValue,$provincesVal);
    }else{
        updateSessionTax($arr_session,(float)$cls_tb_tax->get_fed_tax(),$totalPrice,$taxValue,$provincesVal);
    }
    $arr_return = array('data'=>$arr_session,'NewTaxValue'=>$taxValue,'totalPrice'=>$totalPrice);
    return $arr_return;
}
function updateSessionTax(&$arr_session,$taxValue,&$totalPrice,&$taxValueCheck,$provincesVal='AB'){
//    $taxValueCheck = $taxValue;
    for($i=0;$i<count($arr_session);$i++){
        $arr_session[$i]['tax'] = $taxValue;
        $arr_session[$i]['taxProvince'] = $provincesVal;
        $totalPrice +=$arr_session[$i]['product_quantity']*$arr_session[$i]['product_price'];
    }
    $taxValueCheck += ($taxValue * $totalPrice) / (float)100.0;
}
function create_new_user($db,$v_first_name,$v_last_name,$v_user_name,$v_password,&$v_temp,$delete = false){
    $v_inactive = $delete ? 1 : 0;
    $cls_tb_contact = new cls_tb_contact($db);
    $v_contact_id = $cls_tb_contact->select_next("contact_id");
    $v_temp = $v_contact_id;
    $cls_tb_contact->set_contact_id($v_contact_id);
    $cls_tb_contact->set_first_name($v_first_name);
    $cls_tb_contact->set_last_name($v_last_name);
    $cls_tb_contact->set_user_email($v_user_name);
    $cls_tb_contact->set_email($v_user_name);
//    $cls_tb_contact->set_deleted($delete);
    $cls_tb_contact->set_inactive($v_inactive);
    $cls_tb_contact->set_user_password(md5($v_password));
    $v_result = $cls_tb_contact->insert();
    return $v_result;
}
function create_user_session($v_result,$v_contact_id,$v_first_name,$v_last_name,$v_user_name,$v_password,$delete = false){
    $arr_customer['_id'] = $v_result;
    $arr_customer['contact_id'] = $v_contact_id;
    $arr_customer['first_name'] = $v_first_name;
    $arr_customer['middle_name'] = '';
    $arr_customer['last_name'] = $v_last_name;
    $arr_customer['full_name'] = $v_last_name." ".$v_first_name;
    $arr_customer['addresses'] = array();
    $arr_customer['address_default'] = 0;
    $arr_customer['company'] = '';
    $arr_customer['deleted'] = $delete;
    $arr_customer['direct_dia'] = '';
    $arr_customer['email'] = $v_user_name;
    $arr_customer['extension_no'] = '';
    $arr_customer['fax'] = '';
    $arr_customer['is_customer'] = 1;
    $arr_customer['is_employee'] = 0;
    $arr_customer['mobile'] = '';
    $arr_customer['no'] = '';
    $arr_customer['position'] = '';
    $arr_customer['title'] = '';
    $arr_customer['user_password'] = md5($v_password);
    $arr_customer['user_name'] = '';
    $arr_customer['user_status'] = 0;
    $arr_customer['user_type'] = 3;
    return $arr_customer;
}
?>