<?php
function apply_borders_to_cell_full(PHPExcel_Worksheet & $sheet, $cell, $border=PHPExcel_Style_Border::BORDER_THIN, $fill_color='', $text_color=''){
    if($border=='') $border=PHPExcel_Style_Border::BORDER_THIN;
    $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle($border);
    $sheet->getStyle($cell)->getBorders()->getBottom()->setBorderStyle($border);
    $sheet->getStyle($cell)->getBorders()->getLeft()->setBorderStyle($border);
    $sheet->getStyle($cell)->getBorders()->getRight()->setBorderStyle($border);
    if($fill_color!=''){
        $sheet->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet->getStyle($cell)->getFill()->getStartColor()->setRGB($fill_color);
    }
    if($text_color!=''){
        $sheet->getStyle($cell)->getFont()->getColor()->applyFromArray(array("rgb"=>$text_color));
    }
}
function create_one_cell_full(PHPExcel_Worksheet & $sheet, $col, $row, $value, $align, $bold=false, $wrap=false, $width=0, $format=PHPExcel_Style_NumberFormat::FORMAT_GENERAL, $border=1, $fill_color='', $text_color = '', $p_rotation=0, array $arr_comment = array() ){
    //PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
    if($format=='') $format=PHPExcel_Style_NumberFormat::FORMAT_GENERAL;
    $cell = excel_col($col).$row;
    $left=PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
    $right=PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
    $center=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
    $vertical=PHPExcel_Style_Alignment::VERTICAL_CENTER;
    if($border==1){
        if($fill_color!='')
            apply_borders_to_cell_full($sheet, $cell,'',$fill_color);
        else
            apply_borders_to_cell_full($sheet, $cell);
    }
    if($align=='center'){
        $sheet->getStyle($cell)->getAlignment()->setHorizontal($center);
    }else if($align=='left'){
        $sheet->getStyle($cell)->getAlignment()->setHorizontal($left);
    }else if($align=='right'){
        $sheet->getStyle($cell)->getAlignment()->setHorizontal($right);
    }
    $sheet->getStyle($cell)->getAlignment()->setVertical($vertical);
    if($wrap) $sheet->getStyle($cell)->getAlignment()->setWrapText(true);
    //if(is_numeric($value))
    $sheet->getStyle($cell)->getNumberFormat()->setFormatCode($format);
    if($text_color!='') $sheet->getStyle($cell)->getFont()->getColor()->applyFromArray(array("rgb" => $text_color));
    //else
    //if(isset($arr_value[1]))
    //	$sheet->setCellFormula($cell,$value);
    //else
    $sheet->setCellValue($cell,$value);
    if($bold) $sheet->getStyle($cell)->getFont()->setBold(true);

    if($p_rotation!=0) $sheet->getStyle($cell)->getAlignment()->setTextRotation($p_rotation);
    if($width>0) $sheet->getColumnDimension(excel_col($col))->setWidth($width);

    if(count($arr_comment)>0){
        $sheet->getComment($cell)->setAuthor('AnvyDigital');
        for($i=0; $i<count($arr_comment);$i++){
            $v_text = $arr_comment[$i]['text'];
            $v_bold = isset($arr_comment[$i]['bold']) && $arr_comment[$i]['bold'];
            $v_italic = isset($arr_comment[$i]['italic']) && $arr_comment[$i]['italic'];
            $objCommentRichText  = $sheet->getComment($cell)->getText()->createTextRun($v_text);
            if($v_bold){
                $objCommentRichText->getFont()->setBold(true);
            }
            if($v_italic)
                $objCommentRichText->getFont()->setItalic(true);
            if($i<count($arr_comment)-1)
                $sheet->getComment($cell)->getText()->createTextRun("\r\n");
        }
    }
}


function excel_col($p_num){
    $numeric = ($p_num - 1) % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval(($p_num - 1) / 26);
    if ($num2 > 0) {
        return excel_col($num2) . $letter;
    } else {
        return $letter;
    }
}

function col_excel($p_excel_col){
    $v_cols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $v_excel_col = strtoupper($p_excel_col);
    $v_excel_col = preg_replace('/[^A-Z]/','', $v_excel_col);
    $v_len = strlen($v_cols);
    $v_return = 0;
    $v_p_excel = strlen($v_excel_col);
    $j=0;
    for($i=$v_p_excel-1;$i>=0;$i--){
        $c = substr($v_excel_col, $i, 1);
        $p = strpos($v_cols, $c);
        if($p!==false){
            if($i==$v_p_excel-1){
                $p++;
                $v_return += $p;
            }else{
                $p++;
                $pp = pow($v_len, $j) * $p;
                $v_return += $pp;
            }
        }
        $j++;
    }
    return $v_return;
}
?>