<?php
function handler($v_cls_tag,$v_cls_product,$p_package_id,$arr_package_content){
    $arr_return = array();
    // not use class tag, product or package id with golf but may use for other product
    for($i=0;$i<count($arr_package_content);$i++){
        $v_product_id = isset($arr_package_content[$i]['product_id'])?$arr_package_content[$i]['product_id']:0;
        $v_product_image = isset($arr_package_content[$i]['package_image'])?$arr_package_content[$i]['package_image']:'images/product_sample.jpg';
        $v_product_image_dir = isset($arr_package_content[$i]['saved_dir'])?$arr_package_content[$i]['saved_dir']:'images/product_sample.jpg';
        $v_product_name = isset($arr_package_content[$i]['package_name'])?$arr_package_content[$i]['package_name']:'';
        $v_product_quantity = $arr_package_content[$i]['quantity']?$arr_package_content[$i]['quantity']:1;
        $v_product_price = isset($arr_package_content[$i]['price'])?$arr_package_content[$i]['price']:0;
        $v_width = isset($arr_package_content[$i]['width'])?$arr_package_content[$i]['width']:0;
        $v_length = isset($arr_package_content[$i]['length'])?$arr_package_content[$i]['length']:0;
        $v_usize = isset($arr_package_content[$i]['usize'])?$arr_package_content[$i]['usize']:'ft';
        $arr_template = isset($arr_package_content[$i]['template'])?$arr_package_content[$i]['template']:array();
        $arr_content = array();
        $arr_content[] = array(
            'product_id'=>$v_product_id
            ,'design_name'=>$v_product_name
            ,'product_image'=>$v_product_image
            ,'product_image_dir'=>$v_product_image_dir
            ,'product_design_id'=>0
            ,'product_template_id'=>0
            ,'product_design_quantity'=>0
            ,"date_design"=>date('Y-m-d H:i:s')
            ,"date_modified"=>''
            ,"width"=>$v_width
            ,"length"=>$v_length
            ,"complete"=>-1
        );
        $arr_return[] = array(
            "package_id"=>$p_package_id
            ,"product_id"=>$v_product_id
            ,"product_image"=>$v_product_image
            ,"product_image_dir"=>$v_product_image_dir
            ,"product_name"=>$v_product_name
            ,"product_quantity"=>$v_product_quantity
            ,"product_price"=>$v_product_price
            ,"product_template"=>$arr_template
            ,"width"=>$v_width
            ,"length"=>$v_length
            ,"usize"=>$v_usize
            ,"had_design"=>0
            ,"is_designed"=>$arr_content
            ,"date_create"=>date('Y-m-d H:i:s')
            ,"complete_design"=>-1
        );
    }
    return $arr_return;
}
function handle_design_one_product($p_product_id,$p_template_id,$p_old_design_id,$p_design_id,$p_design_quantity,$p_image_file,$p_image_dir,$p_design_name,$arr_package_content){
    // handle design on session
    // $arr_package_content here is after handler. Not origin packet
    $arr_return = array();
    settype($p_product_id,"int");
    settype($p_design_quantity,"int");
    for($i=0;$i<count($arr_package_content);$i++){
        if($p_product_id != $arr_package_content[$i]['product_id'] ) continue;
        $arr_package_content[$i]['had_design'] = $p_design_quantity;
        if(check_if_designed($arr_package_content,$p_product_id)){
            $arr_is_designed = $arr_package_content[$i]['is_designed'];
            $arr_package_content[$i]['is_designed'] = handle_create_edit_design(false,$arr_is_designed,$p_product_id,$p_image_file,$p_image_dir,$p_old_design_id,$p_design_id,$p_design_quantity,$p_template_id,$p_design_name);
                //$arr_is_designed;
        }else{
            $arr_package_content[$i]['is_designed'] = handle_create_edit_design(true,array(),$p_product_id,$p_image_file,$p_image_dir,$p_old_design_id,$p_design_id,$p_design_quantity,$p_template_id,$p_design_name);
            /*
             $arr_package_content[$i]['is_designed'] = array(
                'product_id'=>$p_product_id
                ,'product_image'=>$p_image_file
                ,'product_image_dir'=>$p_image_dir
                ,'product_design_id'=>$p_design_id
                ,'product_design_quantity'=>$p_design_quantity
                ,"date_design"=>date('Y-m-d H:i:s')
                ,"date_modified"=>''
            );
             */
        }
        if($arr_package_content[$i]['product_quantity'] == $p_design_quantity){
            $arr_package_content[$i]['complete_design'] = 1;
        }
    }
    return $arr_return;
}
function check_if_complete_all_item($arr_package_content){
    for($i=0;$i<count($arr_package_content);$i++){
        if(count($arr_package_content[$i]['complete_design']) <=0) return false;
    }
    return true;
}
function check_if_designed($arr_package_content,$p_product_id){
    // $arr_package_content here is after handler. Not origin packet
    for($i=0;$i<count($arr_package_content);$i++){
        if($arr_package_content[$i]['product_id'] != $p_product_id) continue;
        if(count($arr_package_content[$i]['is_designed']) >0) return true;
    }
    return false;
}
function check_have_old_design($arr_design,$p_product_id,$p_design_id){

}
function handle_create_edit_design($p_create=true,$arr_content,$p_product_id,$p_image_file,$p_image_dir,$p_old_design_id,$p_design_id,$p_design_quantity,$p_template_id,$p_design_name){
    // $arr_content here is what customer design
    if($p_create){
        // create new design content
        $arr_content[] = array(
            'product_id'=>$p_product_id
            ,'design_name'=>$p_design_name
            ,'product_image'=>$p_image_file
            ,'product_image_dir'=>$p_image_dir
            ,'product_design_id'=>$p_design_id
            ,'product_template_id'=>$p_template_id
            ,'product_design_quantity'=>$p_design_quantity
            ,"date_design"=>date('Y-m-d H:i:s')
            ,"date_modified"=>''
        );
    }else{
        // edit design content
        $p_check_existed_design = false;;
        $ar_design_id_temp = array();
        for($i=0;$i<count($arr_content);$i++){
            $v_product_loop = $arr_content[$i]['product_id'];
            if($v_product_loop != $p_product_id) continue;
            $v_design_id_loop = $arr_content[$i]['product_design_id'];

            if($p_old_design_id == $v_design_id_loop){
                // if design product with different design
                $p_check_existed_design = true;
                $arr_content[$i]['product_image'] = $p_image_file;
                $arr_content[$i]['product_image_dir'] = $p_image_dir;
                $arr_content[$i]['product_design_id'] = $p_design_id;
                $arr_content[$i]['product_template_id'] = $p_template_id;
                $arr_content[$i]['product_design_quantity'] = $p_design_quantity;
                $arr_content[$i]['date_modified'] = date('Y-m-d H:i:s');
            }
        }
        if(!$p_check_existed_design){
            $arr_content[] = array(
                'product_id'=>$p_product_id
                ,'design_name'=>$p_design_name
                ,'product_image'=>$p_image_file
                ,'product_image_dir'=>$p_image_dir
                ,'product_design_id'=>$p_design_id
                ,'product_template_id'=>$p_template_id
                ,'product_design_quantity'=>$p_design_quantity
                ,"date_design"=>date('Y-m-d H:i:s')
                ,"date_modified"=>''
            );
        }
    }
    return $arr_content;
}
function check_is_login(){
    // check login before add to cart
    if(isset($_SESSION['ss_user'])){
        $arr_user = unserialize($_SESSION['ss_user']);
        if($arr_user['user_name']==''  || $arr_user['user_login']==0)
            return false;
        return true;
    }
    return false;
}
function save_design_bk($db,$arr_package_content,$p_user_id,$p_user_name,$p_company_id,$p_location_id,$p_print_cost=0,$p_markup_cost=0,$p_kitting_cost=0,$v_design_temp_id,$p_package_id,$p_design_name,$p_url_link,&$p_save_id=0){
    // add item to database before check-out(pay for order)
    // $arr_package_content here is after handler. Not origin packet
    add_class("cls_tb_design_temp");
    add_class("cls_tb_design_save");
    add_class("cls_tb_product");
    $v_total_amount = 0;
    $cls_tb_design_temp = new cls_tb_design_temp($db);
    $cls_tb_product = new cls_tb_product($db);

    $cls_tb_design_save = new cls_tb_design_save($db);
    if(!isset($_SESSION['ss_package']) ){
       // die(var_dump($arr_package_content));
        $v_save_id = $cls_tb_design_save->select_next("design_save_id");
        //
        for($i=0;$i<count($arr_package_content);$i++){
            $v_product_id = $arr_package_content[$i]['product_id'];
            $v_packet_id = $arr_package_content[$i]['package_id'];
            $v_product_origin_quantity = $arr_package_content[$i]['product_quantity'];
            $v_product_price = $arr_package_content[$i]['product_price'];
            $v_product_width = $arr_package_content[$i]['width'];
            $v_product_length = $arr_package_content[$i]['length'];
            $v_product_usize = $arr_package_content[$i]['usize'];
            $arr_content = $arr_package_content[$i]['is_designed'];

            $v_package_dir = $arr_package_content[$i]['product_image_dir'];
            $v_package_image = $arr_package_content[$i]['product_image'];
            $v_package_name = $arr_package_content[$i]['product_name'];

            for($j=0;$j<count($arr_content);$j++){
                $v_design_name = $arr_content[$j]['design_name'];
                $v_design_id = $arr_content[$j]['product_design_id'];
                $v_template_id = $arr_content[$j]['product_template_id'];
                $v_product_saved_dir = $arr_content[$j]['product_image_dir'];
                $v_product_image = $arr_content[$j]['product_image'];
                $v_product_design_quantity = $arr_content[$j]['product_design_quantity'];
                $v_date_design = $arr_content[$j]['date_design'];
                $v_date_modify = $arr_content[$j]['date_modified'];
                $v_total_amount = $v_product_design_quantity * $v_product_price;

                if($v_design_temp_id <=0){
                    $v_design_temp_id_temp = $cls_tb_design_temp->select_next("design_temp_id");
                    $cls_tb_design_temp->set_design_temp_id($v_design_temp_id_temp);
                }

                $cls_tb_design_temp->set_package_dir($v_package_dir);
                $cls_tb_design_temp->set_package_image($v_package_image);
                $cls_tb_design_temp->set_package_name($v_package_name);
                $cls_tb_design_temp->set_product_price($v_product_price);
                $cls_tb_design_temp->set_company_id($p_company_id);
                $cls_tb_design_temp->set_date_design($v_date_design);
                $cls_tb_design_temp->set_date_modify($v_date_modify);
                $cls_tb_design_temp->set_design_id($v_design_id);
                $cls_tb_design_temp->set_design_name($v_design_name);
                $cls_tb_design_temp->set_image_design($v_product_image);
                $cls_tb_design_temp->set_saved_dir($v_product_saved_dir);
                $cls_tb_design_temp->set_template_id($v_template_id);
                $cls_tb_design_temp->set_product_id($v_product_id);
                $cls_tb_design_temp->set_package_id($v_packet_id);
                $cls_tb_design_temp->set_total_product_origin_quantity($v_product_origin_quantity);
                $cls_tb_design_temp->set_product_design_quantity($v_product_design_quantity);
                $cls_tb_design_temp->set_total_amount($v_total_amount);
                $cls_tb_design_temp->set_width($v_product_width);
                $cls_tb_design_temp->set_length($v_product_length);
                $cls_tb_design_temp->set_usize($v_product_usize);
                $cls_tb_design_temp->set_location_id($p_location_id);
                $cls_tb_design_temp->set_user_id($p_user_id);
                $cls_tb_design_temp->set_user_name($p_user_name);
                $cls_tb_design_temp->set_product_print_cost($p_print_cost);
                $cls_tb_design_temp->set_product_markup_cost($p_markup_cost);
                $cls_tb_design_temp->set_product_kitting_cost($p_kitting_cost);
                $cls_tb_design_temp->set_saved_id($v_save_id);
                if($v_design_temp_id <=0){
                    $cls_tb_design_temp->insert();
                }else{
                    $cls_tb_design_temp->update();
                }
            }
            $cls_tb_design_save->set_design_save_id($v_save_id);
            $cls_tb_design_save->set_package_id($p_package_id);
            $cls_tb_design_save->set_user_id($p_user_id);
            $cls_tb_design_save->set_template_id($v_design_temp_id);
            $cls_tb_design_save->set_company_id($p_company_id);
            $cls_tb_design_save->set_location_id($p_location_id);
            $cls_tb_design_save->set_user_name($p_user_name);
            $cls_tb_design_save->set_date_save(date('Y-m-d H:i:s'));
            $cls_tb_design_save->set_date_modify('');
            $cls_tb_design_save->set_design_name($p_design_name);
            $cls_tb_design_save->set_saved_dir($p_url_link);
            $cls_tb_design_save->set_complete(-1);
            $cls_tb_design_save->set_width($v_product_width);
            $cls_tb_design_save->set_length($v_product_length);
            $v_total_price = $cls_tb_product->select_scalar("default_price",array("product_id"=>$v_packet_id));
            $cls_tb_design_save->set_price($v_total_price);
            $cls_tb_design_save->insert();
            //$v_total_amount +=$v_product_price + $p_print_cost + $p_markup_cost + $p_kitting_cost;
        }
    }else{
        $v_save_id = $cls_tb_design_save->select_next("design_save_id");

        //
        $arr_package = unserialize($_SESSION['ss_package']);
        for($i=0;$i<count($arr_package);$i++){
            $v_product_id = $arr_package[$i]['product_id'];
            $v_packet_id = $arr_package[$i]['package_id'];
            $v_product_origin_quantity = $arr_package[$i]['product_quantity'];
            $v_product_price = $arr_package[$i]['product_price'];
            $v_product_width = $arr_package[$i]['width'];
            $v_product_length = $arr_package[$i]['length'];
            $v_product_usize = $arr_package[$i]['usize'];
            $arr_content = $arr_package[$i]['is_designed'];

            $v_package_dir = $arr_package[$i]['product_image_dir'];
            $v_package_image = $arr_package[$i]['product_image'];
            $v_package_name = $arr_package[$i]['product_name'];
            for($j=0;$j<count($arr_content);$j++){
                $v_design_name = $arr_content[$j]['design_name'];
                $v_design_id = $arr_content[$j]['product_design_id'];
                $v_template_id = $arr_content[$j]['product_template_id'];
                $v_product_saved_dir = $arr_content[$j]['product_image_dir'];
                $v_product_image = $arr_content[$j]['product_image'];
                $v_product_design_quantity = $arr_content[$j]['product_design_quantity'];
                $v_date_design = $arr_content[$j]['date_design'];
                $v_date_modify = $arr_content[$j]['date_modified'];
                $v_total_amount = $v_product_design_quantity * $v_product_price;

                if($v_design_temp_id <=0){
                    $v_design_temp_id_temp = $cls_tb_design_temp->select_next("design_temp_id");
                    $cls_tb_design_temp->set_design_temp_id($v_design_temp_id_temp);
                }
                $cls_tb_design_temp->set_package_dir($v_package_dir);
                $cls_tb_design_temp->set_saved_id($v_save_id);
                $cls_tb_design_temp->set_package_image($v_package_image);
                $cls_tb_design_temp->set_package_name($v_package_name);
                $cls_tb_design_temp->set_product_price($v_product_price);
                $cls_tb_design_temp->set_company_id($p_company_id);
                $cls_tb_design_temp->set_date_design($v_date_design);
                $cls_tb_design_temp->set_date_modify($v_date_modify);
                $cls_tb_design_temp->set_design_id($v_design_id);
                $cls_tb_design_temp->set_design_name($v_design_name);
                $cls_tb_design_temp->set_image_design($v_product_image);
                $cls_tb_design_temp->set_saved_dir($v_product_saved_dir);
                $cls_tb_design_temp->set_template_id($v_template_id);
                $cls_tb_design_temp->set_product_id($v_product_id);
                $cls_tb_design_temp->set_package_id($v_packet_id);
                $cls_tb_design_temp->set_total_product_origin_quantity($v_product_origin_quantity);

                $cls_tb_design_temp->set_product_design_quantity($v_product_design_quantity);
                $cls_tb_design_temp->set_total_amount($v_total_amount);
                $cls_tb_design_temp->set_width($v_product_width);
                $cls_tb_design_temp->set_length($v_product_length);
                $cls_tb_design_temp->set_usize($v_product_usize);
                $cls_tb_design_temp->set_location_id($p_location_id);
                $cls_tb_design_temp->set_user_id($p_user_id);
                $cls_tb_design_temp->set_user_name($p_user_name);
                $cls_tb_design_temp->set_product_print_cost($p_print_cost);
                $cls_tb_design_temp->set_product_markup_cost($p_markup_cost);
                $cls_tb_design_temp->set_product_kitting_cost($p_kitting_cost);
                if($v_design_temp_id <=0){
                    $cls_tb_design_temp->insert();
                }else{
                    $cls_tb_design_temp->update();
                }
            }
            $cls_tb_design_save->set_design_save_id($v_save_id);
            $cls_tb_design_save->set_package_id($p_package_id);
            $cls_tb_design_save->set_user_id($p_user_id);
            $cls_tb_design_save->set_template_id($v_design_temp_id);
            $cls_tb_design_save->set_company_id(0);
            $cls_tb_design_save->set_location_id(0);
            $cls_tb_design_save->set_user_name($p_user_name);
            $cls_tb_design_save->set_date_save(date('Y-m-d H:i:s'));
            $cls_tb_design_save->set_date_modify(0);
            $cls_tb_design_save->set_design_name($p_design_name);
            $cls_tb_design_save->set_saved_dir($p_url_link);
            $cls_tb_design_save->set_complete(-1);
            $cls_tb_design_save->set_width($v_product_width);
            $cls_tb_design_save->set_length($v_product_length);
            $v_total_price = $cls_tb_product->select_scalar("default_price",array("product_id"=>$v_packet_id));
            $cls_tb_design_save->set_price($v_total_price);
            $cls_tb_design_save->insert();
        }
    }
    $p_save_id = $v_save_id;
}
function save_design($db,$arr_package_content,$p_user_id,$p_user_name,$p_company_id,$p_location_id,$p_print_cost=0,$p_markup_cost=0,$p_kitting_cost=0,$v_design_temp_id,$p_package_id,$p_design_name,$p_url_link,&$p_save_id=0){
    // add item to database before check-out(pay for order)
    // $arr_package_content here is after handler. Not origin packet
    add_class("cls_tb_design_temp");
    add_class("cls_tb_design_save");
    add_class("cls_tb_product");
    $v_total_amount = 0;
    $cls_tb_design_temp = new cls_tb_design_temp($db);
    $cls_tb_product = new cls_tb_product($db);
    $v_save_id =0;
    $cls_tb_design_save = new cls_tb_design_save($db);
    if($p_save_id >0 ){
        $v_save_id = $cls_tb_design_save->select_next("design_save_id");
        $cls_tb_design_save->select_one(array("design_save_id"=>$p_save_id));
        $cls_tb_design_save->set_design_save_id($v_save_id);
        $cls_tb_design_save->set_design_name($p_design_name);
        $cls_tb_design_save->insert();
        $arr_select = $cls_tb_design_temp->select(array("saved_id"=>$p_save_id));
        foreach($arr_select as $arr){
            $arr_where = array("design_temp_id"=>(int)$arr['design_temp_id']);
            $v_new_id = $cls_tb_design_temp->select_next("design_temp_id");
            $cls_tb_design_temp->select_one($arr_where);
            $cls_tb_design_temp->set_design_temp_id($v_new_id);
            $cls_tb_design_temp->set_saved_id($v_save_id);
            $cls_tb_design_temp->insert();
        }
    }else if(isset($_SESSION['ss_package']) && $p_save_id <=0){
        $v_save_id = $cls_tb_design_save->select_next("design_save_id");
        //
        $arr_package = unserialize($_SESSION['ss_package']);
        for($i=0;$i<count($arr_package);$i++){
            $v_product_id = $arr_package[$i]['product_id'];
            $v_packet_id = $arr_package[$i]['package_id'];
            $v_product_origin_quantity = $arr_package[$i]['product_quantity'];
            $v_product_price = $arr_package[$i]['product_price'];
            $v_product_width = $arr_package[$i]['width'];
            $v_product_length = $arr_package[$i]['length'];
            $v_product_usize = $arr_package[$i]['usize'];
            $arr_content = $arr_package[$i]['is_designed'];

            $v_package_dir = $arr_package[$i]['product_image_dir'];
            $v_package_image = $arr_package[$i]['product_image'];
            $v_package_name = $arr_package[$i]['product_name'];
            for($j=0;$j<count($arr_content);$j++){
                $v_design_name = $arr_content[$j]['design_name'];
                $v_design_id = $arr_content[$j]['product_design_id'];
                $v_template_id = $arr_content[$j]['product_template_id'];
                $v_product_saved_dir = $arr_content[$j]['product_image_dir'];
                $v_product_image = $arr_content[$j]['product_image'];
                $v_product_design_quantity = $arr_content[$j]['product_design_quantity'];
                $v_date_design = $arr_content[$j]['date_design'];
                $v_date_modify = $arr_content[$j]['date_modified'];
                $v_total_amount = $v_product_design_quantity * $v_product_price;

                if($v_design_temp_id <=0){
                    $v_design_temp_id_temp = $cls_tb_design_temp->select_next("design_temp_id");
                    $cls_tb_design_temp->set_design_temp_id($v_design_temp_id_temp);
                }
                $cls_tb_design_temp->set_package_dir($v_package_dir);
                $cls_tb_design_temp->set_saved_id($v_save_id);
                $cls_tb_design_temp->set_package_image($v_package_image);
                $cls_tb_design_temp->set_package_name($v_package_name);
                $cls_tb_design_temp->set_product_price($v_product_price);
                $cls_tb_design_temp->set_company_id($p_company_id);
                $cls_tb_design_temp->set_date_design($v_date_design);
                $cls_tb_design_temp->set_date_modify($v_date_modify);
                $cls_tb_design_temp->set_design_id($v_design_id);
                $cls_tb_design_temp->set_design_name($v_design_name);
                $cls_tb_design_temp->set_image_design($v_product_image);
                $cls_tb_design_temp->set_saved_dir($v_product_saved_dir);
                $cls_tb_design_temp->set_template_id($v_template_id);
                $cls_tb_design_temp->set_product_id($v_product_id);
                $cls_tb_design_temp->set_package_id($v_packet_id);
                $cls_tb_design_temp->set_total_product_origin_quantity($v_product_origin_quantity);

                $cls_tb_design_temp->set_product_design_quantity($v_product_design_quantity);
                $cls_tb_design_temp->set_total_amount($v_total_amount);
                $cls_tb_design_temp->set_width($v_product_width);
                $cls_tb_design_temp->set_length($v_product_length);
                $cls_tb_design_temp->set_usize($v_product_usize);
                $cls_tb_design_temp->set_location_id($p_location_id);
                $cls_tb_design_temp->set_user_id($p_user_id);
                $cls_tb_design_temp->set_user_name($p_user_name);
                $cls_tb_design_temp->set_product_print_cost($p_print_cost);
                $cls_tb_design_temp->set_product_markup_cost($p_markup_cost);
                $cls_tb_design_temp->set_product_kitting_cost($p_kitting_cost);
                if($v_design_temp_id <=0){
                    $cls_tb_design_temp->insert();
                }else{
                    $cls_tb_design_temp->update();
                }
            }
            $cls_tb_design_save->set_design_save_id($v_save_id);
            $cls_tb_design_save->set_package_id($p_package_id);
            $cls_tb_design_save->set_user_id($p_user_id);
            $cls_tb_design_save->set_template_id($v_design_temp_id);
            $cls_tb_design_save->set_company_id(0);
            $cls_tb_design_save->set_location_id(0);
            $cls_tb_design_save->set_user_name($p_user_name);
            $cls_tb_design_save->set_date_save(date('Y-m-d H:i:s'));
            $cls_tb_design_save->set_date_modify(0);
            $cls_tb_design_save->set_design_name($p_design_name);
            $cls_tb_design_save->set_saved_dir($p_url_link);
            $cls_tb_design_save->set_complete(-1);
            $cls_tb_design_save->set_width($v_product_width);
            $cls_tb_design_save->set_length($v_product_length);
            $v_total_price = $cls_tb_product->select_scalar("default_price",array("product_id"=>$v_packet_id));
            $cls_tb_design_save->set_price($v_total_price);
            $cls_tb_design_save->insert();
        }
    }
    $p_save_id = $v_save_id;
}
?>