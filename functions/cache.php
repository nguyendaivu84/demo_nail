<?php
function open_cache($path,$name){
    $url = $_SERVER["SCRIPT_NAME"]."/".$name;
    $break = Explode('/', $url);
    $file = $break[count($break) - 1];
    if($file=='') $file = $break[count($break) - 2];
    $cachefile = $path.'cached-'.$file.'.html';
    $cachetime = 18000;
    if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {
        include($cachefile);
        exit;
    }
    return $cachefile;
}

function create_cache($cachefile){
    $cached = fopen($cachefile, 'w');
    fwrite($cached, ob_get_contents());
    fclose($cached);
    ob_end_flush();
}