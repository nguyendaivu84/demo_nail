<?php
$v_sval = 1;
include 'const.php';
include 'config.php';
include 'connect.php';
include 'functions/index.php';
add_class('cls_tb_company');
add_class('cls_tb_location');
add_class('cls_tb_contact');

$cls_tb_company = new cls_tb_company($db);
$cls_tb_location = new cls_tb_location($db);
$cls_tb_contact = new cls_tb_contact($db);

$v_company_id = 10007; // Parkland
$arr_location = $cls_tb_location->select(array('company_id'=>(int)$v_company_id));
$v_total_location = 0;
$v_total_contact = 0;
foreach($arr_location as $arr)
{
    $v_location_id = isset($arr['location_id']) ? $arr['location_id'] :0;
    $arr_contact = $cls_tb_contact->select(array('location_id'=>(int)$v_location_id));

    foreach($arr_contact as $a){
        $v_contact_id = isset($a['contact_id']) ? $a['contact_id'] : 0;
        $v_result = $cls_tb_location->update_field('main_contact',$v_contact_id,array("location_id"=>$v_location_id));
        if($v_result==true) $v_total_contact++;
    }
    $v_total_location++;
}
echo 'Total location : '. $v_total_location .'<br>';
echo 'Total contact  : '. $v_total_contact .' have updated<br>';


?>