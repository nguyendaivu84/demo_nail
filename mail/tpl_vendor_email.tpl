<body>
Dear [@VENDOR_EMAIL]<br />
We would like to buy list of material from you:
<p class="highlightTitle">
    <span class="note"> Order Information </span>
</p>
<div class="boxContent" >
    <table class="list_table"  cellpadding="3" cellspacing="1" border="1" align="center">
        <tr>
            <th>Product's name</th>
            <th>Product's short description</th>
            <th>Product's long description</th>
            <th>Date required(at least 3 hours earlier)</th>
        </tr>
        [@TABLE_VENDOR]
    </table>
</div>
<p class="Clear"></p>
<div class="boxContent" >
    If you have any questions, please contact us at the address shown below <br />
</div>
<p class="Clear"></p>
<div class="signatures">
    <img src="[@URL]/images/icons/logo.png" alt="">
    <br />Anvydigital Inc  <br />
    No. 103, 3016-10 Avenue N.E. <br />
    Calgary Alberta, Canada T2A6A3 <br />
    Tel: 403.291.2244 <br />
    Fax: 403.291.2246 <br />
    Email: [@SUPPORT_EMAIL] <br />
</div>
</body>
</html>
