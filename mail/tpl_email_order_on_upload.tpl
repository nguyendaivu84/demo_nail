<p>Dear [@ORDER_USER_CREATE]<br />
</p>
<p><span style="font-size:small;">Your order has been uploaded successful </span><br />
    <span style="font-size:small;">You need to log in and submit this order.</span><br />
</p>
<p><span class="note" style="color:#ff3333;"><strong>ORDER INFORMATION</strong></span> </p>
<div class="boxContent"><span style="font-size:small;">Order No.: [@ORDER_ID]</span><br />
    <span style="font-size:small;">PO#: [@ORDER_PO]</span><br />
    <span style="font-size:small;">Order Reference:[@ORDER_REF]</span><br />
    <span style="font-size:small;">Order Date: [@ORDER_DATE]</span><br />
    <span style="font-size:small;">Ordered by: [@ORDER_USER_CREATE]</span></div>
<p class="Clear"></p>
<div>If you have any questions, please contact [@SUPPORT_EMAIL] </div>
<p></p>
<div><img alt="" src="[@URL]images/icons/logo.png" /><strong>Anvydigital </strong><br />
    <span style="font-size:x-small;color:#330033;">No. 103, 3016-10 Avenue N.E. </span><br />
    <span style="font-size:x-small;color:#330033;">Calgary Alberta, Canada T2A6A3 </span><br />
    <span style="font-size:x-small;color:#330033;">Tel: 403.291.2244 </span><br />
    <span style="font-size:x-small;color:#330033;">Fax: 403.291.2246 </span><br />
    <span style="font-size:x-small;color:#330033;">Email: [@SUPPORT_EMAIL] </span></div>
