<body>
Dear [@ORDER_USER_CREATE]<br />
Your order has been put <b><span style="color:Red"> On Hold </span> </b>. <br>
<br>

<p class="highlightTitle">
    <span class="note"> Order Information </span>
</p>
<div class="boxContent" >
    Order No.:  [@ORDER_ID]<br />
    PO#: [@ORDER_PO]<br />
    Order Reference:[@ORDER_REF]<br />
    Order Date: [@ORDER_DATE]<br />
    Ordered by: [@ORDER_USER_CREATE]<br /><br />
    For more information click <a href="[@SUPORT_URL]">HERE</a>
</div>
<p class="Clear"></p>
<div class="boxContent" >
    If you have any questions, please contact us at the address shown below <br />
</div>
<p class="Clear"></p>
<div class="signatures">
    <img src="[@URL]/images/icons/logo.png" alt="">
    <br />Anvydigital Inc  <br />
    No. 103, 3016-10 Avenue N.E. <br />
    Calgary Alberta, Canada T2A6A3 <br />
    Tel: 403.291.2244 <br />
    Fax: 403.291.2246 <br />
    Email: [@SUPPORT_EMAIL] <br />
</div>
</body>
</html>
  