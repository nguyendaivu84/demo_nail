<div id=":42z" class="ii gt m13ba1580111fc4af adP adO"><div id=":430" class="a3s" style="overflow: hidden;"><u></u>
        <div>
            <p>
                Hi <strong>[@FULL_NAME]</strong>,</p>
            <p>
                Wellcome to
                <a href="[@URL]" style="color:Red" target="_blank">[@SITE_NAME]</a></p>
                <br >
                Your default password is :<strong>[@PASSWORD]</strong>
            <ul>
                <li>Please confirm your email and become member of
                    <a href="[@URL]" style="color:Red" target="_blank">[@SITE_NAME]</a><br>
                    click on the link below: <b>
                        <a href="[@CONFIRM_URL]" target="_blank"><u>Confirm email</u></a></b> </li>
            </ul>
            <p class="Clear"></p>
            <div class="boxContent" >
                If you have any questions, please contact us at the address shown below <br />
            </div>
            <p class="Clear"></p>
            <div class="signatures">
                <img src="[@URL]/images/icons/logo.png" alt="">
                <br /> <br />Anvydigital Inc <br />
                No. 103, 3016-10 Avenue N.E. <br />
                Calgary Alberta, Canada T2A6A3 <br />
                Tel: 403.291.2244 <br />
                Fax: 403.291.2246 <br />
                Email: [@SUPPORT_EMAIL] <br />
            </div>
        </div>
    </div>
</div>