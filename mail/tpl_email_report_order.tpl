<p>Dear [@SUPPORT_EMAIL]<br />
</p>
<p><span style="font-size:small;">User [@USER_CREATE_ERROR_ORDER] has used quick tools to create new order </span><br />
<span style="font-size:small;">You need to log in to check this order and check price immediately.</span><br />
</p>
<p><span class="note" style="color:#ff3333;"><strong>ORDER INFORMATION</strong></span> </p>
<div class="boxContent"><span style="font-size:small;">Order No.: [@ORDER_ID]</span>
 <br />
<span style="font-size:small;">PO#: [@ORDER_PO]</span>
 <br />
<span style="font-size:small;">Order Reference:[@ORDER_REF]</span>
 <br />
<span style="font-size:small;">Order Date: [@ORDER_DATE]</span>
</div>
<p class="Clear"></p>
<p><span class="note" style="color:#ff3333;"><strong>ORDER INFORMATION</strong></span> </p>
[@TABLE_PRODUCT_NOT_FOUND]
[@TABLE_LOCATION_NOT_FOUND_ADDRESS]
<p class="Clear"></p>
<div><img alt="" src="[@URL]images/icons/logo.png" /><strong>Anvydigital </strong>
 <br />
<span style="font-size:x-small;color:#330033;">No. 103, 3016-10 Avenue N.E. </span>
 <br />
<span style="font-size:x-small;color:#330033;">Calgary Alberta, Canada T2A6A3 </span>
 <br />
<span style="font-size:x-small;color:#330033;">Tel: 403.291.2244 </span>
 <br />
<span style="font-size:x-small;color:#330033;">Fax: 403.291.2246 </span>
</div>
