<div class="company">
    <h4>Anvy Inc. Canada Headquarters</h4>
    <p>No. 103, 3016-10 Avenue N.E.<br/>Calgary Alberta, Canada T2A 6A3</p>
    <div class="space">
        <p>tel: <span class="bold">403.291.2244</span><br/>fax: 403.291.2246<br/><a href="mailto:[@SUPPORT_EMAIL]">[@SUPPORT_EMAIL]</a></p>
    </div>
</div>
<div class="company rs_company">
    <h4>Anvy Inc. US Headquarters</h4>
    <p>15707 Banty Fall Ct<br/>Houton TX, USA 77068</p>
    <div class="space">
        <p>tel: <span class="bold">1.866.912.2689</span><br/>fax: 403.291.2246<br/><a href="mailto:[@SUPPORT_EMAIL]">[@SUPPORT_EMAIL]</a></p>
    </div>
</div>