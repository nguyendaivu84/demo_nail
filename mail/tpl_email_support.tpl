<body>
<div style="border: 1px solid;margin: auto auto;margin-top: 100px;width: 55%;background:rgba(231, 231, 231, 0.75);border-color: rgba(0, 0, 0, 0.298039);-webkit-box-shadow: 1px 1px 7px 1px rgba(128, 128, 128, 0.298039);box-shadow: 1px 1px 7px 1px rgba(128, 128, 128, 0.298039);border-radius:4px;">
    <form method="post" action="" id="user_registration" name="user_registration"style="margin: 0 auto;background-color: #fff;width: 90%;">
        <h1 style="color:#08c;text-align:center;font-size:43pt">Support Message </h1>
        <h4 style="clear: both;margin-left:25px">
            [@TITLE_MESSAGE]
        </h4>
        <p style="clear: both;margin-left:25px">
            <label for="pass" style="display: block;float: left;width: 118px;font-family:Time New Roman;margin: 0px 10px 0px 5px;text-align: left;line-height: 1em;font: bold 14px arial;color: #08c;font-weight: 900;">At:</label>
            [@DATE_SEND]
        </p>
        <br />
        <p style="clear: both;margin-left:25px">
            <label for="pass" style="display: block;float: left;width: 118px;font-family:Time New Roman;margin: 0px 10px 0px 5px;text-align: left;line-height: 1em;font: bold 14px arial;color: #08c;font-weight: 900;">Customer:</label>
            [@USER_NAME]
        </p>
        <br />
        <p style="clear: both;margin-left:25px">
            <label for="pass2" style="display: block;float: left;width: 118px;font-family:Time New Roman;margin: 0px 10px 0px 5px;text-align: left;line-height: 1em;font: bold 14px arial;color: #08c;font-weight: 900;">From Company:</label>
            [@COMPANY_NAME]
        </p>
        <br />
        <p style="clear: both;margin-left:25px">
            <label for="pass2" style="display: block;float: left;width: 118px;font-family:Time New Roman;margin: 0px 10px 0px 5px;text-align: left;line-height: 1em;font: bold 14px arial;color: #08c;font-weight: 900;">Content:</label>
            [@CONTENT_MESSAGE]
        </p>
        <br />
        <h4 style="clear: both;margin-left:25px">
            Please reply to customer as soon as possible. Thanks you
        </h4>
        <p style="clear: both;"></p>
        <p class="Clear"></p>
        <div class="signatures" style="margin-bottom: 30px;padding-bottom: 20px;padding-left: 20px;">
            <img src="[@URL]/images/icons/logo.png" alt="">
            <br /><br />
            <br />Anvy Digital Imaging  <br />
            No. 103, 3016-10 Avenue N.E. <br />
            Calgary Alberta, Canada T2A6A3 <br />
            Tel: 403.291.2244 <br />
            Fax: 403.291.2246 <br />
        </div>
    </form>
</div>
</body>