<body>
Dear [@USER_FULL_NAME]<br />
[@REASON_CHANGE_PASSWORD]
<p class="Clear"></p>
<br>
<p class="highlightTitle">
    Your new password is<span ><b> [@PASSWORD_GENARATE] </b> </span>
</p>
<div class="boxContent" >
    [@DESCRIPTION]
</div>
<p class="Clear"></p>
<div class="boxContent" >
    If you have any questions, please contact us at the address shown below <br />
</div>
<p class="Clear"></p>
<div class="signatures">
    <img src="[@URL]/images/icons/logo.png" alt="">
    <br /> <br />Anvydigital Inc <br />
    No. 103, 3016-10 Avenue N.E. <br />
    Calgary Alberta, Canada T2A6A3 <br />
    Tel: 403.291.2244 <br />
    Fax: 403.291.2246 <br />
    Email: [@SUPPORT_EMAIL] <br />
</div>
</body>
