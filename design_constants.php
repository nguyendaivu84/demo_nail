<?php
if (!defined('ROOT_DIR')) define('ROOT_DIR', getcwd());
if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
define('UPLOAD_DIR', ROOT_DIR.DS.'upload');
define('ASSET_DIR', UPLOAD_DIR.DS.'assets');
define('DESIGN_DIR', ASSET_DIR.DS.'designs');
define('DESIGN_TEMPLATE_DIR', DESIGN_DIR.DS.'templates');//save images for template
define('DESIGN_THEME_DIR', DESIGN_DIR.DS.'themes');//save images for theme
define('DESIGN_FONT_DIR', DESIGN_DIR.DS.'fonts');//save font for template
define('DESIGN_TEMP_DIR', DESIGN_DIR.DS.'temps'); // save preview
define('DESIGN_DESIGN_DIR', DESIGN_DIR.DS.'designs'); // save images for design
define('DESIGN_FOTOLIA_DIR', DESIGN_DIR.DS.'fotolia'); // save download images from fotolia
define('DESIGN_STOCK_DIR', DESIGN_DIR.DS.'stocks'); // save stocks images
define('DESIGN_USER_DIR', DESIGN_DIR.DS.'users'); // save user upload images
define('DESIGN_DATA_DIR', DESIGN_DIR.DS.'data'); // contain necessary data for design, such as convert image
define('DESIGN_MAX_SIZE_UPLOAD', 2097152); // save user upload images
define('DESIGN_IMAGE_THUMB_SIZE', 200); // save user upload images
define('DESIGN_SITE_CODE', 'AV'); //
define('DESIGN_SIDE_FACE', 'admin_panel'); //
define('DESIGN_FOTOLIA_KEY', 'yfi64LivT5TeR6HDRPTrKobgLDDt8wOx');