<?php
if(!isset($v_sval)) die();
?>
<?php
$v_switch_user_id = isset($_GET['txt_user_id'])?$_GET['txt_user_id']:'0';
$v_switch = isset($_GET['sw'])?$_GET['sw']:'';
add_class('cls_tb_user');
$cls_tb_user = new cls_tb_user($db, LOG_DIR);
add_class('cls_tb_contact');
$cls_tb_contact = new cls_tb_contact($db, LOG_DIR);

add_class('cls_tb_role');
$cls_tb_role = new cls_tb_role($db,LOG_DIR);

add_class('cls_tb_location');
$cls_tb_location = new cls_tb_location($db,LOG_DIR);

add_class('cls_tb_company');
$cls_tb_company = new cls_tb_company($db,LOG_DIR);

$v_role_id = $cls_tb_role->select_scalar('role_id', array('role_key'=>'role_switch_user'));
if(isset($_SESSION['ss_product_tag'])) unset($_SESSION['ss_product_tag']);
if(isset($_SESSION['ss_user'])){
    $v_ss_user = $_SESSION['ss_user'];
    $arr_user = unserialize($v_ss_user);

    $v_company_id = (int)$cls_tb_user->select_scalar("company_id",array('user_id'=>(int) $v_switch_user_id));
    $v_company_code = $cls_tb_company->select_scalar("company_code",array("company_id"=>$v_company_id));

    $v_location_id = (int)$cls_tb_user->select_scalar("location_id",array('user_id'=>(int) $v_switch_user_id));

    $arr_location_banner_temp = $cls_tb_location->select_scalar("location_banner",array("location_id"=>$v_location_id));
    if($arr_location_banner_temp =='' || is_null($arr_location_banner_temp)) $arr_location_banner_temp = '';
    $arr_location_banner_temp = addslashes($arr_location_banner_temp);
    //if(!is_array($arr_location_banner_temp) || is_null($arr_location_banner_temp) || count($arr_location_banner_temp) <=0) $arr_location_banner_temp = array();
    //for($i=0;$i<count($arr_location_banner_temp);$i++){
        //$arr_location_banner_temp[$i] = (int) $arr_location_banner_temp[$i];
    //}


    $arr_user['company_code'] = $v_company_code;
    $arr_user['require_change_pass'] = 0;
    //die(var_dump($arr_user));
    $v_current_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
    $v_current_user_type = isset($arr_user['user_type'])?$arr_user['user_type']:100;
    settype($v_current_user_type,"int");
    if($v_current_user_name!=''){
        $v_current_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;
        $v_is_admin = is_admin() || is_admin_by_user($v_current_user_name);
        settype($v_switch_user_id, 'int');

        if($v_switch=='admin'){
            if(isset($_SESSION['ss_admin_user'])){
                if($v_switch_user_id>0){
                    $_SESSION['ss_user'] = $_SESSION['ss_admin_user'];
                    unset($_SESSION['ss_admin_user']);
                }else{
                    $arr_user['user_admin_type'] = 2;
                    $_SESSION['ss_user'] = serialize($arr_user);
                }
            }else{
                $arr_user['user_admin_type'] = 2;
                $_SESSION['ss_user'] = serialize($arr_user);
            }
        }else if($v_switch=='custom'){
            //if(isset($_SESSION['ss_last_company_id'])) unset($_SESSION['ss_last_company_id']);
            $v_allow_switch = false;
            if(!$v_is_admin){
                settype($v_role_id, 'int');
                if($v_role_id>0){
                    $arr_role = $cls_tb_user->select_scalar('user_role', array('user_id'=>$v_current_user_id));
                    if(in_array($v_role_id, $arr_role)){
                        $v_switch_user_type = (int)$cls_tb_user->select_scalar('user_type', array('user_id'=>$v_switch_user_id));
                        $v_allow_switch = $v_current_user_type<=$v_switch_user_type;
                    }
                }
            }else $v_allow_switch = $v_is_admin;
            if($v_allow_switch){
                $_SESSION['ss_admin_user'] = serialize($arr_user);
                $v_total = $cls_tb_user->select_one(array('user_id'=>(int) $v_switch_user_id));
                if($v_total==1){
                    $v_company_id = $cls_tb_user->get_company_id();
                    $v_company_code = $cls_tb_company->select_scalar("company_code",array("company_id"=>(int)$v_company_id));
                    $arr_tmp_user['user_id'] = (int) $cls_tb_user->get_user_id();
                    $arr_tmp_user['user_name'] = $cls_tb_user->get_user_name();
                    $arr_tmp_user['user_status'] = $cls_tb_user->get_user_status();
                    $arr_tmp_user['user_type'] = $cls_tb_user->get_user_type();
                    $arr_tmp_user['mongo_id'] = $cls_tb_user->get_mongo_id();
                    $arr_tmp_user['contact_id'] = $cls_tb_user->get_contact_id();
                    $arr_tmp_user['contact_name'] = $cls_tb_contact->get_full_name_contact($cls_tb_user->get_contact_id());
                    $arr_tmp_user['company_id'] = $v_company_id;
                    $arr_tmp_user['company_code'] = $v_company_code;
                    $arr_tmp_user['user_rule'] = $cls_tb_user->get_user_rule();
                    $arr_tmp_user['user_login'] = 1;
                    $arr_tmp_user['location_default']= $cls_tb_user->get_location_id();

                    $arr_tmp_user['location_banner']= $arr_location_banner_temp;

                    $arr_tmp_user['user_location_approve'] = $cls_tb_user->get_user_location_approve();
                    $arr_tmp_user['user_location_allocate'] = $cls_tb_user->get_user_location_allocate();
                    $arr_tmp_user['require_change_pass'] = 1;

                    config_user($arr_tmp_user);
                }
            }else{
                $arr_user['user_admin_type'] = $arr_user['user_type'];
                $v_company_id = $arr_user['company_id'];
                settype($v_company_id,"int");
                $v_company_code = $cls_tb_company->select_scalar("company_code",array("company_id"=>(int)$v_company_id));
                $arr_user['company_code'] = $v_company_code;
                $arr_user['location_banner']= $arr_location_banner_temp;
                $arr_user['require_change_pass'] = 0;
                $_SESSION['ss_user'] = serialize($arr_user);
            }
        }else{

        }
    }
}

if($v_switch=='admin'){
    redir(URL."admin");
}else{
    redir(URL."catalogue");
}
?>