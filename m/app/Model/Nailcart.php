<?php
require_once APP.'Model'.DS.'AppModel.php';
class Nailcart extends AppModel {
	public function __construct($db) {
		if(is_object($db)){
			$this->collection = $db->selectCollection('tb_nail_cart');
			$this->db = $db;
		}
	}
}