<?php
App::uses('AppController', 'Controller');
class SalesordersController extends AppController {
	var $name = 'Salesorders';
	public $helpers = array();
	var $modelName = 'Salesorder';

	public function beforeFilter() {
		parent::beforeFilter();
	}



	public function _create_task_default_SO($salesorder_id){
		// Kiem tra xem task default cho SO đã được tạo chưa
		$this->selectModel('Task');
		$arr_tmp = $this->Task->select_one(array('system_default' => true, 'our_rep_type' => 'contacts', 'salesorder_id' => new MongoId($salesorder_id)));
		if( !isset($arr_tmp['_id']) ){
			$arr_save = $this->_get_default($salesorder_id);
			$arr_save['system_default'] = true;
			$arr_save['our_rep_type'] = 'contacts';
			$this->Task->arr_default_before_save = $arr_save;
			if (!$this->Task->add()){
				echo 'Error: save new Task ' . $this->Task->arr_errors_save[1]; die;
			}
		}
	}
}