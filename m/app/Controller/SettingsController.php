<?php

App::uses('AppController', 'Controller');
App::import('Vendor','hook/hook');
class SettingsController extends AppController {

	public $helpers = array();
	var $modelName = 'Setting';
	public $layout = 'setup';

	public function beforeFilter() {
		parent::beforeFilter();
	}

	// ========== Admin page ============================================================================
	public function get_province_from_country($country_id) {
		$this->selectModel('Province');
		$this->set('arr_province', $this->Province->select_all(array(
			'arr_where' => array('country_id' => "CA"),
			'arr_field' => array('_id', 'key', 'name')
		)));
	}

	public function index() {
		if( !$this->Session->check('setup_remember_page') ){
			$this->list_and_menu();
		}
	}

	public function list_message() {
		$this->Session->write('setup_remember_page', 'list_message');
	}

	// public function convert() {
	// 	$this->selectModel('Permission');
	// 	$arr_setting = $this->Permission->select_all();
	// 	foreach ($arr_setting as $key => $value) {
	// 		$value['permission'] = array(
	// 			array('entry' => array(
	// 				array('name' => 'Add', 'codekey' => 'add', 'description' => '', 'deleted' => false),
	// 				array('name' => 'Delete', 'codekey' => 'delete', 'description' => '', 'deleted' => false),
	// 				array('name' => 'View', 'codekey' => 'view', 'description' => '', 'deleted' => false),
	// 				array('name' => 'Edit', 'codekey' => 'edit', 'description' => '', 'deleted' => false)
	// 			))
	// 		);
	// 		$this->Permission->save($value);
	// 	}
	// 	echo 'xong';
	// 	die;
	// }

	public function list_message_detail($message_type) {
		$this->selectModel('SettingMessage');
		$arr_setting = $this->SettingMessage->select_all(array(
			'arr_where' => array(
				'message_type' => $message_type,
			),
			'arr_order' => array('name' => 1)
		));
		$this->set('message_type', $message_type);
		$this->set('arr_setting', $arr_setting);
	}

	public function list_message_detail_auto_save() {

		if (!empty($this->data)) {

			$post = $this->data['Setting'];
			$arr_tmp = $post;
			$arr_save = array();

			$arr_save['_id'] = $post['_id'];
			$arr_save['content'] = $post['content'];
			$arr_save['key'] = $post['key'];

			$this->selectModel('SettingMessage');

			if ($this->SettingMessage->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->SettingMessage->arr_errors_save[1];
			}
		}
		die;
	}

	public function update_system_admin($id, $value = "false") {

		$arr_save['_id'] = $id;
		$arr_save['system_admin'] = ($value == "true")?true:false;
		$this->selectModel('Setting');
		if ($this->Setting->save($arr_save)) {
			echo 'ok';
		} else {
			echo 'Error: ' . $this->Setting->arr_errors_save[1];
		}
		die;
	}

	public function list_message_detail_add($message_type) {
		$this->selectModel('SettingMessage');

		$arr_new_message = array('_id' => new MongoId()
			, 'content' => ''
			, 'key' => '(undefined)'
			, 'deleted' => false
			, 'message_type' => $message_type
		);
		$this->SettingMessage->collection->insert($arr_new_message);

		$this->list_message_detail($message_type);
		$this->render('list_message_detail');
	}

	public function delete_message_detail_null($message_type) {
		$this->selectModel('SettingMessage');
		$this->SettingMessage->collection->update(
				array(
			'content' => '',
			'key' => '(undefined)'
				), array('$set' => array(
				'deleted' => true
			)), array('multiple' => true));

		$this->list_message_detail($message_type);
		$this->render('list_message_detail');
	}

	/*	 * ********************************************Nguyen end code**************************************************** */
	public function list_and_menu_detail_add($id, $all_field_of_option) {
		$arr_field = explode('_._', $all_field_of_option);
		foreach ($arr_field as $value) {
			if ($value == 'name') {
				$option[$value] = '(undefined)';
			} elseif (in_array($value, array('deleted'))) { // nếu có option nào bằng false thì b�? vào array này
				$option[$value] = false;
			} elseif (in_array($value, array('cal_enabled_move'))) { // nếu có option nào bằng true thì b�? vào array này
				$option[$value] = true;
			} else {
				$option[$value] = '';
			}
		}

		$this->selectModel('Setting');
		//Tung, them 22/12/2013, field email_setting khac nhung record khac
		$setting = $this->Setting->select_one(array('_id'=> new MongoId($id)));
		if($setting['setting_value']=='email_setting'){
			$option['value']['host'] = '';
			$option['value']['port'] = '';
		}
		//End Tung
		$this->Setting->collection->update(
				array('_id' => new MongoId($id)), array('$push' => array(
				'option' => $option
			)
				)
		);
		$this->list_and_menu_detail($id);
		$this->render('list_and_menu_detail');   //dùng một view khác view mặc định trùng tên với action
	}

	public function list_and_menu_detail_auto_save() {
		if (!empty($this->data)) {
			$post = $this->data['Setting'];
			$this->selectModel('Setting');
			$arr_save = $this->Setting->select_one(array('_id'=>new MongoId($post['_id'])));
			foreach($post as $key => $value){
				if($key == 'option_key' ||  $key == '_id')
					continue;
				$arr_save['option'][$post['option_key']][$key] = $post[$key];
			}
			$arr_save['option'][$post['option_key']]['name'] = $post['name'];
			$arr_save['option'][$post['option_key']]['value'] = $post['value'];
			$arr_save['option'][$post['option_key']]['deleted'] = ($post['deleted'])?true:false;
			if(isset($post['color'])){// dùng cho những option có thêm color như tasks_status
				$arr_save['option'][$post['option_key']]['color'] = $post['color'];
			}
			if ($this->Setting->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Setting->arr_errors_save[1];
			}
		}
		die;
	}

	public function list_and_menu() {
		$this->selectModel('Setting');
		$arr_settings = $this->Setting->select_all(array(
												   'arr_where'=>array(
																	  'setting_value'=>array('$nin'=>array('email_setting')),
																	  'name'=>array('$ne'=>'Pricing Rules')
																	  ),
												   'arr_order' => array('setting_name' => 1)
												   ));
		$this->set('arr_settings', $arr_settings);

		$this->Session->write('setup_remember_page', 'list_and_menu');
	}

	public function list_and_menu_detail($id,$lang) {
		$this->selectModel('Setting');
		$arr_setting = $this->Setting->select_one(array('_id' => new MongoId($id)));
		$this->set('arr_setting', $arr_setting);
		$this->selectModel('Language');
		$arr_tmp = array();
		$arr_language = $this->Language->select_all(array('arr_order' => array('name' => 1)));
		foreach($arr_language as $key => $value){
			$arr_tmp[$value['value']] = isset($value['lang'])?$value['lang']:'';
		}
		$this->set('arr_language', $arr_tmp);
		$this->set('arr_language_value', $lang);


	}

	public function privileges(){
		$this->selectModel('Permission');
		$arr_permissions = $this->Permission->select_all(array('arr_order' => array('controller' => 1)));
		$this->set('arr_permissions', $arr_permissions);

		$this->Session->write('setup_remember_page', 'privileges');
	}

	public function privileges_detail($id) {
		$this->selectModel('Permission');
		$arr_privilege = $this->Permission->select_one(array('_id' => new MongoId($id)));
		$this->set('arr_privilege', $arr_privilege);

		if( $this->system_admin ){
			$this->render('privileges_detail_system');
		}
	}

	public function privileges_detail_auto_save() {
		if (!empty($this->data)) {
			if( isset($this->data['Privilege']) ){
				$post = $this->data['Privilege'];
				$this->selectModel('Permission');
				$arr_permission = $this->Permission->select_one(array('_id' => new MongoId($post['_id'])));
				if( $post['page'] != $post['page_old'] ){
					if( isset($arr_permission['permission'][0][$post['page_old']][$post['option_key']]) )
						unset($arr_permission['permission'][0][$post['page_old']][$post['option_key']]);
					$arr_permission['permission'][0][$post['page_old']] = array_values($arr_permission['permission'][0][$post['page_old']]);
					if( isset($arr_permission['permission'][0][$post['page_old']]) && empty($arr_permission['permission'][0][$post['page_old']]) )
						unset($arr_permission['permission'][0][$post['page_old']]);
					// Thêm vào mảng cũ
					$arr_permission['permission'][0][$post['page']][] = array(
						'name' => $post['name'],
						'codekey' => $post['codekey'],
						'description' => $post['description'],
						'deleted' => (isset($post['deleted']) && $post['deleted'])?true:false,
						'ownership' => array('all', 'owner', 'group')
					);
				}else{
					$arr_permission['permission'][0][$post['page']][$post['option_key']]['name'] = $post['name'];
					$arr_permission['permission'][0][$post['page']][$post['option_key']]['codekey'] = $post['codekey'];
					$arr_permission['permission'][0][$post['page']][$post['option_key']]['description'] = $post['description'];
					$arr_permission['permission'][0][$post['page']][$post['option_key']]['deleted'] = (isset($post['deleted']) && $post['deleted'])?true:false;
					$arr_permission['permission'][0][$post['page']][$post['option_key']]['ownership'] = array('all', 'owner', 'group');
				}
				if(isset($post['deleted']) && $post['deleted']){
					if(!isset($arr_permission['inactive_permission']))
						$arr_permission['inactive_permission'] = array();
					$arr_permission['inactive_permission'][$arr_permission['controller'].'_@_'.$post['page'].'_@_'.$post['codekey']] = 'all';
				}elseif( isset($arr_permission['inactive_permission'][$arr_permission['controller'].'_@_'.$post['page'].'_@_'.$post['codekey']] ) ){
					unset( $arr_permission['inactive_permission'][$arr_permission['controller'].'_@_'.$post['page'].'_@_'.$post['codekey']] );
				}
			}else{
				$post = $this->data['Optionlist'];
				$this->selectModel('Permission');
				$arr_permission = $this->Permission->select_one(array('_id' => new MongoId($post['_id'])));
				$arr_permission['option_list'][$post['key_column']][$post['key_page']][$post['option_key']]['name'] = $post['name'];
				$arr_permission['option_list'][$post['key_column']][$post['key_page']][$post['option_key']]['deleted'] = ($post['deleted'])?true:false;

				if(isset($post['description']))
					$arr_permission['option_list'][$post['key_column']][$post['key_page']][$post['option_key']]['description'] = $post['description'];
				if(isset($post['codekey']))
					$arr_permission['option_list'][$post['key_column']][$post['key_page']][$post['option_key']]['codekey'] = $post['codekey'];
				if(isset($post['finish']))
					$arr_permission['option_list'][$post['key_column']][$post['key_page']][$post['option_key']]['finish'] = $post['finish'];
				if(isset($post['permission']))
					$arr_permission['option_list'][$post['key_column']][$post['key_page']][$post['option_key']]['permission'] = $post['permission'];
			}

			if ($this->Permission->save($arr_permission)) {
				if( isset($post['page']) && $post['page'] != $post['page_old'] ){
					echo 'ok_change';
				}else{
					echo 'ok';
				}
			} else {
				echo 'Error: ' . $this->Permission->arr_errors_save[1];
			}
		}
		die;
	}

	public function privileges_detail_add($id) {
		$this->selectModel('Permission');
		$arr_permission = $this->Permission->select_one(array('_id' => new MongoId($id)));
		$arr_permission['permission'][0]['undefined'] = array(
			array(
				'name' => '',
				'codekey' => '',
				'ownership' => array('all', 'owner', 'group'),
				'deleted' => '',
			)
		);
		if ($this->Permission->save($arr_permission)) {
			echo 'ok';

		}else{
			echo 'Error: ' . $this->Permission->arr_errors_save[1];
		}
		die;
	}

	// ============================================= ROLE =====================================
	public function roles(){
		$this->selectModel('Role');
		$arr_roles = $this->Role->select_all(array('arr_order' => array('name' => 1)));
		$this->set('arr_roles', $arr_roles);

		$this->Session->write('setup_remember_page', 'roles');
	}

	public function roles_module($role_id) {
		$this->selectModel('Permission');
		$arr_permissions = $this->Permission->select_all(array('arr_order' => array('controller' => 1)));
		$this->set('arr_permissions', $arr_permissions);

		$this->set('role_id', $role_id);
		$this->selectModel('Role');
		$arr_roles = $this->Role->select_one(array('_id' => new MongoId($role_id)));
		$this->set('roles', $arr_roles);
		$this->set('arr_roles', $arr_roles['value']);
	}

	public function roles_module_detail($role_id, $id) {
		$this->selectModel('Permission');
		$arr_permission = $this->Permission->select_one(array('_id' => new MongoId($id)));
		$this->set('arr_permission', $arr_permission);

		// .......................
		// $role_id, lấy ra tất cả permission của role này
		$this->selectModel('Role');
		$arr_roles = $this->Role->select_one(array('_id' => new MongoId($role_id)));
		$this->set('arr_roles', $arr_roles['value']);

		$this->set('role_id', $role_id);
	}

	public function roles_add() {
		$this->selectModel('Role');
		$arr_save['name'] = 'undefined';
		$arr_save['value'] = array();
		if ($this->Role->save($arr_save)) {
			echo 'ok';
		}else{
			echo 'Error: ' . $this->Role->arr_errors_save[1];
		}
		die;
	}

	public function roles_auto_save() {
		$this->selectModel('Role');
		$post = $_POST;
		$arr_save['_id'] = $post['_id'];
		$arr_save['name'] = $post['name'];
		if ($this->Role->save($arr_save))
			echo 'ok';
		else
			echo 'Error: ' . $this->Role->arr_errors_save[1];
		die;
	}

	public function roles_module_detail_option_list(){
		if(!empty($_POST)){
			$this->selectModel('Permission');
			$arr_permission = $this->Permission->select_one(array('controller'=>$_POST['controller']));
			$this->selectModel('Role');
			$arr_save = $this->Role->select_one(array('_id' => new MongoId($_POST['role_id'])));
			//Tick all
			$controller = $_POST['controller'];
			if(isset($_POST['options'])){
				foreach($arr_permission['option_list'] as $option_list){
					foreach($option_list as $value){
						foreach($value as $val){
							$permission_path = $controller.'_@_options_@_'.$val['codekey'];
							$arr_save['value'][$permission_path] = 'all';
						}
					}
				}
			} else { //Untick all
				foreach($arr_permission['option_list'] as $option_list){
					foreach($option_list as $value){
						foreach($value as $val){
							$permission_path = $controller.'_@_options_@_'.$val['codekey'];
							if(isset($arr_save['value'][$permission_path]))
								unset($arr_save['value'][$permission_path]);
						}
					}
				}
			}
			if ($this->Role->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Role->arr_errors_save[1];
			}
		}
		die;
	}

	public function roles_module_detail_auto_save( $checkbox ) {

		if (!empty($this->data)) {
			$post = $_POST;

			// kiểm tra và save vào db
			$this->selectModel('Role');
			$arr_save = $this->Role->select_one(array('_id' => new MongoId($post['role_id'])));

			// nếu đang toàn quyền tất cả
			if( isset($arr_save['value']['all']) ){
				unset( $arr_save['value']['all'] );
				$this->selectModel('Permission');

				// cập nhật lại tất cả quyền của tất cả controller
				$arr_permission = $this->Permission->select_all();
				foreach ($arr_permission as $permission) {
					foreach ($permission['permission'][0] as $key_page => $page) {
						foreach ($page as $key => $value) {

							// lưu lại toàn bộ quyền của module này, ngoại trừ quyền vừa loại bỏ
							$permission_path = $permission['controller'].'_@_'.$key_page.'_@_'.$value['codekey'];
							if( $permission_path != $post['permission_path'] ){
								$arr_save['value'][$permission_path] ='all';
							}
						}
					}
				}

			}else{
				if( $checkbox == 'true' )
					$arr_save['value'][$post['permission_path']] ='all';
				elseif( isset($arr_save['value'][$post['permission_path']]) )
					unset($arr_save['value'][$post['permission_path']]);
			}
			if ($this->Role->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Role->arr_errors_save[1];
			}
		}
		die;
	}

	public function roles_set_all_permission($id, $type = 'true') {
		$this->selectModel('Role');
		$arr_save = $this->Role->select_one(array('_id' => new MongoId($id)));
		if( $type == 'true' )
			$arr_save['value'] = array('all' => '');
		else
			$arr_save['value'] = array();
		if ($this->Role->save($arr_save))
			echo 'ok';
		else
			echo 'Error: ' . $this->Role->arr_errors_save[1];
		die;
	}
	// ========================= ROLE END ===================================================================


	// ============================================= USER ROLE =====================================
	// hiển thị cột 1
	public function user_roles(){
		$this->selectModel('Contact');
		$arr_employees = $this->Contact->select_all(array(
			'arr_where' => array('is_employee' => 1),
			'arr_order' => array('first_name' => 1)
		));
		$this->set('arr_employees', $arr_employees);

		$this->Session->write('setup_remember_page', 'user_roles');
	}

	// hiển thị cột 2
	public function user_roles_module($contact_id) {

		// Lấy danh sách tất cả module có trong bảng permission
		$this->selectModel('Permission');
		$arr_permissions = $this->Permission->select_all(array('arr_order' => array('controller' => 1)));
		$this->set('arr_permissions', $arr_permissions);

		// Lấy danh sách tất cả ROLE
		$this->selectModel('Role');
		$arr_roles = $this->Role->select_all(array('arr_field' => array('_id', 'name'), 'arr_order' => array('name' => 1)));
		$this->set('arr_roles', $arr_roles);

		// danh sách quyền của user này
		$this->set('contact_id', $contact_id);
		$this->selectModel('Contact');
		$arr_user_roles = $this->Contact->select_one(array('_id' => new MongoId($contact_id)), array('_id', 'roles'));
		if( isset($arr_user_roles['roles']) )$arr_user_roles = $arr_user_roles['roles'];
		$this->set('arr_user_roles', $arr_user_roles);
	}

	// hiển thị cột 3
	public function user_roles_module_detail($contact_id, $permission_id) {
		$this->selectModel('Permission');
		$arr_permission = $this->Permission->select_one(array('_id' => new MongoId($permission_id)));
		$this->set('arr_persmiss', $arr_permission);

		// .......................
		// danh sách quyền của user này
		$this->set('contact_id', $contact_id);
		$this->selectModel('Contact');
		$arr_user_roles = $this->Contact->select_one(array('_id' => new MongoId($contact_id)), array('_id', 'roles'));
		if( isset($arr_user_roles['roles']) )$arr_user_roles = $arr_user_roles['roles'];

		if( isset($arr_user_roles['roles']) && !empty($arr_user_roles['roles']) ){ // kiểm tra xem có group nào không, nếu có thì kiểm tra đã có những quyền gì
			$this->selectModel('Role');
			$arr_all_roles = array();
			foreach ($arr_user_roles['roles'] as $role) {
				$arr_tmp = $this->Role->select_one(array('_id' => $role), array('_id', 'value'));
				if( is_array($arr_tmp['value']) )
					$arr_all_roles = array_merge($arr_all_roles, $arr_tmp['value']);
			}
			$arr_user_roles['roles'] = $arr_all_roles;
		}
		$this->set('arr_user_roles', $arr_user_roles);

		$this->set('permission_id', $permission_id);
	}

	// click vào checkbox cột số 2
	public function user_roles_set_all_permission($contat_id, $role_id, $type = 'true') {

		$this->selectModel('Contact');
		$arr_save = $this->Contact->select_one(array('_id' => new MongoId($contat_id)));
		if( !isset($arr_save['roles']) ){
			$arr_save['roles'] = array();
			$arr_save['roles']['roles'] = array();
		}

		if( $type == 'true' )
			$arr_save['roles']['roles'][] = new MongoId( $role_id );

		else // == false tức là loại bỏ quyền
			foreach ($arr_save['roles']['roles'] as $key => $value) {
				if( (string)$value == $role_id ){
					unset( $arr_save['roles']['roles'][$key] );
				}
			}

		if ($this->Contact->save($arr_save))
			echo 'ok';
		else
			echo 'Error: ' . $this->Contact->arr_errors_save[1];
		die;
	}

	function user_roles_module_detail_option_list(){
		if(!empty($_POST)){
			$this->selectModel('Permission');
			$arr_permission = $this->Permission->select_one(array('controller'=>$_POST['controller']));
			$this->selectModel('Contact');
			$arr_save = $this->Contact->select_one(array('_id' => new MongoId($_POST['contact_id'])));
			//Tick all
			$controller = $_POST['controller'];
				if( !isset($arr_save['roles']) ){ // khai báo default
				$arr_save['roles'] = array();
				$arr_save['roles']['roles'] = array();
			}
			if(isset($_POST['options'])){
				foreach($arr_permission['option_list'] as $option_list){
					foreach($option_list as $value){
						foreach($value as $val){
							$permission_path = $controller.'_@_options_@_'.$val['codekey'];
							$arr_save['roles'][$permission_path] = 'all';
						}
					}
				}
			} else { //Untick all
				foreach($arr_permission['option_list'] as $option_list){
					foreach($option_list as $value){
						foreach($value as $val){
							$permission_path = $controller.'_@_options_@_'.$val['codekey'];
							if(isset($arr_save['roles'][$permission_path]))
								unset($arr_save['roles'][$permission_path]);
						}
					}
				}
			}
			if ($this->Contact->save($arr_save))
				echo 'ok';
			else
				echo 'Error: ' . $this->Contact->arr_errors_save[1];
			}
		die;
	}
	// click vào checkbox cột số 3
	public function user_roles_module_detail_set_permission($checkbox = 'false') {
		$post = $_POST;
		$this->selectModel('Contact');
		$arr_save = $this->Contact->select_one(array('_id' => new MongoId($post['contact_id'])));
		if( !isset($arr_save['roles']) ){ // khai báo default
			$arr_save['roles'] = array();
			$arr_save['roles']['roles'] = array();
		}

		if( $checkbox == 'false' && isset($arr_save['roles'][$post['permission_path']]) )
			unset($arr_save['roles'][$post['permission_path']]);
		else
			$arr_save['roles'][$post['permission_path']] = 'all';

		if ($this->Contact->save($arr_save))
			echo 'ok';
		else
			echo 'Error: ' . $this->Contact->arr_errors_save[1];
		die;
	}

	// ========================= USER ROLE END ===================================================================

	public function equipments() {
		$this->selectModel('Equipment');
		$this->Equipment->has_field_deleted = false;
		$arr_equipments = $this->Equipment->select_all(array('arr_order' => array('name' => 1)));
		$this->selectModel('Setting');
		$arr_uom = array_merge(
		   array('hour'=>'Hour','Linch'=>'Lin. ft','Sq.ft.'=>'Sq.ft.','inch'=>'Length (inch)')
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_Sq. ft.'))
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_area'))
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_lengths'))
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_size'))
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_weight'))
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_volume'))
		   // ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_unit'))
		);
		$this->set('arr_uom',$arr_uom);
		$this->set('arr_equipments', $arr_equipments);

		$this->set('arr_tasks_status', $this->Setting->select_option(array('setting_value' => 'tasks_status'), array('option')));

		$this->Session->write('setup_remember_page', 'equipments');
	}

	public function equipments_auto_save() {
		if (!empty($this->data) || isset($_POST['equipment_id'])) {
			$this->selectModel('Equipment');
			$arr_save = array();
			if( isset($_POST['equipment_id']) ){
				$arr_save = $this->Equipment->select_one(array('_id' => new MongoId($_POST['equipment_id'])));
				if(!isset($arr_save['color']))
					$arr_save['color'] = array();
				$arr_save['color'][$_POST['status']] = $_POST['color'];
			}else{
				$post = $this->data['Equipment'];
				$arr_save['_id'] = $post['_id'];
				$arr_save['name'] = $post['name'];
				$arr_save['uom'] = $post['uom'];
				$arr_save['uom_key'] = $post['uom_id'];
				$arr_save['speed_per_hour'] = $post['speed_per_hour'];
				$arr_save['description'] = $post['description'];
				$arr_save['deleted'] = (isset($post['deleted']) ? true : false);
			}

			if ($this->Equipment->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Equipment->arr_errors_save[1];
			}
		}
		die;
	}

	public function equipments_add() {
		$arr_save = array();
		$arr_save['name'] = 'undefined';
		$this->selectModel('Equipment');
		if ($this->Equipment->save($arr_save)) {
			$this->equipments();
			$this->render('equipments');
		} else {
			echo 'Error: ' . $this->Equipment->arr_errors_save[1];
		}
	}

	public function salesorders_status_color() {
		$this->selectModel('Setting');
		$this->set('arr_salesorders_status_color', $this->Setting->select_all(array('setting_value' => 'salesorders_status'), array('option')));
	}

	public function salesorders_status_pickcolor($mongo_id, $idx, $color) {
		if (is_numeric($idx)) {
			$error = 0;
			if (!$error) {
				$this->selectModel('Setting');
				if ($this->Setting->update_field_option($mongo_id, $idx, 'color', '#' . $color)) {
					echo 'ok';
				} else {
					echo 'Error: ' . $this->Setting->arr_errors_save[1];
				}
			}
		}
		die;
	}

	function list_country() {
		$this->selectModel('Country');
		$arr_countrys = $this->Country->select_all(array('arr_order' => array('name' => 1)));
		$this->set('arr_countrys', $arr_countrys);

		$this->Session->write('setup_remember_page', 'list_country');
	}

	public function list_province_add_country(){
		$this->selectModel('Country');
		$arr_save['value'] = $this->data['Setting']['country_code'];
		$arr_save['name'] = $this->data['Setting']['country_name'];
		if ($this->Country->save($arr_save)) {
			echo 'ok';
		} else {
			echo 'Error: ' . $this->Company->arr_errors_save[1];
		}
		die;
	}

	public function list_province_detail($country) {
		$this->selectModel('Province');
		$this->selectModel('Country');
		$arr_country = $this->Country->select_one(array('value' => $country));
		$arr_provinces = $this->Province->select_all(array(
			'arr_where' => array('country_id' => $country),
			'arr_order' => array('name' => 1)
		));
		$this->set('arr_country', $arr_country);
		$this->set('arr_provinces', $arr_provinces);
	}

	public function list_province_detail_auto_save() {
		if (!empty($this->data)) {
			$arr_save = $_REQUEST['Setting'];
			$this->selectModel('Province');
			if ($this->Province->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Province->arr_errors_save[1];
			}
		}
		die;
	}

	public function list_province_add($id) {
		$this->selectModel('Province');
		$save_data = array(
			'country_id' => $id,
			'name' => 'undefine',
			'key' => '',
			'deleted' => False,
		);
		$this->Province->save($save_data);
		$this->list_province_detail($id);
		$this->render('list_province_detail');
	}

	// Permission

	public function permission_list() {
		$this->selectModel('Permission');
		$permissions = $this->Permission->select_all(array(
			'arr_order' => array('_id' => 1)
		));
		$this->set('permissions', $permissions);
	}

	public function permission_detail($id = 0) {
		$this->selectModel('Permission');
		$permissions = $this->Permission->select_one(array('_id' => new MongoId($id)));
		$this->set('permissions', $permissions);
	}

	public function permission_detail_add($id) {
		$this->selectModel('Permission');
		$permissions = $this->Permission->select_one(array('_id' => new MongoId($id)));

		$col = $_REQUEST['col'];
		$data = $permissions;
		//$data = array('option_lists' => array(array(), array(), array()));
		if (isset($_REQUEST['group']) && $_REQUEST['group'] != '') {
			$group = str_replace(' ', '_', $_REQUEST['group']);
			$group = strtolower($group);
			$data['option_list'][$col][$group] = array();
		}

		$line = array();

		if (isset($permissions['option_list'][$col][$group])) {
			$line = $permissions['option_list'][$col][$group];
		}
		$index = count($line);

		$type = str_replace(' ', '_', $_REQUEST['name']);

		$line[$index]['url'] = $_REQUEST['url'];
		$line[$index]['name'] = $_REQUEST['name'];
		$line[$index]['type'] = $_REQUEST['type'];
		$line[$index]['codekey'] = strtolower($type);
		$line[$index]['flag'] = $_REQUEST['flag'];
		$line[$index]['description'] = $_REQUEST['description'];

		$data['option_list'][$col][$group] = $line;
		//pr($data);
		if ($this->Permission->save($data)) {
			echo 'Success';
		}
		die;
	}
	//Tung doi mau status
	public function sys_list()
	{
		$this->selectModel('Setting');
		$arr_system[] = $this->Setting->select_one(array('setting_value' => 'tasks_status'));

		$this->set('arr_system', $arr_system);
	}
	public function system_detail($id)
	{
		$this->selectModel('Setting');
		$arr_system = $this->Setting->select_one(array('_id'=>new MongoId($id)));
		$this->set('arr_system',$arr_system);

	}
	public function system_detail_auto_save()
	{
		if (!empty($this->data)) {
			$arr_save = $_REQUEST['Setting'];
			$this->selectModel('Setting');
			if ($this->Setting->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Setting->arr_errors_save[1];
			}
		}
		die;
	}

	public function system_detail_add($id)
	{
		$this->selectModel('Setting');
		$system = $this->Setting->select_one(array('_id'=>new MongoId($id)));
		$option = array(
			'value' => 'undefine',
			'color' => '#fff',
			'deleted' => '0',
			'cal_enabled_move' => '0',
			'show' => '0'

		);
		$system['option'][] = $option;
		$this->Setting->save($system);
		$this->system_detail($id);
		$this->render('system_detail');
	}

	public function add_action(){
		$this->selectModel('Permission');
		$data = array(
			'controller' => 'shippings',
			'option_list' => array()
		);
		if($this->Permission->save($data)){
			echo 'success';
		}
		die();
	}

	// end Permission
	//module_buider v1.0
	function module_buider() {
		// g�?i class cần dùng
		// tạo file Module.php
		// tạo file ModuleField.php
		// tạo file ModulesController.php
		// tạo thư mục Modules trong View/Themed/Default
	}
	public function auto_process(){
		$this->selectModel("AutoProcess");
		$this->AutoProcess->has_field_deleted = false;
		$arr_process = $this->AutoProcess->select_all(array('arr_order'=>array('controller'=>1)));
		$this->selectModel("Emailtemplate");
		$template = $this->Emailtemplate->select_all(array('arr_order'=>array('name'=>1)));
		$arr_template = array();
		if($template->count()>0){
			foreach($template as $value){
				$arr_template[(string)$value['_id']] = $value['name'];
			}
		}
		$arr_module = array('salesorders'=>'Sales Order','quotations'=>'Quotation');
		$arr_name = array(
						  'When status is changed to Completed - email Customer'=>'Email Customer (Completed)',
						  'When status is changed to Completed - email CSR, Rep'=>'Email CSR, Rep (Completed)',
						  'When status is changed to Completed - email Accounting'=>'Email Accounting (Completed)',
						  'When status is changed to Submitted - check Quotation limit & email CSR'=>'Email CSR (Submitted)',
						  'When status is changed to Approved - email Rep'=>'Email Rep (Approved)',
						  'When status is changed to Completed by whom is not our CSR, and payment term equals to zero, restores status to last state and asks if send email to our CSR'=>'Email CSR (payment term)',
						  );
		$this->set('arr_name',$arr_name);
		$this->set('arr_module',$arr_module);
		$this->set('arr_template',$arr_template);
		$this->set('arr_process',$arr_process);

		$this->Session->write('setup_remember_page', 'auto_process');
	}
	function auto_process_add(){
		$this->selectModel("AutoProcess");
		if($this->AutoProcess->add()){
			$this->auto_process();
			$this->render('auto_process');
		}
		else
			echo $this->AutoProcess->arr_errors_save[1];
	}
	function auto_process_remove(){
		if(isset($_POST['id'])&&strlen($_POST['id'])==24){
			$this->selectModel("AutoProcess");
			if($this->AutoProcess->remove(new MongoId($_POST['id'])))
				echo 'ok';
			else
				echo $this->AutoProcess->arr_errors;
		}
		die;
	}
	function auto_process_auto_save(){
		if (!empty($this->data) || isset($this->data['Process']['_id'])) {
			$this->selectModel('AutoProcess');
			$arr_save = array();
			$post = $this->data['Process'];
			$arr_save['_id'] = new MongoId($post['_id']);

			if($_SESSION['arr_user']['contact_name']=='System Admin'){
				$arr_save['name'] = $post['name'];
				$arr_save['module_name'] = $post['module_name'];
				$arr_save['controller'] = $post['controller'];
				$arr_save['email_template'] = $post['email_template'];
				if(isset($post['email_template_id'])&&strlen($post['email_template_id'])==24)
					$arr_save['email_template_id'] = new MongoId($post['email_template_id']);
				$arr_save['description'] = $post['description'];
			}
			$arr_save['deleted'] = (isset($post['deleted']) ? true : false);

			if ($this->AutoProcess->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->AutoProcess->arr_errors_save[1];
			}
		}
		die;
	}
	function pricing_rules(){
		$this->selectModel('Setting');
		$arr_pricing_rule = $this->Setting->select_one(array('setting_value' => 'pricing_rules'));
		$this->set('arr_pricing_rule', $arr_pricing_rule);
	}
	function system_email(){
		$this->selectModel('Stuffs');
		if(isset($_POST['data'])){
			$arr_save = $_POST['data'];
			$arr_save['host'] = trim($arr_save['host']);
			if(!filter_var($arr_save['username'], FILTER_VALIDATE_EMAIL)){
				echo 'Email must be valid.';
				die;
			}
			if(trim($arr_save['password'])==''){
				echo 'Password must not be empty';
				die;
			}
			if(!is_numeric($arr_save['port'])){
				echo 'Port must be a numeric value.';
				die;
			}
			// if(!filter_var(gethostbyname($arr_save['host']), FILTER_VALIDATE_IP)){
			// 	echo 'Host must be valid.';
			// 	die;
			// }
			if($this->Stuffs->save($arr_save)){
				echo 'ok';
				die;
			} else {
				echo $this->Stuffs->arr_errors_save[1];
				die;
			}
		}
		$email = $this->Stuffs->select_one(array('value'=>"system_email"));
		$this->set('email', $email);
		$this->Session->write('setup_remember_page', 'system_email');

	}
	function hook_setting(){
		$hook = new hook(array('hook_list'=>true));
		$this->set('hook_list',$hook->hook_list);
		$this->Session->write('setup_remember_page', 'hook_setting');
	}
	function hook_detail($classname){
		if($classname!='max_yeild'){
			$arr_data = array(
							  'event'=>$classname,
							  'setting'=>true,
							  );
			if(isset($_POST['data'])){
				$arr_data['setting_data'] = $_POST['data'];
			}
			$hook = new hook($arr_data);
			$this->selectModel('Setting');
			$hook_value = $this->Setting->select_one(array('setting_value'=>$classname));
			$this->set('hook_value',$hook_value);
		}
		else die;
	}

	function get_name_php_file($folder_name='Controller',$last_part_name='Controller.php'){
		$result = array();
		$dir = scandir(ROOT.DS.APP_DIR.DS.$folder_name);
		foreach($dir as $keys=>$values){
			if(strpos($values,".php")===false)
				continue;
			if(strpos($values,$last_part_name)===false)
				continue;
			$result[] = str_replace($last_part_name, '', $values);
		}
		return $result;
	}


	function auto_create_module(){
		if(isset($_POST['ok'])){
			if($_POST['module_name'] == NULL || $_POST['controller_name'] == NULL || $_POST['model_name'] == NULL){
				echo 'Please enter all valid infomation.';die;
		}else{
			$tmp = $this->get_name_php_file();
			if(in_array($_POST['controller_name'],$tmp)){echo 'Name controller exited!';die;}else{
				$controller_dir = ROOT.DS.APP_DIR.DS.'Controller';
				$model_dir = ROOT.DS.APP_DIR.DS.'Model';
				$view_dir = ROOT.DS.APP_DIR.DS.'View'.DS.'Themed'.DS.'Default'.DS. ucfirst($_POST['controller_name']);

				//BasicField
				$file = $model_dir . DS.ucfirst($_POST['model_name']).'Field'. '.php';
				$source = $model_dir . DS . 'BasicField.php';
				copy($source, $file);
				$t = file_get_contents($file);
				$fopen = fopen($file,"w+");
				$t = str_replace("__('Basic'),", "__('".$_POST['model_name']."'),",$t);
				$t = str_replace("'module_label' 	=> __('Basics'),", "'module_label' 	=> __('".$_POST['controller_name']."'),",$t);
				$t = str_replace("'colection' 	=> 'tb_basic',", "'colection' 	=> 'tb_".strtolower($_POST['model_name'])."',",$t);
				$t = str_replace("BasicField", $_POST['model_name'].'Field',$t);
				fwrite($fopen, $t);
				fclose($fopen);

				//Basic
				$file = $model_dir . DS .ucfirst($_POST['model_name']).'.php';
				$source_basic = $model_dir . DS . 'Basic.php';
				copy($source_basic, $file);
				$t = file_get_contents($file);
				$fopen = fopen($file,"w+");
				$t = str_replace("class Basic", "class ".$_POST['model_name'],$t);
				$t = str_replace("'module_name' => 'Basic'", "'module_name' => '".$_POST['model_name']."'",$t);
				fwrite($fopen, $t);
				fclose($fopen);

				//BasicsController
				$file = $controller_dir . DIRECTORY_SEPARATOR . ucfirst($_POST['controller_name']). 'Controller.php';
				$source_controller = $controller_dir . DS . 'BasicsController.php';
				copy($source_controller,$file);
				$t = file_get_contents($file);
				$fopen = fopen($file,"w+");
				$t = str_replace("class Basics", "class ".$_POST['controller_name'],$t);
				$t = str_replace("var \$name = 'Basics';", "var \$name = '".$_POST['controller_name']."';",$t);
				$t = str_replace("\$this->set_module_before_filter('Basic');", "\$this->set_module_before_filter('".$_POST['model_name']."');",$t);
				fwrite($fopen, $t);
				fclose($fopen);

				//View
				mkdir($view_dir);
				$old_folder_path = ROOT.DS.APP_DIR.DS.'View'.DS.'Themed'.DS.'Default'.DS.'Basics';
				$new_folder_path = $view_dir;
				$files = scandir($old_folder_path);

				foreach($files as $file)
					if ($file != "." && $file != "..")
						copy($old_folder_path.DS.$file,$new_folder_path.DS.$file);
				}
				if($this->request->is('ajax')){
					echo 'ok';
					die;
				}
			}
		}
		$this->Session->write('setup_remember_page', 'auto_create_module');
	}

	function language(){
		$this->selectModel('Language');
		$arr_language = $this->Language->select_all(array('arr_order' => array('name' => 1)));
		$this->set('arr_language', $arr_language);
	}

	public function list_laguage(){
		$this->selectModel('Language');
		$arr_tmp = array();
		$arr_language = $this->Language->select_all(array('arr_order' => array('name' => 1)));
		foreach($arr_language as $value){
			$arr_tmp[] = $value['value'];
		}
		if($this->data['Setting']['language_code'] == '' || $this->data['Setting']['language_name'] == ''){
			$this->set('error', '121');
			echo "All input must is not empty";
			die;
		}
		if(in_array($this->data['Setting']['language_code'], $arr_tmp)){
			echo 'This language is exited';die;
		}
		$arr_save['value'] =strtolower($this->data['Setting']['language_code']);
		$arr_save['lang'] = $this->data['Setting']['language_name'];
		if ($this->Language->save($arr_save)) {
			echo 'ok';
		} else {
			echo 'Error: ' . $this->Language->arr_errors_save[1];
		}
		die;
	}

	public function language_detail($language){
		$this->selectModel('Language');
		$this->selectModel('Languagedetail');
		$arr_language = $this->Language->select_one(array('value' => $language));
		$arr_language_detail = $this->Languagedetail->select_all(array(/*
			'arr_where' => array('content.'.$language => array('$exists'=>true)),
			'arr_order' => array('created_by' => 1),*/
		));
		$this->set('arr_language', $arr_language);
		$this->set('arr_language_detail',$arr_language_detail);
	}

	public function language_add($id){
		$this->selectModel('Languagedetail');
		$save_data = array(
			'key' => '',
			'content' => array('en' => 'empty'),
			'deleted' => False,
		);
		$save_data['content'][$id] = '';
		$this->Languagedetail->save($save_data);
		$this->language_detail($id);
		$this->render('language_detail');
	}
	public function language_auto_save() {
		if (!empty($this->data)) {
			$arr_save = $_REQUEST['Setting'];
			$arr_save['_id'] = new MongoId($arr_save['_id']);
			$this->selectModel('Languagedetail');
			//kiem tra trung key

			if(isset($arr_save['key']) && $arr_save['key'] != '' ){
				$arr_save['key'] = trim($arr_save['key']);
				$arr_language = $this->Languagedetail->count(array('key' => $arr_save['key']));
				if($arr_language > 0 && isset($_REQUEST['name_change'])){
					echo 'This key is existed ';die;
				}
			}

			if ($this->Languagedetail->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Languagedetail->arr_errors_save[1];
			}
		}
		die;
	}

	// BaoNam: 20 02 2014
	function create_auto_key($module = 'Company'){
		foreach (glob(APP.'View'.DS.'Themed'.DS.'Default'.DS.Inflector::pluralize($module).DS.'*.ctp') as $filename) {
			$content = file_get_contents($filename);
			if(preg_match_all('/translate\(\s*\'((?:[^\']|(?<=\\\)\')+)\'(?:\s*,\s*\'((?:[^\']|(?<=\\\)\')+)\')?\s*\)/us', $content, $result)) {
				foreach ($result as &$item) {
					array_shift($item);
				}
				unset($item);

				// ------------ SAVE ----------------
				$this->selectModel('Languagedetail');
				foreach ($result[1] as $value) {
					$save['key'] = 'JT_'.strtoupper($module).'_'.str_replace('__', '_', str_replace(' ', '_', strtoupper(preg_replace("/[^A-Za-z0-9 ]/", '', $value))));

					$save['content'][DEFAULT_LANG] = $value;
					$tmp = $this->Languagedetail->select_one(array('key' => $save['key']), array('_id'));
					if(!isset($tmp['_id'])){
						echo $save['key']."<br/>";
						if(!$this->Languagedetail->save($save))
							echo 'error save: '.$save['key'];
					}

				}
				// pr($result);
			}
		}
		echo 'xong';
		die;

		// return preg_replace(array_keys($preg), array_values($preg), $str);
	}

	public function create_auto_key_lang($module_name){
		$this->selectModel('Languagedetail');
		$arr_language = $this->Languagedetail->select_all(array(
				'arr_where' => array('key' => array('$ne'=>'')),
				));
		$arr_key =array();
		if(!empty($arr_language)){
			foreach($arr_language as $keys => $value){
				$arr_key[] = $value['key'];
			}
		}


		$module_field = $module_name.'Field';
		require_once APP.'Model'.DS.$module_field.'.php';
		$arr_tmp =  $$module_field;
		$key_lang =  KEY_LANG.strtoupper($module_name);

		if(!in_array($key_lang,$arr_key) && isset($arr_tmp['module_label'])){
			$new_data['key'] = $key_lang;
			$new_data['content'][DEFAULT_LANG] = $arr_tmp['module_label'];
			$this->Languagedetail->save($new_data);
		}

		// tu dong tao name field cho modules
		foreach($arr_tmp['field'] as $keys => $value){
			foreach($value as $k => $v){
				if(isset($v['name'])){
					$key_name = $key_lang.'_'.strtoupper($k);
					if(!in_array($key_name,$arr_key)){
						echo $key_name."<br/>";
						//tao language moi
						$new_data['key'] = $key_name;
						$new_data['content'][DEFAULT_LANG] = $v['name'];
						$this->Languagedetail->save($new_data);
					}

				}
			}
		}

		//tu dong tao name file cho relationship
		$rel = $arr_tmp['relationship'];
		$key_lang.='_REL';
		foreach($rel as $k => $v){
			$key_rel = $key_lang.'_'.strtoupper($k);
			if(!in_array($key_rel, $arr_key)){
				echo $key_rel."<br/>";
				//tao language moi
				$new_data['key'] = $key_rel;
				$new_data['content'][DEFAULT_LANG] = $v['name'];
				$this->Languagedetail->save($new_data);
			}
			foreach($rel[$k]['block'] as $k_block => $v_block){
				$key_block = $key_lang.'_'.strtoupper($k).'_'.strtoupper($k_block);
				if(!in_array($key_block, $arr_key)){
					echo $key_block."<br/>";
					//tao language moi
					$new_data['key'] = $key_block;
					$new_data['content'][DEFAULT_LANG] = $v_block['title'];
					$this->Languagedetail->save($new_data);
				}
				foreach($v_block['field'] as $k_field => $v_field){
					if(isset($v_field['name'])){
						$key_name = $key_block.'_'.strtoupper($k_field);
						if(!in_array($key_name,$arr_key)){
							echo $key_name."<br/>";
							//tao language moi
							$new_data['key'] = $key_name;
							$new_data['content'][DEFAULT_LANG] = $v_field['name'];
							$this->Languagedetail->save($new_data);
						}
					}
				}
			}
		}
		die;
	}

/*	public function create_auto_key_lang_settings(){
		//Lay thong tin tu bang setting
		$this->selectModel('Languagedetail');
		$arr_language = $this->Languagedetail->select_all(array(
				'arr_where' => array('key' => array('$ne'=>'')),
				));
		$arr_key =array();
		if(!empty($arr_language)){
			foreach($arr_language as $keys => $value){
				$arr_key[] = $value['key'];
			}
		}
		$this->selectModel('Setting');
		$key_lang =  KEY_LANG.'SETTING';
		$arr_setting = $this->Setting->select_all();
		foreach($arr_setting as $key => $value){
			foreach($value['option'] as $k => $v){
				if(isset($v['name']) && is_string($v['name']) && isset($value['setting_value'])){
					$key_name = $key_lang.'_'.strtoupper($value['setting_value']).'_'.strtoupper($v['value']);
					if(!in_array($key_name,$arr_key)){
						echo $key_name."<br/>";
						//tao language moi
						$new_data['key'] = $key_name;
						$new_data['content'][DEFAULT_LANG] = $v['name'];
						$this->Languagedetail->save($new_data);
					}
				}
			}
		}
		die;
	}*/

	public function create_auto_key_lang_settings_message(){

	}
	public function support(){
		$this->selectModel('Permission');
		$arr_modules = $this->Permission->select_all(array('arr_order' => array('controller' => 1)));
		$this->set('arr_modules', $arr_modules);

		$this->Session->write('setup_remember_page', 'support');
	}
	public function support_detail($name){
		$name = ucfirst(strtolower(str_replace(' ', '', $name)));
		$this->selectModel('Support');
		$this->Support->has_field_deleted = false;
		$supports = $this->Support->select_all(array(
		                                       'arr_where'=>array('module'=>$name),
		                                       'arr_order'=>array('name'=>1),
		                                       'arr_field'=>array('name','deleted')
		                                       ));
		$this->set('name',$name);
		$this->set('supports',$supports);
	}
	public function get_content_support(){
		if(isset($_POST['_id'])){
			$this->selectModel('Support');
			$support = $this->Support->select_one(array('_id'=>new MongoId($_POST['_id'])));
			echo $support['content'];
		}
		die;
	}
	public function support_add($name){
		$name = ucfirst(strtolower(str_replace(' ', '', $name)));
		$this->selectModel('Support');
		$arr_save['module'] = $name;
		$this->Support->add($arr_save);
		$this->support_detail($name);
		$this->render('support_detail');
	}
	public function support_auto_save(){
		if(isset($_POST)){
			$arr_save = $_POST;
			if(isset($arr_save['deleted']))
				$arr_save['deleted'] = ($arr_save['deleted']==1 ? true: false);
			$arr_save['_id'] = new MongoId($arr_save['_id']);
			$this->selectModel('Support');
			if($this->Support->save($arr_save))
				echo 'ok';
			else
				echo $this->Support->arr_errors_save[1];
		}
		die;
	}

}
