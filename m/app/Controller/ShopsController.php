<?php
App::uses('AppController', 'Controller');
class ShopsController extends AppController {

	public $helpers = array();

	// public $helpers = array('Cache');
	// public $cacheAction = "1 hour";

	public function beforeFilter( ){
		// goi den before filter cha
		// parent::beforeFilter();
	}

	function show_category(){
		$this->selectModel('Shop');
		$this->Shop->has_field_deleted = false;
		$query = $this->Shop->select_all(array());
		$arr_tmp = iterator_to_array($query);

		$menu = array();
		foreach($arr_tmp as $key=>$value){
			if(isset($value['is_parent']) && $value['is_parent']==1){
				$menu[$key]['name'] = $value['name'];
				foreach ($arr_tmp as $key2 => $value2) {
					if(isset($value2['parent_id']) && $value2['parent_id']==$key){
						$menu[$key]['child'][$key2]['name'] = $value2['name'];
						foreach ($arr_tmp as $key3 => $value3) {
							if(isset($value3['parent_id']) && $value3['parent_id']==$key2){
								$menu[$key]['child'][$key2]['child'][$key3]['name'] = $value3['name'];
							}
						}
					}
				}
			}
		}
		//pr($menu);die;
		$this->set('menu', $menu);
	}

	function get_product_from_category($category_id){
		$this->selectModel('Product');
		$this->Product->has_field_deleted = false;
		$cond = array();
		$cond = array('category_id' => $category_id);
		$query = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit' 	=> 1000
			));
		$this->set('products',$query);
		$arr_tmp = array();
		foreach($query as $key => $value){
			$arr_tmp = $value['brand_id'];
		}
		$this->selectModel('Brand');
		$this->Brand->has_field_deleted = false;
		$q = $this->Brand->select_one(array('_id'=>new MongoId($arr_tmp)));
		$this->set('brand',$q);
	}

	function product_detail($product_id){
		$this->selectModel('Product');
		$this->Product->has_field_deleted = false;
		$arr_tmp = array();
		$arr_tmp = $this->Product->select_one(array('_id'=>new MongoId($product_id)));
		$this->set('product_detail',$arr_tmp);
		$this->selectModel('Brand');
		$this->Brand->has_field_deleted = false;
		$q = $this->Brand->select_one(array('_id'=>new MongoId($arr_tmp['brand_id'])));
		$this->set('brand',$q);
	}

	function show_brand(){
		$this->selectModel('Brand');
		$this->Brand->has_field_deleted = false;
		$query = $this->Brand->select_all();
		$arr = array();
		foreach($query as $key => $value){
			$t = strtolower(substr($value['banner_name'][0], 0));
			$arr[$t][$key] = $value['banner_name'];

		}
		$this->set('brand_name',$arr);
	}

	function show_brand_list($brand_id){
		$this->selectModel('Product');
		$this->Product->has_field_deleted = false;
		$cond = array();
		$cond = array('brand_id' => $brand_id);
		$query = $this->Product->select_all(array(
		                                    'arr_where' => $cond,
		                                    'arr_order'  => array('_id' => 1),
		                                    'limit' => 1000
		                                    ));
		$this->set('data_product', $query);
		$this->selectModel('Brand');
		$this->Brand->has_field_deleted = false;
		$q = $this->Brand->select_one(array('_id'=>new MongoId($brand_id)));
		$this->set('data_brand',$q);
	}

	function added_a_product($product_id) {
		$this->selectModel('Product');
		$this->Product->has_field_deleted = false;
		$arr_product = $this->Product->select_one(array('_id'=>new MongoId($product_id)));
		$this->set('product', $arr_product);

		$brand = array();
		if(strlen($arr_product['brand_id']) > 0){
			$this->selectModel('Brand');
			$this->Brand->has_field_deleted = false;
			$brand = $this->Brand->select_one(array('_id'=>new MongoId($arr_product['brand_id'])));
		}
		$this->set('brand', $brand);

		$save['product_id'] = $product_id;
		$save['product_quantity'] = 1;
		$save['product_type'] = 1;
		$save['brand_id'] = isset($brand['_id'])?$brand['_id']:'';
		$save['brand_name'] = isset($brand['brand_name'])?$brand['brand_name']:'';
		$save['category_id'] = '';
		$save['category_name'] = '';
		$save['product_image'] = $arr_product['products_upload'];
		$save['product_slugger'] = $arr_product['slugger'];
		$save['user_id'] = '';
		if($this->Session->check('arr_user')){
			$user = $this->Session->read('arr_user');
			$save['user_id'] = $user['_id'];
		}
		$save['product_name'] = (strlen($arr_product['name']) > 0)?$arr_product['name']:$arr_product['sku'];
		$save['product_sku'] = $arr_product['sku'];
		$save['product_code'] = $arr_product['code'];
		$save['product_price'] = $arr_product['sell_price'];

		$my_cart = array();
		if( $this->Session->check('my_cart') )
			$my_cart = $this->Session->read('my_cart');
		if( isset($my_cart[$product_id]) )
			$my_cart[$product_id]['product_quantity'] += 1;
		else
			$my_cart[$product_id] = $save;
		$this->Session->write('my_cart', $my_cart);
		$this->set('my_cart', $my_cart);
		$this->set('product_id', $product_id);
	}

	function checkout(){
		$my_cart = array();
		if( $this->Session->check('my_cart') )
			$my_cart = $this->Session->read('my_cart');
		$this->set('my_cart', $my_cart);
	}
}
