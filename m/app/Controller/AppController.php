<?php
App::uses('Controller', 'Controller');
class AppController extends Controller {
	var $components = array('Session', 'Common');
	var $helpers = array('Html', 'Form', 'Common');
	public $viewClass = 'Theme';
	public $theme = 'default';
	var $uses = null;
	var $modelName = '';
	var $arr_permission = null;
	var $arr_inactive_permission = null;
	var $system_admin = false;

	function beforeFilter() {
		//$this->Session->write('default_lang','vi');
		//An, 21/2/2014 Use for Mobile
		// if(!$this->check_report_pdf()){ //Dùng để bypass SESSION cho phantomjs
		// 	if ($this->request->is('ajax') && !$this->Session->check('arr_user')) {
		// 		echo 'Your session is time out, please re-login. Thank you.<script type="text/javascript">location.reload(true);</script>';
		// 		exit;
		// 	}
		// 	if ($this->request->url != 'users/login' && !$this->Session->check('arr_user')) {
		// 		$this->Session->write('REFERRER_LOGIN', '/' . $this->request->url);
		// 		$this->redirect('/users/login');
		// 		die;
		// 	}
		// }
		// $this->theme = 'AnotherExample';
		$this->set('controller', $this->params->params['controller']);
		$this->set('model', $this->modelName);
		$this->set('action', $this->params->params['action']);

		// định nghĩa các thông báo từ hệ thống
		// $this->set('arr_msg', $this->define_system_message());
		$arr_permission = array('current_permission'=>array(),'inactive_permission'=>array());
		$arr_inactive_permission = array();

		// BaoNam: kiem tra system admin
		if( isset($_SESSION['arr_user']) && $_SESSION['arr_user']['contact_name'] == 'System Admin' ){
			$this->system_admin = true;
			$this->set('system_admin', true);
		}

		if ($this->request->url != 'users/login' && $this->Session->check('arr_user') && CHECK_PERMISSION && !$this->system_admin) {
			// dùng cho view hoặc function của controller
			$arr_permission['current_permission'] = $this->get_all_permission();
			$arr_permission['inactive_permission'] = $this->get_all_inactive_permission();
			$this->set('arr_menu',$this->rebuild_header_menu($arr_permission));
			/*	Tạm thời ko dùng khái niệm owner
				//Nếu như không có quyền trên toàn controller, và ko có quyền xem tất cả record entry trên controller
				if(!isset($this->arr_permission['all']) &&
				   isset($this->arr_permission[strtolower($this->name).'_@_entry_@_view']) && $this->arr_permission[strtolower($this->name).'_@_entry_@_view']!='all'){
					$arr_where = array('created_by'=>new MongoId($_SESSION['arr_user']['contact_id']));
					$this->Session->write($this->name.'_where_permission',$arr_where);
				}
			*/
		}
		$this->arr_permission = $arr_permission['current_permission'];
		$this->arr_inactive_permission = $arr_permission['inactive_permission'];
		$this->set('arr_permission',$arr_permission);
	}
	function beforeRender() {
		if ($this->request->is('ajax')) {
			$this->set('ajax', true);
			$this->layout = 'ajax';
		}
		if (is_object($this->db)) {
			// tạm thời không ngắt kết nối để tối ưu các request đỡ phải mất thời gian kết nối nhiều lần, vd: các ajax trên page chẳng hạn
			// if (!$this->mongo_disconnect()) {
			//     die('Can not disconnect mongodb!');
			// }
		}
	}

	protected $db = null;
	private $connectionDB = null;

	function mongo_connect() {
		if (!is_object($this->connectionDB)) {
			// http://docs.mongodb.org/manual/reference/connection-string/#connections-standard-connection-string-format
			// set Timeout và không cần close nữa
			$this->connectionDB = new Mongo(IP_CONNECT_MONGODB . ':27017?connectTimeoutMS=300000');
			$this->db = $this->connectionDB->selectDB(DB_CONNECT_MONGODB);
		}
	}

	function mongo_disconnect() {
		// $this->db->command(array("logout" => 1));
		return $this->connectionDB->close();
	}

	function selectModel($model) {
		$this->mongo_connect();
		if (is_object($this->db) && !is_object($this->$model)) {
			if (file_exists(APP . 'Model' . DS . $model . '.php')) {
				require_once APP . 'Model' . DS . $model . '.php';
				$this->$model = new $model($this->db);
			} else {
				//echo 'File ' . APP.'Model'.DS.$model.'.php' . ' does not exist';die;
			}
		}
	}
}