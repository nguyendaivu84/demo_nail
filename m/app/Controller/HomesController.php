<?php
App::uses('AppController', 'Controller');
class HomesController extends AppController {

	public $helpers = array();

	// public $helpers = array('Cache');
	// public $cacheAction = "1 hour";

	public function beforeFilter( ){
		// goi den before filter cha
		// parent::beforeFilter();
	}

	public function convert(){
		$this->selectModel('Product');
		$this->Product->has_field_deleted = false;
		$products = $this->Product->select_all();
		foreach ($products as $key => $value) {
			$value['deleted'] = false;
			$value['name'] = $value['description'];
			if (!$this->Product->save($value)){
				echo 'error';
			}
		}
		echo '<br>xong';
		die;
	}

	public function index(){
		$this->selectModel('Setting');
		$this->Setting->has_field_deleted = false;
		$arr_setting = $this->Setting->select_option_mobile();
		$this->set('mobile_center_menu',$this->Setting->aasort($arr_setting['mobile_center_menu'],'sort',1));
		$this->set('mobile_menu_today',$this->Setting->aasort($arr_setting['mobile_menu_today'],'sort',1));
		$this->set('mobile_quick_links_home',$this->Setting->aasort($arr_setting['mobile_quick_links_home'],'sort',1));
		$this->set('mobile_welcome',$this->Setting->aasort($arr_setting['mobile_welcome'],'sort',1));
	}

	function get_product_from_category($category_id){
		$this->selectModel('Product');
		$cond = array();
		if( $category_id != 'All' )
			$cond = array('category_id' => $category_id);
		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => 1000
		));
		$this->set('products', $products);
	}

	function get_product_name($get_product_name = ''){
		$this->selectModel('Product');
		$cond = array();
		if( strlen($get_product_name) > 0 )
			$cond = array('name' => new MongoRegex('/' . trim($get_product_name).'/i'));
		$products = $this->Product->select_all(array(
			'arr_where' => $cond,
			'arr_order' => array('_id' => -1),
			'limit'     => 1000
		));
		$this->set('products', $products);
		$this->render('get_product_from_category');
	}

	function update_product_order(){
		$product_id = $_GET['product_id'];
		$field      = $_GET['field'];
		$order_id   = $_GET['order_id'];
		$new_value  = $_GET['new_value'];

		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));

		$checkkey = -1;
		foreach ($order['products'] as $key => $value) {
			if((string)$value['product_id'] == (string)$product_id){
				$checkkey = $key;
				break;
			}
		}

		if($checkkey > -1){
			$order['products'][$checkkey][$field] = $new_value;
			$this->selectModel('Order');
			if($this->Order->save($order))
				echo 'ok';
			else
				echo 'error'.$this->Order->arr_errors_save[1];
		}
		die;
	}

	function reload_product($order_id){
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->set('order', $order);
		$this->render('add_product');
	}

	function add_product($product_id, $qty, $order_id){
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->selectModel('Product');
		$product = $this->Product->select_one(array('_id' => new MongoId($product_id)));
		if(isset($order['_id']) && isset($product['_id'])){
			$new = true;
			foreach ($order['products'] as $key => $value) {
				if((string)$value['product_id'] == (string)$product_id){
					$order['products'][$key]['qty'] += 1;
					$new = false;
				}
			}
			$order['status']= 'new';//An, dung cho check out.
			if($new)
				$order['products'][] = array(
					'product_id' => new MongoId($product_id),
					'name'       => $product['name'],
					'price'      => (isset($product['sell_price'])?$product['sell_price']:0),
					'qty'        => $qty,
					'deleted'    => false
				);
			if(!$this->Order->save($order)){
				echo 'Error in save new product to order, please contact developer. Thanks!'; die;
			}
			$this->set('order', $order);
		}else{
			echo 'Order or product does not exist!'; die;
		}
	}

	function edit_order($order_name, $order_id = ''){
		if(!$this->request->is('ajax'))die('Request is not valid.');

		if($order_name != ''){
			$save['name'] = $order_name;

			if($order_id != '')
				$save['_id'] = $order_id;
			else
				$save['products'] = array();

			$this->selectModel('Order');
			if($this->Order->save($save)){
				if($order_id != '')
					echo 'ok';
				else
					echo $this->Order->mongo_id_after_save;
			}else{
				echo 'error'.$this->Order->arr_errors_save[1]; die;
			}
		}
		die;
	}

	function view_list(){
		$this->selectModel('Order');
		$list = $this->Order->select_all(array('arr_where'=>array('status'=>'new')));
		$this->set('list',$list);
		$check_out = $this->Order->select_all(array('arr_where'=> array('status'=>'complete')));
		$this->set('check_out',$check_out);
	}

	function view_list_detail($order_id){
		$this->selectModel('Order');
		$order = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->set('list',$order);
	}

	function check_out($order_id){
		$this->selectModel('Order');
		$check_out = $this->Order->select_one(array('_id' => new MongoId($order_id)));
		$this->set('check_out_data',$check_out);

		$arr_save['status'] = 'complete';
		$arr_save['_id'] = new MongoId($order_id);
		$this->Order->save($arr_save);
	}
}