<?php echo $this->Html->css('pdp'); ?>
<div id="mobileContent">
<div class="speed-page-transition" id="page">

    <div class="page-section page-level-0 page-active" id="product-page">

      <div class="drop-shadow-container">
        <div class="freeShipBanner">
          3-DAY SHIPPING
          <b>
            FREE over $50
          </b>
        </div>
      </div>

      <div style="border-bottom:1px solid black">

        <div id="product-box" class="in end">
          <?php
          if(isset($product_detail))
          ?>
          <h3 class="brand">
           <?php echo isset($brand['brand_name'])?$brand['brand_name']:''; ?>
          </h3>
          <h3 class="display">
            <?php echo $product_detail['sku']?>
          </h3>
          <div class="flags">
          </div>
          <div id="bi-logos" class="none">
            <img alt="Beauty Insider" src="http://www.sephora.com/images/logo-bi-v.png" class="logo-bi">
            <img alt="VIB Beauty Insider" src="http://www.sephora.com/images/logo-vib-v.png" class="logo-vib">
            <img alt="VIB Rouge" src="http://www.sephora.com/images/logo-rouge-v.png" class="logo-rouge">
          </div>
          <img class="primary-product-image" src="<?php echo URL_IMAGE; ?>/<?php echo str_replace('\\', '/', $product_detail['products_upload']); ?>">
          <div class="multiple-images">
          </div>
          <h3 class="variations" data-variation-type="Color">
            <span class="variation type">
              Color:&nbsp;
            </span>
            <span class="variation val">
              Pale Nude
            </span>
            <span class="variation add">
              fair skin tones
            </span>
          </h3>
          <h3 class="price">
            <div class="product-price">
              <span class="list-price">
                <?php echo $product_detail['sell_price'];?>$
              </span>
            </div>
          </h3>
          <h3 class="sku-size">
          </h3>
        </div>
      </div>

      <div id="variation-box" class="expandable">
        <a class="expand-button">
          <span class="plus">+</span>
          <span class="minus">-</span>
          <span class="text">
            <span class="plus">more</span>
            <span class="minus">less</span>
          </span>
        </a>
      <div class="carousel-naturale-container" style="position: relative;">
        <div id="swatches-carousel" class="carousel swatches initialized" style="white-space: nowrap; -webkit-transform: translate3d(-192px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg);">
          <?php
            if(isset($product_detail['product_slide_image'])){
              foreach($product_detail['product_slide_image'] as $key => $value){
          ?>
              <a href="javascript:;" class="product-thumb carousel-item" style="display: inline-block; vertical-align: top;">
              <img src="<?php echo URL_IMAGE;?>/<?php echo str_replace("\\", '/', $value);?>"></a>
          <?php } } ?>
                </div>
                <a class="carousel-button previous" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a><a class="carousel-button next" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a>
              </div>

      <script type="text/javascript">
        $(".product-thumb").click(function(){


            $(".product-thumb").removeClass('current')
            $(this).addClass('current');

            $("#product-box").css("-webkit-animation", "product-out .5s ease-in forwards");

            setTimeout(function(){$("#product-box").css("-webkit-animation", "product-in .5s ease-out forwards");$(".primary-product-image").css("-webkit-animation", "product-in .5s ease-out forwards");},500);
            var src = $(this).children().attr("src");

            setTimeout(function(){$(".primary-product-image").attr("src",src);},500);
        });
      </script>


      <div class="static expandable" id="reviews-box">
        <div class="summary">
          <span class="stars large black">
            <span title="Average Rating: 5.00" style="width:112px;">
            </span>
          </span>
          <span class="text">
            1 Reviews
          </span>
        </div>

        <div class="action-buttons">
          <select id="bz-sortby">
            <option value="">
              Sort By
            </option>
            <option value="SubmissionTime:desc">
              Newest First
            </option>
            <option value="SubmissionTime:asc">
              Oldest First
            </option>
            <option value="Rating:desc">
              Ratings High to Low
            </option>
            <option value="Rating:asc">
              Ratings Low to High
            </option>
            <option value="helpfulness:desc">
              Helpfulness High to Low
            </option>
            <option value="helpfulness:asc">
              Helpfulness Low to High
            </option>
          </select>
          <a onclick="pdp.reviews.write()" class="write-a-review button-black">
            Write a Review
          </a>
        </div>

        <div class="dynamic-content">
          <div class="review expandable">
            <div class="top-bar">
              <span class="stars black">
                <span title="Rating: 5.00" style="width:78px;">
                </span>
              </span>
              <span class="review-data">
                03.05.14
              </span>
            </div>
            <h3>
              Wow!!! ~
              <span class="user">
                shelley2009
              </span>
            </h3>
            <p class="review-text">
              <span class="brief">
                I picked up golden nude. I was on the fence with this formula but when I wore it under Bobbi Brown's reformulated foundation stick I fell in love. My
              </span>
              <span class="remaining">
                oily skin exhaled in happiness because it kept my oil slick under control (I still had to blot once during the course of the day instead of several times) and it gave me a beautiful glow. No need for a primer because this stuff acts as a nice primer without all the nastiness that is in traditional primers. I wore this alone topping off with Hourglass Ambient Lighting powder in dim light....it was OK. But it definitely performs best under a foundation.
              </span>
            </p>
          </div>
        </div>

        <footer>
          <a class="button-black outline end-of-reviews" id="more-reviews">
            End of Reviews
          </a>
        </footer>
      </div>

      <div id="linkout-box">
        <div class="product-quantity">
          <label for="primary-quantity" class="inline">
            QTY
          </label>
          <select id="primary-quantity">
            <option value="1">
              1
            </option>
            <option value="2">
              2
            </option>
            <option value="3">
              3
            </option>
            <option value="4">
              4
            </option>
            <option value="5">
              5
            </option>
            <option value="6">
              6
            </option>
            <option value="7">
              7
            </option>
            <option value="8">
              8
            </option>
            <option value="9">
              9
            </option>
            <option value="10">
              10
            </option>
          </select>
        </div>

        <div id="find-in-store">
          <a class="find button-black movable">
            Find In Store
          </a>
          <form class="store-finder movable">
            <input type="hidden" value="1" name="convertGET">
            <input type="text" class="input-text" maxlength="10" placeholder="zip" id="find-zip">
            <button class="go button-black" type="submit">
              Go
            </button>
          </form>
        </div>

        <a class="button-black" id="share">
          Share
        </a>
      </div>

      <div class="social-share-buttons" id="social">
        <div class="center-container">
          <div class="share-btn facebook">
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fm.sephora.com%2Fcc-cream-spf-35-P385764%3FskuId%3D1582253%26icid2%3Dbd_bobbicc_US_image%26om_mmc%3Doth-fb-mobilepinbuttons-2014">
            </a>
          </div>
          <div class="share-btn pinterest">
            <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fm.sephora.com%2Fcc-cream-spf-35-P385764%3FskuId%3D1582253%26icid2%3Dbd_bobbicc_US_image%26om_mmc%3Doth-pinterest-mobilepinbuttons-2014&amp;media=http%3A%2F%2Fm.sephora.com%2Fimages%2Fsku%2Fs1582253-main-hero.jpg&amp;description=Sephora%3A%20Bobbi%20Brown%20%3A%20CC%20Cream%20SPF%2035%20%3A%20bb-cc-cream-face-makeup" target="_blank">
            </a>
          </div>
          <div class="share-btn twitter">
            <a href="https://twitter.com/share?url=http%3A%2F%2Fm.sephora.com%2Fcc-cream-spf-35-P385764%3FskuId%3D1582253%26icid2%3Dbd_bobbicc_US_image%26om_mmc%3Doth-twitter-mobilepinbuttons-2014&amp;text=Sephora%3A%20Bobbi%20Brown%20%3A%20CC%20Cream%20SPF%2035%20%3A%20bb-cc-cream-face-makeup" target="_blank">
            </a>
          </div>
          <div class="share-btn gplus">
            <a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fm.sephora.com%2Fcc-cream-spf-35-P385764%3FskuId%3D1582253%26icid2%3Dbd_bobbicc_US_image%26om_mmc%3Doth-google-mobilepinbuttons-2014">
            </a>
          </div>
          <div class="special-share-links">
            <a href="mailto: ?subject=Sephora.com%20-%20Bobbi%20Brown%20-%20CC%20Cream%20SPF%2035&amp;body=Check%20out%20this%20product%20at%20Sephora.com%20%0A%0A%20Bobbi%20Brown%20-%20CC%20Cream%20SPF%2035%0A%0Ahttp%3A%2F%2Fm.sephora.com%2Fcc-cream-spf-35-P385764%3FskuId%3D1582253%26icid2%3Dbd_bobbicc_US_image%26om_mmc%3Doth-email-mobilepinbuttons-2014" class="share-link email button-black outline">
              email
            </a>
          </div>
        </div>
      </div>

      <div id="exclusive-message-box">
      </div>

      <dl id="information-accordion" class="accordion-dl">
        <dt class="no-select" data-analytics-accordion="description">Description<span class="expand-button"><span class="plus">+</span><span class="minus">-</span></span></dt>
        <dd class="dd_description" style="display:none">
          <span><?php echo $product_detail['product_description']; ?></span>
          <span class="item-number">Item #1573377</span>
        </dd>
    </dl>

  <script type="text/javascript">
    $(".accordion-dl").click(function(){
      $(".dd_description",this).animate({
        height:'toggle'
      });
    });
    $(".no-select").click(function(){
      $(this).toggleClass('active');
    })
  </script>
        </div>


  <div id="ymal-box">
  <h3>You Might Also Like</h3>
  <div class="carousel-naturale-container" style="position: relative;">
    <div id="ymal" class="carousel initialized" style="white-space: nowrap;">
      <div class="product-item carousel-item" data-index="0" style="display: inline-block; vertical-align: top;">
        <div class="product-image">
          <a href="http://m.sephora.com/argan-illuminizer-P281607" data-productid="P281607">
            <img alt="Josie Maran - Argan Illuminizer" src="http://www.sephora.com/productimages/sku/s1308196-main-hero.jpg" onerror="Sephora.ui.error.productImage(this,&quot;/images/image-not-available-97.png&quot;);"></a>

        </div>
        <div class="new-love">
          </div>
        <a href="http://m.sephora.com/argan-illuminizer-P281607" data-productid="P281607">
            <span class="product-info">
                <span class="name OneLinkNoTx">
                    <span class="brand">
                        Josie Maran</span>
                    Argan Illuminizer</span>
                <span class="sku-price">
          <span class="list-price"><span class="currency">$</span><span class="price">28.00</span></span>
          </span>
        <span class="flags-market">
                  </span>
              </span>
            </a>
    <div class="out-of-stock" style="display: none;">
    <b>not in stock</b><span>:</span> <a href="#" data-sku_number="1308196" class="btn-email-when-in-stock"></a>
  </div>
</div><div class="product-item carousel-item" data-index="1" style="display: inline-block; vertical-align: top;">
        <div class="product-image">
          <a href="http://m.sephora.com/argan-matchmaker-serum-foundation-P280202" data-productid="P280202">
            <img alt="Josie Maran - Argan Matchmaker Serum Foundation" src="http://www.sephora.com/productimages/sku/s1308121-main-hero.jpg" onerror="Sephora.ui.error.productImage(this,&quot;/images/image-not-available-97.png&quot;);"></a>

        </div>
        <div class="new-love">
          </div>
        <a href="http://m.sephora.com/argan-matchmaker-serum-foundation-P280202" data-productid="P280202">
            <span class="product-info">
                <span class="name OneLinkNoTx">
                    <span class="brand">
                        Josie Maran</span>
                    Argan Matchmaker Serum Foundation</span>
                <span class="sku-price">
          <span class="list-price"><span class="currency">$</span><span class="price">42.00</span></span>
          </span>
        <span class="flags-market">
                  </span>
              </span>
            </a>
    <div class="out-of-stock" style="display: none;">
    <b>not in stock</b><span>:</span> <a href="#" data-sku_number="1308121" class="btn-email-when-in-stock"></a>
  </div>
</div><div class="product-item carousel-item" data-index="2" style="display: inline-block; vertical-align: top;">
        <div class="product-image">
          <a href="http://m.sephora.com/coconut-waterproof-cheek-gelee-P379101" data-productid="P379101">
            <img alt="Josie Maran - Coconut Watercolor Cheek Gelée" src="http://www.sephora.com/productimages/sku/s1506559-main-hero.jpg" onerror="Sephora.ui.error.productImage(this,&quot;/images/image-not-available-97.png&quot;);"></a>

        </div>
        <div class="new-love">
          </div>
        <a href="http://m.sephora.com/coconut-waterproof-cheek-gelee-P379101" data-productid="P379101">
            <span class="product-info">
                <span class="name OneLinkNoTx">
                    <span class="brand">
                        Josie Maran</span>
                    Coconut Watercolor Cheek Gelée</span>
                <span class="sku-price">
          <span class="list-price"><span class="currency">$</span><span class="price">22.00</span></span>
          </span>
        <span class="flags-market">
                  </span>
              </span>
            </a>
    <div class="out-of-stock" style="display: none;">
    <b>not in stock</b><span>:</span> <a href="#" data-sku_number="1506559" class="btn-email-when-in-stock"></a>
  </div>
</div><div class="product-item carousel-item" data-index="3" style="display: inline-block; vertical-align: top;">
        <div class="product-image">
          <a href="http://m.sephora.com/argan-matchmaker-powder-foundation-spf-20-P305134" data-productid="P305134">
            <img alt="Josie Maran - Argan Matchmaker Powder Foundation SPF 20" src="http://www.sephora.com/productimages/sku/s1392364-main-hero.jpg" onerror="Sephora.ui.error.productImage(this,&quot;/images/image-not-available-97.png&quot;);"></a>

        </div>
        <div class="new-love">
          </div>
        <a href="http://m.sephora.com/argan-matchmaker-powder-foundation-spf-20-P305134" data-productid="P305134">
            <span class="product-info">
                <span class="name OneLinkNoTx">
                    <span class="brand">
                        Josie Maran</span>
                    Argan Matchmaker Powder Foundation SPF 20</span>
                <span class="sku-price">
          <span class="list-price"><span class="currency">$</span><span class="price">34.00</span></span>
          </span>
        <span class="flags-market">
                  </span>
              </span>
            </a>
    <div class="out-of-stock" style="display: none;">
    <b>not in stock</b><span>:</span> <a href="#" data-sku_number="1392364" class="btn-email-when-in-stock"></a>
  </div>
</div>
</div><a class="carousel-button previous" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a><a class="carousel-button next" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a></div></div>

      <div id="shop-brand-box">
        <a href="http://m.sephora.com/bobbi-brown" class="button-black">
          Shop All Bobbi Brown
        </a>
      </div>

    </div>

    <div class="page-section page-level-1 huge-zoom-available" id="zoom-product-page">
      <a class="zoom-back">
        Back to Product Details
      </a>
      <a style="display: inline-block;" id="zoom-box">
      </a>
    </div>

    <div class="page-section page-level-1" id="store-locator-page">

      <h2>
        Find in a Sephora Store
      </h2>
      <div onclick="page.load('product-page',displayActionButtons); wa.init();" class="product-clone" id="find-product-clone">
        <div class="product-info" style="background-image: url(&quot;/productimages/sku/s1582253-main-grid.jpg&quot;);">
          <div class="brand">
            Bobbi Brown
          </div>
          <div class="display">
            CC Cream SPF 35
          </div>
          <div class="sku">
            <span class="item-number">
              Item #1582253
            </span>
          </div>
          <div class="variations">
          </div>
        </div>
      </div>
      <div class="dynamic-content">
      </div>
      <div class="back">
        <a href="javascript:page.load('product-page',displayActionButtons);" class="button-black">
          Back to Product
        </a>
      </div>
    </div>

    <div class="page-section page-level-2" id="store-map-page">
      <div onclick="page.load('store-locator-page',displayActionButtons)" id="mapped-store">
      </div>
      <a id="mapped-phone">
      </a>
      <div class="map" id="map-canvas">
      </div>
      <div class="back">
        <a href="javascript:page.load('product-page',displayActionButtons); wa.init();" class="button-black">
          Back to Product
        </a>
      </div>
    </div>

    <div class="page-section page-level-1" id="write-review-page">

      <h2>
        Write a Review
      </h2>
      <div onclick="page.load('product-page',displayActionButtons); wa.init();" class="product-clone" id="write-review-clone">
        <div class="product-info" style="background-image: url(&quot;/productimages/sku/s1582253-main-grid.jpg&quot;);">
          <div class="brand">
            Bobbi Brown
          </div>
          <div class="display">
            CC Cream SPF 35
          </div>
          <div class="sku">
            <span class="item-number">
              Item #1582253
            </span>
          </div>
          <div class="variations">
          </div>
        </div>
      </div>

      <div class="nickname-box">
        <span class="nicknamelabel">
          Your Nickname:
        </span>
        <span class="nickname">
        </span>
      </div>

      <div id="rating">
        <span class="star-label">
          Overall Review:
        </span>
        <div data-rating="one" class="stars black large one no-rating">
        </div>
        <div data-rating="two" class="stars black large two no-rating">
        </div>
        <div data-rating="three" class="stars black large three no-rating">
        </div>
        <div data-rating="four" class="stars black large four no-rating">
        </div>
        <div data-rating="five" class="stars black large five no-rating">
        </div>
      </div>

      <div id="review-box">
        <input type="text" maxlength="50" placeholder="title of your review" name="write-title" class="write-title input-text">
        <textarea placeholder="*your review" name="write-body" rows="6" class="write-body" type="text">
        </textarea>
      </div>

      <div id="review-error">
      </div>

      <div class="details">
        <p class="write-body-subtext">
          20 character minimum.  Please avoid inappropriate language, personal contact info, and non-Sephora.com URLs.
        </p>
        <p>
          <a href="http://reviews.sephora.com/content/8723illuminate/guidelines.htm" target="_blank">
            See Full Ratings &amp; Reviews guidelines.
          </a>
        </p>
      </div>

      <div class="write-buttons">
        <button onclick="page.load('product-page',displayActionButtons); wa.init();" style="float:left;" class="button-black outline">
          Cancel
        </button>
        <button onclick="pdp.reviews.post()" class="button-black">
          Post
        </button>
      </div>
    </div>

    <div class="page-section page-level-2" id="review-complete-page">
      <img src="/img/banners/RatingsAndReviewsTY.png" class="write-banner">
      <div class="thanks-text">
        <p class="red-thanks">
          Thanks for submitting your review &ndash; you're a rock star!
        </p>
        <p>
          Reviews are typically posted within
          <span id="typical-post-time">
            72
          </span>
          hours of the time you submitted them, so stay tuned.
        </p>
      </div>
      <div class="success-buttons">
        <a onclick="location.reload()" class="button-red">
          Return to product Page
        </a>
        <a href="http://m.sephora.com/shop/?sortBy=P_RATING:1" class="button-red">
          see top&ndash;rated products
        </a>

      </div>

    </div>

  </div>
</div>

<div id="product-actions" class="lovable">
	<div id="love-box">
		<button data-sku="1573369" data-pid="P384748" class="love-btn love"> <i></i>
			<span class="loved">Loved</span>
			<span class="love">Love</span>
			<span class="unlove">Unlove</span>
		</button>
	</div>
	<div id="add-to-basket" onclick="add_product('<?php echo $product_detail['_id'] ?>')">
		<a>Add To Basket</a>
	</div>
</div>

<script type="text/javascript">
function add_product(id){
	$.ajax({
		url: "<?php echo URL ?>/shops/added_a_product/" + id,
		success: function(html) {
			$("#inline-basket").html(html);
      if( $("#total_item_cart").attr("id") != undefined ){
        var contain = $("#header-basket");
        $(".item-count", contain).addClass("active").html($("#total_item_cart").val());
      }
		}
	});
}
</script>
