<?php echo $this->Html->css('search.v3'); ?>
<?php echo $this->Html->css('pdp'); ?>
<div id="mWrapper">
  <div id="mobileContent">       
    <div class="speed-page-transition" id="search-container" style="transform: translateX(-100%);">
      
      <div class="page-section page-level-0" id="page-loading">
        <h1>
          Loading...
        </h1>
      </div>
      
      <div class="page-section page-level-1 page-active" id="page-results">
        <h1 id="products-header">
          <span class="name">
            Sale
          </span>
          <span class="count">
          </span>
        </h1>
        <p class="error-message" style="display: none;">
        </p>
        <div id="options">
          
  		<select onchange="productList.sortBy(this.value)" id="sort-by">
            <option value="-1">
              Sort By
            </option>
            <option value="-1">
              relevancy
            </option>
            <option value="P_BEST_SELLING:1::P_RATING:1::P_PROD_NAME:0">
              best selling
            </option>
            <option value="P_NEW:1::P_START_DATE:1">
              new
            </option>
            <option value="P_RATING:1">
              top rated
            </option>
            <option value="P_SEPH_EXCLUSIVE:1">
              exclusive
            </option>
            <option value="price:1">
              price high to low
            </option>
            <option value="price:0">
              price low to high
            </option>
            <option value="P_BRAND_NAME:0">
              brand name
            </option>
  		</select>
          
  		<div class="buttons">
            <a onclick="javascript:productList.filter();" id="view-filters">
              Filter
              <span class="active-filter-count">
              </span>
            </a>
  		</div>
  		
          <span class="count">
           <?php
        	if(isset($products))    
           	$tmp = iterator_to_array($products);
  					echo count($tmp);?> items
          </span>
  		
  		<div id="view-style">
            <a onclick="javascript:productList.listStyle(&quot;list&quot;);" class="list">
            </a>
            <a onclick="javascript:productList.listStyle(&quot;grid&quot;);" class="grid active">
            </a>
  		</div>
  		
        </div>

        <div class="product-list grid" id="product-list">
          <div id="product-box" class="in">
            <?php
                foreach($tmp as $key => $value){
             ?>
              <a class="product-item product-row-start" href="<?php echo URL;?>/shops/product_detail/<?php echo $key;?>" data-pid="P383443" data-sku="1545524" data-base-height="256" data-base-padding-top="6" style="height: 271px; padding-top: 21px;">
                <img class="product-image" src="<?php echo URL_IMAGE; ?>/<?php echo str_replace('\\', '/', $value['products_upload']); ?>">
                <span class="product-info">
                  <span class="name">
                    <span class="brand">
                      <?php echo isset($brand['banner_name'])?$brand['banner_name']:''; ?>
                    </span>
                    <span class="display-name">
                      <?php echo $value['sku'];?>
                    </span>
                  </span>
                  <span class="price sale">
                    <span class="list-price">
                      <?php echo $value['sell_price'];?>$
                    </span>
                  </span>
                  <span class="flags">
                    <span>
                      exclusive
                    </span>
                    <span>
                      limited edition
                    </span>
                  </span>
                  <span class="stars">
                    <span title="Rating: 3.41" style="width:54px;">
                    </span>
                  </span>
                </span>
              </a>
            <?php } ?>
          </div>
        </div>

        <div id="loading-products" class="">
          Loading products...
        </div> 
        
      </div>
      
      <div class="page-section page-level-2" id="page-filter">
        <div class="filter-title-bar">
  		<a onclick="javascript:filters.click.clearAll();" style="float:left;" class="clear button-black outline">
            Clear
          </a>
  		<a onclick="javascript:filters.click.done();" style="float:right;" class="done button-black">
            Done
          </a>
  		<h3>
            Filter
          </h3>
        </div>
        <span class="filter-count">
        </span>
        <div id="filters-accordion">
        </div>
      </div>
      
      <div class="page-section page-level-3" id="page-filter-list">
        <div class="filter-title-bar">
  		<a onclick="javascript:filters.click.clear();" style="float:left;" class="clear button-black outline">
            Clear
          </a>
  		<a onclick="javascript:filters.click.apply();" style="float:right;" class="apply button-black">
            Apply
          </a>
  		<h3>
          </h3>
        </div>
        <span class="filter-count">
        </span>
        <div id="filter-list-accordion">
        </div>
      </div>
      
    </div>
    
  </div>
</div>
</div>
