<div class="inline-basket-wrapper dynamic">
	<div class="inline-basket">
		<div class="inline-basket-inner">
			<p class="freeshipping">You now qualify for <span class="free">free standard shipping!</span></p>
			<h2>Just added to basket:</h2>
			<div class="basket-line-items">
				<div class="product-row container" id="sku1573369">
					<img class="product-image" src="<?php echo URL_IMAGE; ?>/<?php echo str_replace('\\', '/', $product['products_upload']); ?>">
					<div class="product-price">
						<span class="list-price">
							<span class="currency">C$</span><span class="price"><?php echo number_format($my_cart[$product_id]['product_quantity']*$product['sell_price']) ?></span>
						</span>
					</div>
					<div class="product-description">
						<h3>
							<span class="brand"> <?php echo isset($brand['brand_name'])?$brand['brand_name']:''; ?> <span class="product-name">Argan Matchmaker Complexion Kit</span></span>
						</h3>
						<div class="info-row">
							<span class="sku">
								<span class="label">Item #</span>
								<span class="value">1573369</span>
							</span>
							<span class="qty">
								<span class="label qty">Quantity:</span>
								<span class="value"><?php echo $my_cart[$product_id]['product_quantity'] ?></span>
							</span>
						</div>
						<div class="info-row variation">
							<span class="label">Color:</span>
							<span class="value">Fair/ Light</span>
						</div>
					</div>
				</div>
			</div>
			<div id="inline-errors"></div>
			<div class="actions container">
				<a href="#" onclick="$('.inline-basket-wrapper').remove()" class="btn btn-continue-shopping"> <i class="arrow-mini arrow-left"></i>
					Continue Shopping</a>
				<a href="<?php echo URL ?>/shops/checkout" class="btn btn-primary btn-checkout">basket <i class="arrow-mini arrow-right"></i></a>
			</div>
		</div>
		<i class="close icon" onclick="$('.inline-basket-wrapper').remove()"></i>
	</div>
	<?php
		if( $this->Session->check('my_cart') ){
			$my_cart = $this->Session->read('my_cart');
			echo '<input type="hidden" id="total_item_cart" value="'.count($my_cart).'">';
		}
	?>
</div>