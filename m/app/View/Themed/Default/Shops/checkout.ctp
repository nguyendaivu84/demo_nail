<?php echo $this->Html->css('view_cart'); ?>
<div id="mobileContent">
	<div class="bigbigbasket ">
		<div class="shopping-basket basket">
			<div class="basket-banner"></div>
			<div class="basket-actions pull-right container">
				<ul>
					<li class="action-checkout">
						<a href="#" class="btn btn-alt btn-lg btn-block">
							checkout
							<span class="arrow arrow-right"></span>
						</a>
					</li>
					<li>
						<a class="btn btn-link btn-shop" href="http://m.tonynail.com/argan-matchmaker-complexion-kit-P384748?skuId=1573369">
							<p>BACK</p>
						</a>
					</li>
				</ul>
				<div class="calculation">
					<div class="total-row subtotal container">
						<div class="label">Subtotal</div>
						<div class="value">
							C$
							<span class="sub">82.00</span>
						</div>
					</div>
				</div>
			</div>
			<div class="free-shipping">
				<div class="top-promo-msg">
					<p class="free-shipping ">
						You now qualify for
						<span class="free">free shipping.</span>
					</p>
					<p class="free-flash-shipping hide">
						You now qualify for
						<span class="free">free 2 Day shipping.</span>
					</p>
					<p class="free-flash-hazmat-shipping hide">
						You now qualify for
						<span class="free">free ground shipping.</span>
					</p>

					<p class="free-rouge-hazmat-shipping hide"> <b class="label-rouge">VIB Rouge</b>
						members enjoy
						<span class="free">Free Ground Shipping</span>
					</p>
					<p class="free-shipping-with-amount hide">
						Spend $
						<span class="free-shipping-amount"></span>
						or more for free shipping.
					</p>
					<p class="rouge-free-ship hide"> <b class="label-rouge">VIB Rouge</b>
						members enjoy
						<span class="free">free 3-day shipping</span>
						.
					</p>
				</div>
			</div>

			<div class="basket-contents">
				<div class="basket-items">
					<?php foreach ($my_cart as $key => $product) { ?>
					<div class="item basket-item" id="sku-1573369-Standard">
						<div class="product-image">
							<a href="<?php echo URL; ?>/shops/product_detail/<?php echo $product['product_id'] ?>">
								<img src="<?php echo URL_IMAGE; ?>/<?php echo str_replace('\\', '/', $product['product_image']); ?>"></a>
						</div>
						<div class="product-description">
							<h2>
								<a href="/argan-matchmaker-complexion-kit-P384748?skuId=1573369">
									<span class="brand"><?php echo $product['brand_name'] ?></span>
									<?php echo $product['product_name'] ?>
								</a>
							</h2>
							<div class="item-purchase-info">
								<div class="sku-and-variant">
									<div>
										<span class="sku">
											<span class="label">Item #</span>
											<span class="value OneLinkNoTx">1573369</span>
										</span>

									</div>
									<div class="info-row variation">
										<span class="color">
											<span class="label">Color</span>
											<span class="value OneLinkNoTx">Fair/ Light</span>
										</span>
									</div>
								</div>
								<div class="quantity-action item-actions">
									<div class="product-price">
										<span class="list-price">
											<span class="currency">C$</span><span class="price"><?php echo number_format($my_cart[(string)$product['product_id']]['product_quantity']*$product['product_price']) ?></span>
										</span>
									</div>
									<div class="product-quantity">
										<label class="inline" for="1573369">QTY</label>
										<select id="1573369" class="form-control OneLinkNoTx quantitySelector " name="/atg/commerce/order/purchase/CartModifierFormHandler.itemsMap.1573369">
											<?php for ($i=1; $i <= 10; $i++) {
												echo '<option value="'.$i.'"';
												if($i == $product['product_quantity']){
													echo 'selected="selected"';
												}
												echo '>QTY: '.$i.'</option>';
											} ?>
										</select>
										<span class="remove">
											<a href="#" id="ci239240000299" class="basketRemove_Href">x</a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>

				<div class="basket-summary container">
					<div class="promotion-box container">
						<div id="promo-proxy-add-box">
							<h2>ADD PROMOTION</h2>
							<p>
								Enter Promo Code
								<span>(1 code per offer)</span>
							</p>
							<input class="promo-code" type="text">
							<a href="javascript:;" class="button-black apply">ADD</a>
							<div class="current-promo-code"></div>
						</div>
						<h2 class="promo-box-header">SEE ALL OFFERS</h2>
						<div class="promo-proxy-items"></div>
					</div>
					<div class="samples">
						<ul class="free-samples-container _bb-accordion">
							<li>
								<a class="free-samples-header">Add 5 Free Samples</a>
							</li>
							<li class="sample-item-duplicates"></li>
						</ul>
					</div>
				</div>

				<div class="update-and-total basket-actions">
					<div class="basket-totals with-promo-box">
						<div class="total-row container" style="display:none;">
							<div class="label">Merchandise Total</div>
							<div class="value">
								C$
								<span class="merch-total"></span>
							</div>
						</div>

						<div class="total-row container giftcard" style="display:none;">
							<div class="label">Gift Card Purchase</div>
							<div class="value">
								C$
								<span class="gift-card-total"></span>
							</div>
						</div>

						<div class="total-row container e-giftcard" style="display:none;">
							<div class="label">eGift Certificate Purchase</div>
							<div class="value">C$<span class="egift-total"></span></div>
						</div>

						<div class="total-row container discount" style="display:none;">
							<div class="label">Discounts</div>
							<div class="value">-C$<span class="cost"></span></div>
						</div>
						<p class="code-applied" style="display:none;">
							<a href="" class="icon icon-remove"></a>
							<span></span>
						</p>
						<a href="#" class="pop-info" rel="#pop-shipping">Shipping &amp; Handling</a>
						<div class="total-row subtotal container">
							<div class="label">Subtotal</div>
							<div class="value">C$<span class="sub">82.00</span></div>
						</div>
					</div>
				</div>
				<div class="cart-controls basket-actions">
					<a class="btn btn-link btn-shop" href="http://m.tonynail.com/argan-matchmaker-complexion-kit-P384748?skuId=1573369">
						<p>BACK</p>
					</a>
					<li class="action-checkout">
						<button class="canadaswitch" value="canadaswitch" name="canadaswitch" onclick="window.location='/checkout'">checkout</button>
					</li>
					<div class="calculation">
						<div class="total-row subtotal container">
							<div class="label">Subtotal</div>
							<div class="value">
								C$
								<span class="sub">82.00</span>
							</div>
						</div>
					</div>
					<button class="canada-checkout" value="canada" name="canada"></button>
				</div>
				<div class="sweet-steal">
					<div class="container">
						<div class="grab-item  container">
							<div class="grab-img">
								<div class="product-image">
									<img src="<?php echo URL; ?>/theme/default/img/s1474154-main-Sgrid.jpg">
									<a id="sku_1474154" href="#" class="qlook" rel="#mini-quick-look">QUICK LOOK</a>
								</div>
							</div>
							<div class="grab-info">
								<p>
									<span class="brand OneLinkNoTx">Benefit Cosmetics</span>
									<br>
									<span class="product-name OneLinkNoTx">Goodtime Gals</span>
									<br>
									<span class="sale-price">C$18.00</span>
								</p>

								<button class="btn btn-default btn-add-to-basket" data-sku_number="1474154" id="addSweetSteal_href" name="add-to-basket" value="add to basket" type="submit">Add To Basket</button>

							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>