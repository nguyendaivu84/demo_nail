<div id="mobileContent">
      <style>
        h1 {
          padding-bottom:5px;
        }
        
        div.sort-by select {
          width:95px!important;
        }
        #seeall,#specialfilters {
          color:white;
          font-family:arial;
          font-weight:700;
          background:black;
          padding:5px 15px;
          display:inline-block;
          margin:0px 0px 0px 7px;
        }
        .viewing-product {
          position: relative;
          top: -2px;
          width:41px;
        }
        span.flags {
          display:block;
        }
        span.exclusiveflag {
          font-size:8px;
          color:white;
          font-family:arial;
          background:#d20200;
          padding:1px 3px;
          display:inline-block;
          margin:0px 2px 2px 0px;
          text-transform:uppercase;
        }
        span.limitededitionflag {
          font-size:8px;
          color:white;
          font-family:arial;
          background:#646464;
          padding:1px 3px;
          display:inline-block;
          margin:0px 2px 2px 0px;
          text-transform:uppercase;
        }
        span.newflag {
          font-size:8px;
          color:white;
          font-family:arial;
          background:black;
          padding:1px 3px;
          display:inline-block;
          margin:0px 2px 2px 0px;
          text-transform:uppercase;
        }
        span.onlineonlyflag {
          font-size:8px;
          color:white;
          font-family:arial;
          background:#5529c6;
          padding:1px 3px;
          display:inline-block;
          margin:0px 2px 2px 0px;
          text-transform:uppercase;
        }
        #filtermodal {
          padding:10px 0 40px 0;
          display:block;
          z-index:20;
          background:white;
          min-height:100%;
        }
        #filtermodal h4 {
          margin-left:80px;
          font-family:"Helvetica Neue";
          font-size:20px;
          text-transform:uppercase;
          text-align:center;
        }
        #filtermodal .vresults {
          float:right;
          font-size:10px;
          font-weight:700;
          color:white;
          font-family:arial;
          background:black;
          padding:5px 12px;
          display:inline-block;
          margin:2px 10px 0px 0px;
        }
        #filters {
          padding:0;
          margin:10px 10px 0;
          border-left:1px solid #666;
          border-right:1px solid #666;
          border-bottom:1px solid #666;
        }
        #filters > li  {
          display:block;
        }
        #filters > li > ul {
          display:block;
        }
        #filters > li > ul > li {
          font-size:14px;
          text-transform:capitalize;
          color:#666;
          padding:9px 5px;
          border-bottom:1px solid #666;
        }
        #filters > li > ul > li:last-child {
          border-bottom:0px solid white;
        }
        #filters > li > ul > li {
          background: url(http://m.sephora.com/img/common/arrows/black-arrow-right@2x.png) no-repeat right;
          background-repeat: no-repeat;
          background-size: 6px 15px;
          background-position: 90% 50%;
        }
        
        #filters > li > ul > ul {
          display:none;
        }
        #filters > li li a {
          display:block;
        }
        
        #filters > li > h2 {
          background:#ccc;
          font-weight:700;
          color:black;
          border-bottom:1px solid #666;
          text-align:center;
          border-top:1px solid #666;
          text-transform:uppercase;
          font-size:14px;
          font-family:arial;
          padding:10px 10px;
        }
        #filters > li > h2 > span {
          font-style:normal;
          font-weight:700;
          display:inline-block;
          padding-left:40px;
          width:90px;
          text-align:right;
          height:12px;
          overflow:hidden;
          color:#cc0001;
        }
        #filters > li > h2.open {
          background:white;
          color:#cc0001;
          border-bottom:1px solid #666;
          font-weight:700;
          padding:7px;
          text-indent:25px;
        }
        #filters > li > h2.open:after {
          float:right;
          font-size:10px;
          font-weight:700;
          color:white;
          font-family:arial;
          padding:5px 1px;
          display:inline-block;
          margin:0px 0px 0px 0px;
          background:url(http://m.sephora.com/img/search/branddown.png) no-repeat;
          text-indent:6669px;
          background-size:25px;
          content:"a";
          position: relative;
          top: -2px;
          width:25px;
          height:25px;
        }
        #filters .price-container {
          display:block;
        }
        #filtermodal p.clearall {
          text-align:center;
          border-bottom:1px solid #666;
          border-left:1px solid #666;
          border-right:1px solid #666;
          text-transform: uppercase;
          font-size: 14px;
          padding: 7px;
        }
        #filtermodal p.clearall span {
          font-family:arial;
          color:white;
          background:black;
          padding: 5px 12px;
          font-weight: 700;
          font-size: 10px;
        }
        
        .search-results .product-grid .product-item {
          height: 215px;
        }
        .pagination a.prev span{
          text-indent:6669px;
          background:url(http://m.sephora.com/img/search/searchleft.png) no-repeat;
          display:inline-block;
          width:25px;
          height:25px;
          background-size:25px;
        }
        .pagination a.next span {
          text-indent:6669px;
          background:url(http://m.sephora.com/img/search/searchright.png) no-repeat;
          display:inline-block;
          width:25px;
          height:25px;
          background-size:25px;
        }
        .search-results .pagination .next::after, .search-results .pagination .prev::before {
          content: "";
        }
        .pagination .next, .pagination .prev {
          top: 8px !important;
        }
        
        #mobileHeader {
          position:relative;
          z-index:2000;
        }
        #mobileContent {
          position:relative;
          z-index:500;
        }
        .popup {
          z-index:999999 !important;
        }
        
        #special-notice h1 {
          font-size:12px;
          margin:10px 10px 0 10px;
          text-align:center;
        }
        
        /* ALPHASTRIPE */
        #alpha-stripe {
          z-index:1000;
          top:0px;
          right:0px;
          padding:2px 0;
          text-align:center;
          display:-webkit-box;
          display:-moz-box;
          display:box;
          -webkit-box-orient:vertical;
          -moz-box-orient:vertical;
          box-orient:vertical;
          position:fixed;
          top:0px;
          right:0px;
          font-size:10px;
          text-transform:uppercase;
          background-color:black;
          color:white;
          -webkit-transition:-webkit-transform .3s ease-out;
          -webkit-transform:translateX(1000px);
        }
        #alpha-stripe {
          background: -moz-linear-gradient(-45deg,  #4d4d4d 5%, #242424 63%, #000000 100%);
          /* FF3.6+ */ background: -webkit-gradient(linear, left top, right bottom, color-stop(5%,#4d4d4d), color-stop(63%,#242424), color-stop(100%,#000000));
          /* Chrome,Safari4+ */ background: -webkit-linear-gradient(-45deg,  #4d4d4d 5%,#242424 63%,#000000 100%);
          /* Chrome10+,Safari5.1+ */ background: -o-linear-gradient(-45deg,  #4d4d4d 5%,#242424 63%,#000000 100%);
          /* Opera 11.10+ */ background: -ms-linear-gradient(-45deg,  #4d4d4d 5%,#242424 63%,#000000 100%);
          /* IE10+ */ background: linear-gradient(135deg,  #4d4d4d 5%,#242424 63%,#000000 100%);
          /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4d4d4d', endColorstr='#000000',GradientType=1 );
          /* IE6-9 fallback on horizontal gradient */ }
        #alpha-stripe.active {
          box-shadow:0 0 4px black;
        }
        #alpha-stripe li {
          -webkit-box-flex:1;
          -moz-box-flex:1;
          box-flex:1;
          padding:0 16px;
        }
        
      </style>
<div id="breadcrumbs">
  <ul class='breadcrumb' role='navigation'>
    <li>
      <a href="<?php echo URL;?>">
        Home
      </a>
    </li>
    <li>
      Brands
    </li>
  </ul>
</div>
<div id="special-notice"></div>
  <div id="filtermodal">
    <ul id="filters">
      <li id="letter-a">
        <?php 
          if(isset($brand_name))
          foreach($brand_name as $key => $value){
        ?>
        <h2 class="alt-bold text-upper">
          <?php echo $key; ?>
        </h2>
        <ul>
          <?php 
            foreach($value as $k => $v){
          ?>
          <li>
            <a href="<?php echo URL;?>/shops/show_brand_list/<?php echo $k;?>">
              <?php echo $v ;?>
            </a>
          </li>
          <?php }?>
        </ul>
         <?php } ?>
      </li>
    </ul>
  </div>
</div>