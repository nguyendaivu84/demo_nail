<?php echo $this->Html->css('brands'); ?>
<div id="mobileContent" style="height: auto;">


<!-- BBTEMPLATE: brands/brandpage -->

<h1 id="textual-header"><?php echo isset($data_brand['banner_name'])?$data_brand['banner_name']:''; ?></h1>
<div id="description-container">
	<div>
		<a  class="content2" id="show-brand-info">About the Brand +</a>
	      <div class="desc desc-less content1" style="display:none">
			  <p>Since the creation of the iconic Colonia fragrance in 1916, Acqua di Parma has symbolized the best of the Italian lifestyle: a passion for beauty, an eye for detail,  and the contemporary interpretation of luxury. Today, all Acqua di Parma products continue to be exclusively hand made in Italy by skilled master craftsmen.</p>
		  </div>
	</div>
	<script type="text/javascript">
	    $(".content2").click(function(){
	      $(".content1",this).animate({
	        height:'toggle'
	      });
	    });
  </script>
</div>
	<div id="brand-header">
		<div class="brand-logo">
      <a href="/acqua-di-parma" style="background-image:url(http://www.sephora.com/contentimages/brands/acquadiparma/5847_logo_279.png)">
		</a>
		</div>
	</div>

<div id="landing-navigation" class="linear-tree-nav current-expanded">
	<a href="javascript:;" class="category-expander current"><span><?php echo isset($data_brand['banner_name'])?$data_brand['banner_name']:''; ?></span></a>
	<div class="current-sub-listing"><a data-ref="900096" class="view-all">
		<span>View All</span>
		<span class="count">undefined</span></a>
		<?php
		if(isset($data_product))
			foreach($data_product as $key => $value){
				pr($value);
		 ?>
		<a href="javascript:;" data-node="1050072" class=" next-tier no-expansion">
			<span><?php echo $value['category']; ?>
				<span class="count"><?php  echo count($value['product_id']) ?></span>
			</span>
		</a>
		<?php  }?>
	</div>
</div>

<div id="landing-bcc-content">
  <div class="carousels"><div class="carousel-container even" data-name="Acqua_Di_Parma_Editor's_Picks_Carousel"><h2 class="carousel-title">Editors' Picks</h2><div class="carousel-naturale-container" style="position: relative;"><ul class="carousel initialized" style="white-space: nowrap;"><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/colonia-intensa-oud-P377678?icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P377678_image&amp;skuId=1507003" data-pid="P377678" data-sku="1507003"><img class="product-image" src="/productimages/sku/s1507003-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Colonia Intensa Oud</span></span><span class="price"><span class="list-price">
				$210.00
			</span></span><span class="stars"><span style="width:63px;" title="Rating: 4.00"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/iris-nobile-sublime-P376827?icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P376827_image&amp;skuId=1479484" data-pid="P376827" data-sku="1479484"><img class="product-image" src="/productimages/sku/s1479484-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Iris Nobile Sublime</span></span><span class="price"><span class="list-price">
				$148.00
			</span></span><span class="stars"><span style="width:78px;" title="Rating: 5.00"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/blu-mediterraneo-fico-di-amalfi-P307801?icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P307801_image&amp;skuId=1417591" data-pid="P307801" data-sku="1417591"><img class="product-image" src="/productimages/sku/s1417591-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Blu Mediterraneo Fico Di Amalfi</span></span><span class="price"><span class="list-price">
				$98.00
			</span></span><span class="stars"><span style="width:69px;" title="Rating: 4.38"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/blu-mediterraneo-bergamotto-di-calabria-P307802?icid2=Acqua_Di_Parma_Editor's_Picks_Car ousel_P307802_image&amp;skuId=1417633" data-pid="P307802" data-sku="1417633"><img class="product-image" src="/productimages/sku/s1417633-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Blu Mediterraneo Bergamotto Di Calabria</span></span><span class="price"><span class="list-price">
				$98.00
			</span></span><span class="stars"><span style="width:72px;" title="Rating: 4.60"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/colonia-essenza-P269110?icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P269110_image&amp;skuId=1284462" data-pid="P269110" data-sku="1284462"><img class="product-image" src="/productimages/sku/s1284462-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Colonia Essenza</span></span><span class="price"><span class="list-price">
				$102.00
			</span></span><span class="stars"><span style="width:74px;" title="Rating: 4.71"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/gelsomino-nobile-P298761?skuId=1382985&amp;icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P298761_image&amp;skuId=1382985" data-pid="P298761" data-sku="1382985"><img class="product-image" src="/productimages/sku/s1382985-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Gelsomino Nobile</span></span><span class="price"><span class="list-price">
				$180.00
			</span></span><span class="stars"><span style="width:73px;" title="Rating: 4.64"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/blu-mediterraneo-mirto-di-panarea-P307804?skuId=1417625&amp;icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P307804_image&amp;skuId=1417625" data-pid="P307804" data-sku="1417625"><img class="product-image" src="/productimages/sku/s1417625-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Blu Mediterraneo Mirto Di Panarea</span></span><span class="price"><span class="list-price">
				$144.00
			</span></span><span class="stars"><span style="width:63px;" title="Rating: 4.00"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/magnolia-nobile-P252213?skuId=1226893&amp;icid2=Acqua_Di_Parma_Editor's_Picks_Carousel_P252213_image&amp;skuId=1226893" data-pid="P252213" data-sku="1226893"><img class="product-image" src="/productimages/sku/s1226893-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Magnolia Nobile</span></span><span class="price"><span class="list-price">
				$180.00
			</span></span><span class="stars"><span style="width:68px;" title="Rating: 4.33"></span></span></span></a></li></ul><a class="carousel-button previous" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a><a class="carousel-button next" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a></div></div><div class="carousel-container odd" data-name="Acqua_Di_Parma_Must_Haves_Carousel"><h2 class="carousel-title">Bestsellers</h2><div class="carousel-naturale-container" style="position: relative;"><ul class="carousel initialized" style="white-space: nowrap;"><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/magnolia-nobile-P252213?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P252213_image&amp;skuId=1226885" data-pid="P252213" data-sku="1226885"><img class="product-image" src="/productimages/sku/s1226885-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Magnolia Nobile</span></span><span class="price"><span class="list-price">
				$120.00
			</span></span><span class="stars"><span style="width:68px;" title="Rating: 4.33"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/colonia-P163604?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P163604_image&amp;skuId=967083" data-pid="P163604" data-sku="967083"><img class="product-image" src="/productimages/sku/s967083-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Colonia</span></span><span class="price"><span class="list-price">
				$96.00
			</span></span><span class="stars"><span style="width:65px;" title="Rating: 4.16"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/gelsomino-nobile-P298761?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P298761_image&amp;skuId=1382977" data-pid="P298761" data-sku="1382977"><img class="product-image" src="/productimages/sku/s1382977-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Gelsomino Nobile</span></span><span class="price"><span class="list-price">
				$120.00
			</span></span><span class="stars"><span style="width:73px;" title="Rating: 4.64"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/blu-mediterraneo-mandorlo-di-sicilia-P307803?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P307803_image&amp;skuId=1417575" data-pid="P307803" data-sku="1417575"><img class="product-image" src="/productimages/sku/s1417575-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Blu Mediterraneo Mandorlo Di Sicilia</span></span><span class="price"><span class="list-price">
				$98.00
			</span></span><span class="stars"><span style="width:77px;" title="Rating: 4.90"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/colonia-essenza-P269110?skuId=1284488&amp;icid2=Acqua_Di_Parma_Must_Haves_Carousel_P269110_image&amp;skuId=1284488" data-pid="P269110" data-sku="1284488"><img class="product-image" src="/productimages/sku/s1284488-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Colonia Essenza</span></span><span class="price"><span class="list-price">
				$144.00
			</span></span><span class="stars"><span style="width:74px;" title="Rating: 4.71"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/profumo-P252214?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P252214_image&amp;skuId=1227503" data-pid="P252214" data-sku="1227503"><img class="product-image" src="/productimages/sku/s1227503-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Profumo</span></span><span class="price"><span class="list-price">
				$228.00
			</span></span><span class="stars"><span style="width:75px;" title="Rating: 4.75"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/colonia-intensa-P200313?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P200313_image&amp;skuId=1059500" data-pid="P200313" data-sku="1059500"><img class="product-image" src="/productimages/sku/s1059500-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Colonia Intensa</span></span><span class="price"><span class="list-price">
				$96.00
			</span></span><span class="stars"><span style="width:67px;" title="Rating: 4.27"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/colonia-deodorant-P380571?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P380571_image&amp;skuId=967224" data-pid="P380571" data-sku="967224"><img class="product-image" src="/productimages/sku/s967224-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Colonia Deodorant</span></span><span class="price"><span class="list-price">
				$38.00
			</span></span><span class="stars"><span style="width:71px;" title="Rating: 4.50"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/blu-mediterraneo-arancia-di-capri-P375388?skuId=1417559&amp;icid2=Acqua_Di_Parma_Must_Haves_Carousel_P375388_image&amp;skuId=1417559" data-pid="P375388" data-sku="1417559"><img class="product-image" src="/productimages/sku/s1417559-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Blu Mediterraneo Arancia Di Capri</span></span><span class="price"><span class="list-price">
				$98.00
			</span></span><span class="stars"><span style="width:61px;" title="Rating: 3.86"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/iris-nobile-P379225?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P379225_image&amp;skuId=1035831" data-pid="P379225" data-sku="1035831"><img class="product-image" src="/productimages/sku/s1035831-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Iris Nobile</span></span><span class="price"><span class="list-price">
				$120.00
			</span></span><span class="stars"><span style="width:55px;" title="Rating: 3.50"></span></span></span></a></li><li class="carousel-item" style="display: inline-block; vertical-align: top;"><a class="product-item" href="/blu-mediterraneo-fico-di-amalfi-P307801?icid2=Acqua_Di_Parma_Must_Haves_Carousel_P307801_image&amp;skuId=1417591" data-pid="P307801" data-sku="1417591"><img class="product-image" src="/productimages/sku/s1417591-main-grid.jpg"><span class="product-info"><span class="name"><span class="brand">Acqua Di Parma</span><span class="display-name">Blu Mediterraneo Fico Di Amalfi</span></span><span class="price"><span class="list-price">
				$98.00
			</span></span><span class="stars"><span style="width:69px;" title="Rating: 4.38"></span></span></span></a></li></ul><a class="carousel-button previous" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a><a class="carousel-button next" style="-webkit-user-select: none;"><span class="carousel-button-arrow"></span></a></div></div></div>
  <div class="grids"></div>


			</div>