     <?php echo $this->Html->css('new-shop');
     		echo $this->Html->script('menu');
      ?>
      <div id="mobileContent">
        <div id="shop-container" class="speed-page-transition">
          <div id="page-loading" class="page-section page-level-0" style="-webkit-backface-visibility: hidden;">
            <h1>
              Loading...
            </h1>
          </div>
          <div id="page-accordion" class="page-section page-level-0 page-active" style="-webkit-backface-visibility: hidden;">
            <div class="cat-node root" id="category-accordion" style="left: 0%; position: relative; display: block;">
              <ul >
              	<?php
              	if(isset($menu))
              		foreach($menu as $key => $value){
              	 ?>

                <li class=" root1 root <?php if(!empty($value['child'])) echo 'has-children';?>">
                	<a  href="#"><?php echo $value['name']; ?></a>
                 	<!--Level 2-->
                 	<?php if(!empty($value['child'])){?>
	                  <ul class="ul_level2" style="display:none">
	                  	<?php foreach ($value['child'] as $key2 => $value2) {?>
		                  	<li class="li_level2 <?php if(!empty($value2['child'])) echo 'has-children';?>">
		                  		<a><?php echo $value2['name']; ?></a>
		                  		<!--Level 3-->
		                  		<?php if(!empty($value2['child'])){?>
				                  <ul class="ul_level3" style="display:none" >
				                  	<?php foreach ($value2['child'] as $key3 => $value3) {?>
					                  	<li>
					                  		<a href="<?php echo URL;?>/shops/get_product_from_category/<?php echo $key3;?>">    <?php echo $value3['name']; ?></a>
					                  	</li>
					                <?php }?>
				                  </ul>
			                	<?php }?>

		                  	</li>
		                <?php }?>
	                  </ul>

                	<?php }?>
                </li>
                <?php } ?>
            </ul>
            </div>
            <div id="additional-links">
              <h2>
                More Ways to Shop
              </h2>
              <ul>
                <li class="root">
                  <a href="/search/saleResults.jsp?keyword=sale">
                    Sale
                  </a>
                </li>
                <li class="root">
                  <a href="/giftcards">
                    Gift Cards
                  </a>
                </li>
                <li class="root">
                  <a href="/contentStore/mediaContentTemplate.jsp?mediaId=14300062">
                    Bestsellers
                  </a>
                </li>
                <li class="root">
                  <a href="/contentStore/mediaContentTemplate.jsp?mediaId=11000020">
                    Weekly Specials
                  </a>
                </li>
                <li class="root">
                  <a href="/contentStore/mediaContentTemplate.jsp?mediaId=12800020">
                    What's New
                  </a>
                </li>
                <li class="root">
                  <a href="/contentStore/mediaContentTemplate.jsp?mediaId=12200056">
                    Sephora Hot Now
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div id="page-landing" class="page-section page-level-0" style="-webkit-backface-visibility: hidden;">
            <h1 id="landing-header">
            </h1>
            <div id="landing-navigation" class="linear-tree-nav">
            </div>
            <div id="landing-bcc-content">
              <div class="carousels">
              </div>
              <div class="grids">
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
<script type="text/javascript">
	$(".root1").click(function(e){
		$(this).toggleClass('expanded');
		$(".ul_level2",this).animate({
			height:'toggle'
		});
		e.stopPropagation();
	});

	$(".li_level2").click(function(e){
		$(".ul_level3", this).animate({
			height:'toggle'
		});
		e.stopPropagation();
	});
	$(".li_level2").click(function(){
		$(this).toggleClass("expanded");
	});
</script>





