<div style="display: none;" id="mf_1250_div"></div>
<img style="display: inline;" id="homepage-background" <?php echo $this->Html->image('homepage_bg.jpg', array('alt' => 'POS')); ?>>
<div id="mobileContent">
<!-- BBTEMPLATE: home -->
	<div id="home-container">
	  <div id="home-tiles">
      <?php
        foreach ($mobile_center_menu as $key => $value) {
          if(isset($value['status']) && $value['status']==0){
      ?>
	       <a class="pop-tile" href="<?php echo URL;?>/shops/<?php echo $value['key'];?>"><?php echo $value['name'];?></a>
      <?php } } ?>

    </div>
    <?php if(!empty($mobile_welcome)) {?>
	  <div id="bi-container" class="pop-tile not-recognized">
	    <div id="bi-card-image"></div>

	    <h3 class="not-recognized header">Welcome</h3>
	    <h3 class="recognized header normal">Hello <span id="bi-name"></span>!</h3>

	    <div id="bi-account-links" class="inline-links">
            <?php foreach($mobile_welcome as $i => $ii){
                if(isset($ii['status']) && $ii['status'] == 0){
                ?>
	      <a onclick="Sephora.ui.jsignonController.show()"><span><?php echo $ii['name'] ?></span></a>
          <?php } } ?>
	      <!-- <a onclick="Sephora.ui.registrationController.show()">
	          <span class="not-recognized" id="click_registry">Register</span>
	          <span class="recognized">Become a Beauty Insider</span>
	      </a> -->
	    </div>

	    <div id="bi-points">
	    	<span id="bi-points-amount">0</span>
	    	<label>points</label>
	    </div>
	  </div>
      <?php } ?>

      <?php if(!empty($mobile_menu_today)){ ?>
          <div id="today-container" class="pop-tile link-list">
              <h3 class="header">Today</h3>
              <?php   foreach($mobile_menu_today as $k => $v){
                        if(isset($v['status']) && $v['status']==0){ ?>
        	               <a href="#"><?php echo $v['name'];?></a>
               <?php     }
                      } ?>
    	  </div>
      <?php }?>

	  <?php if(!empty($mobile_quick_links_home)){   ?>
        <div id="quick-container" class="pop-tile link-list">
        <h3 class="header">Quick Links</h3>
        <?php
                foreach($mobile_quick_links_home as $kk => $vv){
                    if(isset($vv['status']) && $vv['status'] == 0){
         ?>
	    <a ><?php echo $vv['name'];?></a>
	    <?php } } ?>
	  </div>
      <?php } ?>

	  <div id="beauty-bag" class="pop-tile">
	    <a id="beauty-bag-logo"><span>My <b>Beauty</b> Bag</span></a>
	    <div class="inline-links">
	      <a ><span>Loves</span></a>
	      <a ><span>Purchases</span></a>
	    </div>
	  </div>

	  <div id="trending-carousel" class="pop-tile" data-item-count="3">
	    <h3 class="header">Trending Now</h3>
	      <!-- CAROUSEL ITEMS ARE ROUGHLY 272px WIDTH, SO RETINA UP TO 544px for great justice -->
	      <div data-ss-state="ready" data-ss-id="1" class="soysauce-carousel" data-source="memcache" data-ss-widget="carousel" data-ss-options="peek">
	      	<div data-ss-component="container_wrapper">
	      		<div style="text-align: center;">
              <a href="#" class="mobile-image-link">
                <img src="https://bb-dashboard.s3.amazonaws.com/bdbca16" class="mobile-sephora-lazyload">
              </a>
            </div>
	      	</div>
          <div data-ss-state="enabled" data-ss-component="button" data-ss-button-type="next"></div>
      </div>
        </div>

		</div>
	</div>

<style>
    label[for='subscribe-to-email-checked'] {
      display:inline !important;
      padding-left:5px;
      padding-bottom:2px;
      vertical-align:top;

    }
    #register-pop h2 { font-weight:700; text-transform:capitalize;}

    #pop-social-terms, .stay-in-the-know { display:none; }

    #register-popinput[type='password'], #register-pop input[type='text'], #register-pop select {
      -webkit-border-radius: 3px;
      -moz-border-radius: 3px;
      border-radius: 3px;
    }

    .btn-cancel-register {
      float:left;
      clear:left;
      margin: 10px 0 0;
      width: 120px;
      font-family:Avalon-Demi;
      background: white;
      color: black;
      border:1px solid #666;
      cursor:pointer;
      text-transform:uppercase;
    }
    .indent { display:none;}

    #bi-reg-pop .join-bi img { display:block; margin:10px auto; }
  </style>


<style>
  #register-pop #pop-terms {
    width: 100% !important;
    margin: 0px;
    padding: 0px;
    top:0px !important;
  }
</style><style>
  .signin-returning.container > h3 { margin:10px 0; }
  .btn-cancel-signin {
    float:left;
    clear:left;
    margin: 10px 0 0;
    width: 120px;
    background: white;
    color: black;
    border:1px solid #666;
    cursor:pointer;
    text-transform:uppercase;
    font-family:Avalon-Demi;
  }
  #signin-pop input[type='text'], #signin-pop input[type='password'],   #signin-pop select{
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
  }

</style>
<div id="signin-pop" class="signin popup">
  <div class="close icon"></div>
    <div class="modal-header">
      <button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
      <h4 class="modal-title">sign in</h4>
    </div>
    <div class="modal-body">
      <div class="signin-returning container">
        <h3 class="alt-book text-upper">Sign In To Your Account</h3>

        <form id="loginPopupMainForm" novalidate name="loginPopup" method="post" action="http://m.sephora.com/popups/?_DARGS=/popups/popupLogin.jsp#" >

          <div class="email container">
            <input id="user_name" name="user_name" value="" class="form-control" placeholder="E-mail address" type="email">
          </div>
          <div class="password container">

            <input id="password" name="/atg/userprofiling/ProfileFormHandler.value.password" value="" class="form-control" autocomplete="off" placeholder="Password" type="password">
            <button class="btn-cancel-signin">Cancel</button>
            <button class="btn btn-primary btn-sign-in" type="submit" name="sign_in" data-ajaxlink="">sign in</button>
          </div>
        </form>
      </div>
      <div class="signin-new container">
          <h3 class="alt-book text-upper">New customer or signed-up in store?</h3>
        <form id="loginPopupRegistrationForm" novalidate name="loginPopupRegistration" method="post">
        <div class="email container">

            <input id="email-new" name="user_name" value="" class="form-control" placeholder="E-mail address" type="email">
            <button class="btn btn-primary btn-continue" type="submit" name="continue" data-ajaxlink="">continue</button>
            <p class="help-block">Start here if you’re new to our web site or registered for Beauty Insider in a Sephora store.</p>
          </div>
      </form>
    </div>
    </div>
</div>
