<div id="mobileFooter" role="footer">   
	<div class="freeShipBanner">3-DAY SHIPPING <b>FREE over $50</b></div>

	<ul class="bottom-nav-2">
	  <li><a data-analytics="core nav" data-analytics-type="global footer:help">Help</a></li>
	  <li><a data-analytics="core nav" data-analytics-type="global footer:stores">Stores</a></li>
	  
	  <li><a onclick=" if (!confirm('Switch to Full Site?')) return false;" data-analytics="core nav" data-analytics-type="global footer:desktop">Desktop</a></li>
	  <li id="contact-us">
	    <a data-analytics="core nav" data-analytics-type="global footer:contact us">Contact Us</a>
	  </li>
	</ul>

	<div class="bottom-footer">
	  <a class="legal" data-analytics="core nav" data-analytics-type="global footer:legal">Legal</a>
	  <p class="copyright">
	    <span class="logo"></span>
	    <span class="brand">&copy;2014 Sephora USA, Inc.</span>
	  </p>
	</div>
</div>