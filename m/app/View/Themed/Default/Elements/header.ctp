<div id="mWrapper">
	<div id="mobileHeader" role="heading">
		<div id="top" class="my-sephora">
				<div id="sign-in">
					<?php if ($this->Session->check('arr_user')){
						$arr_user = $this->Session->read('arr_user'); ?>
					<a href="javascript:void(0);"><?php echo $arr_user['contact_name']; ?></a>
					<?php }else{ ?>
					<a href="javascript:sigin_show();">Sign in</a>
					<?php } ?>
				</div>
				<div id="logo"><a href="<?php echo URL; ?>">Sephora</a></div>
				<div id="header-basket">
					<a class="header-basket" href="<?php echo URL; ?>/shops/checkout">
						<?php
							if( $this->Session->check('my_cart') ){
								$my_cart = $this->Session->read('my_cart');
								echo '<span class="item-count active">'.count($my_cart).'</span>';
							}else
								echo '<span class="item-count">0</span>';
						?>
						<span class="basket-img">
							<span class="bar"></span>
							<span class="bar"></span>
							<span class="bar"></span>
							<span class="bar"></span>
							<span class="bar handle left"></span>
							<span class="bar handle right"></span>
						</span>
					</a>
				</div>
		</div>
		<ul class="top-nav" role="navigation">
			<li class="l-one"><a data-analytics="core nav" data-analytics-type="globalnav:shop" class="shop" href="<?php echo URL;?>/shops/show_category">Shop</a></li>
			<li class="l-two"><a data-analytics="core nav" data-analytics-type="globalnav:myaccount" class="myaccount" href="#">My Account</a></li>
			<li class="l-one"><a data-analytics="core nav" data-analytics-type="globalnav:shoppinglist" class="mylist" href="#">Loves</a></li>
		</ul>
		<div class="search">
			<form id="site-search-form" method="POST" action="http://m.sephora.com/search/search.jsp?_DARGS=/common/header/search.jsp.siteFormId">
				<input class="site-search-field ui-autocomplete-input input-text" id="site-search-field" autocomplete="off" aria-autocomplete="list" aria-haspopup="true" data-ignore-flame="true" role="textbox" placeholder="SEARCH SEPHORA" type="text">
				<span class="ui-helper-hidden-accessible" aria-live="polite" role="status"></span>
				<a id="mag-glass">
					<span class="circle"></span>
					<span class="bar"></span>
				</a>
			</form>
		</div>
	</div>
</div>

<!-- BaoNam: 17/03/2014 -->
<style>
	#signin-pop{display: none;}
  .signin-returning.container > h3 { margin:10px 0; }
  .btn-cancel-signin {
	float:left;
	clear:left;
	margin: 10px 0 0;
	width: 120px;
	background: white;
	color: black;
	border:1px solid #666;
	cursor:pointer;
	text-transform:uppercase;
	font-family:Avalon-Demi;
  }
  #signin-pop input[type='text'], #signin-pop input[type='password'],   #signin-pop select{
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
  }
</style>

<div id="signin-pop" class="signin popup">
	<div class="modal-header">
		<button type="button" class="icon icon-close"></button>
		<h4 class="modal-title">sign in</h4>
	</div>
	<div class="modal-body">
		<div class="signin-returning container">
		  <h3 class="alt-book text-upper">Sign In To Your Account</h3>
			<div id="error_login_popup" class="error-message" style="display:none"></div>
			<div class="email container">
				<input id="txt_user_name" name="txt_user_name" value="" class="form-control" type="email" placeholder="E-mail address">
			</div>
			<div class="password container">
				<input id="txt_user_pass" name="txt_user_pass" value="" class="form-control" type="password" autocomplete="off" placeholder="Password">
				<p class="help-block">
					<a class="reset-password" href="#">forgot password?</a>
				</p>
				<button class="btn-cancel-signin" onclick="$('#signin-pop').hide();return false;">Cancel</button>
				<button class="btn btn-primary btn-sign-in" type="submit" name="sign_in" onclick="submit_login_popup()">sign in</button>
			</div>
		</div>
		<div class="signin-new container">
			<h3 class="alt-book text-upper">New customer or signed-up in store?</h3>
			<form id="loginPopupRegistrationForm" novalidate="novalidate" name="loginPopupRegistration" action="" method="post">
				<div class="email container">
					<input id="email-new" name="user_name" value="" class="form-control" type="email" placeholder="E-mail address">
					<button class="btn btn-primary btn-continue" type="submit" name="continue">continue</button>
					<p class="help-block">Start here if you&rsquo;re new to our web site or registered for Beauty Insider in a Sephora store.</p>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	function submit_login_popup(){
		$("#error_login_popup").hide();
		$.ajax({
			url: '<?php echo URL; ?>/users/login_popup',
			type: 'POST',
			data: { txt_user_name: $("#txt_user_name").val(), txt_user_pass: $("#txt_user_pass").val() },
			success: function(html) {
				if (html != "ok") {
					$("#error_login_popup").html(html).show();
				}else{
					console.log("ok");
					location.reload(true);
				}
			}
		});
	}
	function sigin_show(){
		$("#signin-pop").show();
	}
	</script>
</div>