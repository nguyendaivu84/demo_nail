<div id="signin-pop" class="signin popup">
  <div class="close icon"></div>
    <div class="modal-header">
      <button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
      <h4 class="modal-title">sign in</h4>
    </div>
    <div class="modal-body">
      <div class="signin-returning container">
        <h3 class="alt-book text-upper">Sign In To Your Account</h3>
        
        <form id="loginPopupMainForm" novalidate name="loginPopup" method="post">

          <div class="email container">
            <input id="user_name" name="user_name" value="" class="form-control" placeholder="E-mail address" type="email">
          </div>
          <div class="password container">
            
            <input id="password" name="/atg/userprofiling/ProfileFormHandler.value.password" value="" class="form-control" autocomplete="off" placeholder="Password" type="password">
            <button class="btn-cancel-signin">Cancel</button>
            <button class="btn btn-primary btn-sign-in" type="submit" name="sign_in" data-ajaxlink="">sign in</button>
          </div>
        </form>
      </div>
      <div class="signin-new container">
          <h3 class="alt-book text-upper">New customer or signed-up in store?</h3>
        <form id="loginPopupRegistrationForm" novalidate name="loginPopupRegistration" method="post">
        <div class="email container">
            
            <input id="email-new" name="user_name" value="" class="form-control" placeholder="E-mail address" type="email">
            <button class="btn btn-primary btn-continue" type="submit" name="continue" data-ajaxlink="">continue</button>
            <p class="help-block">Start here if you’re new to our web site or registered for Beauty Insider in a Sephora store.</p>
          </div>
      </form>
    </div>
    </div>
</div>