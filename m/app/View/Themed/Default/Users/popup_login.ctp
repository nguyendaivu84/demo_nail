<style>
	.signin-returning.container > h3 { margin:10px 0; }
	.btn-cancel-signin {
		float:left;
		clear:left;
		margin: 10px 0 0;
		width: 120px;
		background: white;
		color: black;
		border:1px solid #666;
		cursor:pointer;
		text-transform:uppercase;
		font-family:Avalon-Demi;
	}
	#signin-pop input[type='text'], #signin-pop input[type='password'], 	#signin-pop select{
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
	}
</style>
<div id="signin-pop" class="signin popup">
	<div class="modal-header">
		<button type="button" class="icon icon-close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">sign in</h4>
	</div>
	<div class="modal-body">
		<div class="signin-returning container">
			<h3 class="alt-book text-upper">Sign In To Your Account</h3>
			<form id="loginPopupMainForm" novalidate="novalidate" name="loginPopup" action="https://m.sephora.com/popups/?_DARGS=/popups/popupLogin.jsp#" method="post">
				<div class="email container">
					<input id="user_name" name="/atg/userprofiling/ProfileFormHandler.value.login" value="" class="form-control" type="email" placeholder="E-mail address">
					<input name="_D:/atg/userprofiling/ProfileFormHandler.value.login" value=" " type="hidden"></div>
				<div class="password container">
					<input id="password" name="/atg/userprofiling/ProfileFormHandler.value.password" value="" class="form-control" type="password" autocomplete="off" placeholder="Password">
					<input name="_D:/atg/userprofiling/ProfileFormHandler.value.password" value=" " type="hidden">
					<input name="sign_in" value="sign in" type="hidden">
					<input name="_D:sign_in" value=" " type="hidden">
					<p class="help-block">
						<a class="reset-password" href="#">forgot password?</a>
					</p>
					<input id="forgotPasswordLinkDo" style="display:none;" name="ForgotPasswordLink" value="ForgotPasswordLink" type="submit">
					<input name="_D:ForgotPasswordLink" value=" " type="hidden">
					<button class="btn-cancel-signin">Cancel</button>
					<button class="btn btn-primary btn-sign-in" type="submit" name="sign_in" data-ajaxlink="">sign in</button>
				</div>
				<input name="_DARGS" value="/popups/popupLogin.jsp" type="hidden"></form>
		</div>
		<div class="signin-new container">
			<h3 class="alt-book text-upper">New customer or signed-up in store?</h3>
			<form id="loginPopupRegistrationForm" novalidate="novalidate" name="loginPopupRegistration" action="https://m.sephora.com/popups/?_DARGS=/popups/popupRegistrationCheckAreaIn.jsp#" method="post">
				<div class="email container">
					<input id="email-new" name="user_name" value="" class="form-control" type="email" placeholder="E-mail address">
					<input name="_D:user_name" value=" " type="hidden">
					<button class="btn btn-primary btn-continue" type="submit" name="continue" data-ajaxlink="">continue</button>
					<input name="/atg/userprofiling/ProfileFormHandler.registrationCheck" value="continue" type="hidden">
					<input name="_D:/atg/userprofiling/ProfileFormHandler.registrationCheck" value=" " type="hidden">
					<p class="help-block">Start here if you&rsquo;re new to our web site or registered for Beauty Insider in a Sephora store.</p>
				</div>
				<input name="_DARGS" value="/popups/popupRegistrationCheckAreaIn.jsp" type="hidden"></form>
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function() {
		$(".not-you-toggle").live('click',function() {
			Sephora.util.Login.logOut(function(response) {
				jQuery("body").trigger("logout");
				// .maincontent-content or .user, , .purchase-history, .payment-methods ?
				if ( jQuery(".user").length > 0 ) {
					window.location.href = Sephora.conf.myAccountLandingUrl;
				} else if ( jQuery(".checkout-module").length > 0 ) {
					window.location.href = Sephora.conf.contextPath ? Sephora.conf.contextPath : "/";
				} else {
					location.reload();
				}
			}, "html");
		});

		$("#loginPopupMainForm").delegate("input", "keypress", function(e) {
			if (e.which == 13) {
				e.preventDefault();
				e.stopPropagation();
				$('form#loginPopupMainForm').submit();
			}
		});

		$("#loginPopupMainForm").delegate("button.btn-cancel-signin", "click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$("#signin-pop").toggle();
			$("p.field-error").remove();
		});

		if ($("div.logged-in-bi-user").length) {
			$("h3.helvetica-light").before("<p class=\"bimessage\">sign in or sign up for Beauty insider to view your point balance and rewards available. Learn more about Beauty Insider")
		}
	});
	</script>
</div>