<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Shop Makeup, Fragrance, Skincare &amp; More Beauty | Sephora</title>
	<?php
		echo $this->Html->script('jquery-1.10.2.min');
		echo $this->Html->css('style');
		echo $this->Html->css('main.v1');
		echo $this->Html->css('new-shop');

		echo $this->Html->css('jquery.mCustomScrollbar');
		echo $this->Html->script('jquery.mCustomScrollbar.concat.min');

		echo $this->Html->script('jquery-ui');
		echo $this->Html->css('jquery-ui');
	?>
	<script type="text/javascript">
		// $(function() {
		// 	$(".container_same_category").mCustomScrollbar({
		// 		scrollButtons:{
		// 			enable:false
		// 		},
		// 		advanced:{
		// 			autoScrollOnFocus: false,
		// 		}
		// 	});
		// });
	</script>
</head>
<body>
	<?php echo $this->element('header'); ?>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element('footer'); ?>
	<div id="inline-basket" class="active"></div>
</body>
</html>