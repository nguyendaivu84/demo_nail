<?php
define('SEARCH_FORM','Search');
define('SIGN_UP_TEXT','Sign up for Sephora e-mails');
define('SIGN_UP_TEXT_INPUT_PLACEHOLDER','Email');
define('STORE_LOCATION_AND_EVENT','Store Locations & Events');
define('STORE_LOCATION_AND_EVENT_INPUT_PLACEHOLDER','Postal Code or City, Province');
